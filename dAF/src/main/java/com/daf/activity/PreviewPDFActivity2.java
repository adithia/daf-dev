package com.daf.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.salesforce.R;

import java.io.File;

public class PreviewPDFActivity2 extends Activity {
    private PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_pdf2);
        pdfView = (PDFView) findViewById(R.id.pdfView);

        if (getIntent() != null) {
            String viewType = getIntent().getStringExtra("viewType");
            if (viewType != null) {
                if (viewType.equalsIgnoreCase("filePath")) {
                    try {
                        String filename = getIntent().getStringExtra("filename");
                        File pdf = new File(filename);
                        pdfView.fromFile(pdf)
                                .defaultPage(0)
                                .enableSwipe(true)
                                .enableDoubletap(false)
                                .enableAntialiasing(true)

                                .onPageError(new OnPageErrorListener() {
                                    @Override
                                    public void onPageError(int page, Throwable t) {
                                        Toast.makeText(PreviewPDFActivity2.this, "Error", Toast.LENGTH_SHORT).show();
                                    }
                                }).onRender(new OnRenderListener() {
                            @Override
                            public void onInitiallyRendered(int nbPages, float pageWidth, float pageHeight) {
                                pdfView.fitToWidth();
                            }
                        })
                                .load();
                    } catch (Exception e) {
                        Toast.makeText(PreviewPDFActivity2.this, "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
