package com.daf.activity;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.model.ModelApplication;
import com.salesforce.service.DAFReciever;
import com.salesforce.utility.Helper;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class MenuApplicationActivity extends Activity{
	public static boolean oneapp = false;
	private Vector<String> vRecord = new Vector<String>();
	private Vector<ModelApplication> vector = new Vector();
//	private Bitmap bitmap = null;;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
				
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.menuapplication2);
		
		DAFReciever.downloadOrder(getApplicationContext());
		
		//check if app context null and app will close by system
		Utility.checkingAppContex(MenuApplicationActivity.this);	
		
		((TextView)findViewById(R.id.textJudul)).setText("Menu Application");
		
		((ImageView)findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		Recordset rst = Connection.DBquery("select * from application");
		if (rst.getRows() == 1) {

			Generator.currApplication = rst.getText(0, "app_id");
			try {
				Generator.currAppTheme = rst.getText(0, "app_theme");
			} catch (Exception e) { }
			Generator.currApplicationName = rst.getText(0, "app_theme").toLowerCase();
			Intent intent = new Intent(MenuApplicationActivity.this, DAFMenu.class);
			intent.putExtra("app_id", Generator.currApplication);
			startActivity(intent);
			oneapp=true;
			finish();
			return;
		}
		
		vRecord = rst.getRecordFromHeader("app_id");
		if (vRecord.size() > 0) {
			for (int i = 0; i < vRecord.size(); i++) {
				try {
					Cursor c = Connection.DBexecute("select * from application where app_id ='" + vRecord.elementAt(i).toString() + "'");
					if (c.getCount() > 0) {
						c.moveToFirst();
						do {
							ModelApplication model = new ModelApplication();
							model.setAppName(c.getString(c.getColumnIndex("app_name")));
							model.setAppIcon(c.getString(c.getColumnIndex("app_icon")));
							model.setAppId(c.getString(c.getColumnIndex("app_id")));
							try {
								model.setAppTheme(c.getString(c.getColumnIndex("app_theme")));
							} catch (Exception e) { }
							vector.addElement(model);
						} while (c.moveToNext());
					}
					c.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		((ListView)findViewById(R.id.lstApp)).setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, 	long arg3) {
				try {
					Generator.currAppTheme = vector.elementAt(position).getAppTheme();
				} catch (Exception e) { }
				Generator.currApplicationName = Helper.releaseTag((String)view.getTag(), false, true).toLowerCase();
				Generator.currApplication = Helper.releaseTag((String)view.getTag(), true, false);
				Intent intent = new Intent(MenuApplicationActivity.this, DAFMenu.class);
				intent.putExtra("app_id", Generator.currApplication);
				startActivity(intent);
			}
		});
		
		if (vector.size()==1) {
				try {
					Generator.currAppTheme = vector.elementAt(0).getAppTheme();
				} catch (Exception e) { }
				Generator.currApplicationName = vector.elementAt(0).getAppName().toLowerCase();
				Generator.currApplication = vector.elementAt(0).getAppId() ;
				Intent intent = new Intent(MenuApplicationActivity.this, DAFMenu.class);
				intent.putExtra("app_id", Generator.currApplication);
				startActivity(intent);
				oneapp=true;
			finish();
		}else{
			oneapp=false;			
			Setuplist(vector);
		}
		
	}
	
	public void onBackPressed() {
		final AlertDialog alertDialog = new AlertDialog.Builder(MenuApplicationActivity.this).create();
		alertDialog.setMessage("Are You Sure To Log Out ?");
		alertDialog.setCancelable(false);
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Oke", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
				finish(); 
			}
		});

		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}
		});
		alertDialog.show();
	}
	
	@SuppressWarnings("rawtypes")
	private void Setuplist(Vector data){
		AdapterConnector.setConnector((ListView)findViewById(R.id.lstApp), data, R.layout.mainmenuadapter2, new AdapterInterface() {			
			@Override
			public View onMappingColumn(int row, final View v, Object data) {
				v.setTag(((ModelApplication)data).getAppId() + "|" +((ModelApplication)data).getAppName());	
				final String icon = ((ModelApplication)data).getAppIcon();
				((TextView)v.findViewById(R.id.TitleMenu)).setText(((ModelApplication)data).getAppName());
				if (icon.contains("http")) {
					Messagebox.newTask(new Runnable() {						
						@Override
						public void run() {
							Utility.LoadImageDirect((ImageView)v.findViewById(R.id.iconMenu), icon);							
						}
					});
				}
				return v;
			}
		});
	}
	

}
