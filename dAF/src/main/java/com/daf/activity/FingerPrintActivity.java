package com.daf.activity;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingerq.FPC1080Sensor;
import com.fingerq.fingerprint.algorithm.matching.FPC1080MatchingEngine;
import com.fingerq.generic.biometrics.device.BiometricSample;
import com.fingerq.generic.biometrics.device.BiometricSampleListener;
import com.fingerq.generic.biometrics.device.Sensor;
import com.fingerq.generic.biometrics.matching.MatchingEngine;
import com.fingerq.helper.BiometricsSystem;
import com.fingerq.helper.EnrollmentListener;
import com.fingerq.helper.IdentificationListener;
import com.fingerq.io.MX25LStorage;
import com.fingerq.io.Storage;
import com.salesforce.R;
import com.salesforce.adapter.DeviceManager;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class FingerPrintActivity extends Activity {
	
	
	private Button btnEnroll;
	private Button btnValidasi;
	private Button btnSubmit;
	private Button btnCancel;
	private ImageView imgViewFinger;
	private TextView txtInfo;
//	private Handler handler;
	private Bitmap bitmap;
	// default score for identification
	private int _security = 484;
	// default count for enrollment
	static final private int ENROLMENT_COUNT = 3;
	private BiometricsSystem _biometricsSystem;
	 
	private String bufferFile = "";
	private String returnFile = "";
	private String filescan = "";
	
	
	private BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			//<usb-device vendor-id="1027" product-id="24596" /> <!-- Q180Mini -->
			if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED) == true) {
				//180 Mini is not detected
				txtInfo.setText( "180 Mini not detected");
			} else if (intent.getAction().equals(DeviceManager.USB_PERMISSION_REQUEST) == true) {
				if (DeviceManager.isDeviceAttached(context) == false)
					return;

				if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false) == true) {
					//180 Mini is detected
					prepareBiometricsSystem(context);
					txtInfo.setText( "Permission OK");
//					enroll(getApplicationContext());
				} else {
					//No USB permission
					txtInfo.setText( "No USB Permission");
				}
			}else  if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED) == true) {
				if (_biometricsSystem!=null) {					
				}else{
					DeviceManager.requestPermission(context);
				}
				txtInfo.setText( "ACTION_USB_DEVICE_ATTACHEDd");
				 
			}
		}
	};
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.finger);

		returnFile = Utility.getDefaultTempPath("finger-nikita");
		bufferFile = Utility.getDefaultPath("finger-creadential");
		
		
		btnEnroll = (Button)findViewById(R.id.btnEnroll);
		btnValidasi = (Button)findViewById(R.id.btnValidasi);
		btnSubmit = (Button)findViewById(R.id.btnSubmit);
		btnCancel = (Button)findViewById(R.id.btnCancel);
		txtInfo = (TextView)findViewById(R.id.txtInfo);
		imgViewFinger = (ImageView)findViewById(R.id.imageView1);
		
		
//		btnEnroll.setEnabled(false);
		btnValidasi.setEnabled(false);
//		btnSubmit.setEnabled(false);
		

		//
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
		intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
		intentFilter.addAction(DeviceManager.USB_PERMISSION_REQUEST);
		
		registerReceiver(_broadcastReceiver, intentFilter);
		
		
		btnEnroll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(FingerPrintActivity.this, Finger_New.class);
				if (getIntent().getStringExtra("image").equals("")) {
					intent.putExtra("image", "");
				}else  if (new File(Utility.getDefaultTempPath(getIntent().getStringExtra("image"))).exists()) {
					intent.putExtra("image", Utility.getDefaultTempPath(getIntent().getStringExtra("image")));
				}else if (new File(Utility.getDefaultPath(getIntent().getStringExtra("image"))).exists()) {
					intent.putExtra("image", Utility.getDefaultPath(getIntent().getStringExtra("image")));
				}
				intent.putExtra("fileFinger", getIntent().getStringExtra("fileFinger"));
				startActivityForResult(intent, 0);
				
//				enroll(getApplicationContext());				
//				imgViewFinger.setImageResource(0);				
//				btnValidasi.setEnabled(false);
//				btnSubmit.setEnabled(false);
//				txtInfo.setText("Silahkan Swap jari anda untuk Scanning");				
//				isFinger=false;
			}
		});
		
		
		btnValidasi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				indetify(getApplicationContext());				
				txtInfo.setText("Identifikasi tangan anda");
			}
		});
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				 
				Intent intent = new Intent();
				//pindahkan kesini filenya
				Utility.copyFile(bufferFile, returnFile);				
				setResult(RESULT_OK,intent);
				finish();
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		
		if (getIntent()!=null && getIntent().getStringExtra("image")!=null) {
			String pf =getIntent().getStringExtra("image");
			if (new File(pf).exists()) {
				 Utility.copyFile(pf, bufferFile);
				 Utility.copyFingerImageFile(bufferFile, bufferFile+"png");					 
				 OpenImage(imgViewFinger,bufferFile+"png" );
				 isFinger=true;
			}
			
		}
		
		
	}
	
	private boolean isFinger =false;
	public static void OpenImage(ImageView context, String path) {
		try {
			Bitmap b=BitmapFactory.decodeFile(path);
			if (b!=null) {
				context.setImageBitmap(b);
				context.setVisibility(View.VISIBLE);
			}else {
				context.setVisibility(View.GONE);
			}
			
		} catch (Exception e) {
			context.setVisibility(View.GONE);
		}
	}

	public void onPause() {
		super.onPause();
		releaseBiometricsSystem();
	}

	public void onResume() {
		super.onResume();
		 if (DeviceManager.isDeviceAttached(this) == true &&  DeviceManager.hasPermission(this) == true) {
				prepareBiometricsSystem(this);
		 }else if (DeviceManager.isDeviceAttached(this) == true &&  DeviceManager.hasPermission(this) == false) {
			 DeviceManager.requestPermission(this);
		 }
	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(_broadcastReceiver);
	}

	protected void prepareBiometricsSystem(Context context) {
		// ! [BiometricsSystem.Constructor]
		
		btnEnroll.setEnabled(true);
		btnValidasi.setEnabled(isFinger);
		
		 
		Sensor sensor = new FPC1080Sensor(context);
		Storage storage = new MX25LStorage(context);

	 	_biometricsSystem = new BiometricsSystem(sensor, storage);
		// ! [BiometricsSystem.setMatchingEngine]
		_biometricsSystem.setMatchingEngine(new FPC1080MatchingEngine());
		// ! [BiometricsSystem.setMatchingEngine]

		final FPC1080MatchingEngine nikitaMatchingEngine = new FPC1080MatchingEngine();
		_biometricsSystem.setMatchingEngine(new MatchingEngine() {
			public int identify(List templatedata, BiometricSample arg1, int arg2) {
				List<byte[]> templatedataL = new ArrayList<byte[]>(); 
				try {
					FileInputStream fis = new FileInputStream(bufferFile);
					DataInputStream dis = new DataInputStream(fis);
					int i = dis.readInt(); 
					byte[] none = new byte[i];
					dis.read(none,0,i);
					
					 i = dis.readInt(); 
					 none = new byte[i];
					 dis.read(none,0,i);
					 
					 templatedataL.add(none);
					
					dis.close();
					fis.close();
				} catch (Exception e) { } 
				
				
				return nikitaMatchingEngine.identify(templatedataL, arg1, arg2);
			}

			public byte[] enroll(byte[] templatedata, BiometricSample arg1) {
				 

				return nikitaMatchingEngine.enroll(templatedata, arg1);
			}
		});

		_biometricsSystem.setBiometricSampleListener(new BiometricSampleListener() {
					private BiometricSample _biometricsSample;
					public void onWaitingForSample() {}

					public void onSampleOn() {
						// needrunoinui'
						
						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								imgViewFinger.clearColorFilter();
								// imgViewFinger.setColorFilter(Color.rgb(0xff, 0xff, 0xff),Mode.SRC_IN);
							}
						});
					}

					public void onSampleOff() {
						// ! [BiometricSample.get]
						bitmap = (Bitmap) _biometricsSample.get();
						// ! [BiometricSample.get]
						
						/*
						try {
							FileOutputStream fos =  new FileOutputStream("/sdcard/fuck.png");
						 fos.write(_biometricsSample.getBuffer().array());
						 fos.flush();
						 fos.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
						 */
						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// needrunoinui
								//imgViewFinger.setImageBitmap(bitmap);
								BitmapDrawable bitmapDrawable=new BitmapDrawable(getResources(), bitmap);
								imgViewFinger.setImageBitmap(createGhostIcon(bitmapDrawable, Color.BLACK, true));								
							}
						});
						

					}

					public void onSampleCaptured(BiometricSample biometricSample) {
						_biometricsSample = biometricSample;
					}
				});

	}
	
	private void releaseBiometricsSystem() {
		// ! [BiometricsSystem.release]
		if (_biometricsSystem != null) {
			_biometricsSystem.release();
			_biometricsSystem = null;
		}
		// ! [BiometricsSystem.release]
	}

	protected void enroll(Context context) {
//		btnValidasi.setEnabled(false);
		_biometricsSystem.deleteTemplate(0);;
		_biometricsSystem.enroll(0, ENROLMENT_COUNT);
		_biometricsSystem.setEnrollmentListener(new EnrollmentListener() {
			public void onAccepted(int progress) {
				final String msg = "Good swiping (" + (progress + 1) + ")";
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						txtInfo.setText( msg);
					}
				});
			}
 
			public void onRejected() {
				// Question swiping
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						txtInfo.setText( "onRejected");
					}
				});
			}

			public void onFinished(byte[] templateData) {
				 
				
				
				// Finish enrolment
			/*
				try {
					FileOutputStream fos =  new FileOutputStream("/sdcard/fq.png");
					 Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.RGB_565);
		Canvas s =new Canvas(b);
		 imgViewFinger.draw(s);
		b.compress(CompressFormat.JPEG, 80, fos);
									fos.flush();
									fos.close();
						} catch (Exception e) {
							// TODO: handle exception
						}
						try {
								FileOutputStream fos =  new FileOutputStream("/sdcard/fq.txt");
							 
								fos.write(templateData);
							 fos.flush();
							 fos.close();
					} catch (Exception e) {
						// TODO: handle exception
					}
						*/
						try {
							String bf = Utility.getDefaultPath("bf1");
//							String bh = Utility.getDefaultPath("bf2");
							/*ByteArrayOutputStream bos = new ByteArrayOutputStream();
							    Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.RGB_565);
								Canvas s =new Canvas(b);
								 imgViewFinger.draw(s);
								 b.compress(CompressFormat.JPEG, 80, bos);
							byte[] out = bos.toByteArray();	
							bos=null;*/
							
						
							FileOutputStream fos =  new FileOutputStream(bf);
							Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
							Canvas s =new Canvas(b);			 
							imgViewFinger.draw(s);							 
							b.compress(CompressFormat.PNG, 100, fos);
							fos.flush();
							fos.close();
									
					 	    ByteArrayOutputStream bos = new ByteArrayOutputStream();							
							//fos =  new FileOutputStream(bh);
							BitmapDrawable bitmapDrawable=new BitmapDrawable(new FileInputStream(bf));
							createGhostIcon(bitmapDrawable, Color.BLACK, true).compress(CompressFormat.PNG, 100, bos);
							//fos.flush();
							//fos.close();
					
							
							byte[] out = bos.toByteArray();	
							bos=null;											
							
							fos = new FileOutputStream(bufferFile);
							DataOutputStream dos = new DataOutputStream(fos);
							
							dos.writeInt(out.length ) ;
							dos.write(out ) ;
							dos.writeInt(templateData.length) ;
							dos.write(templateData ) ;							 
							fos.flush();
							fos.close();
						} catch (Exception e) {
							// TODO: handle exception
						}
				
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
			
						btnValidasi.setEnabled(true);
						txtInfo.setText( "Scan Berhasil");
					}
				});
			}
		});

	}

	private Bitmap createGhostIcon(Drawable src, int color, boolean invert) {
	    int width = src.getIntrinsicWidth();
	    int height = src.getIntrinsicHeight();
	    if (width <= 0 || height <= 0) {
	        throw new UnsupportedOperationException("Source drawable needs an intrinsic size.");
	    }

	    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap);
	    Paint colorToAlphaPaint = new Paint();
	    int invMul = invert ? -1 : 1;
	    colorToAlphaPaint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{
	            0, 0, 0, 0, Color.red(color),
	            0, 0, 0, 0, Color.green(color),
	            0, 0, 0, 0, Color.blue(color),
	            invMul * 0.213f, invMul * 0.715f, invMul * 0.072f, 0, invert ? 255 : 0,
	    })));
	    canvas.saveLayer(0, 0, width, height, colorToAlphaPaint, Canvas.ALL_SAVE_FLAG);
	    canvas.drawColor(invert ? Color.WHITE : Color.BLACK);
	    src.setBounds(0, 0, width, height);
	    src.draw(canvas);
	    canvas.restore();
	    return bitmap;
	}
	
	protected void indetify(Context context) {
		_biometricsSystem.identify(_security);
		_biometricsSystem.setIdentificationListener(new IdentificationListener() {
			public void onGranted(int templateId) {
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						btnSubmit.setEnabled(true);
						txtInfo.setText( "Validsi Diterima");
					}
				});
			}

			public void onDenied() {
					runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
//						btnSubmit.setEnabled(false);
						txtInfo.setText( "Validasi Ditolak");
					}
				});
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == 0 && resultCode == RESULT_OK) {
			btnValidasi.setEnabled(true);
//			btnSubmit.setEnabled(true);
			enroll(getApplicationContext());
//			bufferFile = data.getStringExtra("bio");
//			
//			
//			IntentFilter intentFilter = new IntentFilter();
//			intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
//			intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
//			intentFilter.addAction(DeviceManager.USB_PERMISSION_REQUEST);
//			
//			registerReceiver(_broadcastReceiver, intentFilter);
		}
//		super.onActivityResult(requestCode, resultCode, data);
	}
}
