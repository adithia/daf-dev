package com.daf.activity;

import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.component.Component;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class FinderItemActivity extends Activity{
	
	private Vector<String> header = new Vector<String>();
	private Vector<Vector<String>> values = new Vector<Vector<String>>();
	public static int RESULT_FINDER =123456789;
	public static int REQUEST_FINDER =123456789;
	public static int pos;
	private EditText edt;
	private ProgressDialog pDialog;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.finderui);
		
		edt = (EditText)findViewById(R.id.edtFind);
		edt.setEnabled(true);
		
		if (getIntent() != null && getIntent().getStringExtra("PARAM3") != null) {
//			loadData(getIntent().getStringExtra("PARAM3"));

			new loaddata().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getIntent().getStringExtra("PARAM3"));
		}
		
		((Button)findViewById(R.id.btnSearch)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				values.clear();
				header.clear();
				((ListView)findViewById(R.id.lstFinder)).setAdapter(null);
				
//				loadData(makeQuerySearch(edt.getText().toString()));
				
				new loaddata().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, makeQuerySearch(edt.getText().toString()));
				
			}
		});
		
		((ListView)findViewById(R.id.lstFinder)).setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				onclick(pos, (String[])v.getTag());
			}
		});
	}
	
	private class loaddata extends AsyncTask<String, Void, String>{
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pDialog = Utility.showProgresbar(FinderItemActivity.this, "Please wait ...");
			pDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Recordset data = Connection.DBquery(params[0]);
			if (data.getRows() > 0) {					
				values = data.getAllDataVector();
				header = data.getAllHeaderVector();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
//			super.onPostExecute(result);
			setupList(values);
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
		}
		
	};
	
	
	/**
	 * untuk menmbuat parameter komponen menjadi satuan query yang dapat dibaca
	 * oleh database
	 */
	private String makeQuery(){
		String query = "";
		String va = getIntent().getStringExtra("query");
		Vector<String> queries = new Vector<String>();
		if (va.startsWith("[")) {			
			Nset ns =Nset.readJSON(va);
			queries = Utility.splitVector(ns.getData(0).toString(), "?");
			for (int i = 0; i < ns.getArraySize(); i++) {
				
				if (i == 0) {
					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
						query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(edt.getText().toString()).trim() + "%'";	
					}else if(queries.elementAt(i).toString().contains("="))
						query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "'";
					else {
						query = queries.elementAt(i).toString();
					}
				}else					
					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
						query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(edt.getText().toString()).trim() + "%'";	
					}else if(queries.elementAt(i).toString().contains("="))
						query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "'";
					else {
						query = query + queries.elementAt(i).toString();
					}
			}
		}else {
			query = va;
		}
		
		return query;
	}
	
	private String query2Santances(String param){
		Vector<String> par = Utility.splitVector(param, " ");
		
		return "";
	}
	
	private String makeQuerySearch(String param){
		String query = "";
		String va = getIntent().getStringExtra("query");
		Vector<String> queries = new Vector<String>();
		
		Vector<String> par = Utility.splitVector(param, " ");
		if (par.size() > 1) {
			if (va.startsWith("[")) {			
				Nset ns =Nset.readJSON(va);
				queries = Utility.splitVector(ns.getData(0).toString(), "?");
				for (int i = 0; i < ns.getArraySize(); i++) {
					if (i == 0) {
						if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
							if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
								query = queries.elementAt(i).toString() + "REPLACE ('%" + param.trim() + "%',' ','%')";	
							}else{
								query = queries.elementAt(i).toString() + "REPLACE ('%" + 
							StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "%', ' ', '%')";
							}
								
						}else if(queries.elementAt(i).toString().contains("=")){
							if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
								query = queries.elementAt(i).toString() + "'" + param.trim() + "'";
							}else{
								query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "'";
							}
							
						}else if(queries.elementAt(i).toString().contains("NOT IN")){
							Nset nComp = Nset.readJSON(ns.getData(i+1).toJSON());
							query = queries.elementAt(i).toString();
							for (int j = 0; j < nComp.getArraySize(); j++) {
								String strCom = getDataComp(Utility.comp, nComp.getData(j).toString());
								if (j == 0) {
									if (!strCom.equalsIgnoreCase("")) {
										query =  query + "('%" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
									}else{
										query =  query + "('" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
									}
									
								}else if (j == (nComp.getArraySize()-1)) {
									if (!strCom.equalsIgnoreCase("")) {
										query = query + "'%" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%')";
									}else{
										query = query + "'" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "')";
									}
									
								}else {
									if (!strCom.equalsIgnoreCase("")) {
										query = query + "'%"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
									}else{
										query = query + "'"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
									}
									
								}
							}
						}else if (queries.elementAt(i).toString().contains("&&")) {
							query = query + queries.elementAt(i).toString().replace("&&", "=");
						}else {
							query = queries.elementAt(i).toString();
						}
					}else	
						if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
							if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
								query = query + queries.elementAt(i).toString() + "REPLACE ('%" + param.trim() + "%',' ','%')";	
							}else{
								query = query + queries.elementAt(i).toString() + "REPLACE ('%" + 
							StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "%', ' ', '%')";
							}
									
						}else if(queries.elementAt(i).toString().contains("=")){
							if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
								query = query + queries.elementAt(i).toString() + "'" + param.trim() + "'";
							}else{
								query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "'";
							}
								
						}else if(queries.elementAt(i).toString().contains("NOT IN")){
							Nset nComp = Nset.readJSON(ns.getData(i+1).toJSON());
							query = query + queries.elementAt(i).toString();
							for (int j = 0; j < nComp.getArraySize(); j++) {
								String strCom = getDataComp(Utility.comp, nComp.getData(j).toString());
								if (j == 0) {
									if (!strCom.equalsIgnoreCase("")) {
										query =  query + "('%" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
									}else{
										query =  query + "('" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
									}
									
								}else if (j == (nComp.getArraySize()-1)) {
									if (!strCom.equalsIgnoreCase("")) {
										query = query + "'%" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%')";
									}else{
										query = query + "'" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "')";
									}
									
								}else {
									if (!strCom.equalsIgnoreCase("")) {
										query = query + "'%"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
									}else{
										query = query + "'"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
									}
									
								}
							}
						}else if (queries.elementAt(i).toString().contains("&&")) {
							query = query + queries.elementAt(i).toString().replace("&&", "=");
						}else {
							query = query + queries.elementAt(i).toString();
						}
				}
			}
			
		}else if (va.startsWith("[")) {			
			Nset ns =Nset.readJSON(va);
			queries = Utility.splitVector(ns.getData(0).toString(), "?");
			for (int i = 0; i < ns.getArraySize(); i++) {
				if (i == 0) {
					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
						if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
							query = queries.elementAt(i).toString() + "'%" + param.trim() + "%'";	
						}else{
							query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "%'";
						}
							
					}else if(queries.elementAt(i).toString().contains("=")){
						if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
							query = queries.elementAt(i).toString() + "'" + param.trim() + "'";
						}else{
							query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "'";
						}
						
					}else if(queries.elementAt(i).toString().contains("NOT IN")){
						Nset nComp = Nset.readJSON(ns.getData(i+1).toJSON());
						query = query + queries.elementAt(i).toString();
						for (int j = 0; j < nComp.getArraySize(); j++) {
							String strCom = getDataComp(Utility.comp, nComp.getData(j).toString());
							if (j == 0) {
								if (!strCom.equalsIgnoreCase("")) {
									query =  query + "('%" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
								}else{
									query =  query + "('" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
								}
								
							}else if (j == (nComp.getArraySize()-1)) {
								if (!strCom.equalsIgnoreCase("")) {
									query = query + "'%" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%')";
								}else{
									query = query + "'" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "')";
								}
								
							}else {
								if (!strCom.equalsIgnoreCase("")) {
									query = query + "'%"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
								}else{
									query = query + "'"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
								}
								
							}
						}
					}else if (queries.elementAt(i).toString().contains("&&")) {
						query = query + queries.elementAt(i).toString().replace("&&", "=");
					}else {
						query = queries.elementAt(i).toString();
					}
				}else	
					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
						if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
							query = query + queries.elementAt(i).toString() + "'%" + param.trim() + "%'";	
						}else{
							query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "%'";
						}
								
					}else if(queries.elementAt(i).toString().contains("=")){
						if (ns.getData(i+1).toString().equalsIgnoreCase("F")) {
							query = query + queries.elementAt(i).toString() + "'" + param.trim() + "'";
						}else{
							query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, ns.getData(i+1).toString())).trim() + "'";
						}
							
					}else if(queries.elementAt(i).toString().contains("NOT IN")){
						Nset nComp = Nset.readJSON(ns.getData(i+1).toString());
						query = query + queries.elementAt(i).toString();
						for (int j = 0; j < nComp.getArraySize(); j++) {
							String strCom = getDataComp(Utility.comp, nComp.getData(j).toString());
							if (j == 0) {
								if (!strCom.equalsIgnoreCase("")) {
									query =  query + "('%" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
								}else{
									query =  query + "('" + StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
								}
								
							}else if (j == (nComp.getArraySize()-1)) {
								if (!strCom.equalsIgnoreCase("")) {
									query = query + "'%" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%')";
								}else{
									query = query + "'" +StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "')";
								}
								
							}else {
								if (!strCom.equalsIgnoreCase("")) {
									query = query + "'%"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "%',";
								}else{
									query = query + "'"+StringEscapeUtils.escapeSql(getDataComp(Utility.comp, nComp.getData(j).toString())).trim() + "',";
								}
								
							}
						}
					}else if (queries.elementAt(i).toString().contains("&&")) {
						query = query + queries.elementAt(i).toString().replace("&&", "=");
					}else {
						query = query + queries.elementAt(i).toString();
					}
			}
		}else {
			query = va;
		}
		
		return query;
	}
	

	/**
	 * untuk menampung semua data hasil query kedalam parameter
	 */
	private void loadData(final String query){
		Messagebox.showProsesBar(FinderItemActivity.this, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Recordset data = Connection.DBquery(query);
				if (data.getRows() > 0) {					
					values = data.getAllDataVector();
					header = data.getAllHeaderVector();
				}
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setupList(values);
			}
		});
	}
	
	/**
	 * untuk menampikan data kedalam list
	 */
	private void setupList(Vector<Vector<String>> data){
		AdapterConnector.setConnector((ListView)findViewById(R.id.lstFinder), data, R.layout.orderadapter, new AdapterInterface() {
			
			@SuppressWarnings("unchecked")
			@Override
			public View onMappingColumn(int row, View v, Object data) {
				// TODO Auto-generated method stub
				Vector<String> val = (Vector<String>) data;
				
				v.findViewById(R.id.img_icon).setVisibility(View.GONE);
				LinearLayout container = (LinearLayout) v.findViewById(R.id.container_order_view);
				String[] values = new String[val.size()];
				for (int i = 0; i < val.size(); i++) {
					
					String label = header.get(i);
					String value = val.get(i);
					
					values[i] = value;
					
					if (label.contains("_")) {
						label = label.replace("_", " ");
					}
					
					TextView textView = new TextView(FinderItemActivity.this);
					textView.setText(label + " : " + value);
					container.addView(textView);
				}
				v.setTag(values);
				return v;
			}
		});
	}
	
	/**
	 * methode yang dijalankan ketika data dipilih
	 * men-set beberapa nilai yang dibutuhkan generator
	 */
	private void onclick(int pos, String[] data){
		Intent i = new Intent();		
		i.putExtra("ROW", pos);
		i.putExtra("FIELDS", data);
		i.putExtra("madatory", getIntent().getStringExtra("PARAM1"));
		if (getIntent() != null && getIntent().getStringExtra("PARAM4") != null) {
			i.putExtra("PARAM4", getIntent().getStringExtra("PARAM4") );
		}
		setResult(RESULT_FINDER , i);
		finish();
	}
	
	/**
	 * untuk mendapatkan nilai isi dari komponen
	 */
	private String getDataComp(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
	
	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent i = new Intent();		
		i.putExtra("ROW", pos);
		i.putExtra("FIELDS", new String[]{"",""});
		if (getIntent() != null && getIntent().getStringExtra("PARAM4") != null) {
			i.putExtra("PARAM4", getIntent().getStringExtra("PARAM4") );
		}
		setResult(RESULT_FINDER , i);
		finish();
	}*/

}
