package com.daf.activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salesforce.R;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.model.ModelLoginUser;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class ChangePasswordActivity extends Activity implements OnClickListener {
	
	private Button btnChangePass;
	private EditText fieldCurrPass;
	private EditText fieldNewPass;
	private EditText fieldConfirmPass;
	private String flagForm;
	private AlertDialog alertDialog;
	private boolean isFinish = false;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		//check if app context null and app will close by system
		Utility.checkingAppContex(ChangePasswordActivity.this);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changepassword2);
		
		((ImageView) findViewById(R.id.imageView1)).setImageResource(android.R.drawable.ic_menu_revert);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView1)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		((TextView)findViewById(R.id.textJudul)).setText("Change Password");
		
		btnChangePass = (Button)findViewById(R.id.btn_change_pass);
		btnChangePass.setOnClickListener(this);
		
		((TextView)findViewById(R.id.lbl_curr_pass)).setTypeface(Utility.getFonttype(getApplicationContext()));
		((TextView)findViewById(R.id.lbl_confirm_pass)).setTypeface(Utility.getFonttype(getApplicationContext()));
		((TextView)findViewById(R.id.lbl_new_pass)).setTypeface(Utility.getFonttype(getApplicationContext()));
		
		fieldCurrPass = (EditText)findViewById(R.id.edt_curr_pass);
		fieldNewPass = (EditText)findViewById(R.id.edt_new_pass);
		fieldConfirmPass = (EditText)findViewById(R.id.edt_confirm_pass);
		
		flagForm = getIntent().getStringExtra("flag_from");
		
		if(flagForm == null){
			flagForm = "";
		}
		
		if(!flagForm.equals("Menu Utama")){
			((LinearLayout)findViewById(R.id.container_curr_pass)).setVisibility(View.GONE);
			try {
				((EditText) findViewById(R.id.edt_curr_pass)).setText( getIntent().getStringExtra("PASSWORD"));
			} catch (Exception e) { }
		}
		
		// create alert dialog
		alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this).create();
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				alertDialog.dismiss();
				if(isFinish){
					finish();
				}

			}
		});
	}

	private Recordset sResult;
	@Override
	public void onClick(View arg0) {
		
		if(isValidate()){
			/*
			if(savingChangePass()){
				isFinish = true;
				alertDialog.setMessage("Saving To Database Success");
				alertDialog.show();
				
			} 
			*/
			Messagebox.showProsesBar(ChangePasswordActivity.this, new Runnable() {
				@Override
				public void run() {
					String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "changepasswordservlet/?userid=" , Utility.getSetting(ChangePasswordActivity.this.getApplicationContext(), "USERNAME", "") ,"&passwordlama=" , ((EditText) findViewById(R.id.edt_curr_pass)).getText().toString().trim(),"&passwordbaru=" ,((EditText) findViewById(R.id.edt_new_pass)).getText().toString().trim() ,"&flg=", Utility.getSetting(ChangePasswordActivity.this.getApplicationContext(), "PASSWORD", "")  );
					//if (SettingActivity.URL_SERVER.startsWith("http")) {
						sResult = Syncronizer.getHttpStream(sUrl);
					//}
				}
			}, new Runnable() {
				@Override
				public void run() {
					if (sResult.getRows() >= 1) {
						if (sResult.getText(0, "result").equals("OK")) {
							isFinish=true;
							Utility.setSetting(getApplicationContext(), "PASSWORD","");
							showMessage("Change Password Succes");
						}else{
							if (sResult.getRows() >= 1) {
								if (sResult.getText(0, "message").length()>=3) {
									showMessage(sResult.getText(0, "message"));
								}
							}else{
								Toast.makeText(ChangePasswordActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
								showMessage("Connection Timeout");
							}				
						}
					}else{
						Toast.makeText(ChangePasswordActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
						showMessage("Connection Timeout");
					}
				}
			});
			
		}
		
	}
	public void showMessage(String message) {
		alertDialog.setMessage(message);
		alertDialog.show();
	}
	
	/**
	 * check matching password
	 * @param newPass
	 * @param confirmPass
	 * @return
	 */
	private boolean isMatch(String valNewPass, String valConfirmPass){
		
		if(valNewPass.equals(valConfirmPass)){
			return true;
		} else {
			return false;
		}	
	}
	
	/**
	 * validate all field before saving to server
	 * @return
	 */
	private boolean isValidate(){
		String valCurrPass = fieldCurrPass.getText().toString().trim();
		String valNewPass = fieldNewPass.getText().toString().trim();
		String valConfirmPass = fieldConfirmPass.getText().toString();
		
		if(flagForm.equals("Menu Utama")){
			
			if(valCurrPass.equals("")){
				alertDialog.setMessage("Current Password Cannot Blank");
				alertDialog.show();
				return false;
				
			} else {
				/*
				if(!checkingOldPassword(valCurrPass)){
					alertDialog.setMessage("Your Current Password Is Wrong");
					alertDialog.show();
					return false;
				}
				*/				
			}
			
		}
		
		
		
		if(valNewPass.equals("")){
			alertDialog.setMessage("New Password Cannot Blank");
			alertDialog.show();
			return false;	
		}
		
		if(valConfirmPass.equals("")){
			alertDialog.setMessage("Confirm Password Cannot Blank");
			alertDialog.show();
			return false;	
		}
		
		if(!isMatch(valNewPass, valConfirmPass)){
			alertDialog.setMessage("New Password And Confirm Password Not Matching");
			alertDialog.show();
			return false;
		}
		
		return true;
	}
	
	
	
	/**
	 * checking old password
	 * @return
	 */
	private boolean checkingOldPassword(String currPass) {
		
		String userId = Utility.getSetting(ChangePasswordActivity.this, "USERNAME", "");
		String query = "Select * from dummy_login where user_id='"+userId+"' and password='"+currPass+"'";
		Cursor cursor = Connection.DBexecute(query);
		
		if(cursor.getCount() > 0) {
			cursor.close();
			
			return true;
			
		}
		
		cursor.close();
		return false;
	}
	
	/**
	 * saving records to database server
	 */
	
	private boolean savingChangePass(){
		

		String valNewPass  = fieldNewPass.getText().toString().trim(); 
		String valCurrPassDB = Utility.getSetting(ChangePasswordActivity.this, "PASSWORD", "");
		boolean isSameCurrAndNewPass = checkingOldAndNewPass(valCurrPassDB, valNewPass);
		
		// check if curr and new pass is same
		if(isSameCurrAndNewPass){
//			Toast.makeText(ChangePasswordActivity.this, "New Password Cannot be Same with Current Password", Toast.LENGTH_SHORT).show();
			alertDialog.setMessage("New Password Cannot be Same with Current Password");
			alertDialog.show();
			return false;
		}
		
		
		ModelLoginUser model = new ModelLoginUser();
		model.setUserId(Utility.getSetting(ChangePasswordActivity.this, "USERNAME", ""));
		model.setPassUser(valNewPass);
		
		
		ContentValues column = new ContentValues();
		column.put("password", model.getPassUser());
		try {
			
			// TODO add query to update user login into database server
			Connection.DBupdate("dummy_login", column, "user_Id=?", new String[]{model.getUserId()});	
		} catch (Exception e) {
//			Toast.makeText(ChangePasswordActivity.this, "Cannot Saving to Database", Toast.LENGTH_SHORT).show();
			alertDialog.setMessage( "Cannot Saving to Database");
			alertDialog.show();
			return false;
		}
		
		return true;
	}
	
	/**
	 * check old and new pass
	 * @param currPassDb
	 * @param newPass
	 * @return
	 */
	private boolean checkingOldAndNewPass(String currPassDb, String newPass){
		
		if(currPassDb.equals(newPass)){
			return true;
		}
		
		return false;
	}
}
