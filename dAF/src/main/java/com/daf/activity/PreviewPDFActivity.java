package com.daf.activity;

 
import java.io.File;

import android.app.Activity;
import android.os.Bundle;

import com.joanzapata.pdfview.PDFView;
import com.salesforce.R;

public class PreviewPDFActivity extends Activity{
	
	/**
	 * untuk file pdf
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pdfpreview);
		setTitle("");
		
//		if (getIntent()!=null && getIntent().getStringExtra("filepdf")!=null) {
		if (getIntent()!=null) {
			String filename = getIntent().getStringExtra("filepdf");
			try {
				if(filename!=""){
					PDFView pdfView = (PDFView) findViewById(R.id.pdfView);
					pdfView.fromFile(new File(filename)).defaultPage(1).load();
				}
			} catch (Exception e) { 
				e.printStackTrace();
			}
		}
		
	}  

}
