package com.daf.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.Logout;
import com.salesforce.utility.Utility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class StatusOrderActivity extends Activity{
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.menuapplication2);
		
//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}
		
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		((LinearLayout)findViewById(R.id.mngText)).setVisibility(View.GONE);
		((TextView) findViewById(R.id.textJudul)).setText("Status Order");
		
//		deleteDraft();
		loadData();
		
		((ListView)findViewById(R.id.lstApp)).setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {
				if (pos == 0) {
					startActivityForResult(new Intent(StatusOrderActivity.this, OrderDraft.class), 0);
				}else{
					startActivityForResult(new Intent(StatusOrderActivity.this, OrderPendingActivity.class), 0);
				}
			}
		});
	}
	
	/**
	 * untuk mngumpulkan data Draft dan Pending
	 */
	private void loadData(){
		
		List<String[]> lstData = new ArrayList<String[]>();
		Recordset data = Connection.DBquery("SELECT * FROM DRAFT WHERE user ='"+Utility.getSetting(getApplicationContext(), "USERNAME", "")+"'");
		String[] str = {"Order Draft",data.getRows()+""};		
		lstData.add(str);
		
		data = Connection.DBquery("select orderCode, ndate, time, activityId, user, bussUnit from data_activity where appId = '" + Generator.currApplication + "'and status = '0'");
		String[] str2 = {"Order Pending",data.getRows()+""};
		lstData.add(str2);
		
		setuplist(lstData);
	}
	
	/**
	 * untuk menampikan data kedalam list
	 */
	private void setuplist(List<String[]> data){
		AdapterConnector.setConnector((ListView)findViewById(R.id.lstApp), data, R.layout.inflatstatusorder, new AdapterInterface() {
			
			@Override
			public View onMappingColumn(int row, View v, Object data) {
				// TODO Auto-generated method stub
				String[] menu = (String[]) data;
				((TextView)v.findViewById(R.id.title)).setText(menu[0]);
				((TextView)v.findViewById(R.id.txtCounter)).setText(menu[1]);
				
				return v;
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == 0) {
			((ListView)findViewById(R.id.lstApp)).setAdapter(null);
			loadData();
		}
	}

}
