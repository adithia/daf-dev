package com.daf.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.database.Connection;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.logging.ErrorManager;

import repack.org.bouncycastle.crypto.tls.ContentType;

public class OcrKtpActivity extends Activity {

    private Button addFoto;
    private ImageView picturesss;
    private  String urlpic = "" ;
    private Button cekurls, btnNext;
    private File file;
    private ErrorManager logger;
    private TextView txtLair, txtNoKTP, txtNama, txtGender, txtReligion, txtStatus, txtRtrw, txtVillage, txtDistrict, txtCity, txtProv, txtNationality;
    private String code;
    private UUID idOne;
    private boolean fromDafPameran = false;
    private String table_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr_ktp);

        addFoto = (Button) findViewById(R.id.addFoto);
        cekurls = (Button) findViewById(R.id.cekurl);
        picturesss = (ImageView) findViewById(R.id.picturesss);
        txtLair = (TextView) findViewById(R.id.txtLair);
        txtNama = (TextView) findViewById(R.id.txtNama);
        txtNoKTP = (TextView) findViewById(R.id.txtNoKTP);
        txtGender = (TextView) findViewById(R.id.txtGender);
        txtReligion = (TextView) findViewById(R.id.txtReligion);
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        txtRtrw = (TextView) findViewById(R.id.txtRtrw);
        txtVillage = (TextView) findViewById(R.id.txtVillage);
        txtDistrict = (TextView) findViewById(R.id.txtDistrict);
        txtCity = (TextView) findViewById(R.id.txtCity);
        txtProv = (TextView) findViewById(R.id.txtProv);
        txtNationality = (TextView) findViewById(R.id.txtNationality);
        btnNext = findViewById(R.id.btnNext);

        addFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(OcrKtpActivity.this);
            }

        });

        picturesss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(OcrKtpActivity.this);
            }

        });

        cekurls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    analysFile();
                    Map<String, String> params = new HashMap<String, String>(2);

                    String result = multipartRequest("https://api.advance.ai/openapi/face-recognition/v1/ocr-ktp-check", params, urlpic, "ocrImage", "image/jpg");

                    JSONObject json = new JSONObject(result);

                    code = json.getString("code");
                    Log.w("error",code);
                    if(code.equals("SUCCESS")){
                        txtNoKTP.setText(json.getJSONObject("data").getString("idNumber"));
                        txtNama.setText(json.getJSONObject("data").getString("name"));
                        txtLair.setText(json.getJSONObject("data").getString("birthPlaceBirthday"));
                        txtGender.setText(json.getJSONObject("data").getString("gender"));
                        txtReligion.setText(json.getJSONObject("data").getString("religion"));
                        txtStatus.setText(json.getJSONObject("data").getString("maritalStatus"));
                        txtRtrw.setText(json.getJSONObject("data").getString("rtrw"));
                        txtVillage.setText(json.getJSONObject("data").getString("village"));
                        txtDistrict.setText(json.getJSONObject("data").getString("district"));
                        txtCity.setText(json.getJSONObject("data").getString("city"));
                        txtProv.setText(json.getJSONObject("data").getString("province"));
                        txtNationality.setText(json.getJSONObject("data").getString("nationality"));
                    }

                } catch (Exception e) {
                    Log.w("Error api",e.toString());
                    e.printStackTrace();
                }
            }
        });

    }


    private void selectImage(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        picturesss.setImageBitmap(selectedImage);
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                picturesss.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                picturesss.setImageBitmap(getPicture(data.getData()));
                                cursor.close();
                            }
                        }

                    }
                    break;
            }
        }
    }

    public Bitmap getPicture(Uri selectedImage) {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        Log.w("FIFPOK",picturePath.toString());
        urlpic = picturePath.toString();
        file = new File(picturePath);
        cursor.close();
        return BitmapFactory.decodeFile(picturePath);
    }


    public String multipartRequest(String urlTo, Map<String, String> parmas, String filepath, String filefield, String fileMimeType) throws Exception {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;

        String twoHyphens = "--";
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String lineEnd = "\r\n";

        String result = "";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        String[] q = filepath.split("/");
        int idx = q.length - 1;

        try {
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL(urlTo);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            connection.setRequestProperty("X-ADVAI-KEY","704aaee47e9cb031");

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);

            // Upload POST Data
            Iterator<String> keys = parmas.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = parmas.get(key);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


            if (200 != connection.getResponseCode()) {
                //throw new CustomException("Failed to upload code:" + connection.getResponseCode() + " " + connection.getResponseMessage());
            }

            inputStream = connection.getInputStream();

            result = this.convertStreamToString(inputStream);

            fileInputStream.close();
            inputStream.close();
            outputStream.flush();
            outputStream.close();

            return result.toString();
        } catch (Exception e) {
            //logger.error(e.toString());
            //throw new CustomException(e);
            return  e.toString();
        }

    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public void analysFile() throws Exception{

        if (Build.VERSION.SDK_INT>= 10) {
            StrictMode.ThreadPolicy tp = StrictMode.ThreadPolicy.LAX;
            StrictMode.setThreadPolicy(tp);
        }
        HttpClient httpclient = new DefaultHttpClient();

        HttpPost httppost = new HttpPost("https://api.advance.ai/openapi/face-recognition/v1/ocr-ktp-check");
        httppost.setHeader("X-ADVAI-KEY","704aaee47e9cb031");
        httppost.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:22.0) Gecko/20100101 Firefox/22.0");
        httppost.setHeader("Content-Type", "multipart/form-data");
        httppost.setHeader("Accept", "application/json");
        //httppost.setHeader("Content-Type", "multipart/form-data");


        MultipartEntity reqEntity = new MultipartEntity();

        httppost.setEntity(reqEntity);

        HttpResponse response = httpclient.execute(httppost);
        String jsonString = EntityUtils.toString(response.getEntity());
        try {
            Log.w("kopi","success");
            Log.w("kopi",jsonString);

        }catch (Exception e){
            Log.w("kopi","error");

        }

    }

}
