package com.daf.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.salesforce.R;

@SuppressWarnings("deprecation")
public class StatusOrder extends TabActivity{
	
	private TabHost tabHost;
	private TabSpec tab1;
	private TabSpec tab2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.status_order);
		
		((TextView)findViewById(R.id.textJudul)).setText("Status Order");
		
		((ImageView)findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tab1 = tabHost.newTabSpec("Order Draft");
		tab2 = tabHost.newTabSpec("Order Pending");
		
		tab1.setIndicator("Order Draft");
		Intent intent = new Intent(StatusOrder.this, OrderDraft.class);
		tab1.setContent(intent);

		tab2.setIndicator("Order Pending");
		intent = new Intent(StatusOrder.this, OrderPendingActivity.class);
		tab2.setContent(intent);

		tabHost.addTab(tab1);
		tabHost.addTab(tab2);
		
//		tabHost.setBackgroundColor(Color.GRAY);
		for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
//			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.GRAY);
			
            TextView tv = (TextView)tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
//            tv.setTextColor(Color.WHITE);
        }
		
		if (getIntent().getStringExtra("To") != null) {
			if (getIntent().getStringExtra("To").equalsIgnoreCase("draft")) {
				tabHost.setCurrentTab(0);
			}else
				tabHost.setCurrentTab(1);
		}
		
	}

}
