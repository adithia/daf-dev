package com.daf.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.SendAction;
import com.salesforce.stream.ImagePost;
import com.salesforce.stream.Stream;
import com.salesforce.utility.GPSTracker;
import com.salesforce.utility.LocationOffline;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.UUID;
import java.util.Vector;

/**
 * @author lenovo
 * <p>
 * Class login digunakan sebagai interface user, dimana dalam kelas ini
 * user yang akan masuk kedalam aplikasi harus bisa melewati validasi yang sudah ditentukan
 * dalam flow bisnis
 */
public class LoginActivityTest extends Activity {
    final static int GPS = 1, Bluetooth = 2, Wifi = 3, AppVersion = 4, certificate = 5, token = 6;
    public static String strPathImg = "", message, jsonToken;
    private static boolean MD5HASH = false;
    /**
     * untuk mengupload tanda tangan keserver ketika pertama kali
     */
    String result = "";
    private Recordset data;
    private boolean logintrue = false, hasSigning = false, passSign = false, isCertExist, changeBranch;
    private EditText edtUsername, edtPassword;
    private String strImei;
    private String strFileName, IP_Address;
    private Hashtable<String, Recordset> tblOld = new Hashtable<String, Recordset>();
    private Vector<String> tbleUpdated = new Vector<String>();
    /**
     * untuk menampilkan dialog progress
     */
    private String actionlogin = "";

    public static void downloadOrder(Context context) {
        downloadOrder(context, null);
    }

    public static void downloadOrder(Context context, LoginActivityTest showprogres) {
        Nset order = Nset.newObject();
        StringBuffer sbuBuffer = new StringBuffer();
        String tableName = "";

        Recordset rst = Connection.DBquery("select distinct model_order_tablename from model;");
        for (int i = 0; i < rst.getRows(); i++) {
            tableName = rst.getText(i, 0);
            Recordset x = Connection.DBquery("select order_id from " + tableName);

            for (int j = 0; j < x.getRows(); j++) {
                sbuBuffer.append(j > 0 ? "," : "").append(x.getText(j, 0));
            }
            order.setData(rst.getText(i, 0), sbuBuffer.toString());

            if (!Utility.getSetting(context, "USERNAME", "").equals(Utility.getSetting(context, "LASTUSER", ""))) {
                Connection.DBdelete(rst.getText(i, 0));
                Connection.DBdelete("log_order");
                Connection.DBcreate("log_order", "order_id", "opened", "sent", "downloaded", "data");
                SendAction.deleteSentActity(0);

            }
        }

        SendAction.deleteSentActity(0, Utility.Now().substring(0, 10));
        String sUrl; //= Utility.getURLenc(SettingActivity.URL_SERVER + "orderservlet/?action=nikita&order=" ,order.toJSON(),"&userid=",Utility.getSetting(context, "userid", ""),"&flg=",(showprogres!=null?"":"service"));

        //kode lama
//		sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/"+ (showprogres!=null?"":"?f=service"), new String[] { "action", "order", "userid"  ,"flg"},
//				new String[] { "nikita", order.toJSON(), Utility.getSetting(context, "userid", "") , (showprogres!=null?"":"service") });

        if (Utility.getNewToken()) {
            sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/" + (showprogres != null ? "" : "?f=service"), new String[]{"action", "order", "userid", "flg", "token"},
                    new String[]{"nikita", order.toJSON(), Utility.getSetting(context, "userid", ""), (showprogres != null ? "" : "service"), Utility.getSetting(context, "Token", "")});


            if (showprogres == null && sUrl.startsWith("LOGOUT")) {
                Utility.setSetting(context, "ERRORMSG", "KILL");
            }

            if (showprogres == null && sUrl.endsWith("SERVICEOFF")) {
                Utility.setSetting(context, "SERVICE", "");
            }
            order = Nset.readJSON(sUrl);
            if (showprogres != null) {
                showprogres.setProsesBarMsg("Please Wait \r\nSaving Order (100%)");
            }

            boolean breceived = false, isInsert = false;
            for (int i = 0; i < order.getArraySize(); i++) {
                String table = order.getData(i).getData("table").toString();
                for (int j = 0; j < order.getData(i).getData("data").getArraySize(); j++) {
                    String code = order.getData(i).getData("data").getData(j).getData("code").toString();
                    String drst = order.getData(i).getData("data").getData(j).getData("data").toString();
                    Recordset xxx = Stream.downStream(drst);
                    if (MD5HASH) {
                        String md5 = Utility.MD5(xxx.getAllDataVector().toString());
                        if (!md5.equals(Utility.getSetting(context, table, ""))) {
                            if (code.equals("truncate")) {
                                Syncronizer.saveToDB(table, xxx);
                            } else if (code.equals("insert")) {
                                Syncronizer.createToDB(table, xxx);
                                Syncronizer.insertToDB(table, xxx);
                            } else if (code.equals("delete")) {
                                Syncronizer.createToDB(table, xxx);
                                Syncronizer.deleteToDB(table, xxx);
                            } else if (code.equals("update")) {
                                Syncronizer.createToDB(table, xxx);
                                Syncronizer.updateToDB(table, xxx);
                            }
                        }
                        Utility.setSetting(context, table, md5);
                    } else {
                        if (code.equals("truncate")) {
                            Syncronizer.saveToDB(table, xxx);
                        } else if (code.equals("insert")) {
                            if (xxx.getRows() > 0) {
                                isInsert = true;
                            }
//							Syncronizer.deleteToDB(table, xxx);
                            Syncronizer.createToDB(table, xxx);
                            Syncronizer.insertToDB(table, xxx);
                        } else if (code.equals("delete")) {
                            Syncronizer.createToDB(table, xxx);
                            Syncronizer.deleteToDB(table, xxx);
                        } else if (code.equals("update")) {
                            Syncronizer.createToDB(table, xxx);
                            Syncronizer.deleteToDB(table, xxx);
                            Syncronizer.insertToDB(table, xxx);
                        }
                    }

                    deleteExpiredTasklit(table);
                    if (xxx.getRows() >= 1) {
                        breceived = true;
                    }
                }

            }

            Recordset tabel = null;
            StringBuffer orderNew = new StringBuffer();
            for (int i = 0; i < rst.getRows(); i++) {
                tabel = Connection.DBquery("select order_id from " + rst.getText(i, 0));
                for (int j = 0; j < tabel.getRows(); j++) {
                    orderNew.append(j > 0 ? "," : "").append(tabel.getText(j, 0));
                }
            }

//			if (isInsert || QueryClass.isHaveTable(tableName)) {
            insertRecievedDate(orderNew.toString(), sbuBuffer.toString());
//			}

            if (breceived) {
                Nset order_id = Nset.newObject();
                order_id.setData("trn_task_list", sbuBuffer.toString());
                if (Utility.getNewToken()) {
                    sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[]{"action", "order", "userid", "flg", "token"},
                            new String[]{"RECEIVED", order_id.toJSON(), Utility.getSetting(context, "userid", ""), (showprogres != null ? "" : "service"), Utility.getSetting(Utility.getAppContext(), "Token", "")});
                } else {
                    sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[]{"action", "order", "userid", "flg", "token"},
                            new String[]{"RECEIVED", order_id.toJSON(), Utility.getSetting(context, "userid", ""), "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
                }

            }
        }


    }

    /**
     * @param orderNew order yang baru didapat dari service
     * @param orderOld order yang ada di device mobile
     *                 <p>
     *                 digunakan untuk mengisi field recieved_date pada tabel trn_task_list
     */
    private static void insertRecievedDate(String orderNew, String orderOld) {
        Vector<String> vOrderNew = Utility.splitVector(orderNew, ",");
        Vector<String> vOrderOld = Utility.splitVector(orderOld, ",");
        Connection.DBopen();
        if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
            for (int i = 0; i < vOrderNew.size(); i++) {
                if (!Utility.isContainValue(vOrderNew.elementAt(i), vOrderOld)) {
                    Connection.DBupdate("trn_task_list", "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(i));
                }
            }
        } else {
            for (int i = 0; i < vOrderNew.size(); i++) {
                Connection.DBupdate("trn_task_list", "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(i));
            }
        }
    }

    /**
     * untuk mendelete tasklist yg expired
     *
     * @param table param nama tabel
     */
    private static void deleteExpiredTasklit(String table) {
        Recordset data = Connection.DBquery("SELECT order_id, expired_date FROM " + table);
        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {
                if (!data.getText(i, "expired_date").equalsIgnoreCase("")) {
                    Calendar cal = convertTasklits(data.getText(i, "expired_date"));
                    Date today = new Date();
                    if (today.after(cal.getTime())) {
                        Connection.DBdelete(table, "order_id=?", new String[]{data.getText(i, "order_id")});
                    }
                }
            }
        }
    }

    /**
     * @param strDate tanggal yang dimasukkan
     * @return Calendar
     * <p>
     * untuk mengconvert tanggal dari string ke Calendar
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("SimpleDateFormat")
    public static Calendar convertTasklits(String strDate) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH);
            date = sdf.parse(strDate);
            calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login2);

        new LocationOffline(getApplicationContext());

        // digunakan untuk mendapatkan ip address mobile
        if (Utility.cellularNetworkDataActive(getApplicationContext())) {
            IP_Address = Utility.getLocalIpAddress();
        } else {
            IP_Address = Utility.getIpAddressWifi(getApplicationContext());
        }

//		for (int i = 0; i < 3; i++) {
//			Utility.writeToFile("/storage/sdcard0/DAF/LOG/coba.txt", "SENDDATE="+Utility.Now()+ "\n\n\n"+"ini datanya ya" +"\n\n\nbalikan service = balikan ok");
//		}


        // meminta balikan apakah FIF Certifacate sudah terinstall dalam device ?
//		isCertExist = Utility.certificateFIFExist();
//		String path = "";
//		File oldfile =new File(Utility.getDefaultPath("test"));
//		if (oldfile.exists()) {
//			path = oldfile.getName();
//		}
//
//		File ds = new File(path);
//		String dffd = ds.getName();

        edtUsername = (EditText) findViewById(R.id.editUsername);
        edtPassword = (EditText) findViewById(R.id.editPassword);
        strImei = Utility.getImei(getApplicationContext()).toUpperCase();

        edtUsername.addTextChangedListener(fieldListener(true));
        edtPassword.addTextChangedListener(fieldListener(false));

        if (Utility.isExit) {
            finish();
            Utility.setExit(false);
            return;
        }

        String url = "https://moda.danamon.co.id:444/ServiceMobile/";

        String strResult = Utility.getURLenc(url + "LoginDanamonServlet2?userId=",
                ((EditText) findViewById(R.id.editUsername)).getText().toString().trim(), "&password=",
                ((EditText) findViewById(R.id.editPassword)).getText().toString().trim(), "&imeiNo=",
                Utility.getImei(getApplicationContext()), "&gps=", "1", "&bluetooth=", "0", "&wifi=", "1", "&versi=1",
                "&activityPending=", Connection.DBquery("SELECT id FROM data_activity WHERE status ='0';").getRows() + "",
                "&lastuser=", Utility.getSetting(getApplicationContext(), "LASTUSER", ""), "&dateNow=", Utility.getLoginDate(), "&timeNow=",
                Utility.getLoginTime());

//		Recordset dss = Connection.DBquery("SELECT name FROM sqlite_master WHERE type='table'");

//		Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
//
//		if (c.moveToFirst()) {
//		    while ( !c.isAfterLast() ) {
//		        Toast.makeText(activityName.this, "Table Name=> "+c.getString(0), Toast.LENGTH_LONG).show();
//		        c.moveToNext();
//		    }
//		}

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            SettingActivity.APP_VERSION = pInfo.versionName;
            Utility.setSetting(getApplicationContext(), "app_version", SettingActivity.APP_VERSION);
        } catch (Exception e) {
        }

        SettingActivity.URL_SERVER = SettingActivity.getProductionUrl();// Connection.DBquery("select * from SETTING ").getText(0, "URL");
        SettingActivity.URL_STORAGE = Connection.DBquery("select * from SETTING ").getText(0, "STORAGE");

        if (SettingActivity.URL_SERVER.trim().equals("")) {
            startActivity(new Intent(LoginActivityTest.this, SettingActivity.class));
        }

        //set enable for setting
        if (Global.getText("login.setting.active", "1").equals("1")) {
            ((ImageView) findViewById(R.id.btnSetting)).setEnabled(true);
        } else {
            ((ImageView) findViewById(R.id.btnSetting)).setEnabled(false);
        }

        //setting value text for the last login
        if (Utility.getSetting(getApplicationContext(), "REMEMBER", "").equals("TRUE")) {
            ((CheckBox) findViewById(R.id.checkRemember)).setChecked(true);
            edtUsername.setText(Utility.getSetting(getApplicationContext(), "USERNAME", ""));
            edtPassword.setText("");//Utility.getSetting(getApplicationContext(), "PASSWORD", "")
        } else if (Utility.getSetting(getApplicationContext(), "REMEMBER", "").equals("FALSE")) {
            ((CheckBox) findViewById(R.id.checkRemember)).setChecked(false);
            edtUsername.setText("");
            edtPassword.setText("");
        }


        ((ImageView) findViewById(R.id.btnLogin)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
//				if (isCertExist) {
                if (SettingActivity.URL_SERVER != null || !SettingActivity.URL_SERVER.equalsIgnoreCase("")) {
                    if (!Utility.isTimeToOff()) {
                        if (Utility.isTimeToLogin()) {
                            tbleUpdated.clear();
                            changeBranch = false;
                            validation();
//								goToMainmenu();
                        } else {
                            AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Login tidak sesuai jam server");
                        }
                    } else {
                        AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Maaf, service saat ini tidak dapat digunakan");
                    }
                } else {
                    AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Maaf, Anda belum mengisi alamat URL server");
                }
//				}else{
//					AdapterDialog.goingToWeb(LoginActivityTestNew.this, Uri.parse("http://google.com"), "Informasi", "Maaf, Anda belum install FIF-Certificate\n\n silahkan Install");
//				}
            }
        });


        ((ImageView) findViewById(R.id.btnSetting)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                startActivity(new Intent(LoginActivityTest.this, SettingActivity.class));
            }
        });

//		if (Utility.getSetting(getApplicationContext(), "v.13", "").equalsIgnoreCase("")) {
//			loadImages();
//		}

        //set scale for the checkbox
        final float scale = this.getResources().getDisplayMetrics().density;
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkRemember);
        checkBox.setPadding(checkBox.getPaddingLeft() + (int) (10.0f * scale + 0.5f),
                checkBox.getPaddingTop(),
                checkBox.getPaddingRight(),
                checkBox.getPaddingBottom());
    }

    //for get the max lenght of the input username and password
    private TextWatcher fieldListener(final boolean isUsername) {
        return new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                String message;
                if (isUsername) {
                    message = "Panjang Username maximal 50 karakter";
                } else {
                    message = "Panjang Password maximal 50 karakter";
                }
                if (s.length() >= 50) {
                    AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }

    /**
     * methode yang digunakan untuk validasi awal
     * yaitu username dan password tidak boleh kosong
     */

    private void validation() {
        if (Utility.isOnline(LoginActivityTest.this)) {
            if (edtUsername.getText().length() >= 50 || edtPassword.getText().length() >= 50) {
                AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Panjang Username/Password Maximal 50 karakter");
            } else {
                if (edtUsername.getText().toString().equalsIgnoreCase("")) {
                    AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Username tidak boleh kosong");
                } else if (edtPassword.getText().toString().equalsIgnoreCase("")) {
                    AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Password tidak boleh kosong");
                } else {
//					login();
                    getToken();
                }
            }
        } else {
            AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Maaf, Tidak ada koneksi");
        }
    }

    /**
     * methode ini digunakan untuk mendapatkan token dari service
     * sebagai parameter untuk setiap proses yang berhubungan dengan service
     */
    private void getToken() {
        Messagebox.showProsesBar(LoginActivityTest.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method ;
                String url = SettingActivity.URL_SERVER;
                String[] sf = Utility.split(url, ":");
                url = (String) sf[0] + ":" + (String) sf[1] + ":" + (String) sf[2].substring(0, sf[2].indexOf("/")) + "/";
                url = url + "DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";
                jsonToken = Utility.getHttpConnection2(url);

            }
        }, new Runnable() {

            @Override
            public void run() {
                Nset set = Nset.readJSON(jsonToken);
                String token = set.getData("access_token").toString();
                String refresh_token = set.getData("refresh_token").toString();
                if (!token.equalsIgnoreCase("")) {
                    Utility.setSetting(getApplicationContext(), "Token", token);
                    Utility.setSetting(getApplicationContext(), "Refresh_Token", refresh_token);
                    login();

                } else {
                    AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Maaf, Validasi token pertama gagal");
                }
            }
        });
    }

    /**
     * methode login ini digunakan untuk hit loginservlet dan checking download ttd
     */
    private void login() {
        if (((CheckBox) findViewById(R.id.checkRemember)).isChecked()) {
            Utility.setSetting(getApplicationContext(), "REMEMBER", "TRUE");
        } else {
            Utility.setSetting(getApplicationContext(), "REMEMBER", "FALSE");
        }

        strFileName = edtUsername.getText().toString() + "_" + Utility.getTodayDate().replace("-", "");

        Messagebox.showProsesBar(LoginActivityTest.this, new Runnable() {
            @Override
            public void run() {
                logintrue = false;
                GPSTracker gpsTrack = new GPSTracker(LoginActivityTest.this);
                String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "loginservlet/?userid=", edtUsername.getText().toString().trim(), "&password=", edtPassword.getText().toString().trim(),
                        "&imei=", strImei, "&gps=", gpsTrack.isGpsEnable() ? "1" : "0", "&bluetooth=", gpsTrack.isBluetoothOn() ? "1" : "0", "&datetime=", Utility.Now(),
                        "&wifi=", gpsTrack.isWifiOn() ? "1" : "0", "&versi=", SettingActivity.APP_VERSION, "&mobileprofile=", "",
                        "&activitypending=", Connection.DBquery("SELECT id FROMN data_activity WHERE status ='0';").getRows() + "",
                        "&lastuser=", Utility.getSetting(getApplicationContext(), "LASTUSER", ""), "&batch=",
                        Utility.getSetting(getApplicationContext(), "BATCH", ""), "&ip=", IP_Address, "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));
                try {

                    //update teknik login..

//					Recordset dsa = Syncronizer.getHttpStream(sUrl);
                    String json = Utility.getHttpConnection(Utility.getURLenc(sUrl));

                    Nset nn = Nset.readJSON(json);
                    if (nn.getData("message").getData("error").toString().equals("1")) {
                        message = nn.getData("message").getData("message").toString();
                    } else {

                        //membuat tabel mobileaccess
                        Recordset dsa = Syncronizer.getMenuHttpStream(json);
                        Connection.DBdrop("mobileaccess");
                        Syncronizer.createToDB("mobileaccess", dsa);
                        Syncronizer.insertToDB("mobileaccess", dsa);
                    }

                    data = Syncronizer.getStringHttpStream(json);


//					data = Syncronizer.getHttpStream(sUrl);
                    Utility.setSetting(getApplicationContext(), "office_type", data.getText(0, "office_type"));

                    if (data.getRows() >= 1) {
                        if (data.getText(0, "result").equals("OK")) {

//							String old = data.getText(0, "office_code");
//							String dsds = Utility.getSetting(getApplicationContext(), "Office_Code", "");

//							if (!Utility.getSetting(getApplicationContext(), "Office_Code", "").equalsIgnoreCase("")) {
//								if (!Utility.getSetting(getApplicationContext(), "Office_Code", "").equalsIgnoreCase(data.getText(0, "office_code"))) {
//									changeBranch = true;
//								}
//							}

//							String sds = data.getText(0, "jobcode");
                            Utility.OFFICE_CODE = data.getText(0, "office_code");

                            Utility.setSetting(getApplicationContext(), "Office_Code", Utility.OFFICE_CODE);
                            Utility.setSetting(getApplicationContext(), "USER_NAME", data.getText(0, "user_name"));
                            Utility.setSetting(getApplicationContext(), "userid", data.getText(0, "userid"));

                            logintrue = true;
                            Vector<String> flag = Utility.splitVector(data.getText(0, "flag"), ";");

                            if (flag.elementAt(0).toString().equals("1")) {
                                //belum tanda tangan
                                hasSigning = false;
                                Utility.flagLogin = 1;
                            } else if (flag.elementAt(0).toString().equals("2")) {
                                //sudah pernah tanda tangan
                                hasSigning = true;
                                strPathImg = flag.elementAt(1).toString();
                                Utility.flagLogin = 2;
                            } else {
                                //tidak perlu tanda tangan
                                passSign = true;
                                Utility.flagLogin = 0;
                            }

                        } else {
                            logintrue = false;
                        }
                    } else
                        message = "Login Gagal";

                } catch (Exception e) {
                    message = e.getMessage();
                    logintrue = false;
                }


            }
        }, new Runnable() {

            @Override
            public void run() {
                if (logintrue) {
                    CheckToolsDevice();
                } else {
                    if (data.getRows() >= 1) {
                        if (data.getText(0, "message").length() >= 3) {
                            actionlogin = data.getText(0, "action");
                            showMessage(data.getText(0, "message"), data.getText(0, "ok"), "Cancel", false, 0);
                        } else {
                            AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                        }
                    } else {
                        AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                    }
                }
            }
        });

    }

    /**
     * methode checktoolsdevice digunakan untuk membuat path yang diperlukan untuk
     * mobile
     */
    private void CheckToolsDevice() {
        if (new File(Utility.getDefaultTempPath()).exists() == false) {
            new File(Utility.getDefaultTempPath()).mkdirs();
        }

        if (new File(Utility.getDefaultSign()).exists() == false) {
            new File(Utility.getDefaultSign()).mkdirs();
        }

        if (new File(Utility.getDefaultImagePath()).exists() == false) {
            new File(Utility.getDefaultImagePath()).mkdirs();
        }

        if (new File(Utility.getDefaultPDF()).exists() == false) {
            new File(Utility.getDefaultPDF()).mkdirs();
        }

        if (new File(Utility.getDefaultBio()).exists() == false) {
            new File(Utility.getDefaultBio()).mkdirs();
        }

        if (new File("/storage/sdcard0/DAF/LOG").exists() == false) {
            new File("/storage/sdcard0/DAF/LOG").mkdir();
        }

        setUtilitas();
    }

    /**
     * setUtilitas digunakan untuk checking properti yang ada di global param
     * digunakan untuk menghidupkan perangkat media spt bluetooth, wifi dan sejenisnya
     * tergantung dari parameter global param
     */

    @SuppressWarnings("deprecation")
    private void setUtilitas() {
        GPSTracker gpsTrack = new GPSTracker(LoginActivityTest.this);
        if (!Global.getText("login.gps.active", "0").equals("0")) {
            if (!gpsTrack.canGetLocation()) {
                showDialog(GPS);
                return;
            }
        }
        if (!Global.getText("login.wifi.active", "0").equals("0")) {
            if (!gpsTrack.isWifiOn()) {
                showDialog(Wifi);
                return;
            }
        }
        if (!Global.getText("login.bluetooth.active", "0").equals("0")) {
            if (!gpsTrack.isBluetoothOn()) {
                showDialog(Bluetooth);
                return;
            }
        }

        if (!Global.getText("sentactivity.autodelete", "1").equals("1")) {
            Connection.DBdelete("data_activity", " status =? ", new String[]{"1"});
        }
        if (hasSigning) {
            Utility.ttdPath = strPathImg;
            //cek dulu apakah tanda tangannya sudah ada di local device
            if (!passSign) {
                if (!new File(Utility.getDefaultSign(strPathImg)).exists()) {
                    downloadImage(Utility.getSetting(getApplicationContext(), "userid", ""), strPathImg);
                } else {

                    if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
                        downloadFirsttime();
                    } else {
                        downloadMasterWithThread();
                    }

                }
            } else {
                goToMainmenu();
            }
        } else {
            Intent intent = new Intent(LoginActivityTest.this, DigitalSignatureActivity.class);
            intent.putExtra("image", "");
            intent.putExtra("firstSign", "true");
            intent.putExtra("fileName", strFileName);
            startActivityForResult(intent, 0);
        }
    }

    private void downloadFirsttime() {
        Messagebox.showProsesBar(LoginActivityTest.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Utility.setSetting(getApplicationContext(), "ERRORMSG", "");
                Utility.setSetting(getApplicationContext(), "REALNAME", data.getText(0, "realname"));
                setProsesBarMsg("Please Wait \r\nDownload Version");

                String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
                        Utility.getSetting(getApplicationContext(), "userid", ""), "&master=", "version", "&imei=", strImei, "&v=", SettingActivity.APP_VERSION,
                        "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));


                Recordset versioning = null;
                Nset newMaster = Nset.newArray();

                try {
//
//					download tabel per-cabang
					/*if (Utility.getNewToken()) {

						String url = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
								Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=",
								"MST_DOWNLOAD" ,"&imei=" ,strImei, "&lastSync=", "",
								"&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

						Nset order = Nset.newObject();

						String dfdf = Utility.getHttpConnection(url);
						order = Nset.readJSON(dfdf);

						if (order.getArraySize() > 0) {
							for (int j = 0; j <  order.getArraySize(); j++) {

								String table = order.getData(j).getData("table").toString();

								for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
									String code = order.getData(j).getData("data").getData(k).getData("code").toString();
									String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

									Nset data = Nset.readJSON(drst);
									String kode = data.getData("status").toString();
									if (kode.equalsIgnoreCase("OK")) {
										Recordset xxx = Stream.downStream(drst);
										if (xxx.getRows() > 0) {
											if (code.equals("truncate")) {
												//true untuk medownload semua record tabel karena first install
//												changeBranch = true;
												downloadChangeBranchTable(xxx);
											}
										}
									}else{
										logintrue = false;
										message = "Error ketika download tabel "+table;
										return;
									}

								}

							}
						}
					}*/

                    //download  tabel yang dinamis perubahanya
					/*if (Utility.getNewToken()) {

						String url = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
								Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=",
								"MST_FIRST_DOWNLOAD" ,"&imei=" ,strImei, "&lastSync=", "",
								"&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

						Nset order = Nset.newObject();

						String dfdf = Utility.getHttpConnection(url);
						order = Nset.readJSON(dfdf);

						if (order.getArraySize() > 0) {
							for (int j = 0; j <  order.getArraySize(); j++) {

								String table = order.getData(j).getData("table").toString();

								for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
									String code = order.getData(j).getData("data").getData(k).getData("code").toString();
									String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

									Nset data = Nset.readJSON(drst);
									String kode = data.getData("status").toString();
									if (kode.equalsIgnoreCase("OK")) {
										Recordset xxx = Stream.downStream(drst);
										if (xxx.getRows() > 0) {
											if (code.equals("truncate")) {
												downloadTableFirst(xxx);
											}
										}
									}else{
										logintrue = false;
										message = "Error ketika download tabel "+table;
										return;
									}
								}

							}
						}
					}*/

                    //menbanding versi tabel
                    versioning = Syncronizer.getHttpStream(sUrl);
                    Hashtable<String, String> ver = Global.rowtocol(Connection.DBquery("select vername, verno from IVERSION "));
                    Hashtable<String, String> sync = Global.rowtocol(Connection.DBquery("select vername, last_sync from IVERSION "));

                    for (int i = 0; i < versioning.getRows(); i++) {
                        boolean mustdownload = false;

                        //ini untuk user ketika clear data tapi tidak uninstall aplikasi / untuk pertama kali download
                        if (ver.get(versioning.getText(i, "version_name")) == null) {
                            mustdownload = true;
                        } else if (Utility.getInt(versioning.getText(i, "version_no")) > Utility.getInt(ver.get(versioning.getText(i, "version_name")) != null ? ver.get(versioning.getText(i, "version_name")) : "0")) {
                            mustdownload = true;
                        } else if (Utility.getInt(ver.get(versioning.getText(i, "version_name"))) == 0) {
                            mustdownload = true;
                        }


                        String tble = versioning.getText(i, "version_name");
                        String ddd = "";
                        if (tble.equalsIgnoreCase("MST_BU")) {
                            ddd = tble;
                        }

//						boolean isUpdated = Utility.isContainValue2(tble, tbleUpdated);

//						if (!isUpdated) {
                        if (mustdownload) {

                            //get new token
                            if (Utility.getNewToken()) {
                                if (!tble.equalsIgnoreCase("")) {

//										sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
//												Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=",
//												tble ,"&imei=" ,strImei, "&lastSync=", sync.get(versioning.getText(i, "version_name")),"&v=100",
//												"&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

                                    String strLastSync = sync.get(versioning.getText(i, "version_name"));
                                    if (strLastSync == null) {
                                        strLastSync = "";
                                    }

                                    sUrl = SettingActivity.URL_SERVER + "versionservlet/?userid=" +
                                            Utility.getSetting(getApplicationContext(), "userid", "").trim() + "&v=100&master=" +
                                            versioning.getText(i, "version_name") + "&imei=" + strImei + "&lastSync=" + strLastSync +
                                            "&token=" + Utility.getSetting(getApplicationContext(), "Token", "");

                                    setProsesBarMsg("Please Wait \r\nDownload Master " + tble + " (" + (i * 100 / versioning.getRows()) + "%)");

                                    Nset order = Nset.newObject();

                                    String dfdf = Utility.getHttpConnection(sUrl);
                                    order = Nset.readJSON(dfdf);
                                    Vector<String> allSuccess = new Vector<String>();
                                    if (order.getArraySize() > 0) {
                                        Vector<String> vSucces = new Vector<String>();
                                        long trun = 0, ins = 0, que = 0;
                                        for (int j = 0; j < order.getArraySize(); j++) {

                                            String table = order.getData(j).getData("table").toString();
                                            String lastSync = order.getData(j).getData("lastSync").toString();

                                            for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
                                                String code = order.getData(j).getData("data").getData(k).getData("code").toString();
                                                String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

                                                Nset data = Nset.readJSON(drst);
                                                String kode = data.getData("status").toString();
                                                if (kode.equalsIgnoreCase("OK")) {
                                                    Recordset xxx = Stream.downStream(drst);
                                                    if (xxx.getRows() > 0) {
                                                        if (code.equals("truncate")) {
//																Syncronizer.saveToDBDAF(table, xxx);
                                                            trun = Syncronizer.Truncate(table, xxx);
                                                            vSucces.add(trun + "");
                                                        } else if (code.equals("insert")) {
//																Syncronizer.deleteDBDAF(table, xxx);
                                                            ins = Syncronizer.insertToDBSpecialDAF(table, xxx);
                                                            vSucces.add(ins + "");
                                                        } else if (code.equals("delete")) {
                                                            for (int l = 0; l < xxx.getRows(); l++) {
                                                                String qry = xxx.getText(l, "query");
                                                                Connection.DBquery(qry);
//																	que = Connection.DBQueryDAF(qry);
//																	vSucces.add(que+"");
                                                            }
                                                        } else if (code.equals("update")) {
                                                            for (int l = 0; l < xxx.getRows(); l++) {
                                                                String qry = xxx.getText(l, "query");
                                                                Connection.DBquery(qry);
//																	que = Connection.DBQueryDAF(qry);
//																	vSucces.add(que+"");
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    logintrue = false;
                                                    message = "Error ketika download tabel " + table;
                                                    return;
                                                }

                                            }

                                            if (!Utility.isContainValue2("0", vSucces)) {
                                                Connection.DBupdate("IVERSION", "last_sync=" + lastSync, "verno=" + versioning.getText(i, "version_no").toString(), "where", "vername=" + table);
                                            }

                                        }

//											tbleUpdated.add(versioning.getText(i, "version_name"));
                                    }

                                }
                            } else {
                                message = "Masalah koneksi, Gagal validasi token";
                                data = null;
                                logintrue = false;

                            }
                        }
//						}


                    }

                    Global.newInit();
                    if (SettingActivity.URL_SERVER.startsWith("http")) {
                        setProsesBarMsg("Please Wait \r\nDownload Order (100%)");
                        downloadOrder(LoginActivityTest.this, LoginActivityTest.this);
                    }

                } catch (Exception e) {
                    message = e.getMessage();
                    logintrue = false;
                }
            }
        }, new Runnable() {
            public void run() {
                if (logintrue) {
                    //req closebatc 25071014
                    Utility.setSetting(LoginActivityTest.this, "lockorder", data.getText(0, "lockorder"));

                    if (data.getText(0, "param").equals("DELETEACTIVITY")) {
                        Connection.DBdelete("data_activity", null, null);
                        deleteimage(true);
                    }

                    if (!Utility.getSetting(LoginActivityTest.this, "BATCH", "").equals((data.getText(0, "batch")))) {
                        Utility.setSetting(LoginActivityTest.this, "COUNT", "");
                    }
                    Utility.setSetting(LoginActivityTest.this, "BATCH", data.getText(0, "batch"));

                    if (data.getText(0, "message").length() >= 3) {
                        Utility.setSetting(getApplicationContext(), "SERVICE", "OK");
                        Utility.setSetting(getApplicationContext(), "LASTUSER", Utility.getSetting(getApplicationContext(), "USERNAME", ""));
                        Utility.JOBCODE = data.getText(0, "jobcode");
                        Utility.setSetting(getApplicationContext(), "USERNAME", edtUsername.getText().toString());
                        Utility.setSetting(getApplicationContext(), "PASSWORD", edtPassword.getText().toString());
                        AdapterDialog.goingToNextActivity(LoginActivityTest.this, new MenuApplicationActivity(), "Informasi", data.getText(0, "message"));
                    } else {
                        Utility.setSetting(getApplicationContext(), "firstInstall", "ok");
                        Utility.setSetting(getApplicationContext(), "office_code", Utility.OFFICE_CODE);

                        goToMainmenu();
                    }

                } else {
                    if (data != null) {
                        if (data.getRows() >= 1) {
                            if (data.getText(0, "message").length() >= 3) {
                                actionlogin = data.getText(0, "action");
                                showMessage(data.getText(0, "message"), data.getText(0, "ok"), "Cancel", false, 0);
                            } else {
                                AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                            }
                        } else {
                            AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                        }
                    } else
                        AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                }
            }
        });
    }

    /**
     * download master untuk tabel yang diperlukan dalam aplikasi daf
     * tabel yang didownload berasar dari server
     */
    private void downloadMasterWithThread() {
        Messagebox.showProsesBar(LoginActivityTest.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Utility.setSetting(getApplicationContext(), "ERRORMSG", "");
                Utility.setSetting(getApplicationContext(), "REALNAME", data.getText(0, "realname"));
                setProsesBarMsg("Please Wait \r\nDownload Version");

                // version_id,version_no, app_no, app_all
                String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
                        Utility.getSetting(getApplicationContext(), "userid", ""), "&master=", "version", "&imei=", strImei, "&v=", SettingActivity.APP_VERSION,
                        "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

                Recordset versioning = null;
                Recordset rTblDinamis = null;
                Nset newMaster = Nset.newArray();

                try {

					/*if (changeBranch) {
						if (Utility.getNewToken()) {

							String url = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
									Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=",
									"MST_DOWNLOAD" ,"&imei=" ,strImei, "&lastSync=", "",
									"&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

							Nset order = Nset.newObject();

							String dfdf = Utility.getHttpConnection(url);
							order = Nset.readJSON(dfdf);

							if (order.getArraySize() > 0) {
								for (int j = 0; j <  order.getArraySize(); j++) {

									String table = order.getData(j).getData("table").toString();

									for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
										String code = order.getData(j).getData("data").getData(k).getData("code").toString();
										String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

										Nset data = Nset.readJSON(drst);
										String kode = data.getData("status").toString();
										if (kode.equalsIgnoreCase("OK")) {
											Recordset xxx = Stream.downStream(drst);
											if (xxx.getRows() > 0) {
												if (code.equals("truncate")) {
													//true untuk medownload semua record tabel karena first install
//													changeBranch = true;
													downloadChangeBranchTable(xxx);
												}
											}
										}else{
											logintrue = false;
											message = "Error ketika download tabel "+table;
											return;
										}
									}

								}
							}
						}
					}*/

                    versioning = Syncronizer.getHttpStream(sUrl);

                    Hashtable<String, String> ver = Global.rowtocol(Connection.DBquery("select vername, verno from IVERSION "));
                    Hashtable<String, String> sync = Global.rowtocol(Connection.DBquery("select vername, last_sync from IVERSION "));

                    for (int i = 0; i < versioning.getRows(); i++) {

                        boolean mustdownload = false;

                        //ini untuk user ketika clear data tapi tidak uninstall aplikasi / untuk pertama kali download
                        if (ver.get(versioning.getText(i, "version_name")) == null) {
                            mustdownload = true;
                        } else if (ver.size() < 1) {
                            mustdownload = true;
                        }

                        //ini untuk user yang berbeda login  dalam 1 hape
                        else if (!isSameUser(edtUsername.getText().toString())) {
                            mustdownload = true;
                        } else if (Utility.getInt(versioning.getText(i, "version_no")) > Utility.getInt(ver.get(versioning.getText(i, "version_name")) != null ? ver.get(versioning.getText(i, "version_name")) : "0")) {
                            mustdownload = true;
                        } else if (Utility.getInt(ver.get(versioning.getText(i, "version_name"))) == 0) {
                            mustdownload = true;
                        }

                        String tble = versioning.getText(i, "version_name");
                        String ddd = "";
                        if (tble.equalsIgnoreCase("MST_SALES_DEALER")) {
                            ddd = tble;
                        }

//						boolean isUpdated = Utility.isContainValue2(tble, tbleUpdated);
//						if (!isUpdated) {

                        if (mustdownload) {

                            //get new token
                            if (Utility.getNewToken()) {
                                if (!tble.equalsIgnoreCase("")) {

                                    setProsesBarMsg("Please Wait \r\nDownload Master " + tble + " (" + (i * 100 / versioning.getRows()) + "%)");

                                    sUrl = SettingActivity.URL_SERVER + "versionservlet/?userid=" +
                                            Utility.getSetting(getApplicationContext(), "userid", "").trim() + "&v=100&master=" +
                                            versioning.getText(i, "version_name") + "&imei=" + strImei + "&lastSync=" + sync.get(versioning.getText(i, "version_name")) +
                                            "&token=" + Utility.getSetting(getApplicationContext(), "Token", "");

                                    Nset order = Nset.newObject();

                                    String dfdf = Utility.getHttpConnection(sUrl);
                                    order = Nset.readJSON(dfdf);

                                    Vector<String> allSuccess = new Vector<String>();
                                    if (order.getArraySize() > 0) {
                                        Vector<String> vSucces = new Vector<String>();
                                        long trun = 0, ins = 0, que = 0;
                                        for (int j = 0; j < order.getArraySize(); j++) {

                                            String table = order.getData(j).getData("table").toString();
                                            String lastSync = order.getData(j).getData("lastSync").toString();

                                            for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
                                                String code = order.getData(j).getData("data").getData(k).getData("code").toString();
                                                String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

                                                Nset data = Nset.readJSON(drst);
                                                String kode = data.getData("status").toString();
                                                if (kode.equalsIgnoreCase("OK")) {
                                                    Recordset xxx = Stream.downStream(drst);
                                                    if (xxx.getRows() > 0) {
                                                        if (code.equals("truncate")) {
//																Syncronizer.saveToDBDAF(table, xxx);
                                                            trun = Syncronizer.Truncate(table, xxx);
                                                            vSucces.add(trun + "");
                                                        } else if (code.equals("insert")) {
//																Syncronizer.deleteDBDAF(table, xxx);
                                                            ins = Syncronizer.insertToDBSpecialDAF(table, xxx);
                                                            vSucces.add(ins + "");
                                                        } else if (code.equals("delete")) {
                                                            for (int l = 0; l < xxx.getRows(); l++) {
                                                                String qry = xxx.getText(l, "query");
                                                                Connection.DBquery(qry);
//																	que = Connection.DBQueryDAF(qry);
//																	vSucces.add(que+"");
                                                            }
                                                        } else if (code.equals("update")) {
                                                            for (int l = 0; l < xxx.getRows(); l++) {
                                                                String qry = xxx.getText(l, "query");
                                                                Connection.DBquery(qry);
//																	que = Connection.DBQueryDAF(qry);
//																	vSucces.add(que+"");
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    logintrue = false;
                                                    message = "Error ketika download tabel " + table;
                                                    return;
                                                }
                                            }

                                            if (!Utility.isContainValue2("0", vSucces)) {
                                                Connection.DBupdate("IVERSION", "last_sync=" + lastSync, "verno=" + versioning.getText(i, "version_no").toString(), "where", "vername=" + table);
                                            }
                                        }

                                        tbleUpdated.add(versioning.getText(i, "version_name"));
                                    }
                                }
                            } else {
                                message = "Masalah koneksi, Gagal validasi token";
                                data = null;
                                logintrue = false;

                            }
                        }
//						}
                    }

                    Global.newInit();
                    if (SettingActivity.URL_SERVER.startsWith("http")) {
                        setProsesBarMsg("Please Wait \r\nDownload Order (100%)");
                        downloadOrder(LoginActivityTest.this, LoginActivityTest.this);
                    }

                } catch (Exception e) {
                    message = e.getMessage();
                    data = null;
                    logintrue = false;
                }


            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (logintrue) {
                    //req closebatc 25071014
                    Utility.setSetting(LoginActivityTest.this, "lockorder", data.getText(0, "lockorder"));

                    if (data.getText(0, "param").equals("DELETEACTIVITY")) {
                        Connection.DBdelete("data_activity", null, null);
                        deleteimage(true);
                    }

                    if (!Utility.getSetting(LoginActivityTest.this, "BATCH", "").equals((data.getText(0, "batch")))) {
                        Utility.setSetting(LoginActivityTest.this, "COUNT", "");
                    }
                    Utility.setSetting(LoginActivityTest.this, "BATCH", data.getText(0, "batch"));

                    if (data.getText(0, "message").length() >= 3) {
                        Utility.setSetting(getApplicationContext(), "SERVICE", "OK");
                        Utility.setSetting(getApplicationContext(), "LASTUSER", Utility.getSetting(getApplicationContext(), "USERNAME", ""));
                        Utility.JOBCODE = data.getText(0, "jobcode");
                        Utility.setSetting(getApplicationContext(), "USERNAME", edtUsername.getText().toString());
                        Utility.setSetting(getApplicationContext(), "PASSWORD", edtPassword.getText().toString());
                        AdapterDialog.goingToNextActivity(LoginActivityTest.this, new MenuApplicationActivity(), "Informasi", data.getText(0, "message"));
                    } else {
                        Utility.setSetting(getApplicationContext(), "firstInstall", "ok");
                        Utility.setSetting(getApplicationContext(), "office_code", Utility.OFFICE_CODE);

                        goToMainmenu();
                    }

                } else {
                    if (data != null) {
                        if (data.getRows() >= 1) {
                            if (data.getText(0, "message").length() >= 3) {
                                actionlogin = data.getText(0, "action");
                                showMessage(data.getText(0, "message"), data.getText(0, "ok"), "Cancel", false, 0);
                            } else {
                                AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                            }
                        } else {
                            AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                        }
                    } else
                        AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", message);
                }
            }
        });
    }

    /**
     * digunakan untuk mendownload tabel tabel yang diperlukan
     * oleh cabang, ini dipakai jika device digunakan oleh user berbeda cabang
     */


    private void downloadTableFirst(Recordset tbl) {

        try {
            for (int i = 0; i < tbl.getRows(); i++) {

                String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=", Utility.getSetting(getApplicationContext(), "userid", "").trim(),
                        "&master=", tbl.getText(i, "versionname"), "&v=100", "&imei=", strImei,
                        "&lastSync=", "", "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));


                String tble = tbl.getText(i, "versionname");
                String ddd = "";
                if (tble.equalsIgnoreCase("MST_SALES_DEALER")) {
                    ddd = tble;
                }


                //cek apakah tabel sudah terupdate dan terdownload di versioning
                boolean isUpdated = Utility.isContainValue2(tbl.getText(i, "versionname"), tbleUpdated);

                if (!isUpdated) {
                    if (Utility.getNewToken()) {
                        if (!tble.equalsIgnoreCase("")) {

                            setProsesBarMsg("Please Wait \r\nDownload " + tble);

                            Nset order = Nset.newObject();

                            String dfdf = Utility.getHttpConnection(sUrl);
                            order = Nset.readJSON(dfdf);

                            Vector<String> allSuccess = new Vector<String>();
                            if (order.getArraySize() > 0) {
                                Vector<String> vSucces = new Vector<String>();
                                long trun = 0, ins = 0, que = 0;
                                for (int j = 0; j < order.getArraySize(); j++) {

                                    String table = order.getData(j).getData("table").toString();
                                    String lastSync = order.getData(j).getData("lastSync").toString();

                                    for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
                                        String code = order.getData(j).getData("data").getData(k).getData("code").toString();
                                        String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

                                        Nset data = Nset.readJSON(drst);
                                        String kode = data.getData("status").toString();
                                        if (kode.equalsIgnoreCase("OK")) {
                                            Recordset xxx = Stream.downStream(drst);
                                            if (xxx.getRows() > 0) {
                                                if (code.equals("truncate")) {
//													Syncronizer.saveToDBDAF(table, xxx);
                                                    trun = Syncronizer.Truncate(table, xxx);
                                                    vSucces.add(trun + "");
                                                } else if (code.equals("insert")) {
                                                    Syncronizer.deleteDBDAF(table, xxx);
                                                    ins = Syncronizer.insertToDBSpecialDAF(table, xxx);
                                                    vSucces.add(ins + "");
                                                } else if (code.equals("delete")) {
                                                    for (int l = 0; l < xxx.getRows(); l++) {
                                                        String qry = xxx.getText(l, "query");
                                                        Connection.DBquery(qry);
//														que = Connection.DBQueryDAF(qry);
//														vSucces.add(que+"");
                                                    }
                                                } else if (code.equals("update")) {
                                                    for (int l = 0; l < xxx.getRows(); l++) {
                                                        String qry = xxx.getText(l, "query");
                                                        Connection.DBquery(qry);
//														que = Connection.DBQueryDAF(qry);
//														vSucces.add(que+"");
                                                    }
                                                }
                                            }
                                        } else {
                                            logintrue = false;
                                            message = "Error ketika download tabel " + table;
                                            return;
                                        }
                                    }

                                    if (!Utility.isContainValue2("0", vSucces)) {
                                        Connection.DBupdate("IVERSION", "last_sync=" + lastSync, "verno=" + tbl.getText(i, "version_no").toString(), "where", "vername=" + table);
                                    }

                                    tbleUpdated.add(tble);

                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private void downloadChangeBranchTable(Recordset tbl) {

//		Nset newMaster = Nset.newArray();
        try {
            for (int i = 0; i < tbl.getRows(); i++) {

                //jika cabang sama maka kirim last_sync
                String lstSync = "";
                if (!changeBranch) {
                    Recordset dds = Connection.DBquery("select last_sync from IVERSION where vername ='" + tbl.getText(i, "versionname") + "'");
                    if (dds.getRows() > 0) {
                        lstSync = dds.getText(0, "last_sync");
                    }
                }

//				String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=" ,
//						Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=" , tbl.getText(i, "versionname") ,"&v=100",
//						"&imei=" ,strImei, "&lastSync=", lstSync, "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

                String sUrl = SettingActivity.URL_SERVER + "versionservlet/?userid=" +
                        Utility.getSetting(getApplicationContext(), "userid", "").trim() + "&v=100&master=" +
                        tbl.getText(i, "versionname") + "&imei=" + strImei + "&lastSync=" + lstSync +
                        "&token=" + Utility.getSetting(getApplicationContext(), "Token", "");

                String tble = tbl.getText(i, "versionname"), ddd;
                if (tble.equalsIgnoreCase("MST_USER_MARKETING")) {
                    ddd = tble;
                }

                //cek apakah tabel sudah terupdate dan terdownload di versioning
                boolean isUpdated = Utility.isContainValue2(tbl.getText(i, "versionname"), tbleUpdated);

                if (!isUpdated) {
                    if (Utility.getNewToken()) {
                        if (!tble.equalsIgnoreCase("")) {

                            setProsesBarMsg("Please Wait \r\nDownload " + tble);

                            Nset order = Nset.newObject();
                            String dfdf = Utility.getHttpConnection(sUrl);
                            order = Nset.readJSON(dfdf);

                            Vector<String> allSuccess = new Vector<String>();
                            if (order.getArraySize() > 0) {
                                Vector<String> vSucces = new Vector<String>();
                                long trun = 0, ins = 0, que = 0;
                                for (int j = 0; j < order.getArraySize(); j++) {

                                    String table = order.getData(j).getData("table").toString();
                                    String lastSync = order.getData(j).getData("lastSync").toString();

                                    for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
                                        String code = order.getData(j).getData("data").getData(k).getData("code").toString();
                                        String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

                                        Nset data = Nset.readJSON(drst);
                                        String kode = data.getData("status").toString();
                                        if (kode.equalsIgnoreCase("OK")) {
                                            Recordset xxx = Stream.downStream(drst);
                                            if (xxx.getRows() > 0) {
                                                if (code.equals("truncate")) {
//													Syncronizer.saveToDBDAF(table, xxx);
                                                    trun = Syncronizer.Truncate(table, xxx);
                                                    vSucces.add(trun + "");
                                                } else if (code.equals("insert")) {
                                                    Syncronizer.deleteDBDAF(table, xxx);
                                                    ins = Syncronizer.insertToDBSpecialDAF(table, xxx);
                                                    vSucces.add(ins + "");
                                                } else if (code.equals("delete")) {
                                                    for (int l = 0; l < xxx.getRows(); l++) {
                                                        String qry = xxx.getText(l, "query");
                                                        Connection.DBquery(qry);
//														que = Connection.DBQueryDAF(qry);
//														vSucces.add(que+"");
                                                    }
                                                } else if (code.equals("update")) {
                                                    for (int l = 0; l < xxx.getRows(); l++) {
                                                        String qry = xxx.getText(l, "query");
                                                        Connection.DBquery(qry);
//														que = Connection.DBQueryDAF(qry);
//														vSucces.add(que+"");
                                                    }
                                                }
                                            }
                                        } else {
                                            logintrue = false;
                                            message = "Error ketika download tabel " + table;
                                            return;
                                        }
                                    }

//									if (!changeBranch) {
//										if (!Utility.isContainValue2("0", vSucces)) {
//											Connection.DBupdate("IVERSION", "last_sync="+lastSync, "verno="+tbl.getText(i, "version_no").toString(), "where", "vername="+table);
//										}
//									}else {
                                    if (!Utility.isContainValue2("0", vSucces)) {
                                        Connection.DBupdate("IVERSION", "last_sync=" + lastSync, "verno=" + tbl.getText(i, "version_no").toString(), "where", "vername=" + table);
                                    }
                                    tbleUpdated.add(tble);
//									}

                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * setting parrameter tertentu yang digunakan oleh aplikasi
     * menjalankan service pemgiriman lokasi
     * dan berpindah screen ke mainmenu
     */

    private void goToMainmenu() {

        Global.newInit();
        Utility.setSetting(getApplicationContext(), "SERVICE", "OK");
        Utility.setSetting(getApplicationContext(), "LASTUSER", Utility.getSetting(getApplicationContext(), "USERNAME", ""));

        Utility.setSetting(getApplicationContext(), "USERNAME", edtUsername.getText().toString());
        Utility.setSetting(getApplicationContext(), "PASSWORD", edtPassword.getText().toString());

        Utility.JOBCODE = data.getText(0, "jobcode");
//		Utility.OFFICE_CODE = "10100";

//		DAFReciever.sendLocation(getApplicationContext());

        startActivity(new Intent(LoginActivityTest.this, MenuApplicationActivity.class));
    }

    /**
     * @param userId
     * @return untuk mengecek apakah user id yang login sekarang
     * sama dengan user yang login sebelumnya
     */
    private boolean isSameUser(String userId) {
        if (Utility.getSetting(getApplicationContext(), "USERNAME", "").equalsIgnoreCase(userId)) {
            return true;
        }
        return false;
    }

    /**
     * @param userID
     * @param strPath digunakan untuk mendownload ttd user login
     */
    private void downloadImage(String userID, final String strPath) {
        final String url = SettingActivity.URL_SERVER + "viewimageservlet?userid=" + userID + "&master=signature&filetype=blob&imei=&rnd=&v=100&jenis=user&token=" + Utility.getSetting(Utility.getAppContext(), "Token", "");
        Messagebox.showProsesBar(LoginActivityTest.this, new Runnable() {

            @Override
            public void run() {
                Utility.saveImage(strPath, url);
            }
        }, new Runnable() {

            @Override
            public void run() {

                if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
                    downloadFirsttime();
                } else {
                    downloadMasterWithThread();
                }
            }
        });
    }

    /**
     * @param message menampilkan progress dialog
     */
    private void setProsesBarMsg(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Messagebox.setProsesBarMsg(message);
            }
        });
    }

    /**
     * @param all
     */
    private void deleteimage(boolean all) {
        if (all) {
            String[] s = new File(Utility.getDefaultPath()).list();
            if (s != null) {
                for (int i = 0; i < s.length; i++) {
                    if (!s[i].contains(".")) {
                        new File(Utility.getDefaultPath(s[i])).delete();
                    }
                }
            }
        } else {
        }
    }

    private void dropTables(Vector<String> data) {
        for (int i = 0; i < data.size(); i++) {
            Connection.DBdrop(data.get(i));
        }
    }

    private boolean updatedTable(Vector<String> data, Vector<String> data2) {
        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < data2.size(); j++) {
                if (data.get(i).equalsIgnoreCase(data2.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    private void loadImages() {
        String[] s = new File(Utility.getDefaultPath()).list();
        Vector<String> data = new Vector<String>();

        for (int i = 0; i < s.length; i++) {
            if (s[i].contains("images")) {
                data.add(s[i]);
            }
        }

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).contains("_")) {
                File oldfile = new File(Utility.getDefaultPath(data.get(i)));
                File newfile = new File(Utility.getDefaultPath(data.get(i).replace("images", "")));
                if (oldfile.exists()) {
                    oldfile.renameTo(newfile);
                }
                Utility.copyFile(newfile.getAbsolutePath(), Utility.getDefaultImagePath(newfile.getName()));
//				Utility.deleteFile(Utility.getDefaultPath(data.get(i)));
            }
        }

        Utility.setSetting(getApplicationContext(), "v.13", "ok");
    }

    public void showMessage(String message, final String PositiveButton, final String NegativeButton, boolean cancel, final int reqcode) {
        Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle("");
        if (NegativeButton != null) {
            if (!NegativeButton.equals("")) {
                dlg.setNegativeButton(NegativeButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }
        if (PositiveButton != null) {
            if (!PositiveButton.equals("")) {
                dlg.setPositiveButton(PositiveButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        try {
                            if (actionlogin.toLowerCase().trim().startsWith("intent://")) {
                                Intent i = new Intent(actionlogin.substring(9));
                                startActivity(i);
                            } else {
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(actionlogin));
                                startActivity(i);
                            }

                        } catch (Exception e) {
                        }
                    }
                });
            }
        }

        dlg.setCancelable(cancel);
        dlg.setMessage(message);
        AlertDialog alertDialog = dlg.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            if (Utility.finishSign) {
                uploadSign();
            } else {
                AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Maaf, anda belum melakukan tanda tangan ");
            }

        } else if (requestCode == GPS && resultCode == 0) {
            LocationOffline loc = new LocationOffline(getApplicationContext());
        } else if (requestCode == AppVersion && resultCode == Activity.RESULT_OK) {
            Utility.setSetting(getApplicationContext(), "app_version", Global.getText("app.version", ""));
        } else if (requestCode == certificate && resultCode == 0) {
            isCertExist = Utility.certificateFIFExist();
        }
    }

    private void uploadSign() {
        Messagebox.showProsesBar(LoginActivityTest.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                File file = null;
                if (new File(Utility.getDefaultSign(strFileName)).exists() == true) {
                    file = new File(Utility.getDefaultSign(strFileName));
                } else
                    file = new File(Utility.getDefaultPath(strFileName));

                if (file != null) {
                    result = ImagePost.postHttpConnectionWithPicture(SettingActivity.URL_SERVER + "uploadservlet?filetype=blob&master=signature&userid=" + Utility.getSetting(getApplicationContext(), "userid", ""), null, strFileName, file.toString(), UUID.randomUUID().toString());
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (result.endsWith("OK")) {
                    if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
                        downloadFirsttime();
                    } else
                        downloadMasterWithThread();
                } else {
                    AdapterDialog.showMessageDialog(LoginActivityTest.this, "Informasi", "Maaf pengiriman tanda tangan gagal");
                }
            }
        });
    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog dialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        switch (id) {
            case GPS:
                dialogBuilder.setTitle("Setting GPS");
                dialogBuilder.setMessage("Please Turn On Your GPS");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent inten = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(inten, GPS);
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case Bluetooth:
                dialogBuilder.setTitle("Setting Bluetooth");
                dialogBuilder.setMessage("Please Turn On Your Bluetooth");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent inten = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                        startActivityForResult(inten, Bluetooth);
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case Wifi:
                dialogBuilder.setTitle("Setting Wifi");
                dialogBuilder.setMessage("Please Turn On Your Wifi");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent inten = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivityForResult(inten, Wifi);
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case AppVersion:
                dialogBuilder.setTitle("New App Version");
                dialogBuilder.setMessage("Terdapat App Version terbaru, silahkan update");
                dialog = dialogBuilder.create();
                dialog.setButton("Update", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Uri uri = Uri.parse(Global.getText("app.download.url", ""));
                        Intent inten = new Intent(Intent.ACTION_VIEW, uri);
                        startActivityForResult(inten, AppVersion);
                    }
                });

                dialog.setButton2("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case token:
                dialogBuilder.setTitle("Token Expired");
                dialogBuilder.setMessage("Token expired, apakan anda ingin memperbaharui token ?");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getToken();
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;
        }

        return dialog;
    }

    @Override
    protected void onRestart() {
        if (Utility.isExit) {
            finish();
        }
        super.onRestart();
    }


}
