package com.daf.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salesforce.R;
import com.salesforce.database.Connection;
import com.salesforce.model.ModelSetting;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.ModelDeviceIdentity;
import com.salesforce.utility.Utility;


public class SettingActivity extends Activity {
    public static String URL_SERVER = getProductionUrl();
    //	public static String URL_TOKEN = getProductionUrl().substring(0, 28)+"DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";;
    public static String URL_STORAGE = "";
    public static String APP_VERSION = "1.0.0";

    public static final String INTERNAL_STORAGE = "INTERNAL STORAGE";
    public static final String EXTERNAL_STORAGE = "EXTERNAL STORAGE";


    private Spinner spinner;
    private String typeStorage;
    private EditText edtTextUrl;
    private String mac1;
    private String mac2;
    private String mac3;
    private String mac4;

//	public static String getProductionUrl(){
//		return Connection.DBquery("select * from SETTING ").getText(0, "URL");
//	}//"http://192.168.56.110:8084/ServiceMobile/"; //"https://www.mapproval.bcafinance.co.id:8080/ServiceMobile/"; //


    public static String getProductionUrl() {
//		return Connection.DBquery("select * from SETTING ").getText(0, "URL");
        return Utility.getSetting(Utility.getAppContext(), "URL", "");
        //return "https://www.mapproval.bcafinance.co.id/ServiceMobile/";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.setting2);

        //check if app context null and app will close by system
        Utility.checkingAppContex(SettingActivity.this);

        String url = "";
        String macAddress = "";

		/*TelephonyManager tm = (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE);
		String imei=tm.getDeviceId().toUpperCase();*/

        String imei = "00";
        ModelDeviceIdentity data = Utility.getImeiOrUUID(this);
        imei = data.getImei().toUpperCase();
        if (imei.length() >= 3) {
            ((TextView) findViewById(R.id.txtImei)).setText("IMEI : " + imei);
        }

        if(data.getIsAndroid10()){
            ((TextView) findViewById(R.id.txtImei)).setText("IMEI : " );
            ((TextView) findViewById(R.id.txtValueImei)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.txtValueImei)).setText(imei);
        }

        if (Utility.getSetting(getApplicationContext(), "app_version", "").equalsIgnoreCase("")) {
            try {
                PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
                SettingActivity.APP_VERSION = pInfo.versionName;
            } catch (Exception e) {
            }
            ((TextView) findViewById(R.id.txtApp)).setText("APP. VERSION : " + SettingActivity.APP_VERSION);
        } else
            ((TextView) findViewById(R.id.txtApp)).setText("APP. VERSION : " + Utility.getSetting(getApplicationContext(), "app_version", ""));


        ((ImageView) findViewById(R.id.imageView1)).setImageResource(android.R.drawable.ic_menu_revert);
        ((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageView1)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });

        ((TextView) findViewById(R.id.textJudul)).setText("Setting");
        try {
            Cursor c = Connection.DBexecute("select * from SETTING ");
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    URL_SERVER = c.getString(c.getColumnIndex("URL"));
                    macAddress = c.getString(c.getColumnIndex("MAC_ADDRESS_PRINTER"));
                    URL_STORAGE = c.getString(c.getColumnIndex("STORAGE"));
                    typeStorage = URL_STORAGE;
                } while (c.moveToNext());
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        edtTextUrl = ((EditText) findViewById(R.id.editText1));
        edtTextUrl.setText(URL_SERVER);
//		if(URL_SERVER.equals("")||URL_SERVER.equals("/")||URL_SERVER.isEmpty()||URL_SERVER==null){
//			edtTextUrl.setText("http://daf.fifgroup.co.id:8080/ServiceMobile/");   
//		}else{
//			edtTextUrl.setText(URL_SERVER);
//		}

        //set field mac address printer if data available on DB
        if (macAddress != null && !macAddress.equals("")) {
            if (isHaveMacAddress(macAddress)) {
                ((EditText) findViewById(R.id.editText3)).setText(mac1);
                ((EditText) findViewById(R.id.editText4)).setText(mac2);
                ((EditText) findViewById(R.id.editText5)).setText(mac3);
                ((EditText) findViewById(R.id.editText6)).setText(mac4);
            }
        }

        //instance spinner
        spinner = (Spinner) findViewById(R.id.spinner1);
        List<String> list = getListSpinner();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(SettingActivity.this, android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(arrayAdapter);

        if (typeStorage != null && !typeStorage.equals("")) {
            if (Utility.getExternalPath() != null && typeStorage.equals(EXTERNAL_STORAGE)) {
                spinner.setSelection(arrayAdapter.getPosition(typeStorage));
            } else {
                typeStorage = INTERNAL_STORAGE;
                spinner.setSelection(arrayAdapter.getPosition(typeStorage));
            }

        } else {
            typeStorage = INTERNAL_STORAGE;
            spinner.setSelection(arrayAdapter.getPosition(typeStorage));
        }

        // saving record to database
        ((Button) findViewById(R.id.imageView3)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                StringBuffer buffer = new StringBuffer("");
                String valSdcard = spinner.getSelectedItem().toString();
                setUrlTrack(edtTextUrl.getText().toString());
                if (valSdcard != null) {
                    buffer.append(((EditText) findViewById(R.id.editText3)).getText().toString()).append(":");
                    buffer.append(((EditText) findViewById(R.id.editText4)).getText().toString()).append(":");
                    buffer.append(((EditText) findViewById(R.id.editText5)).getText().toString()).append(":");
                    buffer.append(((EditText) findViewById(R.id.editText6)).getText().toString());

                    ModelSetting model = new ModelSetting();
                    model.setMacAddress(buffer);
                    model.setTypeStorage(valSdcard);


                    model.setUrl(edtTextUrl.getText().toString() + (edtTextUrl.getText().toString().endsWith("/") ? "" : "/"));
                    Utility.setSetting(getApplicationContext(), "URL", edtTextUrl.getText().toString() + (edtTextUrl.getText().toString().endsWith("/") ? "" : "/"));

                    if (insertToDb(model)) {
                        Toast.makeText(SettingActivity.this, "Success Saving to Database", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(SettingActivity.this, "Cannot Saving to Database", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SettingActivity.this, "Please Choose Sd Card Path", Toast.LENGTH_SHORT).show();
                }

            }
        });

        // testConn
        ((Button) findViewById(R.id.imgTest)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Messagebox.showProsesBar(SettingActivity.this, new Runnable() {
                    @Override
                    public void run() {
                        String sd = edtTextUrl.getText().toString() + (edtTextUrl.getText().toString().endsWith("/") ? "" : "/");
                        result = Utility.getHttpConnection(edtTextUrl.getText().toString() + (edtTextUrl.getText().toString().endsWith("/") ? "" : "/"));

                    }
                }, new Runnable() {

                    @Override
                    public void run() {
                        if (result.toLowerCase().contains("nikita") || result.toLowerCase().contains("hello")) {
                            showInfo("Connection Success");
                        } else {
                            showInfo("Connection Timeout");
                        }
                    }
                });
            }
        });

        findViewById(R.id.layoutstorage).setVisibility(View.GONE);
        findViewById(R.id.layoutmac).setVisibility(View.GONE);
    }

    /**
     * untuk menset url tracking order, karena
     * url nya sedikit berbeda dengan url yang lain
     */
    private void setUrlTrack(String url) {
        String first = "";
        Vector<String> dj = Utility.splitVector(url, ":");
        for (int i = 0; i < dj.size(); i++) {
            if (i == 2) {
                first = first + dj.get(i).substring(0, dj.get(i).indexOf("/")) + "/";
            } else {
                first = first + dj.get(i) + ":";
            }
        }
        Utility.setSetting(getApplicationContext(), "URL_TRACK", first);
    }

    /**
     * untuk dialog message
     */
    private String result = "";

    public void showInfo(String message) {
        Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle("");
        dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        dlg.setCancelable(true);
        dlg.setMessage(message);
        AlertDialog alertDialog = dlg.create();
        alertDialog.show();
    }

    /**
     * untuk mengisi nilai pada spinner
     */
    private List<String> getListSpinner() {

        List<String> listSdCard = new ArrayList<String>();

        //checking external storage
        boolean isHaveExternalStorage = Utility.isHaveExternalStorage();

        if (Utility.getExternalPath() != null) {
            listSdCard.add(EXTERNAL_STORAGE);
        }

        listSdCard.add(INTERNAL_STORAGE);

        return listSdCard;
    }

    /**
     * untuk menyimpan nilai setting kedalam tabel
     */
    private boolean insertToDb(ModelSetting model) {
        URL_SERVER = model.getUrl();
        URL_STORAGE = model.getTypeStorage();
        try {
            //Connection.DBclose();
            //Connection.DBopen(SettingActivity.this);
        } catch (Exception e) {
        }
        try {


            Cursor c = Connection.DBexecute("select * from SETTING");
            if (c.getCount() > 0) {
                ContentValues column = new ContentValues();
                column.put("URL", model.getUrl());
                column.put("MAC_ADDRESS_PRINTER", String.valueOf(model.getMacAddress()));
                column.put("STORAGE", model.getTypeStorage());
                Connection.DBupdate("SETTING", column, null, null);
            } else {
                Connection.DBinsert("SETTING", "URL=" + model.getUrl(),
                        "MAC_ADDRESS_PRINTER=" + model.getMacAddress(), "STORAGE=" + model.getTypeStorage());
            }

            c.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * untuk mengecek apakah device mempunya mac address
     */
    private boolean isHaveMacAddress(String macAddress) {
        int length = macAddress.length();
        StringBuilder sb = new StringBuilder();
        int loc1 = 0;
        int loc2 = 0;
        int loc3 = 0;
        int loc4 = 0;
        mac1 = "";
        mac2 = "";
        mac3 = "";
        mac4 = "";

        if (length > 0) {
            for (int i = 0; i < length; i++) {
                sb.append(String.valueOf(macAddress.charAt(i)));

                if ((String.valueOf(sb).endsWith(":"))) {
                    if (mac1.equals("")) {
                        loc1 = sb.length();
                        mac1 = sb.toString();
                        mac1 = mac1.substring(0, loc1 - 1);
                    } else if (mac2.equals("")) {
                        loc2 = sb.length();
                        mac2 = sb.toString();
                        mac2 = mac2.substring(loc1, loc2 - 1);
                    } else if (mac3.equals("")) {
                        loc3 = sb.length();
                        mac3 = sb.toString();
                        mac3 = mac3.substring(loc2, loc3 - 1);
                    }
                }

                if (sb.length() == length) {
                    loc4 = sb.length();
                    mac4 = sb.toString();
                    mac4 = mac4.substring(loc3, loc4);
                }

            }

            return true;
        }

        return false;
    }

}
