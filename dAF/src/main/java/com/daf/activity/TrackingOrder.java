package com.daf.activity;

import java.io.File;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.Logout;
import com.salesforce.utility.Utility;

public class TrackingOrder extends Activity{
	
	private LinearLayout layCab;
//	private static String[] arrMonth = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
//	private static String[] nameMonth = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	private EditText edtTglLahir;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.order_tracking);
		
//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}
		
		layCab = (LinearLayout)findViewById(R.id.layCab);
		
		edtTglLahir = (EditText)findViewById(R.id.edtTglLahir);
		edtTglLahir.setEnabled(false);
		((ScrollView)findViewById(R.id.scrollView1)).setHorizontalScrollBarEnabled(false);
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		((TextView) findViewById(R.id.textJudul)).setText("Tracking Order");
		
		if (Utility.getSetting(getApplicationContext(), "office_type", "").equalsIgnoreCase("H")) {
			layCab.setVisibility(View.VISIBLE);
		}else if (Utility.getSetting(getApplicationContext(), "office_type", "").equalsIgnoreCase("B")) {
			layCab.setVisibility(View.GONE);
		}
		
		((EditText)findViewById(R.id.edtTglOrderFrom)).setText(Utility.getDateDAF());
		((EditText)findViewById(R.id.edtTglOrderTo)).setText(Utility.getDateDAF());
		
		((ImageButton)findViewById(R.id.btnTglLahir)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				edtTglLahir.setEnabled(true);
				Utility.showDatePickerLessThanSysdate(TrackingOrder.this, edtTglLahir).show();				
			}
		});
		
		((ImageButton)findViewById(R.id.btnOrderFrom)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utility.showDatePickerLessThanSysdate(TrackingOrder.this, (EditText)findViewById(R.id.edtTglOrderFrom)).show();				
			}
		});

		((ImageButton)findViewById(R.id.btnOrderTo)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utility.showDatePickerLessThanSysdate(TrackingOrder.this, (EditText)findViewById(R.id.edtTglOrderTo)).show();				
			}
		});
		
		/*edtTglLahir.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (hasFocus) {
					edtTglLahir.setText("");
					edtTglLahir.setEnabled(false);
				}
			}
		});*/
		
		edtTglLahir.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				edtTglLahir.setText("");
				edtTglLahir.setEnabled(false);
				return true;
			}
		});
		
		((Button)findViewById(R.id.btnSearch)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				
				Intent intent = new Intent(TrackingOrder.this, TrackingOrderTabelActivity.class);
				intent.putExtra("custName", ((EditText)findViewById(R.id.edtNamaKonsumen)).getText().toString());
				intent.putExtra("tglLahir", Utility.changeFormatDate(((EditText)findViewById(R.id.edtTglLahir)).getText().toString(), "dd-MMM-yyy", "yyyy-MM-dd").replace("-", ""));
				intent.putExtra("noAPP", ((EditText)findViewById(R.id.edtNoApp)).getText().toString());
				intent.putExtra("dealer", ((EditText)findViewById(R.id.edtNamaDealer)).getText().toString());
				intent.putExtra("tglOrderFrom", Utility.changeFormatDate(((EditText)findViewById(R.id.edtTglOrderFrom)).getText().toString(), "dd-MMM-yyy", "yyyy-MM-dd").replace("-", ""));
				intent.putExtra("tglOrderTo", Utility.changeFormatDate(((EditText)findViewById(R.id.edtTglOrderTo)).getText().toString(), "dd-MMM-yyy", "yyyy-MM-dd").replace("-", ""));
				if (layCab.getVisibility() == View.VISIBLE) {
					intent.putExtra("cabang", ((EditText)findViewById(R.id.edtNamaCabang)).getText().toString());
				}else
					intent.putExtra("cabang", "");
				
				intent.putExtra("userId", ((EditText)findViewById(R.id.edtUserID)).getText().toString());
				startActivity(intent);
				
//				print(new File(Utility.getDefaultPath("PO/PO.pdf")));
			}
		});
	}
	
	/*private String formatTanggal(String tgl){
		String tanggal = "";
		if (!tgl.equalsIgnoreCase("")) {
			Vector<String> vTgl = Utility.splitVector(tgl, "-");			
			for (int i = 0; i < nameMonth.length; i++) {
				if (vTgl.elementAt(1).toString().equalsIgnoreCase(nameMonth[i])) {
					tanggal = vTgl.elementAt(2).toString()+arrMonth[i]+vTgl.elementAt(0).toString();
					break;
				}
			}
			return tanggal;
		}
		return tanggal;
	}*/
	
	/**
	 * untuk memanggil aplikasi eprint
	 */
	public void print(File file) {		
		Intent i = new Intent("org.androidprinting.intent.action.PRINT");
		//Utility.getDefaultPath("PO")
//		i.setPackage("com.hp.android.print");
//		i.putExtra("com.hp.android.print", "com.hp.android.print.preview.ShareFilesPreviewActivity");
//		i.setClassName("com.hp.android.print","com.hp.android.print.preview.ShareFilesPreviewActivity");
 		i.setDataAndType(Uri.fromFile(file), "application/pdf");
		try {
			startActivityForResult(i, 1001);
		} catch (ActivityNotFoundException e) {
			// TODO Auto-generated catch block
//			showDialog("Maaf, anda harus download Aplikasi HP ePrint");
		}

	}
}
