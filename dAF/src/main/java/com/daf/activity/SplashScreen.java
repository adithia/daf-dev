package com.daf.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Vector;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

//import com.google.android.gms.internal.ao;
import com.salesforce.R;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;
import com.salesforce.service.DAFReciever;
import com.salesforce.utility.Utility;

public class SplashScreen extends Activity {

	private boolean sophosInstalled = false;
	ProgressBar pbBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splashscreen);
		Utility.setAppContext(getApplicationContext());
		new splsh().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		
	}
	
//	private boolean listAssetFiles(String path) {
//	    String [] list;
////	    ArrayList<String> ygdicopy=new ArrayList<String>();
//	    try {
//	        list = getAssets().list(path);
//	        if (list.length > 0) {
//	            // This is a folder
//	            for (String file : list) {
//	                if (!listAssetFiles(path + "/" + file)){
//	                    return false;
//	                }else {
//	                	if(file.endsWith(".pdf")||file.endsWith(".txt")){
//	                		String sourceFile = path + "/" + file.toString();
////	                		ygdicopy.add(a);
//	                		File mydir = getApplicationContext().getDir("Doc", getApplicationContext().MODE_PRIVATE); //Creating an internal dir;
//	                		if (!mydir.exists())
//	                		{
//	                		     mydir.mkdirs();
//	                		}
//	                		String dirPath = mydir.getAbsolutePath();
//	                		try{
//	                			InputStream in = null;
//	                			OutputStream out = null;
//	                			AssetManager assetFile = getAssets();
////	                			in = assetFile.open(file.toString());
//	                			if(sourceFile.startsWith("/")){
//	                				in = assetFile.open(file.toString());
//	                			}else{
//	                				in = assetFile.open(sourceFile);
//	                			}
////	                			in = assetFile.open(a);
//	                			out = new FileOutputStream(dirPath+"/"+file.toString());
//	                			Utility.CopyStream(in, out);
////	                			OutputStream out2 = new FileOutputStream(abc+"/"+
//////	                					"_Seno_"+
////	                					file.toString());
////	                			Utility.encrypt(in,out2,"senoSunawar1235");
//	                		}catch(Exception e){
//	                			e.printStackTrace();
//	                		}
//
//	                	}
//	                    // This is a file
//	                    // TODO: add file name to an array list
//	                }
//	            }
//	        }
//	    } catch (IOException e) {
//	        return false;
//	    }
//	    return true;
//	}
	
//	private void deleteFromInternalStorage(String namafile){
//		String pathFolder = Utility.getInternalStorageDocPath();
//	    String[] files = getApplicationContext().getDir("Doc", getApplicationContext().MODE_PRIVATE).list();
//	    ArrayList<File> listdelete = new ArrayList<File>();
//
//	    for(String file:files){
//	    	if(file.contains(namafile)){
//	    		listdelete.add(new File(pathFolder,namafile+".pdf"));
//	    		listdelete.add(new File(pathFolder,namafile+".txt"));
//	    		break;
//	    	}
//	    }
//
//	    if(listdelete.size()>0){
//	    	for(File todelete : listdelete){
//	    		if(todelete.exists()){
//	    			todelete.delete();
//	    		}
//	    	}
//	    }
//	}
	
	public void deleteInternalStorageFolder(){
//		Utility.deleteFolderAll(new File(Utility.getInternalStorageDocPath()));
//		Utility.deleteAllFileInFolder(Utility.getInternalStorageDocPath());
		Utility.deleteAllFileInFolder(SplashScreen.this.getFilesDir().toString());
	}

	public void update(){
		try {
			PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String name = pinfo.versionName;

			int versiApk = (int) (Double.parseDouble(name) * 10000);

			Connection.DBopen(SplashScreen.this);
			String apkVersionFromExistingDB = Utility.getSetting(SplashScreen.this, "app_version", "");
			Recordset dataRecordset = Connection.DBquery("select value from global where code = 'app.version'");
			Vector<String> abc = dataRecordset.getRecordFromHeader("value");		
//			Toast.makeText(SplashScreen.this, "apk Version from shared preferences "+apkVersionFromExistingDB, Toast.LENGTH_LONG).show();
			if(abc.size()>0){
				apkVersionFromExistingDB = abc.get(0);	
			}			
//			Toast.makeText(SplashScreen.this, "apk Version from db "+apkVersionFromExistingDB, Toast.LENGTH_LONG).show();
//			Toast.makeText(SplashScreen.this, "apk version from application "+name, Toast.LENGTH_LONG).show();
//			apkVersionFromExistingDB = "1.10";


			int versiDb = 0;

			try{
                versiDb = (int) (Double.parseDouble(apkVersionFromExistingDB) * 10000);
            }catch (Exception e){

            }

			if(versiDb!=0&&versiApk > versiDb){
//				Toast.makeText(SplashScreen.this, "deleting file", Toast.LENGTH_LONG).show();
//				Toast.makeText(SplashScreen.this, "apk version from application "+name, Toast.LENGTH_LONG).show();
				DAFReciever.cancelAllBackgroundService(SplashScreen.this);
				Connection.DBclose();
				ConnectionBG.DBclose();
				Utility.deleteFolderAll(new File(Utility.getDefaultPath()));
				Utility.clearSetting(SplashScreen.this);
				deleteInternalStorageFolder();
				Utility.setSetting(getApplicationContext(), "DBVERSION", "");
				Utility.setSetting(SplashScreen.this, "importDataFromAsset", "true");
			}else{
				Connection.DBclose();
			}
			
			
//			if(apkVersionFromExistingDB.equalsIgnoreCase(name)||apkVersionFromExistingDB.equalsIgnoreCase("")){
////				Toast.makeText(SplashScreen.this, " tidak Copy file", Toast.LENGTH_LONG).show();
//				Connection.DBclose();
//
//			}else{
////				Toast.makeText(SplashScreen.this, "deleting file", Toast.LENGTH_LONG).show();
////				Toast.makeText(SplashScreen.this, "apk version from application "+name, Toast.LENGTH_LONG).show();
//				DAFReciever.cancelAllBackgroundService(SplashScreen.this);
//				Connection.DBclose();
//				ConnectionBG.DBclose();
//				Utility.deleteFolderAll(new File(Utility.getDefaultPath()));
//				Utility.clearSetting(SplashScreen.this);
//				deleteInternalStorageFolder();
//				Utility.setSetting(getApplicationContext(), "DBVERSION", "");
//				Utility.setSetting(SplashScreen.this, "importDataFromAsset", "true");
//			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}
	


	private void move_from_asset(){
		try {
			String[] list = getAssets().list("");
			Vector<String> blacklist = new Vector<>();
			blacklist.add("data.db");
			blacklist.add("images");
			blacklist.add("sounds");
			blacklist.add("webkit");
			blacklist.add("AutoRunTargetPermission.xml");
			blacklist.add("pskc_schema.xsd");
			for(String file : list){
				if(!blacklist.contains(file)){
					Utility.copyAsset(getApplicationContext(),file);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	class splsh extends AsyncTask<Void, Void, Void>{
		@Override
		protected Void doInBackground(Void... params) {
//			String chekInfo = Utility.getSetting(SplashScreen.this, "checkUpdate", "");
//			if(chekInfo!=""){
//				update();
//			}
			Utility.setSetting(SplashScreen.this, "checkUpdate", "true");
			update();
			String deviceId = Utility.getSetting(SplashScreen.this,"DeviceId","");
			if(deviceId == ""){
				Utility.setSetting(SplashScreen.this,"DeviceId",UUID.randomUUID().toString());
			}
			String abc = Utility.getSetting(SplashScreen.this, "importDataFromAsset", "");
			if(abc.equalsIgnoreCase("true")||abc == ""){
				Utility.setSetting(getApplicationContext(), "importDataFromAsset", "false");
//				Toast.makeText(getApplicationContext(), " Copy file", Toast.LENGTH_LONG).show();
//				listAssetFiles("");
				move_from_asset();
//				new moveFile1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);			
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			Connection.DBopen(SplashScreen.this);
			ConnectionBG.DBopen(SplashScreen.this);
			Connection.DBcreate("SETTING", "URL", "MAC_ADDRESS_PRINTER", "STORAGE");
			Connection.DBcreate("log_order", "order_id", "opened","sent","downloaded", "data");
			
//			Connection.DBcreate("IVERSION", "VERNAME", "VERNO", "LASTSYNC");
			
			Recordset data = Connection.DBquery("select * from SETTING");
			if (data.getRows() < 1) {
				Utility.setSetting(getApplicationContext(), "firstInstall", "");
			}
			
//			Utility.setSetting(getApplicationContext(), "firstInstall", "");
			
			PackageManager pm = getPackageManager();
			List<ApplicationInfo> ai = pm.getInstalledApplications(0);
			List<ApplicationInfo> installedApps = new ArrayList<ApplicationInfo>();
			for(ApplicationInfo app : ai) {
				
			    //checks for flags; if flagged, check if updated system app
			    if((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) == 1) {
			        installedApps.add(app);
			    //it's a system app, not interested
			    } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
			        //Discard this one
			    //in this case, it should be a user-installed app
			    } else {
			        installedApps.add(app);
			    }
			}
			
			for (int i = 0; i < installedApps.size(); i++) {
				ApplicationInfo app = installedApps.get(i);
				if (app.className != null) {
					if (app.className.equalsIgnoreCase("com.sophos.mobilecontrol.client.android.core.SmcApplication")) {
						sophosInstalled = true;
						break;
					}
				}
			}


//			Utility.getImeiOrUUID(SplashScreen.this);
			
//			if (!sophosInstalled) {
//				AdapterDialog.showAlertDialogFinish(SplashScreen.this, "Informasi", "MDM Agent di device anda belum tersedia, harap install MDM Agent terlebih dahulu");
//			}else {

			/**
			 * perubahan pada pengambilan imei di android 10
			 */
			/*ModelDeviceIdentity deviceIdentity = Utility.getImeiOrUUID(SplashScreen.this);
			if(deviceIdentity.getIsAndroid10()){
				if(deviceIdentity.getImei().length()>3){
					leaveSplashScreen();
				}else{
					AdapterDialog.showDialogOneBtn(SplashScreen.this,
							"warning",
							"Please insert simcard in device on slot one before using this application", "OK", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									finish();
									System.exit(0);
								}
							});
				}
			}else{
				leaveSplashScreen();
			}*/

			if (Global.getText("splash.active.state", "true").equals("true")) {
					String imgPath = Global.getText("splash.image.url");
					if (imgPath.contains("http")) {
						Utility.LoadImageDirect((ImageView)findViewById(R.id.imageView1), imgPath);	
					}
					
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							
//							if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
//								new RetrieveCSV(SplashScreen.this).execute();
//							}else{
								startActivity(new Intent(SplashScreen.this, LoginActivity.class));
								finish();
//							}
													
						}
					}, Integer.parseInt(Global.getText("splash.timer.amount", "5")) * 1000);
					
				}else {
//					if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
//						new RetrieveCSV(SplashScreen.this).execute();
//					}else{
						startActivity(new Intent(SplashScreen.this, LoginActivity.class));
						finish();
//					}
				}
//			}
			super.onPostExecute(result);
		}

		public void leaveSplashScreen(){
			if (Global.getText("splash.active.state", "true").equals("true")) {
				String imgPath = Global.getText("splash.image.url");
				if (imgPath.contains("http")) {
					Utility.LoadImageDirect((ImageView)findViewById(R.id.imageView1), imgPath);
				}

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {

//							if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
//								new RetrieveCSV(SplashScreen.this).execute();
//							}else{
						startActivity(new Intent(SplashScreen.this, LoginActivity.class));
						finish();
//							}

					}
				}, Integer.parseInt(Global.getText("splash.timer.amount", "5")) * 1000);

			}else {
//					if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
//						new RetrieveCSV(SplashScreen.this).execute();
//					}else{
				startActivity(new Intent(SplashScreen.this, LoginActivity.class));
				finish();
//					}
			}
		}
	}
}
