package com.daf.activity;

import java.util.UUID;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.database.Connection;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.service.SendPendingImage;
import com.salesforce.utility.Utility;

public class NewEntry extends Activity{
	private String table_name;

	private UUID idOne;
	private Spinner spnCoy, spnCustType, spnLob, spnPlatform;
	private boolean fromDafPameran = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_entry);
		
//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}
		
		//generate no order sementara di device
		idOne = UUID.randomUUID();	
		
		//seno 17:12 19/02/2019
		//untuk menghentikan backroud service SendPendingImage agar tidak mengirim saat ui di buka
//		Intent intent = new Intent("com.salesforce.service.SendPendingImage");
//		sendBroadcast(intent);
//		Intent intent2 = new Intent("com.salesforce.service.LostImageService");
//		sendBroadcast(intent2);
		//end
		
		table_name= Utility.getSetting(NewEntry.this, "tbl_matchingData", "");
		if(table_name!=null&&table_name!=""&&getIntent().getStringExtra("seno")==null){
//			Connection.DBupdate(table_name, "flag_dafpameran=N");
			Connection.DBdelete(table_name);
		}
		
		spnCoy = (Spinner)findViewById(R.id.coy);
		spnCustType = (Spinner)findViewById(R.id.cusType);
		spnLob = (Spinner)findViewById(R.id.bussType);
		spnPlatform = (Spinner)findViewById(R.id.flatform);		

		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		loadSpinner();

		
		((Button)findViewById(R.id.btnNext)).setOnClickListener(new OnClickListener() {
			
			@Override
			public synchronized void onClick(View arg0) {
				if (spnCoy.getSelectedItem() != null) {
					if (spnLob.getSelectedItem() != null && !spnLob.getSelectedItem().toString().equalsIgnoreCase("-- Select --")) {
						if (spnPlatform.getSelectedItem() != null) {
							settup();							
						}else
							AdapterDialog.showMessageDialog(NewEntry.this, "Informasi", "Platform tidak boleh kosong");
					}else
						AdapterDialog.showMessageDialog(NewEntry.this, "Informasi", "LOB tidak boleh kosong");
				}else
					AdapterDialog.showMessageDialog(NewEntry.this, "Informasi", "Coy Name tidak boleh kosong");
			}

		});
		
		spnCoy.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View v,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				loadSpinnerByCoyId(QueryClass.getCoyId(spnCoy.getSelectedItem().toString().toUpperCase()));				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
		
		loadFormDafPameran();
	}
	
	/**
	 * sebagai validasi pengecekan apkah ini data entry baru atau
	 * data yang tadi sudah dibuat
	 * (masih dalam menu new entry)
	 */
	private void settup(){
		String orderID = String.valueOf(idOne);
		orderID = orderID.substring(0, orderID.indexOf("-"));
		
		if(fromDafPameran == true){
			orderID = Generator.currOrderCode;
		}
		
		if (isSameDocument()) {
			String record = QueryClass.getDataDraft(orderID);
			if (!record.equalsIgnoreCase("")) {
				Generator.currOrderCode = orderID;
//				if(!fromDraft == true){
//					Generator.currOrderCode = orderID;
//				}
				Generator.currBufferedView = QueryClass.getDataPendingNView(orderID);
				setupValue();
				Generator.openGenerator(NewEntry.this, record, true);
			}else{
				openGenerator(orderID);
			}
			
		}else{
//			openGenerator(orderID);
			Generator.currOrderCode = orderID;
//			if(!fromDraft == true){
//				Generator.currOrderCode = orderID;
//			}
			setupValue();
			Recordset rst = Connection.DBquery("SELECT modelid FROM MST_COMBINATION_MODEL WHERE coy_id ='"+Generator.coyId+"' AND cust_type ='"+
					spnCustType.getSelectedItem().toString().toUpperCase()+"' AND buss_unit = '"+QueryClass.getBussUnit(spnLob.getSelectedItem().toString().toUpperCase())+"' AND platform ='" +
					QueryClass.getPlatformID(spnPlatform.getSelectedItem().toString().toUpperCase())+"' and flag_daf_pameran ='NP'");
			if (rst.getRows() > 0) {
				Connection.DBdelete(table_name);
				Recordset data = Connection.DBquery("SELECT * FROM model WHERE model_id ='" + rst.getText(0, "modelid") + "'");
				if (data.getRows() > 0) {
					Generator.currModel = rst.getText(0, 0);
					Generator.currOrderTable = rst.getText(0, 2);
					saveToDraft();
					Generator.openGenerator(NewEntry.this, Generator.currApplication, Generator.currModel, Generator.currOrderTable, Generator.currOrderCode);
					return;
				}else {
					AdapterDialog.showMessageDialog(NewEntry.this, "Informasi", "Maaf, Model tidak ada");
				}
			}else {
				AdapterDialog.showMessageDialog(NewEntry.this, "Informasi", "Maaf, Dokumen tidak ada");
			}
		}
	}
	
	/**
	 * 
	 * @param orderID
	 * untuk membuka generator yang berisi form2 sesuai dari bentukan service
	 */
	public void openGenerator(String orderID){
		Generator.currOrderCode = orderID;
//		if(!fromDraft == true){
//			Generator.currOrderCode = orderID;
//		}
		setupValue();
		Recordset rst = Connection.DBquery("SELECT modelid FROM MST_COMBINATION_MODEL WHERE coy_id ='"+Generator.coyId+"' AND cust_type ='"+
				spnCustType.getSelectedItem().toString().toUpperCase()+"' AND buss_unit = '"+QueryClass.getBussUnit(spnLob.getSelectedItem().toString().toUpperCase())+"' AND platform ='" +
				QueryClass.getPlatformID(spnPlatform.getSelectedItem().toString().toUpperCase())+"' and flag_daf_pameran ='NP'");
		if (rst.getRows() > 0) {
			Recordset data = Connection.DBquery("SELECT * FROM model WHERE model_id ='" + rst.getText(0, "modelid") + "'");
			if (data.getRows() > 0) {
				Generator.currModel = rst.getText(0, 0);
				Generator.currOrderTable = rst.getText(0, 2);
//				Generator.currOrderType = "newentry";
				saveToDraft();
				Generator.openGenerator(NewEntry.this, Generator.currApplication, Generator.currModel, Generator.currOrderTable, Generator.currOrderCode);
				return;
			}
		}else {
			AdapterDialog.showMessageDialog(NewEntry.this, "Informasi", "Maaf, Dokumen tidak ada");
		}
	}
	
	/**
	 * validasi pengecekan apakah ini merupakan dokument entry yang sama
	 * seperti sebelumnya
	 * 
	 * @return
	 */
	private boolean isSameDocument(){
		try {
			if (QueryClass.getCoyId(spnCoy.getSelectedItem().toString()).equalsIgnoreCase(Generator.coyId) && spnCustType.getSelectedItem().toString().equalsIgnoreCase(Generator.custtype) && 
					QueryClass.getBussUnit(spnLob.getSelectedItem().toString()).equalsIgnoreCase(Generator.bu) && QueryClass.getPlatformID(spnPlatform.getSelectedItem().toString()).equalsIgnoreCase(Generator.platform)) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}
	
	/**
	 * men-set nilai nilai yang akan digunaka untuk generator
	 */
	public void setupValue(){
		Generator.coyId = QueryClass.getCoyId(spnCoy.getSelectedItem().toString().toUpperCase());
		Generator.bu = QueryClass.getBussUnit(spnLob.getSelectedItem().toString().toUpperCase());
		Generator.platform = QueryClass.getPlatformID(spnPlatform.getSelectedItem().toString().toUpperCase());
		Generator.custtype = spnCustType.getSelectedItem().toString().toUpperCase();
		Generator.isNewEntry = true;
		Generator.freez = true;
		Generator.currAppMenu = "newentry";
		Generator.currOrderType = "newentry";
	}
	
	/**
	 * untuk mengisi nilai pada spinner
	 */
	private void loadSpinner(){
		Vector<String> allData = new Vector<String>();
		Recordset data = Connection.DBquery("SELECT DISTINCT coy_name FROM MST_COY_ID");
		if (data.getRows() > 0) {
			allData = data.getRecordFromHeader("coy_name");
			Utility.setAdapterSpinner(NewEntry.this, spnCoy, allData);
		}
		
		allData = new Vector<String>();
		allData.add("INDIVIDU");
		allData.add("CORPORATE");
		Utility.setAdapterSpinner(NewEntry.this, spnCustType, allData);
		
	}
	
	/**
	 * untuk mengisi nilai pada spinner berdasarkan coyid
	 */
	private void loadSpinnerByCoyId(String coyid){
		spnLob.setAdapter(null);
		spnPlatform.setAdapter(null);
		Vector<String> allData = new Vector<String>();
		allData.add("-- Select --");
		Recordset data = Connection.DBquery("SELECT buss_unit_desc FROM MST_BU WHERE coy_id='"+coyid+"'");
		for (int i = 0; i < data.getRows(); i++) {
			allData.add(data.getText(i, "buss_unit_desc"));
		}
//		if (data.getRows() > 0) {
//			allData = data.getRecordFromHeader("buss_unit_desc");
			Utility.setAdapterSpinner(NewEntry.this, spnLob, allData);
//		}
		
		allData = new Vector<String>();
		data = Connection.DBquery("SELECT platform_desc FROM MST_PLATFORM WHERE coy_id='"+coyid+"'");
		if (data.getRows() > 0) {
			allData = data.getRecordFromHeader("platform_desc");
			Utility.setAdapterSpinner(NewEntry.this, spnPlatform, allData);
		}
		
		
		if(getIntent().getStringExtra("seno")!=null){			
			spnLob.setSelection(getIndex(spnLob, QueryClass.getBussName(Generator.bu,Generator.coyId)),false);			
			spnPlatform.setSelection(getIndex(spnPlatform, QueryClass.getPlatformName(Generator.platform,Generator.coyId)),false);
		}
	}
	
	/**
	 * dialog yang digunaka sebagai pemberitahuan penyimpanan draft
	 */
	private void dialogDraft(){
		AlertDialog.Builder builder = new AlertDialog.Builder(NewEntry.this);
		builder.setCancelable(false);
		builder.setTitle("Informasi");
		builder.setMessage("Apakah anda ingin menyimpan ini sebagai Draft ?");
		builder.setCancelable(false);
		
		builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
//              seno 09:12 09/12/19
//				SendPendingImage.uiActive = false;
//				LostImageService.uiActive = false;
				if(fromDafPameran == true){
					Intent i = new Intent(NewEntry.this,DAFMenu.class);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}else{
					finish();
				}
				
			}
		});
		
		builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
				deleteFingerImg(Generator.currOrderCode);
//              seno 09:12 09/12/19
//				SendPendingImage.uiActive = false;
//				LostImageService.uiActive = false;
				if(fromDafPameran == true){
					Intent i = new Intent(NewEntry.this,DAFMenu.class);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}else{
					finish();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void deleteFingerImg(String orderCode){
		Recordset data = Connection.DBquery("select * from TBL_PATH_Finger where orderCode = '"+Generator.currOrderCode+"'");
		for (int i = 0; i < data.getRows(); i++) {
			Utility.deleteFile(data.getText(i, "path"));
		}
		Connection.DBdelete("TBL_PATH_Finger", "orderCode=?", new String[]{Generator.currOrderCode});
	}
	
	/**
	 * untuk menyimpan data kedalam tabel draft
	 */
	private void saveToDraft(){
		Connection.DBcreate("DRAFT", "orderCode", "user", "coyID", "custType", "bussUnit", "platFormID", "date","time", "data");
		Connection.DBinsert("DRAFT", "orderCode="+Generator.currOrderCode ,"user="+Utility.getSetting(NewEntry.this, "USERNAME", ""),"coyID="+QueryClass.getCoyId(spnCoy.getSelectedItem().toString().toUpperCase()),
				"custType="+spnCustType.getSelectedItem().toString().toUpperCase(), "bussUnit="+QueryClass.getBussUnit(spnLob.getSelectedItem().toString().toUpperCase()),
				"platFormID="+QueryClass.getPlatformID(spnPlatform.getSelectedItem().toString().toUpperCase()),"date="+Utility.Now(),"time="+Utility.getTime(),
				"data=");
	}
	
	
	private void loadFormDafPameran(){
		if(getIntent().getStringExtra("seno")!=null){
			String a = Generator.coyId;
//			String b = Generator.bu;
//			String c = Generator.platform;
//			String d = Generator.custtype;
//			String e = Generator.currOrderCode;
			fromDafPameran = true;
			spnCoy.setSelection(getIndex(spnCoy, QueryClass.getCoyName(Generator.coyId)),false);	
			loadSpinnerByCoyId(a);
			
			spnCustType.setSelection(getIndex(spnCustType, Generator.custtype),false);
//			spnCustType.setSelection(getIndex(spnCustType, "CORPORATE"),false);
			
//			String tmpLob= QueryClass.getBussName(Generator.bu,Generator.coyId);
//			int tmpindex =getIndex(spnLob, tmpLob);
//			spnLob.setSelection(tmpindex,true);			
//			String avv = spnLob.getSelectedItem().toString().toUpperCase();
			
//			spnPlatform.setSelection(getIndex(spnPlatform, QueryClass.getPlatformName(Generator.platform,Generator.coyId)),false);
//			spnPlatform.setSelection(getIndex(spnPlatform, QueryClass.getPlatformName("S",Generator.coyId)),false);
			settup();
		}
	}
	
	private int getIndex(Spinner spinner, String myString){
		int c = 0;
	     for (int i=0;i<spinner.getCount();i++){
	         if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
	             c=i;	  
	             break;
	         }
	     }

	     return c;
	 }
	
	@Override
	public void onBackPressed() {
		dialogDraft();
	}

}
