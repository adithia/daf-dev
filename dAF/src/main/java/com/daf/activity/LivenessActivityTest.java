package com.daf.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.database.Nset;
import com.salesforce.utility.ComparationImage;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import ai.advance.liveness.lib.LivenessResult;
import ai.advance.liveness.sdk.activity.LivenessActivity;

public class LivenessActivityTest extends Activity {
    private static final int REQUEST_CODE_LIVENESS = 2022;

    private ImageView img;
    private ImageView picturesss, picturesss2;
    private String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liveness_test);
        Button btnLiveness = findViewById(R.id.btnLiveness);

        img = findViewById(R.id.imageView);
        picturesss = (ImageView) findViewById(R.id.picturesss);
        picturesss2 = (ImageView) findViewById(R.id.picturesss2);

        btnLiveness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LivenessActivityTest.this, LivenessActivity.class);
                startActivityForResult(intent, REQUEST_CODE_LIVENESS);
            }
        });

        Button btnComparation = findViewById(R.id.btnComparation);

        btnComparation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Messagebox.showProsesBar(LivenessActivityTest.this, new Runnable() {
                            @Override
                            public void run() {
                                String path1 = "storage/emulated/0/Android/data/com.salesforce/files/img1.jpg";
                                String path2 = "storage/emulated/0/Android/data/com.salesforce/files/img2.jpg";
                                result = ComparationImage.comparationTwoImage2(path1, path2);
                            }
                        }, new Runnable() {
                            @Override
                            public void run() {
                                if (result != "Connection Timeout" && result != "") {
                                    Nset nData = Nset.readJSON(result);
                                    AdapterDialog.showMessageDialog(LivenessActivityTest.this, "Informasi", "kemiripan antara 2  gambar = " + nData.getData("data").getData("similarity").toString());
                                } else {
                                    AdapterDialog.showMessageDialog(LivenessActivityTest.this, "Informasi", result);
                                }
                            }
                        });
            }
        });


        picturesss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(LivenessActivityTest.this);
            }

        });

        picturesss2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage2(LivenessActivityTest.this);
            }

        });

    }

    private void selectImage(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImage2(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode){
                case 0:
                    if (requestCode == REQUEST_CODE_LIVENESS && resultCode == RESULT_OK) {
                        if (LivenessResult.isSuccess()) {
                            String livenessId = LivenessResult.getLivenessId();
                            Bitmap livenessBitmap = LivenessResult.getLivenessBitmap();
                            Utility.SaveBitmap2(livenessBitmap,"livenessImage.jpg");
                            img.setImageBitmap(livenessBitmap);
                            String errorMsg = LivenessResult.getErrorMsg();
                        }
                        break;
                    }
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                picturesss.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//                                picturesss.setImageBitmap(getPicture(data.getData()));
                                cursor.close();
                            }
                        }

                    }
                    break;
                case 2:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                picturesss2.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//                                picturesss.setImageBitmap(getPicture(data.getData()));
                                cursor.close();
                            }
                        }

                    }
                    break;

            }
        }
        if (requestCode == REQUEST_CODE_LIVENESS && resultCode == RESULT_OK) {
            if (LivenessResult.isSuccess()) {// liveness detection success
                String livenessId = LivenessResult.getLivenessId();// livenessId
                Bitmap livenessBitmap = LivenessResult.getLivenessBitmap();// liveness picture
                // } else {// liveness detection failed
                Utility.SaveBitmap2(livenessBitmap,"livenessImage.jpg");
                img.setImageBitmap(livenessBitmap);
                String errorMsg = LivenessResult.getErrorMsg();// failed reason
            }
        }
    }
}
