package com.daf.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.utility.Utility;
import com.salesforce.utility.WebHandler;

public class TrackingOrderTabelActivity extends Activity{
	
	private WebHandler handler;
	
	/**
	 * untuk data tracking order melalui webview
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.web_order_track);
		
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		((TextView) findViewById(R.id.textJudul)).setText("Tracking Order");
		
		WebView mWebView = (WebView)findViewById(R.id.web);
		
		
		mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSaveFormData(true);
        mWebView.setBackgroundColor(0);
        mWebView.getSettings().setBuiltInZoomControls(true);
        

//      mWebView.addJavascriptInterface(new WebAppInterface(TrackingOrder.this), "btl-modal");
       
//        mWebView.setWebChromeClient(new WebChromeClient());        
        mWebView.setWebChromeClient(new WebChromeClient(){  
      	  
        	public void onProgressChanged(WebView view, int progress){  
    			setProgress(progress * 1000);
        	}  
	       
	    }); 


        mWebView.setWebViewClient(new WebViewClient());
//        mWebView.setWebViewClient(new MyWebViewClient());
//        mWebView.loadUrl("http://m.kaskus.co.id/thread/52f7bc1f128b4601208b48ea"); 
        String url = Utility.getURLenc(Utility.getSetting(getApplicationContext(), "URL_TRACK", "")+ "daf/daf/nikita.zul?ui=TrackingOrderMobileList&"
        		+ "custName=", getIntent().getStringExtra("custName"),
        		"&tglLahirCust=",getIntent().getStringExtra("tglLahir"),
        		"&applNo=",getIntent().getStringExtra("noAPP"), 
        		"&suplName=", getIntent().getStringExtra("dealer"),
        		"&tglOrderFrom=",getIntent().getStringExtra("tglOrderFrom"),
        		"&tglOrderTo=",getIntent().getStringExtra("tglOrderTo"),
        		"&branchName=", getIntent().getStringExtra("cabang"), 
        		"&userId=",getIntent().getStringExtra("userId"),
        		"&userLogin=",Utility.getSetting(getApplicationContext(), "USERNAME", ""),"&isMobile=Y");
        mWebView.loadUrl(url); 
        handler = new WebHandler(TrackingOrderTabelActivity.this, mWebView);
//        mWebView.addJavascriptInterface(handler, "Android");
        mWebView.addJavascriptInterface(handler, "JsHandler");
        
	}
	

	
	/*private class MyWebViewClient extends WebViewClient { 
		public boolean shouldOverrideUrlLoading(WebView view, String url) { 
			view.loadUrl (url); 		
			return true;
		}
	 
		public void onLoadResource(WebView view, String url) {
			super.onLoadResource(view, url);
		}


	}*/
	/*public class WebAppInterface {
	    Context mContext;

	    //** Instantiate the interface and set the context *//*
	    WebAppInterface(Context c) {
	        mContext = c;
	    }
 
	    public void CallSub(String show, boolean b, String arg) {
	         Log.i("WebAppInterface","CallSub");
	         if (arg.trim().equals("PDF")) {
	        	  
	         }else if (!arg.trim().equals("Kota")) {
	        	 
		          
			 }	         
	    }
	}*/

}
