package com.daf.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.daf.activity.TestActivity.ListItem;
import com.salesforce.R;

public class TestActivity extends Activity {
 
    private String[] arrText =
            new String[]{"Text1","Text2","Text3","Text4"
            ,"Text5","Text6","Text7","Text8","Text9","Text10"
            ,"Text11","Text12","Text13","Text14","Text15"
            ,"Text16","Text17","Text18","Text19","Text20"
            ,"Text21","Text22","Text23","Text24"};
    private String[] arrTemp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lyt_listview_activity);
 
        arrTemp = new String[arrText.length];
        
        MyAdapter adapter = new MyAdapter();
        
        
//        MyListAdapter myListAdapter = new MyListAdapter();
        ListView listView = (ListView) findViewById(R.id.listViewMain);
        listView.setItemsCanFocus(true);
        listView.setAdapter(adapter);
    }
 
    private class MyListAdapter extends BaseAdapter{
 
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if(arrText != null && arrText.length != 0){
                return arrText.length;    
            }
            return 0;
        }
 
        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return arrText[position];
        }
 
        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
 
            //ViewHolder holder = null;
            final ViewHolder holder;
            if (convertView == null) {
 
                holder = new ViewHolder();
                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.lyt_listview_list, null);
                holder.textView1 = (TextView) convertView.findViewById(R.id.textView1);
                holder.editText1 = (EditText) convertView.findViewById(R.id.editText1);    
 
                convertView.setTag(holder);
 
            } else {
 
                holder = (ViewHolder) convertView.getTag();
            }
 
            holder.ref = position;
 
            holder.textView1.setText(arrText[position]);
            holder.editText1.setText(arrTemp[position]);
            holder.editText1.addTextChangedListener(new TextWatcher() {
 
                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
 
                }
 
                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                        int arg3) {
                    // TODO Auto-generated method stub
 
                }
 
                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    arrTemp[holder.ref] = arg0.toString();
                }
            });
 
            return convertView;
        }
 
        private class ViewHolder {
            TextView textView1;
            EditText editText1;
            int ref;
        }
 
 
    }
    
    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        public ArrayList<ListItem> myItems = new ArrayList<ListItem>();
 
        public MyAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for (int i = 0; i < 20; i++) {
                ListItem listItem = new ListItem();
                listItem.caption = "Caption" + i;
                myItems.add(listItem);
            }
            notifyDataSetChanged();
        }
 
        public int getCount() {
            return myItems.size();
        }
 
        public Object getItem(int position) {
            return position;
        }
 
        public long getItemId(int position) {
            return position;
        }
 
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder2 holder;
            if (convertView == null) {
                holder = new ViewHolder2();
                convertView = mInflater.inflate(R.layout.lyt_listview_list, null);
                holder.captionEdt = (EditText) convertView
                        .findViewById(R.id.ItemCaption);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder2) convertView.getTag();
            }
            //Fill EditText with the value you have in data source
            holder.captionEdt.setText(myItems.get(position).caption);
            holder.captionEdt.setId(position);
 
            //we need to update adapter once we finish with editing
            holder.captionEdt.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (!hasFocus){
                      final int position = v.getId();
                      final EditText Caption = (EditText) v;
                      myItems.get(position).caption = Caption.getText().toString();
                  }
				}
			});
//            setOnFocusChangeListener(new OnFocusChangeListener() {
//                public void onFocusChange(View v, boolean hasFocus) {
//                    if (!hasFocus){
//                        final int position = v.getId();
//                        final EditText Caption = (EditText) v;
//                        myItems.get(position).caption = Caption.getText().toString();
//                    }
//                }
//            });
 
            return convertView;
        }
    }
 
    class ViewHolder2 {
        EditText captionEdt;
    }
 
    class ListItem {
        String caption;
    }

}
