package com.daf.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.SendAction;
import com.salesforce.stream.Model;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

public class TaskListDataActivity extends Activity implements OnItemClickListener, Refresh {
    private ListView listview;
    private Spinner spinner;
    private Vector<String> prior = new Vector<String>();
    private Vector<String> vFields = new Vector<String>();
    private Vector<Vector<String>> vValue = new Vector<Vector<String>>();
    private String activityStatusString = "";
    private String iconStatusString = "";
    private ProgressDialog pD;

    private static void insertRecievedDate(String orderNew, String orderOld) {
        Vector<String> vOrderNew = Utility.splitVector(orderNew, ",");
        Vector<String> vOrderOld = Utility.splitVector(orderOld, ",");
        Connection.DBopen();
        if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
            for (int i = 0; i < vOrderNew.size(); i++) {
                if (!Utility.isContainValue(vOrderNew.elementAt(i), vOrderOld)) {
                    Connection.DBupdate("trn_task_list", "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(i));
                }
            }
        } else {
            for (int i = 0; i < vOrderNew.size(); i++) {
                Connection.DBupdate("trn_task_list", "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(i));
            }
        }
    }

    private static void insertRecievedDate2(String orderNew, String orderOld) {


        Vector<String> vOrderNew = Utility.splitVector(orderNew, "|");
        Vector<String> vOrderOld = Utility.splitVector(orderOld, "|");
        Connection.DBopen();
        for (int i = 0; i < vOrderNew.size(); i++) {
            String table_name = vOrderNew.get(i).substring(0, vOrderNew.get(i).indexOf("="));
//			Vector<String> tmpVOrderNew = Utility.splitVector(vOrderNew.get(i).substring(vOrderNew.get(i).indexOf("=")+1), ",");
//			Vector<String> tmpVOrderold = Utility.splitVector(vOrderNew.get(i).substring(vOrderNew.get(i).indexOf("=")+1), ",");

//			System.out.println(tmpVOrderNew+"\n"+tmpVOrderold);
//				Connection.DBopen();
            if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
                for (int j = 0; j < vOrderNew.size(); j++) {
                    if (!Utility.isContainValue(vOrderNew.elementAt(j), vOrderOld)) {
                        Connection.DBupdate(table_name, "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(j));
                    }
                }
            } else {
                for (int j = 0; j < vOrderNew.size(); j++) {
                    Connection.DBupdate(table_name, "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(j));
                }
            }
        }
    }

    //untuk mendelete tasklist yg expired
    private static void deleteExpiredTasklit(String table) {
        Recordset data = Connection.DBquery("SELECT order_id, expired_date FROM " + table);
        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {
                if (!data.getText(i, "expired_date").equalsIgnoreCase("")) {
                    Calendar cal = convertTasklits(data.getText(i, "expired_date"));
                    Date today = new Date();
                    if (today.after(cal.getTime())) {
                        Connection.DBdelete(table, "order_id=?", new String[]{data.getText(i, "order_id")});
                    }
                }
            }
        }
    }

    public static Calendar convertTasklits(String strDate) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH);
            date = sdf.parse(strDate);
            calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_task_list_data);
        ((TextView) findViewById(R.id.textJudul)).setText("Tasklist Data");
        ((ImageView) findViewById(R.id.imageView1)).setImageResource(android.R.drawable.ic_menu_revert);
        ((ImageView) findViewById(R.id.imageView2)).setImageResource(android.R.drawable.ic_popup_sync);
//		((ImageView)findViewById(R.id.imageView2)).setVisibility(View.GONE);
        listview = (ListView) findViewById(R.id.listTasklistData);
        spinner = (Spinner) findViewById(R.id.spnTasklistData);
        ((ImageView) findViewById(R.id.imageView1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        deleteTaklist();

        populateIconStatus();

        populateActivityStatus();

        setupSpinner();

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                loadData(spinner.getSelectedItem().toString().substring(0, spinner.getSelectedItem().toString().indexOf("[")).trim());
//				new loadData().execute(spn.getSelectedItem().toString().substring(0, spn.getSelectedItem().toString().indexOf("[")).trim());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        listview.setOnItemClickListener(this);

        ((ImageView) findViewById(R.id.imageView2)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                forceGetTasklist();
            }
        });

    }

    private void setupSpinner() {
        prior = Utility.splitVector(Global.getText("order." + Generator.currApplication + ".list"), "|");
        if (prior.size() > 1) {
            ((LinearLayout) findViewById(R.id.container_priority_tasklist_data)).setVisibility(View.VISIBLE);
            Vector<String> viewPriority = new Vector<String>();
            for (int i = 0; i < prior.size(); i++) {
                int row = 0;
                String param = Utility.getDataSplit(prior.elementAt(i), ";", false);
                String query = Global.getText("order." + Generator.currApplication + ".list_data." + param.toLowerCase());
                Recordset data = Connection.DBquery(query);
                for (int j = 0; j < data.getRows(); j++) {
                    if (!processActivityStatusHide(data.getText(j, 0))) {
                        row++;
                    }
                }
                viewPriority.addElement(param + " [ " + row + " ]");
            }
            Utility.setAdapterSpinner(TaskListDataActivity.this, (Spinner) findViewById(R.id.spnTasklistData), viewPriority);
        }
    }

    /**
     * @param orderId
     * @return boolean
     * <p>
     * memerikan nilai balikan true jika data tasklist harus disembunyikan
     * karena sudah diproses kirim
     */

    private boolean processActivityStatusHide(String orderId) {
        if (Global.getText("order." + Generator.currApplication + ".activities").equals("1")) {
            if (activityStatusString.indexOf(orderId) >= 0) {
                return true;
            }

        }
        return false;
    }

    /**
     * @param param data from spinner
     *              mengumpulkan semua data tasklist berdasarkan settingan global param
     */
    private void loadData(final String param) {
        vFields.removeAllElements();
        vValue.removeAllElements();
        Messagebox.showProsesBar(TaskListDataActivity.this, new Runnable() {
            @Override
            public void run() {

                String query = Global.getText("order." + Generator.currApplication + ".list_data." + param.toLowerCase()) + " " + Global.getText("order." + Generator.currApplication + ".list." + param.toLowerCase() + ".sort");
                Recordset data = Connection.DBquery(query);

                //mendapatkan query untuk mendapatkan r=key dan value dari data order
                String queryDataView = Global.getText("order." + Generator.currApplication + ".list_data.data.view");

                //loop data untuk mendapatkan key dan value data order
                for (int i = 0; i < data.getRows(); i++) {
                    String orderId = data.getText(i, 0);

                    if (!processActivityStatusHide(data.getText(i, 0))) {
                        if (!QueryClass.isOndataPending(orderId)) {
                            if (!QueryClass.isSent(orderId)) {
                                String qr = queryDataView + "'" + orderId + "'";
                                Recordset dataOrder = Connection.DBquery(qr);
                                if (dataOrder.getRows() > 0) {
                                    vFields = dataOrder.getAllHeaderVector();
                                    Vector<Vector<String>> dataValue = dataOrder.getAllDataVector();
                                    vValue.add(dataValue.elementAt(0));
                                }
                            }
                        }
                    }
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (vFields.size() > 0 && vValue.size() > 0) {
                    setuplist(vValue, vFields);
                } else {
                    listview.setAdapter(null);
                }
            }
        });
    }

    /**
     * untuk menampilkan list dari tasklist
     */
    private void setuplist(Vector<Vector<String>> data, final Vector<String> header) {
        AdapterConnector.setConnector(listview, data, R.layout.orderadapter, new AdapterInterface() {

            @SuppressWarnings("unchecked")
            @Override
            public View onMappingColumn(int row, View v, Object data) {
                // TODO Auto-generated method stub
                final Vector<String> all = (Vector<String>) data;
                LinearLayout container = (LinearLayout) v.findViewById(R.id.container_order_view);
                int maxShow = Global.getInt("max.order.show");
                try {

                    if (all.size() <= maxShow) {
                        for (int i = 0; i < all.size(); i++) {
                            View view = AdapterConnector.setInflateLinier(TaskListDataActivity.this, R.layout.iteminflate);
                            ((TextView) view.findViewById(R.id.txtField)).setText(header.elementAt(i).toString());
                            ((TextView) view.findViewById(R.id.txtValue)).setText(all.elementAt(i).toString());
                            container.addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                        }
                    } else {
                        for (int i = 0; i < maxShow; i++) {
                            View view = AdapterConnector.setInflateLinier(TaskListDataActivity.this, R.layout.iteminflate);
                            ((TextView) view.findViewById(R.id.txtField)).setText(header.elementAt(i).toString());
                            ((TextView) view.findViewById(R.id.txtValue)).setText(all.elementAt(i).toString());
                            container.addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                        }
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }

                v.setTag(all);
                processIconStatus(all.elementAt(0).toString(), v);
                return v;
            }
        });
    }

    /**
     * untuk mengubah icon pada list tasklist
     */
    private void processIconStatus(String orderId, View view) {
        ImageView img = (ImageView) view.findViewById(R.id.img_icon);

        if (iconStatusString.indexOf(orderId) >= 0)
            img.setImageResource(R.drawable.icon_read_data);
        else {
            img.setImageResource(R.drawable.icon_new_data);
        }
    }

    @Override
    public void Refresh() {
        // TODO Auto-generated method stub
        if (listview != null) {
            listview.setAdapter(null);
            populateIconStatus();
            populateActivityStatus();
            loadData(spinner.getSelectedItem().toString().substring(0, spinner.getSelectedItem().toString().indexOf("[")).trim());
        }
    }

    /**
     * get Order Id From Database
     *
     * @param data
     * @return Vector<String>
     * fungsi ketika salah satu list dipilih dan menampilkan form dari generator
     */
//	@SuppressWarnings("unchecked")
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Vector<String> dataAll = (Vector<String>) arg1.getTag();
        final String orderId = dataAll.get(0).toString();

        Generator.isTasklist = true;
        Generator.currAppMenu = "tasklistdata";
        Generator.freez = true;
        Generator.applno = orderId;//all.get(1).toString();
        Generator.branchid = dataAll.get(16).toString();
        Generator.currOrderCode = orderId;
        Generator.bu = dataAll.get(4).toString();
        Generator.currOrderType = "tasklistdata";
        Utility.activity = TaskListDataActivity.this;

        if (isReaded(orderId)) {
            String record = QueryClass.getDataLogOrder(orderId);
            if (record.equalsIgnoreCase("")) {
                loadForm(orderId, arg2);
            } else {
                Generator.openGenerator(TaskListDataActivity.this, record, true);
            }
        } else {
//			Recordset rst = Connection.DBquery("SELECT model_id,model_name, model_order_tablename FROM model WHERE app_id='" + Generator.currApplication + "' ");
            loadForm(orderId, arg2);

        }
    }

    /**
     * untuk mengetahui apakah list tertentu sudah pernah dibuka
     */
    private boolean isReaded(String orderId) {
        if (iconStatusString.indexOf(orderId) >= 0) {
            return true;
        }
        return false;
    }

    private void loadForm(String orderId, int pos) {
//		Recordset rst = Connection.DBquery("SELECT model_id,model_name, model_order_tablename FROM model WHERE model_id='M10"+Generator.currApplication+"'");
        Recordset rst2 = Connection.DBquery("SELECT coy_id,cust_type_desc,buss_unit,platform from trn_tasklist_data where order_id='" + orderId + "'");
//		String a = rst2.getText(0, "coy_id");
        Recordset rst3 = Connection.DBquery("SELECT modelid "
                + "FROM MST_COMBINATION_MODEL WHERE coy_id ='" + rst2.getText(0, "coy_id") + "' AND cust_type ='" +
                rst2.getText(0, "cust_type_desc") + "' AND buss_unit = "
                + "'" + rst2.getText(0, "buss_unit") + "' AND platform ='" +
                rst2.getText(0, "platform") + "' and flag_daf_pameran ='NP'");

        Recordset rst4 = Connection.DBquery("SELECT model_order_tablename FROM MODEL WHERE model_id = '" + rst3.getText(0, 0) + "';");

        Vector<String> header = new Vector<String>();
        Vector<Vector<String>> fields = new Vector<Vector<String>>();
        header.add(rst4.getHeader(0));
        for (int i = 0; i < rst4.getRows(); i++) {
            String namaTbl = rst4.getText(i, 0);
            if (namaTbl.contains(",")) {
                Vector<String> tmp = Utility.splitVector(namaTbl, ",");
                for (int j = 0; j < tmp.size(); j++) {
                    fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
                }
            } else {
                fields.add(new Vector<String>(Arrays.asList(namaTbl)));
            }

        }
        rst4 = new Model(header, fields);

        readStatus(orderId);
        if (rst3.getRows() > 0) {
            Generator.coyId = rst2.getText(0, 0);
            Generator.bu = rst2.getText(0, 2);
            Generator.platform = rst2.getText(0, 3);
            Generator.custtype = rst2.getText(0, 1);
            Generator.currBufferedView = getBufferedView(pos);
            Generator.currModel = rst3.getText(0, 0);
            Generator.currOrderTable = rst4.getText(0, 0);
            Generator.openGenerator(TaskListDataActivity.this, Generator.currApplication, Generator.currModel, Generator.currOrderTable, Generator.currOrderCode);
            return;
        }
    }

    /**
     * untuk menyimpan data list kedalam bentuk json
     */
    private String getBufferedView(int position) {
        try {
            Nset nset = Nset.newArray();
            Vector<String> all = vValue.elementAt(position);
            if (all != null) {
                for (int j = 0; j < vFields.size(); j++) {
                    String label = vFields.elementAt(j).toString();
                    String value = all.elementAt(j).toString();
                    nset.addData(Nset.newArray().addData(label).addData(value));
                }
            }
            return nset.toJSON();
        } catch (Exception e) {
        }
        return "";
    }

    /**
     * untuk mengubah icon pada tampilan list trn_task_list
     */
    private void populateIconStatus() {
        StringBuffer sbBuffer = new StringBuffer();
        Recordset rst = Connection.DBquery("select order_id from log_order;");
        for (int i = 0; i < rst.getRows(); i++) {
            sbBuffer.append(rst.getText(i, 0)).append("|");
        }
        iconStatusString = sbBuffer.toString();
    }

    /**
     * untuk mengambil semua data tasklist yang sudah dikirm keserver
     * untuk kemudian tidak ditampilkan didalam list trn_task_list
     */
    private void populateActivityStatus() {
        StringBuffer sbBuffer = new StringBuffer();
        if (Global.getText("order." + Generator.currApplication + ".activities").equals("1")) {
            Recordset rst = Connection.DBquery("select orderCode from data_activity WHERE appId = '" + Generator.currApplication + "';");
            for (int i = 0; i < rst.getRows(); i++) {
                sbBuffer.append(rst.getText(i, 0)).append("|");
            }
        }
        activityStatusString = sbBuffer.toString();
    }

    /**
     * untuk mengisi tabel log_order, data mana saja yang pernah dibuka
     */
    private void readStatus(String orderId) {
        Recordset rst1 = Connection.DBquery("select order_id from log_order where order_id = '" + orderId + "'");
        ContentValues cv = new ContentValues();
        cv.put("opened", Utility.getDate());
        cv.put("order_id", orderId);

        if (rst1.getRows() == 0)
            Connection.DBinsert("log_order", cv);
        else {
            Recordset rst2 = Connection.DBquery("select opened from log_order where order_id = '" + orderId + "'");
            if (rst2.getText(0, 0) == null) {
                Connection.DBupdate("log_order", cv, "order_id = ?", new String[]{orderId});
            }
        }
    }

    private void deleteTaklist() {
//		Recordset rst = Connection.DBquery("SELECT model_order_tablename FROM model WHERE model_id='M10"+Generator.currApplication+"'");
        Recordset rst = Connection.DBquery("select distinct model_order_tablename from model where model_order_tablename <>'trn_task_list'");
//		25/04/2019 seno daf3
        Vector<String> header = new Vector<String>();
        Vector<Vector<String>> fields = new Vector<Vector<String>>();
        header.add(rst.getHeader(0));
        for (int i = 0; i < rst.getRows(); i++) {
            String namaTbl = rst.getText(i, 0);
            if (namaTbl.contains(",")) {
                Vector<String> tmp = Utility.splitVector(namaTbl, ",");
                for (int j = 0; j < tmp.size(); j++) {
                    fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
                }
            } else {
                fields.add(new Vector<String>(Arrays.asList(namaTbl)));
            }

        }
        rst = new Model(header, fields);
//		25/04/2019 seno selesai
        if (rst.getRows() > 0) {
            String tabel = rst.getText(0, "model_order_tablename");
            Recordset rec = Connection.DBquery("SELECT sent, order_id FROM log_order");
            for (int i = 0; i < rec.getRows(); i++) {
                if (rec.getText(i, "sent").equalsIgnoreCase("Y")) {
                    Connection.DBdelete(tabel, "order_id=?", new String[]{rec.getText(i, "order_id")});
                    Connection.DBdelete("log_order", "order_id=?", new String[]{rec.getText(i, "order_id")});
                    ;
                }
            }
        }
    }

    private void forceGetTasklist() {
        ConnectionBG.DBopen(this);
        getOrder();
    }

    private void getOrder() {
//		Nset order = Nset.newObject();
//		StringBuffer sbuBuffer = new StringBuffer();
//		Recordset rst = ConnectionBG.DBquery("select distinct model_order_tablename from model;");
//		for (int i = 0; i < rst.getRows(); i++) {
//			Recordset x = ConnectionBG.DBquery("select order_id from "+rst.getText(i, 0));
//
//			for (int j = 0; j < x.getRows(); j++) {
//				sbuBuffer.append(j>0?",":"").append(x.getText(j, 0));
//			}
//			order.setData(rst.getText(i, 0), sbuBuffer.toString());
//
//			if (!Utility.getSetting(getApplicationContext(), "USERNAME", "").equals( Utility.getSetting(getApplicationContext(), "LASTUSER", ""))) {
//				ConnectionBG.DBdelete(rst.getText(i, 0));
//				ConnectionBG.DBdelete("log_order");
//				ConnectionBG.DBcreate("log_order", "order_id", "opened","sent","downloaded","data");
//				SendAction.deleteSentActity(0);
//
//			}
//		}

        new download().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void downloadOrder() {

        String tble = "TABLE_LIST_ITEM";
        Nset newMaster = Nset.newArray();

        String url = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=", Utility.getSetting(getApplicationContext(), "userid", "").trim(), "&master=",
                tble, "&imei=", Utility.getImei(getApplicationContext()).toUpperCase());
        String file = Utility.getDefaultPath(tble + ".m2m");

        try {
            if (Utility.getNewToken()) {
                if (!tble.equalsIgnoreCase("")) {
                    Utility.getHttpConnection(url, file);
                    Recordset ri = Stream.downStreamJSON(new InputStreamReader(new FileInputStream(file)));
                    if (ri.getCols() == 0 && ri.getRows() == 0) {
                    } else if (ri.getText(0, "status").equalsIgnoreCase("ERR")) {
                        Utility.copyFile(file, "/storage/sdcard0/DAF/" + tble + ".m2m");
                    } else {
                        ConnectionBG.DBdelete(tble);
                        Nset dataNewMaster = Nset.newObject();
                        dataNewMaster.setData("version_name", tble);
                        dataNewMaster.setData("filename", file);
                        newMaster.addData(dataNewMaster);
                    }

                }
            }
        } catch (Exception e) {
        }

        for (int i = 0; i < newMaster.getArraySize(); i++) {

            Recordset ri = null;
            try {

                ri = Stream.downStreamJSON(new InputStreamReader(new FileInputStream(newMaster.getData(i).getData("filename").toString())));
                Syncronizer.saveToDBBG(newMaster.getData(i).getData("version_name").toString(), ri);
                String sdsd = (newMaster.getData(i).getData("filename").toString());
                Utility.deleteFile((newMaster.getData(i).getData("filename").toString()));

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        Nset order = Nset.newObject();
        StringBuffer sbuBuffer = new StringBuffer();
        StringBuffer newSbuBuffer = new StringBuffer();            //seno daf 3 10:30 12/04/2019
        Recordset rst = ConnectionBG.DBquery("select distinct model_order_tablename from model;");

//		25/04/2019 seno daf3
        Vector<String> header = new Vector<String>();
        Vector<Vector<String>> fields = new Vector<Vector<String>>();
        header.add(rst.getHeader(0));
        for (int i = 0; i < rst.getRows(); i++) {
            String namaTbl = rst.getText(i, 0);
            if (namaTbl.contains(",")) {
                Vector<String> tmp = Utility.splitVector(namaTbl, ",");
                for (int j = 0; j < tmp.size(); j++) {
                    fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
                }
            } else {
                fields.add(new Vector<String>(Arrays.asList(namaTbl)));
            }

        }
        rst = null;
        rst = new Model(header, fields);
//		25/04/2019 seno selesai

        for (int i = 0; i < rst.getRows(); i++) {
            Recordset x = ConnectionBG.DBquery("select order_id from " + rst.getText(i, 0));
            //seno 13:31 23/04/2019 daf3
            if (sbuBuffer.length() > 0) {
                sbuBuffer.delete(0, sbuBuffer.length());
            }
            newSbuBuffer.append(newSbuBuffer.length() > 0 ? "|" : "").append(rst.getText(i, 0) + "=");            //seno daf 3 10:30 12/04/2019
            for (int j = 0; j < x.getRows(); j++) {
                sbuBuffer.append(j > 0 ? "," : "").append(x.getText(j, 0));
                newSbuBuffer.append(j > 0 ? "," : "").append(x.getText(j, 0));            //seno daf 3 10:30 12/04/2019
            }
            order.setData(rst.getText(i, 0), sbuBuffer.toString());

            if (!Utility.getSetting(getApplicationContext(), "USERNAME", "").equals(Utility.getSetting(getApplicationContext(), "LASTUSER", ""))) {
                ConnectionBG.DBdelete(rst.getText(i, 0));
                ConnectionBG.DBdelete("log_order");
                ConnectionBG.DBcreate("log_order", "order_id", "opened", "sent", "downloaded", "data");
                SendAction.deleteSentActity(0);

            }
        }

        SendAction.deleteSentActity(0, Utility.Now().substring(0, 10));
        String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "orderservlet/?action=nikita&order=", order.toJSON(), "&userid=", Utility.getSetting(this, "userid", ""), "&flg=", "service");

        if (Utility.getNewToken()) {

            sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/" + "f=service", new String[]{"action", "order", "userid", "flg", "token"},
                    new String[]{"nikita", order.toJSON(), Utility.getSetting(this, "userid", ""), "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});//"&versi="  ,SettingActivity.APP_VERSION

//			Nset orderOld = order;
            order = Nset.readJSON(sUrl);

            boolean breceived = false;
            boolean newTasklist = false, isInsert = false;

            for (int i = 0; i < order.getArraySize(); i++) {
                String table = order.getData(i).getData("table").toString();
                for (int j = 0; j < order.getData(i).getData("data").getArraySize(); j++) {
                    String code = order.getData(i).getData("data").getData(j).getData("code").toString();
                    String drst = order.getData(i).getData("data").getData(j).getData("data").toString();
                    Recordset xxx = Stream.downStream(drst);
                    if (code.equals("truncate")) {
                        Syncronizer.saveToDBBG(table, xxx);
                    } else if (code.equals("insert")) {
                        if (xxx.getRows() > 0) {
                            newTasklist = true;
                        }
                        Syncronizer.createToDBBG(table, xxx);
                        Syncronizer.insertToDBBG(table, xxx);
                    } else if (code.equals("delete")) {
                        Syncronizer.createToDBBG(table, xxx);
                        Syncronizer.deleteToDBBG(table, xxx);
                    } else if (code.equals("update")) {
                        Syncronizer.createToDBBG(table, xxx);
                        Syncronizer.deleteToDBBG(table, xxx);
                        Syncronizer.insertToDBBG(table, xxx);
                    }
                    deleteExpiredTasklit(table);
                    if (xxx.getRows() >= 1) {
                        breceived = true;
                    }
                }

            }

            Recordset tabel = null;
            StringBuffer orderNew = new StringBuffer();
            StringBuffer orderNew2 = new StringBuffer();            //seno daf 3 10:30 12/04/2019
            for (int i = 0; i < rst.getRows(); i++) {
                tabel = Connection.DBquery("select order_id from " + rst.getText(i, 0));
                orderNew2.append(orderNew2.length() > 0 ? "|" : "").append(rst.getText(i, 0) + "=");            //seno daf 3 10:30 12/04/2019
                for (int j = 0; j < tabel.getRows(); j++) {
                    orderNew.append(j > 0 ? "," : "").append(tabel.getText(j, 0));
                    orderNew2.append(j > 0 ? "," : "").append(tabel.getText(j, 0));            //seno daf 3 10:30 12/04/2019
                }
            }

//			if (isInsert) {
//				Utility.orderNew = orderNew.toString();
//				Utility.orderOld = sbuBuffer.toString();
//				insertRecievedDate(orderNew.toString(), sbuBuffer.toString());
//			}
            //seno 13:31 23/04/2019 daf3
            insertRecievedDate2(orderNew2.toString(), newSbuBuffer.toString());            //seno daf 3 10:30 12/04/2019

//			if (breceived) {
//				Nset order_id = Nset.newObject();
//				order_id.setData("trn_task_list", sbuBuffer.toString());
//				if (Utility.getNewToken()) {
//					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
//							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
//				}else{
//					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
//							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
//				}
//
//
//				if (newTasklist) {
//					setNotificationAlarm("Tasklist baru berhasil didownload");
//				}
//			}


            //seno daf 3 10:30 12/04/2019
            if (breceived) {
                Nset order_id = Nset.newObject();
                Vector<String> vSBuffer = Utility.splitVector(newSbuBuffer.toString(), "|");
                for (int i = 0; i < vSBuffer.size(); i++) {
                    String table = vSBuffer.get(i).substring(0, vSBuffer.get(i).indexOf("="));
                    String data = vSBuffer.get(i).substring(vSBuffer.get(i).indexOf("=") + 1);
                    order_id.setData(table, data);
                }
                if (Utility.getNewToken()) {
                    sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[]{"action", "order", "userid", "flg", "token"},
                            new String[]{"RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", ""), "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
                } else {
                    sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[]{"action", "order", "userid", "flg", "token"},
                            new String[]{"RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", ""), "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
                }

                if (newTasklist) {
//						setNotificationAlarm("Tasklist baru berhasil didownload");
                }

            }
        }
//			Utility.backgroundOrder = false;

    }

    private class download extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            pD = Utility.showProgresbar(TaskListDataActivity.this, "Download data");
            pD.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            downloadOrder();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (pD.isShowing()) {
                pD.dismiss();
                setupSpinner();
            }

            super.onPostExecute(result);
        }

    }


}
