package com.daf.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.service.SchedulerService;
import com.salesforce.service.SendPendingImage;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

//import com.google.android.gms.internal.ca;

public class ImagePendingActivity extends Activity {

    String str = "";
    private ListView list;
    // list yang tidak boleh di kirim
    private Vector<String> vAct = new Vector<String>();
    private BroadcastReceiver brodcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            vAct.clear();
            ((ListView) findViewById(R.id.listView1)).setAdapter(null);
            getAllActivityPending();
        }
    };

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 5;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.datapending);

        // if (Global.getText("service.malam", "").equals("0")) {
        // new Logout(this);
        // }

        //untuk menghentikan backroud service SendPendingImage agar tidak mengirim saat ui di buka
        Intent intent = new Intent("com.salesforce.service.SendPendingImage");
        sendBroadcast(intent);
//		Intent intent2 = new Intent("com.salesforce.service.LostImageService");
//		sendBroadcast(intent2);
//        Intent intent1 = new Intent("com.salesforce.service.SchedulerService");
//        sendBroadcast(intent1);
        // 17/12/2018
//        registerReceiver(brodcast, new IntentFilter(
//                SchedulerService.BROADCAST_ACTION));

        ((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);

        ((TextView) findViewById(R.id.textJudul)).setText("Image Pending");

        list = (ListView) findViewById(R.id.listView1);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                sendImage((String[]) v.getTag(), pos);

            }
        });

        getAllActivityPending();
    }

//	private void LoadDataForlostImage(String[] datas,List<String[]> container,String imgPathOrTmpPath){
//		for (int i = 0; i < tmpFiles.length; i++) {
//			if (!fileNotAllowed.contains(tmpFiles[i])) {
//				// membandingkan antara data pada folder Images dengan data pada
//				// daftar terlarang
//				if (tmpFiles[i].indexOf("imgicon.png") == -1) {
//					if (!vAct2.contains(tmpFiles[i])) {
//						String activityIdFromTmp = tmpFiles[i].substring(0,
//								tmpFiles[i].indexOf("."));
//						if (!vAct.contains(activityIdFromTmp)) {
//							String useridplusplus = tmpFiles[i].substring(0,
//									lengSubstring);
//							if (!listBlockFromLogOrder.contains(useridplusplus)) {
//								String namaFile = tmpFiles[i];
//								String path = Utility.getDefaultTempPath()
//								// + "/"
//										+ namaFile;
//								String type = "";
//								if (namaFile.contains("T")) {
//									type = "S";
//								} else if (namaFile.contains("FP")) {
//									String biomatric = namaFile.substring(namaFile.lastIndexOf("_"),namaFile.length());
//									if(biomatric.contains("FP")){
//										type = "B";
//									}else{
//										type = "F";
//									}
//								} else if (namaFile.contains("jpg")) {
//									type = "P";
//								} else {
//									type = "P";
//								}
//								try {
//									String[] strs = {
//											namaFile.substring(0, 25), "",
//											UUID.randomUUID().toString(), type,
//											"", namaFile, path, "",
//											Utility.getDateDAF() };
//									dataDraft.add(strs);
//								} catch (Exception e) {
//									// TODO: handle exception
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//	}

    private void getAllActivityPending() {
        Recordset dataRecordset = ConnectionBG
                .DBquery("select activityId from data_activity where status = '0'");
        vAct = dataRecordset.getRecordFromHeader("activityid");

        Recordset rstDraft = ConnectionBG.DBquery("select * from DRAFT;");
        // mengambil id pada table draft
        for (int i = 0; i < rstDraft.getRows(); i++) {
            NfData data = new NfData(rstDraft.getText(i, "data"));
            String tmpActivityId = data.getData("INIT", "ID");
            if(tmpActivityId !=null && tmpActivityId!=""){
                vAct.add(tmpActivityId);
            }
            /*Nset nset = new Nset(data.getInternalObject());
            String[] form = nset.getObjectKeys();

            for (int j = 0; j < form.length; j++) {
                if (form[j].equalsIgnoreCase("[INIT]")) {
                    String[] comp = nset.getData(form[j]).getObjectKeys();
                    for (int k = 0; k < comp.length; k++) {
                        String header = form[j];
                        try {
                            // unutk menghilangkan []
                            header = header.substring(1, header.length() - 1);
                        } catch (Exception e) {
                        }
                        if (comp[k].equalsIgnoreCase("ID")) {
                            final String val = data.getText(header, comp[k]);
                            if (!val.equalsIgnoreCase("")) {
                                // menambahkan id ke dalam daftar
                                vAct.add(val);
                            }
                        }
                    }
                }
            }*/
        }

        loadData();
    }

    /**
     * untuk mengumpulkan data dari tabel
     */
    private void loadData() {
        List<String[]> dataDraft = new ArrayList<String[]>();
        Recordset data = Connection
                .DBquery("SELECT * FROM LOG_IMAGE WHERE userId ='"
                        + Utility.getSetting(getApplicationContext(), "userid",
                        "") + "'");
        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {
                // if (data.getText(i, "data").equalsIgnoreCase("")) {
                // Connection.DBdelete("LOG_IMAGE", "activityId=?", new
                // String[]{data.getText(i, "activityId")});
                // }else{
                try {
                    if (!vAct.contains(data.getText(i, "activityid"))) {
                        String[] strs = {data.getText(i, "activityid"), // 0
                                data.getText(i, "applno"),// 1
                                // 17/12/2018
                                data.getText(i, "reqid"),// 2
                                // 17/12/2018
                                data.getText(i, "type"), // 3
                                data.getText(i, "istasklist"),// 4
                                data.getText(i, "imgname"),// 5
                                data.getText(i, "path"),// 6
                                data.getText(i, "userid"),// 7
                                data.getText(i, "tgl")};// 8
                        dataDraft.add(strs);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                // }
            }
        }

        // seno 01/02/2019
        // tambahin image yg ada di folder tmp,images yang tidak ada log image
        // nya dan tidak ada pemiliknya

        // ambil semua data di folder images
        String[] imagesFiles = new File(Utility.getDefaultImagePath()).list();
        // ambil semua data di folder tmp
        String[] tmpFiles = new File(Utility.getDefaultTempPath()).list();
        // signature png, picture jpg, finger png, FP
        ArrayList<String> tmpArrayFile = new ArrayList<String>();// /

        Vector<String> vAct2 = data.getRecordFromHeader("imgName");
        //ambil data pada table path_finger
        Recordset fingerRecordset = ConnectionBG
                .DBquery("select path from TBL_PATH_FINGER");
        for (String a : fingerRecordset.getRecordFromHeader("path")) {
            vAct2.add(a.substring(a.lastIndexOf("/")));
        }
        // membuat list dan memasukan file2 yang tidak boleh dikirim
        Vector<String> fileNotAllowed = new Vector<String>();
        fileNotAllowed.add("finger-nikita");
        fileNotAllowed.add("sign-nikita");
        fileNotAllowed.add("finger-nikita.imgicon.png");
        fileNotAllowed.add("viewImg.png");

        //mendaftarkan file pada folder sign dan mendaftarkan pada list
        String[] signCses = new File(Utility.getDefaultSign()).list();
        for (String Sgin : signCses) {
            fileNotAllowed.add(Sgin + ".png");
        }

        //mebuat list untuk tidak di kirim dari table log_order
        Recordset rstLogOrder = ConnectionBG.DBquery("select * from LOG_ORDER;");
        Vector<String> orderIdFromLogOrder = rstLogOrder.getRecordFromHeader("order_id");
//        String userid = Utility.getSetting(ImagePendingActivity.this, "userid", "").trim();
        /*Vector<String> listBlockFromLogOrder = new Vector<String>();
        int lengSubstringForTasklis = 0;

        if (orderIdFromLogOrder.size() > 0) {
            lengSubstringForTasklis = userid.length() + orderIdFromLogOrder.get(0).length();
        } else {
            lengSubstringForTasklis = userid.length() + 11;
        }

        for (int i = 0; i < orderIdFromLogOrder.size(); i++) {
            listBlockFromLogOrder.add(userid + orderIdFromLogOrder.get(i));
        }*/

        //mebuat list untuk tidak di kirim dari table trn_tasklit_offline
        Recordset rstTasklistOffline = ConnectionBG.DBquery("select * from trn_tasklist_offline;");
        Vector<String> orderIdFromTasklistOffline = rstTasklistOffline.getRecordFromHeader("orderId");
        /*Vector<String> listBlockFromTasklistOffline = new Vector<String>();

        int lengSubstringForTasklisOffline = 0;

        if (orderIdFromTasklistOffline.size() > 0) {
            lengSubstringForTasklisOffline = userid.length() + orderIdFromTasklistOffline.get(0).length();
        } else {
            lengSubstringForTasklisOffline = userid.length() + 8;
        }

        for (int i = 0; i < orderIdFromTasklistOffline.size(); i++) {
            listBlockFromTasklistOffline.add(userid + orderIdFromTasklistOffline.get(i));
        }*/

//		membuat list dari folder images yang akan di tampilkan dalam UI beserta validasi
        for (int i = 0; i < imagesFiles.length; i++) {
            if (!fileNotAllowed.contains(imagesFiles[i])) {
//				if (imagesFiles[i].indexOf("imgicon.png") == -1) {
                if (!imagesFiles[i].contains("imgicon.png")) {
                    if (!imagesFiles[i].contains(".png.png")) {
                        // membandingkan antara data pada folder Images dengan data
                        // pada daftar terlarang
                        if (!vAct2.contains(imagesFiles[i])) {
                            String activityIdFromImages = "not alowed";
                            try {
                                activityIdFromImages = imagesFiles[i].substring(
                                        0, imagesFiles[i].indexOf("."));
                            } catch (Exception e) {
                                activityIdFromImages = "not alowed";
                                continue;
                            }
                            if (!vAct.contains(activityIdFromImages)) {
//                                seno 10:54 09/12/2019
                                /*String useridplusplus = "error";
                                try {
                                    useridplusplus = imagesFiles[i].substring(0, lengSubstringForTasklis);
                                } catch (Exception e) {
                                    useridplusplus = "error";
                                    continue;
                                }*/
                                boolean continue_check1 = true;
                                for (int j = 0; j < orderIdFromLogOrder.size(); j++) {
                                    if (imagesFiles[i].contains(orderIdFromLogOrder.get(j))){
                                        continue_check1 = false;
                                        break;
                                    }
                                }
//                                if (!listBlockFromLogOrder.contains(useridplusplus)) {
//                                seno 10:54 09/12/2019
                                if (continue_check1) {
//                                seno 10:54 09/12/2019
                                    /*try {
                                        useridplusplus = imagesFiles[i].substring(0, lengSubstringForTasklisOffline);
                                    } catch (Exception e) {
                                        useridplusplus = "error";
                                        continue;
                                    }*/
                                    boolean continue_check2 = true;
                                    for (int k = 0; k < orderIdFromTasklistOffline.size(); k++) {
                                        if (imagesFiles[i].contains(orderIdFromTasklistOffline.get(k))){
                                            continue_check2 = false;
                                            break;
                                        }
                                    }
//                                seno 10:54 09/12/2019
//                                    if (!listBlockFromTasklistOffline.contains(useridplusplus)) {
                                    if (continue_check2) {
                                        String namaFile = imagesFiles[i];
                                        String path = Utility.getDefaultImagePath()
                                                + "/" + namaFile;
                                        String type = "";
                                        if (namaFile.contains("T")) {
                                            type = "S";
                                        } else if (namaFile.contains("FP")) {
                                            String biomatric = namaFile.substring(namaFile.lastIndexOf("_"), namaFile.length());
                                            if (biomatric.contains("FP")) {
                                                type = "B";
                                            } else {
                                                type = "F";
                                            }
                                        } else if (namaFile.contains("jpg")) {
                                            type = "P";
                                        } else {
                                            type = "P";
                                        }
                                        try {
                                            String[] strs = {
                                                    namaFile.substring(0, 25), "",
                                                    UUID.randomUUID().toString(), type,
                                                    "", namaFile, path, "",
                                                    Utility.getDateDAF()};
                                            dataDraft.add(strs);
                                        } catch (Exception e) {
                                            // TODO: handle exception
                                            e.printStackTrace();
                                        }
                                    }

                                }

                            }
                        }
                    }

                }
            }
        }
//		membuat list dari folder tmp yang akan di tampilkan dalam UI beserta validasi
        for (int i = 0; i < tmpFiles.length; i++) {
            if (!fileNotAllowed.contains(tmpFiles[i])) {
                // membandingkan antara data pada folder Images dengan data pada
                // daftar terlarang
				/*
				if (tmpFiles[i].indexOf("imgicon.png") == -1) {
				*/
                if (!tmpFiles[i].contains("imgicon.png")) {
                    if (!tmpFiles[i].contains(".png.png")) {
                        if (!vAct2.contains(tmpFiles[i])) {
                            String activityIdFromTmp = "not alowed";
                            try {
                                activityIdFromTmp = tmpFiles[i].substring(0,
                                        tmpFiles[i].indexOf("."));
                            } catch (Exception e) {
                                activityIdFromTmp = "not alowed";
                                continue;
                            }
                            if (!vAct.contains(activityIdFromTmp)) {
//                                seno 11:01 09/12/2019
                              /*  String useridplusplus = "error";
                                try {
                                    useridplusplus = tmpFiles[i].substring(0, lengSubstringForTasklis);
                                } catch (Exception e) {
                                    useridplusplus = "error";
                                    continue;
                                }*/

                                boolean continue_check1 = true;
                                for (int j = 0; j < orderIdFromLogOrder.size(); j++) {
                                    if (tmpFiles[i].contains(orderIdFromLogOrder.get(j))){
                                        continue_check1 = false;
                                        break;
                                    }
                                }
//                                seno 11:01 09/12/2019
//                                if (!listBlockFromLogOrder.contains(useridplusplus)) {
                                if (continue_check1) {
//                                seno 11:01 09/12/2019
                                  /*  try {
                                        useridplusplus = tmpFiles[i].substring(0, lengSubstringForTasklisOffline);
                                    } catch (Exception e) {
                                        useridplusplus = "error";
                                        continue;
                                    }*/

                                    boolean continue_check2 = true;
                                    for (int k = 0; k < orderIdFromTasklistOffline.size(); k++) {
                                        if (tmpFiles[i].contains(orderIdFromTasklistOffline.get(k))){
                                            continue_check2 = false;
                                            break;
                                        }
                                    }
//                                seno 11:01 09/12/2019
//                                    if (!listBlockFromTasklistOffline.contains(useridplusplus)) {
                                    if (continue_check2) {
                                        String namaFile = tmpFiles[i];
                                        String path = Utility.getDefaultTempPath()
                                                // + "/"
                                                + namaFile;
                                        String type = "";
                                        if (namaFile.contains("T")) {
                                            type = "S";
                                        } else if (namaFile.contains("FP")) {
                                            String biomatric = namaFile.substring(namaFile.lastIndexOf("_"), namaFile.length());
                                            if (biomatric.contains("FP")) {
                                                type = "B";
                                            } else {
                                                type = "F";
                                            }
                                        } else if (namaFile.contains("jpg")) {
                                            type = "P";
                                        } else {
                                            type = "P";
                                        }
                                        try {
                                            String[] strs = {
                                                    namaFile.substring(0, 25), "",
                                                    UUID.randomUUID().toString(), type,
                                                    "", namaFile, path, "",
                                                    Utility.getDateDAF()};
                                            dataDraft.add(strs);
                                        } catch (Exception e) {
                                            // TODO: handle exception
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // seno 01/02/2019
        setuplist(dataDraft);
    }

    /**
     * untuk menampikan data kedalam list
     */
    private void setuplist(List<String[]> data) {
        AdapterConnector.setConnector(list, data, R.layout.draftpendinginflate,
                new AdapterInterface() {

                    @Override
                    public View onMappingColumn(int row, View v, Object data) {
                        // TODO Auto-generated method stub
                        try {
                            String[] test = (String[]) data;
                            ((TextView) v.findViewById(R.id.orderCode))
                                    .setText(test[1]);
                            ((TextView) v.findViewById(R.id.date))
                                    .setText(test[3]);
                            if (test[3].equalsIgnoreCase("S")) {
                                ((TextView) v.findViewById(R.id.date))
                                        .setText("Tanda Tangan");
                            } else if (test[3].equalsIgnoreCase("P")) {
                                ((TextView) v.findViewById(R.id.date))
                                        .setText("Foto");
                            } else if (test[3].equalsIgnoreCase("F")) {
                                ((TextView) v.findViewById(R.id.date))
                                        .setText("Finger Print");
                            } else if (test[3].equalsIgnoreCase("B")) {
                                ((TextView) v.findViewById(R.id.date))
                                        .setText("Biometric");
                            }
                            ((TextView) v.findViewById(R.id.time))
                                    .setText(test[8]);
                            if (!test[3].equalsIgnoreCase("B")) {
                                Utility.OpenImage(
                                        (ImageView) v.findViewById(R.id.icon),
                                        test[6], R.drawable.ic_launcher);
                                // setupImage(test[5],
                                // (ImageView)v.findViewById(R.id.icon));
                            }
                            v.setTag(test);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                        return v;
                    }
                });
    }

    private void setupImage(String path, ImageView img) {
        File imgFile = new File(path);

        if (imgFile.exists()) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            // BitmapFactory.decodeResource(getResources(), path, options);
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

            // int imageHeight = options.outHeight;
            // int imageWidth = options.outWidth;
            // String imageType = options.outMimeType;

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, 50, 50);

            options.inJustDecodeBounds = false;
            // options.inPreferredConfig = Config.ARGB_8888;
            // options.inDither = true;
            Bitmap myBitmap = BitmapFactory.decodeFile(
                    imgFile.getAbsolutePath(), options);

            img.setImageBitmap(myBitmap);

        }
    }

    private void sendImage(final String[] dt, final int pos) {
        Messagebox.showProsesBar(ImagePendingActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Recordset dataBlackList = Connection
                        .DBquery("SELECT * FROM LOG_IMAGE WHERE userId ='"
                                + Utility.getSetting(getApplicationContext(),
                                "userid", "") + "'");
                String url = "";
                String path = "";
                path = dt[6];
                String reqId = "";
                reqId = dt[2];

                // if (new File(path).exists()) {

                String imgname = dt[5];
                if (imgname.contains(".jpg")) {
                } else if (imgname.contains(".png")) {
                } else {
                    imgname = imgname + ".";
                }
//				File abc = new File(path);
//				if(abc.exists()){
//					System.out.println("ada");
//				}else{
//					System.out.println("Tidak ada");
//				}

                Vector<String> blacklist2 = new Vector<String>();

                blacklist2.addAll(dataBlackList.getRecordFromHeader("imgname"));
                //untuk membedakan proses kirim jika, file tidak ada di table_lost Image
                // maka akan di kirim melali upload image servlet
                if (blacklist2.contains(dt[5])) {
                    // kirim jalur normal
                    if (Utility.getNewToken()) {
                        if (dt[4].equalsIgnoreCase("Y")) {
                            str = Utility.getHttpConnection(Utility.getURLenc(
                                    SettingActivity.URL_SERVER
                                            + "imagecheckservlet/?"
                                            + (true ? "f=service&" : "")
                                            + "applno=", dt[1], "&modul=",
                                    "tasklist", "&imagename=", imgname,
                                    "&token=", Utility.getSetting(
                                            Utility.getAppContext(), "Token",
                                            "")// ));
                                    , "&reqId=", reqId));
                        } else {
                            str = Utility.getHttpConnection(Utility.getURLenc(
                                    SettingActivity.URL_SERVER
                                            + "imagecheckservlet/?"
                                            + (true ? "f=service&" : "")
                                            + "imagename=", imgname, "&token=",
                                    Utility.getSetting(Utility.getAppContext(),
                                            "Token", "")// ));
                                    , "&reqId=", reqId));
                        }

                    }

                    String dsffd = str;
                    if (str.equalsIgnoreCase("NO")) {

                        if (Utility.getNewToken()) {

                            if (dt[4].equalsIgnoreCase("Y")) {
                                str = Utility.postHttpConnection(
                                        SettingActivity.URL_SERVER
                                                + "uploadservlet/?userid="
                                                + dt[7]
                                                + "&applno="
                                                + dt[1]
                                                + "&modul=tasklist"
                                                + "&imei="
                                                + Utility.getImei(Utility
                                                .getAppContext())
                                                + "&token="
                                                + Utility.getSetting(
                                                Utility.getAppContext(),
                                                "Token", ""), null,
                                        imgname, path, reqId);
//								String check = str;
//								System.out.println(check);
                            } else {
                                str = Utility.postHttpConnection(
                                        SettingActivity.URL_SERVER
                                                + "uploadservlet/?userid="
                                                + dt[7]
                                                + "&imei="
                                                + Utility.getImei(Utility
                                                .getAppContext())
                                                + "&token="
                                                + Utility.getSetting(
                                                Utility.getAppContext(),
                                                "Token", ""), null,
                                        imgname, path, reqId);
//								String check = str;
//								System.out.println(check);
                            }

                            String stris = str;
                            if (str.equalsIgnoreCase("OK")) {
                                ConnectionBG.DBdelete("LOG_IMAGE",
                                        " imgName =? ", new String[]{dt[5]});
                                new File(Utility.getDefaultPath(dt[5]))
                                        .delete();
                                new File(Utility.getDefaultTempPath(dt[5]))
                                        .delete();
                                new File(Utility.getDefaultImagePath(dt[5]))
                                        .delete();
                            }
                        }

                    } else if (str.equalsIgnoreCase("FOUND")) {
                        ConnectionBG.DBdelete("LOG_IMAGE", " imgName =? ",
                                new String[]{dt[5]});
                        new File(Utility.getDefaultPath(dt[5])).delete();
                        new File(Utility.getDefaultTempPath(dt[5])).delete();
                        new File(Utility.getDefaultImagePath(dt[5])).delete();

                    } else {
                        str = "gagal";
                    }
                    // }

                } else {
                    // kirim lewat uploadservlet
                    if (Utility.getNewToken()) {
                        String urll = SettingActivity.URL_SERVER
                                + "uploadimageservlet/?"
                                + "&token="
                                + Utility.getSetting(Utility.getAppContext(),
                                "Token", "");
                        str = Utility.postHttpConnection(urll, null, imgname,
                                path, reqId);
//						String check = str;
//						System.out.println(check);
                    }
                    if (str.equalsIgnoreCase("OK")) {
                        new File(path).delete();
                        // Recordset fingerRecordset =
                        // ConnectionBG.DBquery("select activityId from TBL_PATH_FINGER");
                        // Vector<String> checkpathFinger =
                        // fingerRecordset.getRecordFromHeader("path");
                        // if(checkpathFinger.contains(path)){
                        // Connection.DBdelete("TBL_PATH_Finger", " path =? ",
                        // new String[] {path} );
                        // }
                    }

                }

                // if (Utility.getNewToken()) {
                // if (dt[4].equalsIgnoreCase("Y")) {
                // str = Utility.getHttpConnection(Utility.getURLenc(
                // SettingActivity.URL_SERVER
                // + "imagecheckservlet/?"
                // + (true ? "f=service&" : "")
                // + "applno=", dt[1], "&modul=",
                // "tasklist", "&imagename=", imgname, "&token=",
                // Utility.getSetting(Utility.getAppContext(),
                // "Token", "")// ));
                // , "&reqId=", reqId));
                // } else {
                // str = Utility.getHttpConnection(Utility.getURLenc(
                // SettingActivity.URL_SERVER
                // + "imagecheckservlet/?"
                // + (true ? "f=service&" : "")
                // + "imagename=", imgname, "&token=",
                // Utility.getSetting(Utility.getAppContext(),
                // "Token", "")// ));
                // , "&reqId=", reqId));
                // }
                //
                // }
                //
                // String dsffd = str;
                // if (str.equalsIgnoreCase("NO")) {
                //
                // if (Utility.getNewToken()) {
                //
                // if (dt[4].equalsIgnoreCase("Y")) {
                // str = Utility.postHttpConnection(
                // SettingActivity.URL_SERVER
                // + "uploadservlet/?userid="
                // + dt[7]
                // + "&applno="
                // + dt[1]
                // + "&modul=tasklist"
                // + "&imei="
                // + Utility.getImei(Utility
                // .getAppContext())
                // + "&token="
                // + Utility.getSetting(
                // Utility.getAppContext(),
                // "Token", ""), null,
                // imgname, path, reqId);
                // } else {
                // str = Utility.postHttpConnection(
                // SettingActivity.URL_SERVER
                // + "uploadservlet/?userid="
                // + dt[7]
                // + "&imei="
                // + Utility.getImei(Utility
                // .getAppContext())
                // + "&token="
                // + Utility.getSetting(
                // Utility.getAppContext(),
                // "Token", ""), null,
                // imgname, path, reqId);
                // }
                //
                // String stris = str;
                // if (str.equalsIgnoreCase("OK")) {
                // ConnectionBG.DBdelete("LOG_IMAGE", " imgName =? ",
                // new String[] { dt[5] });
                // new File(Utility.getDefaultPath(dt[5])).delete();
                // new File(Utility.getDefaultTempPath(dt[5]))
                // .delete();
                // new File(Utility.getDefaultImagePath(dt[5]))
                // .delete();
                // }
                // }
                //
                // } else if (str.equalsIgnoreCase("FOUND")) {
                // ConnectionBG.DBdelete("LOG_IMAGE", " imgName =? ",
                // new String[] { dt[5] });
                // new File(Utility.getDefaultPath(dt[5])).delete();
                // new File(Utility.getDefaultTempPath(dt[5])).delete();
                // new File(Utility.getDefaultImagePath(dt[5])).delete();
                //
                // } else {
                // str = "gagal";
                // }
                // // }

            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (str.equalsIgnoreCase("gagal")
                        || str.equalsIgnoreCase("ERROR")) {
                    showMessageDialog(ImagePendingActivity.this, "Informasi",
                            "Maaf, gambar gagal dikirim");
                } else if (str.equalsIgnoreCase("OK")
                        || str.equalsIgnoreCase("FOUND")) {
                    showMessageDialog(ImagePendingActivity.this, "Informasi",
                            "Berhasil !");
                }
            }
        });
    }

    @SuppressWarnings({"deprecation"})
    private void showDialog(final String act, String message,
                            final String[] data, final int pos) {
        AlertDialog dialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Informasi");
        dialogBuilder.setMessage(message);

        dialog = dialogBuilder.create();

        dialog.setButton2("Tidak", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        dialog.setButton3("Kirim Gambar",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                        sendImage(data, pos);
                    }
                });

        dialog.show();
    }

    public void showMessageDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                refreshOrder();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void refreshOrder() {
        vAct.clear();
        ((ListView) findViewById(R.id.listView1)).setAdapter(null);
        getAllActivityPending();
    }

    public void onBackPressed() {
        SendPendingImage.uiActive = false;
//        SchedulerService.uiActive = false;
        // LostImageService.uiActive = false;
        finish();
    }

    ;

    // protected void onDestroy() {
    // unregisterReceiver(brodcast);
    // };

}
