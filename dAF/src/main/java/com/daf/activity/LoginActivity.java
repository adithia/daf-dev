package com.daf.activity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.daf.archieve.ExtractArchive;
import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.SendAction;
import com.salesforce.service.DAFReciever;
import com.salesforce.service.GetDownloadRar;
import com.salesforce.service.GetServiceData;
import com.salesforce.stream.ImagePost;
import com.salesforce.stream.Model;
import com.salesforce.stream.Stream;
import com.salesforce.utility.GPSTracker;
import com.salesforce.utility.InternalStorageLeft;
import com.salesforce.utility.LocationOffline;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

/**
 * @author lenovo
 * <p>
 * Class login digunakan sebagai interface user, dimana dalam kelas ini
 * user yang akan masuk kedalam aplikasi harus bisa melewati validasi yang sudah ditentukan
 * dalam flow bisnis
 */
public class LoginActivity extends Activity {
    private static boolean MD5HASH = false;
    private Recordset data;
    final static int GPS = 1, Bluetooth = 2, Wifi = 3, AppVersion = 4, certificate = 5, token = 6;
    private boolean logintrue = false, hasSigning = false, passSign = false, changeBranch, isCertExist;
    private EditText edtUsername, edtPassword;
    private String strImei, verGlobal, verCabang, verGen, verTable;
    private String strFileName, IP_Address;
    private String namaFileRar = "";
    private String namaFileRarPDf = "";
    public static String strPathImg = "", message, jsonToken;
    public static String newTokenWay;
    //	private Hashtable<String, Recordset> tblOld = new Hashtable<String, Recordset>();
    private Vector<String> tbleUpdated = new Vector<String>();
    public static boolean offline = false;

    @SuppressWarnings("static-access")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login2);

        new LocationOffline(getApplicationContext());
//      13:45 19/06/2019
//		File file = getApplicationContext().getFilesDir();
//		String a = Utility.getExternalPath();
//		ImageLoader imgLoad = ImageLoader.getInstance();

        // digunakan untuk mendapatkan ip address mobile
        if (Utility.cellularNetworkDataActive(getApplicationContext())) {
            IP_Address = Utility.getLocalIpAddress();
        } else {
            IP_Address = Utility.getIpAddressWifi(getApplicationContext());
        }

//		String dfsd = getApplicationContext().getFilesDir().getPath() + "/";

        edtUsername = (EditText) findViewById(R.id.editUsername);
        edtPassword = (EditText) findViewById(R.id.editPassword);
//        String a = Utility.getImeiOrUUID(this).getImei().toUpperCase();
		strImei = Utility.getImei(getApplicationContext()).toUpperCase();
//        strImei = a;

        edtUsername.addTextChangedListener(fieldListener(true));
        edtPassword.addTextChangedListener(fieldListener(false));

        if (Utility.isExit) {
            finish();
            Utility.setExit(false);
            return;
        }

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            SettingActivity.APP_VERSION = pInfo.versionName;
            Utility.setSetting(getApplicationContext(), "app_version", SettingActivity.APP_VERSION);
        } catch (Exception e) {
        }

        SettingActivity.URL_SERVER = SettingActivity.getProductionUrl();// Connection.DBquery("select * from SETTING ").getText(0, "URL");
        SettingActivity.URL_STORAGE = Connection.DBquery("select * from SETTING ").getText(0, "STORAGE");

        if (SettingActivity.URL_SERVER.trim().equals("")) {
            startActivity(new Intent(LoginActivity.this, SettingActivity.class));
        }

        //set enable for setting
        if (Global.getText("login.setting.active", "1").equals("1")) {
            ((ImageView) findViewById(R.id.btnSetting)).setEnabled(true);
        } else {
            ((ImageView) findViewById(R.id.btnSetting)).setEnabled(false);
        }

        //setting value text for the last login
        if (Utility.getSetting(getApplicationContext(), "REMEMBER", "").equals("TRUE")) {
            ((CheckBox) findViewById(R.id.checkRemember)).setChecked(true);
            edtUsername.setText(Utility.getSetting(getApplicationContext(), "USERNAME", ""));
            edtPassword.setText("");
        } else if (Utility.getSetting(getApplicationContext(), "REMEMBER", "").equals("FALSE")) {
            ((CheckBox) findViewById(R.id.checkRemember)).setChecked(false);
            edtUsername.setText("");
            edtPassword.setText("");
        }

        Connection.DBcreate("trn_mobile_process_file", "zip_file_name", "txt_file_name", "flag_download", "flag_extract", "start_time_d", "end_time_d", "start_time_e", "end_time_e");
//		Connection.DBdeleteDAF("MST_USER_MARKETING", "p_id=?", new String[]{"CS1010002"});

        ((ImageView) findViewById(R.id.btnLogin)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
//				if (isCertExist) {
                if (SettingActivity.URL_SERVER != null || !SettingActivity.URL_SERVER.equalsIgnoreCase("")) {
                    if (!Utility.isTimeToOff()) {
                        if (Utility.isTimeToLogin()) {
                            tbleUpdated.clear();
                            changeBranch = false;
                            validation();
                        } else {
                            AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Login tidak sesuai jam server");
                        }
                    } else {
                        AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, service saat ini tidak dapat digunakan");
                    }
                } else {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, Anda belum mengisi alamat URL server");
                }
//				}else{
//					AdapterDialog.goingToWeb(LoginActivity.this, Uri.parse("http://google.com"), "Informasi", "Maaf, Anda belum install FIF-Certificate\n\n silahkan Install");
//				}
            }
        });

        ((ImageView) findViewById(R.id.btnSetting)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                startActivity(new Intent(LoginActivity.this, SettingActivity.class));
            }
        });

        //set scale for the checkbox
        final float scale = this.getResources().getDisplayMetrics().density;
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkRemember);
        checkBox.setPadding(checkBox.getPaddingLeft() + (int) (10.0f * scale + 0.5f),
                checkBox.getPaddingTop(),
                checkBox.getPaddingRight(),
                checkBox.getPaddingBottom());

//		//hapus semua activityid yang statusnya 1
        Connection.DBquery("delete from data_activity where status = '1'");

        ((ImageView) findViewById(R.id.btnTaskListOff)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                offline = true;
                Generator.isLogin = false;
                Global.newInit();
                Intent intent = new Intent(LoginActivity.this, TaskListOfflineActivity.class);
                intent.putExtra("tasklistOffline", "offline");
//				startActivityForResult(new Intent(LoginActivity.this, TaskListOfflineActivity.class), 101);
                startActivityForResult(intent, 101);

            }
        });

    }

    //for get the max lenght of the input username and password
    private TextWatcher fieldListener(final boolean isUsername) {
        return new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                String message;
                if (isUsername) {
                    message = "Panjang Username maximal 50 karakter";
                } else {
                    message = "Panjang Password maximal 50 karakter";
                }
                if (s.length() >= 50) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", message);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }

    /**
     * methode yang digunakan untuk validasi awal
     * yaitu username dan password tidak boleh kosong
     */

    private String status = "";

    private void checkFileRar() {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                Recordset data = Connection.DBquery("select * from trn_mobile_process_file where flag_download = 'Y' and flag_extract = 'N'");
                if (data.getRows() > 0) {
                    for (int i = 0; i < data.getRows(); i++) {
                        try {
                            new ExtractArchive().extractArchive(Utility.getDefaultPath(data.getText(i, "zip_file_name")), Utility.getDefaultPath());
                            if (new File(Utility.getDefaultPath(data.getText(i, "txt_file_name"))).exists()) {

                                Connection.DBupdate("trn_mobile_process_file",
                                        "flag_download=Y", "flag_extract=Y", "start_time_e=" + Utility.getTime(), "where ", "zip_file_name=" + namaFileRar + ".rar");

                                setProsesBarMsg("Please Wait \r\nExtract file");
                                status = Utility.readDirectJsonDAF(Utility.getDefaultPath(namaFileRar + ".txt"));

                            } else {
                                status = "nofile";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (status.equalsIgnoreCase("ok")) {
                    validation();
                } else if (status.equalsIgnoreCase("failed")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Extract file gagal");
                } else if (status.equalsIgnoreCase("nofile")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "File tidak ada");
                }
            }
        });
    }

    private void validation() {
        if (Utility.isOnline(LoginActivity.this)) {
            if (edtUsername.getText().length() >= 50 || edtPassword.getText().length() >= 50) {
                AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Panjang Username/Password Maximal 50 karakter");
            } else {
                if (edtUsername.getText().toString().equalsIgnoreCase("")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Username tidak boleh kosong");
                } else if (edtPassword.getText().toString().equalsIgnoreCase("")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Password tidak boleh kosong");
                } else {
//					Global.newInit();					
                    getToken();
                }
            }
        } else {
            AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, Tidak ada koneksi");
//			offlineNewEntry.setVisibility(View.VISIBLE);
        }
    }


    /**
     * methode ini digunakan untuk mendapatkan token dari service
     * sebagai parameter untuk setiap proses yang berhubungan dengan service
     */
	/*private Boolean getToken2(){
		String url = SettingActivity.URL_SERVER;
		String[] sf = Utility.split(url, ":");				
		url = (String)sf[0]+":"+(String)sf[1]+":"+(String)sf[2].substring(0, sf[2].indexOf("/"))+"/";
		url = url+"DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";
		
		new GetToken<String>(LoginActivity.this, url, "Get Token .."){
			
			public void onResultListener(Boolean response) {
				return response;
			};
			
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		return changeBranch;
	}*/

    private class getToken extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String url = SettingActivity.URL_SERVER;
            String[] sf = Utility.split(url, ":");
            url = (String) sf[0] + ":" + (String) sf[1] + ":" + (String) sf[2].substring(0, sf[2].indexOf("/")) + "/";
            url = url + "DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";
            String jsonToken = Utility.getHttpConnection2(url);
            return jsonToken;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            Nset set = Nset.readJSON(result);
            String token = set.getData("access_token").toString();
            String refresh_token = set.getData("refresh_token").toString();
            if (!token.equalsIgnoreCase("")) {
                Utility.setSetting(getApplicationContext(), "Token", token);
                Utility.setSetting(getApplicationContext(), "Refresh_Token", refresh_token);
                getLogin();
            } else {
                AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, Validasi token pertama gagal");
            }
        }
    }

    private class refreshToken extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String url = SettingActivity.URL_SERVER;
            String[] sf = Utility.split(url, ":");
            url = (String) sf[0] + ":" + (String) sf[1] + ":" + (String) sf[2].substring(0, sf[2].indexOf("/")) + "/";
            url = url + "DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";
            String jsonToken = Utility.getHttpConnection2(url);
            return jsonToken;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            Nset set = Nset.readJSON(result);
            String token = set.getData("access_token").toString();
            String refresh_token = set.getData("refresh_token").toString();
            if (!token.equalsIgnoreCase("")) {
                Utility.setSetting(getApplicationContext(), "Token", token);
                Utility.setSetting(getApplicationContext(), "Refresh_Token", refresh_token);
            } else {
                AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, Validasi token pertama gagal");
            }
        }
    }

    private void getToken() {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method ;
                String url = SettingActivity.URL_SERVER;
                if (url.contains(":")) {
                    String[] sf = Utility.split(url, ":");
                    url = (String) sf[0] + ":" + (String) sf[1] + ":" + (String) sf[2].substring(0, sf[2].indexOf("/")) + "/";
					/*newTokenWay = Utility.postMultiPartForGetToken(
							"http://testauthtoken.fifgroup.co.id:8380/auth/realms/fifgroup/protocol/openid-connect/token",
							new String[]{"username","password","client_id","client_secret","grant_type"},
							new String[]{edtUsername.getText().toString(),edtPassword.getText().toString(),"fifgroup-token","261f1b80-7a18-438e-b9fa-2f9575c97e0b","password"});*/
//					boolean token  = Utility.getNewToken2();
                    url = url + "DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";
                    jsonToken = Utility.getHttpConnection2(url);


                }

            }
        }, new Runnable() {

            @Override
            public void run() {
				/*Nset newSet = Nset.readJSON(newTokenWay);
				String newToken = newSet.get("token_type").toString() +" "+ newSet.get("access_token").toString();*/
                Nset set = Nset.readJSON(jsonToken);
                String token = set.getData("access_token").toString();
                String refresh_token = set.getData("refresh_token").toString();
                if (!token.equalsIgnoreCase("")) {
                    Utility.setSetting(getApplicationContext(), "Token", token);
                    Utility.setSetting(getApplicationContext(), "Refresh_Token", refresh_token);
                    login();
                } else {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, Validasi token pertama gagal");
                }
            }
        });
    }

    private void getLogin() {
        if (((CheckBox) findViewById(R.id.checkRemember)).isChecked()) {
            Utility.setSetting(getApplicationContext(), "REMEMBER", "TRUE");
        } else {
            Utility.setSetting(getApplicationContext(), "REMEMBER", "FALSE");
        }

        if (!namaFileRar.equalsIgnoreCase("")) {
            namaFileRar = "";
        }

        strFileName = edtUsername.getText().toString() + "_" + Utility.getTodayDate().replace("-", "");

        GPSTracker gpsTrack = new GPSTracker(LoginActivity.this);
        String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "loginservlet/?userid=", edtUsername.getText().toString().trim(), "&password=", edtPassword.getText().toString().trim(),
                "&imei=", strImei, "&gps=", gpsTrack.isGpsEnable() ? "1" : "0", "&bluetooth=", gpsTrack.isBluetoothOn() ? "1" : "0", "&datetime=", Utility.Now(),
                "&wifi=", gpsTrack.isWifiOn() ? "1" : "0", "&versi=", SettingActivity.APP_VERSION, "&mobileprofile=", "",
                "&activitypending=", Connection.DBquery("SELECT id FROMN data_activity WHERE status ='0';").getRows() + "",
                "&lastuser=", Utility.getSetting(getApplicationContext(), "LASTUSER", ""), "&batch=",
                Utility.getSetting(getApplicationContext(), "BATCH", ""),
                "&ip=", IP_Address, "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));

        new GetServiceData<String>(LoginActivity.this, sUrl, "Login ...") {

            public void onResultListener(String response) {
                try {
                    //do
                    Nset nn = Nset.readJSON(response);
                    if (nn.getData("message").getData("error").toString().equals("1")) {
                        message = nn.getData("message").getData("message").toString();
                    } else {

                        //membuat tabel mobileaccess / download menu
                        Recordset dsa = Syncronizer.getMenuHttpStream(response);
                        Connection.DBdrop("mobileaccess");
                        Syncronizer.createToDB("mobileaccess", dsa);
                        Syncronizer.insertToDB("mobileaccess", dsa);
                    }

                    Recordset data = Syncronizer.getStringHttpStream(response);
                    Utility.setSetting(getApplicationContext(), "office_type", data.getText(0, "office_type"));

                    if (data.getRows() >= 1) {
                        if (data.getText(0, "result").equals("OK")) {

                            //office_code digunakan untuk membandingkan user beda cabang yang login dgn hp yang sama
                            if (!Utility.getSetting(getApplicationContext(), "Office_Code", "").equalsIgnoreCase("")) {
                                if (!Utility.getSetting(getApplicationContext(), "Office_Code", "").equalsIgnoreCase(data.getText(0, "office_code"))) {
                                    changeBranch = true;
                                }
                            }

//							String sds = data.getText(0, "jobcode");
                            Utility.OFFICE_CODE = data.getText(0, "office_code");

                            Utility.setSetting(getApplicationContext(), "Office_Code", Utility.OFFICE_CODE);
                            Utility.setSetting(getApplicationContext(), "USER_NAME", data.getText(0, "user_name"));
                            Utility.setSetting(getApplicationContext(), "userid", data.getText(0, "userid"));

                            verGlobal = data.getText(0, "global");
                            verGen = data.getText(0, "generator");
                            verCabang = data.getText(0, "branch");
                            verTable = data.getText(0, "table");
//							verDoc   = data.getText(0,"doc_version");
                            namaFileRarPDf = data.getText(0, "file_name_doc");

//							String aa = verGlobal;
//							String oo = verGen;
//							String cc = verCabang;

                            namaFileRar = data.getText(0, "nama_file");
//							String dss = namaFileRar;

                            logintrue = true;
                            Vector<String> flag = Utility.splitVector(data.getText(0, "flag"), ";");

                            if (flag.elementAt(0).toString().equals("1")) {
                                //flag untuk user yang belum tanda tangan
                                hasSigning = false;
                                Utility.flagLogin = 1;
                            } else if (flag.elementAt(0).toString().equals("2")) {
                                //flag untuk user yang sudah pernah tanda tangan
                                hasSigning = true;
                                strPathImg = flag.elementAt(1).toString();
                                Utility.flagLogin = 2;
                            } else {
                                //flag untuk user yang tidak perlu tanda tangan
                                passSign = true;
                                Utility.flagLogin = 0;
                            }

                            CheckToolsDevice();

                        } else {
                            if (data.getRows() >= 1) {
                                if (data.getText(0, "message").length() >= 3) {
                                    actionlogin = data.getText(0, "action");
                                    showMessage(data.getText(0, "message"), data.getText(0, "ok"), "Cancel", false, 0);
                                } else {
                                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", message);
                                }
                            } else {
                                AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", message);
                            }
                        }
                    } else
                        AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Login gagal");

                } catch (Exception e) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Login gagal exception = " + e.getMessage());
                }
            }

            ;

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    /**
     * methode login ini digunakan untuk hit loginservlet dan checking download ttd
     */
    private void login() {
        if (((CheckBox) findViewById(R.id.checkRemember)).isChecked()) {
            Utility.setSetting(getApplicationContext(), "REMEMBER", "TRUE");
        } else {
            Utility.setSetting(getApplicationContext(), "REMEMBER", "FALSE");
        }

        strFileName = edtUsername.getText().toString() + "_" + Utility.getTodayDate().replace("-", "");

        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {
            @Override
            public void run() {

                //loginservlet digunakan untuk mendownload menu akses user login dan data keperluan untuk user login
                logintrue = false;

                if (!namaFileRar.equalsIgnoreCase("")) {
                    namaFileRar = "";
                }

                GPSTracker gpsTrack = new GPSTracker(LoginActivity.this);
                String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "loginservlet/?userid=", edtUsername.getText().toString().trim(), "&password=", edtPassword.getText().toString().trim(),
                        "&imei=", strImei, "&gps=", gpsTrack.isGpsEnable() ? "1" : "0", "&bluetooth=", gpsTrack.isBluetoothOn() ? "1" : "0", "&datetime=", Utility.Now(),
                        "&wifi=", gpsTrack.isWifiOn() ? "1" : "0", "&versi=", SettingActivity.APP_VERSION, "&mobileprofile=", "",
                        "&activitypending=", Connection.DBquery("SELECT id FROMN data_activity WHERE status ='0';").getRows() + "",
                        "&lastuser=", Utility.getSetting(getApplicationContext(), "LASTUSER", ""), "&batch=",
                        Utility.getSetting(getApplicationContext(), "BATCH", ""),
                        "&ip=", IP_Address, "&token=", Utility.getSetting(getApplicationContext(), "Token", ""));
                try {

                    //update teknik login..

//					Recordset dsa = Syncronizer.getHttpStream(sUrl);
                    String json = Utility.getHttpConnection(Utility.getURLenc(sUrl));


                    //do
                    Nset nn = Nset.readJSON(json);
                    if (nn.getData("message").getData("error").toString().equals("1")) {
                        message = nn.getData("message").getData("message").toString();
                    } else {

                        //membuat tabel mobileaccess / download menu
                        Recordset dsa = Syncronizer.getMenuHttpStream(json);
                        Connection.DBdrop("mobileaccess");
                        Syncronizer.createToDB("mobileaccess", dsa);
                        Syncronizer.insertToDB("mobileaccess", dsa);
                    }

                    data = Syncronizer.getStringHttpStream(json);
//					data = Syncronizer.getHttpStream(sUrl);
                    Utility.setSetting(getApplicationContext(), "office_type", data.getText(0, "office_type"));

                    if (data.getRows() >= 1) {
                        if (data.getText(0, "result").equals("OK")) {

                            //office_code digunakan untuk membandingkan user beda cabang yang login dgn hp yang sama
                            if (!Utility.getSetting(getApplicationContext(), "Office_Code", "").equalsIgnoreCase("")) {
                                if (!Utility.getSetting(getApplicationContext(), "Office_Code", "").equalsIgnoreCase(data.getText(0, "office_code"))) {
                                    changeBranch = true;
                                }
                            }

//							String sds = data.getText(0, "jobcode");
                            Utility.OFFICE_CODE = data.getText(0, "office_code");

                            Utility.setSetting(getApplicationContext(), "Office_Code", Utility.OFFICE_CODE);
                            Utility.setSetting(getApplicationContext(), "USER_NAME", data.getText(0, "user_name"));
                            Utility.setSetting(getApplicationContext(), "userid", data.getText(0, "userid"));

                            verGlobal = data.getText(0, "global");
                            verGen = data.getText(0, "generator");
                            verCabang = data.getText(0, "branch");
                            verTable = data.getText(0, "table");
//							verDoc   = data.getText(0,"doc_version");
                            namaFileRarPDf = data.getText(0, "file_name_doc");

//							String aa = verGlobal;
//							String oo = verGen;
//							String cc = verCabang;

                            namaFileRar = data.getText(0, "nama_file");
//							String dss = namaFileRar;

                            logintrue = true;
                            Vector<String> flag = Utility.splitVector(data.getText(0, "flag"), ";");

                            if (flag.elementAt(0).toString().equals("1")) {
                                //flag untuk user yang belum tanda tangan
                                hasSigning = false;
                                Utility.flagLogin = 1;
                            } else if (flag.elementAt(0).toString().equals("2")) {
                                //flag untuk user yang sudah pernah tanda tangan
                                hasSigning = true;
                                strPathImg = flag.elementAt(1).toString();
                                Utility.flagLogin = 2;
                            } else {
                                //flag untuk user yang tidak perlu tanda tangan
                                passSign = true;
                                Utility.flagLogin = 0;
                            }

                        } else {
                            logintrue = false;
                        }
                    } else
                        message = "Login Gagal";

                } catch (Exception e) {
                    message = e.getMessage();
                    logintrue = false;
                }


            }
        }, new Runnable() {

            @Override
            public void run() {
                if (logintrue) {
                    CheckToolsDevice();
                } else {
                    if (data.getRows() >= 1) {
                        if (data.getText(0, "message").length() >= 3) {
                            actionlogin = data.getText(0, "action");
                            showMessage(data.getText(0, "message"), data.getText(0, "ok"), "Cancel", false, 0);
                        } else {
                            AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", message);
                        }
                    } else {
                        AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", message);
                    }
                }
            }
        });

    }

    /**
     * methode checktoolsdevice digunakan untuk membuat path yang diperlukan untuk
     * mobile
     */
    private void CheckToolsDevice() {
        if (new File(Utility.getDefaultTempPath()).exists() == false) {
            new File(Utility.getDefaultTempPath()).mkdirs();
        }

        if (new File(Utility.getDefaultSign()).exists() == false) {
            new File(Utility.getDefaultSign()).mkdirs();
        }

        if (new File(Utility.getDefaultImagePath()).exists() == false) {
            new File(Utility.getDefaultImagePath()).mkdirs();
        }

        if (new File(Utility.getDefaultPDF()).exists() == false) {
            new File(Utility.getDefaultPDF()).mkdirs();
        }

        if (new File(Utility.getDefaultBio()).exists() == false) {
            new File(Utility.getDefaultBio()).mkdirs();
        }

        if (new File("/storage/sdcard0/DAF/LOG").exists() == false) {
            new File("/storage/sdcard0/DAF/LOG").mkdir();
        }

        setUtilitas();
    }

    /**
     * setUtilitas digunakan untuk checking properti yang ada di global param
     * digunakan untuk menghidupkan perangkat media spt bluetooth, wifi dan sejenisnya
     * tergantung dari parameter global param
     */

    @SuppressWarnings("deprecation")
    private void setUtilitas() {
        GPSTracker gpsTrack = new GPSTracker(LoginActivity.this);
        if (!Global.getText("login.gps.active", "0").equals("0")) {
            if (!gpsTrack.canGetLocation()) {
                showDialog(GPS);
                return;
            }
        }
        if (!Global.getText("login.wifi.active", "0").equals("0")) {
            if (!gpsTrack.isWifiOn()) {
                showDialog(Wifi);
                return;
            }
        }
        if (!Global.getText("login.bluetooth.active", "0").equals("0")) {
            if (!gpsTrack.isBluetoothOn()) {
                showDialog(Bluetooth);
                return;
            }
        }

        if (!Global.getText("sentactivity.autodelete", "1").equals("1")) {
            Connection.DBdelete("data_activity", " status =? ", new String[]{"1"});
        }
        if (hasSigning) {
            Utility.ttdPath = strPathImg;
            //cek dulu apakah tanda tangannya sudah ada di local device
            if (!passSign) {
//				10:31 17/04/2019 seno daf3

                if (!new File(Utility.getDefaultSign(strPathImg)).exists()) {
                    downloadImage(Utility.getSetting(getApplicationContext(), "userid", ""), strPathImg);
                } else {
                    if (!namaFileRar.equalsIgnoreCase("")) {
//						//login tunning
                        downloadPdf();
//						downloadTable();
//						downloadfile();
                    } else {
                        downloadOrder();
                    }

                }
            } else {
////				10:31 17/04/2019 seno daf3
                downloadPdf();
//				goToMainmenu();
            }
        } else {
            Intent intent = new Intent(LoginActivity.this, DigitalSignatureActivity.class);
            intent.putExtra("image", "");
            intent.putExtra("firstSign", "true");
            intent.putExtra("fileName", strFileName);
            startActivityForResult(intent, 0);
        }
    }


    /**
     * created 10:31 17/04/2019 daf 3
     */
    private String responseTableVersion;
    private String responseRaw;
    private String lastVersion;

    private void downloadTable() {
        final String vTable = QueryClass.versionNo("00000", "TABLE");

        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {
            @Override
            public void run() {
                if (vTable.equalsIgnoreCase(verTable)) {
                    responseTableVersion = "same";
                } else {
                    if (Utility.getNewToken()) {
                        responseRaw = Utility.postMultipart3(SettingActivity.URL_SERVER + "dataVersionTableServlet/",
                                new String[]{"userid", "master", "imei", "v", "token", "apkVersion", "versionTable"},
                                new String[]{Utility.getSetting(getApplicationContext(), "userid", ""), "version", strImei, "100",
                                        Utility.getSetting(getApplicationContext(), "Token", ""), SettingActivity.APP_VERSION,
                                        vTable});
//						String responseRaw = "{\"status\": \"OK\",\"model\": \"recordset\",\"message\": {\"error\": \"0\",\"message\": \"\"},\"data\": {\"fieldname\": [\"tbl1\", \"tbl2\", \"tbl3\"],\"tablename\": \"\",\"content\": [[\"id\", \"name\",\"desc\"],[\"id\", \"name\",\"desc\"],[\"id\", \"name\",\"desc\"]],\"info\":{\"rows\": \"1\",\"cols\": \"138\"}}}";					
                        Nset nsetResponse = Nset.readJSON(responseRaw);
                        String resSt = responseRaw;
                        responseTableVersion = nsetResponse.getData("data").getData("headerinfo").toString();
                        lastVersion = nsetResponse.getData("data").getData("lastVersion").toString();
                    } else {
                        //gagal dapat token
                    }
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
//				String acd = responseTableVersion;
                if (responseTableVersion != null && responseTableVersion.equalsIgnoreCase("new")) {
                    Recordset newRecord = Syncronizer.getStringHttpStream(responseRaw);
                    Vector<String> header = newRecord.getAllHeaderVector();
                    Vector<Vector<String>> c = new Vector<Vector<String>>();
                    for (int i = 0; i < header.size(); i++) {
                        Recordset ad = new Model(newRecord.getAllDataVector().get(i), c);                        //
                        Syncronizer.saveToDB(header.get(i), ad);
                    }
                    Connection.DBupdate("TRN_MOBILE_DATA_SYNC_INFO", "last_version_no=" + lastVersion, "where", "data_sync_id=TABLE");
                    downloadfile();
                } else if (responseTableVersion != null && responseTableVersion.equalsIgnoreCase("same")) {
                    downloadfile();
                } else {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "validasi token gagal !");
                }

            }
        });
    }

    private String response_code;

    private void downloadPdf() {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {
            @Override
            public void run() {
                final String vDoc = QueryClass.versionNo("00000", "DOCUMENT");
                if (namaFileRarPDf.equalsIgnoreCase(vDoc)) {
                    response_code = "same";
                } else {
                    String sUrl = SettingActivity.URL_SERVER + "downloadDocServlet/?token=" +
                            Utility.getSetting(getApplicationContext(), "Token", "") +
                            "&v=100&versionDoc=" + vDoc + "";
                    String path = Utility.getDefaultPath(namaFileRarPDf + ".rar");
                    if (Utility.getNewToken()) {
                        setProsesBarMsg("Please Wait \r\nDownload file");
                        Connection.DBinsert("trn_mobile_process_file", "zip_file_name=" + namaFileRarPDf + ".rar",
                                "txt_file_name=", "flag_download=", "flag_extract=",
                                "start_time_d=" + Utility.getTime(), "end_time_d=", "start_time_e=", "end_time_e=");
                        Utility.downloadDB(sUrl, new File(path));
                        response_code = "ok";
                    } else {
                        response_code = "token";
                    }
                }

            }
        }, new Runnable() {
            @Override
            public void run() {
                if (response_code.equalsIgnoreCase("ok")) {
                    Connection.DBupdate("trn_mobile_process_file",
                            "flag_download=Y", "flag_extract=N", "end_time_d=" + Utility.getTime(), "where ", "zip_file_name=" + namaFileRarPDf + ".rar");
                    decompressPdfRar(namaFileRarPDf);
                } else if (response_code.equalsIgnoreCase("same")) {
                    downloadTable();
//					downloadfile();
                } else if (response_code.equalsIgnoreCase("token")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "validasi token gagal !");
                }

            }
        });
    }

    private String result_code;

    private void decompressPdfRar(final String namaFileRar) {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {
            @Override
            public void run() {
                result_code = "";
                try {
                    //proses extract rar
                    Utility.deleteAllFileInFolder(Utility.getDefaultPDF());
                    new ExtractArchive().extractArchive(Utility.getDefaultPath(namaFileRar + ".rar"), Utility.getDefaultPDF());
                    if (new File(Utility.getDefaultPath("PDF")).list().length > 0) {
//						Connection.DBupdate("trn_mobile_process_file","txt_file_name="+namaFileRar+".txt",
                        Connection.DBupdate("trn_mobile_process_file", "txt_file_name=" + namaFileRar + "",
                                "flag_download=Y", "flag_extract=Y", "start_time_e=" + Utility.getTime(), "where ", "zip_file_name=" + namaFileRar + ".rar");
                        setProsesBarMsg("Please Wait \r\nExtract file");
                        result_code = updateInternalFiles2(Utility.getDefaultPDF());
                    } else if (dssd.equalsIgnoreCase("failed")) {
                        dssd = "failed";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Runnable() {
            @Override
            public void run() {
                if (result_code.equalsIgnoreCase("ok")) {
                    Connection.DBdelete("trn_mobile_process_file", " zip_file_name =? ", new String[]{namaFileRar + ".rar"});
                    Utility.deleteFile(Utility.getDefaultPath(namaFileRar + ".rar"));
                    Utility.deleteAllFileInFolder(Utility.getDefaultPDF());
                    Connection.DBupdate("TRN_MOBILE_DATA_SYNC_INFO", "last_version_no=" + namaFileRarPDf, "where", "data_sync_id=DOCUMENT");
                    // go to next process  coppy file to internal memory app
//					downloadfile();
                    downloadTable();
                } else if (result_code.equalsIgnoreCase("failed")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "extract file gagal");
                    Utility.deleteFile(Utility.getDefaultPath(namaFileRar + ".rar"));
                } else {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "File tidak ada");
                }
            }
        });

    }


    /**
     * setting parrameter tertentu yang digunakan oleh aplikasi
     * menjalankan service pemgiriman lokasi
     * dan berpindah screen ke mainmenu
     */

    private void goToMainmenu() {

        Global.newInit();
        Utility.setSetting(getApplicationContext(), "SERVICE", "OK");
        Utility.setSetting(getApplicationContext(), "LASTUSER", Utility.getSetting(getApplicationContext(), "USERNAME", ""));

        Utility.setSetting(getApplicationContext(), "USERNAME", edtUsername.getText().toString());
        Utility.setSetting(getApplicationContext(), "PASSWORD", edtPassword.getText().toString());

        Utility.JOBCODE = data.getText(0, "jobcode");

        DAFReciever.sendLocation(getApplicationContext());
        InternalStorageLeft storage = new InternalStorageLeft();

//		int blocker=a.getBlocker();
//		int warning=a.getWarning();
//		float storage=a.getStorage();
//		String input = Global.getText("blocker.storage");
//		if(input.toLowerCase().contains("%")){
//			blocker = Integer.valueOf(Global.getText("blocker.storage").substring(0,Global.getText("blocker.storage").toLowerCase().indexOf("%")));
//			warning = Integer.valueOf(Global.getText("minimum.storage").substring(0,Global.getText("minimum.storage").toLowerCase().indexOf("%")));
//			storage = InternalStorageLeft.hitung();
//		}else if(input.toLowerCase().contains("mb")){
//			blocker = Integer.valueOf(Global.getText("blocker.storage").substring(0,Global.getText("blocker.storage").toLowerCase().indexOf("m")));
//			warning = Integer.valueOf(Global.getText("minimum.storage").substring(0,Global.getText("minimum.storage").toLowerCase().indexOf("m")));
//			storage = Integer.valueOf(InternalStorageLeft.getAvaibleInternalMemorySize());
//		}else{
//			blocker = Integer.valueOf(Global.getText("blocker.storage").substring(0,Global.getText("blocker.storage").toLowerCase().indexOf("m")));
//			warning = Integer.valueOf(Global.getText("minimum.storage").substring(0,Global.getText("minimum.storage").toLowerCase().indexOf("m")));
//			storage = Integer.valueOf(InternalStorageLeft.getAvaibleInternalMemorySize());
//		}

        if (storage.getStorage() < storage.getBlocker()) {
            //notif blocker


            AdapterDialog.showDialogOneBtn(LoginActivity.this, Global.getText("message.storage.header.blocker"),
                    Global.getText("message.storage.blocker") + " " + storage.getSisaStorage(),
                    "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                        }
                    });
        } else if (storage.getStorage() >= storage.getBlocker() && storage.getStorage() <= storage.getWarning()) {

            //notif warning
            AdapterDialog.showDialogTwoBtn(LoginActivity.this, Global.getText("message.storage.header.warning"),
                    Global.getText("message.storage.warning") + " " + storage.getSisaStorage(),
                    "Lanjut", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Generator.isLogin = true;
                            startActivity(new Intent(LoginActivity.this, MenuApplicationActivity.class));
                        }
                    }, "Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                        }
                    });
        } else {
            Generator.isLogin = true;
            startActivity(new Intent(LoginActivity.this, MenuApplicationActivity.class));
        }
//		startActivity(new Intent(LoginActivity.this, MenuApplicationActivity.class));
    }

    /**
     * @param userId
     * @return untuk mengecek apakah user id yang login sekarang
     * sama dengan user yang login sebelumnya
     */
    private boolean isSameUser(String userId) {
        if (Utility.getSetting(getApplicationContext(), "USERNAME", "").equalsIgnoreCase(userId)) {
            return true;
        }
        return false;
    }

    /**
     * @param userID
     * @param strPath digunakan untuk mendownload ttd user login
     */
    private void downloadImage(String userID, final String strPath) {
        final String url = SettingActivity.URL_SERVER + "viewimageservlet?userid=" + userID + "&master=signature&filetype=blob&imei=&rnd=&v=100&jenis=user&token=" + Utility.getSetting(Utility.getAppContext(), "Token", "");
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                Utility.saveImage(strPath, url);
            }
        }, new Runnable() {

            @Override
            public void run() {

                if (!strFileName.equalsIgnoreCase("")) {
                    downloadPdf();
//					downloadTable();
//					downloadfile();
                } else {
                    downloadOrder();
                }
            }
        });
    }

    private void downloadOrder() {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                setProsesBarMsg("Please Wait \r\nDownload Order (100%)");
                downloadOrder(LoginActivity.this, LoginActivity.this);
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                goToMainmenu();
            }
        });
    }

    private void downloadrar() {
        String path = Utility.getDefaultPath(namaFileRar + ".rar");

        String vGlobal = QueryClass.versionNo("00000", "GLOBAL");
        String vCabang = QueryClass.versionNo(Utility.OFFICE_CODE, "BRANCH");
        String vGen = QueryClass.versionNo("00000", "GENERATOR");//

        if (vCabang.equalsIgnoreCase("")) {

            Connection.DBinsert("TRN_MOBILE_DATA_SYNC_INFO", "apk_ver_no=" + SettingActivity.APP_VERSION, "data_sync_id=BRANCH", "office_code=" +
                            Utility.OFFICE_CODE, "last_version_no=0", "last_json_process_date=" + Utility.getDate(), "status=1", "flag_active=1",
                    "created_by=Mobile");

            vCabang = "0";
        }

        String sUrl = SettingActivity.URL_SERVER + "dataVersionServlet/?userid=" +
                Utility.getSetting(getApplicationContext(), "userid", "") + "&master=version" + "&imei=" + strImei + "&v=100" +
                "&token=" + Utility.getSetting(getApplicationContext(), "Token", "") +
                "&versionGlobal=" + vGlobal + "&apkVersion=" + SettingActivity.APP_VERSION +
                "&versionGenerator=" + vGen + "&versionCabang=" + vCabang + "&officeCode=" + Utility.OFFICE_CODE + "&namaFile=" + namaFileRar;

        if (vGlobal.equalsIgnoreCase(verGlobal) && vCabang.equalsIgnoreCase(verCabang) && vGen.equalsIgnoreCase(verGen)) {
            goToMainmenu();
        } else {
            new GetDownloadRar<Boolean>(LoginActivity.this, sUrl, "Download File .. ", path) {

                public void onResultListener(Boolean response) {
                    if (response) {
                        try {
                            new ExtractArchive().extractArchive(Utility.getDefaultPath(namaFileRar + ".rar"), Utility.getDefaultPath());
                            if (new File(Utility.getDefaultPath(namaFileRar + ".txt")).exists()) {


                                Connection.DBupdate("trn_mobile_process_file", "txt_file_name=" + namaFileRar + ".txt",
                                        "flag_download=Y", "flag_extract=Y", "start_time_e=" + Utility.getTime(), "where ", "zip_file_name=" + namaFileRar + ".rar");

//								setProsesBarMsg("Please Wait \r\nExtract file");
                                dssd = Utility.readDirectJsonDAF(Utility.getDefaultPath(namaFileRar + ".txt"));

                                if (dssd.equalsIgnoreCase("ok")) {
                                    Connection.DBdelete("trn_mobile_process_file", " zip_file_name =? ", new String[]{namaFileRar + ".rar"});
                                    goToMainmenu();
                                } else if (dssd.equalsIgnoreCase("failed")) {
                                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "extract tabel gagal");
                                } else {
                                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "File tidak ada");
                                }

                            } else if (dssd.equalsIgnoreCase("failed")) {
                                AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "extract file gagal");
                            }
                        } catch (Exception e) {
                            AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "File tidak ada / Corrupt");
                        }
                    } else {
                        AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "File tidak ada terdownload");
                    }
                }

                ;
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
	
	/*private void downloadOrder(){
		Nset order = Nset.newObject();
		StringBuffer sbuBuffer = new StringBuffer();
		String tableName = "";
		
		Recordset rst = Connection.DBquery("select distinct model_order_tablename from model;");
		for (int i = 0; i < rst.getRows(); i++) {
			tableName = rst.getText(i, 0);
			Recordset x = Connection.DBquery("select order_id from "+tableName);
			
			for (int j = 0; j < x.getRows(); j++) {
				sbuBuffer.append(j>0?",":"").append(x.getText(j, 0));
			}
			order.setData(rst.getText(i, 0), sbuBuffer.toString());
			
			if (!Utility.getSetting(getApplicationContext(), "USERNAME", "").equals( Utility.getSetting(getApplicationContext(), "LASTUSER", ""))) {
				Connection.DBdelete(rst.getText(i, 0));
				Connection.DBdelete("log_order");		
				Connection.DBcreate("log_order", "order_id", "opened","sent","downloaded","data");
				SendAction.deleteSentActity(0);
				
			}			
		}
		
		SendAction.deleteSentActity(0, Utility.Now().substring(0, 10));
		String sUrl;
	}*/

    String dssd = "";

    private void downloadfile() {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                String path = Utility.getDefaultPath(namaFileRar + ".rar");

                String vGlobal = QueryClass.versionNo("00000", "GLOBAL");
                String vCabang = QueryClass.versionNo(Utility.OFFICE_CODE, "BRANCH");
                String vGen = QueryClass.versionNo("00000", "GENERATOR");//

                if (vCabang.equalsIgnoreCase("")) {
                    Connection.DBinsert("TRN_MOBILE_DATA_SYNC_INFO", "apk_ver_no=" + SettingActivity.APP_VERSION, "data_sync_id=BRANCH", "office_code=" +
                                    Utility.OFFICE_CODE, "last_version_no=0", "last_json_process_date=" + Utility.getDate(), "status=1", "flag_active=1",
                            "created_by=Mobile");

                    vCabang = "0";

                    if (Utility.getNewToken()) {

                        String sUrl = SettingActivity.URL_SERVER + "dataVersionServlet/?userid=" +
                                Utility.getSetting(getApplicationContext(), "userid", "") + "&master=version" + "&imei=" + strImei + "&v=100" +
                                "&token=" + Utility.getSetting(getApplicationContext(), "Token", "") +
                                "&versionGlobal=" + vGlobal + "&apkVersion=" + SettingActivity.APP_VERSION +
                                "&versionGenerator=" + vGen + "&versionCabang=" + vCabang + "&officeCode=" + Utility.OFFICE_CODE + "&namaFile=" + namaFileRar;

//						Connection.DBinsert("Log_File", "status=download", "starttime="+Utility.getTime(), "endtime=");
                        setProsesBarMsg("Please Wait \r\nDownload file");

                        Connection.DBinsert("trn_mobile_process_file", "zip_file_name=" + namaFileRar + ".rar",
                                "txt_file_name=", "flag_download=", "flag_extract=",
                                "start_time_d=" + Utility.getTime(), "end_time_d=", "start_time_e=", "end_time_e=");

                        Utility.downloadDB(sUrl, new File(path));

                        //flag untuk menandakan bahwa ada file yang didownload, jika tidak ada langusng ke main menu
                        dssd = "ok";
                    } else {
                        dssd = "token";
                    }   

                } else {
                    if (vGlobal.equalsIgnoreCase(verGlobal) && vCabang.equalsIgnoreCase(verCabang) && vGen.equalsIgnoreCase(verGen)) {
                        dssd = "sama";
                    } else {
                        if (Utility.getNewToken()) {

                            String sUrl = SettingActivity.URL_SERVER + "dataVersionServlet/?userid=" +
                                    Utility.getSetting(getApplicationContext(), "userid", "") + "&master=version" + "&imei=" + strImei + "&v=100" +
                                    "&token=" + Utility.getSetting(getApplicationContext(), "Token", "") +
                                    "&versionGlobal=" + vGlobal + "&apkVersion=" + SettingActivity.APP_VERSION +
                                    "&versionGenerator=" + vGen + "&versionCabang=" + vCabang + "&officeCode=" + Utility.OFFICE_CODE + "&namaFile=" + namaFileRar;

                            Connection.DBinsert("trn_mobile_process_file", "zip_file_name=" + namaFileRar + ".rar", "txt_file_name", "flag_download", "flag_extract",
                                    "start_time_e=" + Utility.getTime(), "end_time_e=");

//							Connection.DBinsert("Log_File", "status=download", "starttime="+Utility.getTime(), "endtime=");
                            setProsesBarMsg("Please Wait \r\nDownload file");
                            Utility.downloadDB(sUrl, new File(path));

                            //flag untuk menandakan bahwa ada file yang didownload, jika tidak ada langusng ke main menu
                            dssd = "ok";
                        } else {
                            dssd = "token";
                        }
                    }
                }


                if (SettingActivity.URL_SERVER.startsWith("http")) {
                    setProsesBarMsg("Please Wait \r\nDownload Order (100%)");
                    downloadOrder(LoginActivity.this, LoginActivity.this);
                }

            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (dssd.equalsIgnoreCase("ok")) {
                    Connection.DBupdate("trn_mobile_process_file",
                            "flag_download=Y", "flag_extract=N", "end_time_d=" + Utility.getTime(), "where ", "zip_file_name=" + namaFileRar + ".rar");
                    decompressRar();
                } else if (dssd.equalsIgnoreCase("sama")) {
                    goToMainmenu();
                } else if (dssd.equalsIgnoreCase("token")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "validasi token gagal !");
                }
            }
        });
    }

    private void decompressRar() {

        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                //jangan lupa check apakah file ada isinya atau korupt
                dssd = "";
                try {
                    //proses extract rar
                    new ExtractArchive().extractArchive(Utility.getDefaultPath(namaFileRar + ".rar"), Utility.getDefaultPath());
                    if (new File(Utility.getDefaultPath(namaFileRar + ".txt")).exists()) {


                        Connection.DBupdate("trn_mobile_process_file", "txt_file_name=" + namaFileRar + ".txt",
                                "flag_download=Y", "flag_extract=Y", "start_time_e=" + Utility.getTime(), "where ", "zip_file_name=" + namaFileRar + ".rar");

                        setProsesBarMsg("Please Wait \r\nExtract file");
                        dssd = Utility.readDirectJsonDAF(Utility.getDefaultPath(namaFileRar + ".txt"));

                    } else if (dssd.equalsIgnoreCase("failed")) {
                        dssd = "failed";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (dssd.equalsIgnoreCase("ok")) {
                    Connection.DBdelete("trn_mobile_process_file", " zip_file_name =? ", new String[]{namaFileRar + ".rar"});
                    //seno 10:08 11/01/2019
                    String path = Utility.getDefaultPath(namaFileRar + ".rar");
                    Utility.deleteFile(path);
                    //seno 10:08 11/01/2019
                    goToMainmenu();
                } else if (dssd.equalsIgnoreCase("failed")) {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "extract tabel gagal");
                } else {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "File tidak ada");
                }
            }
        });

    }
	
	/*private void dcompressGzip(){
		byte[] buffer = new byte[1024];
		try {
			GZIPInputStream gis = new GZIPInputStream(new FileInputStream(Utility.getDefaultPath("fjson.gz")));
			FileOutputStream fos = new FileOutputStream(Utility.getDefaultPath("ex.txt"));
			
			int len;
			while ((len=gis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			
			gis.close();
			fos.close();
			
//			Connection.DBupdate("Log_File", "endtime="+Utility.getTime(), "where", "status=decompress");
			Utility.readDirectJsonDAF(Utility.getDefaultPath("ex.txt"));
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}*/

    /**
     * @param message menampilkan progress dialog
     */
    private void setProsesBarMsg(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Messagebox.setProsesBarMsg(message);
            }
        });
    }

    public static void downloadOrder(Context context) {
        downloadOrder(context, null);
    }

    public static void downloadOrder(Context context, LoginActivity showprogres) {
        Nset order = Nset.newObject();
        StringBuffer sbuBuffer = new StringBuffer();
        StringBuffer newSbuBuffer = new StringBuffer();            //seno daf 3 10:30 12/04/2019
        String tableName = "";

        Recordset rst = Connection.DBquery("select distinct model_order_tablename from model;");

//		25/04/2019 seno daf3
        Vector<String> header = new Vector<String>();
        Vector<Vector<String>> fields = new Vector<Vector<String>>();
        header.add(rst.getHeader(0));
        for (int i = 0; i < rst.getRows(); i++) {
            String namaTbl = rst.getText(i, 0);
            if (namaTbl.contains(",")) {
                Vector<String> tmp = Utility.splitVector(namaTbl, ",");
                for (int j = 0; j < tmp.size(); j++) {
                    fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
                }
            } else {
                fields.add(new Vector<String>(Arrays.asList(namaTbl)));
            }

        }
        rst = null;
        rst = new Model(header, fields);
//		25/04/2019 seno selesai

        for (int i = 0; i < rst.getRows(); i++) {
            tableName = rst.getText(i, 0);
            Recordset x = Connection.DBquery("select order_id from " + tableName);
            //seno 13:31 23/04/2019 daf3
            if (sbuBuffer.length() > 0) {
                sbuBuffer.delete(0, sbuBuffer.length());
            }
            newSbuBuffer.append(newSbuBuffer.length() > 0 ? "|" : "").append(tableName + "=");            //seno daf 3 10:30 12/04/2019
            for (int j = 0; j < x.getRows(); j++) {
                sbuBuffer.append(j > 0 ? "," : "").append(x.getText(j, 0));
                newSbuBuffer.append(j > 0 ? "," : "").append(x.getText(j, 0));            //seno daf 3 10:30 12/04/2019
            }
            order.setData(rst.getText(i, 0), sbuBuffer.toString());
            //tasklist
            if (!Utility.getSetting(context, "USERNAME", "").equals(Utility.getSetting(context, "LASTUSER", ""))) {
                Connection.DBdelete(rst.getText(i, 0));
                Connection.DBdelete("log_order");
                Connection.DBcreate("log_order", "order_id", "opened", "sent", "downloaded", "data");
                SendAction.deleteSentActity(0);

            }
        }

        SendAction.deleteSentActity(0, Utility.Now().substring(0, 10));
        String sUrl; //= Utility.getURLenc(SettingActivity.URL_SERVER + "orderservlet/?action=nikita&order=" ,order.toJSON(),"&userid=",Utility.getSetting(context, "userid", ""),"&flg=",(showprogres!=null?"":"service"));

        //kode lama
//		sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/"+ (showprogres!=null?"":"?f=service"), new String[] { "action", "order", "userid"  ,"flg"},
//				new String[] { "nikita", order.toJSON(), Utility.getSetting(context, "userid", "") , (showprogres!=null?"":"service") });

        if (Utility.getNewToken()) {
            sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/" + (showprogres != null ? "" : "?f=service"), new String[]{"action", "order", "userid", "flg", "token"},
                    new String[]{"nikita", order.toJSON(), Utility.getSetting(context, "userid", ""), (showprogres != null ? "" : "service"), Utility.getSetting(context, "Token", "")});


            if (showprogres == null && sUrl.startsWith("LOGOUT")) {
                Utility.setSetting(context, "ERRORMSG", "KILL");
            }

            if (showprogres == null && sUrl.endsWith("SERVICEOFF")) {
                Utility.setSetting(context, "SERVICE", "");
            }
            order = Nset.readJSON(sUrl);
            if (showprogres != null) {
                showprogres.setProsesBarMsg("Please Wait \r\nSaving Order (100%)");
            }

            boolean breceived = false, isInsert = false;
            for (int i = 0; i < order.getArraySize(); i++) {
                String table = order.getData(i).getData("table").toString();
                for (int j = 0; j < order.getData(i).getData("data").getArraySize(); j++) {
                    String code = order.getData(i).getData("data").getData(j).getData("code").toString();
                    String drst = order.getData(i).getData("data").getData(j).getData("data").toString();
//					System.out.println("abcd "+drst);
                    Recordset xxx = Stream.downStream(drst);
                    if (MD5HASH) {
                        String md5 = Utility.MD5(xxx.getAllDataVector().toString());
                        if (!md5.equals(Utility.getSetting(context, table, ""))) {
                            if (code.equals("truncate")) {
                                Syncronizer.saveToDB(table, xxx);
                            } else if (code.equals("insert")) {
                                Syncronizer.createToDB(table, xxx);
                                Syncronizer.insertToDB(table, xxx);
                            } else if (code.equals("delete")) {
                                Syncronizer.createToDB(table, xxx);
                                Syncronizer.deleteToDB(table, xxx);
                            } else if (code.equals("update")) {
                                Syncronizer.createToDB(table, xxx);
                                Syncronizer.updateToDB(table, xxx);
                            }
                        }
                        Utility.setSetting(context, table, md5);
                    } else {
                        if (code.equals("truncate")) {
                            Syncronizer.saveToDB(table, xxx);
                        } else if (code.equals("insert")) {
                            if (xxx.getRows() > 0) {
                                isInsert = true;
                            }
//							Syncronizer.deleteToDB(table, xxx);
                            Syncronizer.createToDB(table, xxx);
                            Syncronizer.insertToDB(table, xxx);
                        } else if (code.equals("delete")) {
                            Syncronizer.createToDB(table, xxx);
                            Syncronizer.deleteToDB(table, xxx);
                        } else if (code.equals("update")) {
                            Syncronizer.createToDB(table, xxx);
                            Syncronizer.deleteToDB(table, xxx);
                            Syncronizer.insertToDB(table, xxx);
                        }
                    }

                    deleteExpiredTasklit(table);
                    if (xxx.getRows() >= 1) {
                        breceived = true;
                    }
                }

            }
            // hanya untuk trn_task_list
            Recordset tabel = null;
            StringBuffer orderNew = new StringBuffer();
            StringBuffer orderNew2 = new StringBuffer();            //seno daf 3 10:30 12/04/2019
            for (int i = 0; i < rst.getRows(); i++) {
                tabel = Connection.DBquery("select order_id from " + rst.getText(i, 0));
                orderNew2.append(orderNew2.length() > 0 ? "|" : "").append(rst.getText(i, 0) + "=");            //seno daf 3 10:30 12/04/2019
                for (int j = 0; j < tabel.getRows(); j++) {
                    orderNew.append(j > 0 ? "," : "").append(tabel.getText(j, 0));
                    orderNew2.append(j > 0 ? "," : "").append(tabel.getText(j, 0));            //seno daf 3 10:30 12/04/2019
                }
            }

//			if (isInsert || QueryClass.isHaveTable(tableName)) {
//				insertRecievedDate(orderNew.toString(), sbuBuffer.toString());			
//			}

            insertRecievedDate2(orderNew2.toString(), newSbuBuffer.toString());            //seno daf 3 10:30 12/04/2019


//			if (breceived) {
//				Nset order_id = Nset.newObject();
//				order_id.setData("trn_task_list", sbuBuffer.toString());
//				if (Utility.getNewToken()) {
//					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
//							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(context, "userid", ""), (showprogres!=null?"":"service"), Utility.getSetting(Utility.getAppContext(), "Token", "")});
//				}else{
//					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
//							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(context, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
//				}
//				
//			}

//			seno daf 3 10:30 12/04/2019			
            if (breceived) {
                Nset order_id = Nset.newObject();
                Vector<String> vSBuffer = Utility.splitVector(newSbuBuffer.toString(), "|");
                for (int i = 0; i < vSBuffer.size(); i++) {
                    String table = vSBuffer.get(i).substring(0, vSBuffer.get(i).indexOf("="));
                    String data = vSBuffer.get(i).substring(vSBuffer.get(i).indexOf("=") + 1);
                    order_id.setData(table, data);
                }
                if (Utility.getNewToken()) {
                    sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[]{"action", "order", "userid", "flg", "token"},
                            new String[]{"RECEIVED", order_id.toJSON(), Utility.getSetting(context, "userid", ""), (showprogres != null ? "" : "service"), Utility.getSetting(Utility.getAppContext(), "Token", "")});
                } else {
                    sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[]{"action", "order", "userid", "flg", "token"},
                            new String[]{"RECEIVED", order_id.toJSON(), Utility.getSetting(context, "userid", ""), "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
                }
            }
//			//seno daf 3 10:30 12/04/2019
            //..
        }


    }

    /**
     * @param orderNew order yang baru didapat dari service
     * @param orderOld order yang ada di device mobile
     *                 <p>
     *                 digunakan untuk mengisi field recieved_date pada tabel trn_task_list
     */
    private static void insertRecievedDate(String orderNew, String orderOld) {
        Vector<String> vOrderNew = Utility.splitVector(orderNew, ",");
        Vector<String> vOrderOld = Utility.splitVector(orderOld, ",");
        Connection.DBopen();
        if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
            for (int i = 0; i < vOrderNew.size(); i++) {
                if (!Utility.isContainValue(vOrderNew.elementAt(i), vOrderOld)) {
                    Connection.DBupdate("trn_task_list", "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(i));
                }
            }
        } else {
            for (int i = 0; i < vOrderNew.size(); i++) {
                Connection.DBupdate("trn_task_list", "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(i));
            }
        }
    }

    /**
     * digunakan untuk mengisi field recieved_date pada tabel
     * yang terdapat pada order new
     *
     * @param orderNew
     * @param orderOld
     */
    private static void insertRecievedDate2(String orderNew, String orderOld) {


        Vector<String> vOrderNew = Utility.splitVector(orderNew, "|");
        Vector<String> vOrderOld = Utility.splitVector(orderOld, "|");
        Connection.DBopen();
        for (int i = 0; i < vOrderNew.size(); i++) {
            String table_name = vOrderNew.get(i).substring(0, vOrderNew.get(i).indexOf("="));
//			Vector<String> tmpVOrderNew = Utility.splitVector(vOrderNew.get(i).substring(vOrderNew.get(i).indexOf("=")+1), ",");
//			Vector<String> tmpVOrderold = Utility.splitVector(vOrderNew.get(i).substring(vOrderNew.get(i).indexOf("=")+1), ",");

//			System.out.println(tmpVOrderNew+"\n"+tmpVOrderold);
//				Connection.DBopen();		
            if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
                for (int j = 0; j < vOrderNew.size(); j++) {
                    if (!Utility.isContainValue(vOrderNew.elementAt(j), vOrderOld)) {
                        Connection.DBupdate(table_name, "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(j));
                    }
                }
            } else {
                for (int j = 0; j < vOrderNew.size(); j++) {
                    Connection.DBupdate(table_name, "received_date=" + Utility.getTodayDate2(), "where", "order_id=" + vOrderNew.elementAt(j));
                }
            }
        }
    }


    /**
     * untuk mendelete tasklist yg expired
     *
     * @param table param nama tabel
     */
    private static void deleteExpiredTasklit(String table) {
        Recordset data = Connection.DBquery("SELECT order_id, expired_date FROM " + table);
        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {
                if (!data.getText(i, "expired_date").equalsIgnoreCase("")) {
                    Calendar cal = convertTasklits(data.getText(i, "expired_date"));
                    Date today = new Date();
                    if (today.after(cal.getTime())) {
                        Connection.DBdelete(table, "order_id=?", new String[]{data.getText(i, "order_id")});
                    }
                }
            }
        }
    }

    /**
     * @param strDate tanggal yang dimasukkan
     * @return Calendar
     * <p>
     * untuk mengconvert tanggal dari string ke Calendar
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("SimpleDateFormat")
    public static Calendar convertTasklits(String strDate) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH);
            date = sdf.parse(strDate);
            calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }


    /**
     * untuk menampilkan dialog progress
     */
    private String actionlogin = "";

    public void showMessage(String message, final String PositiveButton, final String NegativeButton, boolean cancel, final int reqcode) {
        Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle("");
        if (NegativeButton != null) {
            if (!NegativeButton.equals("")) {
                dlg.setNegativeButton(NegativeButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }
        if (PositiveButton != null) {
            if (!PositiveButton.equals("")) {
                dlg.setPositiveButton(PositiveButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        try {
                            if (actionlogin.toLowerCase().trim().startsWith("intent://")) {
                                Intent i = new Intent(actionlogin.substring(9));
                                startActivity(i);
                            } else {
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(actionlogin));
                                startActivity(i);
                            }

                        } catch (Exception e) {
                        }
                    }
                });
            }
        }

        dlg.setCancelable(cancel);
        dlg.setMessage(message);
        AlertDialog alertDialog = dlg.create();
        alertDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            if (Utility.finishSign) {
                uploadSign();
            } else {
                AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf, anda belum melakukan tanda tangan ");
            }

        } else if (requestCode == GPS && resultCode == 0) {
            LocationOffline loc = new LocationOffline(getApplicationContext());
        } else if (requestCode == AppVersion && resultCode == Activity.RESULT_OK) {
            Utility.setSetting(getApplicationContext(), "app_version", Global.getText("app.version", ""));
        } else if (requestCode == certificate && resultCode == 0) {
            isCertExist = Utility.certificateFIFExist();
        } else if (requestCode == 101 && resultCode == 0) {
//			offlineNewEntry.setVisibility(View.GONE);
        }
    }


    /**
     * untuk mengupload tanda tangan keserver ketika pertama kali
     */
    String result = "";

    private void uploadSign() {
        Messagebox.showProsesBar(LoginActivity.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                File file = null;
                if (new File(Utility.getDefaultSign(strFileName)).exists() == true) {
                    file = new File(Utility.getDefaultSign(strFileName));
                } else
                    file = new File(Utility.getDefaultPath(strFileName));

                if (file != null) {
                    result = ImagePost.postHttpConnectionWithPicture(SettingActivity.URL_SERVER + "uploadservlet?filetype=blob&master=signature&userid=" +
//							17/12/2018
                            Utility.getSetting(getApplicationContext(), "userid", "") + "&iscustomer=N", null, strFileName, file.toString(), UUID.randomUUID().toString());
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (result.endsWith("OK")) {

                    if (!namaFileRar.equalsIgnoreCase("")) {
//						//login tunning
                        downloadPdf();
//						downloadTable();
//						downloadfile();
                    } else {
                        downloadOrder();
                    }
                } else {
                    AdapterDialog.showMessageDialog(LoginActivity.this, "Informasi", "Maaf pengiriman tanda tangan gagal");
                }
            }
        });
    }


    public String updateInternalFiles(String path) {
        String[] updateFiles = new File(path).list();
        try {
            String storagePath = Utility.getReportPath("");
            for (int i = 0; i < updateFiles.length; i++) {
                String fileNew = updateFiles[i];
                if (new File(storagePath + "/" + fileNew).exists()) {
                    Utility.deleteFile(storagePath + "/" + fileNew);
                    Utility.copyFile(path + "/" + fileNew, storagePath + "/" + fileNew);

                } else {
                    Utility.copyFile(path + "/" + fileNew, storagePath + "/" + fileNew);
                }
            }
        } catch (Exception e) {
            return "filed";
        }

        return "OK";
    }


    public String updateInternalFiles2(String path) {
        String[] updateFiles = new File(path).list();
//		updateFiles[1] = path+"/seno.jpg";
        String returns = "OK";
        try {
            String storagePath = Utility.getReportPath("");
            for (int i = 0; i < updateFiles.length; i++) {
                String filename = updateFiles[i];
                File fileNew = new File(path + "/" + filename);
                if (fileNew.isDirectory()) {
                    String tmp = updateInternalFiles2(path + "/" + filename);
                    if (tmp.equals("failed")) {
                        returns = tmp;
                        break;
                    }
                } else {
                    if (new File(storagePath + "/" + filename).exists()) {
                        Utility.deleteFile(storagePath + "/" + filename);
                        Utility.copyFile(path + "/" + filename, storagePath + "/" + filename);

                    } else {
                        Utility.copyFile(path + "/" + filename, storagePath + "/" + filename);
                    }
                }
            }
        } catch (Exception e) {
            returns = "failed";
        }

        return returns;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog dialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        switch (id) {
            case GPS:
                dialogBuilder.setTitle("Setting GPS");
                dialogBuilder.setMessage("Please Turn On Your GPS");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent inten = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(inten, GPS);
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case Bluetooth:
                dialogBuilder.setTitle("Setting Bluetooth");
                dialogBuilder.setMessage("Please Turn On Your Bluetooth");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent inten = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                        startActivityForResult(inten, Bluetooth);
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case Wifi:
                dialogBuilder.setTitle("Setting Wifi");
                dialogBuilder.setMessage("Please Turn On Your Wifi");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent inten = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivityForResult(inten, Wifi);
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case AppVersion:
                dialogBuilder.setTitle("New App Version");
                dialogBuilder.setMessage("Terdapat App Version terbaru, silahkan update");
                dialog = dialogBuilder.create();
                dialog.setButton("Update", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Uri uri = Uri.parse(Global.getText("app.download.url", ""));
                        Intent inten = new Intent(Intent.ACTION_VIEW, uri);
                        startActivityForResult(inten, AppVersion);
                    }
                });

                dialog.setButton2("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;

            case token:
                dialogBuilder.setTitle("Token Expired");
                dialogBuilder.setMessage("Token expired, apakan anda ingin memperbaharui token ?");
                dialog = dialogBuilder.create();
                dialog.setButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getToken();
                    }
                });

                dialog.setButton2("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                break;
        }

        return dialog;
    }

    @Override
    protected void onRestart() {
        if (Utility.isExit) {
            finish();
        }
        super.onRestart();
    }

    String filename;

//	 public void testPDF(){
////		 final ReportTestRunner mreReportTestRunner = new ReportTestRunner(LoginActivity.this);
//
//		 Messagebox.showProsesBar(LoginActivity.this, new Runnable() {
//			 @Override
//			 public void run() {
//				 try {
////					  filename  = mreReportTestRunner.exportReport("mpf.jrxml");
//				 } catch (Exception e) {
//					 e.printStackTrace();
//				 }
//			 }
//		 }, new Runnable() {
//			 @Override
//			 public void run() {
//				if(filename!=null){
//					Log.d("seno ",filename);
//
//				}
//			 }
//		 });
//
//
//	 }

}
