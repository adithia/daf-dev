package com.daf.activity;

import java.io.File;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.salesforce.R;
import com.salesforce.adapter.SignatureView;
import com.salesforce.utility.Utility;

public class DigitalSignatureActivity extends Activity {
	
	private static int TIME_OUT = 500;
	private File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signature);
		LinearLayout laySignature = (LinearLayout) findViewById(R.id.lay_sign);
		
		OpenImage((ImageView)findViewById(R.id.img_sign),getIntent().getStringExtra("image"));
		
		if (getIntent().getStringExtra("firstSign") != null) {
			file = new File(Utility.getDefaultSign(getIntent().getStringExtra("fileName")));
		}else {
			file = new File(Utility.getDefaultTempPath("sign-nikita"));
		}
		
//		OpenImage((ImageView)findViewById(R.id.img_sign),getIntent().getStringExtra("image"));
//		file = new File(Utility.getDefaultTempPath("sign-nikita"));
		
		SignatureView snView = new SignatureView(DigitalSignatureActivity.this, findViewById(R.id.img_sign), laySignature,
				//perubahan 15-09-2018
				(RelativeLayout) findViewById(R.id.lay_btn) , file , DigitalSignatureActivity.this);		
		
		addSign(snView, laySignature);
		
		
		
	}
	
	/**
	 * untuk menampilkan image kedalam view diaplikasi
	 */
	public static void OpenImage(ImageView context, String path) {
		try {
			Bitmap b=BitmapFactory.decodeFile(path);
			if (b!=null) {
				context.setImageBitmap(b);
				context.setVisibility(View.VISIBLE);
			}else {
				context.setVisibility(View.GONE);
			}
			
		} catch (Exception e) {
			context.setVisibility(View.GONE);
		}
	}
	
	/**
	 * untuk membuat coretan sesuai inputan user yang dijadikan sebagai tanda tangan
	 */
	private void addSign(final SignatureView snView, final LinearLayout laySignature) {

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				int width = laySignature.getWidth();
				int height = laySignature.getHeight();

				RelativeLayout layPrimary = (RelativeLayout) findViewById(R.id.LinearLayout1);

				if (width > height) {
					laySignature.addView(snView, height, height);
					layPrimary.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.FILL_PARENT, height));
				} else {
					laySignature.addView(snView, width, width);
					layPrimary.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.FILL_PARENT, width));
				}
			}
		}, TIME_OUT);

	}

}
