package com.daf.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

//import com.google.android.gms.internal.ao;
import com.salesforce.R;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.service.DAFReciever;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class SplashScreenBckp extends Activity {

	private boolean sophosInstalled = false;
	ProgressBar pbBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splashscreen);
		
		Utility.setAppContext(getApplicationContext());
//		startService(new Intent(this, Nikita.class));
		
//		check();
		update();
		
		String abc = Utility.getSetting(SplashScreenBckp.this, "importDataFromAsset", "");
		if(abc.equalsIgnoreCase("true")||abc == ""){
			Utility.setSetting(getApplicationContext(), "importDataFromAsset", "false");
//			Toast.makeText(getApplicationContext(), " Copy file", Toast.LENGTH_LONG).show();
			listAssetFiles("");
//			new moveFile1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);			
		}
		
		Connection.DBopen(SplashScreenBckp.this);
		ConnectionBG.DBopen(SplashScreenBckp.this);
		Connection.DBcreate("SETTING", "URL", "MAC_ADDRESS_PRINTER", "STORAGE");
		Connection.DBcreate("log_order", "order_id", "opened","sent","downloaded", "data");
		
//		Connection.DBcreate("IVERSION", "VERNAME", "VERNO", "LASTSYNC");
		
		Recordset data = Connection.DBquery("select * from SETTING");
		if (data.getRows() < 1) {
			Utility.setSetting(getApplicationContext(), "firstInstall", "");
		}
		
//		Utility.setSetting(getApplicationContext(), "firstInstall", "");
		
		PackageManager pm = getPackageManager();
		List<ApplicationInfo> ai = pm.getInstalledApplications(0);
		List<ApplicationInfo> installedApps = new ArrayList<ApplicationInfo>();
		for(ApplicationInfo app : ai) {
			
		    //checks for flags; if flagged, check if updated system app
		    if((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) == 1) {
		        installedApps.add(app);
		    //it's a system app, not interested
		    } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
		        //Discard this one
		    //in this case, it should be a user-installed app
		    } else {
		        installedApps.add(app);
		    }
		}
		
		for (int i = 0; i < installedApps.size(); i++) {
			ApplicationInfo app = installedApps.get(i);
			if (app.className != null) {
				if (app.className.equalsIgnoreCase("com.sophos.mobilecontrol.client.android.core.SmcApplication")) {
					sophosInstalled = true;
					break;
				}
			}
		}
		
//		if (!sophosInstalled) {
//			AdapterDialog.showAlertDialogFinish(SplashScreen.this, "Informasi", "MDM Agent di device anda belum tersedia, harap install MDM Agent terlebih dahulu");
//		}else {			
			if (Global.getText("splash.active.state", "true").equals("true")) {
				String imgPath = Global.getText("splash.image.url");
				if (imgPath.contains("http")) {
					Utility.LoadImageDirect((ImageView)findViewById(R.id.imageView1), imgPath);	
				}
				
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						
//						if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
//							new RetrieveCSV(SplashScreen.this).execute();
//						}else{
							startActivity(new Intent(SplashScreenBckp.this, LoginActivity.class));
							finish();
//						}
												
					}
				}, Integer.parseInt(Global.getText("splash.timer.amount", "5")) * 1000);
				
			}else {
//				if (Utility.getSetting(getApplicationContext(), "firstInstall", "").equalsIgnoreCase("")) {
//					new RetrieveCSV(SplashScreen.this).execute();
//				}else{
					startActivity(new Intent(SplashScreenBckp.this, LoginActivity.class));
					finish();
//				}
			}
//		}
	}
	
	private boolean listAssetFiles(String path) {
	    String [] list;
//	    ArrayList<String> ygdicopy=new ArrayList<String>();
	    try {
	        list = getAssets().list(path);	        
	        if (list.length > 0) {
	            // This is a folder
	            for (String file : list) {
	                if (!listAssetFiles(path + "/" + file)){
	                    return false;
	                }else {
	                	if(file.endsWith(".pdf")||file.endsWith(".txt")){
	                		String sourceFile = path + "/" + file.toString();
//	                		ygdicopy.add(a);
	                		File mydir = getApplicationContext().getDir("Doc", getApplicationContext().MODE_PRIVATE); //Creating an internal dir;
	                		if (!mydir.exists())
	                		{
	                		     mydir.mkdirs();
	                		}
	                		String dirPath = mydir.getAbsolutePath();	                		
	                		try{
	                			InputStream in = null;
	                			OutputStream out = null;
	                			AssetManager assetFile = getAssets();
//	                			in = assetFile.open(file.toString());
	                			if(sourceFile.startsWith("/")){	                				
	                				in = assetFile.open(file.toString());
	                			}else{
	                				in = assetFile.open(sourceFile);
	                			}	                			
//	                			in = assetFile.open(a);
	                			out = new FileOutputStream(dirPath+"/"+file.toString());
	                			Utility.CopyStream(in, out);
//	                			OutputStream out2 = new FileOutputStream(abc+"/"+
////	                					"_Seno_"+
//	                					file.toString());
//	                			Utility.encrypt(in,out2,"senoSunawar1235");	                			
	                		}catch(Exception e){
	                			e.printStackTrace();
	                		}
	                			
	                	}
	                    // This is a file
	                    // TODO: add file name to an array list
	                }
	            }
	        } 
	    } catch (IOException e) {
	        return false;
	    }
	    return true; 
	} 
	
	private void deleteFromInternalStorage(String namafile){	    	    
		String pathFolder = Utility.getInternalStorageDocPath();
	    String[] files = getApplicationContext().getDir("Doc", getApplicationContext().MODE_PRIVATE).list();
	    ArrayList<File> listdelete = new ArrayList<File>();
    	    
	    for(String file:files){
	    	if(file.contains(namafile)){
	    		listdelete.add(new File(pathFolder,namafile+".pdf"));
	    		listdelete.add(new File(pathFolder,namafile+".txt"));	    		 
	    		break;
	    	}
	    }
	    
	    if(listdelete.size()>0){
	    	for(File todelete : listdelete){
	    		if(todelete.exists()){
	    			todelete.delete();
	    		}
	    	}
	    }	    	    
	}
	
	public static  void deleteInternalStorageFolder(){
		Utility.deleteFolderAll(new File(Utility.getInternalStorageDocPath()));
//		Utility.deleteAllFileInFolder(Utility.getInternalStorageDocPath());
	}
	
	public void check(){
		String abc = Utility.getSetting(SplashScreenBckp.this, "importDataFromAsset", "");
		if(abc.equalsIgnoreCase("true")||abc == ""){
			Utility.setSetting(getApplicationContext(), "importDataFromAsset", "false");
//			Toast.makeText(SplashScreen.this, " Copy file", Toast.LENGTH_LONG).show();
			listAssetFiles("");
//			new moveFile1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);			
		}else{
			update();
			check();
		}
	}
	
	public void update(){
		String name="";
		
		try {
			PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			name = pinfo.versionName;	
			Connection.DBopen(SplashScreenBckp.this);
			String apkVersionFromExistingDB = Utility.getSetting(SplashScreenBckp.this, "app_version", "");
			Recordset dataRecordset = Connection.DBquery("select value from global where code = 'app.version'");
			Vector<String> abc = dataRecordset.getRecordFromHeader("value");		
//			Toast.makeText(SplashScreen.this, "apk Version from shared preferences "+apkVersionFromExistingDB, Toast.LENGTH_LONG).show();
			if(abc.size()>0){
				apkVersionFromExistingDB = abc.get(0);	
			}			
//			Toast.makeText(SplashScreen.this, "apk Version from db "+apkVersionFromExistingDB, Toast.LENGTH_LONG).show();
//			Toast.makeText(SplashScreen.this, "apk version from application "+name, Toast.LENGTH_LONG).show();
//			apkVersionFromExistingDB = "1.10";
			if(apkVersionFromExistingDB.equalsIgnoreCase(name)||apkVersionFromExistingDB.equalsIgnoreCase("")){
//				Toast.makeText(SplashScreen.this, " tidak Copy file", Toast.LENGTH_LONG).show();
				Connection.DBclose();
				
			}else{	
//				Toast.makeText(SplashScreen.this, "deleting file", Toast.LENGTH_LONG).show();
//				Toast.makeText(SplashScreen.this, "apk version from application "+name, Toast.LENGTH_LONG).show();
				DAFReciever.cancelAllBackgroundService(SplashScreenBckp.this);
				Connection.DBclose();
				ConnectionBG.DBclose();
				Utility.deleteFolderAll(new File(Utility.getDefaultPath()));
				Utility.clearSetting(SplashScreenBckp.this);
				deleteInternalStorageFolder();
				Utility.setSetting(getApplicationContext(), "DBVERSION", "");
				Utility.setSetting(SplashScreenBckp.this, "importDataFromAsset", "true");
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	class moveFile1 extends AsyncTask<Void, Integer, String>{
		@Override
		protected void onPreExecute() {
			pbBar.setVisibility(View.VISIBLE);			
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(Void... params) {
//			deleteInternalStorageFolder();
			String path = "";
			 String [] list;
			    ArrayList<String> ygdicopy=new ArrayList<String>();
			    try {
			        list = getAssets().list(path);	        
			        if (list.length > 0) {
			            // This is a folder
			        	int i=0;
			            for (String file : list) {
			            	i++;
//			                if (!listAssetFiles(path + "/" + file)){
////			                    return false;
//			                }else {
			                	if(file.endsWith(".pdf")||file.endsWith(".txt")){
			                		String a = path + "/" + file.toString();
			                		ygdicopy.add(a);
			                		File mydir = getApplicationContext().getDir("Doc", getApplicationContext().MODE_PRIVATE); //Creating an internal dir;
			                		if (!mydir.exists())
			                		{
			                		     mydir.mkdirs();
			                		}
			                		String abc = mydir.getAbsolutePath();	                		
			                		try{
			                			InputStream in = null;
			                			OutputStream out = null;
			                			AssetManager assetFile = getAssets();
//			                			in = assetFile.open(file.toString());
			                			if(a.startsWith("/")){	                				
			                				in = assetFile.open(file.toString());
			                			}else{
			                				in = assetFile.open(a);
			                			}	                			
//			                			in = assetFile.open(a);
			                			out = new FileOutputStream(abc+"/"+file.toString());
			                			Utility.CopyStream(in, out);
			                			Thread.sleep(1000);
			                			publishProgress(i);
//			                			OutputStream out2 = new FileOutputStream(abc+"/"+
////			                					"_Seno_"+
//			                					file.toString());
//			                			Utility.encrypt(in,out2,"senoSunawar1235");	                			
			                		}catch(Exception e){
			                			e.printStackTrace();
			                		}
			                			
			                	}
			                    // This is a file
			                    // TODO: add file name to an array list
//			                }
			            }
			        } 
			    } catch (IOException e) {
//			        return false;
			    }
			
			publishProgress(0);
			return null;
		}
		@Override
		protected void onProgressUpdate(Integer... values) {			
			pbBar.setProgress(values[0]);
			super.onProgressUpdate(values);
		}
		
	}

}
