package com.daf.activity;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fingerq.FPC1080Sensor;
import com.fingerq.fingerprint.algorithm.matching.FPC1080MatchingEngine;
import com.fingerq.generic.biometrics.device.BiometricSample;
import com.fingerq.generic.biometrics.device.BiometricSampleListener;
import com.fingerq.generic.biometrics.device.Sensor;
import com.fingerq.generic.biometrics.matching.MatchingEngine;
import com.fingerq.helper.BiometricsSystem;
import com.fingerq.helper.EnrollmentListener;
import com.fingerq.helper.IdentificationListener;
import com.fingerq.io.MX25LStorage;
import com.fingerq.io.Storage;
import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.adapter.DeviceManager;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.utility.Utility;

public class Finger_New extends Activity{
	
	private ImageView imgViewFinger;
	private TextView txtInfo,txtInfo2,txtTitle, txtTitle2;
//	private Handler handler;
	private Bitmap bitmap;
	
	// default score for identification
	private int _security = 484, posSpin = 0;
	
	// default count for enrollment
	private int ENROLMENT_COUNT = 1;
	private BiometricsSystem _biometricsSystem;	
	private String bufferFile = "";
	private String returnFile = "", strJari= "", strCodeJari;
	private Vector<String> hasSign = new Vector<String>();
	private Vector<String> vJari = new Vector<String>();
	
	private Button btnValidasi;
	private Button btnSubmit;
	private Button btnCancel;
	private Spinner spnFinger;
	private String imgFinger;
	private int totalScan = 0;
	private boolean refreshImg = false, hasFinishMandatory = false, hasDeleted = false, fromScan = false;
	private byte [] bTemplate;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.finger_verify);
		
		returnFile = Utility.getDefaultTempPath("finger-nikita");
		bufferFile = Utility.getDefaultPath("finger-creadential"); //Utility.getDefaultSign("C8F40_4_FP");
		
		txtInfo = (TextView)findViewById(R.id.txtInfo);
		txtInfo2 = (TextView)findViewById(R.id.txtInfo2);
		txtTitle = (TextView)findViewById(R.id.txtTitle);
		txtTitle2 = (TextView)findViewById(R.id.txtTitle2);
		imgViewFinger = (ImageView)findViewById(R.id.img_finger);
		
		btnValidasi = (Button)findViewById(R.id.btnValid);
		btnSubmit = (Button)findViewById(R.id.btnSimpan);
		btnCancel = (Button)findViewById(R.id.btnCancel);
		
		btnSubmit.setEnabled(false);
		btnSubmit.setVisibility(View.INVISIBLE);
		
		btnValidasi.setText("Scan");
		btnValidasi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				enrolValid(btnValidasi.getText().toString());
			}
		});
		
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
		intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
		intentFilter.addAction(DeviceManager.USB_PERMISSION_REQUEST);
		
		setupSpin();
		
		spnFinger = (Spinner)findViewById(R.id.spinFinger);		
		spnFinger.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				posSpin = pos;
				
				if (!fromScan) {
					hasDeleted = false;
					strJari = spnFinger.getSelectedItem().toString();		
					strCodeJari = getCodeFinger(strJari);
					if (!checkMandatory(spnFinger.getSelectedItem().toString())) {					
						if (refreshImg) {
							Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
							BitmapDrawable bitmapDrawable=new BitmapDrawable(getResources(), b);
							imgViewFinger.setImageBitmap(createGhostIcon(bitmapDrawable, Color.BLACK, true));
							refreshImg = false;
							txtInfo.setText("");
							txtInfo2.setText("");
						}
					}else{
						AdapterDialog.showMessageDialog(Finger_New.this, "Informasi", strJari+" sudah pernah di-scan, silahkan pilih jari yang lain");
					}
				}
				fromScan = false;
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		
		((ImageView)findViewById(R.id.imgDelete)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				
				try {
					Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
					BitmapDrawable bitmapDrawable=new BitmapDrawable(getResources(), b);
					imgViewFinger.setImageBitmap(createGhostIcon(bitmapDrawable, Color.BLACK, true));
					txtInfo.setText("");
					txtInfo2.setText("");				
					
					if (spnFinger.getSelectedItem() != null) {
						hasSign.clear();
						deleteImgFinger();
						

						btnValidasi.setEnabled(false);
						btnValidasi.setVisibility(View.GONE);
						hasFinishMandatory = false;
						vJari.clear();
						spnFinger.setAdapter(null);
						setupSpin();
						totalScan = 0;
						txtTitle.setText( "Silahkan Scan jari anda");
						txtTitle2.setText( "");
						enroll(getApplicationContext());
						
					}
					hasDeleted = true;
					
				} catch (Exception e) {
					imgViewFinger.setImageResource(0);
				}
			}
		});
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				//pindahkan kesini filenya
				Utility.copyFile(bufferFile, returnFile);	
				intent.putExtra("flag", "submit");
				if (hasDeleted) {
					intent.putExtra("deleted", "Y");
				}else{
					intent.putExtra("deleted", "N");
				}
				setResult(RESULT_OK,intent);
				finish();
			}
		});
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialogCalcel();
			}
		});
		
		
		
		if (getIntent()!=null && getIntent().getStringExtra("image")!=null) {
			String pf =getIntent().getStringExtra("image");
			if (new File(pf).exists()) {
				 Utility.copyFile(pf, bufferFile);
				 Utility.copyFingerImageFile(bufferFile, bufferFile+"png");					 
				 OpenImage(imgViewFinger,bufferFile+"png" );
			}
			
		}
		

		registerReceiver(_broadcastReceiver, intentFilter);
	}
	
	private void enrolValid(String param){
		try {
			if (param.equalsIgnoreCase("Scan")) {
				enroll(getApplicationContext());
				btnValidasi.setVisibility(View.INVISIBLE);
				txtInfo.setText("");
				txtInfo2.setText("");
				txtTitle.setText("Silahkan Scan jari anda");
			}else{
				indetify(getApplicationContext());	
				
//				btnValidasi.setVisibility(View.INVISIBLE);
				imgViewFinger.setVisibility(View.INVISIBLE);
				
				((ImageView)findViewById(R.id.imgDelete)).setVisibility(View.INVISIBLE);
//				Utility.setAdapterSpinner(Finger_New.this, (Spinner)findViewById(R.id.spinFinger), new Vector());
				((Spinner)findViewById(R.id.spinFinger)).setEnabled(false);
				
				txtInfo.setText("");
				txtInfo2.setText("");
				txtTitle.setText("Silahkan Swap jari anda untuk validasi");
			}
		} catch (Exception e) {
			txtTitle.setText("maaf, USB Fingerprint belum terpasang");
		}
		
		
	}
	
	private void dialogCalcel(){
		AlertDialog.Builder builder = new AlertDialog.Builder(Finger_New.this);
		builder.setTitle("Informasi");
		builder.setMessage("Apakah anda ingin cancel Scan finger print ?");
		builder.setCancelable(false);
		
		builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				deleteImgFinger();
				Intent intent = new Intent();
				//pindahkan kesini filenya
				intent.putExtra("flag", "cancel");
				if (hasDeleted) {
					intent.putExtra("deleted", "Y");
				}else{
					intent.putExtra("deleted", "N");
				}
				setResult(RESULT_OK,intent);
				finish();
			}
		});
		
		builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void deleteImgFinger(){
		Recordset data = Connection.DBquery("SELECT * FROM TBL_PATH_Finger where orderCode ='"+Generator.currOrderCode+"'");
		if (data.getRows() > 0) {
			for (int i = 0; i < data.getRows(); i++) {
				Utility.deleteFile(data.getText(i, "path"));
				Connection.DBdelete("TBL_PATH_Finger", "path=?", new String[]{data.getText(i, "path")});
			}
		}
	}
	
	private void setupSpin(){
		Recordset data = Connection.DBquery("select * from MST_FINGER where status ='1' order by sequence ASC");
		if (data.getRows() > 0) {
			vJari = data.getRecordFromHeader("FINGER_DESC");
			Utility.setAdapterSpinner(Finger_New.this, (Spinner)findViewById(R.id.spinFinger), vJari);
		}		
		
		ENROLMENT_COUNT = vJari.size();
	}
	
	private String getCodeFinger(String name){
		Recordset data = Connection.DBquery("select finger_id from MST_FINGER where finger_desc ='"+name+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "finger_id");
		}
		return "";
	}
	
	private boolean checkMandatory(String param){
		for (int i = 0; i < hasSign.size(); i++) {
			if (param.equalsIgnoreCase(hasSign.elementAt(i).toString())) {
				return true;
			}
		}
		return false;
	}
	
	private boolean fullFinishMandatory(){
		int minReq = Global.getInt("min.finger.req", 1);
		if (hasSign.size() >= minReq) {
			return true;
		}
		return false;
	}
	
	public static void OpenImage(ImageView context, String path) {
		try {
			Bitmap b=BitmapFactory.decodeFile(path);
			if (b!=null) {
				context.setImageBitmap(b);
				context.setVisibility(View.VISIBLE);
			}else {
				context.setVisibility(View.GONE);
			}
			
		} catch (Exception e) {
			context.setVisibility(View.GONE);
		}
	}

	public void onPause() {
		super.onPause();
		releaseBiometricsSystem();
	}
	
	private void releaseBiometricsSystem() {
		if (_biometricsSystem != null) {
			_biometricsSystem.release();
			_biometricsSystem = null;
		}
	}

	public void onResume() {
		super.onResume();
		 if (DeviceManager.isDeviceAttached(this) == true &&  DeviceManager.hasPermission(this) == true) {
				prepareBiometricsSystem(this);
		 }else if (DeviceManager.isDeviceAttached(this) == true &&  DeviceManager.hasPermission(this) == false) {
			 DeviceManager.requestPermission(this);
		 }
	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(_broadcastReceiver);
	}

	
	private BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			//<usb-device vendor-id="1027" product-id="24596" /> <!-- Q180Mini -->
			if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED) == true) {
				//180 Mini is not detected
				txtTitle.setText( "Finger Scanner Tidak Terdeteksi");
				txtTitle2.setText( "");
			} else if (intent.getAction().equals(DeviceManager.USB_PERMISSION_REQUEST) == true) {
				if (DeviceManager.isDeviceAttached(context) == false)
					return;

				if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false) == true) {
					//180 Mini is detected
					prepareBiometricsSystem(context);
					txtInfo.setText( "Silahkan Scan jari anda");
					enroll(getApplicationContext());
					
					//jika permissin berhasil, button scan hilang
					btnValidasi.setVisibility(View.INVISIBLE);
				} else {
					//No USB permission
					txtTitle.setText( "Permission USB gagal");
					txtTitle2.setText( "");
				}
			}else  if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED) == true) {
				if (_biometricsSystem!=null) {					
				}else{
					DeviceManager.requestPermission(context);
				}
				txtTitle.setText( "USB Finger Print Sudah Terpasang");
				txtTitle2.setText( "Silahkan Scan jari anda");
				btnValidasi.setVisibility(View.INVISIBLE);
				 
			}
		}
	};
	
	protected void prepareBiometricsSystem(Context context) {
		// ! [BiometricsSystem.Constructor]
		 
		Sensor sensor = new FPC1080Sensor(context);
		Storage storage = new MX25LStorage(context);

	 	_biometricsSystem = new BiometricsSystem(sensor, storage);
		// ! [BiometricsSystem.setMatchingEngine]
		_biometricsSystem.setMatchingEngine(new FPC1080MatchingEngine());
		// ! [BiometricsSystem.setMatchingEngine]

		final FPC1080MatchingEngine nikitaMatchingEngine = new FPC1080MatchingEngine();
		_biometricsSystem.setMatchingEngine(new MatchingEngine() {
			public int identify(List templatedata, BiometricSample arg1, int arg2) {
				List<byte[]> templatedataL = new ArrayList<byte[]>(); 
				try {
					FileInputStream fis = new FileInputStream(bufferFile);
					DataInputStream dis = new DataInputStream(fis);
					int i = dis.readInt(); 
					byte[] none = new byte[i];
					dis.read(none,0,i);
					
					 i = dis.readInt(); 
					 none = new byte[i];
					 dis.read(none,0,i);
					 
					 templatedataL.add(none);
					
					dis.close();
					fis.close();
				} catch (Exception e) { } 
				
				
				
				
				return nikitaMatchingEngine.identify(templatedataL, arg1, arg2);
			}

			public byte[] enroll(byte[] templatedata, BiometricSample arg1) {
				byte[] res = nikitaMatchingEngine.enroll(templatedata, arg1);
				bTemplate = res;
				return res;
			}
		});

		_biometricsSystem.setBiometricSampleListener(new BiometricSampleListener() {
			private BiometricSample _biometricsSample;

			public void onWaitingForSample() {
				// String msg =
				// "Please swipe your finger over the sensor";
			}

			public void onSampleOn() {
				// needrunoinui'
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						imgViewFinger.clearColorFilter();
						// imgViewFinger.setColorFilter(Color.rgb(0xff, 0xff, 0xff),Mode.SRC_IN);
					}
				});
			}

			public void onSampleOff() {
				// ! [BiometricSample.get]
				bitmap = (Bitmap) _biometricsSample.get();
				// ! [BiometricSample.get]
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// needrunoinui
						//imgViewFinger.setImageBitmap(bitmap);
						BitmapDrawable bitmapDrawable=new BitmapDrawable(getResources(), bitmap);
						imgViewFinger.setImageBitmap(createGhostIcon(bitmapDrawable, Color.BLACK, true));								
					}
				});
				

			}

			public void onSampleCaptured(BiometricSample biometricSample) {
				_biometricsSample = biometricSample;
			}
		});

	}
	
	protected void enroll(Context context) {
		
		_biometricsSystem.deleteTemplate(0);
//		_biometricsSystem.enroll(0, 100);
		_biometricsSystem.enroll(0, ENROLMENT_COUNT);
		_biometricsSystem.setEnrollmentListener(new EnrollmentListener() {
			
			public void onAccepted(final int progress) {
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						
						hasDeleted = false;
						totalScan++;
						
//						if (!btnValidasi.getText().toString().equalsIgnoreCase("Scan")) {
//							btnValidasi.setEnabled(false);
//						}
						
						if (!checkMandatory(spnFinger.getSelectedItem().toString())) {
							hasSign.add(spnFinger.getSelectedItem().toString());
							refreshImg = true;
							
							if (fullFinishMandatory()){
								
								if (!hasFinishMandatory) {
									
									txtTitle.setText("Scan Seluruh jari mandatory telah terpenuhi");
									txtTitle2.setText("Silahkan validasi");
									btnValidasi.setEnabled(true);
									btnValidasi.setVisibility(View.VISIBLE);
									btnValidasi.setText("Validasi");
									hasFinishMandatory = true;

								}
							}
							
							//untuk save gambar jadi sesuai jml 
							try {			
								if (spnFinger.getSelectedItem() != null) {
									
									txtInfo.setText("Scan " +spnFinger.getSelectedItem().toString() + " Berhasil");	
									txtInfo2.setText("Silahkan scan jari berikutnya");
									
									fromScan = true;
									
									imgFinger = Utility.getDefaultImagePath(getIntent().getStringExtra("fileFinger")+"_"+getCodeFinger(spnFinger.getSelectedItem().toString()));
									FileOutputStream fos =  new FileOutputStream(imgFinger);
									Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
									Canvas s =new Canvas(b);			 
									imgViewFinger.draw(s);	
									
									b.compress(CompressFormat.PNG, 100, fos);
									fos.flush();
									fos.close();
									
									String path = Utility.getDefaultImagePath(getIntent().getStringExtra("fileFinger")+"_"+getCodeFinger(spnFinger.getSelectedItem().toString()));
									Connection.DBcreate("TBL_PATH_Finger", "activityId", "path", "orderCode");
//									Connection.DBdelete("TBL_PATH_Finger", " activityId =? ", new String[] {Generator.currModelActivityID} );
									Connection.DBinsert("TBL_PATH_Finger", "activityId="+Generator.currModelActivityID, "path="+path, "orderCode="+Generator.currOrderCode);
								
								
									ByteArrayOutputStream bos = new ByteArrayOutputStream();
									BitmapDrawable bitmapDrawable=new BitmapDrawable(new FileInputStream(imgFinger));
									createGhostIcon(bitmapDrawable, Color.BLACK, true).compress(CompressFormat.PNG, 100, bos);	
									
									byte[] out = bos.toByteArray();	
									bos=null;
									
									
									fos = new FileOutputStream(bufferFile);
									DataOutputStream dos = new DataOutputStream(fos);
									dos.writeInt(out.length ) ;
									dos.write(out) ;
									dos.writeInt(bTemplate.length) ;
									dos.write(bTemplate ) ;							 
									fos.flush();
									
									
									vJari.remove(spnFinger.getSelectedItem().toString());
									spnFinger.setAdapter(null);
									Utility.setAdapterSpinner(Finger_New.this, spnFinger, vJari);
									
//									if ((posSpin+1) != vJari.size()) {
//										spnFinger.setSelection((posSpin+1));
//									}
									
								
								}
								
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
						else{	
							
							_biometricsSystem.deleteTemplate(0);
							totalScan--;
							_biometricsSystem.enroll(0, ENROLMENT_COUNT-totalScan);						
							showMessageDialog(Finger_New.this, "Informasi", spnFinger.getSelectedItem().toString() + " sudah  dilakukan, silahkan pilih jari yang lain ");
						}
											
					}
				});
			}
			
			public void onRejected() {
				// Question swiping
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						hasDeleted = false;
						txtInfo.setText( "Scan "+strJari + " Gagal");
					}
				});
			}

			public void onFinished(byte[] templateData) {				
				// Finish enrolment
				try {
//					imgFinger = Utility.getDefaultTempPath(getIntent().getStringExtra("fileFinger")+"_"+strCodeJari);
					if (spnFinger.getSelectedItem() != null) {
						hasDeleted = false;
						imgFinger = Utility.getDefaultImagePath(getIntent().getStringExtra("fileFinger")+"_"+getCodeFinger(spnFinger.getSelectedItem().toString()));
						FileOutputStream fos =  new FileOutputStream(imgFinger);
						Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
						Canvas s =new Canvas(b);			 
						imgViewFinger.draw(s);							 
						b.compress(CompressFormat.PNG, 100, fos);
						fos.flush();
						fos.close();
						
						String path = Utility.getDefaultImagePath(getIntent().getStringExtra("fileFinger")+"_"+getCodeFinger(spnFinger.getSelectedItem().toString()));
						Connection.DBcreate("TBL_PATH_Finger", "activityId","path");
//						Connection.DBdelete("TBL_PATH_Finger", " activityId =? ", new String[] {Generator.currModelActivityID} );
						Connection.DBinsert("TBL_PATH_Finger", "activityId="+Generator.currModelActivityID, "path="+path);
								
				 	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
						BitmapDrawable bitmapDrawable=new BitmapDrawable(new FileInputStream(imgFinger));
						createGhostIcon(bitmapDrawable, Color.BLACK, true).compress(CompressFormat.PNG, 100, bos);					
						
						byte[] out = bos.toByteArray();	
						bos=null;								
						
						fos = new FileOutputStream(bufferFile);
						DataOutputStream dos = new DataOutputStream(fos);
						
						dos.writeInt(out.length ) ;
						dos.write(out ) ;
						dos.writeInt(templateData.length) ;
						dos.write(templateData ) ;							 
						fos.flush();
						fos.close();
					}
					
				} catch (Exception e) {}
			
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						btnValidasi.setEnabled(true);
						btnValidasi.setVisibility(View.VISIBLE);
						txtTitle.setText("Scan Seluruh jari telah terpenuhi, silahkan validasi");
						txtTitle2.setText("Silahkan validasi");
						
						txtInfo.setText("");	
						txtInfo2.setText("");
						
						spnFinger.setEnabled(false);
						spnFinger.setAdapter(null);
						((ImageView)findViewById(R.id.imgDelete)).setVisibility(View.GONE);
					}
				});
			}
		});

	}
	
	private void showMessageDialog(Context context, String title, String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				if (!btnValidasi.getText().toString().equalsIgnoreCase("Scan")) {
					btnValidasi.setEnabled(true);
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private Bitmap createGhostIcon(Drawable src, int color, boolean invert) {
	    int width = src.getIntrinsicWidth();
	    int height = src.getIntrinsicHeight();
	    if (width <= 0 || height <= 0) {
	        throw new UnsupportedOperationException("Source drawable needs an intrinsic size.");
	    }

	    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap);
	    Paint colorToAlphaPaint = new Paint();
	    int invMul = invert ? -1 : 1;
	    colorToAlphaPaint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{
	            0, 0, 0, 0, Color.red(color),
	            0, 0, 0, 0, Color.green(color),
	            0, 0, 0, 0, Color.blue(color),
	            invMul * 0.213f, invMul * 0.715f, invMul * 0.072f, 0, invert ? 255 : 0,
	    })));
	    canvas.saveLayer(0, 0, width, height, colorToAlphaPaint, Canvas.ALL_SAVE_FLAG);
	    canvas.drawColor(invert ? Color.WHITE : Color.BLACK);
	    src.setBounds(0, 0, width, height);
	    src.draw(canvas);
	    canvas.restore();
	    return bitmap;
	}
	
	protected void indetify(Context context) {
		try {
			_biometricsSystem.identify(_security);
			_biometricsSystem
					.setIdentificationListener(new IdentificationListener() {
						public void onGranted(int templateId) {
							String msg = "template id: " + templateId;
							runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									btnSubmit.setEnabled(true);
									btnSubmit.setVisibility(View.VISIBLE);
									imgViewFinger.setVisibility(View.VISIBLE);
									txtInfo.setText( "Validasi Diterima");
									Utility.setAdapterSpinner(Finger_New.this, (Spinner)findViewById(R.id.spinFinger), new Vector());
								}
							});
						}

						public void onDenied() {
								runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									btnSubmit.setEnabled(false);
									imgViewFinger.setVisibility(View.VISIBLE);
									txtInfo.setText( "Validasi Gagal");
								}
							});
						}
					});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stubd
		dialogCalcel();
	}
}
