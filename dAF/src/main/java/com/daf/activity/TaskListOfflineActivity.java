package com.daf.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Vector;

//import com.google.android.gms.internal.db;
import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.service.SendPendingImage;
import com.salesforce.utility.Utility;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class TaskListOfflineActivity extends Activity implements
		OnItemClickListener, OnItemLongClickListener {
	private ListView listview;
	private Button btnNew;	
	private Vector<String> listHideInfo = new Vector<String>();
	public static boolean draft;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_task_list_offline);
		
		((TextView) findViewById(R.id.textJudul)).setText("Tasklist Offline");

		// untuk menghentikan backroud service SendPendingImage agar tidak
		// mengirim saat ui di buka
//		Intent intent = new Intent("com.salesforce.service.SendPendingImage");
//		sendBroadcast(intent);
		Connection.DBcreate("trn_tasklist_offline","applno","activityId","orderId","cust_name","created_date","data","sent");
		// end
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView1))
				.setImageResource(android.R.drawable.ic_menu_revert);
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView1))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						finish();
					}
				});

		listview = (ListView) findViewById(R.id.listTasklistOffline);
		listview.setOnItemClickListener(this);
		listview.setOnItemLongClickListener(this);
		btnNew = (Button) findViewById(R.id.btnNewTasklistOffline);
		btnNew.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String idOne = UUID.randomUUID().toString();
				draft = false;
				loadForm(idOne.substring(0, idOne.indexOf("-")), 0);

			}
		});
		// listview.setOnItemLongClickListener(this);
		loadDataListView();
		if(getIntent().getStringExtra("tasklistOffline")!=null&&getIntent().getStringExtra("tasklistOffline").equalsIgnoreCase("offline")){
			String idOne = UUID.randomUUID().toString();
			draft = false;
			loadForm(idOne.substring(0, idOne.indexOf("-")), 0);
		}

	}

	private void loadDataListView(){
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		Vector<String> header  = new Vector<String>();
		header.add("id");		
		header.add("Appl No");
		header.add("Activity id");
		header.add("Order id");
		header.add("Nama");
		header.add("Created");
		header.add("Data");
		header.add("Sent");
		
		listHideInfo.add("id");
		listHideInfo.add("Activity id");
		listHideInfo.add("Order id");		
		listHideInfo.add("Data");
		listHideInfo.add("Sent");
	
		Recordset rst = Connection.DBquery("SELECT * from trn_tasklist_offline");
		
	
		try{
			ArrayList<String> field =  rst.getAllHeaderList();
//			header.clear();
			data.clear();
//			for(String c : rst.getAllHeaderList()){
//			header.addAll(a);			
//			data.addAll(rst.getAllDataVector());
			for(int i=0;  i <rst.getAllDataVector().size(); i++){
				Vector<String> c = rst.getAllDataVector().get(i);
				String d = c.get(3);
				if(!QueryClass.isOndataPending(d)){
					data.add(c);
				}			
			}	
//			}			
			}catch(Exception e){
				e.printStackTrace();
				}
		setupListView(header,data);
		
	}

	private void setupListView(final Vector<String> field, Vector<Vector<String>> value) {
		AdapterConnector.setConnector(listview, value, R.layout.orderadapter,
				new AdapterInterface() {
					@Override
					public View onMappingColumn(int row, View v, Object data) {
						// LinearLayout container_img_icon =
						// (LinearLayout)v.findViewById(R.id.container_img_icon);
						// ImageView new_name = (ImageView)
						// v.findViewById(R.id.img_icon);
						// new_name.setImageResource(R.drawable.icon_folder);
						LinearLayout container = (LinearLayout) v.findViewById(R.id.container_order_view);
//						Vector<String> fields = (Vector<String>) field;
						Vector<String> datas = (Vector<String>) data;
						
						try {
							for (int i = 0; i < datas.size(); i++) {
								View view = AdapterConnector.setInflateLinier(TaskListOfflineActivity.this,R.layout.iteminflate);
//								String a = field.get(i).toString();
//								if(!field.get(i).toString().equals("Order id")&&
//										!field.get(i).toString().equals("Activity id")&&
//										!field.get(i).toString().equals("id")&&
//										!field.get(i).toString().equals("Data")){
									if(!listHideInfo.contains(field.get(i).toString())){
									((TextView) view.findViewById(R.id.txtField)).setText(field.get(i).toString());
									if(datas.get(i)!=null){
										((TextView) view.findViewById(R.id.txtValue)).setText(datas.get(i).toString());
									}else{
										((TextView) view.findViewById(R.id.txtValue)).setText("");
									}								
								container.addView(view);
								}								
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						v.setTag(datas);
						return v;
					}
				});

	}
	
	public void refresh(){
		if(listview!=null){
			listview.setAdapter(null);
			loadDataListView();
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Vector<String> all = (Vector<String>) arg1.getTag();

		Generator.freez = true;
//		String a = Generator.currModelActivityID;
		if(all.get(1)!=null){
			if(!all.get(1).equalsIgnoreCase("")){
				Generator.applno = all.get(1);
			}	
		}
		draft = true;
		Generator.currAppMenu = "tasklistoffline";
//		Generator.currModel = rst.getText(0, 0);
//		Generator.currOrderTable = rst.getText(0, 2);
		Generator.currOrderCode = all.get(3);
		Generator.currBufferedView = QueryClass.getDataPendingNView(all.get(3));
		Generator.bu = "";			
		Generator.freez = true;
		Utility.activity = TaskListOfflineActivity.this;
		Generator.currOrderType = "tasklistoffline";
		Generator.isTasklist= false;
				
		if(all.get(6)!=null){
			Generator.openGenerator(TaskListOfflineActivity.this, all.get(6), true);	
		}
		
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		final Vector<String> all = (Vector<String>) arg1.getTag();
		
		AdapterDialog.showDialogTwoBtn(TaskListOfflineActivity.this, "Confirmation", "Apakah anda inggin menghapus order ini ? ", "ya", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deleteTasklistOffline(all.get(3));				
			}
		}, "Batal", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});				
		return true;
	}

	private void loadForm(String orderId, int pos) {

		Recordset rsts = Connection.DBquery("select * from application");
		Generator.currApplication = rsts.getText(0, "app_id");
		 Recordset rst = Connection.DBquery("SELECT model_id,model_name, model_order_tablename FROM model WHERE model_id='M23"+Generator.currApplication+"'");

		if (rst.getRows() > 0) {			
			// Generator.currBufferedView = getBufferedView(pos);
//			String a = Generator.currModelActivityID;
			Generator.currAppMenu = "tasklistoffline";
			Generator.currModel = rst.getText(0, 0);
			Generator.currOrderTable = rst.getText(0, 2);
			Generator.currOrderCode = orderId;
			Generator.currOrderType = "tasklistoffline";
			Generator.bu = "";			
			Generator.freez = true;
			Utility.activity = TaskListOfflineActivity.this;
			Generator.isTasklist=false;
			Connection.DBinsert("trn_tasklist_offline","orderId="+Generator.currOrderCode,"cust_name=","created_date="+Utility.getDate());
			Generator.openGenerator(TaskListOfflineActivity.this,Generator.currApplication, Generator.currModel,Generator.currOrderTable, Generator.currOrderCode);
			return;
		} else {
			AdapterDialog.showMessageDialog(TaskListOfflineActivity.this,
					"Informasi", "Maaf, Model tidak ada");
		}
	}
	
	public void deleteTasklistOffline(String orderId){
		Connection.DBdelete("trn_tasklist_offline", "orderId="+orderId);	
		Connection.DBdelete("LOG_IMAGE" ,"applno="+orderId);
		String userIdFrom = Utility.getSetting(TaskListOfflineActivity.this, "userid",
				"").trim();
		String a = userIdFrom+orderId;
		// ambil semua data di folder images
		String[] imagesFiles = new File(Utility.getDefaultImagePath()).list();
		for(String file :imagesFiles){
			if(file.contains(a)){
			File tmp = new File(Utility.getDefaultImagePath()+"/"+file);
			Utility.deleteFile(tmp.toString());
			}
		}
		// ambil semua data di folder tmp
		String[] tmpFiles = new File(Utility.getDefaultTempPath()).list();
		for(String file :tmpFiles ){
			if(file.contains(a)){
			File tmp = new File(Utility.getDefaultTempPath()+file);
			Utility.deleteFile(tmp.toString());
			}
		}
		
		deleteFingerImg(orderId);
		
		refresh();	
	}
	
	public static void deleteFingerImg(String orderCode){
		Recordset data = Connection.DBquery("select * from TBL_PATH_Finger where orderCode = '"+Generator.currOrderCode+"'");
		for (int i = 0; i < data.getRows(); i++) {
			Utility.deleteFile(data.getText(i, "path"));
		}
		Connection.DBdelete("TBL_PATH_Finger", "orderCode=?", new String[]{Generator.currOrderCode});
	}
	
	@Override
	public void onBackPressed() {

		super.onBackPressed();
//              seno 09:12 09/12/19
//		SendPendingImage.uiActive = false;
	}

}
