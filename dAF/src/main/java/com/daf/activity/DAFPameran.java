package com.daf.activity;

import java.util.UUID;
import java.util.Vector;

import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.database.Connection;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.service.SendPendingImage;
import com.salesforce.utility.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class DAFPameran extends Activity {
	private String table_name;
	private UUID idOne;
	private Spinner spnCoyDP, spnCustTypeDP, spnLobDP, spnPlatformDP;
	private Button nextButton;
	private boolean hitOne = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dafpameran);
		
		((ImageView)findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.imageView2)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.textJudul)).setText("E-Form Pameran");
		/*
		Intent intent = new Intent("com.salesforce.service.SendPendingImage");
		sendBroadcast(intent);
		*/
		spnCoyDP = (Spinner)findViewById(R.id.coy);
		spnCustTypeDP = (Spinner)findViewById(R.id.cusType);
		spnLobDP = (Spinner)findViewById(R.id.bussType);
		spnPlatformDP = (Spinner)findViewById(R.id.platform);
		nextButton =(Button)findViewById(R.id.btnNextdp);
		idOne = UUID.randomUUID();	
		loadSpinner();
		
		table_name= Utility.getSetting(DAFPameran.this, "tbl_matchingData", "");
//		if(table_name!=null&&table_name!=""&&getIntent().getStringExtra("seno")==null){
////			Connection.DBupdate(table_name, "flag_dafpameran=N");
//			Connection.DBdelete(table_name);
//		}
						
		spnCoyDP.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				loadSpinnerByCoyId(QueryClass.getCoyId(spnCoyDP.getSelectedItem().toString().toUpperCase()));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		nextButton.setOnClickListener(new View.OnClickListener() {
			
			public synchronized void onClick(View arg0){
				
				if (spnCoyDP.getSelectedItem() != null) {
					if (spnLobDP.getSelectedItem() != null && !spnLobDP.getSelectedItem().toString().equalsIgnoreCase("-- Select --")) {
						if (spnPlatformDP.getSelectedItem() != null) {
							settup();							
						}else
							AdapterDialog.showMessageDialog(DAFPameran.this, "Informasi", "Platform tidak boleh kosong");
					}else
						AdapterDialog.showMessageDialog(DAFPameran.this, "Informasi", "LOB tidak boleh kosong");
				}else
					AdapterDialog.showMessageDialog(DAFPameran.this, "Informasi", "Coy Name tidak boleh kosong");
				
			}
		});
	}

	
	/**
	 * sebagai validasi pengecekan apkah ini data entry baru atau
	 * data yang tadi sudah dibuat
	 * (masih dalam menu new entry)
	 */
	private void settup(){
		String orderID = String.valueOf(idOne);
		orderID = orderID.substring(0, orderID.indexOf("-"));
		
		if (isSameDocument()) {
			String record = QueryClass.getDataDraft(orderID);
			if (!record.equalsIgnoreCase("")) {
				Generator.currOrderCode = orderID;
				Generator.currBufferedView = QueryClass.getDataPendingNView(orderID);				
				setupValue();
				Generator.openGenerator(DAFPameran.this, record, true);
			}else{
				openGenerator(orderID);
			}
			
		}else{
//			openGenerator(orderID);
			Generator.currOrderCode = orderID;
			setupValue();
			if(table_name!=""){
				Connection.DBdelete(table_name);
			}
			Recordset rst = Connection.DBquery("SELECT modelid FROM MST_COMBINATION_MODEL WHERE coy_id ='"+Generator.coyId+"' AND cust_type ='"+
					spnCustTypeDP.getSelectedItem().toString().toUpperCase()+"' AND buss_unit = '"+QueryClass.getBussUnit(spnLobDP.getSelectedItem().toString().toUpperCase())+"' AND platform ='" +
					QueryClass.getPlatformID(spnPlatformDP.getSelectedItem().toString().toUpperCase())+"' AND FLAG_DAF_PAMERAN='DP'");
			if (rst.getRows() > 0) {
				Recordset data = Connection.DBquery("SELECT * FROM model WHERE model_id ='" + rst.getText(0, "modelid") + "'");
				if (data.getRows() > 0) {
					Generator.currModel = rst.getText(0, 0);
					Generator.currOrderTable = rst.getText(0, 2);
					saveToDraft();
					Generator.openGenerator(DAFPameran.this, Generator.currApplication, Generator.currModel, Generator.currOrderTable, Generator.currOrderCode);
					return;
				}else {
					AdapterDialog.showMessageDialog(DAFPameran.this, "Informasi", "Maaf, Model tidak ada");
				}
			}else {
				AdapterDialog.showMessageDialog(DAFPameran.this, "Informasi", "Maaf, Dokumen tidak ada");
			}
		}
		
	}
	
	/**
	 * untuk mengisi nilai pada spinner
	 */
	private void loadSpinner(){
		Vector<String> allData = new Vector<String>();
		Recordset data = Connection.DBquery("SELECT DISTINCT coy_name FROM MST_COY_ID");
		if (data.getRows() > 0) {
			allData = data.getRecordFromHeader("coy_name");
			Utility.setAdapterSpinner(DAFPameran.this, spnCoyDP, allData);
		}
		
		allData = new Vector<String>();
		allData.add("INDIVIDU");
		allData.add("CORPORATE");
		Utility.setAdapterSpinner(DAFPameran.this, spnCustTypeDP, allData);		
	}
	
	/**
	 * untuk mengisi nilai pada spinner berdasarkan coyid
	 */
	private void loadSpinnerByCoyId(String coyid){
		spnLobDP.setAdapter(null);
		spnPlatformDP.setAdapter(null);
		Vector<String> allData = new Vector<String>();
		allData.add("-- Select --");
		Recordset data = Connection.DBquery("SELECT buss_unit_desc FROM MST_BU WHERE coy_id='"+coyid+"'");
		for (int i = 0; i < data.getRows(); i++) {
			allData.add(data.getText(i, "buss_unit_desc"));
		}
//		if (data.getRows() > 0) {
//			allData = data.getRecordFromHeader("buss_unit_desc");
			Utility.setAdapterSpinner(DAFPameran.this, spnLobDP, allData);
//		}
		
		allData = new Vector<String>();
		data = Connection.DBquery("SELECT platform_desc FROM MST_PLATFORM WHERE coy_id='"+coyid+"'");
		if (data.getRows() > 0) {
			allData = data.getRecordFromHeader("platform_desc");
			Utility.setAdapterSpinner(DAFPameran.this, spnPlatformDP, allData);
		}
	}
	
	
	public void setupValue(){
		Generator.coyId = QueryClass.getCoyId(spnCoyDP.getSelectedItem().toString().toUpperCase());
		Generator.bu = QueryClass.getBussUnit(spnLobDP.getSelectedItem().toString().toUpperCase());
		Generator.platform = QueryClass.getPlatformID(spnPlatformDP.getSelectedItem().toString().toUpperCase());
		Generator.custtype = spnCustTypeDP.getSelectedItem().toString().toUpperCase();
		Generator.isNewEntry = true;
		Generator.freez = true;
		Generator.currAppMenu="dafpameran";
		Generator.currOrderType = "dafpameran";
	}
	
	/**
	 * 
	 * @param orderID
	 * untuk membuka generator yang berisi form2 sesuai dari bentukan service
	 */
	public void openGenerator(String orderID){
		Generator.currOrderCode = orderID;
		setupValue();
		
		Recordset rst = Connection.DBquery("SELECT modelid FROM MST_COMBINATION_MODEL WHERE coy_id ='"+Generator.coyId+"' AND cust_type ='"+
				spnCustTypeDP.getSelectedItem().toString().toUpperCase()+"' AND buss_unit = '"+QueryClass.getBussUnit(spnLobDP.getSelectedItem().toString().toUpperCase())+"' AND platform ='" +
				QueryClass.getPlatformID(spnPlatformDP.getSelectedItem().toString().toUpperCase())+"' AND FLAG_DAF_PAMERAN='DP'");
		
		
		if (rst.getRows() > 0) {
			Recordset data = Connection.DBquery("SELECT * FROM model WHERE model_id ='" + rst.getText(0, "modelid") + "'");
			if (data.getRows() > 0) {
				Generator.currModel = rst.getText(0, 0);
				Generator.currOrderTable = rst.getText(0, 2);				
				saveToDraft();
				Generator.openGenerator(DAFPameran.this, Generator.currApplication, Generator.currModel, Generator.currOrderTable, Generator.currOrderCode);
				return;
			}
		}else {
			AdapterDialog.showMessageDialog(DAFPameran.this, "Informasi", "Maaf, Dokumen tidak ada");
		}
	}
		
	/**
	 * dialog yang digunaka sebagai pemberitahuan penyimpanan draft
	 */
	private void dialogDraft(){
		AlertDialog.Builder builder = new AlertDialog.Builder(DAFPameran.this);
		builder.setCancelable(false);
		builder.setTitle("Informasi");
		builder.setMessage("Apakah anda ingin menyimpan ini sebagai Draft ?");
		builder.setCancelable(false);
		
		builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
//              seno 09:12 09/12/19
//				SendPendingImage.uiActive = false;
//				LostImageService.uiActive = false;
				finish();
			}
		});
		
		builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
				deleteFingerImg(Generator.currOrderCode);
//              seno 09:12 09/12/19
//				SendPendingImage.uiActive = false;
//				LostImageService.uiActive = false;
				finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void deleteFingerImg(String orderCode){
		Recordset data = Connection.DBquery("select * from TBL_PATH_Finger where orderCode = '"+Generator.currOrderCode+"'");
		for (int i = 0; i < data.getRows(); i++) {
			Utility.deleteFile(data.getText(i, "path"));
		}
		Connection.DBdelete("TBL_PATH_Finger", "orderCode=?", new String[]{Generator.currOrderCode});
	}
	
	/**
	 * untuk menyimpan data kedalam tabel draft
	 */
	private void saveToDraft(){
		Connection.DBcreate("DRAFT", "orderCode", "user", "coyID", "custType", "bussUnit", "platFormID", "date","time", "data");
		Connection.DBinsert("DRAFT", "orderCode="+Generator.currOrderCode ,"user="+Utility.getSetting(DAFPameran.this, "USERNAME", ""),"coyID="+QueryClass.getCoyId(spnCoyDP.getSelectedItem().toString().toUpperCase()),
				"custType="+spnCustTypeDP.getSelectedItem().toString().toUpperCase(), "bussUnit="+QueryClass.getBussUnit(spnLobDP.getSelectedItem().toString().toUpperCase()),
				"platFormID="+QueryClass.getPlatformID(spnPlatformDP.getSelectedItem().toString().toUpperCase()),"date="+Utility.Now(),"time="+Utility.getTime(),
				"data=");
	}
	
	/**
	 * validasi pengecekan apakah ini merupakan dokument entry yang sama
	 * seperti sebelumnya
	 * 
	 * @return
	 */
	private boolean isSameDocument(){
		try {
			if (QueryClass.getCoyId(spnCoyDP.getSelectedItem().toString()).equalsIgnoreCase(Generator.coyId) && spnCustTypeDP.getSelectedItem().toString().equalsIgnoreCase(Generator.custtype) && 
					QueryClass.getBussUnit(spnLobDP.getSelectedItem().toString()).equalsIgnoreCase(Generator.bu) && QueryClass.getPlatformID(spnPlatformDP.getSelectedItem().toString()).equalsIgnoreCase(Generator.platform)) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}
		
	@Override
	public void onBackPressed() {		
		dialogDraft();
	}
}
