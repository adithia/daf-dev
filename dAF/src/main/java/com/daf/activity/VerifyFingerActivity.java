package com.daf.activity;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingerq.FPC1080Sensor;
import com.fingerq.fingerprint.algorithm.matching.FPC1080MatchingEngine;
import com.fingerq.generic.biometrics.device.BiometricSample;
import com.fingerq.generic.biometrics.device.BiometricSampleListener;
import com.fingerq.generic.biometrics.device.Sensor;
import com.fingerq.generic.biometrics.matching.MatchingEngine;
import com.fingerq.helper.BiometricsSystem;
import com.fingerq.helper.EnrollmentListener;
import com.fingerq.helper.IdentificationListener;
import com.fingerq.io.MX25LStorage;
import com.fingerq.io.Storage;
import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.adapter.DeviceManager;
import com.salesforce.utility.Utility;

public class VerifyFingerActivity extends Activity{
	

	private ImageView imgViewFinger;
	private TextView txtInfo, txtInfo2;
	private Button btnValidasi;
	private int ENROLMENT_COUNT = 0;
	private Bitmap bitmap;
	private BiometricsSystem _biometricsSystem;
	private int _security = 484;
	private String bufferFile = "";
	private String imgFinger;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.validate_finger);
		
//		bufferFile = Utility.getDefaultPath("finger-creadential"); //Utility.getDefaultSign("C8F40_4_FP");
		bufferFile = Utility.getDefaultSign(getIntent().getStringExtra("path"));
//		bufferFile = getIntent().getStringExtra("path");
		
		txtInfo = (TextView)findViewById(R.id.txtInfo);
		txtInfo2 = (TextView)findViewById(R.id.txtInfo2);
		imgViewFinger = (ImageView)findViewById(R.id.imageView1);
		
		btnValidasi = (Button)findViewById(R.id.btnValid);
		
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
		intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
		intentFilter.addAction(DeviceManager.USB_PERMISSION_REQUEST);

		registerReceiver(_broadcastReceiver, intentFilter);
		
//		btnValidasi.setVisibility(View.GONE);
		btnValidasi.setText("Scan");
		btnValidasi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (btnValidasi.getText().toString().equalsIgnoreCase("Scan")) {
					txtInfo2.setText( "Silahkan tempelkan jari anda");
					txtInfo.setText("");
					enroll(getApplicationContext());
					indetify(getApplicationContext());
					btnValidasi.setVisibility(View.INVISIBLE);
				}
			}
		});
	}
	
	public static void OpenImage(ImageView context, String path) {
		try {
			Bitmap b=BitmapFactory.decodeFile(path);
			if (b!=null) {
				context.setImageBitmap(b);
				context.setVisibility(View.VISIBLE);
			}else {
				context.setVisibility(View.GONE);
			}
			
		} catch (Exception e) {
			context.setVisibility(View.GONE);
		}
	}

	public void onPause() {
		super.onPause();
		releaseBiometricsSystem();
	}
	
	private void releaseBiometricsSystem() {
		// ! [BiometricsSystem.release]
		if (_biometricsSystem != null) {
			_biometricsSystem.release();
			_biometricsSystem = null;
		}
		// ! [BiometricsSystem.release]
	}

	public void onResume() {
		super.onResume();
		 if (DeviceManager.isDeviceAttached(this) == true &&  DeviceManager.hasPermission(this) == true) {
				prepareBiometricsSystem(this);
		 }else if (DeviceManager.isDeviceAttached(this) == true &&  DeviceManager.hasPermission(this) == false) {
			 DeviceManager.requestPermission(this);
		 }
	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(_broadcastReceiver);
	}

	
	private BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			//<usb-device vendor-id="1027" product-id="24596" /> <!-- Q180Mini -->
			if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED) == true) {
				//180 Mini is not detected
				txtInfo.setText( "Finger Scanner Tidak Terdeteksi");
			} else if (intent.getAction().equals(DeviceManager.USB_PERMISSION_REQUEST) == true) {
				if (DeviceManager.isDeviceAttached(context) == false)
					return;

				if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false) == true) {
					//180 Mini is detected
					prepareBiometricsSystem(context);
					txtInfo2.setText( "Silahkan tempelkan jari anda");
					txtInfo.setText("");
					enroll(getApplicationContext());
					indetify(getApplicationContext());
					btnValidasi.setVisibility(View.INVISIBLE);
				} else {
					//No USB permission
					txtInfo.setText( "Permission USB gagal");
				}
			}else  if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED) == true) {
				if (_biometricsSystem!=null) {					
				}else{
					DeviceManager.requestPermission(context);
				}
				txtInfo.setText( "USB Finger Print Sudah Terpasang");
//				txtInfo2.setText( "Silahkan identifikasi jari anda");
//				btnValidasi.setText("Validasi");
//				btnValidasi.setVisibility(View.INVISIBLE);
//				enroll(getApplicationContext());
//				indetify(getApplicationContext());
			}
		}
	};
	
	protected void prepareBiometricsSystem(Context context) {
		
		Sensor sensor = new FPC1080Sensor(context);
		Storage storage = new MX25LStorage(context);

	 	_biometricsSystem = new BiometricsSystem(sensor, storage);
		_biometricsSystem.setMatchingEngine(new FPC1080MatchingEngine());

		final FPC1080MatchingEngine nikitaMatchingEngine = new FPC1080MatchingEngine();
		_biometricsSystem.setMatchingEngine(new MatchingEngine() {
			public int identify(List templatedata, BiometricSample arg1, int arg2) {
				List<byte[]> templatedataL = new ArrayList<byte[]>(); 
				try {
					FileInputStream fis = new FileInputStream(bufferFile);
					DataInputStream dis = new DataInputStream(fis);
					int i = dis.readInt(); 
					byte[] none = new byte[i];
					dis.read(none,0,i);
					
					 i = dis.readInt(); 
					 none = new byte[i];
					 dis.read(none,0,i);
					 
					 templatedataL.add(none);
					
					dis.close();
					fis.close();
				} catch (Exception e) { } 
				
				
				
				
				return nikitaMatchingEngine.identify(templatedataL, arg1, arg2);
			}

			public byte[] enroll(byte[] templatedata, BiometricSample arg1) {
				 

				return nikitaMatchingEngine.enroll(templatedata, arg1);
			}
		});

		_biometricsSystem.setBiometricSampleListener(new BiometricSampleListener() {
			private BiometricSample _biometricsSample;

			public void onWaitingForSample() {
				// String msg =
				// "Please swipe your finger over the sensor";
			}

			public void onSampleOn() {
				// needrunoinui'
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						imgViewFinger.clearColorFilter();
						// imgViewFinger.setColorFilter(Color.rgb(0xff, 0xff, 0xff),Mode.SRC_IN);
					}
				});
			}

			public void onSampleOff() {
				// ! [BiometricSample.get]
				bitmap = (Bitmap) _biometricsSample.get();
				// ! [BiometricSample.get]
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// needrunoinui
						//imgViewFinger.setImageBitmap(bitmap);
						BitmapDrawable bitmapDrawable=new BitmapDrawable(getResources(), bitmap);
						imgViewFinger.setImageBitmap(createGhostIcon(bitmapDrawable, Color.BLACK, true));								
					}
				});
				

			}

			public void onSampleCaptured(BiometricSample biometricSample) {
				_biometricsSample = biometricSample;
			}
		});

	}
	
	protected void enroll(Context context) {
//		btnValidasi.setEnabled(false);		
		try {
			_biometricsSystem.deleteTemplate(0);;
			_biometricsSystem.enroll(0, ENROLMENT_COUNT);
			_biometricsSystem.setEnrollmentListener(new EnrollmentListener() {
				public void onAccepted(int progress) {
					final String msg = "Scan Berhasil";
					runOnUiThread(new Runnable() {					
						@Override
						public void run() {
							txtInfo.setText( msg);	
						}
					});
				}
				
				public void onRejected() {
					// Question swiping
					runOnUiThread(new Runnable() {					
						@Override
						public void run() {
							txtInfo.setText( "Scan Gagal");
						}
					});
				}

				public void onFinished(byte[] templateData) {				
					// Finish enrolment
					try {
						imgFinger = Utility.getDefaultTempPath(getIntent().getStringExtra("path"));						
						FileOutputStream fos =  new FileOutputStream(imgFinger);
						Bitmap b = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
						Canvas s =new Canvas(b);			 
						imgViewFinger.draw(s);							 
						b.compress(CompressFormat.PNG, 100, fos);
						fos.flush();
						fos.close();
								
				 	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
						BitmapDrawable bitmapDrawable=new BitmapDrawable(new FileInputStream(imgFinger));
						createGhostIcon(bitmapDrawable, Color.BLACK, true).compress(CompressFormat.PNG, 100, bos);					
						
						byte[] out = bos.toByteArray();	
						bos=null;		
								
						
						fos = new FileOutputStream(bufferFile);
						DataOutputStream dos = new DataOutputStream(fos);
						
						dos.writeInt(out.length ) ;
						dos.write(out ) ;
						dos.writeInt(templateData.length) ;
						dos.write(templateData ) ;							 
						fos.flush();
						fos.close();
					} catch (Exception e) {
						// TODO: handle exception
					}
				
					runOnUiThread(new Runnable() {					
						@Override
						public void run() {
							btnValidasi.setEnabled(true);
							btnValidasi.setText("Validasi");
							txtInfo.setText("Silahkan Identifikasi jari anda");
							
						}
					});
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			AdapterDialog.showMessageDialog(VerifyFingerActivity.this, "Informasi", "Maaf, Finger Print mohon dipasang");
		}
		

	}
	
	private Bitmap createGhostIcon(Drawable src, int color, boolean invert) {
	    int width = src.getIntrinsicWidth();
	    int height = src.getIntrinsicHeight();
	    if (width <= 0 || height <= 0) {
	        throw new UnsupportedOperationException("Source drawable needs an intrinsic size.");
	    }

	    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap);
	    Paint colorToAlphaPaint = new Paint();
	    int invMul = invert ? -1 : 1;
	    colorToAlphaPaint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{
	            0, 0, 0, 0, Color.red(color),
	            0, 0, 0, 0, Color.green(color),
	            0, 0, 0, 0, Color.blue(color),
	            invMul * 0.213f, invMul * 0.715f, invMul * 0.072f, 0, invert ? 255 : 0,
	    })));
	    canvas.saveLayer(0, 0, width, height, colorToAlphaPaint, Canvas.ALL_SAVE_FLAG);
	    canvas.drawColor(invert ? Color.WHITE : Color.BLACK);
	    src.setBounds(0, 0, width, height);
	    src.draw(canvas);
	    canvas.restore();
	    return bitmap;
	}
	
	protected void indetify(Context context) {
		try {

			_biometricsSystem.identify(_security);
			_biometricsSystem
					.setIdentificationListener(new IdentificationListener() {
						public void onGranted(int templateId) {
							runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtInfo2.setText( "Identifikasi Diterima");
									txtInfo.setText( "");
									Intent intent = new Intent(VerifyFingerActivity.this, IdentifyFingerprint.class);
									setResult(1, intent);
									finish();
								}
							});
						}

						public void onDenied() {
								runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtInfo2.setText( "Identifikasi Gagal");
									txtInfo.setText( "");
								}
							});
						}
					});
		} catch (Exception e) {
			txtInfo2.setText( "Maaf, Finger Print mohon dipasang");
		}

	}
	
	

}
