package com.daf.activity;

import java.io.File;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.action.SendAction;
import com.salesforce.generator.ui.FingerUi;
import com.salesforce.generator.ui.PictureUi;
import com.salesforce.generator.ui.SignatureUi;
import com.salesforce.service.SchedulerService;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class OrderPendingActivity extends Activity{
	
	private Vector<String> vFields = new Vector<String>();
	private Vector<Vector<String>> vValue = new Vector<Vector<String>>();
	private ProgressDialog pDialog;
	String res = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		//check if app context null and app will close by system
		Utility.checkingAppContex(OrderPendingActivity.this);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.datapending);
		
//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}
		
//		17/12/2018
//		registerReceiver(brodcast, new IntentFilter(SchedulerService.BROADCAST_ACTION));

		//untuk menandakan ui sedang dipakai, jdi background service ditahan untuk running
	    Intent intent = new Intent("com.salesforce.service.SchedulerService");
		sendBroadcast(intent);
		
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		((TextView) findViewById(R.id.textJudul)).setText("Order Pending");
		
		((ListView)findViewById(R.id.listView1)).setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {
				String record = QueryClass.getDataPending((String)v.getTag());
				String isTasklist ="";
				isTasklist = QueryClass.isTasklits((String)v.getTag());
				Generator.currBufferedView = QueryClass.getDataPendingNView((String)v.getTag());
				if (isTasklist.equalsIgnoreCase("true")) {
					Generator.isTasklist = true;
				}else
					Generator.isTasklist = false;
				Generator.currAppMenu = "pending";
				Generator.freez = false;
				Generator.openGenerator(OrderPendingActivity.this, record, true);
				
			}
		});
		
		((ListView)findViewById(R.id.listView1)).setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View v,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				String record = QueryClass.getDataPending((String)v.getTag());
				showMessageDialog(record, (String)v.getTag());
				return true;
			}
		});
		
		loadData();
	}
	
	private void showMessageDialog(String data, final String act){
		AlertDialog.Builder builder = new AlertDialog.Builder(OrderPendingActivity.this);
		builder.setTitle("Informasi");
		builder.setMessage(data);
		builder.setNegativeButton("Delete Order", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				deleteCancelOrder(act);
			}
		});
		builder.setPositiveButton("Kirim Order", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
//				if (Utility.sendingData == false) {
					sendPending(act);
//				}else{
//					AdapterDialog.showAlertDialogFinish(OrderPendingActivity.this, "Informasi", "Data ini sedang dikirim melalui Scheduler, silahkan cek beberapa saat lagi");
//				}
				
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	/**
	 * untuk mengumpulkan semua data pending yang berada di table data_activity
	 */
	
	
	private void loadData(){
		Messagebox.showProsesBar(OrderPendingActivity.this, new Runnable() {
			
			@Override
			public void run() {
				// mendapatkan nama table
				Recordset data = Connection.DBquery("select orderCode, ndate, time, activityId, user, bussUnit from data_activity where appId = '" + Generator.currApplication + "'and status = '0'");
				if (data.getRows() >= 1) {
					vFields = data.getAllHeaderVector();
					try {
						vValue = data.getAllDataVector();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				if (vFields.size() > 0 && vValue.size() > 0) {
					setuplist(vValue);
				}else {
					Toast.makeText(getApplicationContext(), "Data Empty", Toast.LENGTH_LONG).show();
				}
				
			}
		});
	}
	
	/**
	 * untuk menampilkan data pending kedalam list
	 */	
	private void setuplist(Vector<Vector<String>> data){	 
		AdapterConnector.setConnector((ListView)findViewById(R.id.listView1), data, R.layout.draftpendinginflate, new AdapterInterface() {
			
			@Override
			public View onMappingColumn(int row, View v, Object data) {	
				try {

					Vector<String> all = (Vector<String>) data;
					v.setTag(all.elementAt(3).toString());
					((TextView)v.findViewById(R.id.orderCode)).setText(all.elementAt(4).toString());
					((TextView)v.findViewById(R.id.date)).setText(all.elementAt(5).toString());
					((TextView)v.findViewById(R.id.time)).setText(all.elementAt(1).toString()+", "+all.elementAt(2).toString());
					processIconStatus((String)v.getTag(), v);	
				} catch (Exception e) {}		
				return v;
			}
		});
	}
	
	/**
	 * untuk mengubah icon status pada data pending
	 */
	private void processIconStatus(String orderId, View view) {		
		
		ImageView img = (ImageView) view.findViewById(R.id.icon); 
		Recordset recordset = Connection.DBquery("SELECT * FROM data_activity WHERE orderCode='"+orderId+"';");
		
		if (recordset.getRows()>=1) {
			NfData data = new NfData(recordset.getText(0, "data"));
			String fdate = data.getData("INIT", "FINISHDATE");
			if (fdate.contains("-")) {
				img.setImageResource(R.drawable.icon_data_pending);
			}else{
				img.setImageResource(R.drawable.savedata);
			}
		}else{
			img.setImageResource(R.drawable.savedata);
		}
	}
	

	
	private String sendPending2(String actId){
		String res = "";
		Recordset recordset = Connection.DBquery("select * from data_activity where activityId = '"+actId+"'");
		String[] s = new File(Utility.getDefaultImagePath()).list();
		StringBuffer sbuf = new StringBuffer("|");
		if (s!=null) {
			for (int i = 0; i < s.length; i++) {
				sbuf.append(s[i]).append("|");
			}
		}
		String buffer = sbuf.toString();
		for (int i = 0; i < recordset.getRows(); i++) {
			NfData data = new NfData(recordset.getText(i, "data"));
			
			String fdate = data.getData("INIT", "FINISHDATE");
			String sdate = data.getData("INIT", "SENDDATE");
			
			String isTasklist = recordset.getText(i, "istasklist");
			String applNo = recordset.getText(i, "applno");
			String branchId = recordset.getText(i, "branchid");
			String userId = recordset.getText(i, "user");

			String orderCode = recordset.getText(i, "ordercode");
				
				if (fdate.contains("-")) {
			/*		Nset nset  = new Nset(data.getInternalObject());
					String[] form = nset.getObjectKeys();
				for (int j = 0; j < form.length; j++) {
					String dsd = form[j];
					String[] comp = nset.getData(form[j]).getObjectKeys();
					for (int k = 0; k < comp.length; k++) {
						String header =form[j];
						try {
							header=header.substring(1,header.length()-1);
						} catch (Exception e) { }

						if (buffer.contains("|"+data.getText(header, comp[k])+"|")) {
							Utility.i("sendingPendingActivity", "sendImage:"+data.getText(header, comp[k]));
							String typeComponent = data.getText(header, comp[k]);
							Vector<String> vTypeComp = Utility.splitVector(typeComponent, "_");
							typeComponent = vTypeComp.elementAt(1);
							boolean b = false;
							if (typeComponent.equalsIgnoreCase("3")) {
								b = SignatureUi.sendImage(recordset.getText(i, "activityid"), comp[k] , data.getText(header, comp[k]), true, isTasklist,userId, applNo);
							}else if(typeComponent.equalsIgnoreCase("1") || typeComponent.equalsIgnoreCase("0")){
								b = PictureUi.sendImage(recordset.getText(i, "activityid"), comp[k] , data.getText(header, comp[k]), true, isTasklist, userId, applNo);
							}else if (typeComponent.equalsIgnoreCase("4")) {
								b = FingerUi.sendImage(recordset.getText(i, "activityid"), comp[k], data.getText(header, comp[k]), true, isTasklist, userId, applNo);
							}
						}
					}
				}*/
				String sbuffer = Utility.replace(recordset.getText(i, "data"), "SENDDATE="+sdate, "SENDDATE="+Utility.Now());
				if (Utility.getNewToken()) {
					res = SendAction.sendActivityBackground(this, recordset.getText(i, "activityid"), recordset.getText(i, "ordercode"), recordset.getText(i, "appid"),
							sbuffer , isTasklist, applNo, branchId, true);
				}
				
				if (res.startsWith("LOGOUT")) {
					Utility.setSetting(getApplicationContext(), "ERRORMSG","KILL");
				}
				if (res.endsWith("OK")) {
					
					//14/12/2018
					Nset nset  = new Nset(data.getInternalObject());
					String[] form = nset.getObjectKeys();
					for (int j = 0; j < form.length; j++) {
						String dsd = form[j];
						String[] comp = nset.getData(form[j]).getObjectKeys();
						for (int k = 0; k < comp.length; k++) {
							String header =form[j];
							try {
								header=header.substring(1,header.length()-1);
							} catch (Exception e) { } 
							 
							if (buffer.contains("|"+data.getText(header, comp[k])+"|")) {
								Utility.i("sendingPendingActivity", "sendImage:"+data.getText(header, comp[k]));
								String typeComponent = data.getText(header, comp[k]);
								Vector<String> vTypeComp = Utility.splitVector(typeComponent, "_");
								typeComponent = vTypeComp.elementAt(1);							
								boolean b = false;							
								if (typeComponent.equalsIgnoreCase("3")) {
									b = SignatureUi.sendImage(recordset.getText(i, "activityid"), comp[k] , data.getText(header, comp[k]), true, isTasklist,userId, applNo);
								}else if(typeComponent.equalsIgnoreCase("1") || typeComponent.equalsIgnoreCase("0")){
									b = PictureUi.sendImage(recordset.getText(i, "activityid"), comp[k] , data.getText(header, comp[k]), true, isTasklist, userId, applNo);
								}else if (typeComponent.equalsIgnoreCase("4")) {
									b = FingerUi.sendImage(recordset.getText(i, "activityid"), comp[k], data.getText(header, comp[k]), true, isTasklist, userId, applNo);
								}
							}
						}
					}
					//14/12/2018
					
					Connection.DBupdate("data_activity","status=1" , "where" ,"activityId="+recordset.getText(i, "activityid"));
					if (isTasklist.equalsIgnoreCase("true")) {
						Connection.DBupdate("log_order","sent=Y", "where", "order_id="+orderCode);
					}
					// 04/04/2019
					if(Generator.currOrderType.equalsIgnoreCase("tasklistoffline")){
						Connection.DBupdate("trn_tasklist_offline","sent=Y", "where", "orderId="+Generator.currOrderCode);
						Connection.DBdelete("trn_tasklist_offline","orderId=?",new String[]{Generator.currOrderCode});
					}
					
					Connection.DBdelete("data_activity" ,"activityId="+recordset.getText(i, "activityid"));
					SendAction.deleteActivityTasklistTemporary();
					
				}
				else if (res.equalsIgnoreCase("Error data sudah ada")) {
					Connection.DBupdate("log_order","sent=Y", "where", "order_id="+orderCode);
					Connection.DBdelete("data_activity" ,"activityId="+recordset.getText(i, "activityid"));
					SendAction.deleteActivityTasklistTemporary();
					// 04/04/2019
					if(Generator.currOrderType.equalsIgnoreCase("tasklistoffline")){
						Connection.DBupdate("trn_tasklist_offline","sent=Y", "where", "orderId="+Generator.currOrderCode);
						Connection.DBdelete("trn_tasklist_offline","orderId=?",new String[]{Generator.currOrderCode});
					}
//					refreshOrder();
				}
				//13:02 14/11/2018 daff 3
				else if(res.startsWith("data_stream_rusak") ){	
					
					if(Generator.isTasklist==false){
						//new entry								
						Connection.DBdelete("data_activity", "activityId=?",new String[]{Generator.currModelActivityID});
						Connection.DBdelete("DRAFT", "order_id=?",new String[]{Generator.currOrderCode});
						OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
//						new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Gagal, "+resSentAction+"\r\n Tolong collect ulang", "result=234352:OK"));
						res = "Data yang di kirim gagal silahkan entry ulang";
					}else{								
						Connection.DBdelete("log_order", "order_id=?",new String[]{Generator.currOrderCode});
						Connection.DBdelete("data_activity", "activityId=?",new String[]{Generator.currModelActivityID});
						OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
						res = "Data yang di kirim gagal silahkan entry ulang";
//						new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Gagal, "+resSentAction+"\r\n Tolong collect ulang", "result=234352:OK"));
					}
					// 04/04/2019
					if(Generator.currOrderType.equalsIgnoreCase("tasklistoffline")){
//						Connection.DBupdate("trn_tasklist_offline","sent=Y", "where", "orderId="+Generator.currOrderCode);
						Connection.DBdelete("trn_tasklist_offline","orderId=?",new String[]{Generator.currOrderCode});
						
					}
//					refreshOrder();
					
				}
				//sampai sini
				else if (true && res.endsWith("SERVICEOFF")) {
					//20/08/2014
					Utility.setSetting(getApplicationContext(), "SERVICE", "");
				}
			}
		}
		
		return res;
	}
	
	private void sendPending(final String actId){
		Messagebox.showProsesBar(OrderPendingActivity.this, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				res = sendPending2(actId);
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				refreshOrder();
				AdapterDialog.showMessageDialog(OrderPendingActivity.this, "Informasi", res);
			}
		});
		
	}
	
	private void deleteCancelOrder(String actId){
		String[] s = new File(Utility.getDefaultImagePath()).list();
		StringBuffer sbuf = new StringBuffer("|");
		if (s!=null) {
			for (int i = 0; i < s.length; i++) {
				sbuf.append(s[i]).append("|");
			}
		}
		String buffer = sbuf.toString();
		Recordset rst = Connection.DBquery("select * from data_activity where activityId = '"+actId+"'");
		for (int i = 0; i < rst.getRows(); i++) {
			deleteSentActityResource(buffer, rst.getText(i, "data"));				
			Connection.DBdelete("data_activity" ,"activityId="+rst.getText(i, "activityid"));	
			Connection.DBdelete("log_order" ,"order_id="+rst.getText(i, "ordercode"));	
			Connection.DBdelete("LOG_IMAGE" ,"activityId="+rst.getText(i, "activityid"));
			//10:04 04/04/2019 daf3
			Connection.DBdelete("trn_tasklist_offline","orderId="+rst.getText(i, "ordercode"));
//			Connection.DBdelete("trn_tasklist_data","orderId="+rst.getText(i, "ordercode"));
//			Connection.DBdelete("trn_tasklist_data_object","orderId="+rst.getText(i, "ordercode"));
		}
		refreshOrder();
	}
	//14:58 14/11/2018 daff 3
	public static void deleteCancelOrder2(String actId){
		String[] s = new File(Utility.getDefaultImagePath()).list();
		StringBuffer sbuf = new StringBuffer("|");
		if (s!=null) {
			for (int i = 0; i < s.length; i++) {
				sbuf.append(s[i]).append("|");
			}
		}
		String buffer = sbuf.toString();
		Recordset rst = Connection.DBquery("select * from data_activity where activityId = '"+actId+"'");
		for (int i = 0; i < rst.getRows(); i++) {
			deleteSentActityResource(buffer, rst.getText(i, "data"));				
			Connection.DBdelete("data_activity" ,"activityId="+rst.getText(i, "activityid"));	
			Connection.DBdelete("log_order" ,"order_id="+rst.getText(i, "ordercode"));	
			Connection.DBdelete("LOG_IMAGE" ,"activityId="+rst.getText(i, "activityid"));
			//10:04 04/04/2019 daf3
			Connection.DBdelete("trn_tasklist_offline","orderId="+rst.getText(i, "ordercode"));
//			Connection.DBdelete("trn_tasklist_data","orderId="+rst.getText(i, "ordercode"));
//			Connection.DBdelete("trn_tasklist_data_object","orderId="+rst.getText(i, "ordercode"));
			
		}						
//		refreshOrder();
	}
	//sampai sini
	public static void deleteSentActityResource(String buffer, String stream){
		NfData data = new NfData(stream);		 
		Nset nset  = new Nset(data.getInternalObject());
		String[] form = nset.getObjectKeys();
		for (int j = 0; j < form.length; j++) {
			String[] comp = nset.getData(form[j]).getObjectKeys();
			for (int k = 0; k < comp.length; k++) {
				String header =form[j];
				try {
					header=header.substring(1,header.length()-1);
				} catch (Exception e) { } 
				 
				if (buffer.contains("|"+data.getText(header, comp[k])+"|")) {
					new File(Utility.getDefaultPath(data.getText(header, comp[k]))).delete();
					new File(Utility.getDefaultTempPath(data.getText(header, comp[k]))).delete();
					new File(Utility.getDefaultImagePath(data.getText(header, comp[k]))).delete();
				}
			}
		}	
	}
	
	private void refreshOrder(){
		vFields.clear();
		vValue.clear();
		((ListView)findViewById(R.id.listView1)).setAdapter(null);
		loadData();
	}
	
	private BroadcastReceiver brodcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			vFields.clear();
			vValue.clear();
			((ListView)findViewById(R.id.listView1)).setAdapter(null);
			loadData();
		}
	};
	
//	protected void onDestroy() {
//		unregisterReceiver(brodcast);
//	};
	
	public void onBackPressed() {
		SchedulerService.uiActive = false;
		finish();
	};
	
}
