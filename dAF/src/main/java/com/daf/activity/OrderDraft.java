package com.daf.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.QueryClass;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.Logout;
import com.salesforce.utility.Utility;

public class OrderDraft extends Activity{
		
	private ListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.datapending);
		
//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}
		
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		((TextView) findViewById(R.id.textJudul)).setText("Order Draft");
		
		list = (ListView)findViewById(R.id.listView1);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String record = QueryClass.getDataDraft((String)v.getTag());
				Generator.currAppMenu = "draft";
				String isTasklist ="";
				isTasklist = QueryClass.isTasklits((String)v.getTag());
				Generator.currBufferedView = QueryClass.getDataPendingNView((String)v.getTag());
				if (isTasklist.equalsIgnoreCase("true")) {
					Generator.isTasklist = true;
				}else
					Generator.isTasklist = false;
				Generator.currOrderCode = (String)v.getTag();
				Generator.freez = true;
				Generator.openGenerator(OrderDraft.this, record, true);
			}
		});
		loadData();
		
	}
	
	/**
	 * untuk mengumpulkan data dari tabel draft
	 */
	private void loadData(){
		List<String[]> dataDraft = new ArrayList<String[]>();
		Recordset data = Connection.DBquery("SELECT * FROM DRAFT WHERE user ='"+Utility.getSetting(getApplicationContext(), "USERNAME", "")+"'");
		if (data.getRows() > 0) {
			for (int i = 0; i < data.getRows(); i++) {
				if (data.getText(i, "data").equalsIgnoreCase("")) {
					Connection.DBdelete("DRAFT", "orderCode=?", new String[]{data.getText(i, "ordercode")});
				}else{					
					String[] strs = {data.getText(i, "ordercode"), data.getText(i, "date"), data.getText(i, "time")};
					dataDraft.add(strs);
				}
			}
		}
		setuplist(dataDraft);
	}
	
	/**
	 * untuk menampikan data kedalam list
	 */
	private void setuplist(List<String[]> data){
		AdapterConnector.setConnector(list, data, R.layout.draftpendinginflate, new AdapterInterface() {
			
			@Override
			public View onMappingColumn(int row, View v, Object data) {
				// TODO Auto-generated method stub
				String[] test = (String[])data;
				v.setTag(test[0]);
//				((TextView)v.findViewById(R.id.orderCode)).setText(test[0]);
				((TextView)v.findViewById(R.id.orderCode)).setText("DRAFT");
				((TextView)v.findViewById(R.id.date)).setText(test[1]);
				((TextView)v.findViewById(R.id.time)).setText(test[2]);
				return v;
			}
		});
	}

}
