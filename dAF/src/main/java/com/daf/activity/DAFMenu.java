package com.daf.activity;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.model.ModelMenuUtama;
import com.salesforce.service.DAFReciever;
import com.salesforce.stream.ImagePost;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Helper;
import com.salesforce.utility.LocationOffline;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import ai.advance.liveness.sdk.activity.LivenessActivity;

public class DAFMenu extends Activity {
    private Vector<ModelMenuUtama> vRecord = new Vector<ModelMenuUtama>();

    private Intent intent;
    private String result = "";
    private String appID;
    private String strFileName;
    private String strPathImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.menuapplication2);

        Utility.checkingAppContex(DAFMenu.this);

//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}

        DAFReciever.sendOrder(getApplicationContext());
        DAFReciever.checkExpiredTasklit(getApplicationContext());
        DAFReciever.sendPendingImage(getApplicationContext());
        DAFReciever.downloadListItem(getApplicationContext());
//		DAFReciever.sendLostImg(getApplicationContext());
        DAFReciever.autoLogout(getApplicationContext());

        new LocationOffline(getApplicationContext());

        deleteDraft();
        Generator.stateCurrModelActivityID = "";
        strPathImg = LoginActivity.strPathImg;

        appID = getIntent().getStringExtra("app_id");

        ((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);

        ((TextView) findViewById(R.id.textView1)).setText("Choose Menu");


        ((TextView) findViewById(R.id.textJudul)).setText("Menu");

        ((ListView) findViewById(R.id.lstApp)).setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position,
                                    long arg3) {
                if (Utility.getSetting(getApplicationContext(), "ERRORMSG", "").length() >= 3) {
                    finish();
                    return;
                }

                int responseCode = 0;
                Generator.isNewEntry = false;
                Generator.isTasklist = false;
                Utility.finishSign = false;
                Generator.currOrderCode = "";

                String menuName = vRecord.elementAt(position).getMenuName();
                String menuId = vRecord.elementAt(position).getMenuId();
                if (menuName == null) {
                    return;
                } else if (menuName.equals("New Entry")) {
                    intent = new Intent(DAFMenu.this, NewEntry.class);
                } else if (menuName.equals("Tasklist"))
                    intent = new Intent(DAFMenu.this, Tasklist.class);
                else if (menuName.equals("Tracking Order")) {
                    intent = new Intent(DAFMenu.this, TrackingOrder.class);
                } else if (menuName.equals("Order Pending"))
                    intent = new Intent(DAFMenu.this, OrderPendingActivity.class);
                else if (menuName.equals("Change Signature")) {
                    showDialog(0);
                    return;
                } else if (menuId.equalsIgnoreCase("identify")) {
//					intent = new Intent(DAFMenu.this, Finger_New.class);
                    intent = new Intent(DAFMenu.this, IdentifyFingerprint.class);
//					intent = new Intent(DAFMenu.this, VerifyFingerActivity.class);
//					intent.putExtra("path", Utility.getDefaultBio("finger"));
                } else if (menuId.equalsIgnoreCase("logimage")) {
                    intent = new Intent(DAFMenu.this, ImagePendingActivity.class);
                } else if (menuName.equals("Setting")) {
                    intent = new Intent(DAFMenu.this, SettingActivity.class);
                } else if (menuName.equals("Close Batch")) {
                    closebatch();
                    return;
                } else if (menuName.equals("Log Out")) {
                    showDialog(1);
                    return;
                } else if (menuName.equalsIgnoreCase("Draft")) {
                    intent = new Intent(DAFMenu.this, OrderDraft.class);
                } else if (menuName.equalsIgnoreCase("TaskList Offline")) {
                    intent = new Intent(DAFMenu.this, TaskListOfflineActivity.class);
                } else if (menuName.equalsIgnoreCase("TaskList Data")) {
                    intent = new Intent(DAFMenu.this, TaskListDataActivity.class);
                } else if (menuName.equalsIgnoreCase("E-Form Pameran")) {
                    intent = new Intent(DAFMenu.this, DAFPameran.class);
                } else if (menuName.equalsIgnoreCase("Liveness Detection")) {
                    intent = new Intent(DAFMenu.this, LivenessActivityTest.class);
                } else if (menuName.equalsIgnoreCase("Ocr KTP")) {
                    intent = new Intent(DAFMenu.this, OcrKtpActivity.class);
                }

                if (view.getTag() != null) {
                    intent.putExtra("menu_id", Helper.releaseTag((String) view.getTag(), false, true));
                    intent.putExtra("app_id", Helper.releaseTag((String) view.getTag(), true, false));
                }

                startActivityForResult(intent, 0);
            }
        });

        addMenuHarcode();


        if (getIntent().getStringExtra("To") != null) {
            Intent intent = null;
            if (getIntent().getStringExtra("To").equalsIgnoreCase("draft")) {
                intent = new Intent(DAFMenu.this, OrderDraft.class);
                intent.putExtra("To", getIntent().getStringExtra("To"));
            } else if (getIntent().getStringExtra("To").equalsIgnoreCase("pending")) {
                intent = new Intent(DAFMenu.this, OrderPendingActivity.class);
                intent.putExtra("To", getIntent().getStringExtra("To"));
            }

            startActivity(intent);
            return;
        }

    }

    /**
     * untuk menghapus draft yang tidak dikerjakan kembali
     * parameter berdasarkan global param
     */
    private void deleteDraft() {
        String[] s = new File(Utility.getDefaultImagePath()).list();
        StringBuffer sbuf = new StringBuffer("|");
        if (s != null) {
            for (int i = 0; i < s.length; i++) {
                sbuf.append(s[i]).append("|");
            }
        }
        String buffer = sbuf.toString();

        Recordset data = Connection.DBquery("SELECT date, orderCode, data FROM DRAFT");
        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {
                Calendar cal = Utility.convertDateDraft(data.getText(i, "date"));
                cal.set(Calendar.DATE, cal.get(Calendar.DATE) + Global.getInt("delete.draft", 0));
                Date today = new Date();
                if (today.after(cal.getTime())) {
                    deleteResource(buffer, data.getText(i, "data"));
                    Connection.DBdelete("DRAFT", "orderCode=?", new String[]{data.getText(i, "orderCode")});
                    Connection.DBdelete("DRAFT", "orderCode=?", new String[]{data.getText(i, "ordercode")});

                    //delete file gambar finger dan data ditabel
                    deleteImgFinger(data.getText(i, "ordercode"));
                    Connection.DBdelete("TBL_PATH_Finger", "orderCode=?", new String[]{data.getText(i, "ordercode")});
                }

            }
        }
    }

    private void deleteImgFinger(String orderCode) {
        Recordset data = Connection.DBquery("SELECT * FROM TBL_PATH_Finger where orderCode ='" + orderCode + "'");
        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {
                Utility.deleteFile(data.getText(i, "path"));
            }
        }
    }

    /**
     * @param buffer
     * @param stream untuk menghapus file2 yang berhubungan dengan draft
     */
    private static void deleteResource(String buffer, String stream) {
        NfData data = new NfData(stream);
        Nset nset = new Nset(data.getInternalObject());
        String[] form = nset.getObjectKeys();
        for (int j = 0; j < form.length; j++) {
            String[] comp = nset.getData(form[j]).getObjectKeys();
            for (int k = 0; k < comp.length; k++) {
                String header = form[j];
                try {
                    header = header.substring(1, header.length() - 1);
                } catch (Exception e) {
                }

                if (buffer.contains("|" + data.getText(header, comp[k]) + "|")) {
                    new File(Utility.getDefaultPath(data.getText(header, comp[k]))).delete();
                    new File(Utility.getDefaultTempPath(data.getText(header, comp[k]))).delete();
                    new File(Utility.getDefaultImagePath(data.getText(header, comp[k]))).delete();

                    new File(Utility.getDefaultPath(data.getText(header, comp[k]) + ".imgicon.png")).delete();
                    new File(Utility.getDefaultTempPath(data.getText(header, comp[k]) + ".imgicon.png")).delete();
                    new File(Utility.getDefaultImagePath(data.getText(header, comp[k]) + ".imgicon.png")).delete();
                    new File(Utility.getDefaultTempPath("finger-nikita.imgicon.png")).delete();
                    new File(Utility.getDefaultImagePath("finger-nikita.imgicon.png")).delete();

                    new File(Utility.getDefaultTempPath("finger-creadentialpng")).delete();
                    new File(Utility.getDefaultImagePath("finger-creadentialpng")).delete();
                }
            }
        }
    }

    public void closebatch() {
        Recordset record = Connection.DBquery("SELECT orderCode FROM data_activity WHERE AppId = '" + Generator.currApplication + "' and status = '0'");
        if (record.getRows() != 0) {
            showDialog(2);
        } else {
            showDialog(3);
        }
    }


    public void onBackPressed() {
        if (MenuApplicationActivity.oneapp) {
            final AlertDialog alertDialog = new AlertDialog.Builder(DAFMenu.this).create();
            alertDialog.setMessage("Apakah anda yakin akan keluar aplikasi ?");
            alertDialog.setCancelable(false);
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    finish();
                }
            });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        } else {
            super.onBackPressed();
        }

    }

    /**
     * menentukan menu dari aplikasi
     */
    private void addMenuHarcode() {
        Recordset rest = Connection.DBquery("SELECT * FROM mobileaccess  WHERE job_code = '" + Utility.JOBCODE + "'");
//		Recordset rest = Connection.DBquery("SELECT * FROM mobileaccess");

        if (rest.getRows() > 0) {
            ModelMenuUtama model = new ModelMenuUtama();



            if (rest.getText(0, "is_entry_order").equalsIgnoreCase("1")) {
                model.setMenuId("newentry");
                model.setMenuName("New Entry");
                model.setAppId(appID);
                model.setIconCount(R.drawable.icon_approval);
                vRecord.add(model);
            }

            if (rest.getText(0, "is_status_order").equalsIgnoreCase("1")) {
                model = new ModelMenuUtama();
                model.setMenuId("draft");
                model.setMenuName("Draft");
                model.setAppId(appID);
                model.setIconCount(R.drawable.draft);
                vRecord.add(model);
            }

            if (rest.getText(0, "is_status_order").equalsIgnoreCase("1")) {
                model = new ModelMenuUtama();
                model.setMenuId("pending");
                model.setMenuName("Order Pending");
                model.setAppId(appID);
                model.setIconCount(R.drawable.icon_data_pending);
                vRecord.add(model);
            }


            if (rest.getText(0, "is_tracking_order").equalsIgnoreCase("1")) {
                model = new ModelMenuUtama();
                model.setMenuId("trackorder");
                model.setMenuName("Tracking Order");
                model.setAppId(appID);
                model.setIconCount(R.drawable.track_order);
                vRecord.add(model);
            }

            if (rest.getText(0, "is_task_list").equalsIgnoreCase("1")) {
                model = new ModelMenuUtama();
                model.setMenuId("tasklist");
                model.setMenuName("Tasklist");
                model.setAppId(appID);
                model.setIconCount(R.drawable.icon_order);
                vRecord.add(model);
            }

//			if(rest.getText(0,"is_task_list_offline").equalsIgnoreCase("1")){
            model = new ModelMenuUtama();
            model.setMenuId("taskListOffline");
            model.setMenuName("TaskList Offline");
            model.setAppId(appID);
            model.setIconCount(R.drawable.icon_tasklist_offline);
            vRecord.add(model);

//			}

//				if(rest.getText(0,"is_task_list_data").equalsIgnoreCase("1")){
            model = new ModelMenuUtama();
            model.setMenuId("tasklisData");
            model.setMenuName("TaskList Data");
            model.setAppId(appID);
            model.setIconCount(R.drawable.icon_tasklist_data);
            vRecord.add(model);

//				}

//					if(rest.getText(0,"is_task_daf_pameran").equalsIgnoreCase("1")){
            model = new ModelMenuUtama();
            model.setMenuId("dafPameran");
            model.setMenuName("E-Form Pameran");
            model.setAppId(appID);
            model.setIconCount(R.drawable.icon_daf_pameran);
            vRecord.add(model);

            model = new ModelMenuUtama();
            model.setMenuId("livenessdetection");
            model.setMenuName("Liveness Detection");
            model.setAppId(appID);
            model.setIconCount(R.drawable.icon_approval);
            vRecord.add(model);

            model = new ModelMenuUtama();
            model.setMenuId("OcrKTP");
            model.setMenuName("Ocr KTP");
            model.setAppId(appID);
            model.setIconCount(R.drawable.img);
            vRecord.add(model);

//				}
			
			/*if (rest.getText(0, "is_status_order").equalsIgnoreCase("1")) {
				model = new ModelMenuUtama();
				model.setMenuId("statusorder");
				model.setMenuName("Status Order");
				model.setAppId(appID);
				model.setIconCount(R.drawable.icon_data_pending);	
				vRecord.add(model);
			}*/

            if (Utility.flagLogin != 0) {
                model = new ModelMenuUtama();
                model.setMenuId("changesign");
                model.setMenuName("Change Signature");
                model.setAppId(appID);
                model.setIconCount(R.drawable.icon_sign);
                vRecord.add(model);
            }

            model = new ModelMenuUtama();
            model.setMenuId("identify");
            model.setMenuName("Identifikasi Fingerprint");
            model.setAppId(appID);
            model.setIconCount(R.drawable.finger);
            vRecord.add(model);

            model = new ModelMenuUtama();
            model.setMenuId("logimage");
            model.setMenuName("Pending Images");
            model.setAppId(appID);
            model.setIconCount(R.drawable.img);
            vRecord.add(model);

            model = new ModelMenuUtama();
            model.setMenuId("setting");
            model.setMenuName("Setting");
            model.setAppId(appID);
            model.setIconCount(R.drawable.icon_setting);
            vRecord.add(model);

            model = new ModelMenuUtama();
            model.setMenuId("logout");
            model.setMenuName("Log Out");
            model.setAppId(appID);
            model.setIconCount(R.drawable.icon_sign_out);
            vRecord.add(model);
        }

        Setuplist(vRecord);
    }


    /**
     * @param data kumpulan data vektor yang akan ditampilka
     *             untuk menampilkan list data
     */
    @SuppressWarnings("rawtypes")
    private void Setuplist(Vector data) {
        AdapterConnector.setConnector((ListView) findViewById(R.id.lstApp), data, R.layout.menuutamaadapter2, new AdapterInterface() {
            @Override
            public View onMappingColumn(int row, View v, Object data) {
                v.setTag(((ModelMenuUtama) data).getAppId() + "|" + ((ModelMenuUtama) data).getMenuId());

                ((TextView) v.findViewById(R.id.menuTitle)).setText(((ModelMenuUtama) data).getMenuName());
                TextView txtCount = (TextView) v.findViewById(R.id.txt_count);
                FrameLayout notifLayout = (FrameLayout) v.findViewById(R.id.notif_layout);
                txtCount.setText(String.valueOf(((ModelMenuUtama) data).getCount()));
                if (((ModelMenuUtama) data).getCount() <= 0) {
                    notifLayout.setVisibility(View.INVISIBLE);
                }
                ((ImageView) v.findViewById(R.id.icon)).setImageResource(((ModelMenuUtama) data).getIconCount());
                return v;
            }
        });
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        AlertDialog dialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        switch (id) {
            case 0:
                View view = AdapterConnector.setInflateLinier(getApplicationContext(), R.layout.password_dialog);
                dialogBuilder.setView(view);
                dialog = dialogBuilder.create();
                break;

            case 1:
                dialog = dialogBuilder.create();
                dialog.setMessage("Apakah anda yakin akan keluar aplikasi ?");
                dialog.setCancelable(false);
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        intent = new Intent(DAFMenu.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });

                dialog.setButton(dialog.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;

            case 2:
                dialog.setMessage("Please resend all pending data");
                dialog.setCancelable(false);
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Oke", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;

            case 3:
                dialog.setTitle("Batch # : " + Utility.getSetting(DAFMenu.this, "BATCH", ""));
                dialog.setMessage("Are You Sure To Close Batch ?");
                dialog.setCancelable(false);
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Oke", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        Messagebox.showProsesBar(DAFMenu.this, new Runnable() {
                            public void run() {
                                TelephonyManager tm = (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE);
//                                String imei = tm.getDeviceId().toUpperCase();
                                String imei = Utility.getImeiOrUUID(DAFMenu.this).getImei().toUpperCase();

                                String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "closebatchservlet/?userid=", Utility.getSetting(getApplicationContext(), "USERNAME", ""), "&imei=", imei, "&batch=", Utility.getSetting(getApplicationContext(), "BATCH", ""), "&versi=", SettingActivity.APP_VERSION);
                                result = Utility.getHttpConnection(sUrl);

                            }
                        }, new Runnable() {
                            public void run() {
                                if (result.endsWith("OK")) {
                                    Utility.setSetting(DAFMenu.this, "TODAY", "");//reset ofline mode
                                    intent = new Intent(DAFMenu.this, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                } else {
                                    AdapterDialog.showMessageDialog(DAFMenu.this, "Informasi", result);
                                }

                            }
                        });
                    }
                });

                dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;

            default:
                break;
        }

        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, final Dialog dialog) {
        switch (id) {
            case 0:
                final EditText edtPass = (EditText) dialog.findViewById(R.id.edtpass);
                ((Button) dialog.findViewById(R.id.btnSubmit)).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (edtPass.getText().toString().equals(Utility.getSetting(getApplicationContext(), "PASSWORD", ""))) {
//						if (Utility.ttdPath.equalsIgnoreCase("")) {
                            strFileName = Utility.getSetting(getApplicationContext(), "USERNAME", "") + "_" + Utility.getTodayDate().replace("-", "");
//						}else{
//							strFileName = Utility.ttdPath;
//						}

                            intent = new Intent(DAFMenu.this, DigitalSignatureActivity.class);
                            if (new File(Utility.getDefaultSign(strPathImg)).exists()) {
                                intent.putExtra("image", Utility.getDefaultSign(Utility.ttdPath));
//							intent.putExtra("image", Utility.getDefaultSign(strPathImg));							
                            } else {
                                intent.putExtra("image", Utility.getDefaultTempPath(Utility.ttdPath));
//							intent.putExtra("image", Utility.getDefaultTempPath(strPathImg));							
                            }
                            intent.putExtra("firstSign", "false");
                            intent.putExtra("fileName", Utility.getSetting(getApplicationContext(), "USERNAME", "") + "_" + Utility.getTodayDate().replace("-", ""));
                            startActivityForResult(intent, 1);
                            dialog.dismiss();
                        } else {
                            AdapterDialog.showMessageDialog(DAFMenu.this, "Informasi", "Password anda salah");
                        }

                    }
                });
                break;

            default:
                break;
        }
    }

    /**
     * untuk mengupload tanda tangan keserver
     */
    private void uploadSign() {
        Messagebox.showProsesBar(DAFMenu.this, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                File file = null;
                if (new File(Utility.getDefaultSign(strFileName)).exists() == true) {
                    file = new File(Utility.getDefaultSign(strFileName));
                } else
                    file = new File(Utility.getDefaultPath(strFileName));

                if (file != null) {
                    result = ImagePost.postHttpConnectionWithPicture(SettingActivity.URL_SERVER + "uploadservlet?filetype=blob&master=signature&userid=" +
//							17/12/2018
                            Utility.getSetting(getApplicationContext(), "USERNAME", "") + "&iscustomer=N", null, strFileName, file.toString(), UUID.randomUUID().toString());
                } else {
                    result = "File kosong";
                }
            }
        }, new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (result.endsWith("OK")) {
                    strPathImg = strFileName;
                    dismissDialog(0);
                    AdapterDialog.showMessageDialog(DAFMenu.this, "Informasi", "Tanda tangan berhasil diganti");
                } else {
                    AdapterDialog.showMessageDialog(DAFMenu.this, "Informasi", "Maaf pengiriman tanda tangan gagal \n\n " + result);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (Utility.finishSign) {
//				new File(Utility.getDefaultTempPath(strPathImg));
                uploadSign();
            }
        } else {
            if (requestCode == 1) {
                AdapterDialog.showMessageDialog(DAFMenu.this, "Informasi", "Anda belum melakukan tanda tangan");
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Generator.stateCurrModelActivityID = "";
    }
}
