package com.daf.activity;

import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.Logout;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class IdentifyFingerprint extends Activity{

	private ListView list;
	private EditText edt;
	private Recordset dataF;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.onlinefinder);
		
//		if (Global.getText("service.malam", "0").equals("0")) {
//			new Logout(this);
//		}
		
		((ImageView) findViewById(R.id.imageView1)).setVisibility(View.GONE);
		((ImageView) findViewById(R.id.imageView2)).setVisibility(View.GONE);
		
		edt = (EditText)findViewById(R.id.txtSearch);
		
		((TextView) findViewById(R.id.textJudul)).setText("Identifikasi Fingerprint");
		list = (ListView)findViewById(R.id.lstFinder);
		
		((Button)findViewById(R.id.tblFind)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validation();
			}
		});
		
		Vector<InputFilter> filt = new Vector<InputFilter>();
		filt.add(Utility.specialCharFilter);
		edt.setFilters(filt.toArray(new InputFilter[filt.size()]));
	}
	
	private void validation(){
		if (!edt.getText().toString().equalsIgnoreCase("")) {
			((ListView)findViewById(R.id.lstFinder)).setAdapter(null);
			search(edt.getText().toString().trim());
		}else{
			AdapterDialog.showMessageDialog(IdentifyFingerprint.this, "Informasi", "No Contract tidak boleh kosong");
		}
	}
	
	private void search(final String param){
		Messagebox.showProsesBar(IdentifyFingerprint.this, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (Utility.getNewToken()) {
					String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "datascanfingerservlet?nokontrak=" ,param,"&user=", Utility.getSetting(getApplicationContext(), "userid", ""),
							"imei=&rnd=&token=" ,Utility.getSetting(Utility.getAppContext(), "Token", ""), "&v=100");		
					dataF = Syncronizer.getHttpStream(sUrl);
				}
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (dataF != null) {
					downloadBiomatric(param);
				}else {
					AdapterDialog.showMessageDialog(IdentifyFingerprint.this, "Informasi", "Maaf, data yang anda cari tidak tersedia");
				}
			}
		});
	}
	
	private void downloadBiomatric(final String param){
		final String url=SettingActivity.URL_SERVER + "downloadfinger?nokontrak="+param+"&user="+Utility.getSetting(getApplicationContext(), "userid", "")+
				"&imei=&rnd=&token="+Utility.getSetting(Utility.getAppContext(), "Token", "")+"&v=100";
		Messagebox.showProsesBar(IdentifyFingerprint.this, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (Utility.getNewToken()) {
					Utility.saveFile(Utility.getDefaultSign(param), url);
				}
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (dataF!= null) {
					if (dataF.getRows() > 0) {
						setupList(dataF.getAllDataVector());
					}else{
						AdapterDialog.showMessageDialog(IdentifyFingerprint.this, "Informasi", "Maaf, data yang anda cari tidak tersedia");
					}
					
				}
			}
		});
	}
	
	private void setupList(Vector<Vector<String>> data){
		AdapterConnector.setConnector(list, data, R.layout.finger_inflate, new AdapterInterface() {
			
			@SuppressWarnings("unchecked")
			@Override
			public View onMappingColumn(int row, View v, Object data) {
				// TODO Auto-generated method stub
				Vector<String> val = (Vector<String>) data;
				((TextView)v.findViewById(R.id.txtNama)).setText(val.elementAt(2));
				((TextView)v.findViewById(R.id.txtAlamat)).setText(val.elementAt(3));
				((TextView)v.findViewById(R.id.txtCustNo)).setText(val.elementAt(1));
				((TextView)v.findViewById(R.id.txtObject)).setText(val.elementAt(7));
				
				
				((Button)v.findViewById(R.id.btnIdentify)).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(IdentifyFingerprint.this, VerifyFingerActivity.class);
						intent.putExtra("path", edt.getText().toString().trim());
						startActivityForResult(intent, 2);
					}
				});
				return v;
			}
		});
	}
	
	private void setupList2(Vector<Vector<String>> data, final Vector<String> header){
		AdapterConnector.setConnector(list, data, R.layout.finger_inflate2, new AdapterInterface() {
			
			@SuppressWarnings({ "unchecked", "deprecation" })
			@Override
			public View onMappingColumn(int row, View v, Object data) {
				// TODO Auto-generated method stub
				Vector<String> val = (Vector<String>) data;
				((TextView)v.findViewById(R.id.txtNama)).setText(val.elementAt(0));
				((TextView)v.findViewById(R.id.txtAlamat)).setText(val.elementAt(1));
				((TextView)v.findViewById(R.id.txtCustNo)).setText(val.elementAt(2));
				((TextView)v.findViewById(R.id.txtObject)).setText(val.elementAt(3));
				
				
				
				for (int i = 0; i < header.size(); i++) {
					View vv = AdapterConnector.setInflateLinier(getApplicationContext(), R.layout.inflate_sub_finger);	
					((TextView)vv.findViewById(R.id.txtTitle)).setText(header.elementAt(i).toUpperCase());
					((TextView)vv.findViewById(R.id.txtValue)).setText(val.elementAt(i+4));
					((LinearLayout)v.findViewById(R.id.mngHide)).addView(vv, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				}
				
				
				((Button)v.findViewById(R.id.btnIdentify)).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(IdentifyFingerprint.this, VerifyFingerActivity.class);
						intent.putExtra("path", edt.getText().toString().trim());
						startActivityForResult(intent, 2);
					}
				});
				return v;
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 2 && resultCode == 1) {
			((ListView)findViewById(R.id.lstFinder)).setAdapter(null);
			Recordset dd = Connection.DBquery("select field_desc from MST_FINGER_IDENFICATION where status = '1' order by seq ASC");
			
			Vector<String> datad = new Vector<String>();
			datad.add(dataF.getText(0, "nama"));
			datad.add(dataF.getText(0, "alamat"));
			datad.add(dataF.getText(0, "cust id"));
			datad.add(dataF.getText(0, "object"));
			
			Vector<String> header = new Vector<String>();
			
			for (int i = 0; i < dd.getRows(); i++) {
				header.add(dd.getText(i, "field_desc"));
				datad.add(dataF.getText(0, dd.getText(i, "field_desc")));
			}
			Vector<Vector<String>> test = new Vector<Vector<String>>();
			test.add(datad);
			
			setupList2(test,header);
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Utility.deleteFile(Utility.getDefaultSign(edt.getText().toString()));
		finish();
	}
}
