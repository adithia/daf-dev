package com.daf.rule;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.utility.Utility;

public class NikitaRule {
 
 
	/**
	 * untuk mendapatkan rule yang akan ditampilkan digenerator
	 */
  public List<DocTypeRuleModel> getNikitaRule(Hashtable<String, String>  compdata,String param){
	  List<DocTypeRuleModel> result =null;
	  Nset flt = getMasterFieldkey();	  
	  Recordset mst = getMasterRuleMaster();
	  
	  for (int i = 0; i < mst.getRows(); i++) {
		  if (isRuleOK(mst.getText(i, 0), flt, compdata)) {
			  return getDocType( mst.getText(i, 1),param  );
		  }		  
	  }
	  
	  return result;
  }
  
  /**
	 * untuk mendapatkan nilai isi dari suatu komponen
	 */
  private String getText(Hashtable<String, String>  compdata, String compname){
	  return compdata.get(compname)!=null?compdata.get(compname):"";
  }
  
  /**
	 * untuk mengecek apakah array yang ada sama
	 */
  private boolean isEqualArray( Hashtable<String, String>  compdata, String arraycompname, String valuemaster){
	  boolean bret =false;
	  String[] split = Utility.split(arraycompname, ";");
	  for (int i = 0; i < split.length; i++) {
		  String mobilevalue  = getText(compdata, split[i] );	
		  if ( mobilevalue.equals(valuemaster ) ) {
			  return true;
		 }
	 }	  
	  return bret;
  }
  
  /**
	 * untuk mengecek apakah validasi rule sesuai dengan ketentuan ditabel
	 */
  private boolean isRuleOK(String ruleid , Nset fileds,  Hashtable<String, String>  compdata){
	  Recordset dtl  = getMasterReuleDetail(ruleid); 
	  boolean rule = dtl.getRows()>=1;
	  for (int j = 0; j < dtl.getRows(); j++) {
		  String filedmaster = dtl.getText(j, 1);//field_id[MST_RULE_DTL]
		  String valuemaster =  dtl.getText(j, 2);//value[MST_RULE_DTL]
		  String fieldmobilename = fileds.getData(filedmaster) .toString() ;//field_id[MST_RULE_DTL]==>comp_id_mobile[MST_FIELD]
		  if (fieldmobilename.contains(";")) {
			  if (  !isEqualArray(compdata, fieldmobilename, valuemaster )    ) { 
				  return false;
			  }
		  }else{
			  String mobilevalue  = getText(compdata, fieldmobilename );			  
			  if (!mobilevalue.equals(valuemaster )) { 
				  return false;
			  }
		  }
		 
	  }  
	  return rule;
  }
  
  /**
	 * untuk mendapatkan field2 yang dipakai oleh rule
	 */
  private Nset getMasterFieldkey() {
	  Nset ns = Nset.newObject();
	  Recordset flt = Connection.DBquery("select field_id,field_name,comp_id_mobile from MST_FIELD ");
	  for (int i = 0; i < flt.getRows(); i++) {
		  ns.setData(flt.getText(i, 0), flt.getText(i,2));
		
	}
	  return ns;
  }
 
  /**
	 * untuk mendapatkan rule detail berdasrkan rule id
	 */
  private Recordset getMasterReuleDetail(String ruleid) {
		
	  	return Connection.DBquery("select rule_id,field_id,value from MST_RULE_DTL WHERE rule_id='"+ruleid+"'");

  }
  /**
   * untuk mendapatkan header dari rule
   * @return
   */
  private Recordset getMasterRuleMaster() {
		return Connection.DBquery("select rule_id,doc_group_id from MST_RULE_HDR ");
	
}
  
  
  //buatan siapa ini methode ??
  private List<DocTypeRuleModel> getDocType(String docGroup,String param) {
  	Recordset rs = null;
  	List<DocTypeRuleModel> fieldMap = new ArrayList<DocTypeRuleModel>();
  	
     try {
     	if(param!=""){
			rs = Connection.DBquery(

					"SELECT b.doc_type_id," + "b.doc_type_desc,"
							+ "b.template_mobile," + "b.template_xml,"
							+ "b.template_image_print,"
							+ "b.template_image_preview "
							+ "FROM MST_DOC_GROUP_MAP_TYPE a, MST_DOC_TYPE b "
							+ "WHERE a.doc_group_id = '" + docGroup
							+ "' AND a.doc_type_id = b.doc_type_id "
							+ "AND b.doc_type_id NOT IN ("+param+")");
		}else{
			rs = Connection.DBquery("select b.doc_type_id, b.doc_type_desc, b.template_mobile, b.template_xml, b.template_image_print,b.template_image_preview from MST_DOC_GROUP_MAP_TYPE a, MST_DOC_TYPE b where a.doc_group_id = '"+docGroup+"' and a.doc_type_id = b.doc_type_id");
		}
//  	   rs = Connection.DBquery(
////  	   		"select b.doc_type_id, b.doc_type_desc, b.template_mobile, b.template_xml, b.template_image_print,b.template_image_preview from MST_DOC_GROUP_MAP_TYPE a, MST_DOC_TYPE b where a.doc_group_id = '"+docGroup+"' and a.doc_type_id = b.doc_type_id");
//			   "SELECT b.doc_type_id," + "b.doc_type_desc,"
//					   + "b.template_mobile," + "b.template_xml,"
//					   + "b.template_image_print,"
//					   + "b.template_image_preview "
//					   + "FROM MST_DOC_GROUP_MAP_TYPE a, MST_DOC_TYPE b "
//					   + "WHERE a.doc_group_id = '" + docGroup
//					   + "' AND a.doc_type_id = b.doc_type_id "
//					   + "AND b.doc_type_id NOT IN ("+param+")");

   	if (rs.getRows() > 0) {
   		for (int i = 0; i < rs.getRows(); i++) {
   			//
   			
   			DocTypeRuleModel doc = new DocTypeRuleModel();
   			doc.setDoc_type_id(rs.getText(i, "doc_type_id"));
   			doc.setDoc_type_desc(rs.getText(i, "doc_type_desc"));
   			doc.setTemplate_image_preview(rs.getText(i, "template_image_preview"));
   			doc.setTemplate_image_print(rs.getText(i, "template_image_print"));
   			doc.setTemplate_mobile(rs.getText(i, "template_mobile"));
   			doc.setTemplate_xml(rs.getText(i, "template_xml"));
   			
   			
				fieldMap.add(doc);
				}
			}
   	
	} catch (Exception e) {
		// TODO: handle exception
	}

      return fieldMap;
  }
  
}
