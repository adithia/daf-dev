package com.daf.rule;

public class DocTypeRuleModel {

	private String doc_type_id;
	private String doc_type_desc;
	private String template_mobile;
	private String template_xml;
	private String template_image_print;
	private String template_image_preview;
	public String getDoc_type_id() {
		return doc_type_id;
	}
	public void setDoc_type_id(String doc_type_id) {
		this.doc_type_id = doc_type_id;
	}
	public String getDoc_type_desc() {
		return doc_type_desc;
	}
	public void setDoc_type_desc(String doc_type_desc) {
		this.doc_type_desc = doc_type_desc;
	}
	public String getTemplate_mobile() {
		return template_mobile;
	}
	public void setTemplate_mobile(String template_mobile) {
		this.template_mobile = template_mobile;
	}
	public String getTemplate_xml() {
		return template_xml;
	}
	public void setTemplate_xml(String template_xml) {
		this.template_xml = template_xml;
	}
	public String getTemplate_image_print() {
		return template_image_print;
	}
	public void setTemplate_image_print(String template_image_print) {
		this.template_image_print = template_image_print;
	}
	public String getTemplate_image_preview() {
		return template_image_preview;
	}
	public void setTemplate_image_preview(String template_image_preview) {
		this.template_image_preview = template_image_preview;
	}
}
