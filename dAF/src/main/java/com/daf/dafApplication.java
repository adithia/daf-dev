package com.daf;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.androidnetworking.AndroidNetworking;
import com.salesforce.BuildConfig;

import ai.advance.liveness.lib.GuardianLivenessDetectionSDK;
import ai.advance.liveness.lib.Market;

public class dafApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        GuardianLivenessDetectionSDK.init(this, BuildConfig.livenessAccessKey, BuildConfig.livenessSecretKey, Market.Indonesia);
        AndroidNetworking.initialize(getApplicationContext());
    }
}
