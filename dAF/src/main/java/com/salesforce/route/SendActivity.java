package com.salesforce.route;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.action.SaveAction;
import com.salesforce.generator.action.SendAction;

public class SendActivity extends RouteClass{
	@Override
	public boolean runRoute(Component comp, SingleRecordset data) {
		new FinishDate().runRoute(comp, data) ;
		new SaveAction().onAction(comp, data) ;
		new SendAction().onAction(comp, data) ;
		return false;
	}
}
