package com.salesforce.route;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class StartDate extends RouteClass{
	@Override
	public boolean runRoute(Component comp, SingleRecordset data) {
		if (!comp.getForm().getFormComponentText("@INIT.STARTDATE").contains("-")) {
			comp.getForm().setFormComponentText("@INIT.STARTDATE", Utility.Now());
		}
		return true;
	}
}
