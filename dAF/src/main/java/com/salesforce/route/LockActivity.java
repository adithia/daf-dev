package com.salesforce.route;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class LockActivity extends RouteClass{

	public boolean runRoute(Component comp, SingleRecordset data) {
		//Log.i("LockActivity",":"+ comp.getForm().getFormComponentText("@INIT.NIKITA"));
		if (comp.getForm().getFormComponentText("@INIT.FINISHDATE").contains("-") || comp.getForm().getFormComponentText("@INIT.NIKITA").contains("FINISH") ) {
			for (int i = 0; i < comp.getForm().getAllComponent().size(); i++) {
				comp.getForm().getComponent(i).setEnable(false);
			}
		 	if (data.getText("result").startsWith("[") && data.getText("result").endsWith("]")) {
				Vector<String> vcmp = Utility.splitVector(data.getText("result").substring(1, data.getText("result").length()-1), ",");
				for (int i = 0; i < vcmp.size(); i++) {
					Component cmp =comp.getForm().getFormComponent(vcmp.elementAt(i).trim());
					if (cmp!=null) {
						cmp.setEnable(true);
					}
				}
			}else{
				Component cmp =comp.getForm().getFormComponent(data.getText("result"));
				if (cmp!=null) {
					cmp.setEnable( true );
				}
			}		
		}
		 
		return true;
	}
}
