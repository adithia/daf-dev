package com.salesforce.route;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.action.ShowDialogAction;

public class AfterSendActivity extends RouteClass{
	@Override
	public boolean runRoute(Component comp, SingleRecordset data) {
		//Log.i("AfterSendActivity", data.getText("result"));
		
		String result = comp.getForm().getFormComponentText(data.getText("result"));
		
		//Log.i("AfterSendActivity.", result);
	 
		if (result.endsWith("OK")) {
			new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Berhasil Terkirim", "result=88:OK"));
		}else{
			new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Gagal, Masalah Koneksi\r\n"+result, "result=88:Simpan"));
		}
		return false;
	}
}
