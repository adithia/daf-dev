package com.salesforce.route;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class FinishDate extends RouteClass{
	@Override
	public boolean runRoute(Component comp, SingleRecordset data) {
		String buffer = Utility.Now();
		if (!comp.getForm().getFormComponentText("@INIT.FINISHDATE").contains("-")) {
			comp.getForm().setFormComponentText("@INIT.FINISHDATE", buffer);
		}
		comp.getForm().setFormComponentText("@INIT.SENDDATE", buffer);
		new Finish().runRoute(comp, data);		
		return true;
	}
}
