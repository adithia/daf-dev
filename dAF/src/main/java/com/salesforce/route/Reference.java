package com.salesforce.route;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class Reference  extends RouteClass{
	@Override
	public boolean runRoute(Component comp, SingleRecordset data) {
		String result = comp.getForm().getFormComponentText(data.getText("result"));
		if (result.trim().length()>=1) {
		}else{
			String refcode = "0000000000000"+Utility.getSetting(comp.getForm().getActivity(), "BATCH", "") ;
			int i = Utility.getSettingInt(comp.getForm().getActivity(), "COUNT", 0)+1;
			Utility.setSetting(comp.getForm().getActivity(), "COUNT", i+"");
			String count = "00000000000"+i;
			refcode=refcode.substring(refcode.length()-13,refcode.length())+count.substring(count.length()-6,count.length());
					
			refcode=Utility.getSetting(comp.getForm().getActivity(), "BATCH", "")+count.substring(count.length()-6,count.length());;
			comp.getForm().setFormComponentText(data.getText("result"), refcode);
		} 
		return true;
	}
}
