package com.salesforce.route;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;

public class Finish extends RouteClass{
	@Override
	public boolean runRoute(Component comp, SingleRecordset data) {
		if (!comp.getForm().getFormComponentText("@INIT.NIKITA").contains("FINISH")) {
			comp.getForm().setFormComponentText("@INIT.NIKITA", comp.getForm().getFormComponentText("@INIT.NIKITA") + ";FINISH");
		}		 
		return true;
	}
}
