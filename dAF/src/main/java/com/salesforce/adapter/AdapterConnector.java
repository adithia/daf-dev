package com.salesforce.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class AdapterConnector {

	@SuppressWarnings("rawtypes")
	public static void notifyDataSetChanged(ListView lstView) {

		try {
			((ArrayAdapter) lstView.getAdapter()).notifyDataSetChanged();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected void connector(ListView lstView, List data, int layout, AdapterInterface iAdapter) {
		lstView.setAdapter(new ProtoAdapter(lstView.getContext(), layout, data, iAdapter));

	}

	@SuppressWarnings("rawtypes")
	public static void setConnector(ListView lstView, List data, int layout, AdapterInterface iAdapter) {
		new AdapterConnector().connector(lstView, data, layout, iAdapter);
	}
	
	@SuppressWarnings("rawtypes")
	protected void connector(ListView lstView, ArrayList data, int layout, AdapterInterface iAdapter) {
		lstView.setAdapter(new ProtoAdapter(lstView.getContext(), layout, data, iAdapter));

	}

	@SuppressWarnings("rawtypes")
	public static void setConnector(ListView lstView, ArrayList data, int layout, AdapterInterface iAdapter) {
		new AdapterConnector().connector(lstView, data, layout, iAdapter);
	}

	@SuppressWarnings("rawtypes")
	protected void connector(ListView lstView, Vector data, int layout, AdapterInterface iAdapter) {
		lstView.setAdapter(new ProtoAdapter(lstView.getContext(), layout, data, iAdapter));

	}

	@SuppressWarnings("rawtypes")
	public static void setConnector(ListView lstView, Vector data, int layout, AdapterInterface iAdapter) {
		new AdapterConnector().connector(lstView, data, layout, iAdapter);
	}
	
	@SuppressWarnings("rawtypes")
	protected void inflateLinierDirect(Context context, ArrayList vData, LinearLayout layContainer, int layoutprototipe, AdapterInterface iAdapter) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0; i < vData.size(); i++) {
			View view = inflater.inflate(layoutprototipe, null);
			iAdapter.onMappingColumn(i, view, vData.get(i));
			layContainer.addView(view);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void setInflateLinierDirect(Context context, ArrayList vData, LinearLayout layContainer, int layoutprototipe, AdapterInterface iAdapter) {
		new AdapterConnector().inflateLinierDirect(context, vData, layContainer, layoutprototipe, iAdapter);
	}

	@SuppressWarnings("rawtypes")
	protected void inflateLinierDirect(Context context, Vector vData, LinearLayout layContainer, int layoutprototipe, AdapterInterface iAdapter) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0; i < vData.size(); i++) {
			View view = inflater.inflate(layoutprototipe, null);
			iAdapter.onMappingColumn(i, view, vData.elementAt(i));
			layContainer.addView(view);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void setInflateLinierDirect(Context context, Vector vData, LinearLayout layContainer, int layoutprototipe, AdapterInterface iAdapter) {
		new AdapterConnector().inflateLinierDirect(context, vData, layContainer, layoutprototipe, iAdapter);
	}

	@SuppressWarnings("rawtypes")
	protected void inflateLinierDirect(Context context, List lstData, LinearLayout layContainer, int layoutprototipe, AdapterInterface iAdapter) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0; i < lstData.size(); i++) {
			View view = inflater.inflate(layoutprototipe, null);
			iAdapter.onMappingColumn(i, view, lstData.get(i));
			layContainer.addView(view);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void setInflateLinierDirect(Context context, List lstData, LinearLayout layContainer, int layoutprototipe, AdapterInterface iAdapter) {
		new AdapterConnector().inflateLinierDirect(context, lstData, layContainer, layoutprototipe, iAdapter);
	}

	public static View setInflateLinier(Context context, int layoutprototipe) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(layoutprototipe, null, false);
		return rowView;
	}

	public static void addViewLastIndex(Context context, LinearLayout layContainer, int layoutId, Object data, AdapterInterface iAdapter) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(layoutId, null);
		iAdapter.onMappingColumn(layContainer.getChildCount(), view, data);
		layContainer.addView(view);
	}

	public static void editView(Context context, LinearLayout layContainer, int layoutId, int positionToBeEdited, Object data, AdapterInterface iAdapter) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(layoutId, null);
		iAdapter.onMappingColumn(positionToBeEdited, view, data);
		layContainer.removeViewAt(positionToBeEdited);
		layContainer.addView(view, positionToBeEdited);
	}

	@SuppressWarnings("rawtypes")
	private class ProtoAdapter extends ArrayAdapter {

		private Context context;
		private AdapterInterface iAdapter;
		private Vector vData;
		private ArrayList lData;
		private List listData;
		private int layoutprototipe;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(layoutprototipe, parent, false);

			if (vData != null) {
				return iAdapter.onMappingColumn(position, rowView, vData.elementAt(position));				
			}else if (lData != null) {
				return iAdapter.onMappingColumn(position, rowView, lData.get(position));
			}else{
				return iAdapter.onMappingColumn(position, rowView, listData.get(position));
			}

		}

		@SuppressWarnings("unchecked")
		public ProtoAdapter(Context context, int textViewResourceId, Vector vData, AdapterInterface iAdapter) {
			super(context, textViewResourceId, vData);
			this.context = context;
			this.iAdapter = iAdapter;
			layoutprototipe = textViewResourceId;
			this.vData = vData;
			notifyDataSetChanged();

		}
		
		@SuppressWarnings("unchecked")
		public ProtoAdapter(Context context, int textViewResourceId, ArrayList lData, AdapterInterface iAdapter) {
			super(context, textViewResourceId, lData);
			this.context = context;
			this.iAdapter = iAdapter;
			layoutprototipe = textViewResourceId;
			this.lData = lData;
			notifyDataSetChanged();

		}
		
		@SuppressWarnings("unchecked")
		public ProtoAdapter(Context context, int textViewResourceId, List listData, AdapterInterface iAdapter) {
			super(context, textViewResourceId, listData);
			this.context = context;
			this.iAdapter = iAdapter;
			layoutprototipe = textViewResourceId;
			this.listData = listData;
			notifyDataSetChanged();

		}

	}

}
