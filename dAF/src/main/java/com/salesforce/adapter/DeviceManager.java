package com.salesforce.adapter;

import android.content.Context;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.Intent;
import android.app.PendingIntent;

import java.util.Iterator;
import java.util.HashMap;

final public class DeviceManager {
	public static final String USB_PERMISSION_REQUEST = "com.nikita.figer";

	public enum DeviceType {
		Q180Mini(1027, 24596);

		private int _vid;
		private int _pid;
		static private HashMap <String, DeviceType> _allTypes = new HashMap <String, DeviceType> ();

		static {
			DeviceType [] allType = DeviceType.values();

			for (DeviceType deviceType: allType)
				_allTypes.put("" + deviceType._vid + deviceType._pid, deviceType);
		}

		private DeviceType(int vid, int pid) {
			_vid = vid;
			_pid = pid;
		}

		static public DeviceType getDeviceType(int vid, int pid) {
			return _allTypes.get("" + vid + pid);
		}
	}

	static public boolean isDeviceAttached(Context context) {
		UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		HashMap <String, UsbDevice> deviceList = usbManager.getDeviceList();
		Iterator <UsbDevice> deviceIterator = deviceList.values().iterator();

		for (UsbDevice usbDevice = null; true == deviceIterator.hasNext() && null != (usbDevice = deviceIterator.next());)
			if (null != DeviceType.getDeviceType(usbDevice.getVendorId(), usbDevice.getProductId()))
				return true;

		return false;
	}

	static public boolean hasPermission(Context context) {
		UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		HashMap <String, UsbDevice> deviceList = usbManager.getDeviceList();
		Iterator <UsbDevice> deviceIterator = deviceList.values().iterator();

		for (UsbDevice usbDevice = null; true == deviceIterator.hasNext() && null != (usbDevice = deviceIterator.next());)
			if (null != DeviceType.getDeviceType(usbDevice.getVendorId(), usbDevice.getProductId()))
				return usbManager.hasPermission(usbDevice);

		return false;
	}

	static public void requestPermission(Context context) {
		UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		HashMap <String, UsbDevice> deviceList = usbManager.getDeviceList();
		Iterator <UsbDevice> deviceIterator = deviceList.values().iterator();

		for (UsbDevice usbDevice = null; true == deviceIterator.hasNext() && null != (usbDevice = deviceIterator.next());)
		{
			if (null == DeviceType.getDeviceType(usbDevice.getVendorId(), usbDevice.getProductId()))
				continue;

			PendingIntent permissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(USB_PERMISSION_REQUEST), 0);
			usbManager.requestPermission(usbDevice, permissionIntent);
		}
	}
}
