package com.salesforce.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.salesforce.R;

public class SpinnerAdapter extends ArrayAdapter<String> {

	private Vector<String> vData;
	private LayoutInflater inflater;
	private ArrayList<String> AlData;
	private List<String> lData;
	

	public SpinnerAdapter(Context context, int textViewResourceId, Vector<String> vData) {
		super(context, textViewResourceId, vData);
		this.vData = vData;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public SpinnerAdapter(Context context, int textViewResourceId, ArrayList<String> AlData) {
		super(context, textViewResourceId, AlData);
		this.AlData = AlData;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public SpinnerAdapter(Context context, int textViewResourceId, List<String> lData) {
		super(context, textViewResourceId, lData);
		this.lData = lData;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		if (vData != null) {
			return vData.size();
		}else if(AlData != null) {
			return AlData.size();
		}else {
			return lData.size();
		}
	}

	private class ViewHolder {
		private TextView txtInList;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.simple_spinner, null);

			viewHolder = new ViewHolder();
			viewHolder.txtInList = (TextView) convertView.findViewById(R.id.text_spinner);
			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		if (vData != null) {
			viewHolder.txtInList.setText(vData.elementAt(position));			
		}else {
			viewHolder.txtInList.setText(lData.get(position));
		}
		return convertView;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.simple_spinner, null);

			viewHolder = new ViewHolder();
			viewHolder.txtInList = (TextView) convertView.findViewById(R.id.text_spinner);
			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		if (vData != null) {
			viewHolder.txtInList.setText(vData.elementAt(position));
		}else {
			viewHolder.txtInList.setText(lData.get(position));
		}
		
		return convertView;
	}
}
