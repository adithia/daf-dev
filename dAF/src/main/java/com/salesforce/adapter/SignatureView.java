package com.salesforce.adapter;

import java.io.File;
import java.io.FileOutputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
//import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.salesforce.R;
import com.salesforce.utility.Utility;

public class SignatureView extends View {
	private static final float STROKE_WIDTH = 5f;
	private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
	private Paint paint = new Paint();
	private Path path = new Path();
	private float lastTouchX;
	private float lastTouchY;
	private final RectF dirtyRect = new RectF();
	private Bitmap mBitmap;
	private LinearLayout container;
	private File myPath;
	private Button btnSave;
	private Button btnClear;
	private Activity dialog;
	private int color = R.color.BackgrounSign;
	private View v_old;
	RelativeLayout rowBtn ;
	public SignatureView(Context context,View v_old,  final LinearLayout container,  final RelativeLayout rowBtn,  File myPath, Activity dialog) {
		super(context, null);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(STROKE_WIDTH);
		setBackgroundColor(Color.TRANSPARENT);
		this.container = container;
		this.myPath = myPath;
		this.dialog = dialog;
		this.rowBtn = rowBtn;
		this.v_old=v_old;
		setButton(context);
		if (v_old.getVisibility() == View.VISIBLE) {
			btnClear.setEnabled(true);
		}
		this.dialog.setFinishOnTouchOutside(false);
	}
	
	//seno Perubahan 15-09-2018
	public void setButton(Context context){
				
	    //rowBtn.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.FILL_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	    //rowBtn.setOrientation(LinearLayout.HORIZONTAL);
	     
	    btnClear = new Button(context);
//        btnClear.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	    LayoutParams params2 = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
	    btnClear.setLayoutParams(params2);
        btnClear.setText("Clear");      
        btnClear.setEnabled(false);        
        rowBtn.addView(btnClear);
        
		btnSave = new Button(context);
//		btnSave.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
		btnSave.setText("Save");
		btnSave.setEnabled(false);		
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);			
		btnSave.setLayoutParams(params);
		rowBtn.addView(btnSave);	
                
		btnClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnClear.setEnabled(false);
				btnSave.setEnabled(false);
				v_old.setVisibility(View.GONE);
				Utility.finishSign = true;
				clear();
			}
		});
		
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnClear.setEnabled(false);
				btnSave.setEnabled(false);		 
				
				//yg lama
//				if (myPath.exists()) {
//					myPath.delete();
//				}
				
				save(container);
				 
				dialog.setResult(Activity.RESULT_OK);
				dialog.finish();
			}
		});
	}

	public boolean save(View v) {
//		Log.v("log_tag", "Width: " + v.getWidth());
//		Log.v("log_tag", "Height: " + v.getHeight());
		if (mBitmap == null) {
			mBitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
		}
		Canvas canvas = new Canvas(mBitmap);
		try {
			FileOutputStream mFileOutStream = new FileOutputStream(myPath);

  			v.draw(canvas);
			mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);

			mFileOutStream.flush();
			mFileOutStream.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void clear() {
		path.reset();
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawPath(path, paint);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float eventX = event.getX();
		float eventY = event.getY();
		// mGetSign.setEnabled(true);

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			path.moveTo(eventX, eventY);
			lastTouchX = eventX;
			lastTouchY = eventY;
			return true;

		case MotionEvent.ACTION_MOVE:

		case MotionEvent.ACTION_UP:

			resetDirtyRect(eventX, eventY);
			int historySize = event.getHistorySize();
			for (int i = 0; i < historySize; i++) {
				float historicalX = event.getHistoricalX(i);
				float historicalY = event.getHistoricalY(i);
				expandDirtyRect(historicalX, historicalY);
				path.lineTo(historicalX, historicalY);
			}
			path.lineTo(eventX, eventY);
	
			if(btnSave != null)
				btnSave.setEnabled(true);
			if(btnClear != null)
				btnClear.setEnabled(true);
			Utility.finishSign = true;
			break;

		default:
			debug("Ignored touch event: " + event.toString());
			return false;
		}

		invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH), (int) (dirtyRect.top - HALF_STROKE_WIDTH), (int) (dirtyRect.right + HALF_STROKE_WIDTH), (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

		lastTouchX = eventX;
		lastTouchY = eventY;

		return true;
	}

	private void debug(String string) {
		
	}

	private void expandDirtyRect(float historicalX, float historicalY) {
		if (historicalX < dirtyRect.left) {
			dirtyRect.left = historicalX;
		} else if (historicalX > dirtyRect.right) {
			dirtyRect.right = historicalX;
		}

		if (historicalY < dirtyRect.top) {
			dirtyRect.top = historicalY;
		} else if (historicalY > dirtyRect.bottom) {
			dirtyRect.bottom = historicalY;
		}
	}

	private void resetDirtyRect(float eventX, float eventY) {
		dirtyRect.left = Math.min(lastTouchX, eventX);
		dirtyRect.right = Math.max(lastTouchX, eventX);
		dirtyRect.top = Math.min(lastTouchY, eventY);
		dirtyRect.bottom = Math.max(lastTouchY, eventY);
	}
}
