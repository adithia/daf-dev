package com.salesforce.model;

public class ModelMenuUtama {
	private String menuId;
	private String menuName;
	private String appId;
	private String className;
	private String iconName;
	private int iCount;
	private int iconCount;
	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public void setCount(int iCount) {
		this.iCount = iCount;
	}
	public int getCount() {
		return this.iCount;
	}
	public void setIconCount(int iCount) {
		this.iconCount = iCount;
	}
	public int getIconCount() {
		return this.iconCount;
	}
}
