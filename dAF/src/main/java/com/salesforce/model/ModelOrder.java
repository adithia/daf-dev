package com.salesforce.model;

public class ModelOrder {
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNoTlp() {
		return noTlp;
	}
	public void setNoTlp(String noTlp) {
		this.noTlp = noTlp;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	public String getKecamatan() {
		return kecamatan;
	}
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	public String getKodePos() {
		return kodePos;
	}
	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTotalKuitansi() {
		return totalKuitansi;
	}
	public void setTotalKuitansi(String totalKuitansi) {
		this.totalKuitansi = totalKuitansi;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHint() {
		return hint;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getStrToShow() {
		return strToShow;
	}
	public void setStrToShow(String strToShow) {
		this.strToShow = strToShow;
	}

	public String getStrToSave() {
		return strToSave;
	}
	public void setStrToSave(String strToSave) {
		this.strToSave = strToSave;
	}

	private String strToShow;
	private String strToSave;
	private String orderId;
	private String orderName;
	private String appid;
	private String menuId;
	private String menuName;
	private String custName;
	private String address;
	private String noTlp;
	private String noHp;
	private String kelurahan;
	private String kecamatan;
	private String kota;
	private String kodePos;
	private String email;
	private String totalKuitansi;
	private String status;
	private String hint;

}
