package com.salesforce.model;

public class ModelApplication {
	private String appName;
	private String appIcon;
	private String appId;
	private String appTheme="";
	
	public String getAppIcon() {
		return appIcon;
	}
	public void setAppIcon(String appIcon) {
		this.appIcon = appIcon;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public void setAppTheme(String appTheme) {
		this.appTheme = appTheme;
	}
	public String getAppTheme() {
		return appTheme;
	}
}
