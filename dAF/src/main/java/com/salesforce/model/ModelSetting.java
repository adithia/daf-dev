package com.salesforce.model;

public class ModelSetting {
	private String url;
	private String typeStorage;
	private StringBuffer macAddress;
	
	public String getUrl() {
		return url;
	}
	
	public String getTypeStorage() {
		return typeStorage;
	}
	public StringBuffer getMacAddress() {
		return macAddress;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setTypeStorage(String typeStorage) {
		this.typeStorage = typeStorage;
	}
	public void setMacAddress(StringBuffer macAddress) {
		this.macAddress = macAddress;
	}
	
	
	
}
