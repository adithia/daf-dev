package com.salesforce.connection;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;

import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Utility;

public class Syncronizer {
	public static NikitaMultipart openHttp(String url, Hashtable<String, String> arg){
		try {
			InputStream is = Utility.doGet(url).getEntity().getContent();
			return  new NikitaMultipart(is);
		} catch (Exception e) { }		
		return new NikitaMultipart();
	}
	
	public static void openHttpImage(String url, Hashtable<String, String> arg, String img){
		 
	}
	
	public static void openHttpImage(String url, Hashtable<String, String> arg, InputStream img){
		 
	}
	
	public static String getHttp(String...url){
		return Utility.getHttpConnection(Utility.getURLenc(url));
	}
	public static Recordset getHttpStream(String...url){
		return Stream.downStream(Syncronizer.getHttp(url));
	}
	public static Recordset getStringHttpStream(String data){
		return Stream.downStream(data);
	}
	public static Recordset getMenuHttpStream(String data){
		return Stream.downStreamMenu(data);
	}
	public static void saveToDB(String table, String...url){
		saveToDB(table, getHttpStream(url));
	}
	public static void saveToDB(String table, Recordset rst){
		 Connection.DBdelete(table);
		 Connection.DBdrop(table);
		 
		 createToDB(table, rst);
		 insertToDB(table, rst);
	}
	public static void saveToDBBG(String table, Recordset rst){
		 ConnectionBG.DBdelete(table);
		 ConnectionBG.DBdrop(table);
		 
		 createToDBBG(table, rst);
		 insertToDBBG(table, rst);
	}
	public static void saveToDBDAF(String table, Recordset rst){
		 Connection.DBdelete(table);
		 Connection.DBdrop(table);
		 
		 createToDB(table, rst);
		 insertToDBSpecialDAF(table, rst);
	}
	public static long Truncate(String table, Recordset rst){
		
		long del = 0, drop = 0, create = 0, insert = 0;
		del = Connection.DBdeleteDAF(table);
//		if (del > 0) {
			Connection.DBdrop(table);			 
			createToDB(table, rst);
			insert = insertToDBSpecialDAF(table, rst);
			if (insert > 0) {
				return 1;
			}
			
//		}		
		 
		 return 0;
	}
	public static long TruncateBG(String table, Recordset rst){
		
		ConnectionBG.DBdelete(table);
//		if (del > 0) {
			ConnectionBG.DBdrop(table);			 
			createToDBBG(table, rst);
			insertToDBBG(table, rst);
			
//		}		
		 
		 return 0;
	}
	public static void insertToDB(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 String[] field = new String[rst.getCols()];
			 for (int j = 0; j < rst.getCols(); j++) {
				 field[j] = rst.getHeader(j)+"="+rst.getText(i, j).trim();//matikan spasy
			 }
			 Connection.DBinsert(table, field);
		 }
	}
	public static void insertToDBBG(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 String[] field = new String[rst.getCols()];
			 for (int j = 0; j < rst.getCols(); j++) {
				 field[j] = rst.getHeader(j)+"="+rst.getText(i, j).trim();//matikan spasy
			 }
			 ConnectionBG.DBinsert(table, field);
		 }
	}
	public static void executeSql(String sql){
		Connection.DBexecute(sql);
	}
	public static int insertToDBSpecialDAF(String table, Recordset rst){
		Vector<String> vIns = new Vector<String>(); 
		 for (int i = 0; i < rst.getRows(); i++) {
			 Vector<String> hdr2 = new Vector<String>();
			 for (int j = 0; j < rst.getCols(); j++) {
				 if (rst.getHeader(j).trim().equalsIgnoreCase("flag_delta_dml")) {
					
				 }else if (rst.getHeader(j).trim().equalsIgnoreCase("where_clause")) {
					
				 }else{
					 hdr2.add(rst.getHeader(j)+"="+rst.getText(i, j).trim());
				 }
				 
			 }
			 long ins = 0;
//			 ins = Connection.DBinsert(table, hdr2);
			 Connection.DBopen();
			 Connection.DBdeleteDAF(table, "p_id=?", new String[]{rst.getText(i, "P_ID")});
			 Connection.DBinsert2(table, hdr2);
			 vIns.add(""+ins);
		 }
		 
		if (!Utility.isContainValue2("0", vIns)) {
			return 1;
		}else{
			return 0;
		}
	}
	
	public static void createToDB(String table, Recordset rst){
		 String[] field = new String[rst.getCols()];
		 for (int j = 0; j < rst.getCols(); j++) {
			 if ( rst.getHeader(j).toString().equalsIgnoreCase("sequence_id")) {
				 field[j] = "-"+rst.getHeader(j).toLowerCase();
			 }else{
				 field[j] = rst.getHeader(j).toLowerCase();
			 }			 
		 }
		 Connection.DBcreate(table,field);
	}
	public static void createToDBBG(String table, Recordset rst){
		 String[] field = new String[rst.getCols()];
		 for (int j = 0; j < rst.getCols(); j++) {
			 if ( rst.getHeader(j).toString().equalsIgnoreCase("sequence_id")) {
				 field[j] = "-"+rst.getHeader(j).toLowerCase();
			 }else{
				 field[j] = rst.getHeader(j).toLowerCase();
			 }			 
		 }
		 ConnectionBG.DBcreate(table,field);
	}
	public static void deleteToDB(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 Connection.DBdelete(table, rst.getHeader(0)+"=?", new String[]{rst.getText(i, 0)});
		 }
	}
	
	public static long deleteByP_ID(String table, Recordset rst){
		Vector<String> v = new Vector<String>();
		for (int i = 0; i < rst.getRows(); i++) {
			long del = 0;
			del = Connection.DBdeleteDAF(table, "P_ID=?", new String[]{rst.getText(i, "P_ID")}); // where_clause
//			del = Connection.DBdeleteDAF(table, "p_id=?", new String[]{rst.getText(i, "P_ID")}); // where_clause
			v.add(""+del);
		}
		
		if (!Utility.isContainValue2("0", v)) {
			return 1;
		}else{
			return 0;
		}
	}

	public static long deleteDBDAF(String table, Recordset rst){
		Vector<String> v = new Vector<String>();
		for (int i = 0; i < rst.getRows(); i++) {
			long del = 0;
			del = Connection.DBdeleteDAF(table, rst.getHeader(0)+"=?", new String[]{rst.getText(i, 0)}); // where_clause
			v.add(""+del);
		}
		
		if (!Utility.isContainValue2("0", v)) {
			return 1;
		}else{
			return 0;
		}
	}
	
	public static void deleteToDBBG(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 ConnectionBG.DBdelete(table, rst.getHeader(0)+"=?", new String[]{rst.getText(i, 0)});
		 }
	}
	
	public static void updateToDB(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 String[] field = new String[rst.getCols()];
			 for (int j = 0; j < rst.getCols(); j++) {
				 field[j] = rst.getHeader(j)+"="+rst.getText(i, j).trim();//matikan spasy				
			 }
			 Connection.DBupdate(table, field);
		 }
	}
	public static boolean compare2DB(String table, Recordset rst, Nset orderstream){
		String orderid = orderstream.getData(table).toString();
		if (rst.getRows() >= Utility.split(orderid, ",").length) {
			return true;
		}
		
		return false;
	}
}
