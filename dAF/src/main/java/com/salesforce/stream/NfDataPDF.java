package com.salesforce.stream;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.salesforce.utility.Utility;

public class NfDataPDF {
	private Hashtable<String, Vector> master = new Hashtable<String, Vector> ();
	public Hashtable getInternalObject(){
		return master;
	}
	public NfDataPDF(String stream){
		stream= Utility.replace(stream, "\r", "\n");
		stream= Utility.replace(stream, "\n\n", "\n");
		
		 Vector<String> rows  = Utility.splitVector(stream, "\n");
		 Vector data = null;
		 String header = null;
		 
		 for (int i = 0; i < rows.size(); i++) {
			 if (rows.elementAt(i).startsWith("[") && rows.elementAt(i).endsWith("]")) {
				 if (data!=null && header!=null) {
					 master.put(header, data);
				 }
				 data = new Vector();
				 header=rows.elementAt(i);
			 }else{
				 data.add(rows.elementAt(i));
			 }
		 }
		 if (data!=null && header!=null) {
			 master.put(header, data);
		 }
	}	
	 
	public Vector<String> getData(String header){
		header="["+header+"]";
		if (master.get(header)!=null) {
			if (master.get(header) !=null) {
				return master.get(header);
			}
		}
		return new Vector<String>() ;
	} 

	public Vector<String> getHeaders(){
		 Vector<String> v = new Vector<String>();
		
		 Enumeration hdata = ((Hashtable)master).keys();
         while (hdata.hasMoreElements()) {                
               v.addElement((String)hdata.nextElement());
         }            

         return v;
	} 
}
