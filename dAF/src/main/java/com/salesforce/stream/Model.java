package com.salesforce.stream;

import java.util.ArrayList;
import java.util.Vector;

import com.salesforce.database.Recordset;

public class Model implements Recordset{
	private Vector<Vector<String>> data = new Vector<Vector<String>>();
	private Vector<String> cols = new Vector<String>();
	private Vector<String> headerNRecord = new Vector<String>();
	private ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
	
	
	public Model(){}
	public Model(Stream s){}
	public Model(Vector<String> col, Vector<Vector<String>> data){
		this.cols=col;
		this.data=data;
	}
	
	public int getRows() {
		return data.size();
	}
	public int getCols() {
		return cols.size();
	}
	public String getText(int row, int col) {
		try {
			return data.elementAt(row).elementAt(col)!=null?data.elementAt(row).elementAt(col):"";
		} catch (Exception e) { }
		return "";
	}
	public String getText(int row, String colname) {
		return getText(row, cols.indexOf(colname));
	}
	public String getHeader(int col) {
		try {
			return cols.elementAt(col)!=null?cols.elementAt(col):"";
		} catch (Exception e) {}
		return "";
	}
	@Override
	public Vector<String> getAllHeaderVector() {
		return cols;
	}
	@Override
	public Vector<String> getRecordFromHeader(String colname) {
		for (int i = 0; i < cols.size(); i++) {
			if (colname.equalsIgnoreCase(cols.elementAt(i).toString())) {
				for (int j = 0; j < getRows(); j++) {
					String record = getText(j, i);
					headerNRecord.add(record);							
				}
			}
		}
		return headerNRecord;
	}
	@Override
	public Vector<Vector<String>> getAllDataVector() {
		// TODO Auto-generated method stub
		return data;
	}
	@Override
	public ArrayList<ArrayList<String>> getAllDataList() {
		// TODO Auto-generated method stub
		return dataList;
	}
	@Override
	public ArrayList<String> getAllHeaderList() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
