package com.salesforce.stream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import android.content.res.AssetManager;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.salesforce.database.Recordset;
import com.salesforce.utility.Utility;

public class Stream {
	public static Recordset downStream(String s){
		 
		if (s.startsWith("{")||s.startsWith("[")) {
			return downStreamJSON(s); 
		}else if (s.startsWith("id")) {
			return downStreamID(s); 
		}else if (s.startsWith("inikita")) {
			
		}
		return new Model(); 
	}
	
	public static Recordset downStreamMenu(String s){
		 
		if (s.startsWith("{")||s.startsWith("[")) {
			return downStreamJSONMenu(s); 
		}else if (s.startsWith("id")) {
			return downStreamID(s); 
		}else if (s.startsWith("inikita")) {
			
		}
		return new Model(); 
	}
	
	public static Recordset downStreamJSON(String s){
		return downStreamJSON(new InputStreamReader(new ByteArrayInputStream(s.getBytes())));
	}
	
	public static Recordset downStreamJSONMenu(String s){
		return downStreamJSONMenu(new InputStreamReader(new ByteArrayInputStream(s.getBytes())));
	}
	
	public static Recordset recordCSV(List<String[]> data){
		try {
			Vector<String> header = null;
			Vector<Vector<String>> value = new Vector<Vector<String>>();
			for (int i = 0; i < data.size(); i++) {
				if (i == 0) {
					header = new Vector<String>(Arrays.asList(data.get(i)));
				}else{
					Vector<String> gh = new Vector<String>(Arrays.asList(data.get(i)));
					value.add(gh);
				}
			}
			return new Model(header, value);
		} catch (Exception e) {
		}
		return new Model(); 
	}
	
	public static Recordset downStreamJSON(InputStreamReader is){
		Object dataObject = null;
        try {  
            JsonReader reader = new JsonReader(is);
            reader.setLenient(true);
            if (reader.peek().equals(JsonToken.BEGIN_ARRAY)) {
                  dataObject=JsonArray(reader);
            }else{
                  dataObject=JsonObject(reader);
            }            
        } catch (Exception ex) {
            System.err.println("JsonArray:"+ex.getMessage());
        }
        if (dataObject instanceof Hashtable) {
        	Hashtable ds = (Hashtable) dataObject;
        	try {
        		if (ds.get("status").equals("OK") && ds.get("model").equals("recordset")) {
        			ds = (Hashtable) ds.get("data");
        			Vector header  =   (Vector) ds.get("fieldname");
        			Vector<Vector<String>> content =   (Vector) ds.get("content");
        			return new Model(header , content); 
    			}else if(ds.get("status").equals("ERR")){
    				String status = ds.get("status").toString();
    				ds = (Hashtable) ds.get("message");
    				Vector header  =   new Vector();//(Vector) ds.get("error");
    				header.add("status");
    				
    				Vector<Vector<String>> content =  new Vector<Vector<String>>();//(Vector) ds.get("message");
    				Vector<String> val = new Vector<String>();
    				val.add(status);
    				content.add(val);
    				val = new Vector<String>();
    				val.add(ds.get("message").toString());
    				content.add(val);
        			return new Model(header, content);
    			}
			} catch (Exception e) {}
		}
        try {
        	is.close();
		} catch (Exception e) { }
		return new Model(); 
	}
	
	public static Recordset downStreamJSONMenu(InputStreamReader is){
		Object dataObject = null;
        try {  
            JsonReader reader = new JsonReader(is);
            if (reader.peek().equals(JsonToken.BEGIN_ARRAY)) {
                  dataObject=JsonArray(reader);
            }else{
                  dataObject=JsonObject(reader);
            }            
        } catch (Exception ex) {
            System.err.println("JsonArray:"+ex.getMessage());
        }
        if (dataObject instanceof Hashtable) {
        	Hashtable ds = (Hashtable) dataObject;
        	try {
        		if (ds.get("status").equals("OK") && ds.get("model").equals("recordset")) {
        			ds = (Hashtable) ds.get("dataMenu");
        			Vector header  =   (Vector) ds.get("fieldnameMenu");
        			Vector<Vector<String>> content =   (Vector) ds.get("contentMenu");
        			return new Model(header , content); 
    			}else if(ds.get("status").equals("ERR")){
    				String status = ds.get("status").toString();
    				ds = (Hashtable) ds.get("message");
    				Vector header  =   new Vector();//(Vector) ds.get("error");
    				header.add("status");
    				
    				Vector<Vector<String>> content =  new Vector<Vector<String>>();//(Vector) ds.get("message");
    				Vector<String> val = new Vector<String>();
    				val.add(status);
    				content.add(val);
    				val = new Vector<String>();
    				val.add(ds.get("message").toString());
    				content.add(val);
        			return new Model(header, content);
    			}
			} catch (Exception e) {}
		}
        try {
        	is.close();
		} catch (Exception e) { }
		return new Model(); 
	}
	
	public static Recordset getCSVFile(String fileName){
		String strLine;
		try {
			AssetManager assetMgr = Utility.getAppContext().getAssets();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assetMgr.open(fileName)));

			// Init lineList
			Vector<String> header = new Vector<String>();
			Vector<Vector<String>> content =   new Vector<Vector<String>>();
			while ((strLine = bufferedReader.readLine()) != null) {
				Vector<String> data = Utility.splitVector(strLine, "\\|");
				content.add(data);
//				String[] strings = strLine.split("\\|");
//				lineList.add(strings);
			}
			return new Model(header, content);
			// Close the input stream
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return new Model();
	}
	public static Recordset downStreamINIKITA(String s){
		return new Model(); 
	}
	public static Recordset downStreamID(String s){
		/*
		 * Format data
		 * Nikitav1.0\tTableName\t\r\n
		 * fname\t\r\n
		 * fdata\t\r\n
		 * OK[End of Data]
		 * 
		 */
		//Log.i("NikitaWilly", "downStream:"+s);
		if (s.endsWith("OK")) {
			s=s.substring(0,s.length()-2);
			int ih = s.indexOf("\r\n");
			if (ih!=-1) {
				//String header = s.substring(0, ih);
								
				s = s .substring(ih+2);
				ih = s.indexOf("\r\n");
				if (ih!=-1) {
					String fname = s.substring(0, ih);
					s = s .substring(ih+2);
					
					if (fname.endsWith("\t")) {
						fname=fname.substring(0,fname.length()-1);
					}
					
					if (!s.contains("\t")) {
						return  new Model(Utility.splitVector(fname, "\t"),  new Vector<Vector<String>>() );
					}
					if (s.endsWith("\r\n")) {
						s=s.substring(0,s.length()-2);
					}
					return  new Model(Utility.splitVector(fname, "\t"), Utility.splitVector(s, "\t" , "\r\n") );
				}
			}			
		}else{
			//Log.i("NikitaWilly", s);
		}
		return new Model() ;
	}	
	public static String upStream(Recordset s){
		StringBuffer sbuBuffer = new StringBuffer();
		
		sbuBuffer.append("id=0;");
		sbuBuffer.append("rows=").append(s.getRows()).append(";");
		sbuBuffer.append("cols=").append(s.getCols()).append(";");
		sbuBuffer.append("\r\n");
		
		for (int i = 0; i < s.getCols(); i++) {
			sbuBuffer.append(s.getHeader(i)).append("\t");
		}
		sbuBuffer.append("\r\n");
		for (int r = 0; r < s.getRows(); r++) {
			for (int i = 0; i < s.getCols(); i++) {
				sbuBuffer.append(s.getText(r, i)).append("\t");
			}
			sbuBuffer.append("\r\n");
		}
		sbuBuffer.append("OK");	    

		return sbuBuffer.toString();
	}	
 
	private static Object JsonArray(JsonReader reader) throws IOException{
        Vector vector = new Vector();
        reader.beginArray();
        while (reader.hasNext()) {
              vector.addElement(JsonValue(reader));
        }              
        reader.endArray();
        return vector;
    }
    private static Object JsonObject(JsonReader reader) throws IOException{
        Hashtable hashtable = new Hashtable();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = JsonMember(reader);
            hashtable.put(name, JsonValue(reader));
        }
        reader.endObject();
        return hashtable;
    }
    private static String JsonMember(JsonReader reader) throws IOException{
        if (reader.peek().equals(JsonToken.NAME)) {
            return reader.nextName();
        }
        return "";//must error
    }    
    private static Object JsonValue(JsonReader reader) throws IOException{
        if (reader.peek().equals(JsonToken.BEGIN_ARRAY)) {
            return JsonArray(reader);
        }else if (reader.peek().equals(JsonToken.BEGIN_OBJECT)) {
            return JsonObject(reader);
        }else if (reader.peek().equals(JsonToken.STRING)) {
            return reader.nextString();
        }else if (reader.peek().equals(JsonToken.NUMBER)) {
            return reader.nextString();
        }else if (reader.peek().equals(JsonToken.BOOLEAN)) {
            return reader.nextString();
        }else if (reader.peek().equals(JsonToken.NULL)) {
            reader.nextNull();
        }else{
            return reader.nextString();
        }    
        return "null";
    }
 
	public static String upStream(File s){
		return null;
	}
}
