package com.salesforce.component;

import com.salesforce.database.SingleRecordset;

public interface IExpression {
	public boolean onExpression (Component comp, SingleRecordset data);
}
