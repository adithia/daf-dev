package com.salesforce.component;

public interface IWillSave {
	public void onWillSave();
}
