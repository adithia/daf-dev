package com.salesforce.component;



public interface IDFormat {
 
	public String getFormatText(String text);
	public String getText(String text);
}
