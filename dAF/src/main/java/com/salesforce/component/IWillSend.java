package com.salesforce.component;

public interface IWillSend {
	public boolean onWillSend();	
}
