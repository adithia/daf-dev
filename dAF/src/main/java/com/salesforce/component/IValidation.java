package com.salesforce.component;

import com.salesforce.database.SingleRecordset;

public interface IValidation {
	public String onValidation (Component comp, SingleRecordset data);
}
