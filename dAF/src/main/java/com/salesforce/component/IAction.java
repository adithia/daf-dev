package com.salesforce.component;

import com.salesforce.database.SingleRecordset;

public interface IAction {
	public boolean onAction (Component comp, SingleRecordset data);
}
