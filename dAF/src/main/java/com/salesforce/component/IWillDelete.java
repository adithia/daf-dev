package com.salesforce.component;

public interface IWillDelete {
	public void onWillDelete();	
}
