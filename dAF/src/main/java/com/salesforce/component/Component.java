package com.salesforce.component;

import android.view.View;

import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.ui.PictureUi;
import com.salesforce.utility.Utility;

public class Component {
	public final static int TYPE_LABEL=0;
	public final static int TYPE_TEXTBOX=1;
	private String name="";
	private String text="";
	private String type="";
	private String list="";
	private String fname=""; 
	private String label="";
	private Object tag;
	public View view;
	private String def = "";
//	protected String mandatory="";
	public String mandatory="";
	private String dftype="";
	private boolean enable=true;
	private boolean visible=true;
	public Form currForm;
	
	protected SingleRecordset currRecordset;
	
	public Form getForm(){
		return currForm;
	}
	public void afterComponentDone(){
		
	}
    public Component(Form form, String name, SingleRecordset rst){
    	currForm=form;
    	this.name=name;
    	currRecordset=rst;
    	mandatory=rst.getText("mandatory");
    	dftype=rst.getText("df_type");
    	setText(rst.getText("text").trim());
    	setList(rst.getText("list"));
//    	setLabel(rst.getText("label") + (mandatory.equals("*")?" *":""));
    	if (mandatory.contains("*")) {
    		setLabel(rst.getText("label") + (mandatory.contains("*")?" *":""));
		}else if (mandatory.equalsIgnoreCase("%")) {
			setLabel(rst.getText("label") + (mandatory.contains("*")?" *":""));
		}
    	setLabel(rst.getText("label") + (mandatory.contains("*")?" *":""));
    	setVisible(rst.getText("visible").equals("true")||rst.getText("visible").equals("1")?true:false);
    	setEnable(rst.getText("enable").equals("true")||rst.getText("enable").equals("1")?true:false);
    	
    	if (currRecordset.getText("hints").startsWith(":")  ) {
			if (currRecordset.getText("hints").contains("|")) {
				currRecordset.getText("hints").substring(0, currRecordset.getText("hints").indexOf( "|"));
			}else{
				fname = currRecordset.getText("hints");
			}			
		}else{
			fname = "";
		}
    	
	}
	public void setName(String name) {
		 this.name=name;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public String getDefault() {//?
		return currRecordset.getText("deftext");
	}
	
	public View onCreate(Form form){
		view = new View(form.getActivity());		 
		return view;
	}
	
	public View getView(){
		return view;
	}
	protected View setView(View v){
		view = v;
		return view;
	}
	public void setVisible(boolean visible){
		this.visible=visible;
		if (view!=null) {
			view.setVisibility(visible?View.VISIBLE:View.GONE);
		}
	}
	public void setEnable(boolean enable){
		this.enable=enable;
		if (view!=null) {
			view.setEnabled(enable);
		} 
	}
	public void setText(String text){
		this.text=text;
	}
	public void setLabel(String label){
		this.label=label;
	}
	public void setList(String list){
		this.list=list;
	}
	public void setType(String type){
		this.type=type;
	}	
	public void setDefault(String text){
		def=text;
	}
	public String fetDefault(){
		return def;
	}
	public boolean getVisible(){
		return visible;
	}
	public boolean getEnable(){
		return enable;
	}
	public String getText(){
		return text;
	}
	public String getLabel(){
		return label;
	}
	public String getList(){
		return list;
	}

	public String getFname(){
		return fname;
	}
	
	public String getDataComp(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
	
	public String validation() {
		//Log.i("NikitaWilly","validation");
		String label = getLabel();
		if (getEnable() && getVisible()) {
			if (mandatory.trim().equals("!")) {
				return execValidation(name);
			}else if (mandatory.trim().startsWith("$")) {
				return execValidation(mandatory.trim().substring(1));
			}else if (mandatory.trim().equals("")) {			
			}
			//untuk memaksa user memilih list pada finder
			else if(mandatory.trim().equals("*T")){
				return "Harap mencari dengan tombol finder dan pilih dari Lov "+getLabel()+"";
			}
			//untuk validasi format email
			else if(mandatory.trim().contains("@")){
				if (getText().length() == 0) {
					return "Field "+getLabel()+"* is mandatory";
				}else {
					boolean isEmail = Utility.isAnEmail(getText());
					if (!isEmail) {
						return "Format "+getLabel()+" Salah";
					}
				}
			}
			//untuk validasi photo
			else if(mandatory.trim().contains("_picture")){
				if (getText().length() <= 5) {
					return getLabel()+" is mandatory";
				}
			}
			//khusus untuk validasi photo lain2
			else if (mandatory.trim().equalsIgnoreCase("#")) {
				String value = getDataComp(this, getList());
				if (value.length() >= 1) {
					if (getText().length() <= 5) {
						return "Anda belum memilih "+getLabel();
					}
				}
			}
			//untuk validasi edittext dengan minimal character
			else if(mandatory.trim().contains("%")){
				if (getText().length() < Integer.parseInt(getList())) {
					return "Karakter "+getLabel()+" tidak boleh kurang dari "+getList();
				}
			}
			//untuk tanda tangan
			else if(mandatory.trim().contains("&")){
				if (getText().length() <= 5) {
					return getLabel()+" is mandatory";
				}
			}
			else if (getText().length()==0) {
				if (mandatory.trim().equals("*") ) {
					return "Field "+getLabel()+" is mandatory";
				}else if(mandatory.trim().equals("^*")){
					return "Field "+getLabel()+"* is mandatory";
				}else if (mandatory.length()>=1){
					return mandatory.trim();
				}
			}
		}else if ( mandatory.startsWith("^")&& getVisible()) {
			//Log.i("NikitaWilly","validationtrue");
			String smandatory = mandatory.substring(1);
			if (smandatory.trim().equals("!")) {
				return execValidation(name);
			}else if (smandatory.trim().startsWith("$")) {
				return execValidation(smandatory.trim().substring(1));
			}else if (smandatory.trim().equals("")) {			
			}else if (getText().length()==0) {
				if (smandatory.trim().equals("*") ) {
					return "Field "+getLabel()+" is mandatory";
				}else if (smandatory.length()>=1) {
					return smandatory.trim();
				}
			}			
		}
		return null;
	}
	public void setFocus(){
		 
	}
	public void nonMandatory(){
		 
	}
	public void runRoute() {		
		Recordset rst = Connection.DBquery("SELECT * FROM ROUTE WHERE comp_id='"+name+"'  ORDER BY cast (sequence_id as number) ASC; ");
		for (int i = 0; i < rst.getRows(); i++) {
			SingleRecordset curr = SingleRecordset.get(i, rst);
			boolean expboolean=Generator.onGeneratorNewExpression(this, curr);
			getForm().setFormComponentText("@EXPRESSION", (expboolean?"true":"false") );
			if (expboolean) {
				if (!Generator.onGeneratorNewAction(this, curr)) {
					break;
				} 
			}
		}	
	}
	public void runRoute(String s) {		
		Recordset rst = Connection.DBquery("SELECT * FROM ROUTE WHERE comp_id='"+name+s+"'  ORDER BY cast (sequence_id as number) ASC; ");
		
		for (int i = 0; i < rst.getRows(); i++) {
			SingleRecordset curr = SingleRecordset.get(i, rst);
			boolean expboolean=Generator.onGeneratorNewExpression(this, curr);
			getForm().setFormComponentText("@EXPRESSION", (expboolean?"true":"false") );
			if (expboolean) {
				if (!Generator.onGeneratorNewAction(this, curr)) {
					break;
				} 
			}
		}	
	}
	private String execValidation(String code) {		
		Recordset rst = Connection.DBquery("SELECT * FROM VALIDATION WHERE comp_id='"+code+"'  ORDER BY cast (sequence_id as number) ASC; ");
		//Log.i("NikitaWilly","runValidation:="+code+"a"+rst.getRows());
		for (int i = 0; i < rst.getRows(); i++) {
			SingleRecordset curr = SingleRecordset.get(i, rst);
			String result = Generator.onGeneratorNewValidatation(this, curr);
			if (result!=null) {
				if (!result.equals("")) {
					return result;
				}
			}
		}	
		return null;

	}
	public Object getTag() {
		return tag;
	}
	public void setTag(Object tag) {
		this.tag = tag;
	}
}
