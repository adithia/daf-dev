package com.salesforce.generator;

import android.app.Activity;
import android.view.View;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.component.IDFormat;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.action.BackAction;
import com.salesforce.generator.action.BackNewEntry;
import com.salesforce.generator.action.BreakAction;
import com.salesforce.generator.action.CalculateAction;
import com.salesforce.generator.action.CalculateSQLAction;
import com.salesforce.generator.action.CalculatingCeil;
import com.salesforce.generator.action.CalculationFloor;
import com.salesforce.generator.action.CloseAction;
import com.salesforce.generator.action.ConcatAction;
import com.salesforce.generator.action.DateDivAction;
import com.salesforce.generator.action.ExitAction;
import com.salesforce.generator.action.FindLocationAction;
import com.salesforce.generator.action.GetMenuAction;
import com.salesforce.generator.action.GetYearAction;
import com.salesforce.generator.action.HomeAction;
import com.salesforce.generator.action.HttpAction;
import com.salesforce.generator.action.HttpGetAction;
import com.salesforce.generator.action.IsLogin;
import com.salesforce.generator.action.MatchingRo;
import com.salesforce.generator.action.NextAction;
import com.salesforce.generator.action.NextValidationAction;
import com.salesforce.generator.action.NoBackAction;
import com.salesforce.generator.action.NonMandatoryAction;
import com.salesforce.generator.action.PrintAction;
import com.salesforce.generator.action.RoutingAction;
import com.salesforce.generator.action.SQLAction;
import com.salesforce.generator.action.SQLAction2;
import com.salesforce.generator.action.SQLAction4;
import com.salesforce.generator.action.SaveAction;
import com.salesforce.generator.action.SaveValidationAction;
import com.salesforce.generator.action.SendAction;
import com.salesforce.generator.action.SetDefaultAction;
import com.salesforce.generator.action.SetEnableAction;
import com.salesforce.generator.action.SetLabelAction;
import com.salesforce.generator.action.SetLocationWithLabel;
import com.salesforce.generator.action.SetMessageAction;
import com.salesforce.generator.action.SetOfficeCode;
import com.salesforce.generator.action.SetTextAction;
import com.salesforce.generator.action.SetVisibleAction;
import com.salesforce.generator.action.ShowDialogAction;
import com.salesforce.generator.action.ShowFinderAction;
import com.salesforce.generator.action.ShowFinderItemAction;
import com.salesforce.generator.action.ShowFormAction;
import com.salesforce.generator.action.SimpanTasklistOffline;
import com.salesforce.generator.action.StringAction;
import com.salesforce.generator.action.SubStringAction;
import com.salesforce.generator.action.SubStringAction2;
import com.salesforce.generator.action.SubStringAction3;
import com.salesforce.generator.action.ValidasiApplNo;
import com.salesforce.generator.action.ValidasiGambar;
import com.salesforce.generator.action.ValidationAction;
import com.salesforce.generator.action.ValidationsAction;
import com.salesforce.generator.action.goToNewEntryFromDAFPameran;
import com.salesforce.generator.displayformat.CurrencyDF;
import com.salesforce.generator.displayformat.CurrencyNoPointDF;
import com.salesforce.generator.expression.AlwaysTrueExpression;
import com.salesforce.generator.expression.ContainsExpression;
import com.salesforce.generator.expression.EndWithExpression;
import com.salesforce.generator.expression.EqualEqualExpression;
import com.salesforce.generator.expression.EqualExpression;
import com.salesforce.generator.expression.EqualNotEqualExpression;
import com.salesforce.generator.expression.EqualParam1Expression;
import com.salesforce.generator.expression.GreatherDateExpression;
import com.salesforce.generator.expression.GreatherExpression;
import com.salesforce.generator.expression.GreatherThanExpression;
import com.salesforce.generator.expression.IfTrueExpression;
import com.salesforce.generator.expression.LessDateExpression;
import com.salesforce.generator.expression.LessExpression;
import com.salesforce.generator.expression.LessThanExpression;
import com.salesforce.generator.expression.MultipleAndLessExpression;
import com.salesforce.generator.expression.MultipleExpression2;
import com.salesforce.generator.expression.NotEqualExpression;
import com.salesforce.generator.expression.NotEqualParam1Expression;
import com.salesforce.generator.expression.StartWithExpression;
import com.salesforce.generator.ui.BrowsePDF;
import com.salesforce.generator.ui.CheckboxUi;
import com.salesforce.generator.ui.DateUi;
import com.salesforce.generator.ui.FinderItemUi;
import com.salesforce.generator.ui.FingerUi;
import com.salesforce.generator.ui.FormPagesUi;
import com.salesforce.generator.ui.Grid;
import com.salesforce.generator.ui.LabelUi;
import com.salesforce.generator.ui.LocationUi;
import com.salesforce.generator.ui.MenuUi;
import com.salesforce.generator.ui.NavigationUi;
import com.salesforce.generator.ui.PictureUi;
import com.salesforce.generator.ui.PreviewPDF;
import com.salesforce.generator.ui.RadioBoxUi;
import com.salesforce.generator.ui.RuleUiDua;
import com.salesforce.generator.ui.ScanPrinterUi;
import com.salesforce.generator.ui.SignatureUi;
import com.salesforce.generator.ui.SpecialLabel;
import com.salesforce.generator.ui.SpinnerUi;
import com.salesforce.generator.ui.TextFinder;
import com.salesforce.generator.ui.TextareaUi;
import com.salesforce.generator.ui.TextboxUi;
import com.salesforce.generator.ui.TimeUi;
import com.salesforce.generator.ui.TombolUi;
import com.salesforce.generator.ui.WebUi;
import com.salesforce.generator.validation.DateFormatValidation;
import com.salesforce.generator.validation.DateGreaterThanValidation;
import com.salesforce.generator.validation.DateLessThanValidation;
import com.salesforce.generator.validation.EmailValidation;
import com.salesforce.generator.validation.LengthMinimum;
import com.salesforce.generator.validation.MustContainsValidation;
import com.salesforce.generator.validation.NoCharValidation;
import com.salesforce.generator.validation.PasswordValidation;
import com.salesforce.generator.validation.ValueGreatherThanValidation;
import com.salesforce.generator.validation.ValueLessThanValidation;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Utility;

import java.util.Hashtable;
import java.util.Vector;

public class Generator {
    public static Vector<Form> forms = new Vector<Form>();
    public static Form currform;
    public static Activity curractivity;
    public static String currBufferedView;
    public static boolean freez;
    public static boolean isLogin = true;
    public static String currModel = "";
    public static String currApplication = "";
    public static String currApplicationName = "";
    public static String currModelActivityID = "";
    public static String currOrderCode = "";
    public static String currOrderTable = "";
    public static String currAppTheme = "";//new 05/05/2014
    public static String currAppMenu = "";
    public static boolean isNewEntry = false;
    public static boolean isTasklist = false;
    public static String coyId = "", custtype = "", office_code = "";
    public static String bu = "";
    public static String platform = "";
    public static String branchid = "";
    public static String currOrderType = "";

    //applno hanya ada di tasklist
    public static String applno = "";

    //06-dec-19
    //digunakan sebagai validasi scheduler agar tidak mengirim order yg sedang di buka
    public static String stateCurrModelActivityID = "";

    public static Hashtable<String, String> virtual = new Hashtable<String, String>();
    public static Recordset currRecordset;
    public static String currResult = "";
    @SuppressWarnings("rawtypes")
    public static Class currHome;

    //untuk memjalanakan generator
    public static void openGenerator(Activity curr, String app, String model, String ordertable, String orderid) {
        currModelActivityID = Utility.getSetting(curr, "userid", "").trim() + orderid + System.currentTimeMillis();
        stateCurrModelActivityID = currModelActivityID;
        currOrderTable = ordertable;//add maybe not valid

        Utility.requestCodeCamera = 0;
        Utility.requestCodeGallery = 0;
        Utility.requesTtd = 0;

        //ambil semua form yang ada ditabel modelform
        openGeneratorForm(curr, app, model, orderid, currModelActivityID);
        fillGeneratorDefault();
        //new methode
        trigerFormAfterComponentDone();

        Utility.deleteFileAll(Utility.getDefaultTempPath(""));
        Utility.createFolder(Utility.getDefaultTempPath(""));

        //jika ada form, maka tampilkan form plg pertama
        if (forms.size() >= 1) {
            forms.elementAt(0).show();
        }
    }

    private static void trigerFormAfterComponentDone() {
        for (int i = 0; i < forms.size(); i++) {
            for (int j = 0; j < forms.elementAt(i).components.size(); j++) {
                forms.elementAt(i).components.elementAt(j).afterComponentDone();
            }
        }
    }

    public static void updateGeneratorOrder() {
        Recordset rst = Connection.DBquery("SELECT * FROM " + currOrderTable + " WHERE order_id='" + currOrderCode + "'  ;");
        //fill with order data
        if (rst.getRows() >= 1) {
            for (int i = 0; i < forms.size(); i++) {
                for (int j = 0; j < forms.elementAt(i).components.size(); j++) {
                    if (forms.elementAt(i).components.elementAt(j).getDefault().startsWith("!")) {
                        forms.elementAt(i).components.elementAt(j).setText(rst.getText(0, forms.elementAt(i).components.elementAt(j).getDefault().substring(1)));
                    }
                }
            }
        }
    }

    private static void fillGeneratorDefault() {
        Recordset rst = Connection.DBquery("SELECT * FROM " + currOrderTable + " WHERE order_id='" + currOrderCode + "'  ;");
        //fill with order data
        if (rst.getRows() >= 1) {
            for (int i = 0; i < forms.size(); i++) {
                for (int j = 0; j < forms.elementAt(i).components.size(); j++) {
                    //Log.i("fillDefault","sss");
                    forms.elementAt(i).components.elementAt(j).setDefault(Generator.fillDefault(forms.elementAt(i).components.elementAt(j), new SingleRecordset(0, rst)));
                    //Log.i("fillDefault","xxx");
                    forms.elementAt(i).components.elementAt(j).setText(forms.elementAt(i).components.elementAt(j).fetDefault());
                }
            }
        }
    }

    //mengambil semua form yang ada di tabel modelform
    private static void openGeneratorForm(Activity curr, String app, String model, String order, String activityID) {
        currHome = curr.getClass();
        forms.removeAllElements();
        virtual.clear();

        currModel = model;
        currApplication = app;
        currOrderCode = order;
        currModelActivityID = activityID;
        stateCurrModelActivityID = currModelActivityID;

        Recordset rst = Connection.DBquery("SELECT MODELFORM.form_id,FORM.form_title,FORM.form_type,MODELFORM.sequence_id FROM MODELFORM  LEFT JOIN FORM ON (MODELFORM.form_id=FORM.form_id)  WHERE model_id='" + model + "'ORDER BY cast(sequence_id as integer) ASC ;");

        for (int i = 0; i < rst.getRows(); i++) {
            Form form = Generator.onGeneratorNewForm(i, rst.getText(i, "form_id"), rst.getText(i, "form_type"), rst.getText(i, "form_title"));
            form.setTitle(rst.getText(i, "form_title"));
            if (form != null) {
                forms.addElement(form);
            }
        }
        for (int i = 0; i < rst.getRows(); i++) {
            String s = Generator.forms.elementAt(i).getTitle();
            s = s.replace("$FORMCOUNT", Generator.forms.size() + "");
            s = s.replace("$INDEX", (i + 1) + "");
            Generator.forms.elementAt(i).setTitle(s);
        }

        Generator.setCurrentActivity(curr);
    }

    public static Form onGeneratorNewForm(final int index, String fname, String type, final String title) {
        if (type.equals("form")) {
            // reserved
        } else if (type.equals("grid")) {
            // reserved
        } else if (type.equals("web")) {
        }
        return new Form(fname) {
            public void onCreate() {
                findImageViewById(R.id.imageView1).setVisibility(View.GONE);
                findImageViewById(R.id.imageView2).setVisibility(View.GONE);
                super.onCreate();
                this.setIndex(index);
//				if(getTitle().length() > 24){
//					int newSize = (int) getActivity().getResources().getDimension(R.dimen.nilai7); 					
//					findTextViewById(R.id.textJudul).setTextSize(newSize);
//				}
                findTextViewById(R.id.textJudul).setText(getTitle());
            }
        };
    }

    public static void openGenerator(Activity curr, String fileStream, boolean first) {
        NfData fileData = new NfData(fileStream);
        currModelActivityID = fileData.getText("INIT", "ID");
        stateCurrModelActivityID = currModelActivityID;
        currOrderTable = fileData.getText("INIT", "ORDERTABLE");//add maybe not valid

        openGeneratorForm(curr, fileData.getText("INIT", "APP"), fileData.getText("INIT", "MODEL"), fileData.getText("INIT", "ORDER"), fileData.getText("INIT", "ID"));
        fillGeneratorDefault();

        //fill init 01/04/2014
        Generator.virtual.put("INIT.OPENDATE", fileData.getText("INIT", "OPENDATE"));
        Generator.virtual.put("INIT.STARTDATE", fileData.getText("INIT", "STARTDATE"));
        Generator.virtual.put("INIT.FINISHDATE", fileData.getText("INIT", "FINISHDATE"));
        Generator.virtual.put("INIT.SENDDATE", fileData.getText("INIT", "SENDDATE"));
        Generator.virtual.put("INIT.NIKITA", fileData.getText("INIT", "NIKITA"));
        //fill init 18/07/2014
        Generator.virtual.put("INIT.BATCH", fileData.getText("INIT", "BATCH"));
        if (!Generator.currAppMenu.equalsIgnoreCase("tasklistoffline")) {
            Generator.currOrderType = fileData.getText("INIT", "APPMENU");
        }
//		10:52 15/07/2019
        Generator.currOrderType = fileData.getText("INIT", "APPMENU");
        //16:13 13/06/2019
        Generator.currModel = fileData.getText("INIT", "MODEL");
        //fill with existing data
        for (int i = 0; i < forms.size(); i++) {
            for (int j = 0; j < forms.elementAt(i).components.size(); j++) {
                forms.elementAt(i).components.elementAt(j).setText(fileData.getText(forms.elementAt(i).getName(), forms.elementAt(i).components.elementAt(j).getName()));
                forms.elementAt(i).components.elementAt(j).setEnable(fileData.getEnable(forms.elementAt(i).getName(), forms.elementAt(i).components.elementAt(j).getName()));
                forms.elementAt(i).components.elementAt(j).setVisible(fileData.getVisible(forms.elementAt(i).getName(), forms.elementAt(i).components.elementAt(j).getName()));
            }
        }

        //new methode
        trigerFormAfterComponentDone();


        if (forms.size() >= 1) {
            if (first) {
                forms.elementAt(0).show();
            } else {
                int p = Utility.getInt(fileData.getText("INIT", "POSITION"));
                if (p < forms.size()) {
                    forms.elementAt(p).show();
                } else {
                    forms.elementAt(0).show();
                }
            }
        }


    }

    public static Vector<Hashtable<String, String>> readGenerator(Activity curr, String fileStream) {
        NfData fileData = new NfData(fileStream);
        currModelActivityID = fileData.getText("INIT", "ID");
        stateCurrModelActivityID = currModelActivityID;
        openGeneratorForm(curr, fileData.getText("INIT", "APP"), fileData.getText("INIT", "MODEL"), fileData.getText("INIT", "ORDER"), fileData.getText("INIT", "ID"));
        Vector<Hashtable<String, String>> resVector = new Vector<Hashtable<String, String>>();
        // get with existing data
        for (int i = 0; i < forms.size(); i++) {
            for (int j = 0; j < forms.elementAt(i).components.size(); j++) {
                if (fileData.getVisible(forms.elementAt(i).getName(), forms.elementAt(i).components.elementAt(j).getName())) {
                    Hashtable<String, String> dataHashtable = new Hashtable<String, String>();
                    // dataHashtable.put("title",
                    // forms.elementAt(i).getTitle());
                    // dataHashtable.put("label",
                    // forms.elementAt(i).components.elementAt(j).getLabel());
                    // dataHashtable.put("text",fileData.getText(forms.elementAt(i).getName(),
                    // forms.elementAt(i).components.elementAt(j).getName()));
                    dataHashtable.put(forms.elementAt(i).components.elementAt(j).getLabel(), fileData.getText(forms.elementAt(i).getName(), forms.elementAt(i).components.elementAt(j).getName()));
                    resVector.addElement(dataHashtable);
                }
            }
        }
        return resVector;
    }

    private static String fillDefault(Component comp, SingleRecordset data) {
        //Log.i("fillDefault", comp.getDefault());
        if (comp.getDefault().startsWith("!")) {
            //Log.i("fillDefault:",data.getText(comp.getDefault().substring(1)));
            return data.getText(comp.getDefault().substring(1));
        } else if (comp.getDefault().startsWith("$") || comp.getDefault().startsWith("@")) {
            return comp.getForm().getFormComponentText(comp.getDefault());
        }
        return comp.getDefault();
    }

    public static Component onGeneratorNewComponent(Form form, String id, SingleRecordset data) {
        if (data.getText("type").equals("checkbok")) {
            return new CheckboxUi(form, id, data);
        } else if (data.getText("type").equals("label")) {
            return new LabelUi(form, id, data);
        } else if (data.getText("type").equals("picture")) {
            return new PictureUi(form, id, data);
        } else if (data.getText("type").equals("dropdown")) {
            return new SpinnerUi(form, id, data);
        } else if (data.getText("type").equals("textarea")) {
            return new TextareaUi(form, id, data);
        } else if (data.getText("type").equals("textbox")) {
            return new TextboxUi(form, id, data);
        } else if (data.getText("type").equals("button")) {
            return new TombolUi(form, id, data);
        } else if (data.getText("type").equals("checkbox")) {
            return new CheckboxUi(form, id, data);
        } else if (data.getText("type").equals("radiobox")) {
            return new RadioBoxUi(form, id, data);
        } else if (data.getText("type").equals("navigation")) {
            return new NavigationUi(form, id, data);
        } else if (data.getText("type").equals("location")) {
            return new LocationUi(form, id, data);
        } else if (data.getText("type").equals("date")) {
            return new DateUi(form, id, data);
        } else if (data.getText("type").equals("formpages")) {
            return new FormPagesUi(form, id, data);
        } else if (data.getText("type").equals("grid")) {
            return new Grid(form, id, data);
        } else if (data.getText("type").equals("menu")) {
            return new MenuUi(form, id, data);
        } else if (data.getText("type").equals("web")) {
            return new WebUi(form, id, data);
        } else if (data.getText("type").equals("scanprinter")) {
            return new ScanPrinterUi(form, id, data);
        } else if (data.getText("type").equals("time")) {
            return new TimeUi(form, id, data);
        } else if (data.getText("type").equals("textfinder")) {
            return new TextFinder(form, id, data);
        } else if (data.getText("type").equals("finderitem")) {
            return new FinderItemUi(form, id, data);
        } else if (data.getText("type").equals("signature")) {
            return new SignatureUi(form, id, data);
        } else if (data.getText("type").equals("pdfpreview")) {
            return new PreviewPDF(form, id, data);
        } else if (data.getText("type").equals("browsepdf")) {
            return new BrowsePDF(form, id, data);
        } else if (data.getText("type").equals("tabelui")) {
            return new Grid(form, id, data);
        } else if (data.getText("type").equals("ruleui")) {
            return new RuleUiDua(form, id, data);
        } else if (data.getText("type").equals("fingerui")) {
            return new FingerUi(form, id, data);
        } else if (data.getText("type").equals("speciallabel")) {
            return new SpecialLabel(form, id, data);
        }
        return new Component(form, id, data);
    }

    public static String onGeneratorNewValidatation(Component comp, SingleRecordset data) {
        if (data.getText("val_type").equals("minlen")) {
            return new LengthMinimum().onValidation(comp, data);
        } else if (data.getText("val_type").equals("val-")) {
            return new ValueLessThanValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("mustcontain")) {
            return new MustContainsValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("val+")) {
            return new ValueGreatherThanValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("date>")) {
            return new DateGreaterThanValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("date<")) {
            return new DateLessThanValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("email")) {
            return new EmailValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("Aa1")) {
            return new PasswordValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("!contain")) {
            return new NoCharValidation().onValidation(comp, data);
        } else if (data.getText("val_type").equals("dateformat")) {
            return new DateFormatValidation().onValidation(comp, data);
        }
        return "";
    }

    public static IDFormat onGeneratorNewDFormat(Component comp, String type) {
        if (type.equals("currency")) {
            return new CurrencyDF();
        } else if (type.equals("currencynopoint")) {
            return new CurrencyNoPointDF();
        }
        return null;
    }

    public static boolean onGeneratorNewExpression(Component comp, SingleRecordset data) {
        if (data.getText("expression").equalsIgnoreCase("true")) {
            return new AlwaysTrueExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("contain")) {
            return new ContainsExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("endwith")) {
            return new EndWithExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("=")) {
            return new EqualExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("date>")) {
            return new GreatherDateExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("date<")) {
            return new LessDateExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals(">")) {
            return new GreatherExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals(">=")) {
            return new GreatherThanExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("<")) {
            return new LessExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("<=")) {
            return new LessThanExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("!=")) {
            return new NotEqualExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("startwith")) {
            return new StartWithExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("iftrue")) {
            return new IfTrueExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("&&<=")) {
            return new MultipleExpression2().onExpression(comp, data);
        } else if (data.getText("expression").equals("&&<")) {
            return new MultipleAndLessExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("==!=")) {
            return new EqualNotEqualExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("====")) {
            return new EqualEqualExpression().onExpression(comp, data);
        } else if (data.getText("expression").equals("!=param1")) {
            return new NotEqualParam1Expression().onExpression(comp, data);
        } else if (data.getText("expression").equals("=param1")) {
            return new EqualParam1Expression().onExpression(comp, data);
        } else if (data.getText("expression").equals("")) {
        } else if (data.getText("expression").equals("")) {
        } else if (data.getText("expression").equals("")) {
        } else if (data.getText("expression").equals("")) {
        } else if (data.getText("expression").equals("")) {
        }
        return false;
    }

    public static boolean onGeneratorNewAction(Component comp, SingleRecordset data) {
//		try {

        if (data.getText("action").equals("break")) {
            return new BreakAction().onAction(comp, data);
        } else if (data.getText("action").equals("sqlcalculate")) {
            return new CalculateSQLAction().onAction(comp, data);
        } else if (data.getText("action").equals("close")) {
            return new CloseAction().onAction(comp, data);
        } else if (data.getText("action").equals("datediv")) {
            return new DateDivAction().onAction(comp, data);
        } else if (data.getText("action").equals("home")) {
            return new HomeAction().onAction(comp, data);
        } else if (data.getText("action").equals("http")) {
            return new HttpAction().onAction(comp, data);
        } else if (data.getText("action").equals("next")) {
            return new NextAction().onAction(comp, data);
        } else if (data.getText("action").equals("nextv")) {
            return new NextValidationAction().onAction(comp, data);
        } else if (data.getText("action").equals("save")) {
            return new SaveAction().onAction(comp, data);
        } else if (data.getText("action").equals("savev")) {
            return new SaveValidationAction().onAction(comp, data);
        } else if (data.getText("action").equals("send")) {
            return new SendAction().onAction(comp, data);
        } else if (data.getText("action").equals("setenable")) {
            return new SetEnableAction().onAction(comp, data);
        } else if (data.getText("action").equals("msg")) {
            return new SetMessageAction().onAction(comp, data);
        } else if (data.getText("action").equals("settext")) {
            return new SetTextAction().onAction(comp, data);
        } else if (data.getText("action").equals("setvisible")) {
            return new SetVisibleAction().onAction(comp, data);
        } else if (data.getText("action").equals("sql")) {
            return new SQLAction().onAction(comp, data);
        } else if (data.getText("action").equals("sql2")) {
            return new SQLAction2().onAction(comp, data);
        } else if (data.getText("action").equals("validate")) {
            return new ValidationAction().onAction(comp, data);
        } else if (data.getText("action").equals("validates")) {
            return new ValidationsAction().onAction(comp, data);
        } else if (data.getText("action").equals("showform")) {
            return new ShowFormAction().onAction(comp, data);
        } else if (data.getText("action").equals("noback")) {
            return new NoBackAction().onAction(comp, data);
        } else if (data.getText("action").equals("back")) {
            return new BackAction().onAction(comp, data);
        } else if (data.getText("action").equals("exit")) {
            return new ExitAction().onAction(comp, data);
        } else if (data.getText("action").equals("calculate")) {
            return new CalculateAction().onAction(comp, data);
        } else if (data.getText("action").equals("calculateceil")) {
            return new CalculatingCeil().onAction(comp, data);
        } else if (data.getText("action").equals("calculatefloor")) {
            return new CalculationFloor().onAction(comp, data);
        } else if (data.getText("action").equals("concate")) {
            return new ConcatAction().onAction(comp, data);
        } else if (data.getText("action").equals("string")) {
            return new StringAction().onAction(comp, data);
        } else if (data.getText("action").equals("setdef")) {
            return new SetDefaultAction().onAction(comp, data);
        } else if (data.getText("action").equals("dialog")) {
            return new ShowDialogAction().onAction(comp, data);
        } else if (data.getText("action").equals("print")) {
            return new PrintAction().onAction(comp, data);
        } else if (data.getText("action").equals("runroute")) {
            return new RoutingAction().onAction(comp, data);
        } else if (data.getText("action").equals("finder")) {
            return new ShowFinderAction().onAction(comp, data);
        } else if (data.getText("action").equals("finderitem")) {
            return new ShowFinderItemAction().onAction(comp, data);
        } else if (data.getText("action").equals("backnewentry")) {
            return new BackNewEntry().onAction(comp, data);
        } else if (data.getText("action").equals("findlocation")) {
            return new FindLocationAction().onAction(comp, data);
        } else if (data.getText("action").equals("findlocation2")) {
            return new SetLocationWithLabel().onAction(comp, data);
        } else if (data.getText("action").equals("getyear")) {
            return new GetYearAction().onAction(comp, data);
        } else if (data.getText("action").equals("substring")) {
            return new SubStringAction().onAction(comp, data);
        } else if (data.getText("action").equals("substring2")) {
            return new SubStringAction2().onAction(comp, data);
        } else if (data.getText("action").equals("substring3")) {
            return new SubStringAction3().onAction(comp, data);
        } else if (data.getText("action").equals("setlabel")) {
            return new SetLabelAction().onAction(comp, data);
        } else if (data.getText("action").equals("nomandatory")) {
            return new NonMandatoryAction().onAction(comp, data);
        } else if (data.getText("action").equals("officecode")) {
            return new SetOfficeCode().onAction(comp, data);
        } else if (data.getText("action").equals("validasigambar")) {
            return new ValidasiGambar().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("validasiapplno")) {
            return new ValidasiApplNo().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("islogin")) {
            return new IsLogin().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("matchingro")) {
            return new MatchingRo().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("simpantasklistoffline")) {
            return new SimpanTasklistOffline().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("sql3")) {
            return new SQLAction4().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("sql4")) {
            return new SQLAction4().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("currentmenu")) {
            return new GetMenuAction().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("goToNewEntryFromDAFPameran")) {
            return new goToNewEntryFromDAFPameran().onAction(comp, data);
        } else if (data.getText("action").equalsIgnoreCase("httpgetaction")) {
            return new HttpGetAction().onAction(comp, data);
        }
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
        return true;
    }


    protected static void setCurrentActivity(Activity activity) {
        curractivity = activity;
    }

    protected static void setCurrentForm(Form form) {
        currform = form;
    }

    public static void removeAllForm() {
        forms.removeAllElements();
    }
}
