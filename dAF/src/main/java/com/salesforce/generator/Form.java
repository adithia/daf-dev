package com.salesforce.generator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.daf.activity.FinderActivity;
import com.daf.activity.LoginActivity;
import com.daf.activity.PreviewPDFActivity;
import com.daf.activity.SettingActivity;
import com.daf.activity.TaskListDataActivity;
import com.daf.activity.TaskListOfflineActivity;
import com.daf.activity.Tasklist;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.component.IWillDelete;
import com.salesforce.component.IWillSave;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.action.HomeAction;
import com.salesforce.generator.action.SaveValidationAction;
import com.salesforce.generator.ui.TextboxUi;
import com.salesforce.route.SendActivity;
import com.salesforce.stream.NfString;
import com.salesforce.utility.Utility;

import java.io.File;
import java.util.Vector;

@SuppressWarnings({"unchecked", "rawtypes"})
public class Form {
    /*
     * Constructor OnCreate getContentView save OnValidate close
     */
    public static final int CAMERA = 1;
    public static final int GALLERY = 2;
    public static final int BROWSER = 3;

    public static String finderName = "";
    public Vector<Component> components = new Vector<Component>();
    public Vector<ActivityResultListener> activityResultListeners = new Vector<ActivityResultListener>();
    protected boolean flagback = false;
    protected int orientation;
    private FormActivity owner = null;
    // private int layout=0;
    private String name = "";
    private String title = "";
    private String type = "";
    private int indexCount = -1;
    private String getCode = "";
    private Form back;
    private BackListener backListener;
    private Vector<ButtonMessageClickListener> buttonMessageClickListeners = new Vector<ButtonMessageClickListener>();

    public Form(String fname) {
        components.removeAllElements();
        this.name = fname;
        Recordset rst = Connection.DBquery("SELECT * FROM COMPONENT WHERE form_id='" + fname + "' ORDER BY cast (sequence_id as number) ASC; ");

        for (int i = 0; i < rst.getRows(); i++) {
            Component component = Generator.onGeneratorNewComponent(this, rst.getText(i, "comp_id"), SingleRecordset.get(i, rst));
            if (component != null) {
                components.addElement(component);
            }
        }
    }

    //untuk menampilkan form kedalam ui android
    public void show() {
        if (Generator.currform != null) {
            Generator.currform.getCode = "";
        }
        getCode = this.hashCode() + ":" + System.currentTimeMillis();
        //Log.i("Sales Force", getCode);
        Intent intent = new Intent(Generator.curractivity, FormActivity.class);
        intent.putExtra("NIKITAFORMGENERATOR", getCode);
        // if (!Generator.forms.contains(this)) {
        // Generator.forms.addElement(this);
        // }
        // Generator.curractivity.startActivity(intent);

        Generator.curractivity.startActivityForResult(intent, Activity.RESULT_FIRST_USER);
        if (getActivity() != null) {
            //Log.i("Sales Force", "showfinish");
            getActivity().finish();
        }
    }

    public void runOnUiThread(Runnable run) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(run);
        }
    }

    public void runOnAsyncTask(Runnable run, Runnable uiPost) {
        if (getActivity() != null) {
            getActivity().runOnAsyncTask(run, uiPost);
        }
    }

    public void runOnAsyncTaskBusy(Runnable run, final Runnable uiPost) {
        if (getActivity() != null) {
            getActivity().runOnAsyncTaskBusy(run, uiPost);
        }
    }

    public void setListAdapter(final View view, final Vector data, final int layout, final ListAdapterListener listener) {
        if (view instanceof ListView) {
            ((ListView) view).setAdapter(new ArrayAdapter(getActivity(), layout, data) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    convertView = Utility.getInflater(view.getContext(), layout);
                    return listener.getView(data, position, convertView, parent);
                }
            });
        } else if (view instanceof GridView) {
            ((GridView) view).setAdapter(new ArrayAdapter(getActivity(), layout, data) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    return listener.getView(data, position, convertView, parent);
                }
            });
        }
    }

    public void setSpinnerAdapter(View view, String[] data, int layout) {
        if (view instanceof Spinner) {
            ((Spinner) view).setAdapter(new ArrayAdapter(getActivity(), (layout <= 0 ? android.R.layout.simple_list_item_1 : layout), data));
        }
    }

    public void setSpinnerAdapter(View view, final Vector data, int layout, final ListAdapterListener listener) {
        if (view instanceof Spinner) {
            ((Spinner) view).setAdapter(new ArrayAdapter(getActivity(), (layout <= 0 ? android.R.layout.simple_list_item_1 : layout), data) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    convertView = super.getView(position, convertView, parent);
                    if (listener != null) {
                        convertView = listener.getView(data, position, convertView, parent);
                    }
                    return convertView;
                }
            });
        }
    }

    public void showChoiceDialog(String title, String[] data, DialogInterface.OnClickListener listener) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data);
        Builder dlg = new AlertDialog.Builder(getActivity());
        if (title != null) {
            if (title.trim().length() >= 1) {
                dlg.setTitle(title);
            }
        }
        dlg.setAdapter(adapter, listener);
        dlg.create().show();
    }

    public View getContentView() {
        return null;
    }

    public void setContentView(int layoutResID) {
        if (getActivity() != null) {
            getActivity().setContentView(layoutResID);
        }
    }

    public void setContentView(View view) {
        if (getActivity() != null) {
            getActivity().setContentView(view);
        }
    }

    public void showContentMenu() {

    }

    public void hideContentMenu() {

    }

    public void setGenerator(String table) {

    }

    public void notifyDataSetChanged() {
    }

    public View findViewById(int id) {
        if (getActivity() != null) {
            return getActivity().findViewById(id);
        }
        return null;
    }

    public TextView findTextViewById(int id) {
        return (TextView) findViewById(id);
    }

    public EditText findEditTextById(int id) {
        return (EditText) findViewById(id);
    }

    public Button findButtonById(int id) {
        return (Button) findViewById(id);
    }

    public ImageView findImageViewById(int id) {
        return (ImageView) findViewById(id);
    }

    public ListView findListViewById(int id) {
        return (ListView) findViewById(id);
    }

    public void showMessage(String message, String PositiveButton, String NegativeButton, String CenterButton, boolean cancel, int reqcode, ButtonMessageClickListener buttonMessageClickListener) {
        buttonMessageClickListeners.removeAllElements();
        buttonMessageClickListeners.add(buttonMessageClickListener);
        showMessage(message, PositiveButton, NegativeButton, CenterButton, cancel, reqcode);
    }

    public void showMessage(String message, String PositiveButton, String NegativeButton, String CenterButton, boolean cancel, int reqcode) {
        if (getActivity() != null) {
            getActivity().showMessage(message, PositiveButton, NegativeButton, CenterButton, cancel, reqcode);
        }
    }

    public void showPasswordDialog() {
        if (getActivity() != null) {
            getActivity().hideMessage();
            getActivity().showDialog(0);
        }
    }

    public void hideMessage() {
        if (getActivity() != null) {
            getActivity().hideMessage();
        }
    }

    public void showInfo(String info) {
        if (getActivity() != null) {
            getActivity().showInfo(info);
        }
    }

    public void showInfo(String title, String info) {
        if (getActivity() != null) {
            getActivity().showInfo(title, info);
        }
    }

    public void onCreate() {
        buttonMessageClickListeners.removeAllElements();
        activityResultListeners.removeAllElements();
        for (int i = 0; i < components.size(); i++) {
            getActivity().addContentView(components.elementAt(i).onCreate(this), new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        }

        //new 05/05/2014
//		try {
//			if (Generator.currAppTheme!=null) {
//				if (Generator.currAppTheme.startsWith("{")||Generator.currAppTheme.startsWith("[")) {
//
//				}else if (Generator.currAppTheme.equals("bg2")){
//					findViewById(R.id.frmMain).setBackgroundResource(R.drawable.bg2);
//				}else if (Generator.currAppTheme.equals("bg3")){
//					findViewById(R.id.frmMain).setBackgroundResource(R.drawable.bg3);
//				}
//			}
//		} catch (Exception e) { }
    }

    protected void onStart() {

    }

    public Vector<View> getViewAll() {
        if (getActivity() != null) {
            return getActivity().getViewAll();
        }
        return new Vector<View>();
    }

    public String getNextFormName() {
        boolean next = false;
        for (int i = 0; i < Generator.forms.size(); i++) {
            if (Generator.forms.elementAt(i).hashCode() == this.hashCode()) {
                next = true;
            } else if (next) {
                return Generator.forms.elementAt(i).getName();
            }
        }
        return "";
    }

    public void showNextWithValidation() {
        nextForm(getNextFormName());
    }

    public int getPositionCount() {
        // boolean back = false;
        for (int i = 0; i < Generator.forms.size(); i++) {
            if (Generator.forms.elementAt(i).hashCode() == this.hashCode()) {
                return i;
            }
        }
        return 0;
    }

    public String getGeneratorCode() {
        return getCode;
    }

    public void show(String fname) {
        Utility.requestCodeCamera = 0;
        Utility.requestCodeGallery = 0;
        Utility.requesTtd = 0;
        for (int i = 0; i < Generator.forms.size(); i++) {
            if (Generator.forms.elementAt(i).getName().equals(fname)) {
                saveAndClear();
                Generator.forms.elementAt(i).show(this);
                break;
            }
        }
    }

    public void show(Form back) {
        this.back = back;
        this.show();
    }

    public boolean back() {
        if (back != null) {
            back.show();
            return true;
        }
        return false;
    }

    public void close() {// equ finish()
        if (Generator.forms.contains(this)) {
            // Generator.forms.remove(this);
        }
        Generator.setCurrentForm(null);// ?
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    public void onBackPressed() {
        if (backListener != null) {
            flagback = backListener.onBackPressListener();
        } else {
            if (getIndex() >= 1) {
                showBack();
                flagback = true;
            } else {
                if (Generator.isNewEntry || Generator.currAppMenu.equalsIgnoreCase("draft") || Generator.currAppMenu.equalsIgnoreCase("dafpameran")) {
                    for (int i = 0; i < Generator.forms.size(); i++) {
                        for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                            if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
                                ((IWillSave) Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
                            }
                        }
                    }
                    updateDraft();
                    flagback = true;
                } else if (Generator.currAppMenu.equalsIgnoreCase("order")) { //taskilist
                    updateLogorder();
                    for (int i = 0; i < Generator.forms.size(); i++) {
                        for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                            if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
                                ((IWillSave) Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
                            }
                        }
                    }

                    if (Utility.activity != null) {
                        if (Utility.activity instanceof Tasklist) {
                            Tasklist sd = (Tasklist) Utility.activity;
                            sd.Refresh();
                        }
                    }
                    flagback = true;
                } else if (Generator.currAppMenu.equalsIgnoreCase("tasklistoffline")) {
                    if (TaskListOfflineActivity.draft == true) {
                        update_tasklist_offline();
                        for (int i = 0; i < Generator.forms.size(); i++) {
                            for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                                if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
                                    ((IWillSave) Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
                                }
                            }
                        }
                        if (Utility.activity != null) {
                            if (Utility.activity instanceof TaskListOfflineActivity) {
                                TaskListOfflineActivity sd = (TaskListOfflineActivity) Utility.activity;
                                sd.refresh();
                            }
                        }
                        flagback = true;
                    } else {
                        dialogSimpan(getActivity());
                    }
                } else if (Generator.currAppMenu.equalsIgnoreCase("tasklistdata")) {
                    updateLogorder();
                    for (int i = 0; i < Generator.forms.size(); i++) {
                        for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                            if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
                                ((IWillSave) Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
                            }
                        }
                    }

                    if (Utility.activity != null) {
                        if (Utility.activity instanceof TaskListDataActivity) {
                            TaskListDataActivity sd = (TaskListDataActivity) Utility.activity;
                            sd.Refresh();
                        }
                    }
                    flagback = true;
                } else {
                    flagback = true;
                }
            }
//				flagback = true;
//			}
        }
    }

    private void dialogSimpan(final Activity ctx) {

        AdapterDialog.showDialogTwoBtn(ctx, "Confirmation", "Apakah anda ingin menyimpan order ini ? ", "ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                update_tasklist_offline();
                for (int i = 0; i < Generator.forms.size(); i++) {
                    for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                        if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
                            ((IWillSave) Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
                        }
                    }
                }
                if (Utility.activity != null) {
                    if (Utility.activity instanceof TaskListOfflineActivity) {
                        TaskListOfflineActivity sd = (TaskListOfflineActivity) Utility.activity;
                        sd.refresh();
                    }
                }

                if (Generator.isLogin == true) {
                    owner.finish();
                } else {
                    Intent intent = new Intent(owner, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        }, "Tidak", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
//				Toast.makeText(ctx,
//						"you click batal", Toast.LENGTH_SHORT)
//						.show();

                Connection.DBdelete("trn_tasklist_offline", "orderId=" + Generator.currOrderCode);

                Connection.DBdelete("LOG_IMAGE", "applno=" + Generator.currOrderCode);
                for (int i = 0; i < Generator.forms.size(); i++) {
                    for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                        if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillDelete) {
                            ((IWillDelete) Generator.forms.elementAt(i).components.elementAt(j)).onWillDelete();
                        }
                    }
                }

                TaskListOfflineActivity.deleteFingerImg(Generator.currOrderCode);

                if (Utility.activity != null) {
                    if (Utility.activity instanceof TaskListOfflineActivity) {
                        TaskListOfflineActivity sd = (TaskListOfflineActivity) Utility.activity;
                        sd.refresh();
                    }
                }
//				flagback = true;
//				owner.finish();

                if (Generator.isLogin == true) {
//					intent = new Intent(comps.getForm().getActivity(), TaskListOfflineActivity.class);
//					comps.getForm().getActivity().finish();
                    owner.finish();
                } else {
                    Intent intent = new Intent(owner, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });

    }

    private void update_tasklist_offline() {
//		Form f = components.get(0).getForm();
//		Vector<Component> cs  = components.get(0).getForm().getAllComponent();
        String applno = "";
        String custname = "";
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i) instanceof TextboxUi) {
                if (components.get(i).getName().equalsIgnoreCase("C101F91")) {
                    //applno
                    applno = components.get(i).getText();
                }
                if (components.get(i).getName().equalsIgnoreCase("C103F91")) {
                    //custno
                    custname = components.get(i).getText();
                }
            }
        }
        Connection.DBupdate("trn_tasklist_offline", "applno=" + applno, "cust_name=" + custname, "data=" + components.get(0).getForm().getDataAll(), "activityId=" + Generator.currModelActivityID, "where", "orderId=" + Generator.currOrderCode);
    }

    private void updateLogorder() {
        Connection.DBupdate("log_order", "data=" + components.get(0).getForm().getDataAll(), "where", "order_id=" + Generator.currOrderCode);
    }

    public void setOnBackListener(BackListener listener) {
        backListener = listener;
    }

    public int getIndex() {
        if (indexCount != -1) {
            return indexCount;
        }
        return getPositionCount();
    }

    public void setIndex(int index) {
        indexCount = index;
    }

    public void showNext() {
        boolean next = false;
        for (int i = 0; i < Generator.forms.size(); i++) {
            if (Generator.forms.elementAt(i).hashCode() == this.hashCode()) {
                next = true;
            } else if (next) {
                saveAndClear();
                Generator.forms.elementAt(i).show(this);
                break;
            }
        }
    }

    public void showBack() {
        boolean back = false;
        for (int i = Generator.forms.size() - 1; i >= 0; i--) {
            if (Generator.forms.elementAt(i).hashCode() == this.hashCode()) {
                back = true;
            } else if (back) {
                saveAndClear();
                Generator.forms.elementAt(i).show(this);
                break;
            }
        }
    }

    private void saveAndClear() {
        getCode = "";
        saveComponent();
    }

    //untuk menyimpan sementara nilai yang diinputkan user kedalam komponen
    protected void saveComponent() {
//		int sd = components.size();
//		Log.e("test", "savecomponent");
        try {
            for (int i = 0; i < components.size(); i++) {
                String name = components.get(i).getLabel();
                if (Generator.isNewEntry || Generator.currOrderType.equals("tasklistdata")) {
                    if (name.equalsIgnoreCase("bu")) {
                        components.elementAt(i).setText(Generator.bu);
                    } else if (name.equalsIgnoreCase("coyid")) {
                        components.elementAt(i).setText(Generator.coyId);
                    } else if (name.equalsIgnoreCase("platform")) {
                        components.elementAt(i).setText(Generator.platform);
                    } else if (name.equalsIgnoreCase("custtype")) {
                        components.elementAt(i).setText(Generator.custtype);
                    } else {
                        components.elementAt(i).setText(components.elementAt(i).getText());
                    }
                } else {
                    Utility.saveComponent = true;
                    components.elementAt(i).setText(components.elementAt(i).getText());
                }
            }
//			Log.d("seono","asda");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDraft() {
        Connection.DBupdate("DRAFT", "date=" + Utility.Now().substring(0, 10), "data=" + components.get(0).getForm().getDataAll(), "where", "orderCode=" + Generator.currOrderCode);
    }

    public void nextForm(String fname) {
        String val = validation();
        if (val != null) {
            if (!val.equals("")) {
//				showMessage(val, true);
                showDialogMessage(val);
                return;
            }
        }
        show(fname);
    }

    public void showDialogMessage(String message) {
        if (getActivity() != null) {
            getActivity().showDialogMessage(message);
        }
    }

    public void showMessage(String message, boolean cancel) {
        if (getActivity() != null) {
            getActivity().showMessage(message, cancel);
        }
    }

    public String validation() {
        //Log.i("NikitaWilly", "Formvalidation");
        for (int i = 0; i < components.size(); i++) {
            String val = components.elementAt(i).validation();
            if (val != null) {
                if (!val.equals("")) {
                    return val;
                }
            }
        }
        return null;
    }

    public String getFormComponentText(String name) {
        if (name.startsWith("$")) {
            Component comp = getFormComponent(name);
            if (comp != null) {
                return comp.getText();
            }
        } else if (name.startsWith("@@") || name.startsWith("@$")) {
            return name.substring(1);
        } else if (name.startsWith("@+")) {
            return Utility.getSetting(getActivity(), name, "");
        } else if (name.equals("@NOW")) {
            return Utility.Now();
        } else if (name.equals("@TIME")) {
            return Utility.Now().substring(11, 16);
        } else if (name.equals("@SERVER")) {
            return SettingActivity.URL_SERVER;
        } else if (name.startsWith("@GLOBAL.")) {
            return Global.getText(name.substring(8), "");
        } else if (name.equals("@GENERATORMODEL")) {
            return Generator.currModel;
        } else if (name.equals("@GENERATORMODELID")) {
            return Generator.currModel;
        } else if (name.equals("@GENERATORAPPID")) {
            return Generator.currApplication;
        } else if (name.equals("@GENERATORAPPNAME")) {
            return Generator.currApplicationName;
        } else if (name.equals("@GENERATORORDERID")) {
            return Generator.currOrderCode;
        } else if (name.equals("@GENERATORACTIVITYID")) {
            return Generator.currModelActivityID;
        } else if (name.equals("@GENERATORAPPMENU")) {
            return Generator.currAppMenu != null ? Generator.currAppMenu : "";
        } else if (name.equals("@USERNAME")) {
            return Utility.getSetting(getActivity(), "USERNAME", "");
        } else if (name.equals("@PASSWORD")) {
            return Utility.getSetting(getActivity(), "PASSWORD", "");
        } else if (name.equals("@REALNAME")) {
            return Utility.getSetting(getActivity(), "REALNAME", "");
        } else if (name.equals("@IMEI")) {
//            TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(android.content.Context.TELEPHONY_SERVICE);
//            String imei = tm.getDeviceId().toUpperCase();
            String imei = Utility.getImeiOrUUID(getActivity()).getImei().toUpperCase();
            return imei;
        } else if (name.equals("@SPACE")) {
            return " ";
        } else if (name.equals("@NEWLINE")) {
            return "\r\n";
        } else if (name.equals("@SPECIALN")) {
            return "\n";
        } else if (name.equals("@SPECIALR")) {
            return "\r";
        } else if (name.startsWith("@")) {
            return Utility.notNull(Generator.virtual.get(name.substring(1)));
        }
        return name;
    }

    public void setFormComponentText(String name, String text) {
        if (name.startsWith("$")) {
            Component cmp = getFormComponent(name);
            if (cmp != null) {
                cmp.setText(text);
            }
        } else if (name.startsWith("@+")) {
            Utility.setSetting(getActivity(), name, text);
        } else if (name.startsWith("@")) {
            Generator.virtual.put(name.substring(1), text);
        }
    }

    public void setChangeFormComponentMandatory(String name, String text) {
        if (name.startsWith("$")) {
            Component cmp = getFormComponent(name);
            if (cmp != null) {
                cmp.nonMandatory();
            }
        }
    }

    public void setFormComponentLabel(String name, String text) {
        if (name.startsWith("$")) {
            Component cmp = getFormComponent(name);
            if (cmp != null) {
                cmp.setLabel(text);
            }
        } else if (name.startsWith("@+")) {
            Utility.setSetting(getActivity(), name, text);
        } else if (name.startsWith("@")) {
            Generator.virtual.put(name.substring(1), text);
        }
    }

    public Component getFormComponent(String name) {
        if (!name.startsWith("$")) {
            return null;
        }
        name = name.substring(1);
        if (name.startsWith("!")) {
            return getFormComponentbyLabel(name.substring(1));
        }
        if (name.contains(".")) {
            for (int i = 0; i < Generator.forms.size(); i++) {
                String fname = name.substring(0, name.indexOf("."));
                String cname = name.substring(name.indexOf(".") + 1);
                if (fname.equals("*")) {
                    Component cmp = Generator.forms.elementAt(i).getComponentbyName(cname);
                    if (cmp != null) {
                        return cmp;
                    }
                } else if (Generator.forms.elementAt(i).getName().equals(fname)) {
                    return Generator.forms.elementAt(i).getComponentbyName(cname);
                }
            }
        } else {
            return getComponentbyName(name);
        }
        return null;
    }

    private Component getFormComponentbyLabel(String name) {
        if (name.contains(".")) {
            for (int i = 0; i < Generator.forms.size(); i++) {
                String fname = name.substring(0, name.indexOf("."));
                if (Generator.forms.elementAt(i).getTitle().equalsIgnoreCase(fname)) {
                    String cname = name.substring(name.indexOf(".") + 1);
                    return Generator.forms.elementAt(i).getComponentbyLabel(cname);
                }
            }
        } else {
            return getComponentbyLabel(name);
        }
        return null;
    }

    public Vector<Component> getAllComponent() {
        return components;
    }

    public void addComponent(Component comp) {
        components.addElement(comp);
    }

    public void removeAllComponent() {
        components.removeAllElements();
    }

    public Component getComponent(int index) {
        return components.elementAt(index);
    }

    public Component getComponentbyName(String name) {
        for (int i = 0; i < components.size(); i++) {
            if (components.elementAt(i).getName().equals(name)) {
                return components.elementAt(i);
            }
        }
        return null;
    }

    public Component getComponentbyLabel(String name) {
        for (int i = 0; i < components.size(); i++) {
            if (components.elementAt(i).getLabel().equalsIgnoreCase(name)) {
                return components.elementAt(i);
            }
        }
        return null;
    }

    public void addContentView(View view, LayoutParams params) {
        if (getActivity() != null) {
            getActivity().addContentView(view, params);
        }
    }

    public String getDataAll() {
        StringBuffer sbuff = new StringBuffer();
        sbuff.append("[INIT]").append("\r\n");
        sbuff.append("VER=1.1").append("\r\n");

        //untuk membedakan new entry dan tasklist
        if (Generator.isTasklist) {
            sbuff.append("NEWENTRY=").append("N").append("\r\n");
        } else {
            sbuff.append("NEWENTRY=").append("Y").append("\r\n");
        }

        sbuff.append("ID=").append(Generator.currModelActivityID.trim()).append("\r\n");
        sbuff.append("APP=").append(Generator.currApplication).append("\r\n");
        sbuff.append("MODEL=").append(Generator.currModel).append("\r\n");
        sbuff.append("ORDER=").append(Generator.currOrderCode).append("\r\n");
        sbuff.append("ORDERTABLE=").append(Generator.currOrderTable).append("\r\n");
        sbuff.append("APPMENU=").append(Generator.currOrderType.toUpperCase()).append("\r\n");
        sbuff.append("USER=").append(Utility.getSetting(getActivity(), "USERNAME", "")).append("\r\n");
        sbuff.append("POSITION=").append(getPositionCount()).append("\r\n");
        sbuff.append("BATCH=").append(Utility.notNull(Generator.virtual.get("INIT.BATCH")).equals("") ? Utility.getSetting(getActivity(), "BATCH", "") : Utility.notNull(Generator.virtual.get("INIT.BATCH"))).append("\r\n");
        sbuff.append("OPENDATE=").append(Utility.notNull(Generator.virtual.get("INIT.OPENDATE"))).append("\r\n");
        sbuff.append("STARTDATE=").append(Utility.notNull(Generator.virtual.get("INIT.STARTDATE"))).append("\r\n");
        sbuff.append("FINISHDATE=").append(Utility.notNull(Generator.virtual.get("INIT.FINISHDATE"))).append("\r\n");
        sbuff.append("SENDDATE=").append(Utility.notNull(Generator.virtual.get("INIT.SENDDATE"))).append("\r\n");
        sbuff.append("NIKITA=").append(Utility.notNull(Generator.virtual.get("INIT.NIKITA"))).append("\r\n");


        for (int i = 0; i < Generator.forms.size(); i++) {
//			String a = Generator.forms.elementAt(i).getData();
            sbuff.append(Generator.forms.elementAt(i).getData());
        }
        return sbuff.toString();
    }

    /*
     * public String getDataBackForm() { if (back!=null) { StringBuffer sbuff =
     * new StringBuffer(); sbuff.append( getData() ).append(
     * back.getDataBackForm() ); return sbuff.toString(); } return getData(); }
     */
    public String getData() {
        /*
         * default format txtcomma [FORMNAME] COMNAME=TEXT ...
         */
        Vector<Component> comp = getAllComponent();
        StringBuffer sbuff = new StringBuffer();
        sbuff.append("[").append(getName()).append("]").append("\r\n");
        for (int i = 0; i < comp.size(); i++) {
            sbuff.append(comp.elementAt(i).getName()).append("=|").append(comp.elementAt(i).getType()).append("!").append(comp.elementAt(i).getVisible() ? "1" : "0").append(",").
                    append(comp.elementAt(i).getEnable() ? "1" : "0").append(",").append(NfString.put(comp.elementAt(i).getText())).append("\r\n");
        }
        return sbuff.toString();
    }

    public FormActivity getActivity() {
        return owner;
    }

    protected void setActivity(FormActivity activity) {
        owner = activity;
    }

    public Intent getIntent() {
        if (getActivity() != null) {
            return getActivity().getIntent();
        }
        return new Intent();
    }

    public void startActivity(Intent intent) {
        if (getActivity() != null) {
            getActivity().startActivity(intent);
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        if (getActivity() != null) {
            getActivity().startActivityForResult(intent, requestCode);
        }
    }

    public void addActivityResultListener(ActivityResultListener listener) {
        if (listener != null) {
            if (!activityResultListeners.contains(listener)) {
                activityResultListeners.addElement(listener);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //tambahan untuk finder
        if (resultCode == FinderActivity.RESULT_FINDER) {
            String p4 = data.getStringExtra("PARAM4");
//			String mandatory= data.getStringExtra("madatory");
            if (data != null) {
                int selectedrow = data.getIntExtra("ROW", -1);
                setFormComponentText("@FINDERROW", selectedrow + "");
                String[] cols = data.getStringArrayExtra("FIELDS");
                if (cols != null) {
                    for (int i = 0; i < cols.length; i++) {
                        setFormComponentText("@FINDERFIELD" + i, cols[i]);
                    }
                }

                if (p4 != null) {
                    setFormComponentText("@FINDERRESULT", p4);
                }
            }
            for (int i = 0; i < components.size(); i++) {
                if (components.elementAt(i).getName().equals(p4)) {
                    components.elementAt(i).setFocus();
                    components.elementAt(i).runRoute();
                }
            }

        } else {
            //original code, without finder
            if (requestCode == 165899 && resultCode == 0) {
                showPasswordDialog();
            } else if (requestCode == 55555 && resultCode == 0) {
                if (components.size() > 0) {
                    if (Generator.isTasklist) {
                        if (new SaveValidationAction().onAction(components.elementAt(0), new SingleRecordset("param3=autoinfo", "result="))) {
                            new SendActivity().runRoute(components.elementAt(0), new SingleRecordset("param3=autoinfo", "result="));
                        }
                    } else
                        new SendActivity().runRoute(components.elementAt(0), new SingleRecordset("param3=autoinfo", "result="));
                }
            } else if (requestCode == 234352) {
                if (components.size() > 0) {
                    new HomeAction().onAction(components.elementAt(0), new SingleRecordset("param3=", "result="));
                }
            } else if (requestCode == 1670) {
                Connection.DBupdate("data_activity", "status=3", "where", "activityId=" + Generator.currModelActivityID);
                if (components.size() > 0) {
                    new HomeAction().onAction(components.elementAt(0), new SingleRecordset("param3=", "result="));
                }
            } else {

                for (int i = 0; i < activityResultListeners.size(); i++) {
                    if (activityResultListeners.elementAt(i) != null) {
                        activityResultListeners.elementAt(i).onActivityResult(requestCode, resultCode, data);
                    }
                }
            }

        }

    }

    public void onButtonMessageClicked(String button, int reqcode) {
        for (int i = 0; i < buttonMessageClickListeners.size(); i++) {
            if (buttonMessageClickListeners.elementAt(i) != null) {
                buttonMessageClickListeners.elementAt(i).onButtonMessageClick(button, reqcode);
            }
        }

    }

    public void openCamera() {
        openCamera(CAMERA, null);
    }

    public void openIntent(Intent intent, int requestCode, ActivityResultListener listener) {
        if (Utility.requesTtd != requestCode) {
            Utility.requesTtd = requestCode;
            if (listener != null) {
                addActivityResultListener(listener);
            }
        }
        Generator.curractivity.startActivityForResult(intent, requestCode);
    }

    public void openCamera(int requestCode, ActivityResultListener listener) {
        if (Utility.requestCodeCamera != requestCode) {

            Utility.requestCodeCamera = requestCode;
            if (listener != null) {
                activityResultListeners.addElement(listener);
            }
        }
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "image");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        Generator.curractivity.startActivityForResult(intent, requestCode);
    }

    public void openPreviewPDF(int requestCode, String path, ActivityResultListener listener) {
        if (listener != null) {
            activityResultListeners.addElement(listener);
        }

//		final String data = Connection.DBquery("SELECT DATA FROM PDF_VIEW WHERE PDF='"+list+"';").getText(0, 0);
//		final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/view.pdf";
//
//		Messagebox.showProsesBar(getActivity(), new Runnable() {
//				public void run() {
//					create(path, data) ;
//			}}, new Runnable() {
//					public void run() {
//						Intent intent = new Intent(getActivity(), PreviewPDFActivity.class);
//						intent.putExtra("filepdf", path);
//						Generator.curractivity.startActivityForResult(intent, 0);
//				}
//		} );

        Intent intent = new Intent(getActivity(), PreviewPDFActivity.class);
        intent.putExtra("filepdf", path);
        Generator.curractivity.startActivityForResult(intent, requestCode);
    }

    public void openPDFGallery(int requestCode, ActivityResultListener listener) {
        if (listener != null) {
            activityResultListeners.addElement(listener);
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        Generator.curractivity.startActivityForResult(intent, requestCode);
    }

    public void openGallery(int requestCode, ActivityResultListener listener) {

        if (Utility.requestCodeGallery != requestCode) {
            Utility.requestCodeGallery = requestCode;
            if (listener != null) {
                activityResultListeners.addElement(listener);
            }
        }

//		if (listener != null) {
//			activityResultListeners.addElement(listener);
//		}
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        Generator.curractivity.startActivityForResult(intent, requestCode);
    }

    public void openWebBrowser(String Url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Url));
        Generator.curractivity.startActivityForResult(intent, BROWSER);
    }

    @SuppressWarnings("static-access")
    public void openMedia(String fileaudiovideopdf) {
        Uri uri = Uri.fromFile(new File(fileaudiovideopdf));
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        String mime = "*/*";
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        if (mimeTypeMap.hasExtension(mimeTypeMap.getFileExtensionFromUrl(uri.toString())))
            mime = mimeTypeMap.getMimeTypeFromExtension(mimeTypeMap.getFileExtensionFromUrl(uri.toString()));
        intent.setDataAndType(uri, mime);
        Generator.curractivity.startActivity(intent);
    }

    public void setRequestedOrientationPortrait() {
        orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(orientation);
        }
    }

    public void setRequestedOrientationLandscape() {
        orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(orientation);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public interface ListAdapterListener {
        public View getView(Vector data, int position, View convertView, ViewGroup parent);
    }

    public interface BackListener {
        public boolean onBackPressListener();
    }

    public interface ButtonMessageClickListener {
        public void onButtonMessageClick(String button, int reqcode);
    }

    public interface ActivityResultListener {
        public void onActivityResult(int requestCode, int resultCode, Intent data);
    }
}
