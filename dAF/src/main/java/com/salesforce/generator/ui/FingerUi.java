
package com.salesforce.generator.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;
import java.util.Vector;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daf.activity.Finger_New;
import com.daf.activity.LoginActivity;
import com.daf.activity.MenuApplicationActivity;
import com.daf.activity.SettingActivity;
import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.component.IWillDelete;
import com.salesforce.component.IWillSave;
import com.salesforce.component.IWillSend;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.utility.InternalStorageLeft;
import com.salesforce.utility.Utility;

public class FingerUi extends Component implements IWillSave, IWillDelete, IWillSend{
	
	private View mainView;
	private Button btnCreate;
	private ImageView img ;
	private TextView txt;
//	private String strID;

	public FingerUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}
	
	@Override
	public View onCreate(final Form form) {
		// TODO Auto-generated method stub
		mainView = Utility.getInflater(form.getActivity(), R.layout.finger_ui);		
		img = (ImageView)mainView.findViewById(R.id.picture);
		txt = (TextView)mainView.findViewById(R.id.textView1);
		
		btnCreate = (Button)mainView.findViewById(R.id.btnCreateFinger);
		
		btnCreate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				InternalStorageLeft storage = new InternalStorageLeft();
				if(storage.getStorage() <storage.getBlocker()){
					//notif blocker
					AdapterDialog.showDialogOneBtn(form.getActivity(),Global.getText("message.storage.header.blocker"),
							Global.getText("message.storage.blocker")+" "+storage.getSisaStorage(),
							"OK",new DialogInterface.OnClickListener() {						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub							
						}
					});	
//				}else if(storage.getStorage()>=storage.getBlocker()&&storage.getStorage()<=storage.getWarning()){
//					//notif warning					
//					AdapterDialog.showDialogTwoBtn(form.getActivity(),Global.getText("message.storage.header.warning"),Global.getText("message.storage.warning"),"Lanjut",new DialogInterface.OnClickListener() {					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						callFingger(form);						
//						}
//					},"Batal",new DialogInterface.OnClickListener() {					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						
//						}
//				});	
				}else{
					callFingger(form);
				}	
				
			}
		});
		
		setLabel(super.getLabel());
		if (super.getLabel().equalsIgnoreCase("")) {
			txt.setVisibility(View.GONE);
		}	
		
		setVisible(super.getVisible());
		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);

		setImage();
		
		return mainView;
	}
	private void callFingger(Form form){
		Intent intent = new Intent(form.getActivity(), Finger_New.class);
//		Intent intent = new Intent(form.getActivity(), FingerPrintActivity.class);
		if (getText().trim().equals("")) {
			
			intent.putExtra("image", "");
			Utility.deleteFile(Utility.getDefaultTempPath("finger-nikita"));
			Utility.deleteFile(Utility.getDefaultPath("finger-creadential"));
			
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			intent.putExtra("image", Utility.getDefaultTempPath(getText()));
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			intent.putExtra("image", Utility.getDefaultPath(getText()));
		}
		intent.putExtra("fileFinger", Generator.currModelActivityID+"."+getName()+"_4_FP");
		
		form.openIntent(intent, FingerUi.this.hashCode(), new Form.ActivityResultListener() {
			public void onActivityResult(int requestCode, int resultCode, Intent data) {
				 if (requestCode == FingerUi.this.hashCode() && resultCode == Activity.RESULT_OK) {
					 
					 if (data != null) {
						if (data.getStringExtra("flag").equalsIgnoreCase("submit")) {
							
							String path = Utility.getDefaultTempPath("finger-nikita");
							 
							 Utility.openFingerQ(null,path);//delete
							 Utility.openFingerQ(img, path);
							 setText(Generator.currModelActivityID+"."+getName()+"_4_FP");
							 try {
								 Utility.copyFile( path, Utility.getDefaultTempPath(getText()) );
							 } catch (Exception e) { }
							 runRoute();
						}else {

							Utility.openFingerQ(null,"",R.drawable.fingerprint);
//							Utility.openFingerQ(img,"",R.drawable.fingerprint);
							img.setImageResource(R.drawable.fingerprint);
							if (data.getStringExtra("deleted").equalsIgnoreCase("Y")) {
								Utility.deleteFile(Utility.getDefaultTempPath(Generator.currModelActivityID+"."+getName()+"_4_FP"));
								Utility.deleteFile(Utility.getDefaultImagePath(Generator.currModelActivityID+"."+getName()+"_4_FP"));
								Utility.deleteFile(Utility.getDefaultTempPath("finger-nikita.imgicon.png"));
								Utility.deleteFile(Utility.getDefaultTempPath("finger-nikita"));
							}
							setText("");
							
						}
					}
					 
				 }						
			}
		});		
	}
	public void onCompressImage(String file){
		if (!Global.getText("image.compress","false").equalsIgnoreCase("true")) {
			return;
		}
		int quality = Global.getInt("camera.image.quality", 80);
		int width = Global.getInt("image.width.max",540);
		String format = Global.getText("image.format","jpg");
		if (getList().contains("quality=")) {
			String text = getList().substring(getList().indexOf("quality=")+8);
			if (text.contains(";")) {
				quality=Utility.getInt(text.substring(0,text.indexOf(";")));
			}else{
				quality=Utility.getInt(text);
			}
		}
		if (getList().contains("width=")) {
			String text = getList().substring(getList().indexOf("width=")+6);
			if (text.contains(";")) {
				width=Utility.getInt(text.substring(0,text.indexOf(";")));
			}else{
				width=Utility.getInt(text);
			}
		}
		if (getList().contains("format=")) {
			String text = getList().substring(getList().indexOf("format=")+7);
			if (text.contains(";")) {
				format=(text.substring(0,text.indexOf(";")));
			}else{
				format=text;
			}
		}
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds=true;	
			BitmapFactory.decodeFile(file, options); 
			int scale=options.outWidth/width; 
			
			
			options = new BitmapFactory.Options();
			options.inSampleSize=scale;
			 
			Bitmap bmp = BitmapFactory.decodeFile(file,  options);
			 
		    FileOutputStream fos = new FileOutputStream(file);
		    bmp.compress(format.equals("jpg")?Bitmap.CompressFormat.JPEG:Bitmap.CompressFormat.PNG, quality, fos);
		    fos.flush();
			fos.close();		    
		} catch (Exception e) { }
	}
	
	
	private void setImage(){
		if (getText().trim().equals("")) {
			img.setImageResource(R.drawable.fingerprint);
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			Utility.openFingerQ(img, Utility.getDefaultTempPath(getText()), R.drawable.ic_launcher);
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			Utility.openFingerQ(img, Utility.getDefaultPath(getText()), R.drawable.ic_launcher);
		}else if (new File(Utility.getDefaultImagePath(getText())).exists()) {
			Utility.openFingerQ(img, Utility.getDefaultImagePath(getText()), R.drawable.ic_launcher);
		}	
	}

	public void setText(String text) {
		super.setText(text);
//		if (img!=null) {
//			setImage();
//		}
	}
 
	public void setLabel(String text) {
		if (txt!=null) {
			txt.setText(text);
		}
		super.setLabel(text);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (mainView!=null) {
			mainView.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	@Override
	public void setEnable(boolean enable) {
		if (btnCreate!=null) {
			btnCreate.setEnabled(enable);
		}
		if (img!=null) {
			img.setEnabled(enable);
		}
		super.setEnable(enable);
	}

	@Override
	public boolean onWillSend() {
		// TODO Auto-generated method stub
		String isTasklist;
		if (Generator.isTasklist) {
			isTasklist = "true";
		}else{
			isTasklist = "false";
		}
		return sendImage(Generator.currModelActivityID, this.getName(), this.getText(), false, isTasklist, Utility.getSetting(Utility.getAppContext(), "userid", ""), Generator.applno);
	}
	
	public static  boolean sendImage(String ActivityId, String compId,  String fsave, boolean service, String isTasklist, String userId, String applno){
		
		String path = "";
		String str = "";
		
		if (service) {
			Connection.DBopen(Utility.getAppContext());
		}
		
		//ini path untuk kirim langsung
		if (!fsave.equalsIgnoreCase("")) {
			
			
			
			if (new File(Utility.getDefaultPath(fsave)).exists()) {
				path = Utility.getDefaultPath(fsave);
				
				//ini path untuk background service dan pending
			}else if (new File(Utility.getDefaultImagePath(fsave)).exists()) {
				path = Utility.getDefaultImagePath(fsave);
			}
			
			if (path.length() > 5) {
				
				String reqId2 = UUID.randomUUID().toString();
////				8:00 04/01/2019 seno
//				Recordset reqIdformdatabase = Connection.DBquery("select reqId from LOG_IMAGE where imgName ='"+fsave+"'");
//				String reqIdFromDb="";
//				reqIdFromDb= reqIdformdatabase.getText(0, "reqid");
//				String reqId2 = reqId;
//				
//				
//					if(!(reqIdFromDb.equals("")) ){
//						if(!(reqIdFromDb.isEmpty())){
//							reqId2 = reqIdFromDb;
//						}
//					}
				
				if (new File(path).exists()) {
					Connection.DBcreate("LOG_IMAGE", "activityId", "reqId", "userId", "applNo", "type", "imgName", "isTasklist" ,"path", "tgl");
					
					if (Utility.getNewToken()) {
						
						if (isTasklist.equalsIgnoreCase("true")) {
							str = Utility.getHttpConnection(Utility.getURLenc( SettingActivity.URL_SERVER + "imagecheckservlet/?"+(service?"f=service&":"")+"applno=",applno,
									"&modul=", "tasklist","&imagename=",fsave+".", "&token=", Utility.getSetting(Utility.getAppContext(), "Token", ""),"&reqId=",reqId2));
						}else
							str = Utility.getHttpConnection( Utility.getURLenc( SettingActivity.URL_SERVER + "imagecheckservlet/?"+ (service?"f=service&":"")+"imagename=",fsave+".", "&token=", Utility.getSetting(Utility.getAppContext(), "Token", ""),"&reqId=",reqId2));
					
					}else{
						
						str = "Token Gagal";
						
						Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
						if (isTasklist.equalsIgnoreCase("true")) {
							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=B", "imgName="+fsave, 
									"isTasklist=Y", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						}else{
							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=B", "imgName="+fsave, 
									"isTasklist=N", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						}
						
					}			 
					
					 if (service && str.endsWith("SERVICEOFF")) {
						//20/08/2014
						try {
							Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
						} catch (Exception e) { }
					 }	
					 
					 //kirim biometric
					 if (str.equalsIgnoreCase("NO")) {
						 
						 if (Utility.getNewToken()) {
							 if (isTasklist.equalsIgnoreCase("true")) {
								 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?userid="+userId+ 
										 "&applno="+applno+"&modul=tasklist"+"&imei="+Utility.getImei(Utility.getAppContext())+"&token="+Utility.getSetting(Utility.getAppContext(), "Token", ""), null, fsave+".", path,reqId2);
							 }else{
								 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?userid="+userId+
										 "&imei="+Utility.getImei(Utility.getAppContext())+"&token="+Utility.getSetting(Utility.getAppContext(), "Token", ""), null, fsave+".", path,reqId2);
							 }	
							 
						 }
						 
						 if (!str.contains("OK")) {
							 
							 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
							 if (isTasklist.equalsIgnoreCase("true")) {
								Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=B", "imgName="+fsave, 
										"isTasklist=Y", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());							
							 }else{
								Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=B", "imgName="+fsave, 
										"isTasklist=N", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
							 }	
							 
						 }else{
							 
							 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
							 
							 new File(Utility.getDefaultPath(fsave)).delete();
							 new File(Utility.getDefaultImagePath(fsave)).delete();
							 new File(Utility.getDefaultTempPath(fsave)).delete();
							 
							 new File(Utility.getDefaultPath(fsave+"imgicon.png")).delete();
							 new File(Utility.getDefaultImagePath(fsave+"imgicon.png")).delete();
						 }
						 
						 if (service && str.endsWith("SERVICEOFF")) {
								//20/08/2014
								try {
									Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
								} catch (Exception e) { }
						 }
					 }else if (str.contains("FOUND")){
						 
						 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
						 
						 new File(Utility.getDefaultPath(fsave)).delete();
						 new File(Utility.getDefaultImagePath(fsave)).delete();
						 new File(Utility.getDefaultTempPath(fsave)).delete();
						 
					 }else{
						 
						 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
						 if (isTasklist.equalsIgnoreCase("true")) {
							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=B", "imgName="+fsave, 
									"isTasklist=Y", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());							
						 }else{
							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=B", "imgName="+fsave, 
									"isTasklist=N", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						 }	
					 }
				}
				
			}	
			
			
			//kirim gambar fingerprint
			Recordset vP = Connection.DBquery("select * from TBL_PATH_Finger where activityId ='"+ActivityId+"'");
			if (vP.getRows() > 0) {
				
				
				for (int i = 0; i < vP.getRows(); i++) {
					
					if (userId == null || userId.equalsIgnoreCase("")) {
						userId = Utility.getSetting(Utility.getAppContext(), "userid", "");
					}
					
					File nn = new File(vP.getText(i, "path"));
					String fNameImage = nn.getName();
					String pathImg = vP.getText(i, "path");
					
					String reqId_finger2 = UUID.randomUUID().toString();
////					8:00 04/01/2019 seno
//					Recordset reqIdformdatabase = Connection.DBquery("select reqId from LOG_IMAGE where imgName ='"+fNameImage+"'");
//					String reqIdFromDb="";
//					reqIdFromDb= reqIdformdatabase.getText(0, "reqid");
//					String reqId_finger2 = reqId_finger;
//					
//					
//						if(!(reqIdFromDb.equals("")) ){
//							if(!(reqIdFromDb.isEmpty())){
//								reqId_finger2 = reqIdFromDb;
//							}
//						}
					 
					if (!fNameImage.equalsIgnoreCase("")) {
						
						if (new File(pathImg).exists()) {
							
							if (Utility.getNewToken()) {
								if (isTasklist.equalsIgnoreCase("true")) {
									str = Utility.getHttpConnection(Utility.getURLenc( SettingActivity.URL_SERVER + "imagecheckservlet/?"+(service?"f=service&":"")+"applno=",applno,
											"&modul=", "tasklist","&imagename=",fNameImage+".", "&token=", Utility.getSetting(Utility.getAppContext(), "Token", ""),"&reqId=",reqId_finger2));
								}else
									str = Utility.getHttpConnection(Utility.getURLenc( SettingActivity.URL_SERVER + "imagecheckservlet/?"+ (service?"f=service&":"")+"imagename=",fNameImage+".", "&token=", Utility.getSetting(Utility.getAppContext(), "Token", ""),"&reqId=",reqId_finger2));
							 
							}else{
									
								str = "validasi token gagal";
								Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fNameImage} );	
								if (isTasklist.equalsIgnoreCase("true")) {
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId_finger2, "userId="+userId, "applNo="+applno, "type=F", "imgName="+fNameImage, 
											"isTasklist=Y", "path="+Utility.getDefaultImagePath(fNameImage), "tgl="+Utility.getDateDAF());
								}else{
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId_finger2, "userId="+userId, "applNo="+applno, "type=F", "imgName="+fNameImage, 
											"isTasklist=N", "path="+Utility.getDefaultImagePath(fNameImage), "tgl="+Utility.getDateDAF());
								}				
							 }	
							 
							 if (str.equalsIgnoreCase("NO")) {
								 
								 if (Utility.getNewToken()) {
									 if (isTasklist.equalsIgnoreCase("true")) {
										 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?userid="+userId+ 
												 "&applno="+applno+"&modul=tasklist"+"&imei="+Utility.getImei(Utility.getAppContext())+"&token="+
												 Utility.getSetting(Utility.getAppContext(), "Token", ""), null, fNameImage+".", pathImg,reqId_finger2);
									 }else{
										 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?userid="+userId+
												 "&imei="+Utility.getImei(Utility.getAppContext())+"&token="+Utility.getSetting(Utility.getAppContext(), "Token", ""), 
												 null, fNameImage+".", pathImg,reqId_finger2);
									 }	
									 
								 }
								 
								 if (service && str.endsWith("SERVICEOFF")) {
										//20/08/2014
										try {
											Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
										} catch (Exception e) { }
								 }
								 
								 if (str.contains("OK")) {
									 
									 Connection.DBdelete("TBL_PATH_Finger", " activityId =? ", new String[] {ActivityId} );
									 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fNameImage} );
									 new File(Utility.getDefaultPath(fNameImage)).delete();
									 new File(Utility.getDefaultImagePath(fNameImage)).delete();
									 new File(Utility.getDefaultTempPath(fNameImage)).delete();
									 
									 new File(Utility.getDefaultPath(fNameImage+"imgicon.png")).delete();
									 new File(Utility.getDefaultImagePath(fNameImage+"imgicon.png")).delete();
									 
								 }else{
									 
									 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fNameImage} );
									 if (isTasklist.equalsIgnoreCase("true")) {
										Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId_finger2, "userId="+userId, "applNo="+applno, "type=F", "imgName="+fNameImage,
												"isTasklist=Y", "path="+Utility.getDefaultImagePath(fNameImage), "tgl="+Utility.getDateDAF());
									 }else{
										Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId_finger2, "userId="+userId, "applNo="+applno, "type=F", "imgName="+fNameImage,
												"isTasklist=N", "path="+Utility.getDefaultImagePath(fNameImage), "tgl="+Utility.getDateDAF());
						   			 } 
								 }
								 
								 return str.contains("OK");
								 
							 }else if (str.contains("FOUND")){
									
								Connection.DBdelete("TBL_PATH_Finger", " activityId =? ", new String[] {ActivityId} );
								Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fNameImage} );
								 
								new File(Utility.getDefaultPath(fNameImage)).delete();
								new File(Utility.getDefaultImagePath(fNameImage)).delete();
								new File(Utility.getDefaultTempPath(fNameImage)).delete(); 
								
								new File(Utility.getDefaultPath(fNameImage+".imgicon.png")).delete();
								new File(Utility.getDefaultImagePath(fNameImage+".imgicon.png")).delete();
								 
								return true;
								
							 }else{
								 
								 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fNameImage} );
								 if (isTasklist.equalsIgnoreCase("true")) {
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId_finger2, "userId="+userId, "applNo="+applno, "type=F", "imgName="+fNameImage,
											"isTasklist=Y", "path="+Utility.getDefaultImagePath(fNameImage), "tgl="+Utility.getDateDAF());
								 }else{
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId_finger2, "userId="+userId, "applNo="+applno, "type=F", "imgName="+fNameImage,
											"isTasklist=N", "path="+Utility.getDefaultImagePath(fNameImage), "tgl="+Utility.getDateDAF());
								 }	
							 }
						}
					}
				}
				 
			}
		}
		
		return true;
	}

	@Override
	public void onWillDelete() {
		// TODO Auto-generated method stub
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			Utility.deleteFileAll(Utility.getDefaultTempPath(getText()));
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			Utility.deleteFileAll(Utility.getDefaultPath(getText()));
		}
	}

	@Override
	public void onWillSave() {
		// TODO Auto-generated method stub
		try {
			if (getText().trim().equals("")) {
			}else if (new File(Utility.getDefaultTempPath(getText())).exists()) {
				String fname =  getText();
//				Utility.copyFile(Utility.getDefaultTempPath(getText()), Utility.getDefaultPath(fname));
				Utility.copyFile(Utility.getDefaultTempPath(getText()), Utility.getDefaultImagePath(fname));
				
				//agar file tidak menumpuk di temp
//				if (new File(Utility.getDefaultPath(fname)).exists()) {
//					Utility.deleteFile(Utility.getDefaultTempPath(getText()));
//				}
				setText(fname);
				
			}				 
		 } catch (Exception e) {}
	}
	
	@Override
	public void nonMandatory() {
		// TODO Auto-generated method stub
		if (mandatory.contains("*")) {
			mandatory = "";//mandatory.replace("*", "");
		}
		super.nonMandatory();
	}

}
