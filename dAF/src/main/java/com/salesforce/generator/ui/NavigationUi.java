package com.salesforce.generator.ui;

import android.content.Intent;
import android.view.View;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Utility;

public class NavigationUi extends Component implements Form.BackListener {

	private View v;

	public NavigationUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}

	@Override
	public View onCreate(Form form) {
		v = Utility.getInflater(form.getActivity(), R.layout.navigation);
		v.setVisibility(View.GONE);

		if (currRecordset.getText("input_type").equals("back")) {
			form.setOnBackListener(this);
		} else if (currRecordset.getText("input_type").equals("result")) {
			form.addActivityResultListener(new Form.ActivityResultListener() {
				@Override
				public void onActivityResult(int requestCode, int resultCode, Intent data) {
					if (data!=null) {
						if (data.getStringExtra("result")!=null) {
							setText(requestCode+":"+resultCode+"-"+data.getStringExtra("result"));
						}else{
							setText(requestCode+":"+resultCode);
						}
					}else{
						setText(requestCode+":"+resultCode);
					}					
					runRoute();
				}
			});

		} else if (currRecordset.getText("input_type").equals("create")) {
			runRoute();
		} else if (currRecordset.getText("input_type").equals("backcancel")) {
			form.setOnBackListener(this);
		} else {

		}

		return v;
	}

	@Override
	public boolean onBackPressListener() {
		runRoute();
		getForm().setOnBackListener(this);
		if (currRecordset.getText("input_type").equals("backcancel")) {
			if (currRecordset.getText("text").startsWith("backto=")) {
				String fid = currRecordset.getText("text").substring("backto=".length());
				if (fid.length()>=1) {
					 getForm().show(fid);
				}
			}		
			return false;
		} else {
			return true;
		}

	}

}
