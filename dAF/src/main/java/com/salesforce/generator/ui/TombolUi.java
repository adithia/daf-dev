package com.salesforce.generator.ui;

import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class TombolUi extends Component{

	private Button button;
	private View view;
	
	public TombolUi(Form form, String id, SingleRecordset rst) {
		super(form, id, rst);		 
	}
	

	public View onCreate(Form form) {
		view = Utility.getInflater(form.getActivity(), R.layout.managerbutton);
		
		final float scale = form.getActivity().getResources().getDisplayMetrics().density;
		ContextThemeWrapper newContext = new ContextThemeWrapper(form.getActivity(), R.style.UIButton);
		
		button = new Button(newContext, null, R.style.UIButton);
		button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		button.setPadding(button.getPaddingLeft(), button.getPaddingTop() + (int)(18.0f * scale + 0.5f), button.getPaddingRight(), button.getPaddingBottom() + (int)(18.0f * scale + 0.5f));
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { 
				runRoute();
			}
		});
		
		if (currRecordset.getText("input_type").equals("left")) {
			((LinearLayout)view.findViewById(R.id.mngButton1)).addView(button, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}else if (currRecordset.getText("input_type").equals("right")) {
			((LinearLayout)view.findViewById(R.id.mngButton3)).addView(button, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}else {
			((LinearLayout)view.findViewById(R.id.mngButton1)).setVisibility(View.GONE);
			((LinearLayout)view.findViewById(R.id.mngButton3)).setVisibility(View.GONE);
			((LinearLayout)view.findViewById(R.id.mngButton2)).addView(button, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}
		
		if (getList().equalsIgnoreCase("send") && Generator.currAppMenu.equalsIgnoreCase("pending")) {
			setVisible(false);
		}else		
			setVisible(super.getVisible());
		
//		if (getList().equalsIgnoreCase("send")) {
//			setVisible(false);
//		}
		
		setEnable(super.getEnable()); 
		setText(super.getText());
		
		return view;
	}
	
	
	@Override
	public void setText(String text) {
		if (button!=null) {
			if (text.equals("")) {
				button.setText(super.getLabel());
			}else{
				button.setText(text);
			}			
		}
		super.setText(text);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (view!=null) {
			view.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	@Override
	public void setEnable(boolean enable) {
		if (button!=null) {
			button.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	 
}
