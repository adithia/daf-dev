package com.salesforce.generator.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Form.ActivityResultListener;
import com.salesforce.utility.GPSTracker;
import com.salesforce.utility.LocationOffline;
import com.salesforce.utility.Utility;

public class LocationUi extends Component {

	private View v;
	private TextView lblLat;
	private TextView lblLong;
	private GPSTracker gpsTracker;
	private AlertDialog alertDialog;
	private boolean isActive;

	public LocationUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}

	@Override
	public View onCreate(final Form form) {

		gpsTracker = new GPSTracker(form.getActivity());
		v = Utility.getInflater(form.getActivity(), R.layout.location);

		lblLat = (TextView) v.findViewById(R.id.lbl_lat);
		lblLong = (TextView) v.findViewById(R.id.lbl_long);
		isActive = true;
		if (!gpsTracker.canGetLocation()) {
			final LocationOffline loc = new LocationOffline(getForm().getActivity());			
			alertDialog = new AlertDialog.Builder(form.getActivity()).create();
			alertDialog.setMessage("GPS not active, do you want to activate GPS ?");
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent  = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					form.openIntent(intent, LocationUi.this.hashCode(), new ActivityResultListener() {

						@Override
						public void onActivityResult(int requestCode, int resultCode, Intent data) {
							if (requestCode == LocationUi.this.hashCode() && resultCode == Activity.RESULT_CANCELED) {
								GPSTracker gps = new GPSTracker(getForm().getActivity());
								if (gps.canGetLocation()) {
									lblLat.setText(String.valueOf(gps.getLatitude()));
									lblLong.setText(String.valueOf(gps.getLongitude()));
								} if (loc.latitude != 0 && loc.longitude != 0) {
									lblLat.setText(loc.latitude +"");
									lblLong.setText(loc.longitude + "");
								}else {
									isActive = false;
								}
							}
						}
					});

				}
			});

			alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					alertDialog.dismiss();
				}
			});
			alertDialog.show();
		} else {
			lblLat.setText(String.valueOf(gpsTracker.getLatitude()));
			lblLong.setText(String.valueOf(gpsTracker.getLongitude()));
		}
		
		setEnable(super.getEnable());
		setVisible(super.getVisible());

		return v;
	}

	@Override
	public String getText() {
		if (lblLat!=null && lblLong!=null) {
			return lblLat.getText()+","+lblLong.getText();
		}
		return "0,0";
	}
	
	@Override
	public void setEnable(boolean enable) {
		// TODO Auto-generated method stub
		if(v!=null)
			v.setEnabled(enable);
		super.setEnable(enable);}
	@Override
	public void setVisible(boolean visible) {
		// TODO Auto-generated method stub
		if(v!=null)
			v.setVisibility(visible ? View.VISIBLE : View.GONE);
		super.setVisible(visible);
	}

	@Override
	public String validation() {
		if (isActive) {
			return super.validation();
		} else {
			if (mandatory.equals("*")) {
				return "GPS is off";
			}

		}
		return super.validation();
	}

}
