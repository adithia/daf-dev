package com.salesforce.generator.ui;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class DateUi extends Component{
	
	private View v;
	private EditText edtDate;
	int hour, minute, mYear, mMonth, mDay, second;
	private String[] arrMonth = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	static final int STARTDATE = 1;
	private TextView txtLabel;
	private Context contex;
	private ImageButton imgDate;

	public DateUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);	
	}
	
	@Override
	public View onCreate(Form form) {

		contex = form.getActivity();
		v = Utility.getInflater(form.getActivity(), R.layout.date);
		edtDate = (EditText)v.findViewById(R.id.edtDate);
		txtLabel = (TextView)v.findViewById(R.id.textView1);
		imgDate = (ImageButton)v.findViewById(R.id.btnDate);
		
		instanceDate();
 		
		imgDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				onDialog(TYPE_TEXTBOX).show();
			}
		});
		
		if (super.getLabel().equalsIgnoreCase("")) {
			txtLabel.setVisibility(View.GONE);
		}
		
 		setLabel(super.getLabel());
 		setVisible(super.getVisible());
		setText(super.getText());
		
 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
 		
		return v;
	}

	
	@Override
	public String getText() {
		if (edtDate != null) {
			StringBuffer sbuBuffer = new StringBuffer("");			
			if (!edtDate.getText().toString().equalsIgnoreCase("")) {
				sbuBuffer.append(edtDate.getText().toString());
			}
			
			return sbuBuffer.toString();
		}
		return super.getText();
	}
	
	@Override
	public void setText(String text) {
		if (edtDate != null) {
			edtDate.setText(text);
		}
		super.setText(text);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	
	@Override
	public void setEnable(boolean enable) {
		if (imgDate != null) {
			imgDate.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	
	public void setLabel(String label) {
		if (txtLabel != null) {
			txtLabel.setText(label);
		}
		super.setLabel(label);
	};
	
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			String sdate = LPad(mDay + "", "0", 2) + "-" + arrMonth[mMonth] + "-" + mYear ;
			edtDate.setText(sdate);
			edtDate.setTag(sdate);
		}
	};
	
	private static String LPad(String schar, String spad, int len) {
		String sret = schar;
		for (int i = sret.length(); i < len; i++) {
			sret = spad + sret;
		}
		return new String(sret);
	}
	
	private Dialog onDialog(int id){
		
		instanceDate();
		
		switch (id) {
		case STARTDATE:
			DatePickerDialog datePicker = new DatePickerDialog(contex, date, mYear, mMonth, mDay);
			if (getList().equalsIgnoreCase("tgllahir")) {
				datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
//				datePicker.getDatePicker().setMaxDate(getDateMin());
//				datePicker.getDatePicker().setMinDate(getDateYear());
			}
			return datePicker;
		}
		return null;
	}
	
	private void instanceDate(){
		Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
 		mMonth = c.get(Calendar.MONTH);
 		mDay = c.get(Calendar.DAY_OF_MONTH);
	}
	
	private long getDateYear(){
		Calendar cal = Calendar.getInstance();
		cal.set(mYear-60, mMonth, mDay);
		return cal.getTimeInMillis();
	}
	
	private long getDateMin(){
		Calendar cal = Calendar.getInstance();
		cal.set(mYear, mMonth, mDay);
		return cal.getTimeInMillis();
	}

}
