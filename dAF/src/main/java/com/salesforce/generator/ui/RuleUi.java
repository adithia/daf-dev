package com.salesforce.generator.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.crypto.NoSuchPaddingException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

import com.daf.activity.PreviewPDFActivity;
import com.daf.activity.SettingActivity;
import com.daf.rule.DocTypeRuleModel;
import com.daf.rule.NikitaRule;
import com.lowagie.text.Annotation;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.stream.NfDataPDF;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class RuleUi extends Component {

//	private Button button;
	private View view; 
	private Hashtable<String, String> componentCollection = new Hashtable<String, String>();
	private List<DocTypeRuleModel> rule = new ArrayList<DocTypeRuleModel>();
	private Form form;
	private String bln1, bln2, bln3, bln4, bln5, adm1,adm2,adm3;
	
	public RuleUi(Form form, String id, SingleRecordset rst) {
		super(form, id, rst);
		this.form = form;
	}

	public View onCreate(final Form form) {
		view = Utility.getInflater(form.getActivity(), R.layout.rule);
		
		float scale = form.getActivity().getResources().getDisplayMetrics().density;
		ContextThemeWrapper newContext = new ContextThemeWrapper(form.getActivity(), R.style.UIButton);
		
		componentCollection = populate();
//		rule =new  NikitaRule().getNikitaRule(componentCollection);
		
		
		
		if (rule != null && rule.size() > 0) {
			for (int i = 0; i < rule.size(); i++) {
				LinearLayout lay = new LinearLayout(form.getActivity());
				LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				param.setMargins(0, 12, 0, 12);
				
				
				Button button = new Button(newContext, null, R.style.UIButton);
				button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
				button.setPadding(button.getPaddingLeft(), button.getPaddingTop() + (int)(18.0f * scale + 0.5f), button.getPaddingRight(), button.getPaddingBottom() + (int)(18.0f * scale + 0.5f));
				button.setTop((int)(18.0f * scale + 0.5f));
				button.setText(rule.get(i).getDoc_type_desc());
				final int pos = i;
				button.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) { 
						String dds = rule.get(pos).getTemplate_xml();
						if (!rule.get(pos).getTemplate_xml().equalsIgnoreCase("")) {
							createDokumenPDF(rule.get(pos).getTemplate_xml());
						}else {
							if (Utility.isContainFile(rule.get(pos).getTemplate_mobile(), Utility.getDefaultPDF())) {
								showDialog(rule.get(pos).getTemplate_mobile(), true);
							}else
								AdapterDialog.showMessageDialog(form.getActivity(), "Informasi", "Maaf, Dokumen PDF ini tidak tersedia");
						}
					}
				});
				lay.addView(button, param);
				((LinearLayout)view.findViewById(R.id.mngButtonPDF)).addView(lay, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			}
		}
		
		
		return view;
	}
	
	private Hashtable<String, String>  populate(){
		Hashtable<String, String> component = new  Hashtable<String, String>();
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
				component.put(Generator.forms.elementAt(i).components.elementAt(c).getName(), getDataComp(this, Generator.forms.elementAt(i).components.elementAt(c).getText()));
			}
			
		}
		return component;
	}
	
	private void create(String path,  String data){		 
		try {	
			
			String iText = "";	
			InputStream is1 = null;
			
			if (new File(Utility.getDefaultPDF(data)).exists()) {
				is1 = Utility.convertFileToStream(new File(Utility.getDefaultPDF(data)));
			}else{
				AssetManager am1 = getForm().getActivity().getAssets();
//				is1 = am1.open(data);
				is1 = open(data);
			}			
			
			iText = Utility.readFile(is1);
			
			NfDataPDF dataPDF = new NfDataPDF(iText);
			Vector<String> pdf = dataPDF.getData("pdf");
			 
			String bg=pdf.elementAt(0);
			if (bg.contains("=")) {
				bg=bg.substring(bg.indexOf("=")+1);
			}	
			
	        InputStream is = null;
	        
	        if (bg.startsWith("/")) {
	        	//bg jika storage
	        	is = new FileInputStream(bg);
			}else{
				//bg jika aset
				AssetManager am = getForm().getActivity().getAssets();
//				is = am.open(bg);
				is = open(bg);
			}	              
	           
	           
	        PdfReader pdfReader = new PdfReader(is);
	        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(path));	        
	        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,  BaseFont.WINANSI, BaseFont.EMBEDDED);
	        
	        Vector<String> headers = dataPDF.getHeaders();
	        Hashtable<String, Component> formdata =populateComp();
        	
	        for (int i = 0; i < headers.size()-1; i++) {

	        	PdfContentByte content = pdfStamper.getOverContent(i+1);
//	        	content.getPdfDocument().setMargins(0, 0, 0, 0);
	        	Vector<String> datapdf = dataPDF.getData("page"+(i+1));
	        	for (int j = 0; j < datapdf.size(); j++) {
	        		setdata(formdata, content, bf, datapdf.elementAt(j));	        		
				}	        	
	        	
			}
	        
	        pdfStamper.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Hashtable<String, Component>  populateComp(){
		Hashtable<String, Component> component = new  Hashtable<String, Component>();
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
				if (Generator.forms.elementAt(i).components.elementAt(c).getFname().startsWith(":")) {
					component.put(Generator.forms.elementAt(i).components.elementAt(c).getFname(), Generator.forms.elementAt(i).components.elementAt(c));					
				}
			}
			
		}
		return component;
	}
	private void setdata(Hashtable<String, Component> formdata , PdfContentByte content,BaseFont bf, String data){
		if (data.startsWith("{") && data.endsWith("}")) {
			Nset n =Nset.readJSON(data);
			if (n.getData("contain").toString().equalsIgnoreCase("image")) {
				data=n.getData("true").toString();
				String[] v = Utility.split(data+",,,", ",");				
				
				//benerin besokk
				String fileName = getValue(formdata, v[0]);
				File first = new File(Utility.getDefaultTempPath(fileName));				
				if (first.exists()) {
					Utility.copyFile(Utility.getDefaultTempPath(fileName), Utility.getDefaultTempPath(fileName+".png"));
				}else if (new File(Utility.getDefaultPath(fileName)).exists()) {
					Utility.copyFile(Utility.getDefaultPath(fileName), Utility.getDefaultTempPath(fileName+".png"));
				}else if (new File(Utility.getDefaultSign(fileName)).exists()) {
					Utility.copyFile(Utility.getDefaultSign(fileName), Utility.getDefaultTempPath(fileName+".png"));
				}
				
		        try {
		        	String scale = n.getData("scale").toString();
		        	Image image = Image.getInstance(Utility.getDefaultTempPath(fileName)+".png");
		        	if (!scale.equalsIgnoreCase("")) {
						String[] sc = Utility.split(scale, ",");
						image.scaleAbsolute(Integer.parseInt(sc[0]), Integer.parseInt(sc[1]));
					}else
						image.scaleAbsolute(50, 20);
			        image.setAbsolutePosition(Utility.getInt(v[1]), Utility.getInt(v[2]));
			        image.setAnnotation(new Annotation(0, 0, 0, 0, 3));  
					content.addImage(image);
				} catch (IOException e) {
		            e.printStackTrace();
		        } catch (DocumentException e) {
		            e.printStackTrace();
		        }
				
			}else if(n.containsKey("coimage")){
				//cek Apakah equal data benar atau salah
				if (getValue(formdata, n.getData("if").toString()).equals(n.getData("coimage").toString())) {
					data=n.getData("true").toString();
					
					String[] v = Utility.split(data+",,,", ",");
					
					String fileName = getValue(formdata, v[0]);
					File first = new File(Utility.getDefaultTempPath(fileName));
					
					if (first.exists()) {
						Utility.copyFile(Utility.getDefaultTempPath(fileName), Utility.getDefaultTempPath(fileName+".png"));
					}else if (new File(Utility.getDefaultPath(fileName)).exists()) {
						Utility.copyFile(Utility.getDefaultPath(fileName), Utility.getDefaultTempPath(fileName+".png"));
					}else if (new File(Utility.getDefaultSign(fileName)).exists()) {
						Utility.copyFile(Utility.getDefaultSign(fileName), Utility.getDefaultTempPath(fileName+".png"));
					}
					
			        try {
			        	String scale = n.getData("scale").toString();
			        	Image image = Image.getInstance(Utility.getDefaultTempPath(fileName)+".png");
			        	if (!scale.equalsIgnoreCase("")) {
							String[] sc = Utility.split(scale, ",");
							image.scaleAbsolute(Integer.parseInt(sc[0]), Integer.parseInt(sc[1]));
						}else
							image.scaleAbsolute(50, 20);
				        image.setAbsolutePosition(Utility.getInt(v[1]), Utility.getInt(v[2]));
				        image.setAnnotation(new Annotation(0, 0, 0, 0, 3));  
						content.addImage(image);
					} catch (IOException e) {
			            e.printStackTrace();
			        } catch (DocumentException e) {
			            e.printStackTrace();
			        }
				}							
				
			}else if (n.getData("contain").toString().equalsIgnoreCase("fingerimage")) {
				data=n.getData("true").toString();
				String[] v = Utility.split(data+",,,", ",");	
				
				String fileName = getValue(formdata, v[0]);
				String viewImg = "viewImg.png";
				
				File f = new File(Utility.getDefaultPath());        
				File file[] = f.listFiles();
				
//				List<String> listbaru = new ArrayList<String>();
				
				for (int i = 0; i < file.length; i++) {
					if(file[i].getName().contains(fileName+"_")){
//						listbaru.add(file[i].getName());
						viewImg = file[i].getName();
						break;
					}
				}
				
				//benerin besokk
//				String fileName = getValue(formdata, v[0]);
//				File first = new File(Utility.getDefaultTempPath(fileName));
				
//				File da = new File(Utility.getDefaultSign(fileName));
				File first = new File(Utility.getDefaultTempPath(fileName));
				
				if (first.exists()) {
					Utility.copyFingerImageFile(Utility.getDefaultTempPath(fileName), Utility.getDefaultTempPath(viewImg));
				}else if (new File(Utility.getDefaultPath(fileName)).exists()) {
					Utility.copyFingerImageFile(Utility.getDefaultPath(fileName), Utility.getDefaultTempPath(viewImg));
				}else if (new File(Utility.getDefaultSign(fileName)).exists()) {
					Utility.copyFingerImageFile(Utility.getDefaultSign(fileName), Utility.getDefaultTempPath(viewImg));
				}
				
		        try {
		        	String scale = n.getData("scale").toString();
		        	Image image = Image.getInstance(Utility.getDefaultTempPath(viewImg));
		        	if (!scale.equalsIgnoreCase("")) {
						String[] sc = Utility.split(scale, ",");
						image.scaleAbsolute(Integer.parseInt(sc[0]), Integer.parseInt(sc[1]));
					}else
						image.scaleAbsolute(50, 20);
			        image.setAbsolutePosition(Utility.getInt(v[1]), Utility.getInt(v[2]));
			        image.setAnnotation(new Annotation(0, 0, 0, 0, 3));  
					content.addImage(image);
				} catch (IOException e) {
		            e.printStackTrace();
		        } catch (DocumentException e) {
		            e.printStackTrace();
		        }
				
			}else if (n.containsKey("equal_sp")) {			
				if (getValue(formdata, n.getData("if").toString()).equals(n.getData("equal_sp").toString())) {
					//data=n.getData("true").toString();
					
					String[] lines = Utility.split(n.getData("lines").toString(), "|");		
					String[] vLines =  Utility.split(n.getData("value").toString(), "|");		       

					String valueComp = getValue(formdata, n.getData("component").toString());
					String valueComp2 = "";
					for (int i = 0; i < lines.length; i++) {
						String[] v = Utility.split(vLines[i]+",,,", ",");					
						int f = Utility.getInt(v[2]);
						content.beginText();
						content.setFontAndSize(bf, f<=6?6:f);
						if (i == 0) {			        	
							if (valueComp.length() > Integer.parseInt(lines[i])) {
								if (lines.length > 1) {
									valueComp2 = valueComp.substring(Integer.parseInt(lines[i]));
								}
								valueComp = valueComp.substring(0, Integer.parseInt(lines[i])); 
							}				        
						}else{						
							if (valueComp2.length() > Integer.parseInt(lines[i])) {
								valueComp = valueComp2.substring(0, Integer.parseInt(lines[i]));
								valueComp2 = valueComp2.substring(Integer.parseInt(lines[i]));
							}else
								valueComp = valueComp2;
						}			        
						content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[0]), Utility.getInt(v[1]),0);
						content.endText();
					}
				}			
			}else if (n.getData("type").toString().equalsIgnoreCase("newline")) {
		        String[] lines = Utility.split(n.getData("lines").toString(), "|");		
				String[] vLines =  Utility.split(n.getData("value").toString(), "|");		       

	        	String valueComp = getValue(formdata, n.getData("component").toString());
	        	String valueComp2 = "";
		        for (int i = 0; i < lines.length; i++) {
					String[] v = Utility.split(vLines[i]+",,,", ",");					
					int f = Utility.getInt(v[2]);
					content.beginText();
			        content.setFontAndSize(bf, f<=6?6:f);
			        if (i == 0) {			        	
				        if (valueComp.length() > Integer.parseInt(lines[i])) {
				        	if (lines.length > 1) {
								valueComp2 = valueComp.substring(Integer.parseInt(lines[i]));
							}
				        	valueComp = valueComp.substring(0, Integer.parseInt(lines[i])); 
						}				        
					}else{						
						if (valueComp2.length() > Integer.parseInt(lines[i])) {
							valueComp = valueComp2.substring(0, Integer.parseInt(lines[i]));
							valueComp2 = valueComp2.substring(Integer.parseInt(lines[i]));
						}else
							if (!valueComp2.equalsIgnoreCase("")) {
								valueComp = valueComp2;
								valueComp2 = "";
							}else {
								valueComp = "";
							}
							
					}			        
			        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[0]), Utility.getInt(v[1]),0);
			        content.endText();
				}
				
			}else if (n.getData("type").toString().equalsIgnoreCase("replace")) {
				data=n.getData("value").toString();
				String[] v = Utility.split(data+",,,", ",");
				
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        
		        String valueComp = getValue(formdata, v[0]);
		        if (!valueComp.equalsIgnoreCase("")) {
		        	valueComp = Utility.replace(valueComp, "PT", "");
				}
		        
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}else if (n.getData("type").toString().equalsIgnoreCase("date")) {
				data=n.getData("value").toString();
				String[] v = Utility.split(data+",,,", ",");
				
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        
		        String valueComp = getValue(formdata, v[0]);
		        valueComp = Utility.changeFormatDate(valueComp, n.getData("formatawal").toString(), n.getData("formatakhir").toString());
		        
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}else if (n.getData("type").toString().equalsIgnoreCase("currency")) {
				data=n.getData("value").toString();
				String[] v = Utility.split(data+",,,", ",");
				
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        
		        String valueComp = getValue(formdata, v[0]);
		        if (!valueComp.equalsIgnoreCase("")) {
		        	valueComp = Utility.formatCurrency(valueComp);
				}
		        
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}else if (n.containsKey("equal_substring")) {
				//cek Apakah equal data benar atau salah
				if (getValue(formdata, n.getData("if").toString()).equals(n.getData("equal_substring").toString())) {
					data=n.getData("true").toString();
				}else{
					data=n.getData("false").toString();
				}
				
				String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f); 
		        String valueComp = getValue(formdata, v[0]);
		        if (v[4] != null && !v[4].equalsIgnoreCase("")) {
		        	if (v[4].contains("|")) {
		        		String[] sbs = Utility.split(v[4], "|");
		        		if(valueComp.length() > Integer.parseInt(sbs[1])){	        		
		        		//	String[] sbs = Utility.split(v[4], "|");
		        			valueComp = valueComp.substring(Integer.parseInt(sbs[0]),Integer.parseInt(sbs[1]));
		        		}
					}else
						if (valueComp.length() > Integer.parseInt(v[4])) {
							valueComp = valueComp.substring(Integer.parseInt(v[4]));
						}
				}
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}else if (n.getData("type").toString().equalsIgnoreCase("table")) {
				if (getValue(formdata, n.getData("condition").toString()).equalsIgnoreCase(getValue(formdata, n.getData("contain").toString()))) {
					Recordset rcd = Connection.DBquery(n.getData("query").toString());
					for (int i = 0; i < rcd.getRows(); i++) {
						if (i == 0) {
							bln1 = rcd.getText(i, "min_inst_no");
							bln2 = rcd.getText(i, "max_inst_no");
							adm1 = rcd.getText(i, "amount_adm");
						}else if (i == 1) {
							bln3 = rcd.getText(i, "min_inst_no");
							bln4 = rcd.getText(i, "max_inst_no");
							adm2 = rcd.getText(i, "amount_adm");
						}else if (i == 2) {
							bln5 = rcd.getText(i, "min_inst_no");
							adm3 = rcd.getText(i, "amount_adm");
						}
					}
				}
			}else if (n.getData("type").toString().equalsIgnoreCase("checkbox")) {
				String value = getValue(formdata, n.getData("component").toString()).toString();
				if (value.contains("|")) {
					String[] v = Utility.split(value, "|");
					for (int i = 0; i < v.length; i++) {
						String[] pos = Utility.split(n.getData("position").toString(), "|");
						String[] valPos = Utility.split(pos[i], ",");
						
						int f = Utility.getInt(valPos[3]);
						content.beginText();
				        content.setFontAndSize(bf, f<=6?6:f); 
				        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valPos[0] ,Utility.getInt(valPos[1]), Utility.getInt(valPos[2]),0);
				        content.endText();
					}
				}else{
					String[] pos = Utility.split(n.getData("position").toString(), "|");
					String[] val = Utility.split(pos[0], ",");
					int f = Utility.getInt(val[3]);
					content.beginText();
			        content.setFontAndSize(bf, f<=6?6:f); 
			        content.showTextAligned(PdfContentByte.ALIGN_LEFT, val[0] ,Utility.getInt(val[1]), Utility.getInt(val[2]),0);
			        content.endText();
				}
				
			}else if (n.containsKey("header")) {
				data=n.getData("value").toString();
				
				String[] v = Utility.split(data+",,,", ",");
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f); 
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, getStaticValue(v[0]) ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}else if (n.containsKey("equalk")) {
				if (getValue(formdata, n.getData("ifm").toString()).equals(n.getData("equalm").toString())) {
					if (getValue(formdata, n.getData("if").toString()).equals(n.getData("equalk").toString())) {
						data=n.getData("true").toString();
						String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
						int f = Utility.getInt(v[3]);
						content.beginText();
						content.setFontAndSize(bf, f<=6?6:f); 
						String valueComp = getValue(formdata, v[0]);
						if (v[4] != null && !v[4].equalsIgnoreCase("")) {
							if (v[4].contains("|")) {
							String[] sbs = Utility.split(v[4], "|");
							valueComp = valueComp.substring(Integer.parseInt(sbs[0]),Integer.parseInt(sbs[1]));
							}else
							if (valueComp.length() > Integer.parseInt(v[4])) {
								valueComp = valueComp.substring(Integer.parseInt(v[4]));
							}
						}
						content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
						content.endText();
					}
				}				
			}else if (n.containsKey("equal")) {
				if (getValue(formdata, n.getData("if").toString()).equals(n.getData("equal").toString())) {
					data=n.getData("true").toString();
				}else{
					data=n.getData("false").toString();
				}
				
				String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f); 
		        String valueComp = getValue(formdata, v[0]);
		        if (!valueComp.equalsIgnoreCase("") && !v[4].equalsIgnoreCase("")) {
			        if (v[4].contains("|")) {
		        		String[] sbs = Utility.split(v[4], "|");
		        		if(valueComp.length() > Integer.parseInt(sbs[1])){
		        			valueComp = valueComp.substring(Integer.parseInt(sbs[0]),Integer.parseInt(sbs[1]));
		        		}
					}else if (valueComp.length() > Integer.parseInt(v[4])) {
							valueComp = valueComp.substring(Integer.parseInt(v[4]));
					}
				}
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
				
			}else if (n.containsKey("hardcode")) {
				
				data=n.getData("true").toString();
				String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, v[0] ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
		        
			}else if (n.containsKey("specialchar")) {
				
				data=n.getData("true").toString();
				String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, getValue(formdata, v[0])+ n.getData("specialchar").toString(),Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
		        
			}else if (n.containsKey("contain")) {
				if (getValue(formdata, n.getData("if").toString()).contains(getValue(formdata, n.getData("contain").toString()))) {
					data=n.getData("true").toString();
				}else{
					data=n.getData("false").toString();
				}
				
				String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, getValue(formdata, v[0]) ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}else if (n.getData("type").toString().equalsIgnoreCase("currency_right")) {
				data=n.getData("value").toString();
				String[] v = Utility.split(data+",,,", ",");
				
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f);
		        
		        String valueComp = getValue(formdata, v[0]);
		        if (!valueComp.equalsIgnoreCase("")) {
		        	valueComp = Utility.formatCurrency(valueComp);
				}
		        
		        content.showTextAligned(PdfContentByte.ALIGN_RIGHT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}// end by irfan
			
			//Add by Irfan 
			else if (n.containsKey("equal_currency")) {
				//cek Apakah equal data benar atau salah
				if (getValue(formdata, n.getData("if").toString()).equals(n.getData("equal_currency").toString())) {
					data=n.getData("true").toString();
				}else{
					data=n.getData("false").toString();
				}
				
				String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
				int f = Utility.getInt(v[3]);
				content.beginText();
		        content.setFontAndSize(bf, f<=6?6:f); 
		        String valueComp = getValue(formdata, v[0]);
		        if (!valueComp.equalsIgnoreCase("")) {
		        	valueComp = Utility.formatCurrency(valueComp);
				}
		        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
		        content.endText();
			}	
			
	        
		}else{
			
			String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
			int f = Utility.getInt(v[3]);
			content.beginText();
	        content.setFontAndSize(bf, f<=6?6:f); 
	        String valueComp = getValue(formdata, v[0]);
	        if (!valueComp.equalsIgnoreCase("") && !v[4].equalsIgnoreCase("")) {
		        if (v[4].contains("|")) {
	        		String[] sbs = Utility.split(v[4], "|");
	        		if(valueComp.length() > Integer.parseInt(sbs[1])){
	        			valueComp = valueComp.substring(Integer.parseInt(sbs[0]),Integer.parseInt(sbs[1]));
	        		}
				}else if (valueComp.length() > Integer.parseInt(v[4])) {
						valueComp = valueComp.substring(Integer.parseInt(v[4]));
				}
			}
	        content.showTextAligned(PdfContentByte.ALIGN_LEFT, valueComp ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
	        content.endText();
		}
	}
	
	private String getStaticValue(String header){
		if (header != null) {
			if (header.equalsIgnoreCase("bln1")) {
				if (bln1 != null) {
					return bln1;
				}
			}else if (header.equalsIgnoreCase("bln2")) {
				if (bln2 != null) {
					return bln2;
				}
			}else if (header.equalsIgnoreCase("bln3")) {
				if (bln3 != null) {
					return bln3;
				}
			}else if (header.equalsIgnoreCase("bln4")) {
				if (bln4 != null) {
					return bln4;
				}
			}else if (header.equalsIgnoreCase("bln5")) {
				if (bln5 != null) {
					return bln5;
				}
			}else if (header.equalsIgnoreCase("adm1")) {
				if (adm1 != null) {
					return adm1;
				}
			}else if (header.equalsIgnoreCase("adm2")) {
				if (adm2 != null) {
					return adm2;
				}
			}else if (header.equalsIgnoreCase("adm3")) {
				if (adm3 != null) {
					return adm3;
				}
			}
		}
		return "";
	}
	
	private String getValue(Hashtable<String, Component> formdata , String hint){
		if (hint.startsWith(":")) {
			Component component =  formdata.get(hint);
			if (component!=null) {
				return component.getText();
			}
			return "";
		}else{
			return hint;
		}
	}

	@Override
	public String getText() {
		return super.getText().trim();
	}

	@Override
	public void setText(String s) {
		super.setText(s);
	}
	
	@Override
	public void setEnable(boolean enable) {
		super.setEnable(enable);
	}

	@Override
	public void setVisible(boolean visible) {
		if (view != null) {
			view.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		super.setVisible(visible);
	}
	
	@SuppressWarnings("deprecation")
	private void showDialog(final String fileName, final boolean isPdf){
		AlertDialog dialog = null;
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(form.getActivity());
		dialogBuilder.setTitle("Informasi");
		dialogBuilder.setMessage("Maaf, Dokumen yang dimaksud belum tersedia, Apakah anda ingin mendownload dokumen ?");
		dialog = dialogBuilder.create();
		dialog.setButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				downloadDokument(fileName, isPdf);
			}
		});
		
		dialog.setButton2("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub		
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
	
	private void downloadDokument(final String fileName, final boolean isPdf){
		Messagebox.showProsesBar(form.getActivity(), new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String file = fileName;
				if (file.equalsIgnoreCase("")) {
					if (!isPdf) {
						file = fileName.substring(0, fileName.indexOf(".")) + ".txt";
					}
					Utility.downloadDB(SettingActivity.URL_SERVER + "downloadtemplate?templatename="+file+"&v=100", new File(Utility.getDefaultPDF(), fileName));
				}
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (!fileName.equalsIgnoreCase("")) {
					if (isPdf) {
						downloadDokument(fileName, false);
					}else
						createDokumenPDF(fileName);
				}else{
					AdapterDialog.showMessageDialog(form.getActivity(), "Informasi", "Maaf nama file yang didownload tidak ditemukan");
				}
			}
		});
	}
	
	private void createDokumenPDF(final String itext){
		final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/view.pdf";
		Messagebox.showProsesBar(getForm().getActivity(), new Runnable() {
				public void run() { 						
					create(path, itext);
			}}, new Runnable() {
				public void run() {
					Intent intent = new Intent(RuleUi.this.form.getActivity(), PreviewPDFActivity.class);
					intent.putExtra("filepdf", path);
					Generator.curractivity.startActivityForResult(intent, 0);
				}
		});
	}
	
	private InputStream open(String data){		
//		String pathFolder = form.getActivity().getDir("Doc", form.getActivity().MODE_PRIVATE).getAbsolutePath();
		String pathFolder = Utility.getReportPath("");
		File acd =new File(pathFolder,data);
//		Boolean checkExist = acd.exists();
		FileInputStream fileInputStream = null;
		try { 
			fileInputStream = new FileInputStream(acd);		
//			Utility.decrypt(fileInputStream,null,"");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fileInputStream;			
	}
	
	private InputStream open2(String data) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IOException{		
//		String pathFolder = form.getActivity().getDir("Doc", form.getActivity().MODE_PRIVATE).getAbsolutePath();
		String pathFolder = Utility.getReportPath("");
		File acd =new File(pathFolder,data);
//		Boolean checkExist = acd.exists();
		FileInputStream fileInputStream = null;
		InputStream finalStep=null;
		try { 
			fileInputStream = new FileInputStream(acd);		
			finalStep = Utility.decrypt(fileInputStream,null,"");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return finalStep;			
	}

}
