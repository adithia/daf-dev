package com.salesforce.generator.ui;

import java.util.Vector;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class CheckboxUi extends Component{

	public CheckboxUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}
	
	private TextView lblText;
	private View v;
	private CheckBox cekBox;
	private Vector<CheckBox> vectorcekBox = new Vector<CheckBox>();
	private LinearLayout mngCheckbox;
	
	
 
	@SuppressWarnings("rawtypes")
	@Override
	public View onCreate(Form form) {
		vectorcekBox.removeAllElements();
		v = Utility.getInflater(form.getActivity(), R.layout.cekbox);
		lblText = (TextView)v.findViewById(R.id.label);
		//settext untuk label di cekbox
 
		mngCheckbox = (LinearLayout)v.findViewById(R.id.mngCekBox);
		Vector vector = Utility.splitVector(getList(), "|");
		
		final float scale = form.getActivity().getResources().getDisplayMetrics().density;		
		for (int i = 0; i < vector.size(); i++) {	
			
			cekBox = new CheckBox(form.getActivity());
			cekBox.setButtonDrawable(R.drawable.checkbox);
			
			cekBox.setPadding(cekBox.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
					cekBox.getPaddingTop() + (int)(5.0f * scale + 0.5f),
					cekBox.getPaddingRight(),
					cekBox.getPaddingBottom());
			
			vectorcekBox.add(cekBox);
						
			if (vector.elementAt(i).toString().contains(":")) {
				cekBox.setText(vector.elementAt(i).toString().substring(vector.elementAt(i).toString().indexOf(":")));
				cekBox.setTag(vector.elementAt(i).toString());
			}else {
				cekBox.setText(vector.elementAt(i).toString());
				cekBox.setTag(vector.elementAt(i).toString());
			}
			
			cekBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					// TODO Auto-generated method stub					
					runRoute();
				}
			});
			
			mngCheckbox.addView(cekBox,  new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}
		
		if (super.getLabel().equalsIgnoreCase("")) {
			lblText.setVisibility(View.GONE);
		}
		
		setLabel(super.getLabel());
		setText(super.getText());
		setVisible(super.getVisible());
		
		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		
		return v;
		
	}
	
	
	
	@Override
	public String getText() {
		if (mngCheckbox != null) {
			StringBuffer sbuBuffer = new StringBuffer("");
			for (int i = 0; i < vectorcekBox.size(); i++) {
				if (vectorcekBox.elementAt(i).isChecked()) {
					sbuBuffer.append((String)vectorcekBox.elementAt(i).getTag()).append("|");
				}
			}
			if (sbuBuffer.toString().endsWith("|")) {
				return sbuBuffer.toString().substring(0, sbuBuffer.toString().length()-1);
			}
			return sbuBuffer.toString();
		}
		return super.getText();
	}
	
	@Override
	public void setText(String text) {
		if (mngCheckbox != null) {
			if (!text.equalsIgnoreCase("")) {
				Vector<String> textvalue = Utility.splitVector(text, "|");
				if (textvalue != null && textvalue.size() > 0) {
					for (int i = 0; i < vectorcekBox.size(); i++) {
						for (int j = 0; j < textvalue.size(); j++) {
							String value = textvalue.get(j);
							String tag = (String)vectorcekBox.elementAt(i).getTag();
							if (tag.equals(value)) {
								vectorcekBox.elementAt(i).setChecked(true);
							}
						}
					}
				}
			}else {
				for (int i = 0; i < vectorcekBox.size(); i++) {
					vectorcekBox.elementAt(i).setChecked(false);
//					for (int j = 0; j < textvalue.size(); j++) {
//						String value = textvalue.get(j);
//						String tag = (String)vectorcekBox.elementAt(i).getTag();
//						if (tag.equals(value)) {
//							vectorcekBox.elementAt(i).setChecked(true);
//						}else{
//							vectorcekBox.elementAt(i).setChecked(false);
//						}
//					}
				}
			}
			
//			else {
//				for (int i = 0; i < vectorcekBox.size(); i++) {
//					if (text.trim().equals("")) {
//						vectorcekBox.elementAt(i).setChecked(false);
//					}else if (text.equals((String)vectorcekBox.elementAt(i).getTag())) {
//						vectorcekBox.elementAt(i).setChecked(true);
//					}else{
//						vectorcekBox.elementAt(i).setChecked(false);
//					}
//				}
//			}
		}
		super.setText(text);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	
	@Override
	public void setEnable(boolean enable) {
		if (mngCheckbox != null) {
			for (int i = 0; i < vectorcekBox.size(); i++) {
				vectorcekBox.elementAt(i).setEnabled(enable);
			}
		}
		super.setEnable(enable);
	}
	
	@Override
	public void setLabel(String label) {
		if (lblText != null) {
			lblText.setText(label);
		}
		super.setLabel(label);
	}
}
