package com.salesforce.generator.ui;

import java.util.Vector;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class RadioBoxUi extends Component {

	private View v;
	private Vector<RadioButton> vectorRadioBtn = new Vector<RadioButton>();
	private TextView lblText;
	private LinearLayout mngrRadioBtn;
	private RadioButton radioBtn;

	public RadioBoxUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}

	@Override
	public View onCreate(final Form form) {
		vectorRadioBtn.removeAllElements();
		v = Utility.getInflater(form.getActivity(), R.layout.radio_box);
		mngrRadioBtn = (LinearLayout) v.findViewById(R.id.radio_container);
		lblText = (TextView) v.findViewById(R.id.label_radio);
		
		RadioGroup rg = new RadioGroup(form.getActivity());
		rg.setOrientation(RadioGroup.VERTICAL);
		
		Vector<String> vector = Utility.splitVector(getList(), "|");

		for (int i = 0; i < vector.size(); i++) {

			radioBtn = new RadioButton(form.getActivity());
			vectorRadioBtn.add(radioBtn);

			if (vector.elementAt(i).toString().contains(":")) {
				radioBtn.setId(i);
				radioBtn.setText(vector.elementAt(i).toString().substring(vector.elementAt(i).toString().indexOf(":")));
				radioBtn.setTag(vector.elementAt(i).toString());
			} else {
				radioBtn.setId(i);
				radioBtn.setText(vector.elementAt(i).toString());
				radioBtn.setTag(vector.elementAt(i).toString());
			}
			
//			rg.addView(radioBtn);

			// radioBtn.setOnCheckedChangeListener(this);
			radioBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					String val = (String) v.getTag();
					for (int i = 0; i < vectorRadioBtn.size(); i++) {
						String btn = vectorRadioBtn.elementAt(i).getTag().toString();
						if (val.equalsIgnoreCase(btn)) {
							vectorRadioBtn.elementAt(i).setChecked(true);
						} else {
							vectorRadioBtn.elementAt(i).setChecked(false);
						}
					}
					runRoute();
				}
			});

			mngrRadioBtn.addView(radioBtn, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}
		
//		mngrRadioBtn.addView(rg, new LayoutParams (LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
//
//		int count = rg.getChildCount();
//		rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			
//			@Override
//			public void onCheckedChanged(RadioGroup group, int post) {
//				// TODO Auto-generated method stub
//				RadioButton rb = (RadioButton)group.findViewById(post);
//				if (rb.isChecked()) {
//					runRoute();					
//				}
//			}
//		});
		
		if (super.getLabel().equalsIgnoreCase("")) {
			lblText.setVisibility(View.GONE);
		}

		setLabel(super.getLabel());
		setText(super.getText());
		setVisible(super.getVisible());
//		setEnable(super.getEnable());
		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		return v;

	}

	@Override
	public String getText() {
		if (mngrRadioBtn != null) {
			StringBuffer sbuBuffer = new StringBuffer("");
			for (int i = 0; i < vectorRadioBtn.size(); i++) {
				if (vectorRadioBtn.elementAt(i).isChecked()) {
					sbuBuffer.append((String) vectorRadioBtn.elementAt(i).getTag()).append("|");
				}
			}
			if (sbuBuffer.toString().endsWith("|")) {
				return sbuBuffer.toString().substring(0, sbuBuffer.toString().length() - 1);
			}
			return sbuBuffer.toString();
		}
		return super.getText();
	}

	@Override
	public void setText(String text) {
		if (mngrRadioBtn != null) {
			for (int i = 0; i < vectorRadioBtn.size(); i++) {
				if (text.trim().equals("")) {
					vectorRadioBtn.elementAt(i).setChecked(false);
				}else if (text.equals((String) vectorRadioBtn.elementAt(i).getTag()) ) {
					vectorRadioBtn.elementAt(i).setChecked(true);
				} else {
					vectorRadioBtn.elementAt(i).setChecked(false);
				}
			}
		}
		super.setText(text);
	}

	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		super.setVisible(visible);
	}

	@Override
	public void setEnable(boolean enable) {
		if (mngrRadioBtn != null) {
			for (int i = 0; i < vectorRadioBtn.size(); i++) {
				vectorRadioBtn.elementAt(i).setEnabled(enable);
			}
		}
		super.setEnable(enable);
	}

	@Override
	public void setLabel(String label) {
		if (lblText != null) {
			lblText.setText(label);
		}
		super.setLabel(label);
	}

	// @Override
	// public void onCheckedChanged(CompoundButton buttonView, boolean
	// isChecked) {
	// String val = (String) buttonView.getText();
	//
	// for (int i = 0; i < vectorRadioBtn.size(); i++) {
	// if (val.indexOf((String)vectorRadioBtn.elementAt(i).getText())!=-1) {
	// vectorRadioBtn.elementAt(i).setChecked(true);
	// }else{
	// vectorRadioBtn.elementAt(i).setChecked(false);
	// }
	// }
	// }

}
