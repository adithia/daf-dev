package com.salesforce.generator.ui;

import android.view.View;
import android.widget.TextView;

import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;

public class SpecialLabel extends Component{

	public SpecialLabel(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
		// TODO Auto-generated constructor stub
	}
	
	private TextView text;
	@Override
	public View onCreate(Form form) {
		text = new TextView(form.getActivity());
		
		final float scale = form.getActivity().getResources().getDisplayMetrics().density;
		
		text.setPadding(text.getPaddingLeft() + (int)(10.0f * scale + 0.2f), text.getPaddingTop(), 
				text.getPaddingRight() + (int)(10.0f * scale + 0.2f) , text.getPaddingBottom());
		text.setText(getLabel());

		String ds = getDefault();
		String lbl = getLabel();
		String textd = super.getText();
		
		text.setText(textd);
		
//		setText(super.getText());
		setVisible(super.getVisible());
		setEnable(super.getEnable());
		
		return text;
	}
	public void setVisible(boolean visible) {
		if ( text!=null) {
			 text.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	@Override
	public void setEnable(boolean enable) {
		if ( text!=null) {
			 text.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	
	public void setLabel(String txt) {
		if (text!=null) {
			text.setText(txt);
		}
		super.setLabel(txt);
	}
	
	@Override
	public void setText(String strText) {
		// TODO Auto-generated method stub
		if (text!=null) {
			text.setText(strText);
		}
		super.setText(strText);
	}
	
	@Override
	public String getText() {
		// TODO Auto-generated method stub
		if (text!=null) {
			return text.getText().toString();
		}
		return super.getText();
	}

}
