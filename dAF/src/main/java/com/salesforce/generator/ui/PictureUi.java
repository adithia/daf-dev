package com.salesforce.generator.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.channels.AlreadyConnectedException;
import java.util.UUID;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daf.activity.Finger_New;
import com.daf.activity.SettingActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.salesforce.R;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.component.IWillDelete;
import com.salesforce.component.IWillSave;
import com.salesforce.component.IWillSend;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.utility.InternalStorageLeft;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;
 

public class PictureUi extends Component implements IWillSave, IWillSend, IWillDelete{
	private ImageView img, imgDel ;
	private Button tblCamera, tblGallery;
	private LinearLayout mngCamera, mngGallery, mngTengah;
	private TextView txt;
	private String strID = "";
	private View v;
	private Form form;


	ImageLoader imgLoad = ImageLoader.getInstance();
	
	public PictureUi(Form form, String id, SingleRecordset rst) {
		super(form, id, rst);
		// TODO Auto-generated constructor stub
	}
	
	public View onCreate(final Form form) {
		this.form = form;
		
		v = Utility.getInflater(form.getActivity(), R.layout.picture);
		
		img = (ImageView)v.findViewById(R.id.picture);
		imgDel = (ImageView)v.findViewById(R.id.imgDelete);
		tblCamera = (Button)v.findViewById(R.id.btnCamera);		
		txt = (TextView)v.findViewById(R.id.textView1);	
		
		mngCamera = (LinearLayout)v.findViewById(R.id.mngCamera);
		mngGallery = (LinearLayout)v.findViewById(R.id.mngGallery);
		mngTengah = (LinearLayout)v.findViewById(R.id.mngMidle);
		if (mandatory.equalsIgnoreCase("#")) {
			
		}else if (!mandatory.equalsIgnoreCase("") && !mandatory.contains("_picture")) {
			mandatory = mandatory+"_picture";
		}
		
		
		//ini digunakan untuk foto statis, seperti ktp dll
		//digunakan untuk mendapatkan kode foto tersebut
		//kode simpan dalam komponen lain, dan nama komponen tersebut diawali dgn @
		//pada list pictureUI
		String[] strCode = Utility.split(getList(), "|");
		for (int i = 0; i < strCode.length; i++) {
			if (strCode[i].contains("@")) {
				strID = strCode[i].replace("@", "$");
			}
		}
		
//		if (super.getText().contains(super.getName())) {
//			Vector<String> data = Utility.splitVector(getText(), "_");
//			strID = data.get(2);
//		}else			
//			strID = currRecordset.getText("text");
		
//		 if (strID.startsWith("$")) {
//			 mandatory = "#";
//		 }
		
		 tblCamera.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				InternalStorageLeft storage = new InternalStorageLeft();
				if(storage.getStorage() <storage.getBlocker()){
					//notif blocker
					AdapterDialog.showDialogOneBtn(form.getActivity(),Global.getText("message.storage.header.blocker"),
							Global.getText("message.storage.blocker")+" "+storage.getSisaStorage(),
							"OK",new DialogInterface.OnClickListener() {						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub							
						}
					});	
//				}else if(storage.getStorage()>= storage.getBlocker()&&storage.getStorage()<=storage.getWarning()){
//					//notif warning					
//					AdapterDialog.showDialogTwoBtn(form.getActivity(),Global.getText("message.storage.header.warning"),Global.getText("message.storage.warning"),"Lanjut",new DialogInterface.OnClickListener() {					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						callCamera();						
//						}
//					},"Batal",new DialogInterface.OnClickListener() {					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						
//						}
//				});	
				}else{
					callCamera();
				}	
				
			}
		});
		 
		imgDel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//perubahan seno 15-09-2018
				AlertDialog.Builder adb = new AlertDialog.Builder(form.getActivity());
				adb.setTitle("Informasi");
				adb.setMessage("Apakah anda ingin menghapus "+txt.getText().toString()+" ?");
				adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (!getText().equalsIgnoreCase("")) {					
							Utility.deleteFile(Utility.getDefaultTempPath(getText()));
							Utility.deleteFile(Utility.getDefaultPath(getText()));
							Utility.deleteFile(Utility.getDefaultImagePath(getText()));
						}
						
						img.setImageDrawable(getForm().getActivity().getResources().getDrawable(R.drawable.noimage));
						removeValueComponent(getList());
						
					}
				});
				adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				AlertDialog alert = adb.create();
				alert.show();								
			}
		});
		
		tblGallery = (Button)v.findViewById(R.id.btnGallery);		
		tblGallery.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				InternalStorageLeft storage = new InternalStorageLeft();
				if(storage.getStorage() <storage.getBlocker()){
					//notif blocker
					AdapterDialog.showDialogOneBtn(form.getActivity(),Global.getText("message.storage.header.blocker"),
							Global.getText("message.storage.blocker")+" "+storage.getSisaStorage(),
							"OK",new DialogInterface.OnClickListener() {						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub							
						}
					});	
//				}else if(storage.getStorage()>=storage.getBlocker()&&storage.getStorage()<=storage.getWarning()){
//					//notif warning					
//					AdapterDialog.showDialogTwoBtn(form.getActivity(),Global.getText("message.storage.header.warning"),Global.getText("message.storage.warning"),"Lanjut",new DialogInterface.OnClickListener() {					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						callGalery();						
//						}
//					},"Batal",new DialogInterface.OnClickListener() {					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						
//						}
//				});	
				}else{
					callGalery();
				}	
			}			
		});
		
		if (Global.getText("image.show.view.onclick","false").equalsIgnoreCase("true")) {
			img.setOnClickListener(new View.OnClickListener() {
				@SuppressWarnings("static-access")
				public void onClick(View v) {
					try {
						Uri uri = null;
						if (getText().trim().equals("")) {
							return ;
						}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
							uri =  Uri.fromFile(new File(Utility.getDefaultTempPath(getText())));
						}else if (new File(Utility.getDefaultPath(getText())).exists()) {
							uri =  Uri.fromFile(new File(Utility.getDefaultPath(getText())));
						}else{
							return;
						}		
		
						Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
						String mime = "image/*";
						MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
						if (mimeTypeMap.hasExtension(
						    mimeTypeMap.getFileExtensionFromUrl(uri.toString())))
						    mime = mimeTypeMap.getMimeTypeFromExtension(
						        mimeTypeMap.getFileExtensionFromUrl(uri.toString()));
						intent.setDataAndType(uri,mime);
						form.startActivity(intent);			
					} catch (Exception e) {}
				}
			});
		}	
		
		if (currRecordset.getText("input_type").equalsIgnoreCase("camera")) {
			mngGallery.setVisibility(View.GONE);
			mngTengah.setVisibility(View.GONE);
		}else if (currRecordset.getText("input_type").equalsIgnoreCase("gallery")) {
			mngCamera.setVisibility(View.GONE);
			mngTengah.setVisibility(View.GONE);
		}
		
		
		
		setLabel(super.getLabel());
		if (super.getLabel().equalsIgnoreCase("")) {
			txt.setVisibility(View.GONE);
		}	
		
		setVisible(super.getVisible());

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
 		

		
		setImage();
 		
		return v;
	}
	private void callCamera(){
		form.openCamera(PictureUi.this.hashCode()+1, new Form.ActivityResultListener() {
			public void onActivityResult(int requestCode, int resultCode, Intent data) {
				if (Utility.requestCodeCamera != 0) {
					if (requestCode==PictureUi.this.hashCode()+1 && resultCode == Activity.RESULT_OK) {
						 File photo = new File(Environment.getExternalStorageDirectory(),  "image");
						 String path = photo.getAbsolutePath();
						 
						 
						 if (mandatory.contains("_picture")) {
							 mandatory = mandatory.substring(0, mandatory.indexOf("_"));
						 }
						 
						 String strCode = "";
						 if (strID.startsWith("$")) {
							 strCode = getValueComp(strID);								
						 }else {
							 strCode = strID;
						 }

						 //delete file yang dipilih dari gallery
						 Utility.deleteFile(Utility.getDefaultTempPath(Generator.currModelActivityID+"."+getName()+"_1_"+strCode+".jpg"));
						 Utility.deleteFile(Utility.getDefaultImagePath(Generator.currModelActivityID+"."+getName()+"_1_"+strCode+".jpg"));
//						 Utility.deleteFile(Generator.currModelActivityID+"."+getName()+"_1_"+strCode+".jpg");
						 
//						 if (getText().contains(".jpg")) {
//							 setText(Generator.currModelActivityID+"."+getName()+"_0"+"_"+strCode);
//						 }else{
							 setText(Generator.currModelActivityID+"."+getName()+"_0_"+strCode+".jpg");
//						 }

							 
						 try {
							 Utility.copyFile(path, Utility.getDefaultTempPath(getText()));
							 onCompressImage( Utility.getDefaultTempPath(getText()));
							 setImage();

//							 setImage(path);
						 } catch (Exception e) { }
						 
						 runRoute();
					}	
				}					
			}
		});	
	}
	private void callGalery(){
		form.openGallery(PictureUi.this.hashCode()+2, new Form.ActivityResultListener() {
			public void onActivityResult(int requestCode, int resultCode, Intent data) {
				if (Utility.requestCodeGallery != 0) {
					if (requestCode==PictureUi.this.hashCode()+2 && resultCode == Activity.RESULT_OK) {
						 Uri selectedImage = data.getData(); 
						 
						 if (mandatory.contains("_picture")) {
							mandatory = mandatory.substring(0, mandatory.indexOf("_"));
						 }
						 		

						 String strCode = "";
						 if (strID.startsWith("$")) {
							 strCode = getValueComp(strID);								
						 }else {
							 strCode = strID;
						 }
						 
						 //delete file yg dipilih dari photo
						 Utility.deleteFile(Utility.getDefaultTempPath(Generator.currModelActivityID+"."+getName()+"_0_"+strCode+".jpg"));
						 Utility.deleteFile(Utility.getDefaultImagePath(Generator.currModelActivityID+"."+getName()+"_0_"+strCode+".jpg"));
						 
//						 if (getText().contains(".jpg")) {
//							 setText(Generator.currModelActivityID+"."+getName()+"_1"+"_"+strCode);
//						 }else{
							 setText(Generator.currModelActivityID+"."+getName()+"_1"+"_"+strCode+".jpg");
//						 }	
						 
						 try {
							 Utility.copyFile( getForm().getActivity().getContentResolver().openInputStream(selectedImage), Utility.getDefaultTempPath(getText()) );
							 onCompressImage( Utility.getDefaultTempPath(getText()));
							 
							 setImage(Utility.getDefaultTempPath(getText()));
							 
//							 setImage();
						 } catch (Exception e) { 
							 img.setImageURI(selectedImage);
						 }
						 runRoute();
					}
				}						
			}
		});
	}
	private void removeValueComponent(String comp){
		Vector<String> comps = Utility.splitVector(comp, "|");
		for (int i = 0; i < comps.size(); i++) {
			for (int j = 0; j < Generator.forms.size(); j++) {
				for (int c = 0; c < Generator.forms.elementAt(j).components.size(); c++) {
					try {
						String sd = Generator.forms.elementAt(j).components.elementAt(c).getName();
						if (!comps.get(i).contains("@")) {
							if (Generator.forms.elementAt(j).components.elementAt(c).getName().equalsIgnoreCase(comps.get(i).substring(1))) {
								Generator.forms.elementAt(j).components.elementAt(c).setText("");
							}	
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}
		}
		
	}
	
	private String getValueComp(String comp){
		return StringEscapeUtils.escapeSql(getDataComp(this, comp));
	}
	
	private void setImage(){
		if (getText().trim().equals("")) {
			img.setImageResource(R.drawable.noimage);
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			Utility.OpenImage(img, Utility.getDefaultTempPath(getText()), R.drawable.ic_launcher);
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			Utility.OpenImage(img, Utility.getDefaultPath(getText()), R.drawable.ic_launcher);
		}else if (new File(Utility.getDefaultImagePath(getText())).exists()) {
			Utility.OpenImage(img, Utility.getDefaultImagePath(getText()), R.drawable.ic_launcher);
		}
	}
	
	private void setImage(String path){	
		Utility.OpenImage(img,path, R.drawable.ic_launcher);
	}

	public void setText(String text) {
		super.setText(text);
		//untuk cek pertama dibuat
		
		if (text.contains("_")) {
			
			String[] strCode = Utility.split(getList(), "|");
			for (int i = 0; i < strCode.length; i++) {
				if (strCode[i].contains("@")) {
					strID = strCode[i].replace("@", "$");
				}
			}
			
			
		}else if (!text.equalsIgnoreCase("")) {
			strID = text;
		}
		
		if (text.equalsIgnoreCase("") && img != null) {
			img.setImageResource(R.drawable.noimage);
		}
		
//		if (img!=null) {
//			if (text.contains(".jpg") || text.contains(".png")) {
//				setImage();
//			}
//		}
	}
 
	public void setLabel(String text) {
		if (txt!=null) {
			txt.setText(text);
		}
		super.setLabel(text);
	}
	
	@Override
	public void setTag(Object tag) {
		// TODO Auto-generated method stub
		if (v.getTag() != null) {
			v.setTag(tag);
		}
		super.setTag(tag);
	}
	
	@Override
	public void nonMandatory() {
		// TODO Auto-generated method stub
		if (mandatory.contains("*")) {
			mandatory = "";//mandatory.replace("*", "");
		}
		super.nonMandatory();
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (v!=null) {
			v.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	@Override
	public void setEnable(boolean enable) {
		if (tblGallery!=null) {
			tblGallery.setEnabled(enable);
		}
		if (tblCamera!=null) {
			tblCamera.setEnabled(enable);
		}
		if (img!=null) {
			img.setEnabled(enable);
		}
		if (imgDel!=null) {
			imgDel.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	
	public void onWillSave() {
		try {
			if (getText().trim().equals("")) {
			}else if (new File(Utility.getDefaultTempPath(getText())).exists()) {
				String fname = getText();
//				Utility.copyFile(Utility.getDefaultTempPath(getText()), Utility.getDefaultPath(fname));
				Utility.copyFile(Utility.getDefaultTempPath(getText()), Utility.getDefaultImagePath(fname));
				
				//agar file tidak menumpuk di temp
//				if (new File(Utility.getDefaultPath(fname)).exists()) {
//					if (!fname.equalsIgnoreCase("")) {
//						String dfd = Utility.getDefaultTempPath(fname);
//						Utility.deleteFile(Utility.getDefaultTempPath(fname));
//					}
//					
//				}
				setText(fname);
			}			 
		 } catch (Exception e) { }
	}
	
	@Override
	public boolean onWillSend() {
		String isTasklist;
		if (Generator.isTasklist) {
			isTasklist = "true";
		}else{
			isTasklist = "false";
		}
		return sendImage(Generator.currModelActivityID, this.getName(), this.getText(),
				false, isTasklist, Utility.getSetting(Utility.getAppContext(), "userid", ""), Generator.applno);
	}
	
	public static  boolean sendImage(String ActivityId, String compId,  String fsave, boolean service, String isTasklist, String userId, String applno){
		
		if (fsave.trim().equals("")) {
			
		}else{
			String path = "";
			String str = "";
			
			//ini path untuk kirim langsung
			if (new File(Utility.getDefaultPath(fsave)).exists()) {
				path = Utility.getDefaultPath(fsave);
				
				//ini path untuk background service dan pending
			}else if (new File(Utility.getDefaultImagePath(fsave)).exists()) {
				path = Utility.getDefaultImagePath(fsave);
			}
			
			//ini untuk mencegah gambar yg tidak ada file nya untuk tidak dikirim
			if (path.length() > 5) {
				//cek diserver untuk gambar yang sama
					String reqId2 = UUID.randomUUID().toString();
////					8:00 04/01/2019 seno
//					Recordset reqIdformdatabase = Connection.DBquery("select reqId from LOG_IMAGE where imgName ='"+fsave+"'");
//					String reqIdFromDb="";
//					reqIdFromDb= reqIdformdatabase.getText(0, "reqid");
//					String reqId2 = reqId;
//					
//					
//						if(!(reqIdFromDb.equals("")) ){
//							if(!(reqIdFromDb.isEmpty())){
//								reqId2 = reqIdFromDb;
//							}
//						}
					
					
				if (new File(path).exists()) {
					Connection.DBcreate("LOG_IMAGE", "activityId", "reqId", "userId", "applNo", "type", "imgName", "isTasklist" ,"path", "tgl");
//					Connection.DBcreate("LOG_IMAGE", "activityId", "userId", "applNo", "type", "imgName", "isTasklist" ,"path", "tgl");
					
					if (Utility.getNewToken()) {
						if (isTasklist.equalsIgnoreCase("true")) {
							str = Utility.getHttpConnection(Utility.getURLenc(SettingActivity.URL_SERVER + "imagecheckservlet/?"+(service?"f=service&":"")+"applno=",applno,
									"&modul=", "tasklist","&imagename=",fsave, "&token=", Utility.getSetting(Utility.getAppContext(), "Token", ""),"&reqId=",reqId2));
						}else
							str = Utility.getHttpConnection(Utility.getURLenc(SettingActivity.URL_SERVER + "imagecheckservlet/?"+ (service?"f=service&":"")+"imagename=",fsave, "&token=",Utility.getSetting(Utility.getAppContext(), "Token", ""),"&reqId=",reqId2));
						 
					}else{
						str = "Token Gagal";
						Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
						if (isTasklist.equalsIgnoreCase("true")) {
//							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									"isTasklist=Y", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						}else{
//							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									"isTasklist=N", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						}
						
					}
					
					
					 if (service && str.endsWith("SERVICEOFF")) {
						//20/08/2014
						try {
							Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
						} catch (Exception e) { }
					 }				
						
					 //yang lama != Found
					 if (str.equalsIgnoreCase("NO")) {				 
						 
						 if (Utility.getNewToken()) {
//							 str=Utility.postHttpConnection(url, null, fsave, path);
							 
							 if (isTasklist.equalsIgnoreCase("true")) {
								 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?userid="+userId+ 
										 "&applno="+applno+"&modul=tasklist"+"&imei="+Utility.getImei(Utility.getAppContext())+"&token="+Utility.getSetting(Utility.getAppContext(), "Token", ""), null, fsave, path,reqId2);
							 }else{
								 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?userid="+userId+
										 "&imei="+Utility.getImei(Utility.getAppContext())+"&token="+Utility.getSetting(Utility.getAppContext(), "Token", ""), null, fsave, path,reqId2);
							 }	
						 }	
						 
						 if (!str.contains("OK")) {

							 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );	
							 if (isTasklist.equalsIgnoreCase("true")) {
//								Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId,  "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
										Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
										"isTasklist=Y", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
							 }else{
//								Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
										Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
										"isTasklist=N", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
							 }	
							 
						 }else{
							 
							 Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );
							 
							 new File(Utility.getDefaultPath(fsave)).delete();
							 new File(Utility.getDefaultImagePath(fsave)).delete(); 
							 new File(Utility.getDefaultTempPath(fsave)).delete(); 
							 
							 
							 return true;
						 }
						 
						 if (service && str.endsWith("SERVICEOFF")) {
								//20/08/2014
								try {
									Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
								} catch (Exception e) { }
						 }
						 
						return str.contains("OK");
						
					 }else if (str.contains("FOUND")){
						 
						new File(Utility.getDefaultPath(fsave)).delete();
						new File(Utility.getDefaultImagePath(fsave)).delete(); 
						new File(Utility.getDefaultTempPath(fsave)).delete(); 
						
						Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );	
						
						return true;
					}else{

						Connection.DBdelete("LOG_IMAGE", " imgName =? ", new String[] {fsave} );	

						 if (isTasklist.equalsIgnoreCase("true")) {
//							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									"isTasklist=Y", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						 }else{
//							Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									Connection.DBinsert("LOG_IMAGE", "activityId="+ActivityId, "reqId="+reqId2, "userId="+userId, "applNo="+applno, "type=P", "imgName="+fsave,
									"isTasklist=N", "path="+Utility.getDefaultImagePath(fsave), "tgl="+Utility.getDateDAF());
						 }	
					}
				}
				
			}
			
		}	
		return true;
	}
	
	@Override
	public void onWillDelete() {
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			Utility.deleteFileAll(Utility.getDefaultTempPath(getText()));
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			Utility.deleteFileAll(Utility.getDefaultPath(getText()));
		}			
	}
	public void onCompressImage(String file){
		if (!Global.getText("image.compress","false").equalsIgnoreCase("true")) {
			return;
		}
		int quality = Global.getInt("camera.image.quality", 80);
		int width = Global.getInt("image.width.max",540);
		String format = Global.getText("image.format","jpg");
		if (getList().contains("quality=")) {
			String text = getList().substring(getList().indexOf("quality=")+8);
			if (text.contains(";")) {
				quality=Utility.getInt(text.substring(0,text.indexOf(";")));
			}else{
				quality=Utility.getInt(text);
			}
		}
		if (getList().contains("width=")) {
			String text = getList().substring(getList().indexOf("width=")+6);
			if (text.contains(";")) {
				width=Utility.getInt(text.substring(0,text.indexOf(";")));
			}else{
				width=Utility.getInt(text);
			}
		}
		if (getList().contains("format=")) {
			String text = getList().substring(getList().indexOf("format=")+7);
			if (text.contains(";")) {
				format=(text.substring(0,text.indexOf(";")));
			}else{
				format=text;
			}
		}
		format = "jpg";
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds=true;	
			BitmapFactory.decodeFile(file, options); 
			int scale=options.outWidth/width; 
			
			
			options = new BitmapFactory.Options();
			options.inSampleSize=scale;
			Bitmap bmp = BitmapFactory.decodeFile(file,  options);
			 
		    FileOutputStream fos = new FileOutputStream(file);
		    bmp.compress(format.equals("jpg")?Bitmap.CompressFormat.JPEG:Bitmap.CompressFormat.PNG, quality, fos);
		    fos.flush();
			fos.close();		    
		} catch (Exception e) { }
	}
	public void showDialog(Form ctx, String title ,String msg ){
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx.getActivity());
		builder.setTitle(title).setMessage(msg);
		
		AlertDialog alert = builder.create();
		alert.show();
	}
}
