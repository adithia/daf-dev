package com.salesforce.generator.ui;

import java.util.Set;
import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Utility;

public class ScanPrinterUi extends Component {

	// Return Intent extra
	public static String EXTRA_DEVICE_ADDRESS = "device_address";

	private View v;
	// Member fields
	//public static BluetoothService mService = null;

	// private ArrayAdapter<String> mPairedDevicesArrayAdapter;
	// private ArrayAdapter<String> mNewDevicesArrayAdapter;

	private Vector<String> vPairedDevice;
	private Vector<String> vNewDeviced;

	private Activity activity;
	private ListView newDeviceListView;
	private ListView pairedListView;

	private Dialog dialogPrint;
	private Button scanButton;
	public ScanPrinterUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}

	@Override
	public View onCreate(Form form) {

		v = Utility.getInflater(form.getActivity(), R.layout.devicelist);
		activity = form.getActivity();
		activity.setResult(Activity.RESULT_CANCELED);

		scanButton = (Button) v.findViewById(R.id.button_scan);
		scanButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				doDiscovery();
				setUp();
				showConfirm();

			}
		});
		setLabel(super.getLabel());
		
		dialogPrint = new Dialog(activity);
		dialogPrint.setTitle("List Of Bluetooth");

		dialogPrint.setContentView(R.layout.listdevice);

		// Initialize array adapters. One for already paired devices and one for
		// newly discovered devices
		vPairedDevice = new Vector<String>();
		vNewDeviced = new Vector<String>();

		// Find and set up the ListView for paired devices
		pairedListView = (ListView) dialogPrint.findViewById(R.id.lstPaired);
		pairedListView.setOnItemClickListener(mDeviceClickListener);

		// Find and set up the ListView for newly discovered devices
		newDeviceListView = (ListView) dialogPrint.findViewById(R.id.lstNewDevices);
		newDeviceListView.setOnItemClickListener(mDeviceClickListener);

		// Register for broadcasts when a device is discovered
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		activity.registerReceiver(mReceiver, filter);

		// Register for broadcasts when discovery has finished
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		activity.registerReceiver(mReceiver, filter);

		 

		// Get a set of currently paired devices
		Set<BluetoothDevice> pairedDevices = getForm().getActivity().getBluetoothService().getPairedDev();

		// If there are paired devices, add each one to the ArrayAdapter
		if (pairedDevices.size() > 0) {
			//v.findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
			for (BluetoothDevice device : pairedDevices) {
				vPairedDevice.add(device.getName() + "\n" + device.getAddress());
			}
		} else {
			String noDevices = "noDevices";
			vPairedDevice.add(noDevices);
		}
		setLabel(super.getLabel());
		 
		return v;
	}
	public void setLabel(String label) {
		super.setLabel(label);
		if (scanButton != null) {
			scanButton.setText(label);
		}		
	};
	/**
	 * Start device discover with the BluetoothAdapter
	 */
	private void doDiscovery() {

		// Indicate scanning in the title
		activity.setProgressBarIndeterminateVisibility(true);
		activity.setTitle("Scanning");

		// If we're already discovering, stop it
		if ( getForm().getActivity().getBluetoothService().isDiscovering()) {
			 getForm().getActivity().getBluetoothService().cancelDiscovery();
		}

		// Request discover from BluetoothAdapter
		 getForm().getActivity().getBluetoothService().startDiscovery();
	}

	// The on-click listener for all devices in the ListViews
	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

			// Cancel discovery because it's costly and we're about to connect
			 getForm().getActivity().getBluetoothService().cancelDiscovery();

			// Get the device MAC address, which is the last 17 chars in the view 
			String info = String.valueOf(v.getTag());
			try {
				//FormActivity.mService.stop();
			} catch (Exception e) { }
			
			if(!info.equals("noDevices")){
				 String address = info.substring(info.length() - 17);
				 setText(address);
				 String[] s = Utility.split(info +"\n", "\n");
				 ScanPrinterUi.this.getForm().setFormComponentText("@BLUETOOTHMAC", address);
				 ScanPrinterUi.this.getForm().setFormComponentText("@BLUETOOTHNAME", s[0]);				 
				 dialogPrint.dismiss();
				 runRoute();
				 
			}
		}
	};

	// The BroadcastReceiver that listens for discovered devices and changes the
	// title when discovery is finished
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {

				// Get the BluetoothDevice object from the Intent
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				// If it's already paired, skip it, because it's been listed
				// already
				if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
					String deviceInfo = device.getName() + "\n" + device.getAddress();
					if (!vNewDeviced.contains(deviceInfo)) {
						vNewDeviced.add(deviceInfo);
					}
				}

				// When discovery is finished, change the Activity title
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				activity.setProgressBarIndeterminateVisibility(false);
				activity.setTitle("ACTION_DISCOVERY_FINISHED");
				if (vNewDeviced.size() == 0) {
					String noDevices = "noDevices";
					vNewDeviced.add(noDevices);
				}

				dialogPrint.findViewById(R.id.progressBluetooth).setVisibility(View.GONE);

				AdapterConnector.setConnector(newDeviceListView, vNewDeviced, R.layout.devicebluetooth, new AdapterInterface() {

					@Override
					public View onMappingColumn(int row, View v, Object data) {
						((TextView) v.findViewById(R.id.blueTooth)).setText(String.valueOf(data));
						v.setTag(data);
						return v;
					}
				});

			}
		}
	};

	private void setUp() {

		if (vNewDeviced != null && vNewDeviced.size() > 0) {
			dialogPrint.findViewById(R.id.progressBluetooth).setVisibility(View.GONE);
		}

		AdapterConnector.setConnector(pairedListView, vPairedDevice, R.layout.devicebluetooth, new AdapterInterface() {

			@Override
			public View onMappingColumn(int row, View v, Object data) {
				String val = String.valueOf(data);
				((TextView) v.findViewById(R.id.blueTooth)).setText(val);
				v.setTag(data);
				return v;
			}
		});

	}

	private void showConfirm() {
		dialogPrint.show();
	}

}
