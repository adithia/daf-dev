package com.salesforce.generator.ui;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.utility.Utility;

public class GridUi extends Component {

	private View v;
	private TextView lbl;
	private TableLayout tLayout;
	// private Activity activity;
	private LayoutInflater inflater;
	String txtRow = "";
	private Form form;
	
	public GridUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
		this.form=form;
	}

	@Override
	public View onCreate(Form form) {
		v = Utility.getInflater(form.getActivity(), R.layout.grid);
		// activity = form.getActivity();
		tLayout = (TableLayout) v.findViewById(R.id.table_layout);

		inflater = (LayoutInflater) Utility.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		lbl = (TextView) v.findViewById(R.id.lbl_name);
		if (getLabel() == null)
			lbl.setVisibility(View.GONE);

		setText(super.getText());
//		setEnable(super.getEnable());
		setVisible(super.getVisible());
		setLabel(super.getLabel());
		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);

		return v;
	}

	
	@Override
	public void setLabel(String label) {
		if (lbl != null) {
			lbl.setText(label);
		}
		super.setLabel(label);
	}

	@Override
	public void setVisible(boolean visible) {
		// TODO Auto-generated method stub
		if (tLayout != null)
			tLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		super.setVisible(visible);
	}

	@Override
	public void setEnable(boolean enable) {
		// TODO Auto-generated method stub
		if (tLayout != null)
			tLayout.setEnabled(enable);
		super.setEnable(enable);
	}

	@Override
	public void setText(String text) {
		if (text.equals("Recordset")) {

			// list = Utility.splitVector(getList(), "|");

			if (tLayout != null && Generator.currRecordset != null) {
				tLayout.removeAllViews();

				TableRow tHeader = (TableRow) inflater.inflate(R.layout.table_header, null);

				int col = Generator.currRecordset.getCols();

				for (int i = 0; i < col; i++) {
					View vItem = (RelativeLayout) inflater.inflate(R.layout.table_item_header_border, null);
					ImageView imgBackground = (ImageView) vItem.findViewById(R.id.img_background);

					if (i == 0)
						imgBackground.setImageResource(R.drawable.header1);
					else if (i == (col - 1))
						imgBackground.setImageResource(R.drawable.header3);
					else
						imgBackground.setImageResource(R.drawable.header2);

					TextView txtView = (TextView) vItem.findViewById(R.id.textView1);
					txtView.setText(Generator.currRecordset.getHeader(i));
					tHeader.addView(vItem);
				}
				tLayout.addView(tHeader);

				
				
				int row = Generator.currRecordset.getRows();

				for (int i = 0; i < row; i++) {
					TableRow tRow = (TableRow) inflater.inflate(R.layout.table_row, null);
					txtRow = "";

					for (int j = 0; j < col; j++) {
						View vItem = (RelativeLayout) inflater.inflate(R.layout.table_item_header_border, null);

						String textCell = Generator.currRecordset.getText(i, j);

						if (j == col - 1)
							txtRow = txtRow + textCell;
						else
							txtRow = txtRow + textCell + "\t";

						TextView txtView = (TextView) vItem.findViewById(R.id.textView1);
						txtView.setText(textCell);
						
					 
						
						if (Global.getText(form.getName()+ ".grid."+j+".alignment").equals("left")) {
							((LayoutParams)txtView.getLayoutParams()).gravity = Gravity.LEFT;
						}else if (Global.getText(form.getName()+ ".grid."+j+".alignment").equals("right")) {
							((LayoutParams)txtView.getLayoutParams()).gravity = Gravity.RIGHT;
						}
						
					  
						
						if (Global.getInt(form.getName()+ ".grid."+j+".width")>=1) {
							txtView.setWidth(Utility.dpToPx( form.getActivity() , Global.getInt(form.getName()+ ".grid."+j+".width") ));
						}
					 
						ImageView imgBackground = (ImageView) vItem.findViewById(R.id.img_background);

						if (j == 0) {
							if (i % 2 == 0)
								imgBackground.setImageResource(R.drawable.rowgenap1);
							else
								imgBackground.setImageResource(R.drawable.rowganjil1);
						} else if (j == (col - 1)) {
							if (i % 2 == 0)
								imgBackground.setImageResource(R.drawable.rowgenap3);
							else
								imgBackground.setImageResource(R.drawable.rowganjil3);
						} else {
							if (i % 2 == 0)
								imgBackground.setImageResource(R.drawable.rowgenap2);
							else
								imgBackground.setImageResource(R.drawable.rowganjil2);
						}

						tRow.addView(vItem);
					}
					tRow.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							setText(txtRow);
							runRoute();
						}
					});

					tLayout.addView(tRow);
				}

			}

		} else
			super.setText(text);
	}
}
