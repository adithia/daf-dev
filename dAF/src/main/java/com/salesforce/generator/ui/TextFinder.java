package com.salesforce.generator.ui;

import java.util.Vector;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.action.ShowFinderAction;
import com.salesforce.utility.Utility;

public class TextFinder extends Component{

	private View view;
	private Context context;
	public ImageView btnSearch, btnClear;
	private TextView lblText;
	private EditText edtSearch;
	private ShowFinderAction finder;
	private String txtAwal = "", txtAkhir ="";
	private long icurr = System.currentTimeMillis();
	private Handler handle;
	private boolean ranrt = false, isDepandOn = false;
	private String comDepandOn = "";
	
	public TextFinder(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}

	@Override
	public View onCreate(final Form form) {
		// TODO Auto-generated method stub
		context = form.getActivity();
		view = Utility.getInflater(context, R.layout.finder);

		lblText = (TextView)view.findViewById(R.id.title);
		btnSearch = (ImageView)view.findViewById(R.id.btnSearch);
		btnClear = (ImageView)view.findViewById(R.id.btnClear);
		edtSearch = (EditText)view.findViewById(R.id.edtFind);
		
		Vector vector = Utility.splitVector(getList(), "|");
		
		if (vector.size() > 1) {
			isDepandOn = true;
			comDepandOn = vector.elementAt(1).toString();
		}
		
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ranrt = true;
				if (getList().trim().length()>=3) {
					setText(edtSearch.getText().toString());
					edtSearch.setFocusable(true);
					edtSearch.requestFocus();
					new ShowFinderAction().showFinderActivity(TextFinder.this, "", "", getList(), getName());
					
				}else{
					runRoute();
				}
				
			}
		});
		
		btnClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
				//seno
				if(Generator.currAppMenu.equalsIgnoreCase("pending")){
//					AlertDialog.Builder adb = new AlertDialog.Builder(form.getActivity());
//					adb.setTitle("Informasi");
//					adb.setMessage("tes btn dell");
//					AlertDialog dialog = adb.create();
//					dialog.show();					
				}else{
					if (!getDefault().equalsIgnoreCase("")) {
						Vector<String> comp = Utility.splitVector(getDefault(), "|");
						for (int i = 0; i < comp.size(); i++) {
							if (comp.elementAt(i).trim().contains("$")) {
								removeValueComponent(comp.elementAt(i).trim());
							}else{
								setFalseEnableComponent(comp.elementAt(i).trim());
							}
						}
					}
					edtSearch.setText("");
					btnClear.setVisibility(View.GONE);
					btnSearch.setVisibility(View.VISIBLE);
				}	
				//seno
//				runRoute();
			}
		});
		
		
		edtSearch.setOnTouchListener(new OnTouchListener() {
		
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction()==MotionEvent.ACTION_DOWN) {
					txtAwal = edtSearch.getText().toString();
					edtSearch.addTextChangedListener(fieldListener());
				}
				return false;
			}
		});
		
		setLabel(super.getLabel());
 		setVisible(super.getVisible());
		setText(super.getText());
		
 		if (Generator.freez) {
 			setEnable(super.getEnable());
		}else
			setEnable(false);
		

		
		//13k ===========================================================================//
 		handle = new Handler();
 		edtSearch.addTextChangedListener(new TextWatcher() {
 			String old = "";
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				old = s.toString();
			}
			public void afterTextChanged(Editable s) {
				if (!old.equals(s.toString()) && edtSearch.isFocused()) {
					ranrt = false;
					handle.postDelayed(new Runnable() {
						public void run() {
							if (Math.abs(icurr - System.currentTimeMillis())>1000 && !ranrt) {
							}
							
						}
					}, 1000);
				}
				
			}
		});
 		//===========================================================================//
		return view;
	}
	
	private void removeValueComponent(String comp){
		if (!comp.equalsIgnoreCase("")) {
			for (int i = 0; i < Generator.forms.size(); i++) {
				for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
					if (Generator.forms.elementAt(i).components.elementAt(c).getName().equalsIgnoreCase(comp.substring(1))) {
						Generator.forms.elementAt(i).components.elementAt(c).setText("");
					}
				}
				
			}
		}
	}
	
	private void setFalseEnableComponent(String comp){
		if (!comp.equalsIgnoreCase("")) {
			for (int i = 0; i < Generator.forms.size(); i++) {
				for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
					if (Generator.forms.elementAt(i).components.elementAt(c).getName().equalsIgnoreCase(comp.substring(1))) {
						Generator.forms.elementAt(i).components.elementAt(c).setEnable(false);
					}
				}
				
			}
		}
	}
	
	public TextWatcher fieldListener(){
		return new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub		
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (mandatory.equalsIgnoreCase("*")) {
					mandatory = "*T";
				}
			}
		};
	}
	
	@Override
	public String getText() {
		if (edtSearch != null) {
			StringBuffer sbuBuffer = new StringBuffer("");			
			if (!edtSearch.getText().toString().equalsIgnoreCase("")) {
				sbuBuffer.append(edtSearch.getText().toString());
			}
			
			return sbuBuffer.toString();
		}
		return super.getText();
	}
	
	@Override
	public void setText(String text) {
		if (edtSearch != null) {
			boolean b = ranrt;
			ranrt = true;
			edtSearch.setText(text);
			ranrt = b;
		}
		if (btnSearch != null && btnClear != null) {			
			if (!text.equalsIgnoreCase("")) {
				btnSearch.setVisibility(View.GONE);
				btnClear.setVisibility(View.VISIBLE);
			}else{				
				btnSearch.setVisibility(View.VISIBLE);
				btnClear.setVisibility(View.GONE);
			}
		}
		
		if (mandatory.contains("T")) { 
			mandatory = mandatory.substring(0, mandatory.indexOf("T"));		 				
		} 	
		super.setText(text);
	}
	
	@Override
	public void setEnable(boolean enable) {
		if (btnSearch != null) {
			btnSearch.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (view != null) {
			view.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	
	@Override
	public void setLabel(String label) {
		if (lblText != null) {
			lblText.setText(label);
		}
		super.setLabel(label);
	}
}
