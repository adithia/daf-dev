package com.salesforce.generator.ui;

import java.util.List;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.utility.Utility;

public class SpinnerUi extends Component {

	public SpinnerUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}

	private View v;
	private android.widget.Spinner spin;
	private TextView lblText;
	private List<String> list;

	@Override
	public View onCreate(Form form) {
		v = Utility.getInflater(form.getActivity(), R.layout.spinner);
		spin = (android.widget.Spinner) v.findViewById(R.id.spinUI);
		lblText = (TextView) v.findViewById(R.id.lblSpinner);

		list = Utility.splitVector(getList(), "|");
		if (Global.getText("generator.ui.dropdown.default").length()>=1) {
			list.add(0,Global.getText("generator.ui.dropdown.default"));
		}		
		
		Utility.setAdapterSpinner(form.getActivity(), spin, list);

		spin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				spin.setTag(list.get(position));
				setText(list.get(position));
				runRoute();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		if (super.getLabel().equalsIgnoreCase("")) {
			lblText.setVisibility(View.GONE);
		}

		setLabel(super.getLabel());
		setText(super.getText());
		setVisible(super.getVisible());
//		setEnable(super.getEnable());
		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		return v;
	}

	@Override
	public String getText() {
		if (spin != null) {
			return (String)spin.getSelectedItem();
		}
		return super.getText();
	}

	@Override
	public void setText(String text) {
		if (spin != null) {
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).equals(text))
					spin.setSelection(i);
			}
		}
		super.setText(text);
	}

	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		super.setVisible(visible);
	}

	@Override
	public void setEnable(boolean enable) {
		if (spin != null) {
			spin.setEnabled(enable);
		}
		super.setEnable(enable);
	}

	@Override
	public void setLabel(String label) {
		if (lblText != null) {
			lblText.setText(label);
		}
		super.setLabel(label);
	}
	@Override
	public String validation() {
		if (  mandatory.trim().equals("*") ) {
			if (Global.getText("generator.ui.dropdown.default").length()>=1 && getText().equals(Global.getText("generator.ui.dropdown.default"))) {
				return "Field "+getLabel()+" is mandatory";
			}
		} 
		return super.validation();
	}
 
}
