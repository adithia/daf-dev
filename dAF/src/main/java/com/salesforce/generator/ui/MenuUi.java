package com.salesforce.generator.ui;

import java.util.Vector;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Utility;

public class MenuUi extends Component{

	public MenuUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
		// TODO Auto-generated constructor stub
	}
	View v;
	private 
	Vector<MenuUi> vector = new Vector<MenuUi>();
	static final int STARTDATE = 1;
	private Context contex;
	
	@Override
	public View onCreate(Form form) {
			
		vector.removeAllElements();
		 v = super.onCreate(form);
		 v.setVisibility(View.GONE);	
		 contex = form.getActivity();
		 
		 if (currRecordset.getText("input_type").equals("menuback")) {
			 ImageView imgLeft = form.findImageViewById(R.id.imageView1);
			 imgLeft.setVisibility(View.VISIBLE);
			 imgLeft.setImageResource(R.drawable.menuback);
			 imgLeft.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					runRoute();					
				}
			});
			Utility.setImageTouch(imgLeft);
		 }else {

			ImageView imgLeft = form.findImageViewById(R.id.imageView2);
			Utility.setImageTouch(imgLeft);
			imgLeft.setVisibility(View.VISIBLE);
			if (currRecordset.getText("input_type").equals("menusetting")) {
				imgLeft.setImageResource(R.drawable.menusetting);				
			}else if (currRecordset.getText("input_type").equals("menusave")) {
				imgLeft.setImageResource(R.drawable.menusave);
			}else if (currRecordset.getText("input_type").equals("menunext")) {
				imgLeft.setImageResource(R.drawable.menunext);
			}else if (currRecordset.getText("input_type").equals("menusend")) {
				imgLeft.setImageResource(R.drawable.menudatasent);
			}
			for (int i = 0; i < form.getAllComponent().size(); i++) {
				if (form.getComponent(i) instanceof MenuUi) {
					if (!((MenuUi)form.getComponent(i)).isContainsBack()) {
						vector.addElement((MenuUi)form.getComponent(i));						
					}
				}
			}
			if (vector.size() > 1) {
				imgLeft.setImageResource(R.drawable.menugroup);
				imgLeft.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						onDialog(STARTDATE);
					}
				});
			}else {
				imgLeft.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						runRoute();					
					}
				});
			}
		}
		 
		 
		 
		 return v;
	}
	
	public boolean isContainsBack(){
		return currRecordset.getText("input_type").equals("menuback");
	}
	
	public String getTitle(){
		if (currRecordset.getText("input_type").equals("menusetting")) {
			return "Setting";				
		}else if (currRecordset.getText("input_type").equals("menusave")) {
			return "Save";
		}else if (currRecordset.getText("input_type").equals("menunext")) {
			return "Next";
		}else if (currRecordset.getText("input_type").equals("menusend")) {
			return "Send";
		}
		return "";
	}
	
	private Dialog onDialog(int id) {
		switch (id) {
		case STARTDATE:
			AlertDialog dialog = null;
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(contex);
			dialogBuilder.setTitle("Menu");

			Vector<String> list = new Vector<String> ();
			
			for (int i = 0; i < vector.size(); i++) {
				list.addElement(vector.elementAt(i).getTitle());
			}

			ListAdapter listAdapter = new ArrayAdapter<String>(contex, android.R.layout.simple_list_item_1, list);

			dialogBuilder.setAdapter(listAdapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
//					setText(Generator.forms.elementAt(which).getName());
					
					dialog.dismiss();
					vector.elementAt(which).runRoute();
//					runRoute();
				}
			});

			dialog = dialogBuilder.create();
			dialog.show();
		}
		return null;
	}

}
