package com.salesforce.generator.ui;

import android.annotation.SuppressLint;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class WebUi extends Component{

	public WebUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
		// TODO Auto-generated constructor stub
	}
	
	private View v;
	private WebView web;
	private TextView txt;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreate(Form form) {
		v = Utility.getInflater(form.getActivity(), R.layout.webui);
		web = (WebView)v.findViewById(R.id.webView1);
		web.getSettings().setJavaScriptEnabled(true);
		web.loadUrl(super.getText());
		web.setTag(super.getText());
		
		if (super.getLabel().equalsIgnoreCase("")) {
			((TextView)v.findViewById(R.id.textView1)).setVisibility(View.GONE);
		}
		
		setLabel(super.getLabel());
 		setVisible(super.getVisible());
//		setEnable(super.getEnable());
 		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		return v;
	}
	
	@Override
	public String getText() {
		if (web != null) {
			StringBuffer sbuBuffer = new StringBuffer("");			
			if (!((String)web.getTag()).equalsIgnoreCase("")) {
				sbuBuffer.append((String)web.getTag());
			}			
			return sbuBuffer.toString();
		}
		return super.getText();
	}
	
	@Override
	public void setText(String text) {
		if (web != null) {
			web.loadUrl(text);
		}
		super.setText(text);
	}
	
	@Override
	public void setLabel(String label) {
		if (txt != null) {
			txt.setText(label);
		}
		super.setLabel(label);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	
	@Override
	public void setEnable(boolean enable) {
		if (web != null) {
			web.setEnabled(enable);
		}
		super.setEnable(enable);
	}

}
