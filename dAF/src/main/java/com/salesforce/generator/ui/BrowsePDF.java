package com.salesforce.generator.ui;

import java.io.File;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daf.activity.SettingActivity;
import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.component.IWillDelete;
import com.salesforce.component.IWillSave;
import com.salesforce.component.IWillSend;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class BrowsePDF extends Component implements IWillSend, IWillSave, IWillDelete{

	private View mainView;
	private ImageView img;
	private Button btnBrowse;
	private TextView txt;	
	
	public BrowsePDF(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}
	
	@Override
	public View onCreate(final Form form) {
		// TODO Auto-generated method stub
		mainView = Utility.getInflater(form.getActivity(), R.layout.sign_ui);
		
		img = (ImageView)mainView.findViewById(R.id.picture);
		img.setImageResource(R.drawable.browse_doc);
		
		btnBrowse = (Button)mainView.findViewById(R.id.btnCreate);
		txt = (TextView)mainView.findViewById(R.id.textView1);	
		
		btnBrowse.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				form.openPDFGallery(BrowsePDF.this.hashCode(), new Form.ActivityResultListener() {
					
					@Override
					public void onActivityResult(int requestCode, int resultCode, Intent data) {
						// TODO Auto-generated method stub
						if (requestCode == BrowsePDF.this.hashCode() && resultCode == Activity.RESULT_OK) {
							Uri selectedData = data.getData(); 
							setText(getName());
							try {
								Utility.copyFile( getForm().getActivity().getContentResolver().openInputStream(selectedData), Utility.getDefaultTempPath(getText()) );
								setImage();
							} catch (Exception e) {
								// TODO: handle exception
							}
							runRoute();
						}
					}
				});
			}
		});
		
		setImage();
		
		setLabel(super.getLabel());
		if (super.getLabel().equalsIgnoreCase("")) {
			txt.setVisibility(View.GONE);
		}	
		
		setVisible(super.getVisible());
		setEnable(super.getEnable());
		
		return mainView;
	}
	
	private void setImage(){
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			img.setImageResource(R.drawable.pdf_icon);
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			img.setImageResource(R.drawable.browse_doc);
		}	
	}
	
	public void setText(String text) {
		super.setText(text);
		if (img!=null) {
			setImage();
		}
	}
 
	public void setLabel(String text) {
		if (txt!=null) {
			txt.setText(text);
		}
		super.setLabel(text);
	}
	@Override
	public void setVisible(boolean visible) {
		if (mainView!=null) {
			mainView.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	@Override
	public void setEnable(boolean enable) {
		if (btnBrowse!=null) {
			btnBrowse.setEnabled(enable);
		}
		if (img!=null) {
			img.setEnabled(enable);
		}
		super.setEnable(enable);
	}

	@Override
	public void onWillDelete() {
		// TODO Auto-generated method stub
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			Utility.deleteFileAll(Utility.getDefaultTempPath(getText()));
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			Utility.deleteFileAll(Utility.getDefaultPath(getText()));
		}	
	}

	@Override
	public void onWillSave() {
		// TODO Auto-generated method stub
		try {
			if (getText().trim().equals("")) {
			}else if (new File(Utility.getDefaultTempPath(getText())).exists()) {
				String fname = System.currentTimeMillis() + "" + new Random(hashCode()).nextLong() ;
				Utility.copyFile( Utility.getDefaultTempPath(getText()), Utility.getDefaultPath(fname) );
				setText(fname);
			}			 
		 } catch (Exception e) { }
	}

	@Override
	public boolean onWillSend() {
		// TODO Auto-generated method stub
		return sendPDF(Generator.currModelActivityID, this.getName(), this.getText(), false);
	}
	
	public static  boolean sendPDF(String ActivityId, String compId,  String fsave, boolean service){
		if (fsave.trim().equals("")) {
		}else if (new File(Utility.getDefaultPath(fsave)).exists()) {
			 String path = Utility.getDefaultPath(fsave);
			 String fname = ActivityId+"."+compId+"."+fsave+".png";
			 String str = "";
			 str = Utility.getHttpConnection( Utility.getURLenc( SettingActivity.URL_SERVER + "imagecheckservlet/?"+ (service?"f=service&":"")+"imagename=",fname));
			 
			 if (service && str.endsWith("SERVICEOFF")) {
				//20/08/2014
				try {
					Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
				} catch (Exception e) { }
			 }
			 if (!str.contains("FOUND")) {
				 str=Utility.postHttpConnection(SettingActivity.URL_SERVER + "uploadservlet/?"+ (service?"f=service":"") , null, fname, path);
				 
				 if (service && str.endsWith("SERVICEOFF")) {
						//20/08/2014
						try {
							Utility.setSetting(Utility.getAppContext(), "SERVICE", "");
						} catch (Exception e) { }
				 }
				return str.contains("OK");
			 }else{
				return true;
			 }
		}	
		return true;
	}

}
