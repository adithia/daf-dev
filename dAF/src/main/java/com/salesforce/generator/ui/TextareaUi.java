package com.salesforce.generator.ui;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Utility;

public class TextareaUi extends TextboxUi {

	public TextareaUi(Form form, String id, SingleRecordset rst) {
		super(form, id, rst);

	}

	@Override
	public View onCreate(Form form) {
		setView(Utility.getInflater(form.getActivity(), R.layout.textarea));
		txt = (EditText) getView().findViewById(R.id.fldArea);
		lbl = (TextView) getView().findViewById(R.id.lblTextArea);
		
		if (super.getLabel().equalsIgnoreCase("")) {
			lbl.setVisibility(View.GONE);
		}

		afterCreate(form);

		return getView();
	}

}
