package com.salesforce.generator.ui;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class TimeUi extends Component{
	
	private View v;
	private EditText edtDate;
	int hour, minute, second;
	static final int STARTDATE = 1;
	private TextView txtLabel;
	private Context contex;
	private ImageButton imgTime;

	public TimeUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}
	
	@Override
	public View onCreate(Form form) {
		
		v = Utility.getInflater(form.getActivity(), R.layout.date);
		contex = form.getActivity();
		
		edtDate = (EditText)v.findViewById(R.id.edtDate);
		txtLabel = (TextView)v.findViewById(R.id.textView1);
		imgTime = (ImageButton)v.findViewById(R.id.btnDate);
		
		imgTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				onDialog(TYPE_TEXTBOX).show();
			}
		});
		
		if (super.getLabel().equalsIgnoreCase("")) {
			txtLabel.setVisibility(View.GONE);
		}
		
		setLabel(super.getLabel());
 		setVisible(super.getVisible());
//		setEnable(super.getEnable());
		setText(super.getText());
		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		return v;
	}
	
	@Override
	public String getText() {
		if (edtDate != null) {
			StringBuffer sbuBuffer = new StringBuffer("");			
			if (!edtDate.getText().toString().equalsIgnoreCase("")) {
				sbuBuffer.append(edtDate.getText().toString());
			}
			
			return sbuBuffer.toString();
		}
		return super.getText();
	}
	
	@Override
	public void setText(String text) {
		if (edtDate != null) {
			edtDate.setText(text);
		}
		
		super.setText(text);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	
	@Override
	public void setEnable(boolean enable) {
		if (imgTime != null) {
			imgTime.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	
	public void setLabel(String label) {
		if (txtLabel != null) {
			txtLabel.setText(label);
		}
		super.setLabel(label);
	};
	
	private TimePickerDialog.OnTimeSetListener mStartTime = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour) {
			hour = hourOfDay;
			minute = minuteOfHour;
			String stime = LPad("" + hour, "0", 2) + ":" + LPad("" + minute, "0", 2);
			edtDate.setText(stime);
		}
	};
	
	private Dialog onDialog(int id){
		switch (id) {
		case STARTDATE:
			return new TimePickerDialog(contex, mStartTime, hour, minute, true);
		}
		return null;
	}
	
	private static String LPad(String schar, String spad, int len) {
		String sret = schar;
		for (int i = sret.length(); i < len; i++) {
			sret = spad + sret;
		}
		return new String(sret);
	}

}
