package com.salesforce.generator.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

import com.daf.activity.PreviewPDFActivity;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.stream.NfDataPDF;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class PreviewPDF extends Component{
	
	private Button button;
	private View view;
	private AssetManager am;

	public PreviewPDF(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
		// TODO Auto-generated constructor stub
	}
	
	public void afterComponentDone() {


	}
	public View onCreate(final Form form) {
		view = Utility.getInflater(form.getActivity(), R.layout.managerbutton);		
		
		am = form.getActivity().getAssets();
		
		final float scale = form.getActivity().getResources().getDisplayMetrics().density;
		ContextThemeWrapper newContext = new ContextThemeWrapper(form.getActivity(), R.style.RedButton);
		
		button = new Button(newContext, null, R.style.RedButton);
		button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		button.setPadding(button.getPaddingLeft(), button.getPaddingTop() + (int)(18.0f * scale + 0.5f), button.getPaddingRight(), button.getPaddingBottom() + (int)(18.0f * scale + 0.5f));
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { 
				
				final String data = Connection.DBquery("SELECT DATA FROM PDF_VIEW WHERE PDF='"+getList()+"';").getText(0, 0);
				final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/view.pdf";
				
				Messagebox.showProsesBar(getForm().getActivity(), new Runnable() {
 					public void run() { 						
 						create(path, data);
					}}, new Runnable() {
	 					public void run() {
	 						Intent intent = new Intent(PreviewPDF.this.getForm().getActivity(), PreviewPDFActivity.class);
	 						intent.putExtra("filepdf", path);
	 						Generator.curractivity.startActivityForResult(intent, 0);
						}
				} );	
				
				
			}
		});
		
		if (currRecordset.getText("input_type").equals("left")) {
			((LinearLayout)view.findViewById(R.id.mngButton1)).addView(button, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}else if (currRecordset.getText("input_type").equals("right")) {
			((LinearLayout)view.findViewById(R.id.mngButton3)).addView(button, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}else {
			((LinearLayout)view.findViewById(R.id.mngButton1)).setVisibility(View.GONE);
			((LinearLayout)view.findViewById(R.id.mngButton3)).setVisibility(View.GONE);
			((LinearLayout)view.findViewById(R.id.mngButton2)).addView(button, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}
		
		setVisible(super.getVisible());
		setEnable(super.getEnable()); 
		setText(super.getText());
		
		return view;
	}
	@Override
	public void setText(String text) {
		if (button!=null) {
			if (text.equals("")) {
				button.setText(super.getLabel());
			}else{
				button.setText(text);
			}			
		}
		super.setText(text);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (view!=null) {
			view.setVisibility(visible?View.VISIBLE:View.GONE);
		}
		super.setVisible(visible);
	}
	@Override
	public void setEnable(boolean enable) {
		if (button!=null) {
			button.setEnabled(enable);
		}
		super.setEnable(enable);
	}
	
	private String getPathPDF(String name){
		InputStream is;
		try {
			is = am.open(name);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			
			String path = new String(buffer);
			return path;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "";
	}
	
	private void create(String path,  String data){		 
		try {			
			NfDataPDF dataPDF = new NfDataPDF(data);
			Vector<String> pdf = dataPDF.getData("pdf");
			 
			String bg=pdf.elementAt(0);
			if (bg.contains("=")) {
				bg=bg.substring(bg.indexOf("=")+1);
			}	
			
	        InputStream is = null;	 
	        if (new File(Utility.getDefaultPath()+bg).exists()) {// if (bg.startsWith("/")) {
	        	//bg jika storage
	        	is = new FileInputStream(bg);
			}else{
				//bg jika aset
				AssetManager am = getForm().getActivity().getAssets();
				is = am.open(bg);
			}	        
	        PdfReader pdfReader = new PdfReader(is);	        

	        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(path));
	        
	        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,  BaseFont.WINANSI, BaseFont.EMBEDDED);
	        
	        Vector<String> headers = dataPDF.getHeaders();
	        Hashtable<String, Component> formdata =populate();
	        for (int i = 0; i < headers.size()-1; i++) {
	        	PdfContentByte content = pdfStamper.getOverContent(i+1);
	        	
	        	Vector<String> datapdf = dataPDF.getData("page"+(i+1));
	        	for (int j = 0; j < datapdf.size(); j++) {
	        		setdata(formdata, content, bf, datapdf.elementAt(j));	        		
				}	        	
	        	
			}
	        
	        pdfStamper.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Hashtable<String, Component>  populate(){
		Hashtable<String, Component> component = new  Hashtable<String, Component>();
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
				if (Generator.forms.elementAt(i).components.elementAt(c).getFname().startsWith(":")) {
					component.put(Generator.forms.elementAt(i).components.elementAt(c).getFname(), Generator.forms.elementAt(i).components.elementAt(c));
					
				}
			}
			
		}
		return component;
	}
	private String getValue(Hashtable<String, Component> formdata , String hint){
		if (hint.startsWith(":")) {
			Component component =  formdata.get(hint);
			if (component!=null) {
				return component.getText();
			}
			return "";
		}else{
			return hint;
		}
	}
	private void setdata(Hashtable<String, Component> formdata , PdfContentByte content,BaseFont bf, String data){
		if (data.startsWith("{") && data.endsWith("}")) {
			Nset n =Nset.readJSON(data);
			if (n.getData("equal").toString().length()>=1) {
				if (getValue(formdata, n.getData("if").toString()).equals(getValue(formdata, n.getData("equal").toString()))) {
					data=n.getData("true").toString();
				}else{
					data=n.getData("false").toString();
				}
			}else if (n.getData("contain").toString().length()>=1) {
				if (getValue(formdata, n.getData("if").toString()).contains(getValue(formdata, n.getData("contain").toString()))) {
					data=n.getData("true").toString();
				}	else{
					data=n.getData("false").toString();
				}
			}			
		}else{}
		
		String[] v = Utility.split(data+",,,", ",");//text,x,y,fontz
		int f = Utility.getInt(v[3]);
		content.beginText();
        content.setFontAndSize(bf, f<=6?6:f);
        content.showTextAligned(PdfContentByte.ALIGN_LEFT, getValue(formdata, v[0]) ,Utility.getInt(v[1]), Utility.getInt(v[2]),0);
        content.endText();
	}


}
