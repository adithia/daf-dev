package com.salesforce.generator.ui;

import java.util.Vector;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.component.IDFormat;
import com.salesforce.database.QueryClass;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.utility.Utility;

public class TextboxUi extends Component {
	private IDFormat display;
	private long lastChanged = System.currentTimeMillis();
	
	public TextboxUi(Form form, String id, SingleRecordset rst) {
		super(form, id, rst);
	}

	protected EditText txt;
	protected TextView lbl;
	protected StringBuffer sb = new StringBuffer();
	protected View view;
	
	public static String getHInts(String str){
		if (str.contains(":")  ) {
			if (str.contains("|")) {
				return str.substring(str.indexOf("|"+1));
			}
			return "";
		}
		return str;
	};


	public View onCreate(Form form) {
		view = Utility.getInflater(form.getActivity(), R.layout.textbox);
		txt = (EditText) view.findViewById(R.id.fldTextBox);
		lbl = (TextView) view.findViewById(R.id.lblText);
		
		sb.append(Global.getText("show.symbol.currency"));
		
		if (super.getLabel().equalsIgnoreCase("")) {
			lbl.setVisibility(View.GONE);
		}

//		String lbl = getLabel();
//		String ds = getText();
//		
//		String text = super.getText();
//		String lght = currRecordset.getText("text");
		
		afterCreate(form);
		return view;
	}

	protected void afterCreate(final Form form) {
		
		txt.setHint(getHInts(currRecordset.getText("hints")));		
		
		if (currRecordset.getText("input_type").equals("decimal")) {
			txt.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		} else if (currRecordset.getText("input_type").equals("phone")) {
			txt.setInputType(InputType.TYPE_CLASS_PHONE|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		} else if (currRecordset.getText("input_type").equals("password")) {
			txt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		} else if (currRecordset.getText("input_type").equals("ucase")) {
			txt.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		} else if (currRecordset.getText("input_type").equals("email")) {
			mandatory = mandatory + "@";
			txt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		} else if (currRecordset.getText("input_type").equals("number")) {
			txt.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		} else {
		}
		 
		
		setVisible(super.getVisible());		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
 		

		
		display = Generator.onGeneratorNewDFormat(this, currRecordset.getText("df_type"));
		if (display!=null) {
			txt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					Vector<InputFilter> filt = new Vector<InputFilter>();
					if (!hasFocus || !getEnable()) {
						String s = txt.getText().toString().trim();
						if (Utility.getInt(currRecordset.getText("max_length"))>=1) {
							filt.add(new InputFilter.LengthFilter(Utility.getInt(currRecordset.getText("max_length"))+Utility.getInt(currRecordset.getText("max_length"))));
						}
						if (filt.size() > 0) {
							txt.setFilters(filt.toArray(new InputFilter[filt.size()]));
						}
						txt.setText(display.getFormatText(s));
					} else {//onfocus
						if (Utility.getInt(currRecordset.getText("max_length"))>=1) {
							filt.add(new InputFilter.LengthFilter(Utility.getInt(currRecordset.getText("max_length"))));
						}
						String s = getText();//txt.getText().toString().trim();
						if (filt.size() > 0) {
							txt.setFilters(filt.toArray(new InputFilter[filt.size()]));
						}
//						setText(display.getFormatText(s));
						txt.setText(display.getText(s));
					}
					
				}
			});
		}
		
		txt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				txt.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (Math.abs(System.currentTimeMillis() - lastChanged) > 1000) {
							if (currRecordset.getText("df_type").trim().equals("currency") && !getText().equalsIgnoreCase("")) {
								
							}
						}
					}
				}, 1000);
			}
		});

		if (Generator.isNewEntry) {
			if (super.getLabel().equalsIgnoreCase("bu")) {
				setText(Generator.bu);
			}else if (super.getLabel().equalsIgnoreCase("coyid")) {
				setText(Generator.coyId);
			}else if (super.getLabel().equalsIgnoreCase("platform")) {
				setText(Generator.platform);
			}else if (super.getLabel().equalsIgnoreCase("coyname")) {
				setText(QueryClass.getCoyName(Generator.coyId));
			}else if (super.getLabel().equalsIgnoreCase("custtype")) {
				setText(Generator.custtype);
			}else if (super.getLabel().equalsIgnoreCase("userid")) {
				setText(Utility.getSetting(form.getActivity(), "USERNAME", ""));
			}else if (super.getLabel().equalsIgnoreCase("username")) {
				setText(Utility.getSetting(form.getActivity(), "USER_NAME", ""));
			}else if (super.getLabel().equalsIgnoreCase("ttdcs")) {
				setText(Utility.ttdPath);
			}else if (super.getLabel().equalsIgnoreCase("branchid")) {
				setText(Utility.OFFICE_CODE);
			}else if (super.getLabel().equalsIgnoreCase("jmlbrg")) {
				setText(Global.getText("parameter.object.mpf", "0"));
			}else if (super.getLabel().equalsIgnoreCase("biayasurat")) {
				setText(Global.getText("biaya.surat.penyimpanan", "0"));
			}else {
				setText(super.getText());
			}
		}else {
			if (super.getText().equalsIgnoreCase("")) {
				setText(currRecordset.getText("text"));
			}else {
				setText(super.getText());
			}
			
		}

		if (getLabel().contains("@")) {
			String label = getLabel().substring(0, getLabel().indexOf("@"));
			setLabel(label);
		}else
			setLabel(super.getLabel());
		
		
		if (currRecordset.getText("df_type").trim().equals("currency") && sb.toString().trim().length()>=1) {
			sb.append(" " + txt.getText().toString());
			txt.setText(sb.toString());
		}
		
		Vector<InputFilter> filt = new Vector<InputFilter>();
		
		if (currRecordset.getText("df_type").trim().equals("currency") && !getText().equalsIgnoreCase("")) {
			if (Utility.getInt(currRecordset.getText("max_length")) == getText().length()) {
				filt.add(new InputFilter.LengthFilter(Utility.getInt(currRecordset.getText("max_length"))+Utility.getInt(currRecordset.getText("max_length"))));
			}else{
				filt.add(new InputFilter.LengthFilter(Utility.getInt(currRecordset.getText("max_length"))));
			}
		}else if (Utility.getInt(currRecordset.getText("max_length"))>=1) {
			filt.add(new InputFilter.LengthFilter(Utility.getInt(currRecordset.getText("max_length"))));
		}
		
		if (currRecordset.getText("df_type").equals("doublespace") && sb.toString().trim().length()>=1) {
			filt.add(Utility.doubleSpaceNSpecialCharFilter);
		}
		
		if (currRecordset.getText("df_type").equals("specialchar")) {
			filt.add(Utility.specialCharFilter);
		}
		
		if (currRecordset.getText("df_type").equals("specialcharwithenter")) {
			filt.add(Utility.specialCharFilterWithEnter);
		}
		
		if (currRecordset.getText("df_type").equals("charwithnonumber")) {
			filt.add(Utility.charNoNumber);
		}
		
		if (currRecordset.getText("df_type").equals("firstzero")) {
			filt.add(Utility.firstZero);
		}
		
		if (filt.size() > 0) {
			txt.setFilters(filt.toArray(new InputFilter[filt.size()]));
		}
		
//		if (currRecordset.getText("df_type").trim().equals("currency") && !getText().equalsIgnoreCase("")) {
//			txt.setText(display.getFormatText(getText()));
//		}
	}

	public String getText() {
		if (currRecordset.getText("input_type").equals("nocase")) {
			return getTextfirst();
		}else if (currRecordset.getText("input_type").equals("email")) {
			return getTextfirst();
		}
		return getTextfirst().toUpperCase();
	}
	public String getTextfirst() {
		if (txt != null) {
			if (!txt.isFocused() && display!=null) {
				String s = txt.getText().toString().trim();
				return display.getText(s);
			}else if (txt.isFocused() && display!=null) {//onfocus
				String s = txt.getText().toString().trim();
				return display.getText(s);
			}else{//onfocus
				return txt.getText().toString().trim();
			}
			 
		}
		return super.getText().trim();
	}

	@Override
	public void setText(String s) {
		super.setText(s);
		if (txt != null) {
			if (!txt.isFocused() && display!=null) {
				Vector<InputFilter> filt = new Vector<InputFilter>();
				if (currRecordset.getText("df_type").trim().equals("currency") && !getText().equalsIgnoreCase("")) {
					if (Utility.getInt(currRecordset.getText("max_length"))>=1) {
						filt.add(new InputFilter.LengthFilter(Utility.getInt(currRecordset.getText("max_length"))+Utility.getInt(currRecordset.getText("max_length"))));
					}
					if (filt.size() > 0) {
						txt.setFilters(filt.toArray(new InputFilter[filt.size()]));
					}
					txt.setText(display.getFormatText(s));
				}else{
					txt.setText(s);
				}
				
			}else{//onfocus
				txt.setText(s); 
			}
		}
	}

	@Override
	public void setLabel(String label) {
		if (lbl != null) {
			lbl.setText(label);
		}
		super.setLabel(label);
	}

	@Override
	public void setEnable(boolean enable) {
		if (txt != null) {
			txt.setEnabled(enable);
		}
		super.setEnable(enable);
	}

	@Override
	public void setVisible(boolean visible) {
		if (view != null) {
			view.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		super.setVisible(visible);
	}
	
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		if (txt != null) {
			txt.setFocusable(true);
			txt.requestFocus();
		}
		super.setFocus();
	}
	
	@Override
	public void nonMandatory() {
		// TODO Auto-generated method stub
		if (mandatory.contains("*")) {
			mandatory = mandatory.replace("*", "");
		}
		super.nonMandatory();
	}

}
