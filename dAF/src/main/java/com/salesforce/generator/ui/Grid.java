package com.salesforce.generator.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterInterface;
import com.salesforce.component.Component;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class Grid extends Component{

	private View v;
	private TextView lblText;
	private List<String[]> listData = new ArrayList<String[]>();
	private LinearLayout lay;
	
	public Grid(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}
	
	@Override
	public View onCreate(Form form) {
		// TODO Auto-generated method stub
		v = Utility.getInflater(form.getActivity(), R.layout.tabelui);
		lblText = (TextView) v.findViewById(R.id.lblTable);
		lay = (LinearLayout)v.findViewById(R.id.manager);
		
		loaddata(form);
		
		if (super.getLabel().equalsIgnoreCase("")) {
			lblText.setVisibility(View.GONE);
		}

		setLabel(super.getLabel());
		if (super.getLabel().equalsIgnoreCase("")) {
			lblText.setVisibility(View.GONE);
		}	
		
		setVisible(super.getVisible());
//		setEnable(super.getEnable());

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		return v;
	}
	
	private void setupHader(Form form){
		View header = AdapterConnector.setInflateLinier(form.getActivity(), R.layout.field);
		lay.addView(header, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	}
	
	private void loaddata(Form form){
		listData.clear();
		lay.removeAllViews();
		setupHader(form);
		String query = "";
		Vector<String> queries = new Vector<String>();
		if (getList().startsWith("[")) {
			Nset ns =Nset.readJSON(getList());
			for (int i = 0; i < ns.getArraySize(); i++) {
				if (i==0) {
					queries = Utility.splitVector(ns.getData(i).toString(), "?");
				}else {					
					if (i == 1) {
						query = queries.elementAt(i-1).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(this, ns.getData(i).toString())) + "'";	
					}else {
						query = query  +  queries.elementAt(i-1).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(this, ns.getData(i).toString())) + "'";													
					}
				}
			}
		}
		
		Recordset data = Connection.DBquery(query);
		for (int i = 0; i < data.getRows(); i++) {
			String[] brg = new String[]{data.getText(i, "obj_brand"), data.getText(i, "obj_type"), data.getText(i, "obj_tahun"), data.getText(i, "qty")};
			listData.add(brg);
		}
		
		setupTable(form);
	}
	
	private void setupTable(Form form){
		if (listData.size() > 0) {
			AdapterConnector.setInflateLinierDirect(form.getActivity(), listData, lay, R.layout.tabel_inflate, new AdapterInterface() {
				
				@Override
				public View onMappingColumn(int row, View v, Object data) {
					// TODO Auto-generated method stub
					String[] value = listData.get(row);
					Utility.changeColorRowTable(row, (LinearLayout)v.findViewById(R.id.mngRow));
					if (value != null && value.length > 0) {
						((TextView)v.findViewById(R.id.txtNo)).setText(row+1+"");
						((TextView)v.findViewById(R.id.txtMerk)).setText(value[0]);
						((TextView)v.findViewById(R.id.txtType)).setText(value[1]);
						((TextView)v.findViewById(R.id.txtTahun)).setText(value[2]);
						((TextView)v.findViewById(R.id.txtQty)).setText(value[3]);
					}
					return v;
				}
			});
		}
	}
	
	@Override
	public void setText(String text) {
		// TODO Auto-generated method stub
		super.setText(text);
		if (lay!=null) {
			loaddata(getForm());
		}
	}
	
	@Override
	public void setTag(Object tag) {
		// TODO Auto-generated method stub
		if (v.getTag() != null) {
			v.setTag(tag);
		}
		super.setTag(tag);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (v != null) {
			v.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		super.setVisible(visible);
	}

	@Override
	public void setEnable(boolean enable) {
		if (v != null) {
			v.setEnabled(enable);
		}
		super.setEnable(enable);
	}

	@Override
	public void setLabel(String label) {
		if (lblText != null) {
			lblText.setText(label);
		}
		super.setLabel(label);
	}

}
