package com.salesforce.generator.ui;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Utility;

public class AddItemUi extends Component {
	
	private View view;
	private Context context;
	private TextView txtLabel;
	private ImageView btnAdd;

	public AddItemUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);
	}
	
	@Override
	public View onCreate(Form form) {
		// TODO Auto-generated method stub
		context = form.getActivity();
		view = Utility.getInflater(context, R.layout.tabel_ui);

		txtLabel = (TextView)view.findViewById(R.id.txtLabel);
		btnAdd = (ImageView)view.findViewById(R.id.btnAdd);
		
		return view;
	}

}
