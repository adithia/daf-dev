package com.salesforce.generator.ui;

import java.util.Vector;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;

import com.salesforce.R;
import com.salesforce.component.Component;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class FormPagesUi extends Component {

	private View v;
	private Button btnPages;
	static final int STARTDATE = 1;
	private Context context;

	public FormPagesUi(Form form, String name, SingleRecordset rst) {
		super(form, name, rst);

	}

	@Override
	public View onCreate(Form form) {
		v = Utility.getInflater(form.getActivity(), R.layout.formpages);
		context = form.getActivity();
		btnPages = (Button) v.findViewById(R.id.btn_pages);

		btnPages.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				onDialog(STARTDATE);

			}
		});
		setLabel(getLabel());
		setVisible(super.getVisible());
		

 		if (Generator.freez) {
			setEnable(super.getEnable());
		}else
			setEnable(false);
		
		return v;
	}

	@Override
	public void setEnable(boolean enable) {
		// TODO Auto-generated method stub
		if (btnPages != null)
			btnPages.setEnabled(enable);
		super.setEnable(enable);
	}

	@Override
	public void setVisible(boolean visible) {
		// TODO Auto-generated method stub
		if (btnPages != null) {
			btnPages.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		super.setVisible(visible);
	}

	private Dialog onDialog(int id) {
		switch (id) {
		case STARTDATE:
			AlertDialog dialog = null;
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
			dialogBuilder.setTitle("Go to Form");

			Vector<String> list = new Vector<String> ();
			
//			Utility.splitVector(getList(), "|");
			
			for (int i = 0; i < Generator.forms.size(); i++) {
				list.addElement(Generator.forms.elementAt(i).getTitle());
			}

			ListAdapter listAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, list);

			dialogBuilder.setAdapter(listAdapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					setText(Generator.forms.elementAt(which).getName());
					runRoute();
					dialog.dismiss();
				}
			});

			dialog = dialogBuilder.create();
			dialog.show();
		}
		return null;
	}

}
