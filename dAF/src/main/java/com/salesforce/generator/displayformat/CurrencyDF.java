package com.salesforce.generator.displayformat;

import com.salesforce.component.IDFormat;
import com.salesforce.utility.Utility;

public class CurrencyDF implements IDFormat{
	 
	@Override
	public String getFormatText(String text) {
		String s = getText(text);
		
		if (!text.equalsIgnoreCase("") && text.length() > 1) {
			if (String.valueOf(text.charAt(0)).equalsIgnoreCase("0")) {
				text = text.substring(1);
			}
		}
		
		if (s.indexOf(".")>=0) {
			return Utility.insertStringRev(s.substring(0, s.indexOf(".")), ",", 3)+s.substring(s.indexOf(".")); 
		}
		return Utility.insertStringRev(getText(text), ",", 3); 
	}

	@Override
	public String getText(String text) {
		return text.replace(".", ".").replace(",", "");
	}
}

