package com.salesforce.generator.displayformat;

import com.salesforce.component.IDFormat;
import com.salesforce.utility.Utility;

public class CurrencyNoPointDF implements IDFormat{
	 
	@Override
	public String getFormatText(String text) {
		String s = getText(text);
		if (s.indexOf(".")>=0) {
			return Utility.insertStringRev(s.substring(0, s.indexOf(".")), ",", 3) ; 
		}
		return Utility.insertStringRev(getText(text), ",", 3); 
	}

	@Override
	public String getText(String text) {
		String s = text;
		if (s.indexOf(".")>=0) {
			text = s.substring(0, s.indexOf(".")); 
		}
		return text.replace(".", ".").replace(",", "");
	}
}
