package com.salesforce.generator;

import java.lang.Thread.UncaughtExceptionHandler;

import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class Apps extends android.app.Application{
	public Apps(){
		//Thread.setDefaultUncaughtExceptionHandler(
         //       (UncaughtExceptionHandler) new CustomExceptionHandler( getApplicationContext() ));
	}
	
	public static void Nikita(){
    	new Apps().init();
    }
	private void init (){
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
		    @Override
		    public void uncaughtException(Thread thread, final  Throwable ex) {
		    	//Log.i("Nikita crashed", ex.getMessage());
                //Toast.makeText( getApplicationContext(), "Application crashed", Toast.LENGTH_LONG).show();
		    	
		    	new Thread() {
		            @Override
		            public void run() { 
		                Looper.prepare();
		                //Log.i("Application crashed", ex.getMessage());
		                Toast.makeText( getApplicationContext(), "Application crashed", Toast.LENGTH_LONG).show();
		                Looper.loop();
		            }
		        };

		        try{
		            Thread.sleep(1000); // Let the Toast display before app will get shutdown
		        }catch (InterruptedException e) {
		            // Ignored.
		        }
		    }
		});
		
		
//		 public void uncaughtException(Thread thread, Throwable exception) {
//
//		        Toast.makeText(myContext,
//		                "The application has crashed, and a report is sent to the admin",
//		                Toast.LENGTH_SHORT).show();
//		        StringWriter stackTrace = new StringWriter();
//		        exception.printStackTrace(new PrintWriter(stackTrace));
//		        System.err.println(stackTrace);// You can use LogCat too
//		        Intent intent = new Intent(myContext, CrashActivity.class);
//		        myContext.startActivity(intent);
//		        Process.killProcess(Process.myPid());
//		        System.exit(10);
//		    }
	}
	
	 
}
