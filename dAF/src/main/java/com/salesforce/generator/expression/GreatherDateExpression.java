package com.salesforce.generator.expression;

import com.salesforce.component.Component;
import com.salesforce.component.IExpression;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class GreatherDateExpression implements IExpression{

	@Override
	public boolean onExpression(Component comp, SingleRecordset data) {
		String param1 = comp.getForm().getFormComponentText(data.getText("param1")).trim();
		String param2 = "";
		if (param1.contains("/")) {
			param2 = comp.getForm().getFormComponentText(data.getText("param2")).trim().replace("-", "/");			
		}else if (param1.contains("-")) {
			param2 = comp.getForm().getFormComponentText(data.getText("param2")).trim().replace("/", "-");
		}
		
		long date1 = Utility.converDateToLong(param1);
		long date2 = Utility.converDateToLong(param2);
		
		try {			
			if (date1 > date2) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

}
