package com.salesforce.generator.expression;

import com.salesforce.component.Component;
import com.salesforce.component.IExpression;
import com.salesforce.database.SingleRecordset;

public class AlwaysTrueExpression implements IExpression{

	@Override
	public boolean onExpression(Component comp, SingleRecordset data) {
		return true;
	}

}
