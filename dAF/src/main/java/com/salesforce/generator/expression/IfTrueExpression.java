package com.salesforce.generator.expression;

import com.salesforce.component.Component;
import com.salesforce.component.IExpression;
import com.salesforce.database.SingleRecordset;

public class IfTrueExpression implements IExpression{

	@Override
	public boolean onExpression(Component comp, SingleRecordset data) {
		String param = data.getText("param1");
		if (param.equals("")) {
			param="@EXPRESSION";
		}
		String param1 = comp.getForm().getFormComponentText(param).trim().toLowerCase();
		if (param1.equalsIgnoreCase("") || param1 != null) {
			if (param1.equalsIgnoreCase("true")) {
				return true;
			}			
		}
		return false;
	}

}
