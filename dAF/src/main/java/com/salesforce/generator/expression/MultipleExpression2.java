package com.salesforce.generator.expression;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IExpression;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class MultipleExpression2 implements IExpression{

	@Override
	public boolean onExpression(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String param1 = data.getText("param1");//comp.getForm().getFormComponentText(data.getText("param1")).trim();@com|kawin
		Vector<String> val = Utility.splitVector(param1, "|");
		String valcomp = comp.getForm().getFormComponentText(val.elementAt(0).trim()).trim();
		String valCompare = val.elementAt(1).trim();
		
		String param2 = data.getText("param2");//comp.getForm().getFormComponentText(data.getText("param2")).trim();
		Vector<String> val2 = Utility.splitVector(param2, "|");
		String valcomp2 = comp.getForm().getFormComponentText(val2.elementAt(0).trim()).trim();
		String valCompare2 = val2.elementAt(1).trim();
		
		if (valcomp.equals(valCompare) && Long.parseLong(valcomp2) <= Long.parseLong(valCompare2)) {
			return true;
		}
		
//		try {
//			if (valcomp.equals(valCompare) && Long.parseLong(valcomp2) <= Long.parseLong(valCompare2)) {
//				return true;
//			}
//		} catch (NumberFormatException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return false;
	}

}
