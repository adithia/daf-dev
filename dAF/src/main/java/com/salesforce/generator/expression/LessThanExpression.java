package com.salesforce.generator.expression;

import com.salesforce.component.Component;
import com.salesforce.component.IExpression;
import com.salesforce.database.SingleRecordset;

public class LessThanExpression implements IExpression{

	@Override
	public boolean onExpression(Component comp, SingleRecordset data) {
		String param1 = comp.getForm().getFormComponentText(data.getText("param1")).trim();
		String param2 = comp.getForm().getFormComponentText(data.getText("param2")).trim();
		
		try {
			if (Integer.parseInt(param1) <= Integer.parseInt(param2)) {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		}
		return false;
	}

}
