package com.salesforce.generator.expression;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IExpression;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class EqualParam1Expression implements IExpression{

	@Override
	public boolean onExpression(Component comp, SingleRecordset data) {
		String param1 = data.getText("param1");
		Vector<String> val = Utility.splitVector(param1, "|");
		String valcomp = comp.getForm().getFormComponentText(val.elementAt(0).trim()).trim();
		String valCompare = val.elementAt(1).trim();
		
		if(valcomp.equalsIgnoreCase(valCompare)){
			return true;
		}
		return false;
	}

}
