package com.salesforce.generator.validation;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class ValueGreatherThanValidation implements IValidation{
	private static String defaultMessage = "Length Maximum @PARAM";
	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		if (comp.getText().length() > Utility.getInt(data.getText("param"))) {
			String s = data.getText("message").trim();
			if (s.length() >= 1) {
				return data.getText("message").replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
			}else {
				return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
			}
		}
		return null;
	}

}
