package com.salesforce.generator.validation;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class DateGreaterThanValidation implements IValidation {
	private final static String defaultMessage = "Date @LABEL Must be less than @PARAM";
	private final static String message = "Date can't be empty";
	
	@SuppressWarnings("unused")
	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		String sDateDevice = comp.getText().toString().trim();
		String sDateServer = data.getText("param").toString().trim();
		
		if (sDateDevice.equalsIgnoreCase("") || sDateDevice != null) {
			return message;
		}
		
		String dateDeviceFormat = "";
		String dateServerFormat = "";
		
		if (sDateDevice.contains("/")) {
			dateDeviceFormat = sDateDevice.replace("-", "/");			
		}else if (sDateDevice.contains("-")) {
			sDateDevice = sDateDevice.replace("/", "-");
		}
		
		if (sDateServer.contains("/")) {
			dateServerFormat = sDateServer.replace("-", "/");			
		}else if (sDateServer.contains("-")) {
			dateServerFormat =sDateServer.replace("/", "-");
		}
		
		long dateDevice = Utility.converDateToLong(dateDeviceFormat);
		long dateServer = Utility.converDateToLong(dateServerFormat);
		
		try {			
			if (dateDevice > dateServer) {
				String s = data.getText("message").toString().trim();
				if (s.length()>=1) {
					return data.getText("message").replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
				}else{
					return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
				}		
				
			} 
		} catch (Exception e) {
			
		}
		return null;
	}

}
