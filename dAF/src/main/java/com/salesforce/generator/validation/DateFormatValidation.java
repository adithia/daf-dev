package com.salesforce.generator.validation;

import java.text.SimpleDateFormat;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;

public class DateFormatValidation implements IValidation{
	
	private final static String defaultMessage = "Date Format must @PARAM";
	
	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		String s = comp.getText().toString().trim();
		String fDate = data.getText("param").toString().trim();
		boolean validFormat = isValidDateFormat(s, fDate);
		
		if(!validFormat){
			return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
		} 
		
		return null;
		
	}
	
	private boolean isValidDateFormat(String s, String fdate){
		
		try {
			SimpleDateFormat dFormat = new SimpleDateFormat(fdate);
			dFormat.setLenient(false);
			dFormat.parse(s);
		} catch (Exception e) {
			return false;
		}
		return true;
		
	}

}
