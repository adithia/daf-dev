package com.salesforce.generator.validation;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;

public class MustContainsValidation implements IValidation{
	private final static String defaultMessage = "@LABEL Have to Contains @PARAM";
	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		String s = comp.getText().toString().trim();
		if (!s.contains(data.getText("param").toString().trim())) {
			String str = data.getText("message").toString().trim();
			if (str.length() >= 1) {
				return data.getText("message").replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
			}else {
				return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
			}
		}
		return null;
	}

}
