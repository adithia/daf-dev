package com.salesforce.generator.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;

public class PasswordValidation implements IValidation{
	private Pattern pattern;
	private Matcher matcher;

	private static final String PASSWORD_PATTERN = 
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
	
	private final static String defaultMessage = "Password must contains @PARAM";

	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		String s = comp.getText().toString().trim();
		
		if(!validation(s)){
			return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
		}
		
		return null;
	}

	private boolean validation(String password){
		
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);
		return matcher.matches();
		
	}
}
