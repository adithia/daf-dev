package com.salesforce.generator.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;

public class EmailValidation implements IValidation {

	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	private final static String defaultMessage = "email must contains @PARAM";
	
	private Pattern pattern;
	private Matcher matcher;
	
	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		
		String s = comp.getText().toString().trim();
		
		if(!validate(s)){
			return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
		} 
		
		return null;
		
	}
	
	private boolean validate(final String hex){
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(hex);
		return matcher.matches();
	}

}
