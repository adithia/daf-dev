package com.salesforce.generator.validation;

import com.salesforce.component.Component;
import com.salesforce.component.IValidation;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class ValueLessThanValidation implements IValidation {
	private static String defaultMessage = "Length Minimum @PARAM";
	@Override
	public String onValidation(Component comp, SingleRecordset data) {
		if (comp.getText().length() < Utility.getInt(data.getText("param"))) {
			String s = data.getText("message").trim();
			if (s.length() >= 1) {
				return data.getText("message").replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
			}else {
				return defaultMessage.replace("@PARAM", data.getText("param")).replace("@LABEL", comp.getLabel());
			}
		}
		return null;
	}

}
