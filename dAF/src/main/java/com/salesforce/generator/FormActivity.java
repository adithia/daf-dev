package com.salesforce.generator;


import java.io.OutputStream;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.salesforce.R;
import com.salesforce.adapter.AdapterConnector;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.utility.Utility;
import com.sdk.bluetooth.android.BluetoothPrinter;
import com.sdk.bluetooth.android.BluetoothPrinter.PrinterType;
import com.zj.btsdk.BluetoothService;

public class FormActivity extends Activity{
	private AlertDialog alertDialog = null;
	private Form owner = null;
	private LinearLayout lnrContent;
	private Vector<View> view = new Vector<View>();
	private boolean asynbusy=false;
	private ProgressDialog mProgressDialog;	
	private String dataPrint="";
	private byte[] dataPrintb=null;
	public BluetoothService mService = null;
	public BluetoothSocket mSocket;
	public OutputStream mSocketOut =null;
 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.formactivity);	
		
		lnrContent=(LinearLayout)findViewById(R.id.frmGenerator);		
		newBT();
		
		if (asynbusy) {
			showAsyncTaskBusyBar();
		}
		
		String gen = getIntent().getStringExtra("NIKITAFORMGENERATOR") ;
		
		//menampilkan semua komponen berdasarkan form
		if (gen!=null) {
			for (int i = 0; i < Generator.forms.size(); i++) {
				if (Generator.forms.elementAt(i).getGeneratorCode().equals(gen)) {
					owner=Generator.forms.elementAt(i);
					owner.setActivity(this);
					if (owner.orientation!=0) {
						setRequestedOrientation(owner.orientation);
					}
					owner.onCreate();
					Generator.setCurrentForm(owner);
					Generator.setCurrentActivity(this);
					return;
				}
			}
			finish();
		}
		
	}
		
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		
		AlertDialog dialog = null;
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		
		switch (id) {
		case 0:
			View view = AdapterConnector.setInflateLinier(getApplicationContext(), R.layout.password_dialog);
			dialogBuilder.setView(view);
			dialog = dialogBuilder.create();
			break;

		default:
			break;
		}
		return dialog;
	}
	
	@Override
	protected void onPrepareDialog(int id, final Dialog dialog) {
		// TODO Auto-generated method stub
		final EditText edtPass = (EditText)dialog.findViewById(R.id.edtpass);
		((Button)dialog.findViewById(R.id.btnSubmit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (edtPass.getText().toString().equals(Utility.getSetting(getApplicationContext(), "PASSWORD", ""))) {
					dialog.dismiss();
					String gen = getIntent().getStringExtra("NIKITAFORMGENERATOR") ;
					if (gen!=null) {
						for (int i = 0; i < Generator.forms.size(); i++) {
							if (Generator.forms.elementAt(i).getGeneratorCode().equals(gen)) {
								if (owner!=null) {
									owner.onActivityResult(55555, 0, null);
								}
								return;
							}
						}
						if (owner!=null) {
							owner.saveComponent();
						}
						finish();
					}	
					finish();
				}else {
					AdapterDialog.showMessageDialog(FormActivity.this, "Informasi", "Password anda salah");
				}
			}
		});
	}
	
	@Override
	protected void onStart() {
		super.onStart();
//		if (alertDialog!=null) {			
//			alertDialog.show();//alertDialog.isShowing()
//		}	
		
		view.removeAllElements();
		getAllView(view, getWindow().getDecorView());
		
//		for (int l = 0; l < view.size(); l++) {
//			//Log.i("A","AA:"+view.elementAt(l).getId());
//			//Log.i("A","AB:"+view.elementAt(l).getTag());
//			//Log.i("A","AC:"+view.elementAt(l).getContentDescription());
//		}
		
		if (owner!=null) {
			owner.onStart();
		}
	}
	 
	public View findViewById(int id) {
		return super.findViewById(id);
	}
 
	@Override
	public void finish() {
		super.finish();
	}
	
	public Vector<View>  getViewAll(){
		return view;
	}
	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		super.startActivityForResult(intent, requestCode);
	}
	public void startActivityForResultThanFinish(Intent intent, int requestCode) {
		super.startActivityForResult(intent, requestCode);
		finish();
	}
	@Override
	public void startActivityFromChild(Activity child, Intent intent, int requestCode) {
		 
		super.startActivityFromChild(child, intent, requestCode);
	}
	@Override
	public void startActivity(Intent intent) {
		 
		super.startActivity(intent);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String gen = getIntent().getStringExtra("NIKITAFORMGENERATOR") ;
		if (gen!=null) {
			    //Log.i("Sales Force", gen);
				for (int i = 0; i < Generator.forms.size(); i++) {
					if (Generator.forms.elementAt(i).getGeneratorCode().equals(gen)) {
						if (owner!=null) {
							owner.onActivityResult(requestCode, resultCode, data);
						}
						super.onActivityResult(requestCode, resultCode, data);
						return;
					}
				}
				if (owner!=null) {
					owner.saveComponent();
				}				
				finish();
		}		
	}
	@Override
	protected void onDestroy() {
		closBT();
		if (owner!=null) {
			//Log.i("Sales Force", "onDestroy:"+owner.getName());
			owner.setActivity(null);
		} else{
			//Log.i("Sales Force", "onDestroy");
		}
		if (mPrinter!=null) {
			try {
				mPrinter.close();
			} catch (Exception e) { }
		}
		super.onDestroy();
	}
	public void runOnAsyncTask(Runnable run, Runnable uiPost) {
		new AsyncTask<Runnable, Void, Runnable> (){
		      protected Runnable doInBackground(Runnable... params) {
		    	  params[0].run();
		    	  return params[1];
		      }      
		      protected void onPostExecute(Runnable result) {    
		    	  result.run();
		      }		      
		      protected void onCancelled(Runnable result) {
		    	  result.run();
			  }
		      protected void onPreExecute() {}
		      protected void onProgressUpdate(Void... values) {}
		}.execute(run, uiPost);
	}
	
	private void showAsyncTaskBusyBar(){
		mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please Wait");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
	}
	
	public void runOnAsyncTaskBusy(Runnable run, final Runnable uiPost) {
		if (!asynbusy) {
			asynbusy=true;
			showAsyncTaskBusyBar();
			runOnAsyncTask(run, new Runnable() {
				@Override
				public void run() {
					if (uiPost!=null) {
						uiPost.run();
					}	
					try {
						mProgressDialog.dismiss();
					} catch (Exception e) { }					
					asynbusy=false;
				}
			});
		}		
	}
	
	public void showMessage(String message,final String PositiveButton,final String NegativeButton, final String CenterButton, boolean cancel, final int reqcode) {
		hideMessage();
		Builder dlg = new AlertDialog.Builder(this);
		dlg.setTitle("");
		if (NegativeButton!=null) {
			if (!NegativeButton.equals("")) {
				dlg.setNegativeButton(NegativeButton, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (owner!=null) {
							owner.onButtonMessageClicked(NegativeButton, reqcode);
						}
					}
				});
			}
		} 
		if (PositiveButton!=null) {
			if (!PositiveButton.equals("")) {
				dlg.setPositiveButton(PositiveButton, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (owner!=null) {
							owner.onButtonMessageClicked(PositiveButton, reqcode);
						}
					}
				});
			}
		} 
		if (CenterButton!=null) {
			if (!CenterButton.equals("")) {
				dlg.setNeutralButton(CenterButton, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (owner!=null) {
							owner.onButtonMessageClicked(CenterButton, reqcode);
						}
					}
				});
			}
		} 
		dlg.setCancelable(cancel);
		dlg.setMessage(message);
		alertDialog=dlg.create();
		alertDialog.show();
	}
	
	public void showDialogMessage(String message){
		AdapterDialog.showMessageDialog(FormActivity.this, "Information", message);
	}
	
	public void showMessage(String message, boolean cancel) {
		showMessage(message, null, null, null, cancel, 0);
	}	
	public void hideMessage(){
		if (alertDialog!=null) {
			alertDialog.dismiss();
		}
	}
	public void showInfo(String info) {
		showInfo("Info", info);
	}	
	public void showInfo(String title, String info) {
		hideMessage();
		Builder dlg = new AlertDialog.Builder(this);
		dlg.setTitle(title);
		dlg.setMessage(info);
		dlg.setCancelable(true);
		alertDialog=dlg.create();
		alertDialog.show();
	}
	@Override
	public void addContentView(View view, LayoutParams params) {
		lnrContent.addView(view, params);
	}
	@Override
	public void onBackPressed() {
		if (owner!=null) {
			owner.onBackPressed();
			if (owner.flagback) {
				owner.close();
				//Log.i("Sales Force", "onBackPressed1");
				super.onBackPressed();
			}else{
				//tLog.i("Sales Force", "onBackPressed2");
				//super.onBackPressed();//?
			}
		}else{
			//Log.i("Sales Force", "onBackPressed3");
			super.onBackPressed();
		}		
	}
	public void close(){
		super.onBackPressed();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (owner!=null) {
			owner.saveComponent();
		}
		super.onSaveInstanceState(outState);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void getAllView(Vector view, View child){
		if (child.getContentDescription()!=null||child.getTag()!=null) {
			view.addElement(child);
		}			
		if (child instanceof ViewGroup ) {
			 for (int j = 0; j < ((ViewGroup)child).getChildCount(); j++) {
				 getAllView(view, ((ViewGroup)child).getChildAt(j));
			 }
		}
	}
	
	//bluetooth
	public void newBT( ){
		mService = new BluetoothService( this,  getHandler()){
			@Override
			public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
				mSocket=socket;				
				super.connected(socket, device);
			}
		} ;
	}
	
	public void closBT( ){
		try {
			if (mSocket!=null) {
				mSocket.close();
			}			
		} catch (Exception e) { }
		try {
			if (mSocketOut!=null) {
				//mSocketOut.close();
			}			
		} catch (Exception e) { }
	}
	
	public void printBT(String mac, String data ){
		dataPrint=data+"\n\n\n";
		try {
			if (mService.getState()==BluetoothService.STATE_CONNECTED) {
				if (!dataPrint.equals("")) {
					mService.sendMessage(dataPrint, "GBK"); 
				}
				dataPrint="";
			}else{
				mService.connect(mService.getDevByMac(mac));				
			}			
		} catch (Exception e) { }
	}
	
	public void printBT(String mac, byte[] data ){
		dataPrintb=data;
		try {
			if (mService.getState()==BluetoothService.STATE_CONNECTED) {
				if (mSocketOut==null) {
					mSocketOut = mSocket.getOutputStream();
				}				
				mSocketOut.write(dataPrintb);
				mSocketOut.flush();
				dataPrintb=null;
			}else{
				mService.connect(mService.getDevByMac(mac));				
			}			
		} catch (Exception e) { }
	}
	
	private BluetoothPrinter mPrinter;
	public void printSprt(String name, byte[] data, String text ){
		try {
			if (mPrinter!=null) {
			}else{
				mPrinter = new BluetoothPrinter(name);
			}
			int ret = 0;
			if (mPrinter.isConnected()) {
				
			}else{
				mPrinter.setCurrentPrintType(PrinterType.TIII);
				ret = mPrinter.open();
			}
			
			if (ret == 0) {
				if (data!=null) {
					mPrinter.send(data)	;
					mPrinter.setPrinter(4);
				}


				if (text!=null) {
					byte[] charLarge1 = { 0x1d, 0x21, 0 };
					mPrinter.send(charLarge1);
					
					mPrinter.send(text)	;
					mPrinter.setPrinter(4);
				}
				
				//mPrinter.close();
			}else{
				Toast.makeText(this, "Connect Failed", Toast.LENGTH_SHORT);		
			}			
		} catch (Exception e) { }
	}
	
	public Handler getHandler(){
		return mHandler;
	}
	
	public BluetoothService getBluetoothService(){
		return mService;
	}
	
	private final  Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	Intent intent = new Intent();

            switch (msg.what) {
            case BluetoothService.MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:   
                	Toast.makeText(getApplicationContext(), "Connect successful",  Toast.LENGTH_SHORT).show();
                	try {
                		new Thread(new Runnable() {
                			public void run() {
								try { 									 
									if (!dataPrint.equals("")) {
										mService.sendMessage(dataPrint, "GBK");
									}else if (dataPrintb!=null){
										mSocketOut = mSocket.getOutputStream();
										mSocketOut.write(dataPrintb);
										mSocketOut.flush();	
									}
									dataPrintb=null;
									dataPrint="";
								} catch (Exception e) { }
							}
						}).start();
					} catch (Exception e) { }
                	onActivityResult(12345,  12345, intent );
                    break;
                case BluetoothService.STATE_CONNECTING:  
                	
                    break;
                case BluetoothService.STATE_LISTEN:
                	
                case BluetoothService.STATE_NONE:
                	
                    break;
                }         
                break;
            case BluetoothService.MESSAGE_CONNECTION_LOST:
            	//Toast.makeText(getApplicationContext(), "Device connection was lost", Toast.LENGTH_SHORT).show();
            	//onActivityResult(12345,  00001, intent );
                break;
            case BluetoothService.MESSAGE_UNABLE_CONNECT:   
            	//Toast.makeText(getApplicationContext(), "Unable to connect device", Toast.LENGTH_SHORT).show();
            	//onActivityResult(12345,  00002, intent );
            	break;
            }
        }
        
    };
}
