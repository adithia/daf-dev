package com.salesforce.generator.action;

import java.util.HashMap;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;

public class EvaluateAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public double evaluate (String code,SingleRecordset data ){
        double result = 0;
          
        HashMap map = new HashMap();
        
//        map.put("A", AUtility.getDouble(nForm.getVirtualString(data.get("parama").toString())));
//        map.put("B", AUtility.getDouble(nForm.getVirtualString(data.get("paramb").toString())));
//        map.put("C", AUtility.getDouble(nForm.getVirtualString(data.get("paramc").toString())));
//        map.put("D", AUtility.getDouble(nForm.getVirtualString(data.get("paramd").toString())));
//        map.put("E", AUtility.getDouble(nForm.getVirtualString(data.get("parame").toString())));
//        map.put("F", AUtility.getDouble(nForm.getVirtualString(data.get("paramf").toString())));
//        map.put("G", AUtility.getDouble(nForm.getVirtualString(data.get("paramg").toString())));
//        map.put("H", AUtility.getDouble(nForm.getVirtualString(data.get("paramh").toString())));
        
      
        try {
            Calculable cc =  new ExpressionBuilder(code).withVariables(map).build();
            result = cc.calculate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return result;
    }

}
