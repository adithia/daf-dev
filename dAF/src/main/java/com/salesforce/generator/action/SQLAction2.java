package com.salesforce.generator.action;

import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class SQLAction2 implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String param3 = data.getText("param3");
		
		//klo ada yang uppercase salah lisna 15/2/2016
		String value = data.getText("param2").toLowerCase();
		
		Recordset rec = Connection.DBquery(getStrQuery(comp, param3));
		if (rec.getRows() > 0) {
			comp.getForm().setFormComponentText(data.getText("result"),rec.getText(0, value));
		}
		return true;
	}
	
	private String getStrQuery(Component comp, String param){
		String query = "";
		String va = param;
		Vector<String> queries = new Vector<String>();
		if (va.startsWith("[")) {			
			Nset ns =Nset.readJSON(va);
			queries = Utility.splitVector(ns.getData(0).toString(), "?");
			for (int i = 0; i < ns.getArraySize(); i++) {
				if (i == 0) {
					//edit ctm 08 Jan 2019 irfan
//					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
//						query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
//					}else if(queries.elementAt(i).toString().contains("="))
//						query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
//					else {
//						query = queries.elementAt(i).toString();
//					}
//				}else					
//					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
//						query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
//					}else if(queries.elementAt(i).toString().contains("="))
//						query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
//					else {
//						query = query + queries.elementAt(i).toString();
//					}
					//end edit
					
					//add ctm 08 Jan 2019
				if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
					query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
				}else if(queries.elementAt(i).toString().contains("=")){
					query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
				}else if (queries.elementAt(i).toString().contains("&&")) {
					query = query + queries.elementAt(i).toString().replace("&&", "=");
				}else {
					query = queries.elementAt(i).toString();
				}
			}else	
				if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
					query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
				}else if(queries.elementAt(i).toString().contains("=")){
					query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
				}else if (queries.elementAt(i).toString().contains("&&")) {
					query = query + queries.elementAt(i).toString().replace("&&", "=");
				}else {
					query = query + queries.elementAt(i).toString();
				}
			}
			//end ctm
			
		}else {
			query = va;
		}
		
		return query;
	}
	
	private String getDataComp(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}

}
