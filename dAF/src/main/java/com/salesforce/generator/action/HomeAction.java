package com.salesforce.generator.action;

import android.content.Intent;

import com.daf.activity.DAFMenu;
import com.daf.activity.LoginActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class HomeAction implements IAction{
	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		
		if (Generator.currAppMenu.equalsIgnoreCase("order")) {
			new goToOrderAction().onAction(comp, data);
		}else if (Generator.currAppMenu.equalsIgnoreCase("newentry")) {
			Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
			Intent intent = new Intent(comp.getForm().getActivity(), DAFMenu.class);			
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			comp.getForm().getActivity().startActivity(intent);
			comp.getForm().getActivity().finish();
		}else if (Generator.currAppMenu.equalsIgnoreCase("pending")) {
			new goToPending().onAction(comp, data);
		}else if (Generator.currAppMenu.equalsIgnoreCase("draft")) {
			new goToDraft().onAction(comp, data);
		}else if(Generator.currAppMenu.equalsIgnoreCase("tasklistoffline")){
			new goToOrderOfflineAction().onAction(comp, data);
		}else if(Generator.currAppMenu.equalsIgnoreCase("tasklistdata")){
			new goToTasklisDataAction().onAction(comp, data);
		}else if(Generator.currAppMenu.equalsIgnoreCase("dafpameran")){
			Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
			Intent intent = new Intent(comp.getForm().getActivity(), DAFMenu.class);			
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			comp.getForm().getActivity().startActivity(intent);
			comp.getForm().getActivity().finish();
		}
		
//		Intent intent = null;
//		if (Generator.currAppMenu.equalsIgnoreCase("newentry")) {
//			intent = new Intent(comp.getForm().getActivity(), DAFMenu.class);
//			Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			comp.getForm().getActivity().startActivity(intent);
//			comp.getForm().getActivity().finish();
//		}else if (Generator.currAppMenu.equalsIgnoreCase("newentry")) {
//			
//		}else {
//			intent = new Intent(comp.getForm().getActivity(), Generator.currHome);
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			comp.getForm().getActivity().startActivity(intent);
//			comp.getForm().getActivity().finish();
//		}
		
		return false;//new CloseAction().onAction(comp, data);
	}

}
