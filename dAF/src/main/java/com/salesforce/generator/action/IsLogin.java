package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class IsLogin implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		final String resultComp = data.getText("result");
//		for(int i=0; i<comp.getForm().getAllComponent().size(); i++){
//			if(comp.getForm().getAllComponent().get(i).getName().equalsIgnoreCase(resultComp.substring(1))){
//				comp.getForm().getAllComponent().get(i).setText(String.valueOf(Generator.isLogin));
//			}
//		}

		comp.getForm().setFormComponentText(data.getText("result"), String.valueOf(Generator.isLogin).toUpperCase());
		
		return false;
	}

}
