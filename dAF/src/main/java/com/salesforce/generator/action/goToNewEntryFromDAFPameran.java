package com.salesforce.generator.action;

import android.app.Activity;
import android.content.Intent;

import com.daf.activity.NewEntry;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class goToNewEntryFromDAFPameran implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		Activity activity = comp.getForm().getActivity();
		Intent in = new Intent(activity, NewEntry.class);
		in.putExtra("seno", "seno");
		in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
		activity.startActivity(in);
		activity.finish();
		return false;
	}

}
