package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class NextValidationAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		comp.getForm().showNextWithValidation();
		return true;
	}

}
