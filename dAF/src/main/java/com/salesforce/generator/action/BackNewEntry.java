package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class BackNewEntry implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		if(comp.getForm().getIndex()==0){
			new goToNewEntry().onAction(comp, data);
		} else {
			comp.getForm().showBack();
		}
		return false;
	}
	
}
