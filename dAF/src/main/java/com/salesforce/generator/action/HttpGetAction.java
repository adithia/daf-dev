package com.salesforce.generator.action;

import android.app.Activity;

import com.daf.activity.SettingActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class HttpGetAction implements IAction{
	private String hasilHit;
	@Override
	public boolean onAction(final Component comp, SingleRecordset data) {
		final String result = data.getText("result");		
		final String rawUrl =comp.getForm().getFormComponentText(data.getText("param3")).toLowerCase(); 
//				data.getText("param3").toLowerCase();
		final Activity context = comp.getForm().getActivity();
		final String param3 =SettingActivity.URL_SERVER+rawUrl;
//				+ "userid=12167"
//				+ "&v=100"
////				+ "&token=6d8da4d8-7e8e-4322-8269-bd3e6e9ea63a"
//				+ "&branchid=21200"
//				+ "&objyear=2000"
//				+ "&objcode=V00184";
		
		Messagebox.showProsesBar(context, new Runnable() {			
			@Override
			public void run() {
				if(Utility.getNewToken()){
					String token = Utility.getSetting(context, "Token", "");
					if(token!=""){
//						String url = param3 ;
//						+"&token="+Utility.getSetting(context, "Token", "");
						String hasil = Syncronizer.getHttp(param3);
						try {
							double d = Double.parseDouble(hasil);
							hasilHit = hasil;
						}catch (NullPointerException|NumberFormatException nfe){
							hasilHit = "0";
						}
					}else{
						hasilHit = "0";
					}					
				}else{
					hasilHit="0";
				}
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				comp.getForm().setFormComponentText(result,hasilHit);
			}
		});		
		return false;
	}

}
