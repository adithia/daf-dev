package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class GetYearAction implements IAction {
	private String[] arrMonth = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	private String[] arrMonth2 = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		
		String value = data.getText("param3");
		value = comp.getDataComp(comp, value);
		String targetComp = data.getText("result");
		String date = "";
		
		if (value != null || !value.equalsIgnoreCase("")) {
			Vector<String> vData = com.salesforce.utility.Utility.splitVector(value, "-");
			if (vData != null) {
				for (int i = 0; i < arrMonth.length; i++) {
					if (arrMonth[i].equalsIgnoreCase(vData.get(1))) {
						value = value.replace(arrMonth[i], arrMonth2[i]);
						break;
					}
				}
			}
		}
		
		Recordset rst = Connection.DBquery("SELECT (strftime('%Y', 'now') - strftime('%Y', '"+Utility.convertDate2(value)+"')) - (strftime('%m-%d', 'now') < strftime('%m-%d', '"+Utility.convertDate2(value)+"')) as age");
		
		
		if (rst.getRows()>0) {
			date = rst.getText(0, "age");
			if (targetComp.startsWith("$")||targetComp.startsWith("@")) {
				comp.getForm().setFormComponentText(targetComp, date);
			}
		}else{
			comp.getForm().setFormComponentText(targetComp, "");//error
		}
		
		//SELECT (strftime('%Y', 'now') - strftime('%Y', 'now')) - (strftime('%m-%d', 'now') < strftime('%m-%d', '2010-12-10')) as age;
		return true;
	}

}
