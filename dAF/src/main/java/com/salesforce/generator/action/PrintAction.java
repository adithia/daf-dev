package com.salesforce.generator.action;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Vector;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Nset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;
import com.sdk.bluetooth.android.PrintGraphics;

public class PrintAction implements IAction{
	 
	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String va = data.getText("result");
		va=execJson(comp, va);
		final String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		final String result = va;
		
		String param=comp.getForm().getFormComponentText("@BLUETOOTHNAME");
		
		if (printerName.equals("") &&  param.startsWith("TII")) {
			printerName="sprt";
		}

		if (printerName.equals("sprt")) {
			comp.getForm().getActivity().printSprt(param, LogoAsset(comp.getForm(), imageName), result);
		}else{
			//comp.getForm().getActivity().printBT(param3, result);
			Messagebox.showProsesBar(comp.getForm().getActivity(), new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(7000);
					} catch (Exception e) { } 				
				}
			}, new Runnable() {			
				@Override
				public void run() {				 
					
				}
			});
			if (param.toLowerCase().startsWith("bc") && param.toLowerCase().endsWith("b") && printerName.toLowerCase().equals("nikita")) {
				comp.getForm().getActivity().printBT(param3,   result  );
			}else if (  printerName.toLowerCase().equals("nologo")) {
				comp.getForm().getActivity().printBT(param3,   result  );
			}else{
				comp.getForm().getActivity().printBT(param3, GetLogo(comp.getForm(), result) );
			}			
		}
		 
		 
		
//		try {
//			comp.getForm().getActivity().printBT(param3,  new String( GetLogo(comp.getForm()),"GBK")  );
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		if (!param3.equals("")) {
//			new Thread(new Runnable() {
//				public void run() {
//					try {
//						//FormActivity.mService.connect(FormActivity.mService.getDevByMac(param3));
//					} catch (Exception e) { }
//				}
//			}).start();
//			
//		}
		
		return false;
	}
	
	 
	
	public byte[] GetLogo(Form form , String data) {
         String logo = "";
        
         try {
        	 InputStream is = form.getActivity().getAssets().open("logo.jpg");//n.png
        	 Bitmap img0 =BitmapFactory.decodeStream(is);
        	 
             boolean[] dots = getBitmapMonoChromeWithScalePrinter(img0);             
            
             
             ByteArrayOutputStream out= new ByteArrayOutputStream();
           
     		
     		 
     		  int threshold=127;
      
             int multiplier = 570; // this depends on your printer model. for Beiyang you should use 1000
             double scale = (double)(multiplier/(int)img0.getWidth());
             int iheight = (int)(img0.getHeight() * scale);
             int iwidth = (int)(img0.getWidth() * scale);
             
             
             
             int integer = iwidth;
             byte[] width = new byte[4];
             for (int i = 0; i < 4; i++) {
            	 width[i] = (byte)(integer >>> (i * 8));
             }
             byte[] widthmsb=  new byte[] {
                     (byte)(integer >>> 24),
                     (byte)(integer >>> 16),
                     (byte)(integer >>> 8),
                     (byte)integer};
             
             
             int offset = 0;
     
             
             out.write((char)0x1B);
             out.write('3');
             out.write((byte)0);
             
             while (offset < iheight){
            	 
            	 
            	 //raster error
            	 //out.write((char)0x1D);
            	 //out.write((char)0x76);        
            	 // out.write((char)0x30);   
            	 
            	 //bit image
            	 out.write((char)0x1B);
            	 out.write('*');         // bit-image mode
            	 out.write((byte)33);    // 24-dot double-density
            	 out.write(width[0]);  // width low byte
            	 out.write(width[1]);  // width high byte
 
            	 
                 for (int x = 0; x < iwidth; ++x)   {
                     for (int k = 0; k < 3; ++k)  {
                         byte slice = 0;
                        
                         for (int b = 0; b < 8; ++b) {
                             int y = (((offset / 8) + k) * 8) + b;
                             // Calculate the location of the pixel we want in the bit array.
                             // It'll be at (y * width) + x.
                             int i = (y * iwidth) + x;

                             // If the image is shorter than 24 dots, pad with zero.
                             boolean v = false;
                             if (i < dots.length) {
                                 v = dots[i];
                             }
                             slice |= (byte)((v ? 1 : 0) << (7 - b));
                         }

                         out.write(slice);
                     }
                 }
                 offset += 24;
                 out.write((char)0x0A);
             }
           
             
             //fort 12x24
             out.write((char)0x1B);
             out.write((char)0x4D);
             out.write((byte)0);
             
             // Restore the line spacing to the default of 30 dots.
             out.write((char)0x1B);
             out.write('2');
            //out.write((byte)32);
             
             out.write(data.getBytes());;
            
             return out.toByteArray();
         } catch (Exception e) {  }
         return new byte[0];
    }

     
	public boolean[] getBitmapMonoChromeWithScalePrinter(Bitmap img0) {
		int height = img0.getHeight(); 
		int width = img0.getWidth(); 
		
		 
		int threshold=127;
 
        int multiplier = 570; // this depends on your printer model. for Beiyang you should use 1000
        double scale = (double)(multiplier/(int)width);
        int xheight = (int)(height * scale);
        int xwidth = (int)(width * scale);
        int dimensions = xwidth * xheight;
      
		
		int index=0;
		boolean[] dots= new boolean[dimensions];
		
		for (int y = 0; y < xheight; y++) {
            for (int x = 0; x < xwidth; x++)  {
                int _x = (int)(x / scale);
                int _y = (int)(y / scale);
                int rgb = img0.getPixel(_x, _y); 
                int r = (rgb >> 16) & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb & 0xFF);
                //int gray = (r + g + b) / 3;
                int luminance = (int)(r* 0.3 + g * 0.59 + b * 0.11);
                dots[index] =    (luminance < threshold);
                index++;
            }
        }
		return dots;
    }
	
	public static byte[] LogoAsset (Form form, String imageName){
		com.salesforce.generator.action.PrintGraphics pg = new com.salesforce.generator.action.PrintGraphics();
		pg.initCanvas(380);
		pg.initPaint();
		try {
			InputStream is = form.getActivity().getAssets().open("logo.jpg", AssetManager.ACCESS_BUFFER);
			pg.drawImage(0, 0, is);
		}catch (Exception e) { }		
		return pg.printDraw();
	}
	
	public static byte[] Logo (Form form, String imageName){
		if (imageName.length()>=3) {
			try {
				new File("/sdcard/logo.jpg").delete();
			} catch (Exception e) {}
			try {
				InputStream is = form.getActivity().getAssets().open("logo.jpg");
				Utility.copyFile(is, "/sdcard/logo.jpg");
			} catch (Exception e) { }
		}		
		PrintGraphics pg = new PrintGraphics();
		pg.initCanvas(540);
		pg.initPaint();
		pg.drawImage(0, 0, "/sdcard/logo.jpg");
		pg.printPng();
		
		return pg.printDraw();
	}
	private String printerName= "";
	private String imageName= "";
	
	private String execJson(Component comp, String s){
		if (s.startsWith("[")||s.startsWith("{")) {
			StringBuffer sbuBuffer = new StringBuffer();
			Nset nikiset = Nset.readJSON(s);
			for (int i = 0; i < nikiset.getArraySize(); i++) {
				  if (nikiset.getData(i).getType()==Nset.NTYPE_OBJECT) {
					  if (nikiset.getData(i).getData("right").toInteger()>=1) {
						  String data =filldata(comp, nikiset.getData(i).getData("data").toString());
						  sbuBuffer.append(rightCcurrency(data, nikiset.getData(i).getData("right").toInteger()));
					  }else if (nikiset.getData(i).getData("rightcurrency").toInteger()>=1) {
						  String data =filldata(comp, nikiset.getData(i).getData("data").toString());
						  data=data.replace(",", "").replace(".", "");
						  data=Utility.formatCurrency(data) ;
						  sbuBuffer.append(rightCcurrency(data, nikiset.getData(i).getData("rightcurrency").toInteger()));
					  }else if (nikiset.getData(i).getData("left").toInteger()>=1) {
						  String data =filldata(comp, nikiset.getData(i).getData("data").toString());
						  sbuBuffer.append(leftsubstring(data, nikiset.getData(i).getData("left").toInteger()));
					  }else if (nikiset.getData(i).getData("action").toString().equals("currency")) {
						  String data =filldata(comp, nikiset.getData(i).getData("data").toString());
						  data=data.replace(",", "").replace(".", "");
						  sbuBuffer.append(Utility.formatCurrency(data)) ;
					  }else if (nikiset.getData(i).getData("printername").toString().length()>=3) {
						  printerName=nikiset.getData(i).getData("printername").toString().toLowerCase();
					  }else if (nikiset.getData(i).getData("image").toString().length()>=3) {
						  imageName=nikiset.getData(i).getData("image").toString();
					  }
				  }else{
					  sbuBuffer.append(filldata(comp, nikiset.getData(i).toString()));
				  }
			}
			return sbuBuffer.toString();
		}else{
			return exec(comp,s);
		}		
	}
	private String leftsubstring(String s, int max){
		if (s.length()>max) {
			return s.substring(0, max);
		}
		return s;
	}
	private String rightCcurrency(String s, int max){
		s="                                          "+s;
		return s.substring(s.length()-max, s.length());
	}
	private String filldata(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
	
	private String exec(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
		StringBuffer sbBuffer = new StringBuffer("");
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
					sbBuffer.append(comp.getForm().getFormComponentText(p)).append(" ");
				}else{
					sbBuffer.append(p).append(" ");
				}
			}
		}
		return sbBuffer.toString();
    }
	public static byte[] PrintImage (Form form, String imageName){
 
		try {
			InputStream is = form.getActivity().getAssets().open("lg.png", AssetManager.ACCESS_BUFFER);
			Bitmap btm = BitmapFactory.decodeStream(is);
			PrintImage(btm);
		}catch (Exception e) { }		
		return PrintImage(null);
	}
	public static  byte[]  PrintImage(Bitmap img0) {
		try {
			
			int width = img0.getWidth()/8;
			int heigh = img0.getHeight();
			
			 
			
    		byte x[]=new byte[width*heigh+8]; 
    		
    		
	    	x[0]=(byte) 29; 
	    	x[1]=(byte) 118; 
	    	x[2]=(byte) 48; 
	    	x[3]=(byte) 0; 
	    	x[4]=(byte) ( width  ); 
	    	x[5]=(byte) 0;
	    	x[6]=(byte) heigh;  
	    	x[7]=(byte) 0;
	    	 
	    	     int w = img0.getWidth();
	    	    int hi = img0.getHeight();
	    	    int bytesOfWidth = w / 8 + (w % 8 != 0 ? 1 : 0);

	    	    x[4] = (byte) (bytesOfWidth % 256);
	    	    x[5] = (byte) (bytesOfWidth / 256);
	    	    x[6] = (byte) (hi % 256);
	    	    x[7] = (byte) (hi / 256);
	    	    
	    	    
	    	 
	    	int ix = 7;
	    	for (int h = 0; h < heigh; h++) {

	    		for (int k = 0; k < width ; k++)  {
	 ix++; 
	    		/* 267 */         int c0 = img0.getPixel(k * 8 + 0, h);
	    		/*     */         int p0;
	    		/* 270 */         if (c0 == -1)
	    		/* 271 */           p0 = 0;
	    		/*     */         else {
	    		/* 273 */           p0 = 1;
	    		/*     */         }
	    		/* 275 */         int c1 = img0.getPixel(k * 8 + 1, h);
	    		/*     */         int p1;
	    		/* 276 */         if (c1 == -1)
	    		/* 277 */           p1 = 0;
	    		/*     */         else {
	    		/* 279 */           p1 = 1;
	    		/*     */         }
	    		/* 281 */         int c2 = img0.getPixel(k * 8 + 2, h);
	    		/*     */         int p2;
	    		/* 282 */         if (c2 == -1)
	    		/* 283 */           p2 = 0;
	    		/*     */         else {
	    		/* 285 */           p2 = 1;
	    		/*     */         }
	    		/* 287 */         int c3 = img0.getPixel(k * 8 + 3, h);
	    		/*     */         int p3;
	    		/* 288 */         if (c3 == -1)
	    		/* 289 */           p3 = 0;
	    		/*     */         else {
	    		/* 291 */           p3 = 1;
	    		/*     */         }
	    		/* 293 */         int c4 = img0.getPixel(k * 8 + 4, h);
	    		/*     */         int p4;
	    		/* 294 */         if (c4 == -1)
	    		/* 295 */           p4 = 0;
	    		/*     */         else {
	    		/* 297 */           p4 = 1;
	    		/*     */         }
	    		/* 299 */         int c5 = img0.getPixel(k * 8 + 5, h);
	    		/*     */         int p5;
	    		/* 300 */         if (c5 == -1)
	    		/* 301 */           p5 = 0;
	    		/*     */         else {
	    		/* 303 */           p5 = 1;
	    		/*     */         }
	    		/* 305 */         int c6 = img0.getPixel(k * 8 + 6,h);
	    		/*     */         int p6;
	    		/* 306 */         if (c6 == -1)
	    		/* 307 */           p6 = 0;
	    		/*     */         else {
	    		/* 309 */           p6 = 1;
	    		/*     */         }
	    		/* 311 */         int c7 = img0.getPixel(k * 8 + 7,h);
	    		/*     */         int p7;
	    		/* 312 */         if (c7 == -1)
	    		/* 313 */           p7 = 0;
	    		/*     */         else {
	    		/* 315 */           p7 = 1;
	    		/*     */         }
	    		/* 317 */         int value = p0 * 128 + p1 * 64 + p2 * 32 + p3 * 16 + p4 * 8 + 
	    		/* 318 */           p5 * 4 + p6 * 2 + p7;
	    		/* 319 */          x[ix] = ((byte)value);
	    		/*     */       }
	    		
	    		
	    		
	    		
			}
	 
	    	return x;
    	} catch (Exception e) {return   ( "" ).getBytes();}
		
		
	}
  
}
