package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.component.IWillSave;
import com.salesforce.database.Connection;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class SaveAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		
		for (int i = 0; i < Generator.forms.size(); i++) {
			
			for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
				if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
					((IWillSave)Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
				}
			}
		}
		
		if (!Generator.currAppMenu.equals("sent")) {
			
			Connection.DBcreate("data_activity", "activityId", "orderCode","appId","user", "data", "status", "nview", "ndate", "time", "isTasklist", "applNo", "branchId", "bussUnit");
			Connection.DBdelete("data_activity", "activityId=?", new String[]{Generator.currModelActivityID});
			
			if (Generator.isTasklist) {
				Connection.DBinsert("data_activity", "activityId="+Generator.currModelActivityID.trim(), "orderCode="+Generator.currOrderCode , 
						"appId="+Generator.currApplication, "user="+Utility.getSetting(comp.getForm().getActivity(), "USERNAME", ""),"data="+ comp.getForm().getDataAll(),
						"status=0", "time="+Utility.getTime(), "isTasklist=true", "applNo="+Generator.applno, "branchId="+Generator.branchid, "bussUnit="+Generator.bu);				
				
			}else {
				Connection.DBinsert("data_activity", "activityId="+Generator.currModelActivityID.trim(), "orderCode="+Generator.currOrderCode , 
						"appId="+Generator.currApplication, "user="+Utility.getSetting(comp.getForm().getActivity(), "USERNAME", ""),"data="+ comp.getForm().getDataAll(),
						"status=0", "time="+Utility.getTime(),"isTasklist=false", "bussUnit="+Generator.bu);
			}
			
			
			Connection.DBupdate("data_activity", "ndate="+Utility.Now().substring(0, 10),"nview="+Generator.currBufferedView,  "where", "activityId="+Generator.currModelActivityID.trim());
		}
		
		//delete data yang ada ditabel draft berdasarkan orderCode
		if (Generator.isNewEntry || Generator.currAppMenu.equalsIgnoreCase("draft")) {
			Connection.DBquery("DELETE FROM DRAFT WHERE orderCode = '"+Generator.currOrderCode+"'");
		}

		if(Generator.currAppMenu.equalsIgnoreCase("tasklistoffline")){
//			Connection.DBupdate("trn_tasklist_offline","sent=Y", "where", "orderId="+Generator.currOrderCode);
			Connection.DBquery("DELETE FROM TRN_TASKLIST_OFFLINE WHERE orderId = '"+Generator.currOrderCode+"' and data is null");			
//			Connection.DBdelete("trn_tasklist_offline","orderId='"+Generator.currOrderCode+"'");
		}
		return true;
	}

}
