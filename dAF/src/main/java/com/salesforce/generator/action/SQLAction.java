package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class SQLAction implements IAction {
	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// String param3 =
		String param3 = execJson(comp, data.getText("param3"));
		Generator.currRecordset = Connection.DBquery(param3);
		Generator.currResult = "rows="+Generator.currRecordset.getRows();
		comp.getForm().setFormComponentText(data.getText("result"), Generator.currResult);
		return true;
	}
	private String execJson(Component comp, String s){
		if (s.startsWith("[")||s.startsWith("{")) {
			StringBuffer sbuBuffer = new StringBuffer();
			Nset nikiset = Nset.readJSON(s);
			for (int i = 0; i < nikiset.getArraySize(); i++) {
				sbuBuffer.append(filldata(comp, nikiset.getData(i).toString()));
			}
			return sbuBuffer.toString();
		}else{
			return filldata(comp,s);
		}		
	}
	private String filldata(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
}
