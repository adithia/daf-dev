package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SubStringAction2 implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String vir1 = comp.getForm().getFormComponentText(data.getText("param1"));
		String vir3 = comp.getForm().getFormComponentText(data.getText("param3"));
		String value = "";
		if (vir3.length() > Integer.valueOf(vir1)) {
			value = vir3.substring(0, Integer.valueOf(vir1));
			comp.getForm().setFormComponentText(data.getText("result"), value);
		}else{
			value = vir3;
			comp.getForm().setFormComponentText(data.getText("result"), value);
		}
		return true;
	}

}
