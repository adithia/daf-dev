package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class NonMandatoryAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		if (!param3.equalsIgnoreCase("-")) {
			removeMandatoryComponent(data.getText("param2"));
		}
		return true;
	}
	
	private void removeMandatoryComponent(String comp){
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
				if (Generator.forms.elementAt(i).components.elementAt(c).getName().equalsIgnoreCase(comp.substring(1))) {
					Generator.forms.elementAt(i).components.elementAt(c).nonMandatory();
				}
			}
			
		}
	}

}
