package com.salesforce.generator.action;

import java.io.File;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Adapter;
import android.widget.Toast;

import com.daf.activity.DAFPameran;
import com.daf.activity.NewEntry;
import com.daf.activity.SettingActivity;
import com.daf.activity.TaskListOfflineActivity;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.component.IWillSave;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.generator.ui.TextboxUi;
import com.salesforce.stream.Model;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;


public class ValidasiApplNo implements IAction {
	String msgToUser;
	String response="";

	String applNo;
	String custName;
	@Override
	public boolean onAction(final Component comp, final SingleRecordset data) {
		String[] splitValue =Utility.split(data.getText("param3"), "|");
		String value = data.getText("param3");		
		SingleRecordset cdess = null;
//		applNo = comp.getForm().getFormComponentText(value);
		applNo = comp.getForm().getFormComponentText(splitValue[0]);
		custName = comp.getForm().getFormComponentText(splitValue[1]);
		
		//untuk mengambil satu recorset dari arrayrecordset
		for(int i=0; i<data.getCols(); i++){
			SingleRecordset cdedd = new  SingleRecordset(data,i);
			if(cdedd.getText("param3").equalsIgnoreCase("#SendingFile")){
				cdess = cdedd;
				break;
			}
		}
		
		final SingleRecordset cdes = cdess;
		final String resultComp = data.getText("result");
		final Activity ctx = comp.getForm().getActivity();
		
		
		 msgToUser = "No aplikasi atas customer\n"+custName+" ditemukan:\n\n"
				+ "ApplNo: "+applNo+"\n";		 		
		
		 Messagebox.showProsesBar(comp.getForm().getActivity(), new Runnable() {			
			@Override
			public void run() {	
				if(Utility.getNewToken()){
					String sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "validasiApplnoServlet/",new String[]{"applno","token","v"},
							new String[] {applNo, Utility.getSetting(ctx, "Token", ""),"100"});								
					Recordset newRecord = Syncronizer.getStringHttpStream(sUrl);					
					Vector<Vector<String>> respons = newRecord.getAllDataVector();
					Vector<String> header  =  newRecord.getAllHeaderVector();
					if(newRecord.getText(0, "result").equalsIgnoreCase("OK")){
						for(int i=3;i<respons.get(0).size();i++){
							msgToUser = msgToUser+header.get(i)+": "+respons.get(0).get(i)+"\n";
						}										
						msgToUser = msgToUser +"\nJika data sudah sesuai, "
								+ "pilih button kirim untuk mengirim Data, pilih button hapus untuk hapus tasklist yang di kerjakan";
						
						response = "ditemukan";
					}else if(newRecord.getText(0, "result").equalsIgnoreCase("BAD")){
						msgToUser = newRecord.getText(0, "message");
						response  = "tidak ditemukan";
					}else{
						msgToUser = "Validasi gagal masalah koneksi \nConnection Timeout";
						response="Time out";
					}
				}else{
					msgToUser = "Validasi gagal masalah koneksi \nConnection Timeout";
					response="Time out";
				}
							
				
			}
		}, new Runnable() {
			
			@Override
			public void run() {
//				comp.getForm().setFormComponentText(resultComp, response);
				if (response.equalsIgnoreCase("ditemukan")) {
					AdapterDialog.showDialogThreeBtn(ctx, "Informasi", msgToUser, "Kirim",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									//hapus tasklist//									Toast.makeText(ctx, "tada u click kirm", Toast.LENGTH_SHORT).show();
									for (int i = 0; i < comp.getForm().getAllComponent().size(); i++) {
										if (comp.getForm().getAllComponent().get(i).getName().equalsIgnoreCase(resultComp.substring(1))) {
											comp.getForm().getAllComponent().get(i).setText("TRUE");
										}
									}
									if (cdes != null) {
										new RoutingAction().onAction(comp, cdes);
									}

								}
							}, "Hapus", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									//sendaction
//									Toast.makeText(ctx, "tada u click hapus", Toast.LENGTH_SHORT).show();
									AdapterDialog.showDialogTwoBtn(ctx, "Confirmation", "Apakah anda yakin akan menghapus order ini?", "Ya", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											Toast.makeText(ctx, "tada u click ya", Toast.LENGTH_SHORT).show();

											Connection.DBdelete("trn_tasklist_offline", "orderId=" + Generator.currOrderCode);
											Connection.DBdelete("LOG_IMAGE", "applno=" + Generator.currOrderCode);
											String userIdFrom = Utility.getSetting(comp.getForm().getActivity(), "userid",
													"").trim();
											String a = userIdFrom + Generator.currOrderCode;
											// ambil semua data di folder images
											String[] imagesFiles = new File(Utility.getDefaultImagePath()).list();
											for (String file : imagesFiles) {
												if (file.contains(a)) {
													File tmp = new File(Utility.getDefaultImagePath() + "/" + file);
													Utility.deleteFile(tmp.toString());
												}
											}

											// ambil semua data di folder tmp
											String[] tmpFiles = new File(Utility.getDefaultTempPath()).list();
											for (String file : tmpFiles) {
												if (file.contains(a)) {
													File tmp = new File(Utility.getDefaultTempPath() + file);
													Utility.deleteFile(tmp.toString());
												}
											}

											TaskListOfflineActivity.deleteFingerImg(Generator.currOrderCode);

											if (Utility.activity != null) {
												if (Utility.activity instanceof TaskListOfflineActivity) {
													TaskListOfflineActivity sd = (TaskListOfflineActivity) Utility.activity;
													sd.refresh();
												}
											}
											comp.getForm().getActivity().finish();
										}
									}, "Tidak", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
//											Toast.makeText(ctx, "tada u click tidak", Toast.LENGTH_SHORT).show();
										}
									});

								}
							}, "Batal", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Toast.makeText(ctx,"you Click Kembali button",Toast.LENGTH_SHORT).show();
								}
							});

				}else if(response.equalsIgnoreCase("tidak ditemukan")){					
					AdapterDialog.showDialogOneBtn(ctx, "Informasi", msgToUser, "Kembali", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});
					/*AdapterDialog.showDialogThreeBtn(ctx, "Informasi", msgToUser, "ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(ctx,"you Click Ok button",Toast.LENGTH_SHORT).show();
						}
					}, "no", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(ctx,"you Click No button",Toast.LENGTH_SHORT).show();

						}
					}, "cancle", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(ctx,"you Click Cancle button",Toast.LENGTH_SHORT).show();

						}
					});*/
				}else if(response.equalsIgnoreCase("Time out")){
					
					update_tasklist_offline(comp,applNo,custName);
					AdapterDialog.showDialogOneBtn(comp.getForm().getActivity(),"Information", msgToUser, "Ok",new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {

							comp.getForm().getActivity().finish();
						}
					});
				}
				
			}
		});
		return false;
	}
	
	private void update_tasklist_offline(Component components, String applNo, String custName){				
		String applno="";
		String custno="";
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
				if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
					((IWillSave)Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
				}
			}
		}		
		applno = components.getForm().getFormComponentText(applNo);
		custno = components.getForm().getFormComponentText(custName);		
		Connection.DBupdate("trn_tasklist_offline","applno="+applno,"cust_name="+custno, "data="+components.getForm().getDataAll(),"activityId="+Generator.currModelActivityID,"where","orderId="+Generator.currOrderCode);		
		if (Utility.activity != null) {
			if (Utility.activity instanceof TaskListOfflineActivity) {
				TaskListOfflineActivity sd = (TaskListOfflineActivity) Utility.activity;
				sd.refresh();
			}
		}
		
	}
		
}
