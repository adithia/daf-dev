package com.salesforce.generator.action;

import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class SQLAction4 implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		Vector<String> results = new Vector<String>();
		Vector<String> values = new Vector<String>();
		Vector<Vector<String>> resultss = new Vector<Vector<String>>();
		
		if(data.getText("result").contains("#")&&data.getText("result").contains(",")){
			Vector<String> tmp = Utility.splitVector(data.getText("result"), "#");
			for(int i=0; i< tmp.size(); i++){
				Vector<String> tmpi = Utility.splitVector(tmp.get(i), ",");
				resultss.add(tmpi);
			}						
		}else if(data.getText("result").contains("|")){
			results = Utility.splitVector(data.getText("result"), "|");
		}else{
			results = new Vector<String>();
			results.add(data.getText("result"));
		}
		
		if(data.getText("param2").contains("|")){
			Vector<String> tmp =Utility.splitVector(data.getText("param2"), "|"); 
			for(int i=0; i<tmp.size();i++){
				values.add(tmp.get(i).toLowerCase());
			}
		}else{
			values = new Vector<String>();
			values.add(data.getText("param2").toLowerCase());
		}
				
		String param3 = data.getText("param3");
		
		//klo ada yang uppercase salah lisna 15/2/2016
		String SQL = getStrQuery(comp, param3);
		Recordset rec = Connection.DBquery(SQL);
		
		
		if (rec.getRows() > 0) {
			
			try{
				if(results.size()>0){
					for(int j=0; j<rec.getRows(); j++){						
						for(int i=0; i < results.size(); i++){						
							comp.getForm().setFormComponentText(results.get(i),rec.getText(j, values.get(i)));
						}
					}
				}
				
				if(resultss.size()>0){
//					for(int k = 0; k< resultss.size(); k++){
//						Vector<String> tmpResult = resultss.get(k);
//						for(int j=0; j<rec.getRows(); j++){						
//							for(int i=0; i < tmpResult.size(); i++){						
//								comp.getForm().setFormComponentText(tmpResult.get(i),rec.getText(j, values.get(i)));
//							}
//						}
//					}
					
//					for(int j=0; j<rec.getRows(); j++){
//						for(int k = 0; k< resultss.size(); k++){
//							Vector<String> tmpResult = resultss.get(k);						
//							for(int i=0; i < tmpResult.size(); i++){						
//								comp.getForm().setFormComponentText(tmpResult.get(i),rec.getText(j, values.get(i)));
//							}
//						}
//					}
					
						for(int j=0; j<rec.getRows(); j++){
						for(int k = 0; k< resultss.size(); k++){
							Vector<String> tmpResult = resultss.get(j);						
							for(int i=0; i < tmpResult.size(); i++){						
								comp.getForm().setFormComponentText(tmpResult.get(i),rec.getText(j, values.get(i)));
							}
						}
					}
				}	
			}catch(Exception e){
				e.printStackTrace();
			}			
		}
		
		return true;
	}
	
	private String getStrQuery(Component comp, String param){
		String query = "";
		String va = param;
		Vector<String> queries = new Vector<String>();
		if (va.startsWith("[")) {			
			Nset ns =Nset.readJSON(va);
			queries = Utility.splitVector(ns.getData(0).toString(), "?");
			for (int i = 0; i < ns.getArraySize(); i++) {
				if (i == 0) {
					//edit ctm 08 Jan 2019 irfan
//					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
//						query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
//					}else if(queries.elementAt(i).toString().contains("="))
//						query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
//					else {
//						query = queries.elementAt(i).toString();
//					}
//				}else					
//					if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
//						query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
//					}else if(queries.elementAt(i).toString().contains("="))
//						query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
//					else {
//						query = query + queries.elementAt(i).toString();
//					}
					//end edit
					
					//add ctm 08 Jan 2019
				if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
					query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
				}else if(queries.elementAt(i).toString().contains("=")){
					query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
				}else if (queries.elementAt(i).toString().contains("&&")) {
					query = query + queries.elementAt(i).toString().replace("&&", "=");
				}else {
					query = queries.elementAt(i).toString();
				}
			}else	
				if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
					query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
				}else if(queries.elementAt(i).toString().contains("=")){
					query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
				}else if (queries.elementAt(i).toString().contains("&&")) {
					query = query + queries.elementAt(i).toString().replace("&&", "=");
				}else {
					query = query + queries.elementAt(i).toString();
				}
			}
			//end ctm
			
		}else {
			query = va;
		}
		
		return query;
	}
	
	private String getDataComp(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
}
