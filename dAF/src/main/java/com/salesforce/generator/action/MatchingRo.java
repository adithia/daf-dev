package com.salesforce.generator.action;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import com.daf.activity.DAFMenu;
import com.daf.activity.NewEntry;
import com.daf.activity.SettingActivity;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.stream.Model;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import java.util.Vector;

public class MatchingRo implements IAction {
    final Vector<String> hasil = new Vector<String>();
    final Vector<String> fieldname = new Vector<String>();
    Recordset newRecord;
    private String resultMaching;
    //	private String messageTimeOut = "Matching data tidak bisa di lakukan tidak ada koneksi";
    private String headerInfo = "Information";
    private String table_name;
    private Activity activity;

    @Override
    public boolean onAction(final Component comp, final SingleRecordset data) {
        final Vector<String> abc = Utility.splitVector(data.getText("param3"), "|");

        for (int i = 0; i < abc.size(); i++) {
            if (abc.get(i).contains("$")) {
                hasil.add(comp.getForm().getFormComponentText(abc.get(i).toString()));
            } else {
                hasil.add(abc.get(i));
            }
        }
        fieldname.addAll(Utility.splitVector(data.getText("param2"), "|"));
        activity = comp.getForm().getActivity();

        final String param1 = data.getText("param1");
        Messagebox.showProsesBar(activity, new Runnable() {
            @Override
            public void run() {

                Utility.setSetting(activity, "tbl_matchingData", param1);

                if (Utility.getNewToken()) {
                    String sUrl = Utility.postMultipart(SettingActivity.URL_SERVER + "matchingDataServlet/",
                            new String[]{"nik", "fullname", "pob", "dob", "gender", "mothername", "token", "v", "userid"},
                            new String[]{comp.getForm().getFormComponentText(abc.get(0)),
                                    comp.getForm().getFormComponentText(abc.get(1)),
                                    comp.getForm().getFormComponentText(abc.get(2)),
                                    Utility.changeFormatDate(comp.getForm().getFormComponentText(abc.get(3)), "dd-MMM-yyyy", "yyyy-MM-dd"),
                                    comp.getForm().getFormComponentText(abc.get(4)),
                                    comp.getForm().getFormComponentText(abc.get(5)),
                                    Utility.getSetting(activity, "Token", ""), "100",
                                    Utility.getSetting(activity, "userid", "").trim()});

                    if (!sUrl.equalsIgnoreCase("Connection Timeout")) {
                        Nset order = Nset.readJSON(sUrl);

                        String customerType = order.getData("data").getData("customertype").toString();
//						table_name = order.getData("data").getData("tablename").toString();
                        table_name = param1;
                        if (!table_name.equalsIgnoreCase("")) {
                            Utility.setSetting(activity, "tbl_matchingData", table_name);
                        }
                        if (!customerType.equalsIgnoreCase("0")) {
                            newRecord = Syncronizer.getStringHttpStream(sUrl);

                            int row = newRecord.getRows();
                            if (row > 0) {
                                Syncronizer.saveToDB(table_name, newRecord);
                            } else {
                                defaultValue(table_name, fieldname, hasil);
                            }

                        }
                        resultMaching = customerType;
                    } else {
                        resultMaching = "Fieled to get Data";
                        defaultValue(param1, fieldname, hasil);
                    }
                } else {
                    resultMaching = "Fieled to get Data";
                    defaultValue(param1, fieldname, hasil);
                }

            }
        }, new Runnable() {
            @Override
            public void run() {
                String value = "";

//				Vector<String> blacklist = Utility.splitVector("0",",");
//				Vector<String> ro = Utility.splitVector("1",",");
//				Vector<String> fgc = Utility.splitVector("2",",");
//				Vector<String> dukcapil = Utility.splitVector("3",",");
//				Vector<String> newcustomer = Utility.splitVector("4",",");

                if (Utility.splitVector(Global.getText("matching.value.blacklist"), ",").contains(resultMaching)) {
                    value = "BLACKLIST";
                } else if (Utility.splitVector(Global.getText("matching.value.ro"), ",").contains(resultMaching)) {
                    value = "RO";
                } else if (Utility.splitVector(Global.getText("matching.value.fgc"), ",").contains(resultMaching)) {
                    value = "FGC";
                } else if (Utility.splitVector(Global.getText("matching.value.dukcapil"), ",").contains(resultMaching)) {
                    value = "DUKCAPIL";
                } else if (Utility.splitVector(Global.getText("matching.value.new_customer"), ",").contains(resultMaching)) {
                    value = "NEW CUSTOMER";
                } else {
                    value = "";
                }

                if (value.equals("") || value.isEmpty()) {
                    comp.getForm().setFormComponentText(data.getText("result"), "");
                } else {
                    comp.getForm().setFormComponentText(data.getText("result"), Global.getText("matching.flag.result." + resultMaching));
                }

                if (Generator.currOrderType.equalsIgnoreCase("dafpameran")) {
                    Connection.DBquery("ALTER TABLE " + table_name + " ADD " + fieldname.get(fieldname.size() - 1) + " TEXT;");
                    Connection.DBquery("update " + table_name + " set " + fieldname.get(fieldname.size() - 1) + "= 'Y' ;");
                } else {
                    Connection.DBquery("ALTER TABLE " + table_name + " ADD " + fieldname.get(fieldname.size() - 1) + " TEXT;");
                }

                if (value.equalsIgnoreCase("RO")) {//Ro
                    AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.ro." + resultMaching));
                } else if (value.equalsIgnoreCase("FGC")) {//FGC
                    String noFgc = "";
                    if (newRecord != null) {
                        noFgc = newRecord.getText(0, "noFGC");
                    }
                    AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.fgc." + resultMaching) + ": " + noFgc);
                    //autoFill
                } else if (value.equalsIgnoreCase("BLACKLIST")) {//Blacklist
                    AdapterDialog.showDialogOneBtn(activity, headerInfo,
                            Global.getText("matching.blacklist." + resultMaching),
                            "Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(activity, DAFMenu.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    activity.startActivity(intent);
                                    activity.finish();
                                }
                            });
                } else if (value.equalsIgnoreCase("NEW CUSTOMER")) {//new customer
                    if (Generator.currOrderType.equalsIgnoreCase("dafpameran")) {
                        askToMove(Global.getText("matching.newcustomer.pameran." + resultMaching));
                    } else {
                        AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.newcustomer.newentry." + resultMaching));
                    }
                } else if (value.equalsIgnoreCase("DUKCAPIL")) {//dukcapil
                    if (Generator.currOrderType.equalsIgnoreCase("dafpameran")) {
//						Connection.DBquery("ALTER TABLE "+table_name+" ADD "+fieldname.get(fieldname.size()-1)+" TEXT;");
//						Connection.DBquery("update "+ table_name +" set "+ fieldname.get(fieldname.size()-1) +"= 'Y' ;");
                        askToMove(Global.getText("matching.dukcapil.pameran." + resultMaching));
                    } else {
                        AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.dukcapil." + resultMaching));
                    }

                } else {
                    if (resultMaching.equalsIgnoreCase("Fieled to get Data")) {

                        if (Generator.currAppMenu.equalsIgnoreCase("draft")) {
                            if (Generator.currOrderType.equalsIgnoreCase("dafpameran")) {
                                askToMove(Global.getText("matching.error.pameran"));
                            } else {
                                AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.error"));
                            }
                        } else {
                            if (Generator.currOrderType.equalsIgnoreCase("dafpameran")) {
                                askToMove(Global.getText("matching.error.pameran"));
                            } else if (Generator.currOrderType.equalsIgnoreCase("newentry")) {
                                AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.error"));
                            } else {
                                AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.error"));
                            }
                        }
                    } else {
                        AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.error"));
                    }

                }

//				if(resultMaching.equalsIgnoreCase("0")){
//					value ="BLACKLIST";
//				}else if(resultMaching.equalsIgnoreCase("1")){
//					value ="RO";
//				}else if(resultMaching.equalsIgnoreCase("2")){
//					value ="FGC";
//				}else if(resultMaching.equalsIgnoreCase("3")){
//					value ="DUKCAPIL";
//				}else if(resultMaching.equalsIgnoreCase("4")){
//					value ="NEW CUSTOMER";
//				}else{
//					value ="";
//				}
//				comp.getForm().setFormComponentText(data.getText("result"), value);


//				if(Generator.currOrderType.equalsIgnoreCase("dafpameran")){
//					Connection.DBquery("ALTER TABLE "+table_name+" ADD "+fieldname.get(fieldname.size()-1)+" TEXT;");
//					Connection.DBquery("update "+ table_name +" set "+ fieldname.get(fieldname.size()-1) +"= 'Y' ;");
//				}else{
//					Connection.DBquery("ALTER TABLE "+table_name+" ADD "+fieldname.get(fieldname.size()-1)+" TEXT;");
//				}
//
//				if(resultMaching.equalsIgnoreCase("1")){//Ro
//					AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.ro"));
//				}else if(resultMaching.equalsIgnoreCase("2")){//FGC
//					String noFgc="" ;
//					if(newRecord!=null){
//						noFgc = newRecord.getText(0, "noFGC");
//					}
//					AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.fgc")+": "+noFgc);
//					//autoFill
//				}else if(resultMaching.equalsIgnoreCase("0")){//Blacklist
//					AdapterDialog.showDialogOneBtn(activity, headerInfo,
//							Global.getText("matching.blacklist"),
//							"Ok", new DialogInterface.OnClickListener() {
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									Intent intent = new Intent(activity, DAFMenu.class);
//									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//									activity.startActivity(intent);
//									activity.finish();
//								}
//							});
//				}else if(resultMaching.equalsIgnoreCase("4")){//new customer
//					if(Generator.currOrderType.equalsIgnoreCase("dafpameran")){
//						askToMove(Global.getText("matching.newcustomer.pameran"));
//					}else{
//						AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.newcustomer.newentry"));
//					}
//				}else if(resultMaching.equalsIgnoreCase("3")){//dukcapil
//					if(Generator.currOrderType.equalsIgnoreCase("dafpameran")){
////						Connection.DBquery("ALTER TABLE "+table_name+" ADD "+fieldname.get(fieldname.size()-1)+" TEXT;");
////						Connection.DBquery("update "+ table_name +" set "+ fieldname.get(fieldname.size()-1) +"= 'Y' ;");
//						askToMove(Global.getText("matching.dukcapil.pameran"));
//					}else{
//						AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.dukcapil"));
//					}
//
//				}else{
//					if(resultMaching.equalsIgnoreCase("Fieled to get Data")){
//
//						if(Generator.currAppMenu.equalsIgnoreCase("draft")){
//							AdapterDialog.showMessageDialog(activity, headerInfo,Global.getText("matching.error"));
//						}else{
//							if(Generator.currOrderType.equalsIgnoreCase("dafpameran")){
//								askToMove(Global.getText("matching.error.pameran"));
//							}else if(Generator.currOrderType.equalsIgnoreCase("newentry")){
//								AdapterDialog.showMessageDialog(activity, headerInfo,Global.getText("matching.error"));
//							}else{
//								AdapterDialog.showMessageDialog(activity, headerInfo, Global.getText("matching.error"));
//							}
//						}
//					}else{
//						AdapterDialog.showMessageDialog(activity, headerInfo,Global.getText("matching.error"));
//					}
//
//				}
            }

        });
        return false;
    }

    private void defaultValue(String nama_table, Vector<String> fieldname, Vector<String> values) {
        Vector<String> header = new Vector<String>();
        header.addAll(fieldname);
        if (Generator.currOrderType.equalsIgnoreCase("dafpameran")) {
            values.add("Y");
        }

        Vector<Vector<String>> value = new Vector<Vector<String>>();
        value.add(values);
        Model a = new Model(header, value);
        Syncronizer.saveToDB(nama_table, a);
    }


    private void askToMove(String message) {
        AdapterDialog.showDialogTwoBtn(activity, headerInfo,
                message,
                "lanjut", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent in = new Intent(activity, NewEntry.class);
                        in.putExtra("seno", "seno");
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
                        activity.startActivity(in);
                        activity.finish();

                    }
                }, "Kembali", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Generator.currAppMenu != "draft") {
                            Connection.DBdelete("DRAFT", "orderCode=?", new String[]{Generator.currOrderCode});
                            Intent intent = new Intent(activity, DAFMenu.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            activity.startActivity(intent);
                            activity.finish();
                        }
                    }
                });
    }
}

