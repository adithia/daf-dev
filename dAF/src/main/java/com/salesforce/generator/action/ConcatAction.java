package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Nset;
import com.salesforce.database.SingleRecordset;

public class ConcatAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String vir1 = comp.getForm().getFormComponentText(data.getText("param1"));
		String vir2 = execJson(comp, comp.getForm().getFormComponentText(data.getText("param2")) );
		String vir3 = comp.getForm().getFormComponentText(data.getText("param3"));
		comp.getForm().setFormComponentText(data.getText("result"), vir1+vir2+vir3);
		
		return true;
	}
	private String execJson(Component comp, String s){
		if (s.startsWith("[")||s.startsWith("{")) {
			StringBuffer sbuBuffer = new StringBuffer();
			Nset nikiset = Nset.readJSON(s);
			for (int i = 0; i < nikiset.getArraySize(); i++) {
				  if (nikiset.getData(i).getType()==Nset.NTYPE_OBJECT) {
					  sbuBuffer.append(filldata(comp, s)) ;
				  }else{
					  sbuBuffer.append(filldata(comp, nikiset.getData(i).toString()));
				  }
			}
			return sbuBuffer.toString();
		}else{
			return filldata(comp, s);
		}		
	}
	private String filldata(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
}
