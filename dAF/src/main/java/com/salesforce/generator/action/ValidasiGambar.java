package com.salesforce.generator.action;


import java.util.ArrayList;
import java.util.Vector;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Global;
import com.salesforce.utility.ModelVerificationPhoto;
import com.salesforce.utility.Utility;
import com.salesforce.utility.VerificationPhoto;

public class ValidasiGambar implements IAction{	

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String value = data.getText("param3");
		String resultComp = data.getText("result");		

		Context ctx = comp.getForm().getActivity();		
//		VerificationPhoto photo = new VerificationPhoto("$C21F32");
		VerificationPhoto photo = new VerificationPhoto(value);
		ArrayList<ModelVerificationPhoto> hasil = photo.getResult();
		String outputToStream = null;
		String outputToUser=null;
		
		if(hasil!=null){
			for(ModelVerificationPhoto s:hasil){						
				if(!s.getNotification().contains("OK")){			
					if(outputToUser==null){
						if(s.getNotification().contains("corrupt")){
							outputToUser="- "+s.getLabel()+" "+Global.getText("message.corrupt");
						}else{
							outputToUser="- "+s.getLabel();
						}						
					}else{
						if(s.getNotification().contains("corrupt")){
							outputToUser=outputToUser+".\n"+"- "+s.getLabel()+" "+Global.getText("message.corrupt");
						}else{
							outputToUser=outputToUser+".\n"+"- "+s.getLabel();	
						}						
					}
				}			
			}
			
			for(ModelVerificationPhoto s:hasil){
										
					if(outputToStream==null){
						outputToStream=s.getFlag();
					}else{
						outputToStream=outputToStream+","+s.getFlag();
					}						
			}
		}
		
		
		myAlert("Informasi", outputToUser, ctx, outputToStream, comp, resultComp);		
		return true;
	}
	
	public void myAlert(String title, String message,Context ctx, final String log,final Component comp, final String result){
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
		alertDialogBuilder.setTitle(title);
		for(int i=0; i<comp.getForm().getAllComponent().size(); i++){
			if(comp.getForm().getAllComponent().get(i).getName().equalsIgnoreCase(result.substring(1))){
				comp.getForm().getAllComponent().get(i).setText(log);
			}
		}
		
		Vector<String> a = Utility.splitVector(Global.getText("message.footer"), "|");
		String stepMessage = "";
		for(String b:a){
			if(stepMessage ==""){
				stepMessage = stepMessage + b;
				}else{
					stepMessage= stepMessage + "\n"+b;
				}
		}		
		alertDialogBuilder.setMessage(Global.getText("message.header")+"\n"
				+message+"\n"
				+"\n"+				
				stepMessage)
				.setPositiveButton(Global.getText("message.button"), new DialogInterface.OnClickListener() {
//			Component resultComponent;
			@Override			
			public void onClick(DialogInterface dialog, int which) {														
//				resultComponent.setText(log);
//				for(int j=0; j<Generator.forms.size(); j++){
//					if(Generator.forms.get(j).getName().equalsIgnoreCase(getFormOrComponentName(result,1))){
//						for(int k=0; k<Generator.forms.get(j).getAllComponent().size();k++){
//							if(Generator.forms.get(j).getComponent(k).getName().equalsIgnoreCase(getFormOrComponentName(result,2))){
//								resultComponent=(Generator.forms.get(j).getComponent(k));
//							}
//						}
//					}
//				}
				
			}
		});
		
		if(message==null){			
			alertDialogBuilder.setMessage(Global.getText("message.complete"));
		}
		alertDialogBuilder.setCancelable(false);
		
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
		
	}
	
//	private static boolean isValid(String imageUrl,Context ctx) throws IOException, InterruptedException {		
//	    Bitmap imgYgAkanDicek = Image.OpenImage(ctx, imageUrl);
//	    Bitmap imgPembanding = Bitmap.createBitmap(imgYgAkanDicek.getWidth(), imgYgAkanDicek.getHeight(), Bitmap.Config.ARGB_8888);
//	    int i = imageUrl.indexOf("T");
//	    if (i>=1){
//	    	String a = Utility.getDefaultPath()+"imgTtdFalse.png";
//	    	imgPembanding =Image.OpenImage(ctx, a);
//	    }else{
//	    	int whiteColor = 0xFFFFFFFF; 
//		    Canvas canvas = new Canvas(imgPembanding);
//		    canvas.drawColor(whiteColor);
//	    }
//	    Boolean result = false;
//
//	    if (imgPembanding.sameAs(imgYgAkanDicek)) {
//	    	result =false;
//	    }else{
//	    	result =true;
//	    }
//	    
//	    return result;
//	}

}
