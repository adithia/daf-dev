package com.salesforce.generator.action;

import android.content.Context;
import android.content.Intent;

import com.daf.activity.LoginActivity;
import com.daf.activity.NewEntry;

public class Logout {

	public Logout(Context context){
		Intent intent = new Intent(context, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
}
