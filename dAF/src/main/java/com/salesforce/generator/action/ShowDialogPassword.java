package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class ShowDialogPassword implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		comp.getForm().showPasswordDialog();
		return false;
	}

}
