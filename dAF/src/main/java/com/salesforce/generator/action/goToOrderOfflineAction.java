package com.salesforce.generator.action;

import android.content.Intent;

import com.daf.activity.DAFMenu;
import com.daf.activity.LoginActivity;
import com.daf.activity.TaskListOfflineActivity;
import com.daf.activity.Tasklist;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class goToOrderOfflineAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		Intent intent;
		if(Generator.isLogin){
			intent = new Intent(comp.getForm().getActivity(), TaskListOfflineActivity.class);
			
		}else{
			intent = new Intent(comp.getForm().getActivity(), LoginActivity.class);
		}
		
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		comp.getForm().getActivity().startActivity(intent);
		return false;
	
	}

}
