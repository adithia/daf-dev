package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SetTextAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		//tambahan untuk kegalauan daf 05/02/2015
		/*
		if (data.getText("param3").startsWith("@FINDER")) {
			if (data.getText("param3").contains("(")&&data.getText("param3").contains(")")) {
				String key = data.getText("param3").substring(data.getText("param3").indexOf("(")+1);
				if (key.indexOf(")")>=0) {
					key = key.substring(key.indexOf(")"));
				}
				int index = Utility.getInt(key);
				String[] sarr = Utility.split(ShowFinderAction.FINDER_RESULT, "\t");
				if (sarr.length>index) {
					comp.getForm().setFormComponentText(data.getText("result"), sarr[index]);
				}else{
					comp.getForm().setFormComponentText(data.getText("result"), "");
				}
			}else{
				comp.getForm().setFormComponentText(data.getText("result"), ShowFinderAction.FINDER_RESULT);
			}
			return true;
		}
		*/
		
		//original function
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		comp.getForm().setFormComponentText(data.getText("result"), param3);
		return true;
	}

}
