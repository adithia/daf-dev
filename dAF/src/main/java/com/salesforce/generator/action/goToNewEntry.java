package com.salesforce.generator.action;

import android.content.Intent;

import com.daf.activity.NewEntry;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class goToNewEntry implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(comp.getForm().getActivity(), NewEntry.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		comp.getForm().getActivity().startActivity(intent);
		return false;
	}

}
