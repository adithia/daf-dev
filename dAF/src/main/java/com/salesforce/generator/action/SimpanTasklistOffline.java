package com.salesforce.generator.action;

import java.util.Vector;

import android.content.DialogInterface;
import android.content.Intent;

import com.daf.activity.LoginActivity;
import com.daf.activity.TaskListOfflineActivity;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.component.IWillSave;
import com.salesforce.database.Connection;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class SimpanTasklistOffline implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
				if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSave) {
					((IWillSave)Generator.forms.elementAt(i).components.elementAt(j)).onWillSave();
				}
			}
		}
		String namaLengkap = comp.getForm().getFormComponentText("$F91.C103F91"); 
		String applNo =  comp.getForm().getFormComponentText("$F91.C101F91"); 
		
		Connection.DBupdate("trn_tasklist_offline","applno="+applNo,"cust_name="+namaLengkap, "data="+Generator.currform.getDataAll(),"activityId="+Generator.currModelActivityID,"where","orderId="+Generator.currOrderCode);
		
		if (Utility.activity != null) {
			if (Utility.activity instanceof TaskListOfflineActivity) {
				TaskListOfflineActivity sd = (TaskListOfflineActivity) Utility.activity;
				sd.refresh();
			}
		}
		final Component comps= comp;
		AdapterDialog.showDialogOneBtn(comp.getForm().getActivity(), "Informasi", "Data berhasil disimpan", "Ok", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {								
				if(Generator.isLogin == true){
//					intent = new Intent(comps.getForm().getActivity(), TaskListOfflineActivity.class);
					comps.getForm().getActivity().finish();					
				}else{
					Intent intent = new Intent(comps.getForm().getActivity(), LoginActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					comps.getForm().getActivity().startActivity(intent);	
				}											
			}
		});
		
		return false;
	}

}
