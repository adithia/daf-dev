package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SetMessageAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		String result = comp.getForm().getFormComponentText(data.getText("result"));
		comp.getForm().showInfo(result, param3);
		return true;
	}

}
