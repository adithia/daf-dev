package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class CalculatingCeil implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		
		try {
			String va = data.getText("param3");
			if (va.contains("$")||va.contains("@")) {
				va=exec(comp, va);
			}
			if (va.contains("$")||va.contains("@")) {
				va=exec(comp, va);
			}
			Recordset rst = Connection.DBquery("SELECT "+va+"  AS result");

			String r = rst.getText(0, "result");
			if (r.equals("")) {
				r = "0";
			}
			String p = data.getText("result");
			
			double flt = Math.ceil(Float.valueOf(r));
//			r = Double.toString(flt);

			int valueInteger = (int)flt;
			r = Integer.toString(valueInteger);
			
			if (rst.getRows()>=1) {			
				if (p.startsWith("$")||p.startsWith("@")) {
					comp.getForm().setFormComponentText(p, r);
				}
			}else{
				comp.getForm().setFormComponentText(p, "");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}		
		
		return true;
	}
	
	private String exec(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
		StringBuffer sbBuffer = new StringBuffer("");
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
					sbBuffer.append(comp.getForm().getFormComponentText(p)).append(" ");
				}else{
					sbBuffer.append(p).append(" ");
				}
			}
		}
		return sbBuffer.toString();
    }

}
