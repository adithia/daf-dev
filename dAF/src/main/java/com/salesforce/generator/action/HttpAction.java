package com.salesforce.generator.action;

import android.util.Log;
import android.widget.EditText;

import com.daf.activity.SettingActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

public class HttpAction implements IAction {

	@Override
	public boolean onAction(final Component comp,final SingleRecordset data) {
		Messagebox.showProsesBar(comp.getForm().getActivity(), new Runnable() {
			@Override
			public void run() {
				//Log.i("Action0", data.getText("param3"));
				String param3 = "";
				String x = data.getText("param3");
				if ( data.getText("param3").endsWith("SQL") ) {
					param3  = Utility.postMultipart(comp.getForm().getFormComponentText("@URL") , new String[] {  "userid" , "password", "sql"},
							new String[] { Utility.getSetting( comp.getForm().getActivity(), "USERNAME","") ,Utility.getSetting( comp.getForm().getActivity(), "PASSWORD","") , execJson(comp, comp.getForm().getFormComponentText("@SQL")  )  });
					//Log.i("Action1", param3);
					Generator.currResult=param3;
				}else{
					param3 = comp.getForm().getFormComponentText((execJson(comp, data.getText("param3"))));
					//Log.i("Action1", param3);
					Generator.currResult=Syncronizer.getHttp(param3);
				}				
				Log.i("Action", Generator.currResult);
				Generator.currRecordset = Stream.downStream(Generator.currResult);
			}
		}, new Runnable() {
			public void run() { 
				comp.getForm().setFormComponentText(data.getText("result"), Generator.currResult);
				comp.getForm().onActivityResult(1305, 2807, null);
			}
		});	
		return false;
	}
	private String execJson(Component comp, String s){
		if (s.startsWith("[")||s.startsWith("{")) {
			StringBuffer sbuBuffer = new StringBuffer();
			Nset nikiset = Nset.readJSON(s);
			for (int i = 0; i < nikiset.getArraySize(); i++) {
				if (nikiset.getData(i).getObjectKeys().length>=1) {
					if (nikiset.getData(i).getData("encode").getArraySize()>=1) {
						StringBuffer sBuffer = new StringBuffer();
						for (int j = 0; j < nikiset.getData(i).getData("encode").getArraySize(); j++) {
							sBuffer.append(filldata(comp, nikiset.getData(i).getData("encode").getData(j).toString()));
						}
						sbuBuffer.append(Utility.getURLenc("",filldata(comp, sBuffer.toString())));
					}else{
						sbuBuffer.append(Utility.getURLenc("",filldata(comp, nikiset.getData(i).getData("encode").toString())));
					}					
				}else{
					sbuBuffer.append(filldata(comp, nikiset.getData(i).toString()));
				}
				
			}
			return sbuBuffer.toString();
		}else{
			return filldata(comp,s);
		}		
	}
	private String filldata(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
}
