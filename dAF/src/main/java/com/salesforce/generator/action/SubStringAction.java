package com.salesforce.generator.action;

import java.util.StringTokenizer;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SubStringAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String vir1 = comp.getForm().getFormComponentText(data.getText("param1"));
		String vir3 = comp.getForm().getFormComponentText(data.getText("param3"));
		String value = "";
		if (vir3.length() > Integer.valueOf(vir1)) {
			value = vir3.substring(0, Integer.valueOf(vir1));
			
			StringTokenizer st = new StringTokenizer(vir3);
			StringTokenizer st2 = new StringTokenizer(value);
			StringBuffer sb = new StringBuffer();
			
			while (st2.hasMoreTokens()) {
				String val1 = st.nextToken();
				String val2 = st2.nextToken();
				if (val1.equals(val2)) {
					sb.append(val2 + " ");
				}
			}
			comp.getForm().setFormComponentText(data.getText("result"), sb.toString());
		}else{
			value = vir3;
			comp.getForm().setFormComponentText(data.getText("result"), value);
		}
			
				
		
		return true;
	}

}
