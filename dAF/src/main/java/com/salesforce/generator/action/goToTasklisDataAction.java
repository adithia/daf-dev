package com.salesforce.generator.action;

import android.content.Intent;

import com.daf.activity.TaskListDataActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class goToTasklisDataAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		Intent intent = new Intent(comp.getForm().getActivity(), TaskListDataActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		comp.getForm().getActivity().startActivity(intent);		
		return false;
	}

}
