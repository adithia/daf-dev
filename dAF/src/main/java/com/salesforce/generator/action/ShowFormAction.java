package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class ShowFormAction implements IAction {

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String va = comp.getForm().getFormComponentText(data.getText("param3")).trim();
		
		comp.getForm().show(va);
		return true;
	}

}
