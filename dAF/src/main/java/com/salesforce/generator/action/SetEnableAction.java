package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.ui.FinderItemUi;
import com.salesforce.generator.ui.TextFinder;
import com.salesforce.utility.Utility;

public class SetEnableAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		if (data.getText("result").equals("[ALL]")) {
			for (int i = 0; i < comp.getForm().getAllComponent().size(); i++) {
				comp.getForm().getComponent(i).setEnable(param3.equals("true")?true:false);
			}
		}else if (data.getText("result").startsWith("[") && data.getText("result").endsWith("]")) {
			Vector<String> vcmp = Utility.splitVector(data.getText("result").substring(1, data.getText("result").length()-1), ",");
			for (int i = 0; i < vcmp.size(); i++) {
				Component cmp =comp.getForm().getFormComponent(vcmp.elementAt(i).trim());
				if (cmp!=null) {
					cmp.setEnable(param3.equals("true")?true:false);
					if(cmp instanceof FinderItemUi){
						((FinderItemUi) cmp).btnClear.setEnabled(param3.equals("true")?true:false);
					}else if(cmp instanceof TextFinder){
//						TextFinder textFinder = (TextFinder) cmp;
						((TextFinder) cmp).btnClear.setEnabled(param3.equals("true")?true:false);
					}
				}
			}
		}

		else{
			Component cmp =comp.getForm().getFormComponent(data.getText("result"));
			if (cmp!=null) {
				if(cmp.getText().startsWith("[")&&cmp.getText().endsWith("]")){
					Vector<String> vcmp = Utility.splitVector(cmp.getText().substring(1, cmp.getText().length()-1), ",");
					for (int i = 0; i < vcmp.size(); i++) {
						Component abc =comp.getForm().getFormComponent(vcmp.elementAt(i).trim());
//						String cmpid = cmp.getName();
						if (abc!=null) {
							abc.setEnable(param3.equals("true")?true:false);
						}
					}
				}else{
					cmp.setEnable(param3.equals("true")?true:false);
				}
			}
		}		
		return true;
	}

}
