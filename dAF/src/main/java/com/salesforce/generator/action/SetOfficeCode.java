package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class SetOfficeCode implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		comp.getForm().setFormComponentText(data.getText("result"), Utility.getSetting(comp.currForm.getActivity(), "Office_Code", ""));
		return true;
	}

}
