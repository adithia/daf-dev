package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class BackAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		if(comp.getForm().getIndex()==0){
			if (Generator.currAppMenu.equalsIgnoreCase("order")) {
				new goToOrderAction().onAction(comp, data);
			}else if (Generator.currAppMenu.equalsIgnoreCase("newentry")) {
				new goToNewEntry().onAction(comp, data);
			}else if (Generator.currAppMenu.equalsIgnoreCase("pending")) {
				new goToPending().onAction(comp, data);
			}else if (Generator.currAppMenu.equalsIgnoreCase("draft")) {
				new goToDraft().onAction(comp, data);
			}else{
				new goToOrderAction().onAction(comp, data);
			}
		} else {
			comp.getForm().showBack();
		}
		return false;
	}

}
