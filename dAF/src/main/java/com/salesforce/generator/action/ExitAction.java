package com.salesforce.generator.action;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.daf.activity.LoginActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class ExitAction implements IAction{

		
	@SuppressWarnings("deprecation")
	@Override
	public boolean onAction(final Component comp, SingleRecordset data) {
		
//		comp.getForm().showMessage("Do You Want to Exit?", "Yes", "No", null, true);
		AlertDialog dialog = new AlertDialog.Builder(comp.getForm().getActivity()).create();
		dialog.setMessage("Do You Want to Exit?");
		dialog.setButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
			}
		});
		dialog.setButton2("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Utility.setExit(true);
				Intent intent = new Intent(comp.getForm().getActivity(), LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				comp.getForm().getActivity().startActivity(intent);
			}
		});
		
		dialog.show();
		return false;
	}

}
