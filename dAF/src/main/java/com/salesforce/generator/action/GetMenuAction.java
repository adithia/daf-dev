package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;

public class GetMenuAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String a =data.getText("result");
		comp.getForm().setFormComponentText(data.getText("result").toString(), Generator.currAppMenu);
		return true;
	}

}
