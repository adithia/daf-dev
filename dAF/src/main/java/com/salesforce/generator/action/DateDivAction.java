package com.salesforce.generator.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class DateDivAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		
		String param2 = comp.getForm().getFormComponentText(data.getText("param2")).trim();
		String param3 = "";
		long hasil;
		
		if (param2.contains("/")) {
			param3 = comp.getForm().getFormComponentText(data.getText("param3")).trim().replace("-", "/");			
		}else if (param2.contains("-")) {
			param3 = comp.getForm().getFormComponentText(data.getText("param3")).trim().replace("/", "-");
		}
		
		try {
			
			hasil = hasil(param2, param3);
			comp.getForm().setFormComponentText(data.getText("result"), String.valueOf(hasil));
			
		} catch (Exception e) {}
		
		return false;
	}
	
	@SuppressLint("SimpleDateFormat")
	public long hasil(String param1, String param2){
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyy");
		try{

		//Konversi dari string ke tanggal
			Date TanggalPinjam =df.parse(param1);
			Date TanggalKembali = df.parse(param2);

		//Tgl di konversi ke milidetik
			long Hari1 = TanggalPinjam.getTime();
			long Hari2 = TanggalKembali.getTime();
			long diff = Hari1 - Hari2;
			long Lama = diff / (24 * 60 * 60 * 1000);


			return Lama;
		} catch (ParseException e){}
		return 0;
	}

}
