package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class StringAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		for (int i = 0; i < Utility.getInt(comp.getForm().getFormComponentText("@string-split-length")); i++) {
			comp.getForm().setFormComponentText("@string-split-"+i,"");
		}
		
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		comp.getForm().setFormComponentText("@string-length", ""+param3.length());
		comp.getForm().setFormComponentText("@string-concat", param3 + comp.getForm().getFormComponentText("@concat1")+comp.getForm().getFormComponentText("@concat2")+comp.getForm().getFormComponentText("@concat3"));
		  
		
		comp.getForm().setFormComponentText("@string-ucase", param3.toUpperCase());
		comp.getForm().setFormComponentText("@string-lcase", param3.toLowerCase());
		
		String find = comp.getForm().getFormComponentText("@string-find");
		String replace = comp.getForm().getFormComponentText("@string-replace");
		if (find.length()>=1 && replace.length()>=1) {
			comp.getForm().setFormComponentText("@string-replace",param3.replace(find, replace));
		}else{
			comp.getForm().setFormComponentText("@string-replace","");
		}
		
		int start = Utility.getInt(comp.getForm().getFormComponentText("@string-substring-start"));
		int end =  Utility.getInt(comp.getForm().getFormComponentText("@string-substring-end"));
		if (end>=1) {
			try {
				comp.getForm().setFormComponentText("@string-substring",param3.substring(start, end));
			} catch (Exception e) {
				comp.getForm().setFormComponentText("@string-substring","");
			}
		}else{
			try {
				comp.getForm().setFormComponentText("@string-substring",param3.substring(start));
			} catch (Exception e) {
				comp.getForm().setFormComponentText("@string-substring","");
			}
		}
		
		
		String split = comp.getForm().getFormComponentText("@string-split");
		if (split.length()>=1) {
			Vector<String> v = Utility.splitVector(param3, comp.getForm().getFormComponentText("@string-split"));
			for (int i = 0; i < v.size(); i++) {
				comp.getForm().setFormComponentText("@string-split-"+i,v.elementAt(i));
			}
			comp.getForm().setFormComponentText("@string-split-length", ""+v.size());
		}
		
		comp.getForm().setFormComponentText("@string-substring-start","");
		comp.getForm().setFormComponentText("@string-substring-end","");
		
		comp.getForm().setFormComponentText("@string-find","");
		comp.getForm().setFormComponentText("@string-split","");
		comp.getForm().setFormComponentText("@concat1","");
		comp.getForm().setFormComponentText("@concat2","");
		comp.getForm().setFormComponentText("@concat3","");
		
		//currency
		String currency = comp.getForm().getFormComponentText("@currency");
		String currencystring = comp.getForm().getFormComponentText("@currency-string");
		int icurrency = Utility.getInt(comp.getForm().getFormComponentText("@currency-space"));
		
		if (icurrency!=0) {
			currency= Utility.formatCurrency(currency);
			currency=rightCcurrency(currency,icurrency);
			
			currencystring=rightCcurrency(currencystring,icurrency);
		}else{
			currency= Utility.formatCurrency(currency);
		}		
		
		comp.getForm().setFormComponentText("@currency",currency);
		comp.getForm().setFormComponentText("@currency-string",currencystring);
		comp.getForm().setFormComponentText("@currency-space","");
		
		comp.getForm().setFormComponentText(data.getText("result"), param3);
		return true;
	}
	private String rightCcurrency(String s, int max){
		s="                                          "+s;
		return s.substring(s.length()-max, s.length());
	}
}
