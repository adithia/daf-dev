package com.salesforce.generator.action;

import android.content.Intent;

import com.daf.activity.DAFMenu;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class goToPending implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		Intent intent = new Intent(comp.getForm().getActivity(), DAFMenu.class);
//		Intent intent = new Intent(comp.getForm().getActivity(), StatusOrder.class);
		intent.putExtra("To", "pending");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		comp.getForm().getActivity().startActivity(intent);
		return false;
	}

}
