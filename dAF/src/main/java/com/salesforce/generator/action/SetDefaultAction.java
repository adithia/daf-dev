package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class SetDefaultAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		//String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		if (data.getText("result").equals("[ALL]")) {
			for (int i = 0; i < comp.getForm().getAllComponent().size(); i++) {
				comp.getForm().getComponent(i).setText(comp.getForm().getComponent(i).fetDefault());
			}
		}else if (data.getText("result").startsWith("[") && data.getText("result").endsWith("]")) {
			Vector<String> vcmp = Utility.splitVector(data.getText("result").substring(1, data.getText("result").length()-1), ",");
			for (int i = 0; i < vcmp.size(); i++) {
				Component cmp =comp.getForm().getFormComponent(vcmp.elementAt(i).trim());
				if (cmp!=null) {
					cmp.setText(cmp.fetDefault());
				}
			}
		}else{
			Component cmp =comp.getForm().getFormComponent(data.getText("result"));
			if (cmp!=null) {
				cmp.setText(cmp.fetDefault());
			}
		}
		return true;
	}

}
