package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SetLabelAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		comp.getForm().setFormComponentLabel(data.getText("result"), param3);
		return true;
	}

}
