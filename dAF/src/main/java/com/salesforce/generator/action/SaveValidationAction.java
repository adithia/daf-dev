package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SaveValidationAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		if (new ValidationAction().onAction(comp, data)) {
			new SaveAction().onAction(comp, data);
			return true;
		}
		return false;
	}

}
