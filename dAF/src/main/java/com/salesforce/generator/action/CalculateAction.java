package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;

public class CalculateAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String va = data.getText("param3");
		if (va.contains("$")||va.contains("@")) {
			va=exec(comp, va);
		}
		if (va.contains("$")||va.contains("@")) {
			va=exec(comp, va);
		}
		
		// TODO ganti ini dengan classnya pak dewa
//		Recordset rst = Connection.DBquery("SELECT "+va+" AS result");
//		String r = rst.getText(0, "result");
		
		String p = data.getText("result");
		try {
			
			Calculable calc = new ExpressionBuilder(va).build();
			
			if (p.startsWith("$")||p.startsWith("@")) {
				
				String nilai = calc.calculate()+"";				
				boolean isHex = nilai.trim().matches("[0-9A-F]+");//^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$.
				
				if (isHex) {
					comp.getForm().setFormComponentText(p,Integer.parseInt(nilai, 16)+"");
				}else
					comp.getForm().setFormComponentText(p, nilai);
			}
			
		} catch (Exception e) {
			
			comp.getForm().setFormComponentText(p, "");//error 2.0050785E9

		}
		
		return true;
	}
	
    private String exec(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
		StringBuffer sbBuffer = new StringBuffer("");
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
					sbBuffer.append(comp.getForm().getFormComponentText(p)).append(" ");
				}else{
					sbBuffer.append(p).append(" ");
				}
			}
		}
		return sbBuffer.toString();
    
	}

}
