package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;

public class NoBackAction implements IAction {

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		comp.getForm().setOnBackListener(new Form.BackListener() {
			
			@Override
			public boolean onBackPressListener() {
				return false;
			}
		});
		return false;
	}

}
