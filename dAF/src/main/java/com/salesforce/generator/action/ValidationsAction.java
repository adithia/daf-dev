package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class ValidationsAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
     	
		if (data.getText("result").equals("[ALL]")) {
			for (int i = 0; i < Generator.forms.size(); i++) {
				String val = Generator.forms.elementAt(i).validation();
				val=val!=null?val:"";
				if (val.equals("")) {
				}else{
					comp.getForm().showMessage(val, true);
					return false;	
				}
			}
		}else if (data.getText("result").startsWith("[") && data.getText("result").endsWith("]")) {
			Vector<String> vcmp = Utility.splitVector(data.getText("result").substring(1, data.getText("result").length()-1), ",");
			for (int i = 0; i < vcmp.size(); i++) {
				Form form = getFormByName(vcmp.elementAt(i).trim());
				if (form!=null) {
					String val = form.validation();
					val=val!=null?val:"";
					if (val.equals("")) {
					}else{
						comp.getForm().showMessage(val, true);
						return false;	
					}
				}
			}
		}else{
			for (int i = 0; i <= comp.getForm().getIndex(); i++) {
				String val = Generator.forms.elementAt(i).validation();
				val=val!=null?val:"";
				if (val.equals("")) {
				}else{
					if (Generator.forms.elementAt(i).getIndex()!=comp.getForm().getIndex()) {
						val=Generator.forms.elementAt(i).getName() + "\r\n" + val;
					}
					comp.getForm().showMessage(val, true);
					return false;				
				}
			}	
		}		
		return true;

	}
	private boolean validation(Component comp, Form form){
		String val = form.validation();
		val=val!=null?val:"";
		if (val.equals("")) {
			return true;
		}
		comp.getForm().showMessage(val, true);
		return false;
	}
	private Form getFormByName(String fname){
		for (int i = 0; i < Generator.forms.size(); i++) {
			if (Generator.forms.elementAt(i).getName().equals(fname)) {
				return Generator.forms.elementAt(i);
			}
		}
		return null;
	}
}

