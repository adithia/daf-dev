package com.salesforce.generator.action;

import java.util.HashMap;
import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.utility.Utility;

public class CalculateSQLAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String va = data.getText("param3");//rumus
//		String va2 = makeFormula(comp, va);
//		double result = 0;
//		try {
//            Calculable cc =  new ExpressionBuilder(va2).withVariables(loadKey(comp, va)).build();
//            result = cc.calculate();
//    		String p = data.getText("result");
//    		if (p.startsWith("$")||p.startsWith("@")) {
//				comp.getForm().setFormComponentText(p, result+"");
//			}
//        } catch (Exception ex) {
//        	ex.printStackTrace();
//        }
//		Math.ceil(2.2);
//		Math.floor(2.8);
		if (va.contains("$")||va.contains("@")) {
			va=exec(comp, va);
		}
		if (va.contains("$")||va.contains("@")) {
			va=exec(comp, va);
		}
		Recordset rst = Connection.DBquery("SELECT "+va+"  AS result");
		String r = rst.getText(0, "result");
		String p = data.getText("result");
		if (rst.getRows()>=1) {			
			if (p.startsWith("$")||p.startsWith("@")) {
				comp.getForm().setFormComponentText(p, r);
			}
		}else{
			comp.getForm().setFormComponentText(p, "");//error
		}		
		
		return true;
	}
	
    private String exec(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
		StringBuffer sbBuffer = new StringBuffer("");
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
					sbBuffer.append(comp.getForm().getFormComponentText(p)).append(" ");
				}else{
					sbBuffer.append(p).append(" ");
				}
			}
		}
		return sbBuffer.toString();
    }
    
    private String makeFormula(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
		StringBuffer sbBuffer = new StringBuffer("");
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
					sbBuffer.append("A"+i).append(" ");
				}else{
					sbBuffer.append(p).append(" ");
				}
			}
		}
		return sbBuffer.toString();
    }
    
    private HashMap loadKey(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
    	HashMap map = new HashMap();
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
//					map.put(p, comp.getForm().getFormComponentText(p));
					map.put("A"+i, comp.getForm().getFormComponentText(p));
				}
			}
		}
		return map;
    }
}
