package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.route.RouteClass;

public class RoutingAction implements IAction{
	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		//Log.i("Action","RoutingAction:"+data.getText("param3"));
		Component cmp =comp.getForm().getFormComponent(data.getText("param3"));
		if (cmp!=null) {
			//Log.i("Action","RoutingAction");
			cmp.runRoute();
		}else if (data.getText("param3").startsWith("#")) {
			//Log.i("Action","runLocalRouteClass");
			return runLocalRouteClass(data.getText("param3").substring(1), comp, data);
		}
		return true;
	}
	private static boolean runLocalRouteClass(String servlet, Component comp, SingleRecordset data  ){
        try {
        	//Log.i("Action","Class.forName runLocalRouteClass");
        	RouteClass newServlet = (RouteClass) Class.forName("com.salesforce.route."+servlet).newInstance();
        	return newServlet.runRoute(comp, data);
        } catch (Exception e) { 
        	e.printStackTrace();
        }
        return false;
    }
}
