package com.salesforce.generator.action;

import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import android.app.ProgressDialog;
import android.content.Intent;

import com.daf.activity.FinderActivity;
import com.daf.activity.FinderItemActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.Utility;

public class ShowFinderItemAction implements IAction{
	public static String FINDER_RESULT = "";
	private static String[] vaStrings;
	 
	public boolean onAction(Component comp, SingleRecordset data) {		
		showFinderActivity(comp, comp.getForm().getFormComponentText(data.getText("param1")).trim(), 
				comp.getForm().getFormComponentText(data.getText("param2")).trim() ,
				comp.getForm().getFormComponentText(data.getText("param3")).trim(),
				comp.getForm().getFormComponentText(data.getText("result")).trim());
		return true;
	}
	
	public void showFinderActivity(Component comp, String p1, String p2, String p3, String r){
		try {
			Utility.comp = comp;
			String query = "";
			vaStrings = Utility.split(p3, "#");
			Vector<String> queries = new Vector<String>();
			if (vaStrings[0].startsWith("[")) {			
				Nset ns =Nset.readJSON(vaStrings[0]);
				queries = Utility.splitVector(ns.getData(0).toString(), "?");
				for (int i = 0; i < ns.getArraySize(); i++) {
//					String compxz = ns.getData(i+1).toString();
					if (i == 0) {
						if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
							query = queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
						}else if(queries.elementAt(i).toString().contains("=")){
							query = queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
						}else if(queries.elementAt(i).toString().contains("NOT IN")){
							Nset nComp = Nset.readJSON(ns.getData(i+1).toJSON());
							query = queries.elementAt(i).toString();
							for (int j = 0; j < nComp.getArraySize(); j++) {
								String strCom = getDataComp(comp, nComp.getData(j).toString());
								if (j == 0) {
//									if (!strCom.equalsIgnoreCase("")) {
//										query =  query + "('%" + StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "%',";
//									}else{
										query =  query + "('" + StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "',";
//									}
									
								}else if (j == (nComp.getArraySize()-1)) {
//									if (!strCom.equalsIgnoreCase("")) {
//										query = query + "'%" +StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "%')";
//									}else{
										query = query + "'" +StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "')";
//									}
									
								}else {
//									if (!strCom.equalsIgnoreCase("")) {
//										query = query + "'%"+StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "%',";
//									}else{
										query = query + "'"+StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "',";
//									}
									
								}
							}
						}else if (queries.elementAt(i).toString().contains("&&")) {
							query = query + queries.elementAt(i).toString().replace("&&", "=");
						}else {
							query = queries.elementAt(i).toString();
						}
					}else					
						if (queries.elementAt(i).toString().contains("LIKE") || queries.elementAt(i).toString().contains("like")) {
							query = query + queries.elementAt(i).toString() + "'%" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "%'";	
						}else if(queries.elementAt(i).toString().contains("NOT IN")){
							Nset nComp = Nset.readJSON(ns.getData(i+1).toJSON());
							query = query + queries.elementAt(i).toString();
							for (int j = 0; j < nComp.getArraySize(); j++) {
								String strCom = getDataComp(comp, nComp.getData(j).toString());
								if (j == 0) {
//									if (!strCom.equalsIgnoreCase("")) {
//										query =  query + "('%" + StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "%',";
//									}else{
										query =  query + "('" + StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "',";
//									}
									
								}else if (j == (nComp.getArraySize()-1)) {
//									if (!strCom.equalsIgnoreCase("")) {
//										query = query + "'%" +StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "%')";
//									}else{
										query = query + "'" +StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "')";
//									}
									
								}else {
//									if (!strCom.equalsIgnoreCase("")) {
//										query = query + "'%"+StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "%',";
//									}else{
										query = query + "'"+StringEscapeUtils.escapeSql(getDataComp(comp, nComp.getData(j).toString())).trim() + "',";
//									}
									
								}
							}
						}else if(queries.elementAt(i).toString().contains("=")){
							query = query + queries.elementAt(i).toString() + "'" + StringEscapeUtils.escapeSql(getDataComp(comp, ns.getData(i+1).toString())).trim() + "'";
						}else if (queries.elementAt(i).toString().contains("&&")) {
							query = query + queries.elementAt(i).toString().replace("&&", "=");
						}else {
							query = query + queries.elementAt(i).toString();
						}
				}
				
			}else {
				query = vaStrings[0];
			}
			
			if (p1.equals("noshowfinder")) {
				Recordset data = Connection.DBquery(query);
				if (data.getRows() > 0) {				
					
					Vector<String> val = data.getAllDataVector().elementAt(0);
					String[] values = new String[val.size()];
					for (int i = 0; i < val.size(); i++) {
						String value = val.get(i);
						values[i] = value;
					}
					Intent i = new Intent();		
					i.putExtra("ROW", 0);
					i.putExtra("FIELDS", values);
					i.putExtra("madatory", "");
					i.putExtra("PARAM4", r);
					comp.getForm().onActivityResult(0, FinderActivity.RESULT_FINDER, i);
				}
				
			}else{

				Intent intent = new Intent(comp.getForm().getActivity(), FinderItemActivity.class);
				intent.putExtra("PARAM1", p1);
				intent.putExtra("PARAM2", p2);		
				intent.putExtra("PARAM3", query);	
				intent.putExtra("PARAM4", r );
				if (vaStrings.length > 1) {
					intent.putExtra("query", vaStrings[1]);				
				}
				comp.getForm().getActivity().startActivityForResult(intent, FinderActivity.REQUEST_FINDER);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/*private String execJson(Component comp, String s){
		if (s.startsWith("[")||s.startsWith("{")) {
			StringBuffer sbuBuffer = new StringBuffer();
			Nset nikiset = Nset.readJSON(s);
			for (int i = 0; i < nikiset.getArraySize(); i++) {
				  if (nikiset.getData(i).getType()==Nset.NTYPE_OBJECT) {
					  sbuBuffer.append(getDataComp(comp, s)) ;
				  }else{
					  sbuBuffer.append(getDataComp(comp, nikiset.getData(i).toString()));
				  }
			}
			return sbuBuffer.toString();
		}else{
			return getDataComp(comp, s);
		}		
	}*/
	private String getDataComp(Component comp, String s){
		if (s.startsWith("$")||s.startsWith("@")) {
			return comp.getForm().getFormComponentText(s);
		}else{
			return s;
		}
	}
	/*private String exec(Component comp, String s){
    	Vector<String> paStrings = Utility.splitVector(s, " ");
		StringBuffer sbBuffer = new StringBuffer("");
		for (int i = 0; i < paStrings.size(); i++) {
			String p = paStrings.elementAt(i).trim();
			
			if (p.length()>=1) {
				if (p.startsWith("$")||p.startsWith("@")) {
					sbBuffer.append(comp.getForm().getFormComponentText(p)).append(" ");
				}else{
					sbBuffer.append(p).append(" ");
				}
			}
		}
		return sbBuffer.toString();
    }*/

}
