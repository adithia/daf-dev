package com.salesforce.generator.action;

import android.content.Context;
import android.util.Log;

import com.daf.activity.OrderPendingActivity;
import com.daf.activity.SettingActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.component.IWillSend;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.service.SendPendingImage;
import com.salesforce.utility.Messagebox;
import com.salesforce.utility.Utility;

import java.io.File;

public class SendAction implements IAction {
    private String resSentAction;

    public static String sendActivity(Context context, String activityId, String orderCode, String appId, String data, boolean service) {
        String result = "";
        if (Generator.isTasklist) {
            result = Utility.postMultipartConnection(SettingActivity.URL_SERVER + "activityservlet/" + (service ? "?f=service" : ""),
                    "lat=" + Utility.latitude, "lon=" + Utility.longitude, "activityId=" + activityId, "orderCode=" + orderCode, "appId=" + appId,
                    "user=" + Utility.getSetting(context, "userid", "").trim(), "applno=" + Generator.currOrderCode, "branchid=" + Generator.branchid, "data=" + data, "imei=" + Utility.getImei(context), "token=" + Utility.getSetting(Utility.getAppContext(), "Token", ""));
        } else {
            result = Utility.postMultipartConnection(SettingActivity.URL_SERVER + "activityservlet/" + (service ? "?f=service" : ""),
                    "lat=" + Utility.latitude, "lon=" + Utility.longitude, "activityId=" + activityId, "orderCode=" + orderCode, "appId=" + appId,
                    "user=" + Utility.getSetting(context, "userid", "").trim(), "data=" + data, "imei=" + Utility.getImei(context), "token=" + Utility.getSetting(Utility.getAppContext(), "Token", ""));
        }
        return result;
    }

    public static String sendActivityBackground(Context context, String activityId, String orderCode, String appId, String data, String isTasklist, String applNo, String branchId, boolean service) {
        String result = "";
        if (isTasklist.equalsIgnoreCase("true")) {
            result = Utility.postMultipartConnection(SettingActivity.URL_SERVER + "activityservlet/" + (service ? "?f=service" : ""),
                    "lat=" + Utility.latitude, "lon=" + Utility.longitude, "activityId=" + activityId, "orderCode=" + orderCode, "appId=" + appId,
                    "user=" + Utility.getSetting(context, "userid", "").trim(), "applno=" + applNo, "branchid=" + branchId, "data=" + data, "imei=" + Utility.getImei(context), "token=" + Utility.getSetting(Utility.getAppContext(), "Token", ""));
        } else {
            result = Utility.postMultipartConnection(SettingActivity.URL_SERVER + "activityservlet/" + (service ? "?f=service" : ""),
                    "lat=" + Utility.latitude, "lon=" + Utility.longitude, "activityId=" + activityId, "orderCode=" + orderCode, "appId=" + appId,
                    "user=" + Utility.getSetting(context, "userid", "").trim(), "data=" + data, "imei=" + Utility.getImei(context), "token=" + Utility.getSetting(Utility.getAppContext(), "Token", ""));
        }
        return result;
    }

    public static void deleteSentActity(int imax) {
        deleteSentActity(imax, "");
    }

    public static void deleteSentActity(int imax, String date) {
        Utility.i("deleteSentActity", "sendActivity:" + imax);
//		if (imax>=0) {
        String[] s = new File(Utility.getDefaultPath()).list();
        StringBuffer sbuf = new StringBuffer("|");
        if (s != null) {
            for (int i = 0; i < s.length; i++) {
                sbuf.append(s[i]).append("|");
            }
        }
        String buffer = sbuf.toString();
        Recordset rst = Connection.DBquery("select * from data_activity where  status = '1' ORDER BY activityId ASC;");//appId = '" + Generator.currApplication + "'and
        Utility.i("deleteSentActity", "r:" + rst.getRows());


        for (int i = 0; i < rst.getRows(); i++) {
            Utility.i("deleteSentActity", "sendActivity:" + rst.getText(i, "activityid") + ":" + rst.getText(i, "ndate"));
            if (date.equals("") || (!date.equals(rst.getText(i, "ndate")))) {

//					deleteSentActityResource(buffer, rst.getText(i, "data"));

                Connection.DBdelete("data_activity", " activityId =? ", new String[]{rst.getText(i, "activityid")});
                Connection.DBdelete("data_activity", "activityId=" + rst.getText(i, "activityid"));
            }
        }
//		}
    }

    public static void deleteActivityTasklistTemporary() {
        String[] s = new File(Utility.getDefaultPath()).list();
        StringBuffer sbuf = new StringBuffer("|");
        if (s != null) {
            for (int i = 0; i < s.length; i++) {
                sbuf.append(s[i]).append("|");
            }
        }
        String buffer = sbuf.toString();
        Recordset rst = Connection.DBquery("select * from data_activity where  status = '1' ORDER BY activityId ASC;");
        for (int i = 0; i < rst.getRows(); i++) {

//			deleteSentActityResource(buffer, rst.getText(i, "data"));
            //delete fingerprint img di tabel
            Connection.DBdelete("TBL_PATH_Finger", " activityId =? ", new String[]{rst.getText(i, "activityid")});

            //delete log activity
            Connection.DBdelete("data_activity", " activityId =? ", new String[]{rst.getText(i, "activityid")});
            Connection.DBdelete("data_activity", "activityId=" + rst.getText(i, "activityid"));
        }
    }

    @Override
    public boolean onAction(final Component comp, final SingleRecordset data) {
        final String param3 = data.getText("param3");
        if (new ValidationAction().onAction(comp, data)) {
            Messagebox.showProsesBar(comp.getForm().getActivity(), new Runnable() {
                @Override
                public void run() {

                    resSentAction = sendActivity(comp.getForm().getActivity(), Generator.currModelActivityID, Generator.currOrderCode, Generator.currApplication, comp.getForm().getDataAll(), false);

                    if (param3.contains("autoinfo")) {
                        if (resSentAction.endsWith("OK")) {
                            if (!param3.contains("noimage")) {
                                for (int i = 0; i < Generator.forms.size(); i++) {
                                    for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                                        if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSend) {
//                                                    String sds = Generator.forms.elementAt(i).components.elementAt(j).getName();
                                            if (!((IWillSend) Generator.forms.elementAt(i).components.elementAt(j)).onWillSend()) {
                                                Log.d("seno", "run: selesai kirim satu doc");
//														resSentAction = "Upload Image  ";
                                            }
//                                                    Log.d("seno", "run: selesai kirim satu doc");
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }, new Runnable() {
                public void run() {
				/*	if (!param3.contains("noupdate")) {
						if (resSentAction.endsWith("OK")) {
							Connection.DBupdate("data_activity","status=1" , "where" ,"activityId="+Generator.currModelActivityID);
							deleteSentActity();
						}
					}*/
                    if (param3.contains("autoinfo")) {
                        if (resSentAction.endsWith("OK")) {
//                            Log.d("seno", "run: setelah kirim  ok");
                            //14/12/2018
                            Messagebox.showProsesBar(comp.getForm().getActivity(), new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    if (!param3.contains("noimage")) {
//                                      seno 09:12 09/12/19
                                        /*for (int i = 0; i < Generator.forms.size(); i++) {
                                            for (int j = 0; j < Generator.forms.elementAt(i).components.size(); j++) {
                                                if (Generator.forms.elementAt(i).components.elementAt(j) instanceof IWillSend) {
//                                                    String sds = Generator.forms.elementAt(i).components.elementAt(j).getName();
                                                    if (!((IWillSend) Generator.forms.elementAt(i).components.elementAt(j)).onWillSend()) {
                                                        Log.d("seno", "run: selesai kirim satu doc");
//														resSentAction = "Upload Image  ";
                                                    }
//                                                    Log.d("seno", "run: selesai kirim satu doc");
                                                }
                                            }
                                        }*/
                                    }

                                    //untuk menjalankan kembali backround SendPendingImage
//                                  seno 09:12 09/12/19
                                    //seno 17:12 19/02/2019
//                                    SendPendingImage.uiActive = false;
//									LostImageService.uiActive = false;
                                    //end
                                }
                            }, new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    //17:34 04/01/2019 seno
                                    if (!param3.contains("noupdate")) {
                                        if (resSentAction.endsWith("OK")) {
                                            Connection.DBupdate("data_activity", "status=1", "where", "activityId=" + Generator.currModelActivityID);
                                            deleteSentActity();
                                        }
                                    }
                                    Connection.DBupdate("data_activity", "status=1", "where", "activityId=" + Generator.currModelActivityID);
                                    deleteActivityTasklistTemporary();
                                    //log_order it merupakan tasklist
                                    if (Generator.isTasklist) {
                                        Connection.DBupdate("log_order", "sent=Y", "where", "order_id=" + Generator.currOrderCode);
                                        Connection.DBdelete("trn_task_list", "order_id=?", new String[]{Generator.currOrderCode});
                                        Connection.DBdelete("log_order", "order_id=?", new String[]{Generator.currOrderCode});
//										Utility.deleteFile(Utility.getDefaultImagePath()+"/"+Generator.currModelActivityID+".C31F32_4_FP.imgicon.png");
                                    }

                                    if (Generator.currOrderType.equalsIgnoreCase("tasklistdata")) {
                                        Connection.DBdelete("trn_tasklist_data", "order_id=?", new String[]{Generator.currOrderCode});
                                        Connection.DBupdate("log_order", "sent=Y", "where", "order_id=" + Generator.currOrderCode);
                                        Connection.DBdelete("trn_tasklist_data_object", "order_id=?", new String[]{Generator.currOrderCode});
                                    }

                                    //seno 10:04 04/04/2019
                                    //untuk menghapus tasklist offline
                                    if (Generator.currOrderType.equalsIgnoreCase("tasklistoffline")) {
                                        Connection.DBupdate("trn_tasklist_offline", "sent=Y", "where", "orderId=" + Generator.currOrderCode);
                                        Connection.DBdelete("trn_tasklist_offline", "orderId=?", new String[]{Generator.currOrderCode});
                                    }

                                    String[] imagesFiles = new File(Utility.getDefaultImagePath()).list();
                                    for (String a : imagesFiles) {
                                        if (a.contains("_4_FP.imgicon.png")) {
                                            if (a.contains(Generator.currModelActivityID)) {
                                                String filePath = Utility.getDefaultImagePath() + "/" + a;
                                                Utility.deleteFile(filePath);
                                                File abc = new File(filePath);
                                                if (!abc.exists()) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
//										Utility.deleteFile(Utility.getDefaultImagePath()+"/"+Generator.currModelActivityID+".C8F40_4_FP.imgicon.png");
                                    new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Berhasil Terkirim", "result=234352:OK"));
                                }
                            });
                            //14/12/2018
                            // 17:34 04/01/2019
//							Connection.DBupdate("data_activity","status=1" , "where" ,"activityId="+Generator.currModelActivityID);
//							deleteActivityTasklistTemporary();
//
//							//log_order it merupakan tasklist
//							if (Generator.isTasklist) {
//								Connection.DBupdate("log_order","sent=Y", "where", "order_id="+Generator.currOrderCode);
//								Connection.DBdelete("trn_task_list", "order_id=?", new String[]{Generator.currOrderCode});
//								Connection.DBdelete("log_order", "order_id=?", new String[]{Generator.currOrderCode});
//							}

                        } else if (resSentAction.startsWith("1670")) {
                            new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Pengiriman Data Gagal, \r\n" + resSentAction, "result=1670:OK"));
//                                  seno 09:12 09/12/19
//                            SendPendingImage.uiActive = false;
//							LostImageService.uiActive = false;
                        }
                        //13:02 14/11/2018 daff 3
                        else if (resSentAction.startsWith("data_stream_rusak")) {
                            if (Generator.isTasklist == false) {
                                //new entry
                                if (Generator.currAppMenu.equalsIgnoreCase("pending")) {
                                    OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
                                    new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data yang di kirim gagal silahkan entry ulang", "result=234352:OK"));
                                } else {
                                    OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
                                    new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data yang di kirim gagal silahkan entry ulang", "result=234352:OK"));
                                }
//                                  seno 09:12 09/12/19
//                                SendPendingImage.uiActive = false;
//								LostImageService.uiActive = false;
                            } else {
                                OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
                                new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data yang di kirim gagal silahkan entry ulang", "result=234352:OK"));
//                                  seno 09:12 09/12/19
//                                SendPendingImage.uiActive = false;
//								LostImageService.uiActive = false;
                            }
                        }
                        //sampai sini
                        else {
                            new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Gagal, Masalah Koneksi\r\n" + resSentAction, "result=234352:Simpan"));
//                                  seno 09:12 09/12/19
//                            SendPendingImage.uiActive = false;
//						LostImageService.uiActive = false;
                        }
                    } else {
                        comp.getForm().setFormComponentText(data.getText("result"), resSentAction);
                        comp.getForm().onActivityResult(2807, 1305, null);
//                         seno 09:12 09/12/19
//                        SendPendingImage.uiActive = false;
//						LostImageService.uiActive = false;
                    }
                }
            });
        }
        return false;
    }

    private void deleteSentActity() {
        int imax = Global.getInt("sentactivity.maxshow");
        deleteSentActity(imax);
    }


}
