package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class SubStringAction3 implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		String StringToSubstring = comp.getForm().getFormComponentText(data.getText("param3"));
		String index1 = comp.getForm().getFormComponentText(data.getText("param1"));
		String index2 = comp.getForm().getFormComponentText(data.getText("param2"));
		String hasil = "";
		
		try{
			hasil = StringToSubstring.substring(Integer.parseInt(index1), Integer.parseInt(index2));
			comp.getForm().setFormComponentText(data.getText("result"), hasil);
		}catch(Exception e){
			comp.getForm().setFormComponentText(data.getText("result"), hasil);
		}
		
		return true;
	}

}
