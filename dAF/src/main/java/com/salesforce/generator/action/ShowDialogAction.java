package com.salesforce.generator.action;

import java.util.Vector;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Form;
import com.salesforce.utility.Utility;

public class ShowDialogAction implements IAction {

	@Override
	public boolean onAction(final Component comp, final SingleRecordset data) {
		String param3 = comp.getForm().getFormComponentText(data.getText("param3"));
		String result = comp.getForm().getFormComponentText(data.getText("result"));
		//01:OK|CANCEL|YES
		int reqcode = 0;
		if (result.indexOf(":")>=1) {
			reqcode = Utility.getInt(result.substring(0,result.indexOf(":")));
			result=result.substring(result.indexOf(":")+1,result.length());
		} 
		final Vector<String> tbl = Utility.splitVector(result+"||", "|");
		comp.getForm().showMessage(param3, tbl.elementAt(0).trim(), tbl.elementAt(1).trim(), tbl.elementAt(2).trim(), false, reqcode, new Form.ButtonMessageClickListener() {
			public void onButtonMessageClick(String button, int reqcode) {
				comp.getForm().onActivityResult(reqcode, tbl.indexOf(button), null);					
			}
		});
		return false;
	}
	

}
