package com.salesforce.generator.action;

import android.content.Intent;

import com.daf.activity.AddItemActivity;
import com.daf.activity.FinderActivity;
import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class AddItemAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		showFinderItem(comp, comp.getForm().getFormComponentText(data.getText("param1")).trim(), 
				comp.getForm().getFormComponentText(data.getText("param2")).trim() ,
				comp.getForm().getFormComponentText(data.getText("param3")).trim(),
				comp.getForm().getFormComponentText(data.getText("result")).trim());
		return true;
	}
	
	private void showFinderItem(Component comp, String p1, String p2, String p3, String r){
		Intent intent = new Intent(comp.getForm().getActivity(), AddItemActivity.class);
		intent.putExtra("PARAM1", p1);
		intent.putExtra("PARAM2", p2);		
		intent.putExtra("PARAM3", p3);	
		intent.putExtra("PARAM4", r );
		comp.getForm().getActivity().startActivityForResult(intent, 1);
	}

}
