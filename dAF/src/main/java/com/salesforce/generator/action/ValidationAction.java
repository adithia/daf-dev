package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;

public class ValidationAction implements IAction{

	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		String val = comp.getForm().validation();
		val=val!=null?val:"";
		if (val.equals("")) {
			return true;
		}
//		comp.getForm().showMessage(val, true);
		comp.getForm().showDialogMessage(val);
		return false;
	}

}
