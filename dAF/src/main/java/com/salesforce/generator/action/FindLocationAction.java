package com.salesforce.generator.action;

import com.salesforce.component.Component;
import com.salesforce.component.IAction;
import com.salesforce.database.SingleRecordset;
import com.salesforce.generator.Generator;
import com.salesforce.utility.GPSTracker;
import com.salesforce.utility.Utility;

public class FindLocationAction implements IAction{

	private GPSTracker gpsTracker;
	
	@Override
	public boolean onAction(Component comp, SingleRecordset data) {
		// TODO Auto-generated method stub
		gpsTracker = new GPSTracker(comp.currForm.getActivity());
		String componentTarget = data.getText("result");
		if (gpsTracker.canGetLocation()) {
			if (gpsTracker.getLatitude() == 0.0 && gpsTracker.getLongitude() == 0.0) {
				comp.getForm().setFormComponentText(componentTarget,Utility.latitude + ","+Utility.longitude);
			}else{
				comp.getForm().setFormComponentText(componentTarget,gpsTracker.getLatitude() + ","+gpsTracker.getLongitude());
			}
			
		}else{
			comp.getForm().setFormComponentText(componentTarget,Utility.latitude + ","+Utility.longitude);
		}
		removeValueComponent(componentTarget);
		
		return true;
	}
	
	private void removeValueComponent(String comp){
		for (int i = 0; i < Generator.forms.size(); i++) {
			for (int c = 0; c < Generator.forms.elementAt(i).components.size(); c++) {
				if (Generator.forms.elementAt(i).components.elementAt(c).getName().equalsIgnoreCase(comp.substring(1))) {
					Generator.forms.elementAt(i).components.elementAt(c).setFocus();
				}
			}
			
		}
	}

}
