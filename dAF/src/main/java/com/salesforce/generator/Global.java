package com.salesforce.generator;

import java.util.HashMap;
import java.util.Hashtable;

import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.utility.Utility;

public class Global {
//	public static final String Image_Width_max = "image.width.max";//540
//	public static final String Image_Compress = "image.compress";//false
//	public static final String Image_Compress_Ratio = "image.compress.ratio";//80
//	public static final String Image_Format = "image.format";//jpg
//	public static final String Image_Show_View_OnClick = "image.show.view.onclick";//false
//	
//	
//	public static final String GPS_Track_Interval = "gps.track.interval";//6000(10menit)
//	
//	public static final String Order_Delete_On_Download = "order.delete.on.download";//no
//	public static final String Order_Activity_Count = "order.activity.count";//1 (1 order, 1 activiti)
//	
//	public static final String Activity_Start_Work_Time =  "activity.start.work.time";//00:00
//	public static final String Activity_Stop_Work_Time =  "activity.stop.work.time";//23:59
//	
//	public static final String Splash =  "splash.image.url";//gambar splash isi nya http
//	public static final String Splash_Amount =  "splash.timer.amount";//5 detik
//	public static final String Splash_Active =  "splash.active.state";//true
//	
//	public static final String Login_GPS_Active = "login.gps.active"; //1
//	public static final String Login_Bluetooth_Active = "login.bluetooth.active"; //0
//	public static final String Login_Wifi_Active = "login.wifi.active"; //0
//	public static final String Login_Setting_Active = "login.setting.active"; //1
//	
//	public static final String Order_List = "order.market.list";//contoh anak|bapak
//	public static final String Order_List_Priority = "order.market.list.priority";//Query sql
//	public static final String Order_List_NonPriority = "order.market.list.nonpriority";//Query sql
//	public static final String Order_List_Data_View = "order.market.list.data.view";//Query sql
//	public static final String Order_Download = "order.market.download";//0 / 1
	
	private static HashMap<String, String> newInitDef = new  HashMap<String, String>();
	public static HashMap<String, String> newInit(Recordset rst){
		newInitDef.clear();
		for (int i = 0; i < rst.getRows(); i++) {
			newInitDef.put(rst.getText(i, 0), rst.getText(i, 1));//code,value global_param
		}
		return newInitDef;
	}
	public static void newInit(){
		newInit(Connection.DBquery("SELECT code,value FROM global;"));
	}
	public static Hashtable<String, String> rowtocol(Recordset rst){
		Hashtable data = new Hashtable<String, String>();
		for (int i = 0; i < rst.getRows(); i++) {
			data.put(rst.getText(i, 0), rst.getText(i, 1));//code,value global_param
		}
		return data;
	}
	
	public static String getText(String colname, String def){
		if (newInitDef.get(colname)!=null) {
			return newInitDef.get(colname);
		}
		return def;
	}
	public static String getText(String colname){
		return getText(colname, "");
	}
	
	public static int getInt(String colname){
		return getInt(colname, 0);
	}
	public static int getInt(String colname, int def){
		return  Utility.getInt(getText(colname, def+""));
	}
}
