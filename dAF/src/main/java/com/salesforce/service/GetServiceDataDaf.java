package com.salesforce.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.salesforce.utility.Utility;

public class GetServiceDataDaf <T> extends AsyncTask<Void, Void, String> {

//	private int timeOut = 60000;	
	private ProgressDialog pDialog;	
	private Context context;
	private String url, message;
	
	public GetServiceDataDaf(Context context, String url, String message){
		this.context = context;
		this.url = url;
		this.message = message;
		
	}
	
	@Override
	protected void onPreExecute() {
		pDialog = Utility.showProgresbar(context, message);
		pDialog.setCancelable(false);
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		String result = Utility.getHttpConnection(url);		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result != null) {
			onResultListener(result);
		}else {
			onErrorResultListener(result);
		}
		
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}
	}
	
	public void onResultListener(String response) {

	}
	
	public void onErrorResultListener(String response) {
		
	}

}
