package com.salesforce.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.daf.activity.LoginActivity;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.database.Nset;
import com.salesforce.utility.Utility;

public class GetToken <T> extends AsyncTask<Void, Void, Boolean> {

//	private int timeOut = 60000;	
	private ProgressDialog pDialog;	
	private Context context;
	private String url, message;
	
	public GetToken(Context context, String url, String message){
		this.context = context;
		this.url = url;
		this.message = message;
		
	}
	
	@Override
	protected void onPreExecute() {
		pDialog = Utility.showProgresbar(context, message);
		pDialog.setCancelable(false);
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		String result = Utility.getHttpConnection2(url);	
		Nset set = Nset.readJSON(result);
		String token = set.getData("access_token").toString();
		String refresh_token = set.getData("refresh_token").toString();
		if (!token.equalsIgnoreCase("")) {
			Utility.setSetting(context, "Token", token);
			Utility.setSetting(context, "Refresh_Token", refresh_token);
			return true;
		}
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}
		
		if (result != null) {
			onResultListener(result);
		}else {
			onErrorResultListener(result);
		}
	}
	
	public void onResultListener(Boolean response) {

	}
	
	public void onErrorResultListener(Boolean response) {
		
	}


}
