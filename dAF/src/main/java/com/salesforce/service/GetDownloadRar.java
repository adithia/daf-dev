package com.salesforce.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Path;
import android.os.AsyncTask;

import com.salesforce.utility.Utility;

public class GetDownloadRar <T> extends AsyncTask<Void, Void, Boolean> {

//	private int timeOut = 60000;	
	private ProgressDialog pDialog;	
	private Context context;
	private String url, message, directory;
	
	public GetDownloadRar(Context context, String url, String message, String directory){
		this.context = context;
		this.url = url;
		this.message = message;
		this.directory = directory;
	}
	
	@Override
	protected void onPreExecute() {
		pDialog = Utility.showProgresbar(context, message);
		pDialog.setCancelable(false);
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		try {
			
			URL urlx = new URL(url);
			HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();
			
			httpConn.getHeaderFields();
			String contentencode = httpConn.getContentEncoding()!=null?httpConn.getContentEncoding().toLowerCase():"";
			if (httpConn.getResponseCode()!=304) { 
				
				InputStream is = httpConn.getInputStream();
				FileOutputStream fileOutputStream = new FileOutputStream(directory);
				
				
				byte[] buffer = new byte[1024];
		        int bufferLength = 0;
		        
				if (contentencode.contains("gzip")) {				
					GZIPInputStream gzipInputStream = new GZIPInputStream(is);				
			       
		            while((bufferLength = gzipInputStream.read(buffer))>0 ){
		                fileOutputStream.write(buffer, 0, bufferLength);
		            }
				}else{
		            while((bufferLength = is.read(buffer))>0 ){
		                fileOutputStream.write(buffer, 0, bufferLength);
		            }
				}
				 
		        
	            fileOutputStream.close();
	            

				if (new File(directory).exists()) {
					return true;
				}
			}	        
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			String sd = e.getMessage();
			e.printStackTrace();
		}		
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}
		
		if (result != null) {
			onResultListener(result);
		}else {
			onErrorResultListener(result);
		}
	}
	
	public void onResultListener(Boolean response) {

	}
	
	public void onErrorResultListener(Boolean response) {
		
	}

}
