package com.salesforce.service;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.salesforce.generator.Global;
import com.salesforce.utility.LocationOffline;

public class DAFReciever extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent arg1) {
		// TODO Auto-generated method stub
		
		//for get location
		new LocationOffline(context);		
		Intent ptIntent = new Intent(context, SchedulerService.class);
		//17/12/2018
        context.startService(ptIntent);
	}
	
	public static void sendOrder(Context context){
		Intent intent = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 60*1000, recurringDownload);
        // 1 hari = 24*60*60*1000
        if (Global.getText("activity.scheduller.send", "").equalsIgnoreCase("")) {
        	alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 300000, recurringDownload);
		}else
			alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), Integer.parseInt(Global.getText("activity.scheduller.send", "")), recurringDownload);
	}
	
	public static void cancelOrderService(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	
	public static void downloadOrder(Context context){
		Intent intent = new Intent(context, DownloadOrderService.class);
        PendingIntent recurringDownload = PendingIntent.getService(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 300000, recurringDownload);
        if (Global.getText("order.scheduller.get", "").equalsIgnoreCase("")) {
        	alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 300000, recurringDownload);
		}else
			alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), Integer.parseInt(Global.getText("activity.scheduller.send", "")), recurringDownload);
	}
	
	public static void cancelDownloadOrderService(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 1, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void sendLocation(Context context){
		Intent intent = new Intent(context, SendLocation.class);
        PendingIntent recurringDownload = PendingIntent.getService(context, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 60*60*1000, recurringDownload);
        if (Global.getText("gps.scheduller.send", "").equalsIgnoreCase("")) {
        	alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 60*60*1000, recurringDownload);
		}else
			alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), Integer.parseInt(Global.getText("gps.scheduller.send", "")), recurringDownload);
	}
	
	public static void cancelLocation(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 2, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void getToken(Context context){
		Intent intent = new Intent(context, GetTokenService.class);
        PendingIntent recurringDownload = PendingIntent.getService(context, 3, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 2*60*1000, recurringDownload);
	}
	
	public static void cancelToken(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 3, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void checkExpiredTasklit(Context context){
		Intent intent = new Intent(context, CheckTasklistExpired.class);
        PendingIntent recurringDownload = PendingIntent.getService(context, 4, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 11);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 24*60*60*1000, recurringDownload);
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 120000, recurringDownload);
	}
	
	public static void cancelExpiredTasklit(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 4, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void sendPendingImage(Context context){
		Intent intent = new Intent(context, SendPendingImage.class);
        PendingIntent recurringDownload = PendingIntent.getService(context, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 2*60*1000, recurringDownload);
	}
	
	public static void cancelSendPendingImage(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 5, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	
	public static void downloadListItem(Context context){
		Intent intent = new Intent(context, downloadListItem.class);
        PendingIntent recurringDownload = PendingIntent.getService(context, 6, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 3*60*1000, recurringDownload);
	}
	
	public static void cancelListItem(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 6, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void sendLostImg(Context context){
//		Intent intent = new Intent(context, LostImageService.class);
//        PendingIntent recurringDownload = PendingIntent.getService(context, 7, intent, PendingIntent.FLAG_UPDATE_CURRENT); //getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 5*60*1000, recurringDownload);
	}
	
	public static void cancelsendLostImg(Context context){
		Intent downloader = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 7, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void autoLogout(Context context){
		Intent autoLogout = new Intent(context, AutoLogout.class);
		PendingIntent autoLogOutPendingIntent  = PendingIntent.getService(context, 8, autoLogout, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarms.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), 10*60*1000, autoLogOutPendingIntent);
	}
	
	public static void cancelAutoLogout(Context context){
		Intent autoLogout = new Intent(context, DAFReciever.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 8, autoLogout, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(recurringDownload);        
        context.stopService(new Intent(context, DAFReciever.class));
	}
	
	public static void cancelAllBackgroundService(Context context){
		cancelDownloadOrderService(context);
		cancelOrderService(context);
		cancelSendPendingImage(context);
		cancelListItem(context);
		cancelLocation(context);
		cancelAutoLogout(context);
		cancelExpiredTasklit(context);
		cancelToken(context);
	}

}
