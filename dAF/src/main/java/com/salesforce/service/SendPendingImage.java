package com.salesforce.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.daf.activity.SettingActivity;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class SendPendingImage extends Service {

    public static final String BROADCAST_ACTION = "com.daf.activity.ImagePendingActivity";
    public static boolean uiActive;
    private Vector<String> vAct = new Vector<String>();
    private Intent intentBroadcast;
    private BroadcastReceiver brodcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            uiActive = true;
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        Log.d("e-formService",SendPendingImage.class.getSimpleName() + " Service Trriggered");
        registerReceiver(brodcast, new IntentFilter(
                "com.salesforce.service.SendPendingImage"));

        Utility.setServContext(this);
        ConnectionBG.DBopen(this);
        Global.newInit();

        if (!uiActive) {
            if (Utility.backgroundImage == false) {
                Utility.backgroundImage = true;
                getAllActivityPending();
                new sendImage()
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }

        return Service.START_NOT_STICKY;
    }

    private void getAllActivityPending() {
        Recordset dataRecordset = ConnectionBG
                .DBquery("select activityId from data_activity where status = '0'");
        vAct = dataRecordset.getRecordFromHeader("activityId");

        Recordset rstDraft = ConnectionBG.DBquery("select * from DRAFT;");
        // mengambil id pada table draft
        for (int i = 0; i < rstDraft.getRows(); i++) {
            NfData data = new NfData(rstDraft.getText(i, "data"));
//            Nset nset = new Nset(data.getInternalObject());
//            String[] form = nset.getObjectKeys();
            String tmpActivityId = data.getData("INIT", "ID");
            if (tmpActivityId != null && tmpActivityId != "") {
                vAct.add(tmpActivityId);
            }

//            for (int j = 0; j < form.length; j++) {
//                if (form[j].equalsIgnoreCase("[INIT]")) {
//                    String[] comp = nset.getData(form[j]).getObjectKeys();
//                    for (int k = 0; k < comp.length; k++) {
//                        String header = form[j];
//                        try {
//                            // untuk menghilangkan []
//                            header = header.substring(1, header.length() - 1);
//                        } catch (Exception e) {
//                        }
//                        if (comp[k].equalsIgnoreCase("ID")) {
//                            final String val = data.getText(header, comp[k]);
//                            if (!val.equalsIgnoreCase("")) {
//                                // menambahkan id ke dalam daftar
//                                vAct.add(val);
//                            }
//                        }
//                    }
//                }
//            }
        }
    }

    private void sendImage() {
        Recordset data = ConnectionBG.DBquery("SELECT * FROM LOG_IMAGE");

        // ambil semua data di folder images
        String[] imagesFiles = new File(Utility.getDefaultImagePath()).list();

        // ambil semua data di folder tmp
        String[] tmpFiles = new File(Utility.getDefaultTempPath()).list();

        // ambil activity id dari table logImage
        Vector<String> activityIdFromData = data.getRecordFromHeader("activityid");

        // buat blacklist yg tidak boleh dikirim
        Vector<String> vAct2 = new Vector<String>();
        for (int i = 0; i < vAct.size(); i++) {
            vAct2.add(vAct.get(i));
        }

        for (int i = 0; i < activityIdFromData.size(); i++) {
            vAct2.add(activityIdFromData.get(i));
        }

        // mengambil activityId pada table path_finger
        Recordset fingerRecordset = ConnectionBG.DBquery("select activityId from TBL_PATH_FINGER");

        Vector<String> fingerName = fingerRecordset.getRecordFromHeader("activityid");

        for (int i = 0; i < fingerName.size(); i++) {
            vAct2.add(fingerName.get(i));
        }

        // file yg tidak boleh dikirim
        Vector<String> fileNotAllowed = new Vector<String>();
        fileNotAllowed.add("finger-nikita");
        fileNotAllowed.add("sign-nikita");
        fileNotAllowed.add("finger-nikita.imgicon.png");
        fileNotAllowed.add("viewImg.png");

        // mengabil data pada folder sign
        String[] signCses = new File(Utility.getDefaultSign()).list();
        if (signCses != null) {
            for (int i = 0; i < signCses.length; i++) {
                fileNotAllowed.add(signCses[i] + ".png");
            }
        }

        // mengambil semua file yang ada pada folder image ke dalam array
        List<String[]> dataYgDiKirim = new ArrayList<String[]>();
        if (imagesFiles != null) {
            for (int i = 0; i < imagesFiles.length; i++) {
                if (!fileNotAllowed.contains(imagesFiles[i])) {
                    // if (imagesFiles[i].indexOf("imgicon.png") == -1) {
                    if (!imagesFiles[i].contains("imgicon.png")) {
                        if (!imagesFiles[i].contains(".png.png")) {
                            String[] tmp = {
                                    imagesFiles[i],
                                    Utility.getDefaultImagePath() + "/"
                                            + imagesFiles[i],
                                    UUID.randomUUID().toString()};
                            dataYgDiKirim.add(tmp);
                        }
                    }

                }
            }
        }

        // mengambil semua file yang ada pada folder tmp ke dalam array
        if (tmpFiles != null) {
            for (int i = 0; i < tmpFiles.length; i++) {
                if (!fileNotAllowed.contains(tmpFiles[i])) {
                    // if (tmpFiles[i].indexOf("imgicon.png") == -1) {
                    if (!tmpFiles[i].contains("imgicon.png")) {
                        if (!tmpFiles[i].contains(".png.png")) {
                            String[] tmp = {tmpFiles[i],
                                    Utility.getDefaultTempPath() +
                                            // "/"+
                                            tmpFiles[i],
                                    UUID.randomUUID().toString()};
                            dataYgDiKirim.add(tmp);
                        }

                    }
                }
            }
        }

        // menagambil data pada table log_order
        Recordset rstLogOrder = ConnectionBG.DBquery("select * from LOG_ORDER;");
//      seno 27/09/2019
//        String userIdFrom = Utility.getSetting(SendPendingImage.this, "userid",
//                "").trim();
        Vector<String> orderIdFromLogOrder = rstLogOrder.getRecordFromHeader("order_id");
//        seno 30/09/2019
//        Vector<String> listBlockFromLogOrder = new Vector<String>();
//
//        for (int i = 0; i < orderIdFromLogOrder.size(); i++) {
//            listBlockFromLogOrder.add(userIdFrom + orderIdFromLogOrder.get(i));
//        }
//
//        int lengSubstringForTasklist = 0;
//
//        if (orderIdFromLogOrder.size() > 0) {
//            lengSubstringForTasklist = userIdFrom.length()
//                    + orderIdFromLogOrder.get(0).length();
//        } else {
//            lengSubstringForTasklist = userIdFrom.length() + 11;
//        }

        //mengambil data pada table trn_tasklist_offline
        Recordset rstTrnTaskListOffline = ConnectionBG.DBquery("select * from trn_tasklist_offline ");
        //seno 27/09/2019
//		Vector<String> orderIdFromTasklistOffline = rstTrnTasklistOffline.getRecordFromHeader("orderId");
        Vector<String> orderIdFromTaskListOffline = rstTrnTaskListOffline.getRecordFromHeader("orderId");

        Recordset rstDraft = ConnectionBG.DBquery("select * from DRAFT");

        Vector<String> orderCodeFromDraft = rstDraft.getRecordFromHeader("orderCode");


     /*   Vector<String> listBlockFromTasklistOffline = new Vector<String>();

        for (int i = 0; i < orderIdFromTasklistOffline.size(); i++) {
            listBlockFromTasklistOffline.add(userIdFrom + orderIdFromTasklistOffline.get(i));
        }

        int lengSubstringForTasklistOffline = 0;


        if (orderIdFromTasklistOffline.size() > 0) {
            lengSubstringForTasklistOffline = userIdFrom.length()
                    + orderIdFromTasklistOffline.get(0).length();
        } else {
//            seno 27/09/2019
            lengSubstringForTasklistOffline = userIdFrom.length() + 8;
//            lengSubstringForTasklist = userIdFrom.length() + 8;
        }*/

        // PROSES MENGIRIM LOST IMAGE
        for (int i = 0; i < dataYgDiKirim.size(); i++) {
            String[] a = dataYgDiKirim.get(i);
            String fileName = a[0];
            String filePath = a[1];
            String reqId = a[2];
            String str = "";
            String activityId = "notAllowed";

            if (uiActive) {
                Utility.backgroundImage = false;
                return;
            }

            if (Generator.stateCurrModelActivityID != "" && fileName.contains(Generator.stateCurrModelActivityID)) {
                Utility.backgroundImage = false;
                return;
            }

            try {
                activityId = fileName.substring(0, fileName.indexOf("."));
            } catch (Exception e) {
//                continue;
            }
//          log_image, data_activity, draft
            if (!vAct2.contains(activityId) && activityId != "notAllowed") {
                // seno 30/09/2019
//                String userPlusOrderId = "notAllowed";
//
//                try {
//                    userPlusOrderId = fileName.substring(0, lengSubstringForTasklist);
//                } catch (Exception e) {
//                    userPlusOrderId = "notAllowed";
//                    continue;
//                }
                boolean lanjut = true;
                for (int j = 0; j < orderIdFromLogOrder.size(); j++) {
                    if (activityId.contains(orderIdFromLogOrder.get(j))) {
                        lanjut = false;
                        break;
                    }
                }
//                tasklist, tasklist data
                if (lanjut) {
                    // seno 30/09/2019
//                if (!listBlockFromLogOrder.contains(userPlusOrderId)) { //log_order
                    // seno 27/09/19
//					try{
//						userPlusOrderId = fileName.substring(0, lengSubstringForTasklistOffline);
//					}catch(Exception e){
//						userPlusOrderId = "notAllowed";
//						continue;
//					}
                    boolean lanjut1 = true;
                    for (int k = 0; k < orderIdFromTaskListOffline.size(); k++) {
                        if (activityId.contains(orderIdFromTaskListOffline.get(k))) {
                            lanjut1 = false;
                            break;
                        }
                    }
                    if (lanjut1) { //taskList_offline
/*
                        //seno 27/09/19
//					if(!listBlockFromTasklistOffline.contains(userPlusOrderId)){ //taskList_offline
//
//                        if (uiActive) {
//                            return;
//                        }*/
                        boolean lanjut2 = true;
                        for (int l = 0; l < orderCodeFromDraft.size(); l++) {
                            if (activityId.contains(orderCodeFromDraft.get(l))) {
                                lanjut2 = false;
                                break;
                            }
                        }
                        if (lanjut2) {

                            if (Utility.getNewToken()) {
                                String urll = SettingActivity.URL_SERVER
                                        + "uploadimageservlet/?"
                                        + "&token="
                                        + Utility.getSetting(Utility.getAppContext(),
                                        "Token", "");
                                String fname = fileName;

                                if (!fileName.contains(".png")) {
                                    if (!fileName.contains(".jpg")) {
                                        fname = fileName + ".";
                                    }
                                }
                                str = Utility.postHttpConnection(urll, null, fname,
                                        filePath, reqId);
//							String check = str;
//							System.out.println(check);
                            }
                            if (str.equalsIgnoreCase("OK")) {
                                new File(filePath).delete();
                            }
                        }
                    }

                }
            }
        }


        //PROSES MENGIRIM PENDING IMAGE
        String fname = "", path = "";
        String str = "";

        if (data.getRows() > 0) {
            for (int i = 0; i < data.getRows(); i++) {

                if (uiActive) {
                    Utility.backgroundImage = false;
                    return;
                }

                // cek untuk tidak mengirim gambar yang data stream nya masih
                // ada di pending data
                if (!vAct.contains(data.getText(i, "activityId"))) {

                    fname = data.getText(i, "imgName");
                    path = data.getText(i, "path");

                    // perbaikan terbaru 22/2/18
                    if (!fname.contains(".png")) {
                        if (!fname.contains(".jpg")) {
                            fname = fname + ".";
                        }

                    }
					/*File abc = new File(path);
					if(abc.exists()){
						System.out.println("ada");
					}else{
						System.out.println("Tidak ada");
					}*/

                    String applno = data.getText(i, "applNo");
                    String userid = data.getText(i, "userId");
                    // 17/12/2018 seno
                    String reqId = data.getText(i, "reqId");

                    if (Utility.getNewToken()) {
                        if (data.getText(i, "isTasklist").equalsIgnoreCase("Y")) {
                            str = Utility.getHttpConnection(Utility.getURLenc(
                                    SettingActivity.URL_SERVER
                                            + "imagecheckservlet/?"
                                            + (true ? "f=service&" : "")
                                            + "applno=", applno, "&modul=",
                                    "tasklist", "&imagename=", fname,
                                    "&token=", Utility.getSetting(
                                            Utility.getAppContext(), "Token",
                                            ""), "&reqId=", reqId));
//							String check = str;
//							System.out.println(check);
                        } else {
                            str = Utility.getHttpConnection(Utility.getURLenc(
                                    SettingActivity.URL_SERVER
                                            + "imagecheckservlet/?"
                                            + (true ? "f=service&" : "")
                                            + "imagename=", fname, "&token=",
                                    Utility.getSetting(Utility.getAppContext(),
                                            "Token", ""), "&reqId=", reqId));
//							String check = str;
//							System.out.println(check);
                        }

                    }

                    if (str.equalsIgnoreCase("NO")) {

                        if (Utility.getNewToken()) {
                            if (data.getText(i, "isTasklist").equalsIgnoreCase(
                                    "Y")) {
                                str = Utility
                                        .postHttpConnection(
                                                SettingActivity.URL_SERVER
                                                        + "uploadservlet/?userid="
                                                        + userid
                                                        + "&applno="
                                                        + applno
                                                        + "&modul=tasklist"
                                                        + "&imei="
                                                        + Utility
                                                        .getImei(Utility
                                                                .getAppContext())
                                                        + "&token="
                                                        + Utility.getSetting(
                                                        Utility.getAppContext(),
                                                        "Token", ""),
                                                null, fname, path, reqId);
//								String check = str;
//								System.out.println(check);
                            } else {
                                str = Utility
                                        .postHttpConnection(
                                                SettingActivity.URL_SERVER
                                                        + "uploadservlet/?userid="
                                                        + userid
                                                        + "&imei="
                                                        + Utility
                                                        .getImei(Utility
                                                                .getAppContext())
                                                        + "&token="
                                                        + Utility.getSetting(
                                                        Utility.getAppContext(),
                                                        "Token", ""),
                                                null, fname, path, reqId);
//								String check = str;
//								System.out.println(check);
                            }

                            if (str.equalsIgnoreCase("OK")) {

                                // untuk menghilangkan titik gambar yang tidak
                                // ada extensi nya agar bisa dihapus
                                if (!fname.contains(".png")) {
                                    if (!fname.contains(".jpg")) {
                                        fname = fname.substring(0,
                                                (fname.length() - 1));
                                    }
                                }

                                ConnectionBG.DBdelete("LOG_IMAGE",
                                        " imgName =? ", new String[]{fname});
                                new File(Utility.getDefaultPath(fname))
                                        .delete();
                                new File(Utility.getDefaultTempPath(fname))
                                        .delete();
                                new File(Utility.getDefaultImagePath(fname))
                                        .delete();
                            }
                        }
                    } else if (str.equalsIgnoreCase("FOUND")) {

                        // untuk menghilangkan titik gambar yang tidak ada
                        // extensi nya agar bisa dihapus
                        if (!fname.contains(".png")) {
                            if (!fname.contains(".jpg")) {
                                fname = fname
                                        .substring(0, (fname.length() - 1));
                            }
                        }

                        ConnectionBG.DBdelete("LOG_IMAGE", " imgName =? ",
                                new String[]{fname});
                        new File(Utility.getDefaultPath(fname)).delete();
                        new File(Utility.getDefaultTempPath(fname)).delete();
                        new File(Utility.getDefaultImagePath(fname)).delete();
                    }
                }
                sendBroadcast(intentBroadcast);
            }
        }


        Utility.backgroundImage = false;
    }


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        intentBroadcast = new Intent(BROADCAST_ACTION);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private class sendImage extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            if (!uiActive) {
                sendImage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // if (ImagePendingActivity.class != null) {
            //
            // }
        }
    }

}
