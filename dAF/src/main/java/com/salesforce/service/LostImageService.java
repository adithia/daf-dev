package com.salesforce.service;

import java.io.File;
import java.util.UUID;
import java.util.Vector;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.IBinder;

import com.daf.activity.SettingActivity;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Utility;

public class LostImageService extends Service {

	private Vector<String> vImages = new Vector<String>();
	// 17:12 19/02/2019 seno
//	public static final String BROADCAST_ACTION = "com.salesforce.service.LostImageService";
	public static final String BROADCAST_ACTION = "com.daf.activity.NewEntry";	
	private Intent intent;
	public static boolean uiActive;
	//end

	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		
		//seno 17:12 19/02/2019
		registerReceiver(brodcast, new IntentFilter("com.salesforce.service.LostImageService"));
		//end
		
		Utility.setServContext(this);
		ConnectionBG.DBopen(this);
		Global.newInit();
		//seno 17:12 19/02/2019
//		// if (Utility.backgroundImage == false) {
//		if (Utility.backLostImage == false) {
//			Utility.backLostImage = true;
//			new sendImage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//		}
//		// }
		if(!uiActive){
			if (Utility.backLostImage == false) {
				Utility.backLostImage = true;
				new sendImage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}	
		}
		//end
		return Service.START_NOT_STICKY;
	}

	private void loadImages() {
		String[] s = new File(Utility.getDefaultImagePath()).list(); // ambil
																		// semua
																		// data
																		// di
																		// folder
																		// images
		StringBuffer sbuf = new StringBuffer("|");
		if (s != null) {
			for (int i = 0; i < s.length; i++) {
				sbuf.append(s[i]).append("|");
			}
		}
		String buffer = sbuf.toString();

		Recordset rst = ConnectionBG
				.DBquery("select * from data_activity where status = '0' ORDER BY activityId ASC;");
		Recordset rstLogImg = ConnectionBG.DBquery("select * from LOG_IMAGE;");

		for (int i = 0; i < rst.getRows(); i++) {
			NfData data = new NfData(rst.getText(i, "data"));
			Nset nset = new Nset(data.getInternalObject());
			String[] form = nset.getObjectKeys();

			for (int j = 0; j < form.length; j++) {
				if (form[j].equalsIgnoreCase("[F12]")
						|| form[j].equalsIgnoreCase("[F32]")
						|| form[j].equalsIgnoreCase("[F40]")
						|| form[j].equalsIgnoreCase("[F31]")) {
					String[] comp = nset.getData(form[j]).getObjectKeys();
					for (int k = 0; k < comp.length; k++) {
						String header = form[j];
						try {
							header = header.substring(1, header.length() - 1);
						} catch (Exception e) {
						}
						final String val = data.getText(header, comp[k]);

						String returnImg = "";
						// kirim gambar yang ada di root dan image
						if (!val.equalsIgnoreCase("")) {
							if (val.contains("FP") || val.contains(".png")
									|| val.contains(".jpg")
									|| val.contains("_T")) {
								vImages.add(val);
							}
						}

					}
				}

			}
		}
		// SENO 18:43 04/01/2019 start
		Recordset rstDraft = ConnectionBG.DBquery("select * from DRAFT;");

		for (int i = 0; i < rstDraft.getRows(); i++) {
			NfData data = new NfData(rstDraft.getText(i, "data"));
			Nset nset = new Nset(data.getInternalObject());
			String[] form = nset.getObjectKeys();

			for (int j = 0; j < form.length; j++) {
				if (form[j].equalsIgnoreCase("[F12]")
						|| form[j].equalsIgnoreCase("[F32]")
						|| form[j].equalsIgnoreCase("[F40]")
						|| form[j].equalsIgnoreCase("[F31]")) {
					String[] comp = nset.getData(form[j]).getObjectKeys();
					for (int k = 0; k < comp.length; k++) {
						String header = form[j];
						try {
							header = header.substring(1, header.length() - 1);
						} catch (Exception e) {
						}
						final String val = data.getText(header, comp[k]);

						String returnImg = "";
						// kirim gambar yang ada di root dan image
						if (!val.equalsIgnoreCase("")) {
							if (val.contains("FP") || val.contains(".png")
									|| val.contains(".jpg")
									|| val.contains("_T")) {
								// menambahkan nama file yang ada pada table
								// draft ke dalam array yg akan di bandingkan
								if (val.length() > 18) {
									vImages.add(val);
								}

							}
						}

					}
				}

			}
		}
		// SENO 18:43 04/01/2019 end

		// menambahkan nama file yang ada pada table log image ke dalam array yg
		// akan di bandingkan
		for (int i = 0; i < rstLogImg.getRows(); i++) {
			vImages.add(rstLogImg.getText(i, "imgName"));
		}

		// menambahkan nama file yang ada pada table tbl_path_finger ke adalam
		// aray yg akan di bandingkan
		Recordset rstPathFinger = ConnectionBG
				.DBquery("select * from TBL_PATH_FINGER");
		for (int i = 0; i < rstPathFinger.getRows(); i++) {
			String a = rstPathFinger.getText(i, "path");
			String hasilsubsting = a.substring(a.lastIndexOf("/") + 1);
			vImages.add(hasilsubsting);
		}

		Recordset rstLogOrder = ConnectionBG
				.DBquery("select * from LOG_ORDER;");
		String userIdFrom = Utility.getSetting(LostImageService.this, "userid",
				"").trim();
		Vector<String> orderIdFromLogOrder = rstLogOrder
				.getRecordFromHeader("order_id");
		Vector<String> listBlockFromLogOrder = new Vector<String>();

		for (int i = 0; i < orderIdFromLogOrder.size(); i++) {
			listBlockFromLogOrder.add(userIdFrom + orderIdFromLogOrder.get(i));
		}

		int lengSubstring = 0;
		if (orderIdFromLogOrder.size() > 0) {
			lengSubstring = userIdFrom.length()
					+ orderIdFromLogOrder.get(0).length();
		} else {
			lengSubstring = userIdFrom.length() + 11;
		}

		String[] strImages = Utility.split(buffer, "|");
		for (int i = 0; i < strImages.length; i++) {
			if (!vImages.contains(strImages[i])) {

				String returnImg = "";
				String imgName = strImages[i];

				// if (!imgName.contains(".png") || !imgName.contains(".jpg")) {
				// imgName = imgName+".";
				// }

				// perbaikan terbaru 22/2/18
				if (!imgName.contains(".png")) {
					if (!imgName.contains(".jpg")) {
						imgName = imgName + ".";
					}

				}

				if (!strImages[i].equalsIgnoreCase("")) {
					if (!listBlockFromLogOrder.contains(strImages[i].substring(
							0, lengSubstring))) {
						if (Utility.getNewToken()) {
							String url = SettingActivity.URL_SERVER
									+ "uploadimageservlet/?"
									+ "&token="
									+ Utility.getSetting(
											Utility.getAppContext(), "Token",
											"");
							returnImg = Utility.postHttpConnection(url, null,
									imgName,
									Utility.getDefaultImagePath(strImages[i]),
									UUID.randomUUID().toString());
						}
						if (returnImg.equalsIgnoreCase("OK")) {
							new File(Utility.getDefaultImagePath(strImages[i]))
									.delete();
						}
					}
				}

				// String returnImg = "";
				// String imgName = strImages[i];
				//
				// // if (!imgName.contains(".png") ||
				// !imgName.contains(".jpg")) {
				// // imgName = imgName+".";
				// // }
				//
				// //perbaikan terbaru 22/2/18
				// if (!imgName.contains(".png")) {
				// if (!imgName.contains(".jpg")) {
				// imgName = imgName+".";
				// }
				//
				// }
				//
				//
				// if (!strImages[i].equalsIgnoreCase("")) {
				// if (Utility.getNewToken()) {
				// String url = SettingActivity.URL_SERVER +
				// "uploadimageservlet/?"+"&token="+Utility.getSetting(Utility.getAppContext(),
				// "Token", "");
				// returnImg = Utility.postHttpConnection(url, null, imgName,
				// Utility.getDefaultImagePath(strImages[i]),
				// UUID.randomUUID().toString());
				// }
				// if (returnImg.equalsIgnoreCase("OK")) {
				// new File(Utility.getDefaultImagePath(strImages[i])).delete();
				// }
				// }

			}
		}
		vImages.clear();
		Utility.backLostImage = false;
	}

	private class sendImage extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// seno 17:12 19/02/2019
//			loadImages();
			if(!uiActive){
				loadImages();
			}
			//end
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			// if (ImagePendingActivity.class != null) {
			//
			// }
		}
	};
	//seno 17:12 19/02/2019
	private BroadcastReceiver brodcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			uiActive = true;
		}
	};
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	    intent = new Intent(BROADCAST_ACTION);
	}
	//end
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
