package com.salesforce.service;


import com.salesforce.route.Finish;
import com.salesforce.utility.Utility;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class AutoLogout extends Service{
//	Intent a ;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("e-formService",AutoLogout.class.getSimpleName() + " Service Trriggered");
//		return super.onStartCommand(intent, flags, startId);
//		if(Utility.autoLogout==false){
			Utility.autoLogout=true;
//			a = intent;
			new autoLogOut().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//		}
				
		return Service.START_NOT_STICKY;
	}
	
	private class autoLogOut extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			autoLogOut();
			return null;
		}
	}
	
	private void autoLogOut(){
		if(Utility.isTimeToOff()){
//			 if(a.getAction().equals(getPackageName()+".closeapp")){
//		            if(Build.VERSION.SDK_INT>=16 && Build.VERSION.SDK_INT<21){		            			            	
//		            	finishAffinity();
//		            } else if(Build.VERSION.SDK_INT>=21){
//		                finishAndRemoveTask();
//		            }
//		        }
			ExitActivity.exit(getApplicationContext());
//			System.out.println("sdfadfadfa");
		}
	}
	
	public static class ExitActivity extends Activity{
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			finish();
		}
		
		 public static void exit(Context context) {
		        Intent intent = new Intent(context, ExitActivity.class);
		        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		        context.startActivity(intent);
		    }
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {

		return null;
	}

}
