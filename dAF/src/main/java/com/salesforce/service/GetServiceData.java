package com.salesforce.service;

import org.json.JSONObject;

import com.salesforce.utility.Utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class GetServiceData<T> extends AsyncTask<Void, Void, String> {

//	private int timeOut = 60000;	
	private ProgressDialog pDialog;	
	private Context context;
	private String url, message;
	
	public GetServiceData(Context context, String url, String message){
		this.context = context;
		this.url = url;
		this.message = message;
		
	}
	
	@Override
	protected void onPreExecute() {
		pDialog = Utility.showProgresbar(context, message);
		pDialog.setCancelable(false);
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		String result = Utility.getHttpConnection2(url);		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result != null) {
			onResultListener(result);
		}else {
			onErrorResultListener(result);
		}
		
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}
	}
	
	public void onResultListener(String response) {

	}
	
	public void onErrorResultListener(String response) {
		
	}

}
