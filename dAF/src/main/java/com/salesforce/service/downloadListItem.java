package com.salesforce.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.daf.activity.SettingActivity;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Utility;

public class downloadListItem extends Service{
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d("e-formService",downloadListItem.class.getSimpleName() + " Service Trriggered");
		ConnectionBG.DBopen(this);	
		if (Utility.backgroundListItem == false) {
			Utility.backgroundListItem = true;
//			downloadListItem();
			new getItem().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
		
		return Service.START_NOT_STICKY;
	}
	
	private class getItem extends AsyncTask<Void, Void, Void>{
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			downloadListItem();
			
			return null;
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void downloadListItem(){
		
		if (Utility.getNewToken()) {

			String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=",
					Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=", 
					"TABLE_LIST_ITEM" ,"&imei=" ,Utility.getImei(getApplicationContext()).toUpperCase(), "&lastSync=", "" ,"&v=100",
					"&token=", Utility.getSetting(getApplicationContext(), "Token", ""));	
				
			Nset order = Nset.newObject();
			
			String dfdf = Utility.getHttpConnection(sUrl);
			order = Nset.readJSON(dfdf);
			if (order.getArraySize() > 0) {
				for (int j = 0; j <  order.getArraySize(); j++) {
					
					String table = order.getData(j).getData("table").toString();
					
					for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
						String code = order.getData(j).getData("data").getData(k).getData("code").toString();
						String drst = order.getData(j).getData("data").getData(k).getData("data").toString();
						Recordset xxx = Stream.downStream(drst);
						if (xxx.getRows() > 0) {
							if (code.equals("truncate")) {
								Syncronizer.TruncateBG(table, xxx);
							}
						}
					}
					
				}
			}
		}
		
		Utility.backgroundListItem = false;
	}

}
