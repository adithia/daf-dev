package com.salesforce.service;

import java.util.Vector;

import android.os.AsyncTask;

public class Background {
	private static Vector<ModelBackgroud> backgrounds = new Vector<ModelBackgroud>();
	
	public static void runOnBackground(Runnable run, Runnable onfinish){
		backgrounds.addElement(new Background().ModelBackgroud("", run, onfinish));
	}
    private ModelBackgroud ModelBackgroud(String string, Runnable run, Runnable onfinish) {
		return new ModelBackgroud(string, run, onfinish);
	}
	public static void runOnBackground(String id, Runnable run){
		backgrounds.addElement(new Background().ModelBackgroud(id, run, new Runnable(){public void run() {}}));
	}
    public static boolean isRunningOnBackground(String id){
    	for (int i = 0; i < backgrounds.size(); i++) {
			if (backgrounds.elementAt(i).getName().equals(id)) {
				return backgrounds.elementAt(i).isAlive();
			}
		}
    	return false;
    }
    public static void stopRunningOnBackground(String id){
    	for (int i = 0; i < backgrounds.size(); i++) {
			if (backgrounds.elementAt(i).getName().equals(id)) {
				backgrounds.elementAt(i).stop();
				return ;
			}
		}
    }
    
    
    public class ModelBackgroud{
    	private String code="";
    	@SuppressWarnings("rawtypes")
		private AsyncTask asyncTask;
    	private boolean isAlive=false;
    	
    	public String getName(){
    		return code;
    	}
    	public ModelBackgroud(String name, Runnable run, Runnable uiPost){
    		code=name;
    		run(run, uiPost);
    	}
    	@SuppressWarnings("unchecked")
		public void run(Runnable run, Runnable uiPost){
    		asyncTask= new AsyncTask<Runnable, Void, Runnable> (){
	  		      protected Runnable doInBackground(Runnable... params) {
	  		    	  isAlive=true;
	  		    	  params[0].run();
	  		    	  return params[1];
	  		      }      
	  		      protected void onPostExecute(Runnable result) {    
	  		    	  isAlive=false;
	  		    	  result.run();
	  		      }		      
	  		      protected void onCancelled(Runnable result) {
	  		    	  isAlive=false;
	  		    	  result.run();
	  			  }
	  		      protected void onPreExecute() {}
	  		      protected void onProgressUpdate(Void... values) {}
    		};
    		asyncTask.execute(run, uiPost);
    	}
    	public void stop (){
    		if (asyncTask!=null) {
    			asyncTask.cancel(true);
			}
    	}
    	public boolean isAlive(){
    		return isAlive;
    	}
    }   
    
    
}
