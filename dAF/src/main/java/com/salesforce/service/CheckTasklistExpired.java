package com.salesforce.service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.salesforce.R;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;
import com.salesforce.stream.Model;
import com.salesforce.utility.Utility;

public class CheckTasklistExpired extends Service{
	
	 private NotificationCompat.Builder builder;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("e-formService",CheckTasklistExpired.class.getSimpleName() + " Service Trriggered");
		// TODO Auto-generated method stub
		ConnectionBG.DBopen(this);
		Global.newInit();
		
		checkTasklist();
		
		return Service.START_NOT_STICKY;
	}

	public  void checkTasklist(){
//		Recordset data = ConnectionBG.DBquery("SELECT order_id, modified_date FROM trn_task_list");
//		Recordset data = Connection.DBquery("SELECT order_id, modified_date FROM trn_task_list where modified_by='12169'");

		Recordset rst = ConnectionBG.DBquery("select distinct model_order_tablename from model;");
		
//		25/04/2019 seno daf3
		Vector<String> header = new Vector<String>();
		Vector<Vector<String>> fields= new Vector<Vector<String>>();
		header.add(rst.getHeader(0));
		for (int i = 0; i < rst.getRows(); i++) {
			String namaTbl = rst.getText(i,0);
			if(namaTbl.contains(",")){
				Vector<String> tmp = Utility.splitVector(namaTbl, ",");
				for(int j=0; j<tmp.size(); j++){
					if(!tmp.get(j).equals("trn_tasklist_data_object")){
						fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
					}
				}
			}else{				
				fields.add(new Vector<String>(Arrays.asList(namaTbl)));
			}
			
		}		
		rst = null;
		rst = new Model (header,fields);
		
		if(rst.getRows()>0){
			int size = rst.getRows(); 
			int dayNotif = Global.getInt("sched.tasklist.akan.expired");
			for(int i=0; i< size; i++){
				Recordset data = ConnectionBG.DBquery("SELECT order_id, expired_date FROM "+rst.getText(i,0)+"");
				if (data.getRows() > 0) {
					for (int j = 0; j < data.getRows(); j++) {
//						int dayNotif = Global.getInt("sched.tasklist.akan.expired");
						if (!data.getText(j, "expired_date").equalsIgnoreCase("")) {
							Calendar cal = convertTasklits(data.getText(j, "expired_date").substring(0, data.getText(j, "expired_date").indexOf(" ")));
							cal.add(Calendar.DATE, -dayNotif);
							String satu = Utility.getDateDAF();
							String dua  = Utility.getDateToString(cal.getTime());
							if (Utility.getDateDAF().equalsIgnoreCase(Utility.getDateToString(cal.getTime()))) {
								String model = rst.getText(i,0);
								if(model.equals("trn_task_list")){
									setNotificationAlarm("Tasklist dengan Order Id "+data.getText(j, "order_id") + " akan expired dalam "+dayNotif +" Hari", j);
								}else if(model.equals("trn_tasklist_data")){
									setNotificationAlarm("Tasklist Data dengan Order Id "+data.getText(j, "order_id") + " akan expired dalam "+dayNotif +" Hari", j);
								}else{
									setNotificationAlarm("Tasklist dengan Order Id "+data.getText(j, "order_id") + " akan expired dalam "+dayNotif +" Hari", j);
								}

							}
						}				
					}
				}
				
			}
			
		}
		//selesai 
		
	
		
//		if (data.getRows() > 0) {
//			for (int i = 0; i < data.getRows(); i++) {
//				int dayNotif = Global.getInt("sched.tasklist.akan.expired");
//				if (!data.getText(i, "modified_date").equalsIgnoreCase("")) {
//					Calendar cal = convertTasklits(data.getText(i, "modified_date").substring(0, data.getText(i, "modified_date").indexOf(" ")));				
//					cal.add(Calendar.DATE, -dayNotif);
//					
//					if (Utility.getDateDAF().equalsIgnoreCase(Utility.getDateToString(cal.getTime()))) {
//						setNotificationAlarm("Tasklist dengan Order Id "+data.getText(i, "order_id") + " akan expired dalam "+dayNotif +" Hari", i);
//					}
//				}				
//			}
//		}
	}
	
	public static Calendar convertTasklits(String strDate){
		Calendar calendar = Calendar.getInstance();
		try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH);
			date = sdf.parse(strDate);
			calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return calendar;
	}
	
	private void setNotificationAlarm( String msg, int code){
		NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);		
		builder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.notif48)
                .setContentTitle("e-Form Notification")
                .setContentText(msg)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setDefaults(Notification.DEFAULT_ALL);
		nm.notify(code, builder.build());
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
