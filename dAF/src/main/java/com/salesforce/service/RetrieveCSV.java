package com.salesforce.service;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.daf.activity.LoginActivity;
import com.daf.activity.SplashScreen;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Utility;

public class RetrieveCSV extends AsyncTask<Void, String, Void> {
	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values[0]);
		dialog.setMessage(values[0].toString());
	}

	private ProgressDialog dialog;
	private Activity context;

	public RetrieveCSV(Activity activity) {
		this.context = activity;
		dialog = new ProgressDialog(context);
		dialog.setCancelable(false);
		
		Connection.DBopen();
	}

	@Override
	protected void onPreExecute() {
		dialog.setMessage("Retrieve Master From local, please wait ...");
		dialog.show();
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {

			Connection.DBdelete("MST_BU");
			publishProgress("Insert MST_BU to Database");
			insertBu();
			
			Connection.DBdelete("MST_BU_OBJECT_GROUP_SYNC");
			publishProgress("Insert MST_BU_OBJECT_GROUP_SYNC to Database");
			insertBuObjectGroupSync();	
			
			Connection.DBdelete("MST_COMBINATION_MODEL");
			publishProgress("Insert MST_COMBINATION_MODEL to Database");
			insertComModel();
			
			Connection.DBdelete("MST_COY_ID");
			publishProgress("Insert MST_COY_ID to Database");
			insertCoyId();
			
			Connection.DBdelete("MST_CUST_CATEGORIES");
			publishProgress("Insert MST_CUST_CATEGORIES to Database");
			insertCustCategories();
			
			Connection.DBdelete("MST_DOC");
			publishProgress("Insert MST_DOC to Database");
			insertDoc();
			
			Connection.DBdelete("MST_DOC_GENERATE");
			publishProgress("Insert MST_DOC_GENERATE to Database");
			insertDocGenerate();
			
			Connection.DBdelete("MST_DOC_GROUP");
			publishProgress("Insert MST_DOC_GROUP to Database");
			insertDocGroup();
			
			Connection.DBdelete("MST_DOC_GROUP_MAP_TYPE");
			publishProgress("Insert MST_DOC_GROUP_MAP_TYPE to Database");
			insertDocGroupMapType();
			
			Connection.DBdelete("MST_DOC_TYPE");
			publishProgress("Insert MST_DOC_TYPE to Database");
			insertDocType();
			
			Connection.DBdelete("MST_EDUCATION");
			publishProgress("Insert MST_EDUCATION to Database");
			insertEducation();
			
			Connection.DBdelete("MST_FIELD");
			publishProgress("Insert MST_FIELD to Database");
			insertField();
			
			Connection.DBdelete("MST_HOUSE_STAT");
			publishProgress("Insert MST_HOUSE_STAT to Database");
			insertHouseState();	
			
			Connection.DBdelete("MST_IDENTITY");
			publishProgress("Insert MST_IDENTITY to Database");
			insertIdentity();	
			
			Connection.DBdelete("MST_INTEREST_RULE");
			publishProgress("Insert MST_INTEREST_RULE to Database");
			insertInterestRule();	
			
			Connection.DBdelete("MST_MAP_JOB_CODE");
			publishProgress("Insert MST_MAP_JOB_CODE to Database");
			insertMapJobCode();	
			
			Connection.DBdelete("MST_MARITAL_STATUS");
			publishProgress("Insert MST_MARITAL_STATUS to Database");
			insertMarital();
			
			Connection.DBdelete("MST_OBJECT_CODE");
			publishProgress("Insert MST_OBJECT_CODE to Database");
			insertObjectCode();
			
			Connection.DBdelete("MST_OBJ_WARNA_DTL_SYNC");
			publishProgress("Insert MST_OBJ_WARNA_DTL_SYNC to Database");
			insertObjWarnaDtlSync();
			
			Connection.DBdelete("MST_OCCUPATION");
			publishProgress("Insert MST_OCCUPATION to Database");
			insertOccupation();
			
			Connection.DBdelete("MST_PLATFORM");
			publishProgress("Insert MST_PLATFORM to Database");
			insertPlatform();
			
			Connection.DBdelete("MST_RULE_DTL");
			publishProgress("Insert MST_RULE_DTL to Database");
			insertRuleDtl();
			
			Connection.DBdelete("MST_RULE_HDR");
			publishProgress("Insert MST_RULE_HDR to Database");
			insertRuleHdr();
			
			Connection.DBdelete("MST_SOURCES_ORDER");
			publishProgress("Insert MST_SOURCES_ORDER to Database");
			insertSourceOrder();
			
			Connection.DBdelete("MST_SUB_OCCUPATION");
			publishProgress("Insert MST_SUB_OCCUPATION to Database");
			insertSubOccupation();
			
			Connection.DBdelete("MST_ZIP_SYNC");
			publishProgress("Insert MST_ZIP_SYNC to Database");
			insertZipSync();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
		
		
	}

	@Override
	protected void onPostExecute(Void result) {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		
		if (context instanceof SplashScreen) {
			context.startActivity(new Intent(context, LoginActivity.class));
			context.finish();
		}
		
	}
	
	private void insertIdentity() {
		List<String[]> lFile = Utility.CSVRead("MST_IDENTITY.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_IDENTITY", data);
		Syncronizer.insertToDB("MST_IDENTITY", data);
	}
	
	private void insertHouseState() {
		List<String[]> lFile = Utility.CSVRead("MST_HOUSE_STAT.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_HOUSE_STAT", data);
		Syncronizer.insertToDB("MST_HOUSE_STAT", data);
	}
	
	private void insertEducation() {
		List<String[]> lFile = Utility.CSVRead("MST_EDUCATION.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_EDUCATION", data);
		Syncronizer.insertToDB("MST_EDUCATION", data);
	}
	
	private void insertOccupation() {
		List<String[]> lFile = Utility.CSVRead("MST_OCCUPATION.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_OCCUPATION", data);
		Syncronizer.insertToDB("MST_OCCUPATION", data);
	}
	
	private void insertSubOccupation() {
		List<String[]> lFile = Utility.CSVRead("MST_SUB_OCCUPATION.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_SUB_OCCUPATION", data);
		Syncronizer.insertToDB("MST_SUB_OCCUPATION", data);
	}
	
	private void insertSourceOrder() {
		List<String[]> lFile = Utility.CSVRead("MST_SOURCE_ORDER.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_SOURCE_ORDER", data);
		Syncronizer.insertToDB("MST_SOURCE_ORDER", data);
	}
	
	private void insertInterestRule() {
		List<String[]> lFile = Utility.CSVRead("MST_INTEREST_RULES.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_INTEREST_RULES", data);
		Syncronizer.insertToDB("MST_INTEREST_RULES", data);
	}
	
	private void insertCoyId() {
		List<String[]> lFile = Utility.CSVRead("MST_COY_ID.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_COY_ID", data);
		Syncronizer.insertToDB("MST_COY_ID", data);
	}
	
	private void insertBu() {
		List<String[]> lFile = Utility.CSVRead("MST_BU.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_BU", data);
		Syncronizer.insertToDB("MST_BU", data);
	}
	
	private void insertPlatform() {
		List<String[]> lFile = Utility.CSVRead("MST_PLATFORM.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_PLATFORM", data);
		Syncronizer.insertToDB("MST_PLATFORM", data);
	}
	
	private void insertComModel() {
		List<String[]> lFile = Utility.CSVRead("MST_COMBINATION_MODEL.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_COMBINATION_MODEL", data);
		Syncronizer.insertToDB("MST_COMBINATION_MODEL", data);
	}

	//yang paling banyak
	private void insertObjectCode() {
		List<String[]> lFile = Utility.CSVRead("MST_OBJ_CODE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_OBJ_CODE", data);
		Syncronizer.insertToDB("MST_OBJ_CODE", data);
	}
	
	private void insertMKTProgram() {
		List<String[]> lFile = Utility.CSVRead("MST_MKT_PROGRAM.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_MKT_PROGRAM", data);
		Syncronizer.insertToDB("MST_MKT_PROGRAM", data);
	}
	
	private void insertMapJobCode() {
		List<String[]> lFile = Utility.CSVRead("MST_MAP_JOB_CODE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_MAP_JOB_CODE", data);
		Syncronizer.insertToDB("MST_MAP_JOB_CODE", data);
	}
	
	private void insertCustCategories() {
		List<String[]> lFile = Utility.CSVRead("MST_CUST_CATEGORIES.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_CUST_CATEGORIES", data);
		Syncronizer.insertToDB("MST_CUST_CATEGORIES", data);
	}
	
	private void insertBuObjectGroupSync() {
		List<String[]> lFile = Utility.CSVRead("MST_BU_OBJ_GROUP_SYNC.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_BU_OBJ_GROUP_SYNC", data);
		Syncronizer.insertToDB("MST_BU_OBJ_GROUP_SYNC", data);
	}
	
	private void insertDocGenerate() {
		List<String[]> lFile = Utility.CSVRead("MST_DOC_GENERATE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_DOC_GENERATE", data);
		Syncronizer.insertToDB("MST_DOC_GENERATE", data);
	}
	
	private void insertDocGroup() {
		List<String[]> lFile = Utility.CSVRead("MST_DOC_GROUP.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_DOC_GROUP", data);
		Syncronizer.insertToDB("MST_DOC_GROUP", data);
	}
	
	private void insertDocGroupMapType() {
		List<String[]> lFile = Utility.CSVRead("MST_DOC_GROUP_MAP_TYPE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_DOC_GROUP_MAP_TYPE", data);
		Syncronizer.insertToDB("MST_DOC_GROUP_MAP_TYPE", data);
	}
	
	private void insertDocType() {
		List<String[]> lFile = Utility.CSVRead("MST_DOC_TYPE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_DOC_TYPE", data);
		Syncronizer.insertToDB("MST_DOC_TYPE", data);
	}
	
	private void insertField() {
		List<String[]> lFile = Utility.CSVRead("MST_FIELD.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_FIELD", data);
		Syncronizer.insertToDB("MST_FIELD", data);
	}
	
	private void insertRuleDtl() {
		List<String[]> lFile = Utility.CSVRead("MST_RULE_DTL.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_RULE_DTL", data);
		Syncronizer.insertToDB("MST_RULE_DTL", data);
	}
	
	private void insertRuleHdr() {
		List<String[]> lFile = Utility.CSVRead("MST_RULE_HDR.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_RULE_HDR", data);
		Syncronizer.insertToDB("MST_RULE_HDR", data);
	}
	
	private void insertOffice() {
		List<String[]> lFile = Utility.CSVRead("MST_OFFICE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_OFFICE", data);
		Syncronizer.insertToDB("MST_OFFICE", data);
	}
	
	//bayang juga
	private void insertServiceOffice() {
		List<String[]> lFile = Utility.CSVRead("MST_SERVICE_OFFICE.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_SERVICE_OFFICE", data);
		Syncronizer.insertToDB("MST_SERVICE_OFFICE", data);
	}
	
	private void insertMarital() {
		List<String[]> lFile = Utility.CSVRead("MST_MARITAL_STATUS.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_MARITAL_STATUS", data);
		Syncronizer.insertToDB("MST_MARITAL_STATUS", data);
	}
	
	private void insertDoc() {
		List<String[]> lFile = Utility.CSVRead("MST_DOC.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_DOC", data);
		Syncronizer.insertToDB("MST_DOC", data);
	}
	
	//banyak ke 2
	private void insertZipSync() {
		List<String[]> lFile = Utility.CSVRead("MST_ZIP_SYNC.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_ZIP_SYNC", data);
		Syncronizer.insertToDB("MST_ZIP_SYNC", data);
	}
	
	private void insertObjWarnaDtlSync() {
		List<String[]> lFile = Utility.CSVRead("MST_OBJ_WARNA_DTL_SYNC.csv", context);
		Recordset data = Stream.recordCSV(lFile);
		Syncronizer.createToDB("MST_OBJ_WARNA_DTL_SYNC", data);
		Syncronizer.insertToDB("MST_OBJ_WARNA_DTL_SYNC", data);
	}

}