package com.salesforce.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.daf.activity.SettingActivity;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;
import com.salesforce.generator.action.SendAction;
import com.salesforce.generator.ui.FingerUi;
import com.salesforce.generator.ui.PictureUi;
import com.salesforce.generator.ui.SignatureUi;
import com.salesforce.stream.NfData;
import com.salesforce.utility.Utility;

import java.io.File;
import java.util.Vector;

public class SchedulerService extends Service {
    //	private NotificationCompat.Builder builder;
    public static final String BROADCAST_ACTION = "com.daf.activity.OrderPendingActivity";
    public static boolean uiActive;
    private Intent intent;
    private BroadcastReceiver brodcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            uiActive = true;
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        Log.d("e-formService",SchedulerService.class.getSimpleName() + " Service Trriggered");
        registerReceiver(brodcast, new IntentFilter("com.salesforce.service.SchedulerService"));

        SettingActivity.URL_SERVER = SettingActivity.getProductionUrl();
        SettingActivity.URL_STORAGE = ConnectionBG.DBquery("select * from SETTING ").getText(0, "STORAGE");

        Utility.setServContext(this);
        ConnectionBG.DBopen(this);
        Global.newInit();

        if (!uiActive) {
            if (Utility.sendingData == false) {
                Utility.sendingData = true;
                new runningOrder().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }


        return Service.START_NOT_STICKY;
    }

    private void sendActivity() {

        Recordset recordset = ConnectionBG.DBquery("SELECT * FROM data_activity WHERE status='0';");
        String[] s = new File(Utility.getDefaultImagePath()).list();
        StringBuffer sbuf = new StringBuffer("|");
        if (s != null) {
            for (int i = 0; i < s.length; i++) {
                sbuf.append(s[i]).append("|");
            }
        }
        String buffer = sbuf.toString();
        for (int i = 0; i < recordset.getRows(); i++) {
            String openedActivityId = Generator.stateCurrModelActivityID;
            if (uiActive) {
                Utility.sendingData = false;
                return;
            }

            NfData data = new NfData(recordset.getText(i, "data"));

            if (openedActivityId != null && openedActivityId.equals(recordset.getText(i, "activityId"))) {
                Utility.sendingData = false;
                return;
            }

            String fdate = data.getData("INIT", "FINISHDATE");
            String sdate = data.getData("INIT", "SENDDATE");
            String isTasklist = recordset.getText(i, "isTasklist");
            String applNo = recordset.getText(i, "applNo");
            String branchId = recordset.getText(i, "branchId");
            String userId = recordset.getText(i, "user");
            String orderCode = recordset.getText(i, "orderCode");

            if (fdate.contains("-")) {
                String res = "";

                //ini tuh digunakan buat log stream data
                String sbuffer = Utility.replace(recordset.getText(i, "data"), "SENDDATE=" + sdate, "SENDDATE=" + Utility.Now());
                if (!uiActive) {
                    if (Utility.getNewToken()) {
                        Utility.writeToFile("/storage/sdcard0/DAF/LOG/" + applNo + "_SEND", "SENDDATE=" + Utility.Now());
                        res = SendAction.sendActivityBackground(this, recordset.getText(i, "activityId"), recordset.getText(i, "orderCode"), recordset.getText(i, "appId"),
                                sbuffer, isTasklist, applNo, branchId, true);
                        Utility.writeToFile("/storage/sdcard0/DAF/LOG/" + applNo, "RETURNDATE=" + Utility.Now() + "\n\n\n" + recordset.getText(i, "data") + "\n\n\nbalikan service = " + res);
                        // daff 3 16:59 15/11/2018
						/*if(!res.equalsIgnoreCase("data_stream_rusak")){
							for (int j = 0; j < form.length; j++) {
								if (uiActive) {
									return;
								}
								String[] comp = nset.getData(form[j]).getObjectKeys();
								for (int k = 0; k < comp.length; k++) {
									if (uiActive) {
										return;
									}
									String header =form[j];
									try {
										header=header.substring(1,header.length()-1);
									} catch (Exception e) { }

									if (buffer.contains("|"+data.getText(header, comp[k])+"|")) {

										Utility.i("sendingPendingActivity", "sendImage:"+data.getText(header, comp[k]));
										String typeComponent = data.getText(header, comp[k]);
										Vector<String> vTypeComp = Utility.splitVector(typeComponent, "_");
										typeComponent = vTypeComp.elementAt(1);
										boolean b = false;
										if (typeComponent.equalsIgnoreCase("3")) {
											b = SignatureUi.sendImage(recordset.getText(i, "activityId"), comp[k] , data.getText(header, comp[k]), true, isTasklist,userId, applNo);
										}else if(typeComponent.equalsIgnoreCase("1") || typeComponent.equalsIgnoreCase("0")){
											b = PictureUi.sendImage(recordset.getText(i, "activityId"), comp[k] , data.getText(header, comp[k]), true, isTasklist, userId, applNo);
										}else if (typeComponent.equalsIgnoreCase("4")) {
											b = FingerUi.sendImage(recordset.getText(i, "activityId"), comp[k], data.getText(header, comp[k]), true, isTasklist, userId, applNo);
										}
									}
								}
							}
						}*/
                        //sampai sini
                    }

                    if (res.startsWith("LOGOUT")) {
                        Utility.setSetting(getApplicationContext(), "ERRORMSG", "KILL");
                    }

                    if (res.endsWith("OK")) {

                        //14/12/2018 seno
                        Nset nset = new Nset(data.getInternalObject());
                        String[] form = nset.getObjectKeys();
                        for (int j = 0; j < form.length; j++) {
                            if (uiActive) {
                                return;
                            }
                            String[] comp = nset.getData(form[j]).getObjectKeys();
                            for (int k = 0; k < comp.length; k++) {
                                if (uiActive) {
                                    return;
                                }
                                String header = form[j];
                                try {
                                    header = header.substring(1, header.length() - 1);
                                } catch (Exception e) {
                                }

                                if (buffer.contains("|" + data.getText(header, comp[k]) + "|")) {

                                    Utility.i("sendingPendingActivity", "sendImage:" + data.getText(header, comp[k]));
                                    String typeComponent = data.getText(header, comp[k]);
                                    Vector<String> vTypeComp = Utility.splitVector(typeComponent, "_");
                                    typeComponent = vTypeComp.elementAt(1);
                                    boolean b = false;
                                    if (typeComponent.equalsIgnoreCase("3")) {
                                        b = SignatureUi.sendImage(recordset.getText(i, "activityId"), comp[k], data.getText(header, comp[k]), true, isTasklist, userId, applNo);
//										System.out.println(b);
                                    } else if (typeComponent.equalsIgnoreCase("1") || typeComponent.equalsIgnoreCase("0")) {
                                        b = PictureUi.sendImage(recordset.getText(i, "activityId"), comp[k], data.getText(header, comp[k]), true, isTasklist, userId, applNo);
//										System.out.println(b);
                                    } else if (typeComponent.equalsIgnoreCase("4")) {
                                        b = FingerUi.sendImage(recordset.getText(i, "activityId"), comp[k], data.getText(header, comp[k]), true, isTasklist, userId, applNo);
//										System.out.println(b);
                                    }
                                }
                            }
                        }
                        //14/12/2018 seno
                        ConnectionBG.DBupdate("data_activity", "status=1", "where", "activityId=" + recordset.getText(i, "activityId"));
                        if (isTasklist.equalsIgnoreCase("true")) {
                            ConnectionBG.DBupdate("log_order", "sent=Y", "where", "order_id=" + orderCode);
                            Connection.DBdelete("log_order", "order_id=?", new String[]{orderCode});
                            Connection.DBdelete("trn_task_list", "order_id=?", new String[]{orderCode});
                        }

                        //10:04 04/04/2019 seno
                        if (Generator.currOrderType.equalsIgnoreCase("tasklistoffline")) {
                            Connection.DBupdate("trn_tasklist_offline", "sent=Y", "where", "orderId=" + Generator.currOrderCode);
                            Connection.DBdelete("trn_tasklist_offline", "orderId=?", new String[]{Generator.currOrderCode});
                        }

                        if (Generator.currOrderType.equalsIgnoreCase("tasklistdata")) {
                            Connection.DBdelete("trn_tasklist_data", "order_id=?", new String[]{orderCode});
                            Connection.DBdelete("trn_tasklist_data_object", "order_id=?", new String[]{orderCode});
                        }

                        // hapus file log
                        new File(Utility.getDefaultPath("LOG/" + applNo)).delete();
                        new File(Utility.getDefaultPath("LOG/" + applNo + "_SEND")).delete();
                        SendAction.deleteActivityTasklistTemporary();

                        Connection.DBdelete("data_activity", "activityId=?", new String[]{recordset.getText(i, "activityId")});
                    } else if (res.equalsIgnoreCase("Error data sudah ada")) {

                        Connection.DBdelete("data_activity", "activityId=?", new String[]{recordset.getText(i, "activityId")});

                        if (isTasklist.equalsIgnoreCase("true")) {
                            ConnectionBG.DBupdate("log_order", "sent=Y", "where", "order_id=" + orderCode);
                            Connection.DBdelete("log_order", "order_id=?", new String[]{orderCode});
                            Connection.DBdelete("trn_task_list", "order_id=?", new String[]{orderCode});
                        }

                        if (Generator.currOrderType.equalsIgnoreCase("tasklistdata")) {
                            Connection.DBdelete("trn_tasklist_data", "order_id=?", new String[]{orderCode});
                            Connection.DBdelete("trn_tasklist_data_object", "order_id=?", new String[]{orderCode});
                        }

                        //10:04 04/04/2019 seno
                        if (Generator.currOrderType.equalsIgnoreCase("tasklistoffline")) {
                            Connection.DBupdate("trn_tasklist_offline", "sent=Y", "where", "orderId=" + Generator.currOrderCode);
                            Connection.DBdelete("trn_tasklist_offline", "orderId=?", new String[]{Generator.currOrderCode});
                        }

                        SendAction.deleteActivityTasklistTemporary();
                    }
                    //13:02 14/11/2018 daff 3
                    else if (res.startsWith("data_stream_rusak")) {
                        if (Generator.isTasklist == false) {
                            //new entry
//							Connection.DBdelete("data_activity", "activityId=?",new String[]{Generator.currModelActivityID});
//							Connection.DBdelete("DRAFT", "order_id=?",new String[]{Generator.currOrderCode});
//							OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
//							new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Gagal, "+resSentAction+"\r\n Tolong collect ulang", "result=234352:OK"));
                        } else {
//							Connection.DBdelete("log_order", "order_id=?",new String[]{Generator.currOrderCode});
//							Connection.DBdelete("data_activity", "activityId=?",new String[]{Generator.currModelActivityID});
//							OrderPendingActivity.deleteCancelOrder2(Generator.currModelActivityID);
//							new ShowDialogAction().onAction(comp, new SingleRecordset("param3=Data Gagal, "+resSentAction+"\r\n Tolong collect ulang", "result=234352:OK"));
                        }
                    }
                    //sampai sini
                    else if (true && res.endsWith("SERVICEOFF")) {
                        //20/08/2014
                        Utility.setSetting(getApplicationContext(), "SERVICE", "");
                    }
                }

            }
            sendBroadcast(intent);
        }

        Utility.sendingData = false;
    }

//	public boolean isForeground(String myPackage) {
//	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//	    List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1); 
//	    ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
//	    return componentInfo.getPackageName().equals(myPackage);
//	}

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private class runningOrder extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            sendActivity();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }


    }

}
