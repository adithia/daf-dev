package com.salesforce.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import com.daf.activity.SettingActivity;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Utility;

public class GetTokenService extends Service{
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		new getToken().execute();
		return Service.START_NOT_STICKY;
	}
	
	private class getToken extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String url = SettingActivity.URL_SERVER+"createtoken?v=100";
			String jsonToken = Utility.getHttpConnection2(url);
			Nset set = Nset.readJSON(jsonToken);
			if (set.getData("status").toString().equalsIgnoreCase("ok")) {
				Recordset data = Stream.downStream(jsonToken);
				Utility.setSetting(getApplicationContext(), "Token", data.getText(0, "access_token"));
			}
			return null;
		}
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
