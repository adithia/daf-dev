package com.salesforce.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.daf.activity.SettingActivity;
import com.salesforce.R;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.ConnectionBG;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.action.SendAction;
import com.salesforce.stream.Model;
import com.salesforce.stream.Stream;
import com.salesforce.utility.Utility;

public class DownloadOrderService extends Service{
	
	 private NotificationCompat.Builder builder;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d("e-formService",DownloadOrderService.class.getSimpleName() + " Service Trriggered");
		ConnectionBG.DBopen(this);
		if (Utility.backgroundOrder == false) {
			Utility.backgroundOrder = true;
			getOrder();
		}		
		
		return Service.START_NOT_STICKY;
	}
	
	private void getOrder() {
		Nset order = Nset.newObject();
		StringBuffer sbuBuffer = new StringBuffer();
		Recordset rst = ConnectionBG.DBquery("select distinct model_order_tablename from model;");

//		25/04/2019 seno daf3
		Vector<String> header = new Vector<String>();
		Vector<Vector<String>> fields= new Vector<Vector<String>>();
		header.add(rst.getHeader(0));
		for (int i = 0; i < rst.getRows(); i++) {
			String namaTbl = rst.getText(i,0);
			if(namaTbl.contains(",")){
				Vector<String> tmp = Utility.splitVector(namaTbl, ",");
				for(int j=0; j<tmp.size(); j++){
					if(!tmp.get(j).equals("trn_tasklist_data_object")&&!tmp.get(j).equals("trn_tasklist_data")) {
						fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
					}
				}
			}else{
				fields.add(new Vector<String>(Arrays.asList(namaTbl)));
			}

		}
//		rst = null;
		rst = new Model (header,fields);
//		25/04/2019 seno selesai


		for (int i = 0; i < rst.getRows(); i++) {
			Recordset x = ConnectionBG.DBquery("select order_id from "+rst.getText(i, 0));

			for (int j = 0; j < x.getRows(); j++) {
				sbuBuffer.append(j>0?",":"").append(x.getText(j, 0));
			}
			order.setData(rst.getText(i, 0), sbuBuffer.toString());

			if (!Utility.getSetting(getApplicationContext(), "USERNAME", "").equals( Utility.getSetting(getApplicationContext(), "LASTUSER", ""))) {
				ConnectionBG.DBdelete(rst.getText(i, 0));
				ConnectionBG.DBdelete("log_order");
				ConnectionBG.DBcreate("log_order", "order_id", "opened","sent","downloaded","data");
				SendAction.deleteSentActity(0);

			}
		}
		
		new download().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	private void downloadOrder(){
		
		String tble = "TABLE_LIST_ITEM";
		Nset newMaster = Nset.newArray();
		
		String url = Utility.getURLenc(SettingActivity.URL_SERVER + "versionservlet/?userid=" ,Utility.getSetting(getApplicationContext(), "userid", "").trim(),"&master=" ,
				tble ,"&imei=" ,Utility.getImei(getApplicationContext()).toUpperCase());
		String file= Utility.getDefaultPath(tble+".m2m");
		
		try {
			if (Utility.getNewToken()) {
				if (!tble.equalsIgnoreCase("")) {
					Utility.getHttpConnection(url, file);			
					Recordset ri = Stream.downStreamJSON( new InputStreamReader( new FileInputStream(file)));
					if (ri.getCols() == 0 && ri.getRows() == 0) {
					}else if (ri.getText(0, "status").equalsIgnoreCase("ERR")) {
						Utility.copyFile(file, "/storage/sdcard0/DAF/"+tble+".m2m");
					}else{
						ConnectionBG.DBdelete(tble);
						Nset dataNewMaster = Nset.newObject();
						dataNewMaster.setData("version_name",tble);
						dataNewMaster.setData("filename",file);
						newMaster.addData(dataNewMaster);
					}
					
				}
			}
		} catch (Exception e) {}
		
		for (int i = 0; i < newMaster.getArraySize(); i++) {
			
			Recordset ri = null;
			try {
				
				ri = Stream.downStreamJSON( new InputStreamReader( new FileInputStream(newMaster.getData(i).getData("filename").toString())));				
				Syncronizer.saveToDBBG(newMaster.getData(i).getData("version_name").toString(), ri);		
//				String sdsd = (newMaster.getData(i).getData("filename").toString());
				Utility.deleteFile((newMaster.getData(i).getData("filename").toString()));
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				 	
		}		
		
		
		Nset order = Nset.newObject();
		StringBuffer sbuBuffer = new StringBuffer();
		StringBuffer newSbuBuffer = new StringBuffer(); 			//seno daf 3 10:30 12/04/2019
		Recordset rst = ConnectionBG.DBquery("select distinct model_order_tablename from model;");
		
//		25/04/2019 seno daf3
		Vector<String> header = new Vector<String>();
		Vector<Vector<String>> fields= new Vector<Vector<String>>();
		header.add(rst.getHeader(0));
		for (int i = 0; i <rst.getRows() ; i++) {
			String namaTbl = rst.getText(i,0);
			if(namaTbl.contains(",")){
				Vector<String> tmp = Utility.splitVector(namaTbl, ",");
				for(int j=0; j<tmp.size(); j++){
					fields.add(new Vector<String>(Arrays.asList(tmp.get(j))));
				}
			}else{				
				fields.add(new Vector<String>(Arrays.asList(namaTbl)));
			}
			
		}		
//		rst = null;
		rst = new Model (header,fields);	
//		25/04/2019 seno selesai
						
		for (int i = 0; i < rst.getRows(); i++) {
			Recordset x = ConnectionBG.DBquery("select order_id from "+rst.getText(i, 0));
			//seno 13:31 23/04/2019 daf3
			if(sbuBuffer.length()>0){
				sbuBuffer.delete(0, sbuBuffer.length()); 
			}	
			newSbuBuffer.append(newSbuBuffer.length()>0?"|":"").append(rst.getText(i, 0)+"=");			//seno daf 3 10:30 12/04/2019
			for (int j = 0; j < x.getRows(); j++) {
				sbuBuffer.append(j>0?",":"").append(x.getText(j, 0));
				newSbuBuffer.append(j>0?",":"").append(x.getText(j, 0)); 			//seno daf 3 10:30 12/04/2019
			}
			order.setData(rst.getText(i, 0), sbuBuffer.toString());
			
			if (!Utility.getSetting(getApplicationContext(), "USERNAME", "").equals( Utility.getSetting(getApplicationContext(), "LASTUSER", ""))) {
				ConnectionBG.DBdelete(rst.getText(i, 0));
				ConnectionBG.DBdelete("log_order");		
				ConnectionBG.DBcreate("log_order", "order_id", "opened","sent","downloaded","data");
				SendAction.deleteSentActity(0);
				
			}			
		}

		SendAction.deleteSentActity(0, Utility.Now().substring(0, 10));
		String sUrl = Utility.getURLenc(SettingActivity.URL_SERVER + "orderservlet/?action=nikita&order=" ,order.toJSON(),"&userid=",Utility.getSetting(this, "userid", ""),"&flg=","service");
	 
		if (Utility.getNewToken()) {
			String oldAPplno = order.toString();

			sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/"+ "f=service", new String[] { "action", "order", "userid"  ,"flg", "token"},
					new String[] { "nikita", order.toJSON(), Utility.getSetting(this, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "") });//"&versi="  ,SettingActivity.APP_VERSION

			order = Nset.readJSON(sUrl);

			boolean breceived=false;
//			boolean newTasklist=false, isInsert = false;

			Map<String,Boolean> mapNotif = new HashMap<>();
			for (int i = 0; i < order.getArraySize(); i++) {
				String table = order.getData(i).getData("table").toString();
				mapNotif.put(table,false);
				for (int j = 0; j < order.getData(i).getData("data").getArraySize(); j++) {
					String code = order.getData(i).getData("data").getData(j).getData("code").toString();
					String drst = order.getData(i).getData("data").getData(j).getData("data").toString();
					Recordset xxx = Stream.downStream(drst);
					if (code.equals("truncate")) {
						if (xxx.getRows() > 0) {
							for(int k = 0; k < xxx.getRows(); k++){
								String order_id = xxx.getText(k,"order_id");
								String appl_no = xxx.getText(k,"appl_no");
								if(order_id!=""&&!oldAPplno.contains(order_id)){
//									newTasklist = true;
									mapNotif.put(table,true);
								}
								if(appl_no!=""&&!oldAPplno.contains(order_id)){
//									newTasklist = true;
									mapNotif.put(table,true);
								}
							}
						}
						Syncronizer.saveToDBBG(table, xxx);
					}else if (code.equals("insert")) {
						Syncronizer.createToDBBG(table, xxx);
						Syncronizer.insertToDBBG(table, xxx);
					}else if (code.equals("delete")) {
						Syncronizer.createToDBBG(table, xxx);
						Syncronizer.deleteToDBBG(table, xxx);
					}else if (code.equals("update")) {
						Syncronizer.createToDBBG(table, xxx);
						Syncronizer.deleteToDBBG(table, xxx);
						Syncronizer.insertToDBBG(table, xxx);
					}
					deleteExpiredTasklit(table);
					if (xxx.getRows()>=1) {
						breceived=true;
					}
				}

			}

			Recordset tabel = null;
			StringBuffer orderNew = new StringBuffer();
			StringBuffer orderNew2 = new StringBuffer();			//seno daf 3 10:30 12/04/2019
			for (int i = 0; i < rst.getRows(); i++) {
				tabel = Connection.DBquery("select order_id from "+rst.getText(i, 0));
				orderNew2.append(orderNew2.length()>0?"|":"").append(rst.getText(i, 0)+"=");			//seno daf 3 10:30 12/04/2019
				for (int j = 0; j < tabel.getRows(); j++) {
					orderNew.append(j>0?",":"").append(tabel.getText(j, 0));
					orderNew2.append(j>0?",":"").append(tabel.getText(j, 0));			//seno daf 3 10:30 12/04/2019
				}
			}

//			if (isInsert) {
//				Utility.orderNew = orderNew.toString();
//				Utility.orderOld = sbuBuffer.toString();
//				insertRecievedDate(orderNew.toString(), sbuBuffer.toString());
//			}
			//seno 13:31 23/04/2019 daf3
			insertRecievedDate2(orderNew2.toString(), newSbuBuffer.toString()); 			//seno daf 3 10:30 12/04/2019

//			if (breceived) {
//				Nset order_id = Nset.newObject();
//				order_id.setData("trn_task_list", sbuBuffer.toString());
//				if (Utility.getNewToken()) {
//					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
//							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
//				}else{
//					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
//							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
//				}
//
//
//				if (newTasklist) {
//					setNotificationAlarm("Tasklist baru berhasil didownload");
//				}
//			}


			//seno daf 3 10:30 12/04/2019
			if(breceived){
				Nset order_id = Nset.newObject();
				Vector<String> vSBuffer = Utility.splitVector(newSbuBuffer.toString(), "|");
				for(int i=0 ; i<vSBuffer.size(); i++){
					String table = vSBuffer.get(i).substring(0,vSBuffer.get(i).indexOf("="));
					String data = vSBuffer.get(i).substring(vSBuffer.get(i).indexOf("=")+1);
					order_id.setData(table, data);
				}
				if (Utility.getNewToken()) {
					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", ""), "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
				}else{
					sUrl  = Utility.postMultipart(SettingActivity.URL_SERVER + "orderservlet/", new String[] { "action", "order", "userid"  ,"flg", "token"},
							new String[] { "RECEIVED", order_id.toJSON(), Utility.getSetting(this, "userid", "") , "service", Utility.getSetting(Utility.getAppContext(), "Token", "")});
				}

				int codeNotif = 0;
				for(Map.Entry<String,Boolean> singleData : mapNotif.entrySet()){
					if (singleData.getValue()) {
						if(singleData.getKey().equals("trn_task_list")){
							setNotificationAlarm("Tasklist baru berhasil didownload",codeNotif);
						}else if(singleData.getKey().equals("trn_tasklist_data")){
							setNotificationAlarm("Tasklist Data baru berhasil didownload",codeNotif);
						}
//						else{
//							setNotificationAlarm(singleData.getKey()+" baru berhasil didownload",codeNotif);
//						}

//						setNotificationAlarm("Tasklist baru berhasil didownload");
					}
					codeNotif++;
				}
//
////				if (newTasklist) {
////					setNotificationAlarm("Tasklist baru berhasil didownload");
////				}
//
			}
			//seno daf 3 10:30 12/04/2019




		}
		Utility.backgroundOrder = false;
	}
	
//	private static void insertRecievedDate(String orderNew, String orderOld){
//		Vector<String> vOrderNew = Utility.splitVector(orderNew, ",");
//		Vector<String> vOrderOld = Utility.splitVector(orderOld, ",");
//		Connection.DBopen();
//		if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
//			for (int i = 0; i < vOrderNew.size(); i++) {
//				if (!Utility.isContainValue(vOrderNew.elementAt(i), vOrderOld)) {
//					Connection.DBupdate("trn_task_list", "received_date="+Utility.getTodayDate2(), "where", "order_id="+vOrderNew.elementAt(i));
//				}
//			}
//		}else{
//			for (int i = 0; i < vOrderNew.size(); i++) {
//				Connection.DBupdate("trn_task_list", "received_date="+Utility.getTodayDate2(), "where", "order_id="+vOrderNew.elementAt(i));
//			}
//		}
//	}
	
	
private static void insertRecievedDate2(String orderNew, String orderOld){

		
		Vector<String> vOrderNew = Utility.splitVector(orderNew, "|");
		Vector<String> vOrderOld = Utility.splitVector(orderOld, "|");
		Connection.DBopen();
		for(int i=0; i<vOrderNew.size(); i++){
			String table_name = vOrderNew.get(i).substring(0,vOrderNew.get(i).indexOf("="));
//			Vector<String> tmpVOrderNew = Utility.splitVector(vOrderNew.get(i).substring(vOrderNew.get(i).indexOf("=")+1), ",");
//			Vector<String> tmpVOrderold = Utility.splitVector(vOrderNew.get(i).substring(vOrderNew.get(i).indexOf("=")+1), ",");
			
//			System.out.println(tmpVOrderNew+"\n"+tmpVOrderold);
//				Connection.DBopen();		
				if (!orderOld.equalsIgnoreCase("") && vOrderNew.size() > vOrderOld.size()) {
					for (int j = 0; j < vOrderNew.size(); j++) {
						if (!Utility.isContainValue(vOrderNew.elementAt(j), vOrderOld)) {
							Connection.DBupdate(table_name, "received_date="+Utility.getTodayDate2(), "where", "order_id="+vOrderNew.elementAt(j));
						}
					}
				}else{
					for (int j = 0; j < vOrderNew.size(); j++) {
						Connection.DBupdate(table_name, "received_date="+Utility.getTodayDate2(), "where", "order_id="+vOrderNew.elementAt(j));
					}
				}				
			}
	}
	
	
	//untuk mendelete tasklist yg expired
	private static void deleteExpiredTasklit(String table){
		Recordset data = Connection.DBquery("SELECT order_id, expired_date FROM "+table);
		if (data.getRows() > 0) {
			for (int i = 0; i < data.getRows(); i++) {
				if (!data.getText(i, "expired_date").equalsIgnoreCase("")) {
					Calendar cal = convertTasklits(data.getText(i, "expired_date"));				
					Date today = new Date();
					if (today.after(cal.getTime())) {
						Connection.DBdelete(table, "order_id=?", new String[]{data.getText(i, "order_id")});
					}	
				}					
			}
		}
	}
	
	public static Calendar convertTasklits(String strDate){
		Calendar calendar = Calendar.getInstance();
		try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH);
			date = sdf.parse(strDate);
			calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return calendar;
	}
	
	private void setNotificationAlarm( String msg,int id){
		NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);		
		builder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.notif48)
                .setContentTitle("e-Form Notification")
                .setContentText(msg)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setDefaults(Notification.DEFAULT_ALL);
		nm.notify(id, builder.build());
	}
	
	private class download extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			downloadOrder();
			return null;
		}
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
