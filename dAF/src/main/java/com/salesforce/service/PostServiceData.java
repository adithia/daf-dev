package com.salesforce.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.salesforce.utility.Utility;

public class PostServiceData<T> extends AsyncTask<Void, Void, String> {

//	private int timeOut = 60000;	
	private ProgressDialog pDialog;	
	private Context context;
	private String url;
	private JSONObject jsonObject;
	
	public PostServiceData(Context context, JSONObject jsonObject, String url){
		this.context = context;
		this.jsonObject = jsonObject;
		this.url = url;
		
	}
	
	@Override
	protected void onPreExecute() {
		pDialog = Utility.showProgresbar(context, "Loading...");
		pDialog.setCancelable(false);
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		URL object;
		try {
			object = new URL(url);

			HttpURLConnection con;
			try {
				con = (HttpURLConnection) object.openConnection();

				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setRequestProperty("Accept", "application/json");
				con.setRequestMethod("POST");
				
				JSONObject cred   = jsonObject;//new JSONObject();
//				
//				for (int i = 0; i < keys.length; i++) {				
//					cred.put(keys[i],data[i]);
//				}
				
				
				OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
				wr.write(cred.toString());
				wr.flush();

				//display what returns the POST request

				StringBuilder sb = new StringBuilder();  
				int HttpResult = con.getResponseCode(); 
				if (HttpResult == HttpURLConnection.HTTP_OK) {
				    BufferedReader br = new BufferedReader(
				            new InputStreamReader(con.getInputStream(), "utf-8"));
				    String line = null;  
				    while ((line = br.readLine()) != null) {  
				        sb.append(line + "\n");  
				    }
				    br.close();
				    
				    return sb.toString();
				} else {
				    System.out.println(con.getResponseMessage());  
				}  
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result != null) {
			onResultListener(result);
		}else {
			onErrorResultListener(result);
		}
		
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}
	}
	
	public void onResultListener(String response) {

	}
	
	public void onErrorResultListener(String response) {
		
	}

}
