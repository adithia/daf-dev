package com.salesforce.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.daf.activity.SettingActivity;
import com.salesforce.database.Nset;
import com.salesforce.utility.GPSTracker;
import com.salesforce.utility.Utility;

public class SendLocation extends Service{
	StringBuffer sBuff = new StringBuffer();
	GPSTracker gpsTracker;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d("e-formService",SendLocation.class.getSimpleName() + " Service Trriggered");
		if (Utility.backgroundLocation == false) {

			Utility.backgroundLocation = true;
			gpsTracker = new GPSTracker(getApplicationContext());
			captureLocation();
			new sendLocation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
		
		return Service.START_NOT_STICKY;
	}
	
	private void captureLocation(){
		double latitude; //= Utility.latitude;//LocationService.getLatitude();
		double longitude;// = Utility.longitude;//LocationService.getLongitude();
		
		
		gpsTracker.getLocation(getApplicationContext());
		if (gpsTracker.canGetLocation()) {
			latitude = gpsTracker.getLatitude();
			longitude = gpsTracker.getLongitude();
		}else{
			latitude = Utility.latitude;;
			longitude = Utility.longitude;;
		}
		
		if (latitude == 0 && longitude == 0) {
			Utility.backgroundLocation = false;
			return;
		}
		
		Nset nset = Nset.newArray();
		nset.addData(Utility.Now());
		nset.addData(String.valueOf(latitude));
		nset.addData(String.valueOf(longitude));

		if (sBuff.length() == 0)
			sBuff.append(nset.toJSON());
		else
			sBuff.append("," + nset.toJSON());

		Utility.setSetting(getApplicationContext(), "POSITION", sBuff.toString());
	}
	
	private String getSavedPosition() {
		return Utility.getSetting(getApplicationContext(), "POSITION", "");
	}

	private void clearSavedPosition() {
		Utility.setSetting(getApplicationContext(), "POSITION", "");
	}
	
	private class sendLocation extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			sendLocation();
			return null;
		}
		
	}
	
	private void sendLocation(){
		String data = "[" + getSavedPosition() + "]";

		String response = "";
		if (Utility.getNewToken()) {
			response = Utility.postMultipart(SettingActivity.URL_SERVER + "gpsservlet/?f=service", new String[] { "userid", "imei", "data", "token" },
					new String[] { Utility.getSetting(getApplicationContext(), "USERNAME", ""), Utility.getSetting(getApplicationContext(), "IMEI", ""), data, Utility.getSetting(Utility.getAppContext(), "Token", "") });
		}	
		
		if (response != null && response.startsWith("LOGOUT")) {
			Utility.setSetting(getApplicationContext(), "ERRORMSG","KILL");
		 }
		
		if (response != null && response.endsWith("OK")) {
			clearSavedPosition();
			sBuff.setLength(0);
		}else if (response != null && response.endsWith("SERVICEOFF")) {
			//20/08/2014
			Utility.setSetting(getApplicationContext(), "SERVICE", "" );
		}
		

		Utility.backgroundLocation = false;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
