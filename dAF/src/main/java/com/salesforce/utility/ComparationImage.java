package com.salesforce.utility;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.error.ANError;
import com.androidnetworking.model.MultipartFileBody;
import com.salesfoce.ssl.EasySSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import java.io.File;

import okhttp3.OkHttpClient;

public class ComparationImage {
    public static String comparationTwoImage(String path1, String path2) {
        HttpEntity resEntity;
        String result = "";
        try {
            HttpClient client = new DefaultHttpClient();

            //13kcrazy
            /*SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

            HttpParams params = new BasicHttpParams();
            params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


            params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setConnectionTimeout(params, 0);//asli 0
            HttpConnectionParams.setSoTimeout(params, 0);//asli 0
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
            client = new DefaultHttpClient(cm, params);*/
            //13kcrazy

            HttpPost post = new HttpPost("https://api.advance.ai/openapi/face-recognition/v3/check");
            post.setHeader("X-ADVAI-KEY", "704aaee47e9cb031");
            post.setHeader(HttpHeaders.CONTENT_TYPE, "multipart/form-data");

            File a = new File(path1);
            File b = new File(path2);

            if (a.exists() && b.exists()) {
                ContentBody cb1 = new FileBody(a);
                ContentBody cb2 = new FileBody(b);
                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                reqEntity.addPart("firstImage", cb1);
                reqEntity.addPart("secondImage", cb2);

                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);

                resEntity = response.getEntity();
                result = EntityUtils.toString(resEntity);

                if (resEntity != null) {
                    //Log.i("postHttpConnectionWithPicture", result);

                }
            }

        } catch (Exception ex) {
            Log.e(ComparationImage.class.getSimpleName(), "error: " + ex.getMessage(), ex);
            return "Connection Timeout";
        }
        //Log.i("postHttpConnectionWithPicture", result);
        return result != null ? result : "Connection Timeout";
    }

    public static String comparationTwoImage2(String path1, String path2) {
        String result = "";
        try {
            File a = new File(path1);
            File b = new File(path2);
            if (a.exists() && b.exists()) {
                ANRequest request = AndroidNetworking.upload("https://api.advance.ai/openapi/face-recognition/v3/check")
                        .addHeaders("X-ADVAI-KEY", "704aaee47e9cb031")
                        .addHeaders("Content-Type", "multipart/form-data")
                        .addMultipartFile("firstImage", a)
                        .addMultipartFile("secondImage", b)
                        .build();
                ANResponse response = request.executeForString();
                result = response.getResult().toString();
            }
        } catch (Exception ex) {
            Log.e(ComparationImage.class.getSimpleName(), "error: " + ex.getMessage(), ex);
            return "Connection Timeout";
        }
        //Log.i("postHttpConnectionWithPicture", result);
        return result != null ? result : "Connection Timeout";
    }
}

