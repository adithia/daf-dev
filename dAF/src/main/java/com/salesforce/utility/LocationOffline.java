package com.salesforce.utility;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationOffline implements LocationListener{

	public static double latitude = 0;
	public static double longitude = 0;
	
	public LocationOffline(Context context){
		LocationManager loc = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		loc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		Utility.latitude = location.getLatitude();
		Utility.longitude = location.getLongitude();
		Utility.Accuracy = location.getAccuracy();
		latitude = location.getLatitude();
		longitude = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
