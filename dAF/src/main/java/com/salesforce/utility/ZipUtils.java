package com.salesforce.utility;

public class ZipUtils {

	/**
	 * Execute a command
	 * @param command command string
	 * @return return code
	 */
	public static native int executeCommand(String command);
	
	/**
	 * load native library
	 */
	static {
		
		try {
			System.loadLibrary("p7zip");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
