package com.salesforce.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Connection {
	private static final String dbName="data.db";
	private static SQLiteDatabase mConn ;
	private static String dbVersion="24";
	
	public static String getSQLfile(){
		return  Utility.getDefaultPath(dbName);
	}	
	public static void openConnection(Context context){
		if (!Utility.getSetting(context, "DBVESION", "").equals(dbVersion)) {
			copyDB(context);
			Utility.setSetting(context, "DBVESION", dbVersion);
		}		

		if (!checkDB()) {
			copyDB(context);
		}
		openConnection();
	}
	public static void openConnection(){
//		Log.i("Connection", "openConnection");
		if (mConn==null) {
			openDB();
		}else if (!mConn.isOpen()) {
			openDB();
		}
	}
	private static void openDB(){
//		Log.i("Connection", "openConnection true");
		mConn=SQLiteDatabase.openDatabase(getSQLfile(), null, SQLiteDatabase.OPEN_READWRITE);
	}
	public static boolean checkDB() {
		
//		Log.i("Connection", "check database");
		try {
			SQLiteDatabase db=SQLiteDatabase.openDatabase(getSQLfile(), null, SQLiteDatabase.OPEN_READWRITE);
			db.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public static void copyDB(Context context) {
//		Log.i("Connection", "copy database");
		Utility.createFolder(Utility.getDefaultPath());
		try {
			InputStream is =  context.getAssets().open(dbName);
			OutputStream os = new FileOutputStream(getSQLfile());

			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception e) {}
	}

	public static boolean RemoveDatabase(){
		return new File(getSQLfile()).delete();
	}
	public static void closeConnection(){
		try {
			mConn.close();
		} catch (Exception e) {}
	}
	public static Cursor Query(String sql ){
		try {
			return mConn.rawQuery(sql, null);
		} catch (Exception e) {return null;}	
	}
	public static boolean EOF(Cursor rst){
		try {
			if (rst==null) {
				return !false;
			}else{
				return !rst.moveToNext();
			}
		} catch (Exception e) { return !false; }
	}
	public static void Update(String table, ContentValues initialValues, String whereClause, String whereArgs[]){
		try {
			mConn.update(table, initialValues, whereClause, whereArgs);
		} catch (Exception e) {}		
	}
	public static long Insert(String table, ContentValues contentvalues){
		try {
			return mConn.insert(table, "", contentvalues);
		} catch (Exception e) {return 0;}	
	}
	public static void Delete(String table, String whereClause, String[] whereArgs){
		try {
			mConn.delete(table, whereClause, whereArgs);
		} catch (Exception e) {}	
	}
}
