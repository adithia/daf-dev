package com.salesforce.utility;

public class ModelDeviceIdentity {
    private String imei = "";
    private Boolean isAndroid10 = false;
    private String uuid = "";

    public ModelDeviceIdentity(String imei, Boolean isAndroid10, String uuid) {
        this.imei = imei;
        this.isAndroid10 = isAndroid10;
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getImei() {
        return this.imei;
    }

    public Boolean getIsAndroid10() {
        return this.isAndroid10;
    }
}
