package com.salesforce.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.salesforce.R;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.Global;

public class Helper {
	
	public static void setInflaterIconHardcode(View view, String appId, String menuName){
		setIcon(view, appId, menuName);
		setSizeOrder(view, appId, menuName);
	}

	private static void setIcon(View view, String appName, String menuName){
		FrameLayout notifLayout = (FrameLayout) view.findViewById(R.id.notif_layout);
		
		if (menuName.equals("Order")) {			
			((ImageView)view.findViewById(R.id.imageView1)).setImageResource(R.drawable.icon_order);		
		} else if (menuName.equals("Data Delivered")) { //		
			((ImageView)view.findViewById(R.id.imageView1)).setImageResource(R.drawable.icon_data_delivered);	
		} else if (menuName.equals("Data Pending")) { 			
			((ImageView)view.findViewById(R.id.imageView1)).setImageResource(R.drawable.icon_data_pending);		
		} else if (menuName.equals("Setting")) {
			((ImageView)view.findViewById(R.id.imageView1)).setImageResource(R.drawable.icon_setting);
			notifLayout.setVisibility(View.GONE);
		} else if (menuName.equals("Change Password")) {
			((ImageView)view.findViewById(R.id.imageView1)).setImageResource(R.drawable.icon_change_password);
			notifLayout.setVisibility(View.GONE);
		} else if (menuName.equals("Profile")) {
			notifLayout.setVisibility(View.GONE);
		} else if (menuName.equals("Sign Out")) {
			((ImageView)view.findViewById(R.id.imageView1)).setImageResource(R.drawable.icon_sign_out);
			notifLayout.setVisibility(View.GONE);
		} else 
			notifLayout.setVisibility(View.GONE);
		
		((ImageView)view.findViewById(R.id.imageView1)).setScaleType(ScaleType.CENTER_INSIDE);
	}
	
	private static void setSizeOrder(View viewGroup, String appId, String menuName){
		TextView txtCount = (TextView) viewGroup.findViewById(R.id.txt_count);
		FrameLayout notifLayout = (FrameLayout) viewGroup.findViewById(R.id.notif_layout);
		Recordset record;
		
		notifLayout.setVisibility(View.VISIBLE);
		if (menuName.equals("Order")) {			
			//txtCount.setText(countSizeOrder(appId));	
			if (Utility.getInt(txtCount.getText().toString())==0) {
				notifLayout.setVisibility(View.GONE);
			}
		} else if (menuName.equals("Data Delivered")) {	
			record = Connection.DBquery("SELECT orderCode FROM data_activity WHERE AppId = '" + Generator.currApplication + "' and status = '1'");
			txtCount.setText(String.valueOf(record.getRows()));	
			if (Utility.getInt(txtCount.getText().toString())==0) {
				notifLayout.setVisibility(View.GONE);
			}
		} else if (menuName.equals("Data Pending")) { 			
			record = Connection.DBquery("SELECT orderCode FROM data_activity WHERE AppId = '" + Generator.currApplication + "' and status = '0'");
			txtCount.setText(String.valueOf(record.getRows()));
			if (Utility.getInt(txtCount.getText().toString())==0) {
				notifLayout.setVisibility(View.GONE);
			}
		} else 
			notifLayout.setVisibility(View.GONE);
		
	}
	
	private static String countSizeOrder(String appId){
		
		//mengambil list param order dari global param
		Vector<String> param = Utility.splitVector(Global.getText("order." + appId + ".list"), "|");
		Vector<String> jmlParam = new Vector<String>();
		Vector<String> jmlListParam = new Vector<String>();
		int jml = 0;
		Vector<LinkedHashMap<String, String>> vMap = Utility.getRecordWithSeparate(param, ";");
		
		for (int i = 0; i < vMap.size(); i++) {
			Map<String, String> vMapping = (HashMap<String, String>)vMap.elementAt(i);
			List<Entry<String,String>> listOrderView = new ArrayList<Map.Entry<String, String>>(vMapping.entrySet());
			for (int j = 0; j < listOrderView.size(); j++) {
				Map.Entry<String, String> entry = listOrderView.get(j);
				String label = String.valueOf(entry.getKey());
				jmlParam.add(label);
			}
		}
		
		for (int i = 0; i < jmlParam.size(); i++) {
			Recordset record = Connection.DBquery(Global.getText("order." + appId + ".list." + jmlParam.elementAt(i)));
			jmlListParam = getRecordActivityActive(record, appId, null);			
			if (i == 0) {
				jml = jmlListParam.size();
			}else {
				jml = jml + jmlListParam.size();
			}
		}
		
		return String.valueOf(jml);
		
	}

	public static Vector<String> getRecordActivityActive(Recordset data, String appName, Vector<String> orderNew){
		Vector<String> vector = new Vector<String>();
		
		if (orderNew != null) {
			orderNew = Utility.splitVector(Global.getText("order." + appName + ".new"), "|");
			if (orderNew.size() > 0) {
				for (int i = 0; i < orderNew.size(); i++) {
					vector.add(orderNew.elementAt(i).toString());
				}
			}
		}
		
		for (int i = 0; i < data.getCols(); i++) {
			for (int j = 0; j < data.getRows(); j++) {
				if (Global.getText("order." + appName + ".activities").equalsIgnoreCase("1")) {
					boolean isThere = Utility.isAvailableRecordTable("data_activity", "orderCode", data.getText(j, i));
					if (!isThere) {
						vector.add(data.getText(j, i));
					}
				}else {
					vector.add(data.getText(j, i));
				}
			}
		}
		return vector;
	}
	
	public static String releaseTag(String tag, boolean appId, boolean appName){
		if (appName) {
			return tag = tag.substring(tag.indexOf("|")+1);
		}else if (appId) {
			return tag = tag.substring(0, tag.indexOf("|"));
		}else {
			return tag;
		}
	}
}
