package com.salesforce.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.util.Log;

import com.lowagie.text.Annotation;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.VerticalPositionMark;
import com.salesforce.database.Nikitaset;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;

public class ClassPrinter {

	public static final Font FONT = new Font(Font.HELVETICA, 12, Font.NORMAL,
			GrayColor.GRAYWHITE);

	public static String createDocument(Nset data) {
		String buss_unit = data.getData("buss_unit").toString();
		String path = Utility.getDefaultPath("PO");
		String filename = "";
		
		if (buss_unit.equalsIgnoreCase("NMC")) {
			processCreatePO(data);
			filename = path + "/PO.pdf";
		} else if (buss_unit.equalsIgnoreCase("REFI")) {
			processCreatePOUfi(data);
			filename = path + "/PO_UFI.pdf";
		} else if (buss_unit.equalsIgnoreCase("MPF")) {
			processCreatePOSpektra(data);
			filename = path + "/PO_SPEKTRA.pdf";
		}
		
		return filename;
	}

	public static Image getWatermarkedImage(PdfContentByte cb, Image img,
			String watermark) throws DocumentException {
		float width = img.getScaledWidth();
		float height = img.getScaledHeight();
		PdfTemplate template = cb.createTemplate(width, height);
		template.addImage(img, width, 0, 0, height, 0, 0);
		ColumnText.showTextAligned(template, Element.ALIGN_CENTER, new Phrase(
				watermark, FONT), width / 2, height / 2, 30);
		return Image.getInstance(template);
	}

	private static void processCreatePO(Nset data) {
		Document doc = new Document(PageSize.A4, 8.0f, 8.0f, 8.0f, 8.0f);
		String path = Utility.getDefaultPath("PO");

		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();

		String strDocument = data.getData("document").toString();
		String noPO = data.getData("NOPO").toString();
		Recordset rsRecordset = Nikitaset.getRecordset(Nset.newObject()
				.setData("header", data.getData("fieldnametasklist"))
				.setData("data", data.getData("contenttasklist")));
		Recordset rsObj = Nikitaset.getRecordset(Nset.newObject()
				.setData("header", data.getData("fieldnametasklistobject"))
				.setData("data", data.getData("contenttasklistobject")));
		Recordset recTtd = Nikitaset.getRecordset(Nset.newObject()
				.setData("header", data.getData("fieldnametandatangan"))
				.setData("data", data.getData("contenttandatangan")));
		FileOutputStream fOut = null;
		try {
			File file = new File(dir, "PO.pdf");
			fOut = new FileOutputStream(file);
			PdfWriter writer = PdfWriter.getInstance(doc, fOut);
			doc.open();

			// watermark
			File f = new File(dir, "watermark.png");
			if (!f.exists())
				try {

					InputStream is = Utility.getAppContext().getAssets()
							.open("watermark.png");
					int size = is.available();
					byte[] buffer = new byte[size];
					is.read(buffer);
					is.close();

					FileOutputStream fos = new FileOutputStream(f);
					fos.write(buffer);
					fos.close();

					Utility.compressImage(f.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}

			// PdfContentByte cb = writer.getDirectContentUnder();
			// doc.add(getWatermarkedImage(cb, Image.getInstance(f.getPath()),
			// ""));
			PdfContentByte canvas = writer.getDirectContentUnder();
			Image img = Image.getInstance(f.getPath());
			// image.scaleAbsolute(PageSize.A4.rotate());
			img.scaleAbsolute(500f, 200f);
			// img.setRotation(125f);
			img.setAbsolutePosition(50, 350);
			canvas.addImage(img);

			// tab
			Chunk tableft = new Chunk(new VerticalPositionMark(), 400f, false);
			Chunk tabsemicolon = new Chunk(new VerticalPositionMark(), 90f,
					false);
			// Chunk afterSemiColom = new Chunk(new VerticalPositionMark(), 50f,
			// false);
			Chunk tabmidle = new Chunk(new VerticalPositionMark(), 200f, false);
			Chunk semicolon = new Chunk(": ");

			Chunk tabTblNo = new Chunk(new VerticalPositionMark(), 30f, false);
			Chunk tabTblType = new Chunk(new VerticalPositionMark(), 140f,
					false);
			Chunk tabTblMerk = new Chunk(new VerticalPositionMark(), 230f,
					false);
			Chunk tabTblModel = new Chunk(new VerticalPositionMark(), 380f,
					false);
			Chunk tabTblQty = new Chunk(new VerticalPositionMark(), 480f, false);

			// Font
			// Small Font 10.0
			Font smallFont = new Font(Font.HELVETICA);
			smallFont.setSize(6.0f);

			// Big Font 20.0
			// Font bigFont = new Font(Font.HELVETICA);
			// bigFont.setSize(15.0f);
			// bigFont.setStyle(Font.BOLD);

			// Standart Font 12.0
			Font standartFont = new Font(Font.HELVETICA);
			standartFont.setSize(8.0f);

			Font font14 = new Font(Font.HELVETICA);
			font14.setSize(10.0f);

			Paragraph header = new Paragraph();
			header.setFont(standartFont);
			Chunk pt_astra = new Chunk(rsRecordset.getText(0, "coy_name"));
			header.add(pt_astra);
			header.add(tableft);

			Chunk date = new Chunk("Date : ");
			Chunk date2 = new Chunk(Utility.getDateStandart());
			header.add(date);
			header.add(date2);

			Paragraph header2 = new Paragraph();
			header2.setFont(standartFont);

			Chunk kota = new Chunk(rsRecordset.getText(0, "office_name"));
			header2.add(kota);
			header2.add(tableft);

			Chunk Halaman = new Chunk("Hal : ");
			Chunk jml = new Chunk("1");
			header2.add(Halaman);
			header2.add(jml);

			Paragraph border = new Paragraph();
			Chunk bord;
			for (int i = 0; i < 143; i++) {
				bord = new Chunk("-");
				border.add(bord);
			}
			border.add(Chunk.NEWLINE);
			Paragraph judul = new Paragraph();
			judul.setFont(font14);
			judul.add(tabmidle);
			Chunk cJudul = new Chunk("PERSETUJUAN PEMBIAYAAN");
			judul.add(cJudul);

			judul.add(Chunk.NEWLINE);
			judul.add(Chunk.NEWLINE);

			Paragraph placeNdate = new Paragraph();
			placeNdate.setFont(standartFont);

			Chunk cPlace = new Chunk(rsRecordset.getText(0, "office_city")
					+ ", ");
			Chunk cDate = new Chunk(rsRecordset.getText(0, "approve_date"));
			placeNdate.add(cPlace);
			placeNdate.add(cDate);
			placeNdate.add(Chunk.NEWLINE);

			Chunk lblNoAppl = new Chunk("Nomor");
			Chunk noAppl = new Chunk(noPO);
			placeNdate.add(lblNoAppl);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add(semicolon + " ");
			placeNdate.add(noAppl);
			placeNdate.add(Chunk.NEWLINE);

			Chunk lblPerihal = new Chunk("Perihal");
			Chunk perihal = new Chunk("Persetujuan Pembiayaan / ");
			Chunk noAppLabel = new Chunk("No. Aplikasi : ");
			placeNdate.add(lblPerihal);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add(semicolon + " ");
			placeNdate.add(perihal);
			placeNdate.add(noAppLabel);
			placeNdate.add(rsRecordset.getText(0, "appl_no"));
			placeNdate.add(Chunk.NEWLINE);

			Chunk lblyth = new Chunk("Kepada YTH");
			Chunk yth = new Chunk(rsRecordset.getText(0, "supl_name"));
			Chunk supl_add = new Chunk(rsRecordset.getText(0, "supl_address1"));// tambah
																				// alamat
																				// dealer
																				// +"\n\t\t\t\t\t\t\t\t\t\t\t\t"
			placeNdate.add(lblyth);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add(semicolon + " ");
			placeNdate.add(yth);
			placeNdate.add(Chunk.NEWLINE);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add("   " + supl_add);
			placeNdate.add(Chunk.NEWLINE);
			// for (int i = 0; i < 5; i++) {
			placeNdate.add(tabsemicolon);
			// }
			Chunk supl_city = new Chunk(rsRecordset.getText(0, "supl_city"));
			placeNdate.add("   " + supl_city);

			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(Chunk.NEWLINE);

			Chunk hormat = new Chunk("Dengan Hormat,");
			placeNdate.add(hormat);
			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(Chunk.NEWLINE);

			Paragraph pPemberitahuan = new Paragraph();
			pPemberitahuan.setFont(standartFont);
			Chunk pembertitahuan = new Chunk(
					"Dengan ini kami beritahukan, bahwa kami telah menyetujui permohonan pembiayaan dari customer :");
			pPemberitahuan.add(pembertitahuan);
			pPemberitahuan.add(Chunk.NEWLINE);

			Paragraph dataDiri = new Paragraph();
			dataDiri.setFont(standartFont);

			Chunk lblNama = new Chunk("Nama");
			Chunk nama = new Chunk(rsRecordset.getText(0,
					"nama_lengkap_ktp_cust"));
			dataDiri.add(lblNama);
			// for (int i = 0; i < 4; i++) {
			dataDiri.add(tabsemicolon);
			// }
			dataDiri.add(semicolon + " ");
			dataDiri.add(nama);
			dataDiri.add(Chunk.NEWLINE);

			Chunk lblAlamat = new Chunk("Alamat");

			String alamatLengkap = alamatLengkap = rsRecordset.getText(0,
					"alamat_ktp_cust")
					+ " "
					+ rsRecordset.getText(0, "rt_ktp_cust")
					+ "/"
					+ rsRecordset.getText(0, "rw_ktp_cust")
					+ " "
					+ rsRecordset.getText(0, "kel_ktp_cust");
			String alamatsisa = "";
			int totalLines = getNewLines(alamatLengkap);
			// dataDiri.add(lblAlamat);

			if (totalLines > 1) {
				for (int i = 0; i < totalLines; i++) {

					if (i == 0) {
						if (alamatLengkap.length() > 60) {
							alamatsisa = alamatLengkap.substring(60);
							alamatLengkap = alamatLengkap.substring(0, 60);
						}
					} else {
						if (alamatsisa.length() > 60) {
							alamatLengkap = alamatsisa.substring(0, 60);
							alamatsisa = alamatsisa.substring(60);
						} else
							alamatLengkap = alamatsisa;

					}

					Chunk alamat = new Chunk(alamatLengkap);
					dataDiri.add(tabsemicolon);
					if (i == 0) {
						dataDiri.add(semicolon + " ");
						dataDiri.add(alamat);
					} else {
						dataDiri.add("  " + alamat);
					}

					// dataDiri.add(alamat);
					dataDiri.add(Chunk.NEWLINE);
				}
			} else {
				Chunk alamat = new Chunk(rsRecordset.getText(0,
						"alamat_ktp_cust")
						+ " "
						+ rsRecordset.getText(0, "rt_ktp_cust")
						+ "/"
						+ rsRecordset.getText(0, "rw_ktp_cust")
						+ " "
						+ rsRecordset.getText(0, "kel_ktp_cust"));
				dataDiri.add(lblAlamat);
				dataDiri.add(tabsemicolon);
				dataDiri.add(semicolon + " ");
				dataDiri.add(alamat);

				dataDiri.add(Chunk.NEWLINE);
			}

			dataDiri.add(tabsemicolon);
			Chunk kec = new Chunk(rsRecordset.getText(0, "kec_ktp_cust"));
			dataDiri.add("   " + kec);
			dataDiri.add(Chunk.NEWLINE);

			dataDiri.add(tabsemicolon);
			Chunk pos = new Chunk(rsRecordset.getText(0, "kota_ktp_cust") + " "
					+ rsRecordset.getText(0, "kode_pos_ktp_cust"));
			dataDiri.add("   " + pos);
			dataDiri.add(Chunk.NEWLINE);
			dataDiri.add(Chunk.NEWLINE);

			ArrayList<ArrayList<String>> taskObject = rsObj.getAllDataList();
			int jmlItm = 0;
			jmlItm = taskObject.size();
			String strJml = "";

			if (jmlItm == 1) {
				strJml = "1 (satu)";
			} else if (jmlItm == 2) {
				strJml = "2 (dua)";
			} else {
				strJml = "3 (tiga)";
			}

			Paragraph ket = new Paragraph();
			ket.setFont(standartFont);
			Chunk keterangan = new Chunk(
					"Untuk pembelian "
							+ strJml
							+ " unit kendaraan Motor Smh ('kendaraan') dari saudara dengan spesifikasi :");
			ket.add(keterangan);
			ket.add(Chunk.NEWLINE);
			ket.add(Chunk.NEWLINE);

			Paragraph headerTable = new Paragraph();
			headerTable.setFont(standartFont);
			Chunk noHeader = new Chunk("NO");
			headerTable.add(noHeader);
			headerTable.add(tabTblNo);

			Chunk typeHeader = new Chunk("TYPE");
			headerTable.add(typeHeader);
			headerTable.add(tabTblType);

			Chunk merkHeader = new Chunk("MERK");
			headerTable.add(merkHeader);
			headerTable.add(tabTblMerk);

			Chunk merkModel = new Chunk("MODEL/WARNA");
			headerTable.add(merkModel);
			headerTable.add(tabTblModel);

			Chunk qty = new Chunk("QTY");
			headerTable.add(qty);
			headerTable.add(tabTblQty);

			Chunk price = new Chunk("PRICE");
			headerTable.add(price);
			headerTable.add(Chunk.NEWLINE);

			doc.add(header);
			doc.add(header2);
			doc.add(border);
			doc.add(judul);
			doc.add(placeNdate);
			doc.add(pPemberitahuan);
			doc.add(dataDiri);
			doc.add(ket);

			if (taskObject.size() > 1) {
				doc.add(border);
				doc.add(headerTable);
				doc.add(border);

				Paragraph value = null;
				for (int i = 0; i < taskObject.size(); i++) {
					ArrayList<String> obj = taskObject.get(i);

					value = new Paragraph();
					value.setFont(standartFont);

					Chunk no = new Chunk(i + 1 + "");
					value.add(no);
					value.add(tabTblNo);

					Chunk type = new Chunk(obj.get(3));
					value.add(type);
					value.add(tabTblType);

					Chunk merk = new Chunk(obj.get(2));
					value.add(merk);
					value.add(tabTblMerk);

					Chunk modelWarna = new Chunk(obj.get(7));
					value.add(modelWarna);
					value.add(tabTblModel);

					Chunk quantity = new Chunk("1");
					value.add(quantity);
					value.add(tabTblQty);

					if (!obj.get(6).equalsIgnoreCase("")) {
						Chunk harga = new Chunk(Utility.getDecimalCurr(obj
								.get(6)));
						value.add(harga);
					}

					doc.add(value);
					value.add(Chunk.NEWLINE);
				}
				doc.add(border);
			} else {

				Paragraph singleStuff = new Paragraph();
				singleStuff.setFont(standartFont);

				ArrayList<String> obj = taskObject.get(0);

				Chunk lblNamaBpkb = new Chunk("Nama di BPKB");
				Chunk namaBpkb = new Chunk(obj.get(0));
				singleStuff.add(lblNamaBpkb);
				for (int i = 0; i < 4; i++) {
					singleStuff.add(tabsemicolon);
				}
				singleStuff.add(semicolon + " ");
				singleStuff.add(namaBpkb);
				singleStuff.add(Chunk.NEWLINE);

				Chunk lblMerk = new Chunk("Merek/Jenis.Type");
				Chunk merk = new Chunk(obj.get(2) + "/" + obj.get(3));
				singleStuff.add(lblMerk);
				for (int i = 0; i < 5; i++) {
					singleStuff.add(tabsemicolon);
				}
				singleStuff.add(semicolon + " ");
				singleStuff.add(merk);
				singleStuff.add(Chunk.NEWLINE);

				Chunk lblHarga = new Chunk("Harga");
				if (!obj.get(5).equalsIgnoreCase("")) {
					Chunk harga = new Chunk("Rp. "
							+ Utility.getDecimalCurr(obj.get(6)));
					singleStuff.add(lblHarga);
					for (int i = 0; i < 4; i++) {
						singleStuff.add(tabsemicolon);
					}
					singleStuff.add(semicolon + " ");
					singleStuff.add(harga);
					singleStuff.add(Chunk.NEWLINE);
				}

				doc.add(singleStuff);
			}

			// uang muka
			Paragraph calculate = new Paragraph();
			calculate.setFont(standartFont);

			Chunk lblUangMuka = new Chunk("Uang Muka");
			Chunk uangMuka = null;
			if (!rsRecordset.getText(0, "uang_muka_cust").equalsIgnoreCase("")) {
				uangMuka = new Chunk("Rp. "
						+ Utility.getDecimalCurr(rsRecordset.getText(0,
								"uang_muka_cust")));
			} else
				uangMuka = new Chunk("");
			calculate.add(lblUangMuka);
			for (int i = 0; i < 4; i++) {
				calculate.add(tabsemicolon);
			}
			calculate.add(semicolon + " ");
			calculate.add(uangMuka);
			calculate.add(Chunk.NEWLINE);

			Chunk lblAngsuran = new Chunk("Angsuran                  ");
			Chunk angsuran = new Chunk(
					"Rp. "
							+ Utility.getDecimalCurr(rsRecordset.getText(0,
									"angsuran")));
			Chunk per = new Chunk(" /" + rsRecordset.getText(0, "periode")
					+ " " + rsRecordset.getText(0, "periode_type"));
			calculate.add(lblAngsuran);
			for (int i = 0; i < 4; i++) {
				calculate.add(tabsemicolon);
			}
			calculate.add(semicolon + " ");
			calculate.add(angsuran);
			calculate.add(per);
			calculate.add(Chunk.NEWLINE);

			Chunk lblTop = new Chunk("TOP");
			Chunk top = new Chunk(rsRecordset.getText(0, "top"));
			calculate.add(lblTop);
			for (int i = 0; i < 4; i++) {
				calculate.add(tabsemicolon);
			}
			calculate.add(semicolon + " ");
			calculate.add(top);
			calculate.add(Chunk.NEWLINE);
			calculate.add(Chunk.NEWLINE);

			Paragraph ket2 = new Paragraph();
			ket2.setFont(standartFont);
			Chunk keterangan2 = new Chunk(
					"Kami akan mencairkan dana pembelian kendaraan tersebut dan menyerahkan kepada saudara, apabila kendaraan"
							+ " tersebut diatas telah saudara kirimkan dan diterima dengan baik oleh customer kami tersebut. Untuk itu mohon kesediaan saudara untuk menyerakan kepada kami"
							+ " asli kwitansi penagihan, Berita Acara Serah Terima Kendaraan dan  Surat Pernyataan Penyerahan/Pengambilan BPKB.");
			ket2.add(keterangan2);
			ket2.add(Chunk.NEWLINE);

			Paragraph ket3 = new Paragraph();
			ket3.setFont(standartFont);
			Chunk keterangan3 = new Chunk(
					"Persetujuan pembiayaan untuk pembelian barang tersebut diatas berlaku sampai dengan tanggal "
							+ rsRecordset.getText(0, "expired_po_date"));
			ket3.add(keterangan3);
			ket3.add(Chunk.NEWLINE);
			ket3.add(Chunk.NEWLINE);

			Image image = null;

			File first = new File(Utility.getDefaultPath("Sign/"
					+ recTtd.getText(0, "user_id")));
			File two = new File(Utility.getDefaultPath("Sign/"
					+ recTtd.getText(0, "user_id"))
					+ ".png");
			if (first.exists()) {
				first.renameTo(two);
			}

			try {
				image = Image.getInstance(Utility.getDefaultPath("Sign/"
						+ recTtd.getText(0, "user_id"))
						+ ".png");
				image.scaleAbsolute(70, 40);
				image.setAnnotation(new Annotation(0, 0, 0, 0, 3));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			Paragraph ket4 = new Paragraph();
			ket4.setFont(standartFont);
			Chunk hormat2 = new Chunk("Hormat Kami,");
			ket4.add(hormat2);
			ket4.add(Chunk.NEWLINE);
			Chunk pt = new Chunk(rsRecordset.getText(0, "coy_name"));
			ket4.add(pt);
			ket4.add(Chunk.NEWLINE);
			ket4.add(Chunk.NEWLINE);

			if (image != null) {
				ket4.add(image);
			}
			// ket4.add(Chunk.NEWLINE);

			Chunk lblName = new Chunk("Nama ");
			Chunk nama2 = new Chunk(recTtd.getText(0, "user_name"));
			ket4.add(lblName);
			ket4.add(semicolon);
			ket4.add(nama2);
			ket4.add(Chunk.NEWLINE);
			Chunk kurang = new Chunk("Kekurangan dokumen");
			Chunk docKrg = new Chunk(strDocument);
			ket4.add(kurang);
			ket4.add(semicolon);
			ket4.add(docKrg);
			ket4.add(Chunk.NEWLINE);

			doc.add(calculate);
			doc.add(ket2);
			doc.add(ket3);
			doc.add(ket4);
			doc.add(border);

			Paragraph ket5 = new Paragraph();
			ket5.setFont(standartFont);

			Chunk penjuan = new Chunk("Penjual ");
			ket5.add(penjuan);
			ket5.add(Chunk.NEWLINE);
			Chunk menerima = new Chunk(
					"Menerima dengan baik syarat dan ketentuan tersebut diatas :");
			ket5.add(menerima);
			ket5.add(Chunk.NEWLINE);

			Paragraph footer = new Paragraph();
			footer.setFont(standartFont);

			Chunk tagl = new Chunk("Tanggal :");
			footer.add(tagl);
			footer.add(Chunk.NEWLINE);
			footer.add(Chunk.NEWLINE);

			Chunk footerName = new Chunk("Nama     :");
			footer.add(footerName);
			footer.add(tableft);

			Chunk cat = new Chunk("Cetakan ke : ");
			Chunk counter = new Chunk(rsRecordset.getText(0, "print_ctr"));
			footer.add(cat);
			footer.add(counter);
			footer.add(Chunk.NEWLINE);
			footer.add(Chunk.NEWLINE);

			Paragraph footer2 = new Paragraph();
			footer2.setFont(standartFont);

			Chunk jab = new Chunk("Jabatan :");
			footer2.add(jab);
			footer2.add(tableft);

			Chunk cetak = new Chunk("Dicetak oleh : ");
			Chunk user = new Chunk(Utility.getSetting(Utility.getAppContext(),
					"USER_NAME", ""));
			footer2.add(cetak);
			footer2.add(user);

			doc.add(ket5);
			doc.add(footer);
			doc.add(footer2);

		} catch (DocumentException de) {
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (IOException e) {
			Log.e("PDFCreator", "ioException:" + e);
		} finally {
			doc.close();
			try {
				fOut.flush();
				fOut.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}

	// po ufi mamet
	// date: 10-01-2018
	private static void processCreatePOUfi(Nset data) {
		Document doc = new Document(PageSize.A4, 8.0f, 8.0f, 8.0f, 8.0f);
		// String path = "/storage/sdcard0/DAF/PO";
		String path = Utility.getDefaultPath("PO");

		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();

		FileOutputStream fOut = null;
		try {

			String strDocument = data.getData("document").toString();
			String noPO = data.getData("NOPO").toString();

			Recordset rsRecordset = Nikitaset.getRecordset(Nset.newObject()
					.setData("header", data.getData("fieldnametasklist"))
					.setData("data", data.getData("contenttasklist")));
			Recordset rsObj = Nikitaset.getRecordset(Nset.newObject()
					.setData("header", data.getData("fieldnametasklistobject"))
					.setData("data", data.getData("contenttasklistobject")));
			Recordset recTtd = Nikitaset.getRecordset(Nset.newObject()
					.setData("header", data.getData("fieldnametandatangan"))
					.setData("data", data.getData("contenttandatangan")));

			File file = new File(dir, "PO_UFI.pdf");
			// if (!file.exists()) {
			// file.mkdir();
			// }
			fOut = new FileOutputStream(file);
			PdfWriter writer = PdfWriter.getInstance(doc, fOut);
			doc.open();

			// watermark
			File f = new File(dir, "img_ufi.png");
			if (!f.exists())
				try {

					InputStream is = Utility.getAppContext().getAssets()
							.open("img_ufi.png");
					int size = is.available();
					byte[] buffer = new byte[size];
					is.read(buffer);
					is.close();

					FileOutputStream fos = new FileOutputStream(f);
					fos.write(buffer);
					fos.close();

					Utility.compressImage(f.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}

			// PdfContentByte cb = writer.getDirectContentUnder();
			// doc.add(getWatermarkedImage(cb, Image.getInstance(f.getPath()),
			// ""));
			PdfContentByte canvas = writer.getDirectContentUnder();
			Image img = Image.getInstance(f.getPath());
			// image.scaleAbsolute(PageSize.A4.rotate());
			img.scaleAbsolute(500f, 200f);
			// img.setRotation(125f);
			img.setAbsolutePosition(50, 350);

			// opacity watermark
			canvas.saveState();
			PdfGState gs1 = new PdfGState();
			gs1.setFillOpacity(0.2f);
			canvas.setGState(gs1);
			canvas.addImage(img);
			canvas.restoreState();

			// tab
			Chunk tableft = new Chunk(new VerticalPositionMark(), 400f, false);
			Chunk tabsemicolon = new Chunk(new VerticalPositionMark(), 90f,
					false);
			Chunk tabsemicolon2 = new Chunk(new VerticalPositionMark(), 115f,
					false);
			// Chunk afterSemiColom = new Chunk(new VerticalPositionMark(), 50f,
			// false);
			Chunk tabmidle = new Chunk(new VerticalPositionMark(), 200f, false);
			Chunk semicolon = new Chunk(": ");

			Chunk tabTblNo = new Chunk(new VerticalPositionMark(), 30f, false);
			Chunk tabTblType = new Chunk(new VerticalPositionMark(), 140f,
					false);
			Chunk tabTblMerk = new Chunk(new VerticalPositionMark(), 230f,
					false);
			Chunk tabTblModel = new Chunk(new VerticalPositionMark(), 380f,
					false);
			Chunk tabTblQty = new Chunk(new VerticalPositionMark(), 480f, false);

			// Font
			// Small Font 10.0
			Font smallFont = new Font(Font.HELVETICA);
			smallFont.setSize(7.0f);

			// Big Font 20.0
			// Font bigFont = new Font(Font.HELVETICA);
			// bigFont.setSize(15.0f);
			// bigFont.setStyle(Font.BOLD);

			// Standart Font 12.0
			Font standartFont = new Font(Font.HELVETICA);
			standartFont.setSize(9.5f);

			Font font14 = new Font(Font.HELVETICA);
			font14.setSize(13.0f);

			Paragraph header = new Paragraph();
			header.setFont(standartFont);
			Chunk pt_astra = new Chunk(rsRecordset.getText(0, "coy_name"));
			header.add(pt_astra);
			header.add(tableft);

			Chunk date = new Chunk("Date : ");
			Chunk date2 = new Chunk(Utility.getDateStandart());
			header.add(date);
			header.add(date2);

			Paragraph header2 = new Paragraph();
			header2.setFont(standartFont);

			Chunk kota = new Chunk(rsRecordset.getText(0, "office_name"));
			header2.add(kota);
			header2.add(tableft);

			Chunk Halaman = new Chunk("Hal : ");
			Chunk jml = new Chunk("1");
			header2.add(Halaman);
			header2.add(jml);

			Paragraph border = new Paragraph();
			Chunk bord;
			for (int i = 0; i < 143; i++) {
				bord = new Chunk("-");
				border.add(bord);
			}
			border.add(Chunk.NEWLINE);
			Paragraph judul = new Paragraph();
			judul.setFont(font14);
			judul.add(tabmidle);
			Chunk cJudul = new Chunk("PERSETUJUAN PEMBIAYAAN");
			judul.add(cJudul);

			judul.add(Chunk.NEWLINE);
			judul.add(Chunk.NEWLINE);

			Paragraph placeNdate = new Paragraph();
			placeNdate.setFont(standartFont);
			Chunk cPlace = new Chunk(rsRecordset.getText(0, "office_city")
					+ ", ");
			Chunk cDate = new Chunk(rsRecordset.getText(0, "approve_date"));
			placeNdate.add(cPlace);
			placeNdate.add(cDate);
			placeNdate.add(Chunk.NEWLINE);

			Chunk lblNoAppl = new Chunk("Nomor");
			Chunk noAppl = new Chunk(noPO);
			placeNdate.add(lblNoAppl);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add(semicolon + " ");
			placeNdate.add(noAppl);
			placeNdate.add(Chunk.NEWLINE);

			Chunk lblPerihal = new Chunk("Perihal");
			Chunk perihal = new Chunk("Persetujuan Pembiayaan / ");
			Chunk noAppLabel = new Chunk("No. Aplikasi : ");
			placeNdate.add(lblPerihal);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add(semicolon + " ");
			placeNdate.add(perihal);
			placeNdate.add(noAppLabel);
			placeNdate.add(rsRecordset.getText(0, "appl_no"));
			placeNdate.add(Chunk.NEWLINE);

			Chunk lblyth = new Chunk("Kepada Yth");
			Chunk yth = new Chunk(rsRecordset.getText(0, "supl_name"));
			Chunk supl_add = new Chunk(rsRecordset.getText(0, "supl_address1"));

			placeNdate.add(lblyth);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add(semicolon + " ");
			placeNdate.add(yth);
			placeNdate.add(Chunk.NEWLINE);
			// for (int i = 0; i < 4; i++) {
			placeNdate.add(tabsemicolon);
			// }
			placeNdate.add("   " + supl_add);
			placeNdate.add(Chunk.NEWLINE);
			// for (int i = 0; i < 5; i++) {
			placeNdate.add(tabsemicolon);
			// }
			// placeNdate.add("   " + supl_add2);
			// placeNdate.add(Chunk.NEWLINE);
			// for (int i = 0; i < 5; i++) {
			placeNdate.add(tabsemicolon);
			// }
			Chunk supl_city = new Chunk(rsRecordset.getText(0, "supl_city"));
			placeNdate.add("   " + supl_city);

			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(Chunk.NEWLINE);

			Chunk hormat = new Chunk("Dengan Hormat,");
			placeNdate.add(hormat);
			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(Chunk.NEWLINE);

			Paragraph pPemberitahuan = new Paragraph();
			pPemberitahuan.setFont(standartFont);
			Chunk pemberitahuan = new Chunk(
					"Dengan ini kami beritahukan, bahwa kami PT FEDERAL INTERNATIONAL FINANCE telah menyetujui permohonan pembiayaan dari konsumen :");
			pPemberitahuan.add(pemberitahuan);
			pPemberitahuan.add(Chunk.NEWLINE);

			Paragraph dataDiri = new Paragraph();
			dataDiri.setFont(standartFont);

			Chunk lblNama = new Chunk("Nama");
			Chunk nama = new Chunk(rsRecordset.getText(0,
					"nama_lengkap_ktp_cust"));
			dataDiri.add(lblNama);
			// for (int i = 0; i < 4; i++) {
			dataDiri.add(tabsemicolon);
			// }
			dataDiri.add(semicolon + " ");
			dataDiri.add(nama);
			dataDiri.add(Chunk.NEWLINE);

			Chunk lblAlamat = new Chunk("Alamat");

			String alamatLengkap = alamatLengkap = rsRecordset.getText(0,
					"alamat_ktp_cust")
					+ " RT. "
					+ rsRecordset.getText(0, "rt_ktp_cust")
					+ "/RW. "
					+ rsRecordset.getText(0, "rw_ktp_cust")
					+ " Kel. "
					+ rsRecordset.getText(0, "kel_ktp_cust");

			String alamatsisa = "";
			int totalLines = getNewLines(alamatLengkap);
			// dataDiri.add(lblAlamat);

			if (totalLines > 1) {
				for (int i = 0; i < totalLines; i++) {

					if (i == 0) {
						if (alamatLengkap.length() > 60) {
							alamatsisa = alamatLengkap.substring(60);
							alamatLengkap = alamatLengkap.substring(0, 60);
						}
					} else {
						if (alamatsisa.length() > 60) {
							alamatLengkap = alamatsisa.substring(0, 60);
							alamatsisa = alamatsisa.substring(60);
						} else
							alamatLengkap = alamatsisa;

					}

					Chunk alamat = new Chunk(alamatLengkap);
					dataDiri.add(tabsemicolon);
					if (i == 0) {
						dataDiri.add(semicolon + " ");
						dataDiri.add(alamat);
					} else {
						dataDiri.add("   " + alamat);
					}

					// dataDiri.add(alamat);
					if(i!=totalLines-1) {
						dataDiri.add(Chunk.NEWLINE);						
					}
				}
			} else {
				Chunk alamat = new Chunk(rsRecordset.getText(0,
						"alamat_ktp_cust")
						+ " RT. "
						+ rsRecordset.getText(0, "rt_ktp_cust")
						+ "/RW. "
						+ rsRecordset.getText(0, "rw_ktp_cust")
						+ " Kel. "
						+ rsRecordset.getText(0, "kel_ktp_cust"));
				dataDiri.add(lblAlamat);
				dataDiri.add(tabsemicolon);
				dataDiri.add(semicolon + " ");
				dataDiri.add(alamat);

//				dataDiri.add(Chunk.NEWLINE);
			}

//			dataDiri.add(tabsemicolon);
			Chunk kec = new Chunk(" Kec. "+rsRecordset.getText(0, "kec_ktp_cust"));
			dataDiri.add(kec);
			dataDiri.add(Chunk.NEWLINE);

			dataDiri.add(tabsemicolon);
			Chunk pos = new Chunk("  "+rsRecordset.getText(0, "kota_ktp_cust") + " "
					+ rsRecordset.getText(0, "kode_pos_ktp_cust"));
			dataDiri.add(" " + pos);
			dataDiri.add(Chunk.NEWLINE);

			String telp_rumah = "-";
			if (!rsRecordset.getText(0, "telp_rumah_cust").equalsIgnoreCase(""))
				telp_rumah = rsRecordset.getText(0, "telp_rumah_cust");

			Chunk lblPhone = new Chunk("Phone");
			Chunk phone = new Chunk(telp_rumah + " / "
					+ rsRecordset.getText(0, "hp1_cust"));
			dataDiri.add(lblPhone);
			// for (int i = 0; i < 4; i++) {
			dataDiri.add(tabsemicolon);
			// }
			dataDiri.add(semicolon + " ");
			dataDiri.add(phone);
			dataDiri.add(Chunk.NEWLINE);
			dataDiri.add(Chunk.NEWLINE);

			doc.add(header);
			doc.add(header2);
			doc.add(border);
			doc.add(judul);
			doc.add(placeNdate);
			doc.add(pPemberitahuan);
			doc.add(dataDiri);

			Paragraph ket = new Paragraph();
			ket.setFont(standartFont);
			Chunk keterangan = new Chunk(
					"Untuk pembiayaan berdasarkan Perjanjian Pembiayaan antara FIF dengan konsumen dengan spesifikasi pembiayaan sebagai berikut :");
			ket.add(keterangan);
			ket.add(Chunk.NEWLINE);

			Paragraph spesifikasi = new Paragraph();
			ket.setFont(standartFont);

			Chunk lblProduk = new Chunk("Produk Pembiayaan");
			Chunk produk = new Chunk(rsRecordset.getText(0, "purpose"));
			ket.add(lblProduk);
			// for (int i = 0; i < 4; i++) {
			ket.add(tabsemicolon2);
			// }
			ket.add(semicolon + " ");
			ket.add(produk);
			ket.add(Chunk.NEWLINE);

			Chunk lblutkPembiayaan = new Chunk("Peruntukan Pembiayaan");
			Chunk utkPembiayaan = new Chunk("-");
			ket.add(lblutkPembiayaan);
			// for (int i = 0; i < 4; i++) {
			ket.add(tabsemicolon2);
			// }
			ket.add(semicolon + " ");
			ket.add(utkPembiayaan);
			ket.add(Chunk.NEWLINE);

			Chunk lblBsrPembiayaan = new Chunk("Besar Pembiayaan");
			Chunk besarPembiayaan = new Chunk("Rp. "
					+ Utility.getDecimalCurr(rsRecordset.getText(0,
							"pengeluaran")));
			ket.add(lblBsrPembiayaan);
			// for (int i = 0; i < 4; i++) {
			ket.add(tabsemicolon2);
			// }
			ket.add(semicolon + " ");
			ket.add(besarPembiayaan);
			ket.add(Chunk.NEWLINE);

			Chunk Angsuran = new Chunk("Angsuran");
			Chunk angs = new Chunk(
					"Rp. "
							+ Utility.getDecimalCurr(rsRecordset.getText(0,
									"angsuran")));
			ket.add(Angsuran);
			// for (int i = 0; i < 4; i++) {
			ket.add(tabsemicolon2);
			// }
			ket.add(semicolon + " ");
			ket.add(angs);
			ket.add(Chunk.NEWLINE);

			Chunk TOP = new Chunk("TOP");
			Chunk top = new Chunk(rsRecordset.getText(0, "top"));
			ket.add(TOP);
			// for (int i = 0; i < 4; i++) {
			ket.add(tabsemicolon2);
			// }
			ket.add(semicolon + " ");
			ket.add(top);
			ket.add(Chunk.NEWLINE);

			Paragraph ket2 = new Paragraph();
			ket2.setFont(standartFont);
			Chunk keterangan2 = new Chunk(
					"Berdasarkan data tersebut diatas,"
							+ "kami meminta agar saudara sebagai pihak agen pembayaran meneruskan pembiayaan tersebut kepada penyedia barang dan/ atau jasa sesuai dengan kebutuhan Konsumen untuk kemudian menyerahkan dokumen pengadaan barang dan/ atau jasa tersebut kepada kami.");
			ket2.add(keterangan2);
			ket2.add(Chunk.NEWLINE);
			ket2.add(Chunk.NEWLINE);

			Paragraph ket3 = new Paragraph();
			ket3.setFont(standartFont);
			Chunk keterangan3 = new Chunk(
					"Kami akan melunasi pembiayaan dan secara langsung menyerahkan kepada saudara,"
							+ " apabila barang dan/ atau jasa tersebut diatas telah diterima dengan baik oleh konsumen kami tersebut."
							+ " Untuk itu mohon kesediaan saudara menyerahkan kepada kami asli dokumen pengadaan barang dan/ atau jasa tersebut.");
			ket3.add(keterangan3);
			ket3.add(Chunk.NEWLINE);
			ket3.add(Chunk.NEWLINE);

//			Paragraph ket4 = new Paragraph();
//			ket4.setFont(standartFont);
//			Chunk keterangan4 = new Chunk(
//					"Untuk itu mohon kesediaan saudara menyerahkan kepada kami asli dokumen pengadaan barang dan/ atau jasa tersebut.");
//			ket4.add(keterangan4);
//			ket4.add(Chunk.NEWLINE);
//			ket4.add(Chunk.NEWLINE);
			Paragraph ket5 = new Paragraph();
			ket5.setFont(standartFont);
			Chunk keterangan5 = new Chunk(
					"Persetujuan pembiayaan tersebut diatas berlaku sampai dengan tanggal "
							+ rsRecordset.getText(0, "expired_po_date"));
			ket5.add(keterangan5);
			ket5.add(Chunk.NEWLINE);
			ket5.add(Chunk.NEWLINE);

			Image image = null;

			File first = new File(Utility.getDefaultPath("Sign/"
					+ recTtd.getText(0, "user_id")));
			File two = new File(Utility.getDefaultPath("Sign/"
					+ recTtd.getText(0, "user_id"))
					+ ".png");

			if (first.exists()) {
				first.renameTo(two);
			}

			try {
				image = Image.getInstance(Utility.getDefaultPath("Sign/"
						+ recTtd.getText(0, "user_id"))
						+ ".png");
				image.scaleAbsolute(70, 40);
				image.setAnnotation(new Annotation(0, 0, 0, 0, 3));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			Paragraph ket6 = new Paragraph();
			ket6.setFont(standartFont);
			Chunk hormat2 = new Chunk("Hormat Kami,");
			ket6.add(hormat2);
			ket6.add(Chunk.NEWLINE);
			Chunk pt = new Chunk(rsRecordset.getText(0, "coy_name"));
			ket6.add(pt);
			ket6.add(Chunk.NEWLINE);
			ket6.add(Chunk.NEWLINE);
//			ket6.add(Chunk.NEWLINE);

			if (image != null) {
				ket6.add(image);
			}
			// ket6.add(Chunk.NEWLINE);

			Chunk lblName = new Chunk("Nama ");
			Chunk nama2 = new Chunk(recTtd.getText(0, "user_name"));
			ket6.add(lblName);
			ket6.add(semicolon);
			ket6.add(nama2);
			ket6.add(Chunk.NEWLINE);
			Chunk kurang = new Chunk("Kekurangan dokumen");
			Chunk docKrg = new Chunk(strDocument);
			ket6.add(kurang);
			ket6.add(semicolon);
			ket6.add(docKrg);
			ket6.add(Chunk.NEWLINE);

			// doc.add(calculate);
			doc.add(ket);
			doc.add(ket2);
			doc.add(ket3);
//			doc.add(ket4);
			doc.add(ket5);
			doc.add(ket6);
			doc.add(border);

			Paragraph ket7 = new Paragraph();
			ket7.setFont(standartFont);

			Chunk penjuan = new Chunk("Pemasok ");
			ket7.add(penjuan);
			ket7.add(Chunk.NEWLINE);
			Chunk menerima = new Chunk(
					"Menerima dengan baik syarat dan ketentuan tersebut diatas ");
			ket7.add(menerima);
			ket7.add(Chunk.NEWLINE);

			Paragraph footer = new Paragraph();
			footer.setFont(standartFont);

			Chunk tagl = new Chunk("Tanggal :");
			footer.add(tagl);
			footer.add(Chunk.NEWLINE);
			footer.add(Chunk.NEWLINE);

			Chunk footerName = new Chunk("Nama     :");
			footer.add(footerName);
			footer.add(tableft);

			Chunk cat = new Chunk("Cetakan ke : ");
			int num = Integer.parseInt(rsRecordset.getText(0, "print_ctr")); 
			Chunk counter = new Chunk(Utility.getPrintCounter(num));
			footer.add(cat);
			footer.add(counter);
			footer.add(Chunk.NEWLINE);
			footer.add(Chunk.NEWLINE);

			Paragraph footer2 = new Paragraph();
			footer2.setFont(standartFont);

			doc.add(ket7);
			doc.add(footer);
			doc.add(footer2);

		} catch (DocumentException de) {
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (IOException e) {
			Log.e("PDFCreator", "ioException:" + e);
		} finally {
			doc.close();
			try {
				fOut.flush();
				fOut.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

	}

	// created by jadmiko
	// date: 10-01-2018
	public static void processCreatePOSpektra(Nset data) {
		Document doc = new Document(PageSize.LEGAL, 8.0f, 8.0f, 8.0f, 8.0f);
//		String path = "/storage/sdcard0/DAF/PO";
		String path = Utility.getDefaultPath("PO");

		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();

		String strDocument = data.getData("document").toString();
		String noPO = data.getData("NOPO").toString();
		Recordset rsRecordset = Nikitaset.getRecordset(Nset.newObject()
				.setData("header", data.getData("fieldnametasklist"))
				.setData("data", data.getData("contenttasklist")));
		Recordset rsObj = Nikitaset.getRecordset(Nset.newObject()
				.setData("header", data.getData("fieldnametasklistobject"))
				.setData("data", data.getData("contenttasklistobject")));
		Recordset recTtd = Nikitaset.getRecordset(Nset.newObject()
				.setData("header", data.getData("fieldnametandatangan"))
				.setData("data", data.getData("contenttandatangan")));
		FileOutputStream fOut = null;

		try {
			File file = new File(dir, "PO_SPEKTRA.PDF");
//			if (!file.exists()) {
////				file.mkdir();
//			}		
			fOut = new FileOutputStream(file);
			PdfWriter writer = PdfWriter.getInstance(doc, fOut);
			doc.open();

			// watermark
			File f = new File(dir, "img_spektra.png");
			if (!f.exists())
				try {
					InputStream is = Utility.getAppContext().getAssets()
							.open("img_spektra.png");
					int size = is.available();
					byte[] buffer = new byte[size];
					is.read(buffer);
					is.close();

					FileOutputStream fos = new FileOutputStream(f);
					fos.write(buffer);
					fos.close();

					Utility.compressImage(f.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}

			PdfContentByte canvas = writer.getDirectContentUnder();
			Image img = Image.getInstance(f.getPath());
			img.scaleAbsolute(500f, 200f);
			img.setAbsolutePosition(50, 350);
			canvas.saveState();
			PdfGState gs1 = new PdfGState();
			gs1.setFillOpacity(0.2f);
			canvas.setGState(gs1);
			canvas.addImage(img);
			canvas.restoreState();

			// tab
			Chunk tableft = new Chunk(new VerticalPositionMark(), 400f, false);
			Chunk tabsemicolon = new Chunk(new VerticalPositionMark(), 90f,
					false);
			Chunk tabmidle = new Chunk(new VerticalPositionMark(), 200f, false);
			Chunk semicolon = new Chunk(": ");
			Chunk tabTblNo = new Chunk(new VerticalPositionMark(), 30f, false);
			Chunk tabTblType = new Chunk(new VerticalPositionMark(), 140f,
					false);
			Chunk tabTblMerk = new Chunk(new VerticalPositionMark(), 230f,
					false);
			Chunk tabTblModel = new Chunk(new VerticalPositionMark(), 360f,
					false);
			Chunk tabTblQty = new Chunk(new VerticalPositionMark(), 480f, false);
			Chunk tabTblNo2 = new Chunk(new VerticalPositionMark(), 30f, false);
			Chunk tabTblType2 = new Chunk(new VerticalPositionMark(), 130f,
					false);
			Chunk tabTblMerk2 = new Chunk(new VerticalPositionMark(), 260f,
					false);
			Chunk tabTblModel2 = new Chunk(new VerticalPositionMark(), 390f,
					false);
			Chunk tabTblQty2 = new Chunk(new VerticalPositionMark(), 490f,
					false);
			Font smallFont = new Font(Font.HELVETICA);
			smallFont.setSize(7.0f);
			Font standartFont = new Font(Font.HELVETICA);
			standartFont.setSize(9.5f);
			Font font14 = new Font(Font.HELVETICA);
			font14.setSize(13.0f);

			Paragraph header = new Paragraph();
			header.setFont(standartFont);
			Chunk ch_jabatan = new Chunk("Jabatan : ");
			header.add(ch_jabatan);
			header.add(tableft);
			String pic = Utility.getSetting(Utility.getAppContext(),
					"USER_NAME", "");
			Chunk ch_cetak_oleh = new Chunk("Dicetak Oleh : ");
			Chunk ch_pic = new Chunk(pic);
			header.add(ch_cetak_oleh);
			header.add(ch_pic);

			Paragraph header2 = new Paragraph();
			header2.setFont(standartFont);
			Chunk ch_pt_fif = new Chunk(rsRecordset.getText(0, "coy_name"));
			header2.add(ch_pt_fif);
			header2.add(tableft);
			String date = Utility.getDateStandart();
			Chunk ch_date = new Chunk("Date : ");
			Chunk ch_date_val = new Chunk(date);
			header2.add(ch_date);
			header2.add(ch_date_val);

			Paragraph header3 = new Paragraph();
			header3.setFont(standartFont);
			Chunk ch_branch = new Chunk(rsRecordset.getText(0, "office_name"));
			header3.add(ch_branch);
			header3.add(tableft);
			Chunk ch_hal = new Chunk("Hal : ");
			Chunk ch_jml = new Chunk("1");
			header3.add(ch_hal);
			header3.add(ch_jml);

			Paragraph border = new Paragraph();
			Chunk bord;
			for (int i = 0; i < 143; i++) {
				bord = new Chunk("-");
				border.add(bord);
			}
			border.add(Chunk.NEWLINE);
			Paragraph judul = new Paragraph();
			judul.setFont(font14);
			judul.add(tabmidle);
			Chunk cJudul = new Chunk("PERSETUJUAN PEMBIAYAAN");
			judul.add(cJudul);
			judul.add(Chunk.NEWLINE);
			judul.add(Chunk.NEWLINE);

			Paragraph placeNdate = new Paragraph();
			placeNdate.setFont(standartFont);
			String city_office = rsRecordset.getText(0, "office_city");
			String date2 = rsRecordset.getText(0, "approve_date");
			Chunk ch_place = new Chunk(city_office + ", ");
			Chunk ch_date2 = new Chunk(date2);
			placeNdate.add(ch_place);
			placeNdate.add(ch_date2);
			placeNdate.add(Chunk.NEWLINE);

			String appl_no = noPO + "/" + rsRecordset.getText(0, "appl_no");
			Chunk ch_nomor = new Chunk("No.PP /Appl");
			Chunk ch_appl_no = new Chunk(appl_no);
			placeNdate.add(ch_nomor);
			placeNdate.add(tabsemicolon);
			placeNdate.add(semicolon + " ");
			placeNdate.add(ch_appl_no);
			placeNdate.add(Chunk.NEWLINE);

			Chunk ch_perihal = new Chunk("Perihal");
			Chunk ch_pp = new Chunk("Persetujuan Pembiayaan");
			placeNdate.add(ch_perihal);
			placeNdate.add(tabsemicolon);
			placeNdate.add(semicolon + " ");
			placeNdate.add(ch_pp);
			placeNdate.add(Chunk.NEWLINE);

			Chunk ch_kpd = new Chunk("Kepada Yth");
			Chunk ch_suppl_nama = new Chunk(rsRecordset.getText(0, "supl_name"));
			Chunk ch_suppl_add = new Chunk(rsRecordset.getText(0,
					"supl_address1"));
			Chunk ch_suppl_city = new Chunk(rsRecordset.getText(0, "supl_city"));

			placeNdate.add(ch_kpd);
			placeNdate.add(tabsemicolon);
			placeNdate.add(semicolon + " ");
			placeNdate.add(ch_suppl_nama);
			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(tabsemicolon);
			placeNdate.add("   " + ch_suppl_add);
			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(tabsemicolon);
			placeNdate.add("   " + ch_suppl_city);

			placeNdate.add(Chunk.NEWLINE);

			Chunk ch_dh = new Chunk("Dengan Hormat,");
			placeNdate.add(ch_dh);
			placeNdate.add(Chunk.NEWLINE);
			placeNdate.add(Chunk.NEWLINE);

			Paragraph pPemberitahuan = new Paragraph();
			pPemberitahuan.setFont(standartFont);
			Chunk ch_info = new Chunk(
					"Dengan ini kami beritahukan, bahwa kami telah menyetujui permohonan pembiayaan dari customer :");
			pPemberitahuan.add(ch_info);
			pPemberitahuan.add(Chunk.NEWLINE);

			Paragraph dataDiri = new Paragraph();
			dataDiri.setFont(standartFont);

			Chunk ch_nama = new Chunk("Nama");
			Chunk ch_nama_val = new Chunk(rsRecordset.getText(0,
					"nama_lengkap_ktp_cust"));
			dataDiri.add(ch_nama);
			dataDiri.add(tabsemicolon);
			dataDiri.add(semicolon + " ");
			dataDiri.add(ch_nama_val);
			dataDiri.add(Chunk.NEWLINE);

			Chunk ch_alamat = new Chunk("Alamat");

			String alamatLengkap = alamatLengkap = rsRecordset.getText(0,
					"alamat_ktp_cust")
					+ " RT. "
					+ rsRecordset.getText(0, "rt_ktp_cust")
					+ "/RW. "
					+ rsRecordset.getText(0, "rw_ktp_cust")
					+ " Kel. "
					+ rsRecordset.getText(0, "kel_ktp_cust");
			String alamatsisa = "";
			int totalLines = getNewLines(alamatLengkap);

			if (totalLines > 1) {
				for (int i = 0; i < totalLines; i++) {
					if (i == 0) {
						if (alamatLengkap.length() > 60) {
							alamatsisa = alamatLengkap.substring(60);
							alamatLengkap = alamatLengkap.substring(0, 60);
						}
					} else {
						if (alamatsisa.length() > 60) {
							alamatLengkap = alamatsisa.substring(0, 60);
							alamatsisa = alamatsisa.substring(60);
						} else
							alamatLengkap = alamatsisa;
					}

					Chunk alamat = new Chunk(alamatLengkap);
					dataDiri.add(tabsemicolon);
					if (i == 0) {
						dataDiri.add(semicolon + " ");
						dataDiri.add(alamat);
					} else {
						dataDiri.add("   " + alamat);
					}
					if(i!=totalLines-1) {
						dataDiri.add(Chunk.NEWLINE);						
					}
				}
			} else {
				Chunk alamat = new Chunk(rsRecordset.getText(0,
						"alamat_ktp_cust")
						+ " RT. "
						+ rsRecordset.getText(0, "rt_ktp_cust")
						+ "/RW. "
						+ rsRecordset.getText(0, "rw_ktp_cust")
						+ " Kel. "
						+ rsRecordset.getText(0, "kel_ktp_cust"));
				dataDiri.add(ch_alamat);
				dataDiri.add(tabsemicolon);
				dataDiri.add(semicolon + " ");
				dataDiri.add(alamat);

//				dataDiri.add(Chunk.NEWLINE);
			}
			
//			dataDiri.add(tabsemicolon);
			Chunk kec = new Chunk(" Kec. "+rsRecordset.getText(0, "kec_ktp_cust"));
			dataDiri.add(kec);
			dataDiri.add(Chunk.NEWLINE);

			dataDiri.add(tabsemicolon);
			Chunk pos = new Chunk(rsRecordset.getText(0, "kota_ktp_cust") + " "
					+ rsRecordset.getText(0, "kode_pos_ktp_cust"));
			dataDiri.add("   " + pos);
			dataDiri.add(Chunk.NEWLINE);
//			dataDiri.add(Chunk.NEWLINE);

			Chunk ch_phone = new Chunk("Phone");
			String telp_rumah = "-";
			String hp1 = "-";
			if (!rsRecordset.getText(0, "telp_rumah_cust").equalsIgnoreCase(""))
				telp_rumah = rsRecordset.getText(0, "telp_rumah_cust");
			if (!rsRecordset.getText(0, "hp1_cust").equalsIgnoreCase(""))
				hp1 = rsRecordset.getText(0, "hp1_cust");

			Chunk ch_phone_val = new Chunk(telp_rumah + " / " + hp1);
			dataDiri.add(ch_phone);
			dataDiri.add(tabsemicolon);
			dataDiri.add(semicolon + " ");
			dataDiri.add(ch_phone_val);
			dataDiri.add(Chunk.NEWLINE);
			dataDiri.add(Chunk.NEWLINE);

			ArrayList<ArrayList<String>> taskObject = rsObj.getAllDataList();
			int jmlItm = 0;
			jmlItm = taskObject.size();
			String strJml = "";

			if (jmlItm == 1) {
				strJml = "1 (satu)";
			} else if (jmlItm == 2) {
				strJml = "2 (dua)";
			} else {
				strJml = "3 (tiga)";
			}

			Paragraph ket = new Paragraph();
			ket.setFont(standartFont);
			Chunk keterangan = new Chunk(
					"Untuk pembelian "
							+ strJml
							+ " unit Barang dari Saudara dengan spesifikasi sebagai berikut :");
			ket.add(keterangan);
//			ket.add(Chunk.NEWLINE);

			doc.add(header);
			doc.add(header2);
			doc.add(header3);
			doc.add(border);
			doc.add(judul);
			doc.add(placeNdate);
			doc.add(pPemberitahuan);
			doc.add(dataDiri);
			doc.add(ket);

			Paragraph headerTable2 = new Paragraph();
			headerTable2.setFont(standartFont);
			Chunk noHeader2 = new Chunk("NO");
			headerTable2.add(noHeader2);
			headerTable2.add(tabTblNo2);

			Chunk typeHeader2 = new Chunk("TYPE");
			headerTable2.add(typeHeader2);
			headerTable2.add(tabTblType2);

			Chunk merkHeader2 = new Chunk("MERK");
			headerTable2.add(merkHeader2);
			headerTable2.add(tabTblMerk2);

			Chunk modelHeader2 = new Chunk("MODEL/WARNA");
			headerTable2.add(modelHeader2);
			headerTable2.add(tabTblModel2);

			Chunk qtyModel2 = new Chunk("QTY");
			headerTable2.add(qtyModel2);
			headerTable2.add(tabTblQty2);

			Chunk price2 = new Chunk("PRICE");
			headerTable2.add(price2);

			if (taskObject.size() >= 1) {
				doc.add(border);
				doc.add(headerTable2);
				doc.add(border);

				int nomor = 0;
				for (int i = 0; i < taskObject.size(); i++) {
					nomor += 1;
					String nomor_str = Integer.toString(nomor);

					Paragraph header4 = new Paragraph();
					header4.setFont(standartFont);

					ArrayList<String> obj = taskObject.get(i);
					// define list object
					String[] type_obj = { obj.get(3) };
					String[] merk_obj = { obj.get(2) };
					String[] model_obj = { obj.get(5) };
					String[] qty_obj = { obj.get(9) }; // array ke=>9
					String[] price_obj = { obj.get(6) };

					Chunk ch_no_obj = new Chunk(nomor_str);
					Chunk ch_type_obj = new Chunk(type_obj[0]);
					Chunk ch_merk_obj = new Chunk(merk_obj[0]);
					Chunk ch_model_obj = new Chunk(model_obj[0]);
					Chunk ch_qty_obj = new Chunk(
							Utility.getDecimalCurr(qty_obj[0]));
					Chunk ch_price_obj = new Chunk(
							Utility.getDecimalCurr(price_obj[0]));

					header4.add(ch_no_obj);
					header4.add(tabTblNo2);
					header4.add(ch_type_obj);
					header4.add(tabTblType2);
					header4.add(ch_merk_obj);
					header4.add(tabTblMerk2);
					header4.add(ch_model_obj);
					header4.add(tabTblModel2);
					header4.add(ch_qty_obj);
					header4.add(tabTblQty2);
					header4.add(ch_price_obj);

					doc.add(header4);
				}
				doc.add(border);
			} else {
			}

			Paragraph calculate = new Paragraph();
			calculate.setFont(standartFont);

			Chunk lblUangMuka = new Chunk("Uang Muka");
			Chunk uangMuka = null;
			if (!"uang muka cust".equalsIgnoreCase("")) {
				uangMuka = new Chunk("Rp. "
						+ Utility.getDecimalCurr(rsRecordset.getText(0,
								"uang_muka_cust")));
			} else
				uangMuka = new Chunk("");
			calculate.add(lblUangMuka);
			for (int i = 0; i < 4; i++) {
				calculate.add(tabsemicolon);
			}
			calculate.add(semicolon + " ");
			calculate.add(uangMuka);
			calculate.add(Chunk.NEWLINE);

			Chunk lblAngsuran = new Chunk("Angsuran                  ");
			Chunk angsuran = new Chunk(
					"Rp. "
							+ Utility.getDecimalCurr(rsRecordset.getText(0,
									"angsuran")));
			Chunk per = new Chunk(" /" + rsRecordset.getText(0, "periode")
					+ " " + rsRecordset.getText(0, "periode_type"));
			calculate.add(lblAngsuran);
			for (int i = 0; i < 4; i++) {
				calculate.add(tabsemicolon);
			}
			calculate.add(semicolon + " ");
			calculate.add(angsuran);
			calculate.add(per);
			calculate.add(Chunk.NEWLINE);

			Chunk lblTop = new Chunk("TOP");
			Chunk top = new Chunk(rsRecordset.getText(0, "top"));
			calculate.add(lblTop);
			for (int i = 0; i < 4; i++) {
				calculate.add(tabsemicolon);
			}
			calculate.add(semicolon + " ");
			calculate.add(top);
			calculate.add(Chunk.NEWLINE);
			calculate.add(Chunk.NEWLINE);

			Paragraph ket2 = new Paragraph();
			ket2.setFont(standartFont);
			Chunk keterangan2 = new Chunk(
					"Kami akan mencairkan dana pembiayaan atas pembelian barang oleh customer kami tersebut dan menyerahkan kepada Saudara, "
							+ "apabila barang tersebut diatas telah Saudara serahkan dan diterima dengan baik oleh Customer kami tersebut. "
							+ "Untuk itu mohon kesediaan Saudara menyerahkan kepada kami asli kwitansi penagihan, Berita Acara Serah Terima.");
			ket2.add(keterangan2);
			ket2.add(Chunk.NEWLINE);
			ket2.add(Chunk.NEWLINE);

			Paragraph ket3 = new Paragraph();
			ket3.setFont(standartFont);
			String date3 = rsRecordset.getText(0, "expired_po_date");
			Chunk keterangan3 = new Chunk(
					"Persetujuan pembiayaan untuk pembelian barang tersebut diatas berlaku sampai dengan tanggal "
							+ date3 + ".");
			ket3.add(keterangan3);
			ket3.add(Chunk.NEWLINE);
			ket3.add(Chunk.NEWLINE);

			Image image = null;

			File first = new File(Utility.getDefaultPath("Sign/"
					+ recTtd.getText(0, "user_id")));
			File two = new File(Utility.getDefaultPath("Sign/"
					+ recTtd.getText(0, "user_id"))
					+ ".png");
			if (first.exists()) {
				first.renameTo(two);
			}

			try {
				image = Image.getInstance(Utility.getDefaultPath("Sign/"
						+ recTtd.getText(0, "user_id"))
						+ ".png");
				image.scaleAbsolute(70, 40);
				image.setAnnotation(new Annotation(0, 0, 0, 0, 3));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			Paragraph ket4 = new Paragraph();
			ket4.setFont(standartFont);
			Chunk hormat2 = new Chunk("Hormat Kami,");
			ket4.add(hormat2);
			ket4.add(Chunk.NEWLINE);
			Chunk pt = new Chunk(rsRecordset.getText(0, "coy_name"));
			ket4.add(pt);
			ket4.add(Chunk.NEWLINE);
			ket4.add(Chunk.NEWLINE);

			if (image != null) {
				ket4.add(image);
			}

			Chunk lblName = new Chunk("Nama ");
			Chunk nama2 = new Chunk(recTtd.getText(0, "user_name"));
			ket4.add(lblName);
			ket4.add(semicolon);
			ket4.add(nama2);
			ket4.add(Chunk.NEWLINE);
			Chunk kurang = new Chunk("Kekurangan dokumen ");
			Chunk docKrg = new Chunk(strDocument);
			ket4.add(kurang);
			ket4.add(semicolon);
			ket4.add(docKrg);
			ket4.add(Chunk.NEWLINE);

			doc.add(calculate);
			doc.add(ket2);
			doc.add(ket3);
			doc.add(ket4);
			doc.add(border);

			Paragraph ket5 = new Paragraph();
			ket5.setFont(standartFont);

			Chunk penjuan = new Chunk("Penjual ");
			ket5.add(penjuan);
			ket5.add(Chunk.NEWLINE);
			Chunk menerima = new Chunk(
					"Menerima dengan baik syarat dan ketentuan tersebut diatas :");
			ket5.add(menerima);
			ket5.add(Chunk.NEWLINE);

			Paragraph footer = new Paragraph();
			footer.setFont(standartFont);

			Chunk tagl = new Chunk("Tanggal :");
			footer.add(tagl);
			footer.add(Chunk.NEWLINE);
			footer.add(Chunk.NEWLINE);

			Chunk footerName = new Chunk("Nama     :");
			footer.add(footerName);
			footer.add(tableft);

			Chunk cat = new Chunk("Cetakan ke : ");
			int num = Integer.parseInt(rsRecordset.getText(0, "print_ctr")); 
			Chunk counter = new Chunk(Utility.getPrintCounter(num));
			footer.add(cat);
			footer.add(counter);
			footer.add(Chunk.NEWLINE);
			footer.add(Chunk.NEWLINE);

			Paragraph footer2 = new Paragraph();
			footer2.setFont(standartFont);

			Chunk jab = new Chunk("Jabatan :");
			footer2.add(jab);
			footer2.add(tableft);

			Chunk cetak = new Chunk("Dicetak oleh : ");
			Chunk user = new Chunk(Utility.getSetting(Utility.getAppContext(),
					"USER_NAME", ""));
			footer2.add(cetak);
			footer2.add(user);

			doc.add(ket5);
			doc.add(footer);
			doc.add(footer2);

		} catch (DocumentException de) {
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (IOException e) {
			Log.e("PDFCreator", "ioException:" + e);
		} finally {
			doc.close();
			try {
				fOut.flush();
				fOut.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}

	private static int getNewLines(String santance) {
		int totalLines = 1;
		if (santance.length() > 60) {
			totalLines = santance.length() % 60;

			if (totalLines == 0) {
				totalLines = santance.length() / 60;
			} else if (totalLines > 0) {
				totalLines = santance.length() / 60 + 1;
			}
		} else
			totalLines = 1;

		return totalLines;
	}

	@SuppressLint("SimpleDateFormat")
	private static String getExpiredPODate(String date, int validDay) {
		Calendar cal = Utility.convertDate(date);
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) + validDay);
		return new SimpleDateFormat("dd/MM/yyy").format(cal.getTime());
	}
}
