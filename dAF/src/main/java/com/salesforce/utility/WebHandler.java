package com.salesforce.utility;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.daf.activity.SettingActivity;
import com.salesforce.adapter.AdapterDialog;
import com.salesforce.database.Nikitaset;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;


public class WebHandler{

	private Activity activity;
//	private String TAG = "JsHandler";
	private WebView webView;
	private String result;
	private Nset data;
	
	
	public WebHandler(Activity _contxt,WebView _webView) {
		activity = _contxt;
		webView = _webView;
	}

	/**
	 * This function handles call from JS
	 */
	@JavascriptInterface
	public void jsFnCall(String jsString) {
		data = Nset.readJSON(jsString);
		String URL = Utility.getSetting(activity, "URL_TRACK", "")+"ServiceMobile/trackingservlet?applno=";
		URL = Utility.getURLenc(URL, data.getData("APPL_NO").toString(), "&v=100");
//		URL = Utility.getURLenc(URL, "10116010252", "&v=100");
//		URL = Utility.getURLenc(URL, "10116000732", "&v=100");
		hitServerPO(URL, true);
	}
	
	/*public void jsFnCall(String jsString) {
		data = Nset.readJSON(jsString);
		String URL = Utility.getSetting(activity, "URL_TRACK", "")+"ServiceMobile/trackingservlet?applno=";
		URL = Utility.getURLenc(URL, data.getData("APPL_NO").toString(), "&v=100");
		hitServerPO(URL, true);
	}*/

	/**
	 * This function handles call from Android-Java
	 */
	public void javaFnCall(String jsString) {
		
		final String webUrl = "javascript:diplayJavaMsg('"+jsString+"')";
		// Add this to avoid android.view.windowmanager$badtokenexception unable to add window
		if(!activity.isFinishing()) 
			// loadurl on UI main thread
			activity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				webView.loadUrl(webUrl); 
			}
		});
	}
	
	private void hitServerPO(final String url, final boolean hitStatus){
		result = "";
		Messagebox.showProsesBar(activity, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				result = Utility.getHttpConnection(url);
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (hitStatus) {
					showStatus(result);
				}else
					print(result);
			}
		});
	}
	
	public void print(File file) {		
		Intent i = new Intent("org.androidprinting.intent.action.PRINT");
		//Utility.getDefaultPath("PO")
//		i.setPackage("com.hp.android.print");
//		i.putExtra("com.hp.android.print", "com.hp.android.print.preview.ShareFilesPreviewActivity");
//		i.setClassName("com.hp.android.print","com.hp.android.print.preview.ShareFilesPreviewActivity");
 		i.setDataAndType(Uri.fromFile(file), "application/pdf");
		try {
//			activity.startActivity(i);
			activity.startActivityForResult(i, 1001);
		} catch (ActivityNotFoundException e) {
			// TODO Auto-generated catch block
			showDialog("Maaf, anda harus download Aplikasi HP ePrint");
		}

	}
	
	private void print(String status){
		Nset set = Nset.readJSON(status);
		
		int sts = set.getData("STATUS").toInteger();
		if (sts > 0) {			
			String pathFolder = Utility.getDefaultPath("PO");
			
			File dir = new File(pathFolder);
			if (!dir.exists())
				dir.mkdirs();
			
			Recordset recTtd = Nikitaset.getRecordset(Nset.newObject().setData("header", set.getData("fieldnametandatangan")).setData("data", set.getData("contenttandatangan") ));
			if (recTtd.getRows() > 0) {
				if (!new File(Utility.getDefaultTempPath(recTtd.getText(0, "user_id"))).exists()) {
					if (recTtd.getText(0, "user_id") != null || !recTtd.getText(0, "user_id").equalsIgnoreCase("")) {
						downloadImage(recTtd.getText(0, "user_id"), set);
					}
				}else{
					String path = ClassPrinter.createDocument(set);
					print(new File(path));
				}
					
				
			}else{
				String path = ClassPrinter.createDocument(set);
				print(new File(path));
			}
		}else {
			String msg = set.getData("message").toString();
			AdapterDialog.showMessageDialog(activity, "Information", msg);
		}
		
	}
	
	private void downloadImage(final String userID, final Nset set){
		final String url=SettingActivity.URL_SERVER + "viewimageservlet?userid="+userID+"&master=signature&filetype=blob&imei=&rnd=&v=100&jenis=user";
		Messagebox.showProsesBar(activity, new Runnable() {
			
			@Override
			public void run() {
				Utility.saveImage(userID, url);
			}
		}, new Runnable() {
			
			@Override
			public void run() {
				String path = ClassPrinter.createDocument(set);
				print(new File(path));
			}
		});
	}
	
	public void showStatus(String status){
		Nset set = Nset.readJSON(status);
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();  
		alertDialog.setTitle("Informasi");
		alertDialog.setMessage(set.getData("message").toString());
		if (set.getData("STATUS").toString().equalsIgnoreCase("1")) {
			
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Print", new DialogInterface.OnClickListener(){  
			public void onClick(DialogInterface dialog, int which){ 
					String URL = Utility.getSetting(activity, "URL_TRACK", "")+"ServiceMobile/trackingservlet?applno=";
					URL = Utility.getURLenc(URL, data.getData("APPL_NO").toString(), "&act=print&v=100");
//					URL = Utility.getURLenc(URL, "10116010252", "&act=print&v=100");
//					URL = Utility.getURLenc(URL, "10116000732", "&act=print&v=100");
					hitServerPO(URL, false);
				}
			}); 
			
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					dialog.dismiss();
				}
			}); 
			
		}else if(set.getData("STATUS").toString().equalsIgnoreCase("0")){
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Ok", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					dialog.dismiss();
				}
			});
		} 		
		alertDialog.show();
	}
	
	/**
	 * function shows Android-Native Alert Dialog
	 */
	public void showDialog(String msg){
		
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();  
		alertDialog.setTitle("Informasi");
		alertDialog.setMessage(msg);  
		
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Download", new DialogInterface.OnClickListener(){  
			public void onClick(DialogInterface dialog, int which){  
				
				String url = "https://play.google.com/store/apps/details?id=com.hp.android.print&hl=en";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				activity.startActivity(i);
			}
		}); 
		
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		}); 
		alertDialog.show();
	}
}