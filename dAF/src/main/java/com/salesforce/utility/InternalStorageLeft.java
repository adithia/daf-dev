package com.salesforce.utility;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

import java.text.DecimalFormat;

import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;

public class InternalStorageLeft {

    private int blocker;
    private int warning;
    private double storage;
    private String sisaStorage;
    private String totalStorage;
    private String sisaStoragePersen;

    public InternalStorageLeft() {
        String input = Global.getText("blocker.storage").toLowerCase();
//        if(input.isEmpty()||input == null){
//        	Recordset tmp = Connection.DBquery("select ")
//        	input =  
//        }
        if(input.contains("%")){
            blocker = Integer.valueOf(input.substring(0,input.indexOf("%")));
            warning = Integer.valueOf(Global.getText("minimum.storage").substring(0,Global.getText("minimum.storage").toLowerCase().indexOf("%")));
            storage = InternalStorageLeft.getAvaibleStoragePersen();
            sisaStorage = formatSize(InternalStorageLeft.avaibleInternalStorage());
            totalStorage= formatSize(InternalStorageLeft.totalInternalStorage());
            sisaStoragePersen = formatString(InternalStorageLeft.getAvaibleStoragePersen())+"%";
        }else if(input.contains("mb")){
            blocker = Integer.valueOf(input.substring(0,input.indexOf("m")));
            warning = Integer.valueOf(Global.getText("minimum.storage").substring(0,Global.getText("minimum.storage").toLowerCase().indexOf("m")));
            storage = formatSize2(InternalStorageLeft.avaibleInternalStorage());
            sisaStorage = formatSize(InternalStorageLeft.avaibleInternalStorage());
            totalStorage= formatSize(InternalStorageLeft.totalInternalStorage());
            sisaStoragePersen = formatString(InternalStorageLeft.getAvaibleStoragePersen())+"%";
        }else{
            blocker = Integer.valueOf(input);
            warning = Integer.valueOf(Global.getText("minimum.storage"));
            long tmp = InternalStorageLeft.avaibleInternalStorage();
            storage = formatSize2(tmp);
            sisaStorage = formatSize(tmp);
            totalStorage= formatSize(InternalStorageLeft.totalInternalStorage());
            sisaStoragePersen = formatString(InternalStorageLeft.getAvaibleStoragePersen())+"%";
        }
    }

    public static double avaibleInternalStoragePersen(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return getAvaibleStoragePersen();
        }else{
            return getAvaibleStoragePersen2();
        }
    }

    public static long avaibleInternalStorage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return getAvaibleStorage();
        }else{
            return getAvaibleStorage2();
        }
    }

    public static long totalInternalStorage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return getTotalStorage();
        }else{
            return getTotalStorage2();
        }
    }

    public static double getAvaibleStoragePersen(){
        return (formatSize2(avaibleInternalStorage())*100)/formatSize2(totalInternalStorage());
//        return (formatSize2(getAvaibleStorage())*100)/formatSize2(getTotalStorage());
    }

    public static double getAvaibleStoragePersen2() {
        return (formatSize2(getAvaibleStorage2())*100)/formatSize2(getTotalStorage2());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static long getAvaibleStorage(){
        StatFs a = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return a.getAvailableBlocksLong()*a.getBlockSizeLong();
    }

    public static long getAvaibleStorage2(){
        StatFs a = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (long)a.getAvailableBlocks()*(long)a.getBlockSize();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static long getTotalStorage(){
        StatFs a = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return a.getBlockCountLong()*a.getBlockSizeLong();
    }

    public static long getTotalStorage2(){
        StatFs a = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (long)a.getBlockCount()*(long)a.getBlockSize();
    }

    public static String formatSize(Long size){
        String satuan = "";
        double hasil = 0;
        if(size>1024){
            hasil=size/1024;
            satuan = "KB";
            if(hasil>1024){
                hasil=hasil/1024;
                satuan = "MB";
                if(hasil>1024) {
                    hasil = hasil / 1024;
                    satuan = "GB";
                }
            }
        }
        return new DecimalFormat("#.##").format(hasil)+satuan;
    }

    public static Double formatSize2(Long size) {
        double hasil = 0;
        if (size > 1024) {
            hasil = size / 1024;
            if (hasil > 1024) {
                hasil = hasil / 1024;
            }
        }
        return hasil;
    }

    public static String formatString(double size){
        return new DecimalFormat("#.##").format(size);
    }

    public int getBlocker () {
        return blocker;
    }

    public int getWarning () {
        return warning;
    }

    public double getStorage () {
        return storage;
    }

    public String getSisaStorage () {
        return sisaStorage;
    }
    }

