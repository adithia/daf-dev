package com.salesforce.utility;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daf.activity.LoginActivity;
import com.daf.activity.SettingActivity;
import com.salesfoce.ssl.EasySSLSocketFactory;
import com.salesforce.R;
import com.salesforce.adapter.SpinnerAdapter;
import com.salesforce.component.Component;
import com.salesforce.connection.Syncronizer;
import com.salesforce.database.Connection;
import com.salesforce.database.Nset;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Global;
import com.salesforce.stream.ImagePost;
import com.salesforce.stream.Stream;

@SuppressLint({"SimpleDateFormat", "DefaultLocale"})
public class Utility {
    private static Context defContext;
    public static Context sevContext;
    public static int KeepRatioW;
    public static int flagLogin;
    public static String JOBCODE;
    public static String OFFICE_CODE;
    public static String ttdPath;
    private static int mYear, mMonth, mDay;
    private static String[] arrMonth = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    private static String[] nameMonth = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static double latitude, longitude, Accuracy;
    public static Component comp;
    public static Activity activity;
    public static boolean finishSign, saveComponent;
    public static boolean backgroundOrder, backgroundListItem, sendingData, backgroundImage, backgroundLocation, backLostImage, autoLogout;
    //	public static Context act;
    public static int requestCodeCamera = 0, requestCodeGallery = 0, requesTtd = 0;
//	public static String orderNew, orderOld;

    public static String getLoginDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String getLoginTime() {
        return new SimpleDateFormat("HH.mm").format(new Date());
    }

    public static ProgressDialog showProgresbar(Context context, String message) {
        ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        return mProgressDialog;
    }

    public static Number getNumber(Object n) {
        if (n instanceof Number) {
            return ((Number) n);

        } else if (isDecimalNumber(String.valueOf(n))) {
            return Double.valueOf(String.valueOf(n));
        } else if (isLongIntegerNumber(String.valueOf(n))) {
            return Long.valueOf(String.valueOf(n));
        }
        return 0;
    }

    public static double getDouble(Object n) {
        return getNumber(n).doubleValue();
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    public static boolean isDecimalNumber(String str) {
        return str.matches("^[-+]?[0-9]*.?[0-9]+([eE][-+]?[0-9]+)?$");
    }

    public static boolean isLongIntegerNumber(String str) {
        return str.matches("-?\\d+");
    }

    public static boolean isAnEmail(String value) {
        boolean isAnEmail = false;
        return isAnEmail = value.trim().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    public static boolean isContainSpecialChar(String value) {
        if (value.equals("")) {
            return false;
        }
        for (int i = 0; i < value.length(); i++) {
            if ("qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM ".contains(value.substring(i, i + 1))) {

            } else {
                return true;
            }
        }
        return false;

    }

    public static boolean notContainNumber(String value) {
        if (value.equals("")) {
            return false;
        }
        for (int i = 0; i < value.length(); i++) {
            if ("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ".contains(value.substring(i, i + 1))) {

            } else {
                return true;
            }
        }
        return false;

    }

    public static boolean isContainValue(String value, Vector<String> v) {
        for (int i = 0; i < v.size(); i++) {
            if (value.equalsIgnoreCase(v.elementAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static boolean isContainValue2(String value, Vector<String> v) {
        if (v.contains(value)) {
            return true;
        }
        return false;
    }

    public static boolean isContainSpecialCharWithEnter(String value) {
        if (value.equals("")) {
            return false;
        }
        for (int i = 0; i < value.length(); i++) {
            if ("qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM \n".contains(value.substring(i, i + 1))) {

            } else {
                return true;
            }
        }
        return false;

    }

    public static boolean isFirstZero(String value) {
        if (value.equals("")) {
            return false;
        }
        if (!value.startsWith("0")) {
            return true;
        }
        return false;
    }

    public static boolean isContainChar(String value) {
        if (value.equals("")) {
            return false;
        }
        for (int i = 0; i < value.length(); i++) {
            if ("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".contains(value.substring(i, i + 1))) {
                return true;
            }
        }
        return false;

    }

    public static boolean isContainFile(String fileName, String dir) {
        if (new File(dir, fileName).exists()) {
            return true;
        }
        return false;
    }

    public static void compressImage(String file) {
        int quality = 80;
        int width = 540;
        String format = "png";
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file, options);
            int scale = options.outWidth / width;


            options = new BitmapFactory.Options();
            options.inSampleSize = scale;

            Bitmap bmp = BitmapFactory.decodeFile(file, options);

            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(format.equals("jpg") ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.PNG, quality, fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
        }
    }
	/*public static boolean isContainSpecialChar(String value){
		String regex1 = "^[a-zA-Z0-9]*$]";
		boolean contain = value.trim().matches(regex1);
		if (contain) {
			return true;
		}
		return false;
//		try {
//			String regex1 = "^[a-zA-Z0-9~@#$^*()_+=[\\]{}|\\,.?: -]*$]";
//			Pattern regex = Pattern.compile(regex1);
//			Matcher matcher = regex.matcher(value);
//			if (matcher.find()){
//			    // Do something
//				return true;
//			}
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			return false;
//		}
//		return false;
	}*/

    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo().isConnectedOrConnecting();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean wifiActive(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifi.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static String getImei(Context context) {
//        return ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        return getImeiOrUUID(context).getImei();
    }

    public static ModelDeviceIdentity getImeiOrUUID(Context context) {
        TelephonyManager tm = (TelephonyManager) context.
                getSystemService(android.content.Context.TELEPHONY_SERVICE);

        String imei = "";
        String uuid = "";
        Boolean isAndriod10 = false;
        try {
            if (Build.VERSION.SDK_INT >= 29) {
                isAndriod10 = true;
            /*String ICCID = tm.getSimSerialNumber();
            if (ICCID != null) {
                imei = ICCID.toUpperCase();
            }*/
//            uuid = getMacAddr();
                imei = Utility.getSetting(context, "DeviceId", "");
            imei = "355046091224513";
            } else {
                String tmpImei = tm.getDeviceId().toUpperCase();
                imei = tmpImei;
            }
        } catch (Exception e) {

        }
        return new ModelDeviceIdentity(imei, isAndriod10, uuid);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    //res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public static boolean certificateFIFExist() {
        boolean isCertExist = false;
        try {
            KeyStore ks = KeyStore.getInstance("AndroidCAStore");
            if (ks != null) {
                ks.load(null, null);
                Enumeration aliases = ks.aliases();
                while (aliases.hasMoreElements()) {
                    String alias = (String) aliases.nextElement();
                    java.security.cert.X509Certificate cert = (java.security.cert.X509Certificate) ks.getCertificate(alias);

                    if (cert.getIssuerDN().getName().contains("FIF-Certificate")) {
                        isCertExist = true;
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (java.security.cert.CertificateException e) {
            e.printStackTrace();
        }
        return isCertExist;
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return Formatter.formatIpAddress(inetAddress.hashCode());//inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
//	          Log.e("IP Address", ex.toString());
        }
        return null;
    }

    @SuppressWarnings("static-access")
    public static String getIpAddressWifi(Context context) {
        try {
            WifiManager wm = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
            String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
            return ip;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "0.0.0.0";
    }

    public static boolean cellularNetworkDataActive(Context context) {
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true);
            // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);
        } catch (Exception e) {
            // Some problem accessible private API
            // TODO do whatever error handling you want here
        }
        return mobileDataEnabled;
    }

    public static boolean isTimeToLogin() {
        //untuk pertama kali login
        if (Global.getText("service.on.pagi") == null || Global.getText("service.on.pagi").equalsIgnoreCase("")) {
            return true;
        }
        Calendar calServer = convertDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " " + Global.getText("service.on.pagi"));
        Date today = new Date();
        Date dateServer = calServer.getTime();
        if (dateServer.before(today)) {
            return true;
        }
        return false;
    }

    public static boolean isTimeToOff() {
        if (Global.getText("service.off.malam") == null || Global.getText("service.off.malam").equalsIgnoreCase("")) {
            return false;
        }
        Calendar calServer = convertDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " " + Global.getText("service.off.malam"));
        Date today = new Date();
        Date dateServer = calServer.getTime();
        if (dateServer.before(today)) {
            return true;
        }
        return false;
    }

    public static Calendar convertDate(String strDate) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
            date = sdf.parse(strDate);
            calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }
	 
	 /*public static Calendar convertTasklits(String strDate){
			Calendar calendar = Calendar.getInstance();
			try {
				Date date = new Date();
//				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy HH:mm:ss");
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy");
				date = sdf.parse(strDate);
				calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return calendar;
		}*/

    public static Calendar convertDateDraft(String strDate) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
            date = sdf.parse(strDate);
            calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static Calendar convertDateTo(String strDate, String format) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(strDate);
            calendar.set(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static String changeFormatDate(String strDate, String formatawal, String formatakhir) {
        try {

            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(formatawal, Locale.ENGLISH);
            SimpleDateFormat sdf2 = new SimpleDateFormat(formatakhir, Locale.ENGLISH);
//			 SimpleDateFormat sdf = new SimpleDateFormat(formatawal);
//			 SimpleDateFormat sdf2 = new SimpleDateFormat(formatakhir);
            date = sdf.parse(strDate);
            return sdf2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }

    public static String convertDate2(String strDate) {
        try {

            Date date = new Date();

            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-mm-dd");

            date = sdf.parse(strDate);
            return sdf2.format(date);
        } catch (Exception e) {
        }
        return "";

    }

    @SuppressLint("SimpleDateFormat")
    public static String getTodayDate() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
    }

    @SuppressLint("SimpleDateFormat")
    public static String getTodayDate2() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    @SuppressLint("SimpleDateFormat")
    public static String getDateStandart() {
        return new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(new Date());
    }

    @SuppressLint("SimpleDateFormat")
    public static String getDateDAF() {
        return new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH).format(new Date());
    }


    @SuppressLint("SimpleDateFormat")
    public static String getDateToString(Date date) {
        return new SimpleDateFormat("dd-MMM-yyy", Locale.ENGLISH).format(date);
    }

    public static String getGMT() {

        try {
            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));

            //Local time zone
            SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

            //Time in GMT
            String timezone = "";
//			String fgd = TimeZone.getTimeZone("GMT").getDisplayName();
            timezone = dateFormatLocal.parse(dateFormatGmt.format(new Date())).toString();

            String[] lc = split(timezone, " ");
            timezone = lc[4].substring(lc[4].indexOf("+") + 1, lc[4].indexOf(":"));

            return timezone;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    public static void setDecimalCurr(View view, String param) {

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
//		symbols.setCurrencySymbol("IDR ");
        symbols.setCurrencySymbol("");
        symbols.setMonetaryDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        formatter.setDecimalFormatSymbols(symbols);
        String backCome = "";
//		if (param.contains(".")) {
//			backCome = param.substring(param.indexOf(".")+1);
//			param =  param.substring(0, param.indexOf("."));
//		}

        if (view.getClass() == TextView.class) {
            TextView textView = (TextView) view;
            textView.setText(formatter.format(Double.parseDouble(param)) + backCome);
        } else if (view.getClass() == EditText.class) {
            EditText editText = (EditText) view;
            editText.setText(formatter.format(Double.parseDouble(param)) + backCome);
        }

    }

    public static String getDecimalCurr(String param) {

        try {

            DecimalFormat formatter = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
//			symbols.setCurrencySymbol("IDR ");
            symbols.setCurrencySymbol("");
            symbols.setMonetaryDecimalSeparator(',');
            symbols.setGroupingSeparator('.');
            formatter.setDecimalFormatSymbols(symbols);

            return formatter.format(Double.parseDouble(param));
        } catch (Exception e) {
            // TODO: handle exception
            return "0";
        }

    }

    @SuppressLint("SimpleDateFormat")
    public static String getTime() {
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    public static InputFilter uppercase = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // TODO Auto-generated method stub
            for (int i = start; i < end; i++) {
                if (Character.isLowerCase(source.charAt(i))) {
                    return source.toString().toUpperCase(Locale.getDefault());
                }
            }
            return null;

        }
    };

    public static DatePickerDialog showDatePicker(Context context, View v) {
        Calendar c = Calendar.getInstance();
        return new DatePickerDialog(context, setDatePickerDialog(v), c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
    }

    public static DatePickerDialog showDatePickerLessThanSysdate(Context context, View v) {
        Calendar c = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(context, specialDate(v), c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());

//		datePicker.getDatePicker().setMaxDate(getDateMin());
//		datePicker.getDatePicker().setMinDate(getDateYear());

        return datePicker;
    }

    public static DatePickerDialog.OnDateSetListener specialDate(final View v) {

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
//				String sdate = LPad(mDay + "", "0", 2) + "-" + nameMonth[mMonth] + "-" + mYear ;
                String sdate = LPad(dayOfMonth + "", "0", 2) + "-" + nameMonth[monthOfYear] + "-" + year;
                if (v.getClass() == EditText.class) {
                    EditText edit = (EditText) v;
                    edit.setText(sdate);
                } else if (v.getClass() == TextView.class) {
                    TextView text = (TextView) v;
                    text.setText(sdate);
                }
            }
        };

        return date;
    }


    public static long getDateYear() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) - 60, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        return cal.getTimeInMillis();
    }

    public static long getDateMin() {
        Calendar cal = Calendar.getInstance();
//		cal.set(date.getYear(), date.getMonth(), date.getDay()-1);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH) - 1);
//		cal.set(mYear, mMonth, mDay-1);
        return cal.getTimeInMillis();
    }


    public static DatePickerDialog.OnDateSetListener setDatePickerDialog(final View v) {

        DatePickerDialog.OnDateSetListener mDateSetListenerOrder = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
                String sdate = mYear + "/" + arrMonth[mMonth] + "/" + LPad(mDay + "", "0", 2);

                if (v.getClass() == EditText.class) {
                    EditText edit = (EditText) v;
                    edit.setText(sdate);
                } else if (v.getClass() == TextView.class) {
                    TextView text = (TextView) v;
                    text.setText(sdate);
                }

            }
        };

        return mDateSetListenerOrder;
    }

    public static String LPad(String schar, String spad, int len) {
        String sret = schar;
        for (int i = sret.length(); i < len; i++) {
            sret = spad + sret;
        }
        return new String(sret);
    }

    public static InputFilter doubleSpaceNSpecialCharFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // TODO Auto-generated method stub
            for (int i = start; i < end; i++) {
                if ((Character.isSpace(source.charAt(i)) && dend == 0) || (Character.isSpace(source.charAt(i)) &&
                        dend > 0 && Character.isSpace(dest.charAt(dend - 1))) || !Character.isLetterOrDigit(source.charAt(i))) {
                    // return source.toString().substring(0,
                    // source.toString().length() - 2);
                    return "";
                }
            }
            return null;

        }
    };

    public static InputFilter specialCharFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // TODO Auto-generated method stub

            boolean isContainSpecialChar = Utility.isContainSpecialChar(source.toString());

            if (source != null && isContainSpecialChar) {
                return "";
            }
            return null;

        }
    };

    public static InputFilter specialCharFilterWithEnter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // TODO Auto-generated method stub

            boolean isContainSpecialChar = Utility.isContainSpecialCharWithEnter(source.toString());

            if (source != null && isContainSpecialChar) {
                return "";
            }
            return null;

        }
    };

    public static InputFilter charNoNumber = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // TODO Auto-generated method stub

            boolean isContainSpecialChar = Utility.notContainNumber(source.toString());

            if (source != null && isContainSpecialChar) {
                return "";
            }
            return null;

        }
    };

    public static InputFilter firstZero = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // TODO Auto-generated method stub

            boolean isfirstZore = Utility.isFirstZero(dest.toString());

            if (source != null && isfirstZore) {
                return "0" + source;
            } else if (dest.toString().equalsIgnoreCase("")) {
                return "0" + source;
            }
            return null;

        }
    };

    public static void setAdapterSpinner(Context context, Spinner spinner, ArrayList<String> object) {
        SpinnerAdapter adapter = new SpinnerAdapter(context, R.layout.simple_spinner, object);
        spinner.setAdapter(adapter);
    }

    public static void setAdapterSpinner(Context context, Spinner spinner, Vector<String> object) {
        SpinnerAdapter adapter = new SpinnerAdapter(context, R.layout.simple_spinner, object);
        spinner.setAdapter(adapter);
    }

    public static void setAdapterSpinner(Context context, Spinner spinner, List<String> object) {
        SpinnerAdapter adapter = new SpinnerAdapter(context, R.layout.simple_spinner, object);
        spinner.setAdapter(adapter);
    }


    public static String replace(String _text, String _searchStr, String _replacementStr) {
        StringBuffer sb = new StringBuffer();
        int searchStringPos = _text.indexOf(_searchStr);
        int startPos = 0;
        int searchStringLength = _searchStr.length();
        while (searchStringPos != -1) {
            sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
            startPos = searchStringPos + searchStringLength;
            searchStringPos = _text.indexOf(_searchStr, startPos);
        }
        sb.append(_text.substring(startPos, _text.length()));
        return sb.toString();
    }

    public static String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public static Context getAppContext() {
        return defContext != null ? defContext : sevContext;
    }

    public static void setServContext(Context appContext) {
        Utility.sevContext = appContext;
        Utility.KeepRatioW = Integer.valueOf(appContext.getResources().getString(R.string.keep_ratio));
    }

    public static void setAppContext(Context appContext) {
        Utility.defContext = appContext;
        Utility.KeepRatioW = Integer.valueOf(appContext.getResources().getString(R.string.keep_ratio));
    }

    public static String getExternalPath() {
        try {
            if (getAppContext().getExternalFilesDir(null).getPath().length() >= 3) {
                return getAppContext().getExternalFilesDir(null).getPath() + "/";
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static String getDefaultPath() {
        String defpath = getExternalPath();

        if (defpath != null) {
            if (SettingActivity.URL_STORAGE == null || SettingActivity.INTERNAL_STORAGE == null) {
            } else if (SettingActivity.URL_STORAGE.equals(SettingActivity.INTERNAL_STORAGE)) {
                //return  appContext.getFilesDir().getPath() + "/";
//				return Environment.getExternalStorageDirectory().toString();
            }
            return defpath;
        }
        return getAppContext().getFilesDir().getPath() + "/";
//		return "/storage/sdcard0/DAF/";
    }

    public static String getDefaultTempPath(String fname) {
        return getDefaultPath(getAppContext().getResources().getString(R.string.temp_folder)) + fname;
    }

    public static String getPathPDF(String fname) {
        return getDefaultPath("PDF") + fname;
    }

    public static String getDefaultTempPath() {
        return getDefaultPath(getAppContext().getResources().getString(R.string.temp_folder));
    }

    public static String getDefaultImagePath() {
        return getDefaultPath("images");
    }

    public static String getDefaultPDF() {
        return getDefaultPath("PDF");
    }

    public static String getDefaultPDF(String fname) {
        return getDefaultPath("PDF/") + fname;
    }

    public static String getDefaultBio() {
        return getDefaultPath("Bio");
    }

    public static String getDefaultBio(String fname) {
        return getDefaultPath("Bio/") + fname;
    }

    public static String getDefaultSign() {
        return getDefaultPath("Sign");
    }

    public static String getDefaultSign(String fname) {
        return getDefaultPath("Sign/") + fname;
    }

    public static String getDefaultImagePath(String fname) {
        return getDefaultPath("images/") + fname;
    }

    public static String getDefaultPath(String fname) {
        return getDefaultPath() + fname;
    }

    public static String getInternalStorageDocPath() {
        String pathFolder = getAppContext().getDir("Doc", getAppContext().MODE_PRIVATE).getAbsolutePath();
        return pathFolder;
    }

    public static String getReportPath(String folder) {
        if (folder != null && folder != "") {
            return getAppContext().getFilesDir() + "/reports/jrxml/" + folder;
        } else {
            return getAppContext().getFilesDir() + "/reports/jrxml/";
        }
    }

    public static void createFolderAll(String folder) {
        folder = folder.replace("//", "//");
        String[] s = split(folder, "/");
        String path = "";
        for (int i = 0; i < s.length - ((folder.endsWith("/") ? 0 : 1)); i++) {
            if (!s[i].equals("")) {
                path = path + s[i] + "/";
                createFolder(path);
            }
        }
    }

    public static Vector<Vector<String>> splitVector(String original, String separatorcol, String separatorrow) {
        Vector<Vector<String>> nodes = new Vector<Vector<String>>();
        int index = original.indexOf(separatorrow);
        while (index >= 0) {
            nodes.addElement(splitVector(original.substring(0, index), separatorcol));
            original = original.substring(index + separatorrow.length());
            index = original.indexOf(separatorrow);
        }
        nodes.addElement(splitVector(original, separatorcol));
        return nodes;
    }

    public static Vector<String> splitVector(String original, String separator) {
        Vector<String> nodes = new Vector<String>();
        int index = original.indexOf(separator);
        while (index >= 0) {
            nodes.addElement(original.substring(0, index));
            original = original.substring(index + separator.length());
            index = original.indexOf(separator);
        }
        nodes.addElement(original);
        return nodes;
    }

    public static String getDataSplit(String data, String separator, boolean first) {
        Vector<String> dataString = Utility.splitVector(data, separator);
        if (dataString.size() > 0) {
            if (first) {
                return dataString.elementAt(0);
            } else {
                return dataString.elementAt(1);
            }
        }
        return "";
    }

    public static String[] split(String original, String separator) {
        Vector<String> nodes = splitVector(original, separator);
        String[] result = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++) {
                result[loop] = (String) nodes.elementAt(loop);
            }
        }
        nodes.removeAllElements();
        return result;
    }

    public static String postHttpConnection(String stringURL, String... param) {
        Hashtable<String, String> arg = new Hashtable<String, String>();
        for (int i = 0; i < param.length; i++) {
            int key = param[i].indexOf("=");
            if (key != -1) {
                arg.put(param[i].substring(0, key), param[i].substring(key + 1));
            }
        }
//		17/12/2018
        return ImagePost.postHttpConnectionWithPicture(stringURL, arg, null, null, null);
    }

    public static String postMultipartConnection(String stringURL, String... param) {
        String[] keys = new String[param.length];
        String[] data = new String[param.length];

        for (int i = 0; i < param.length; i++) {
            int key = param[i].indexOf("=");
            if (key != -1) {
                keys[i] = param[i].substring(0, key);
                data[i] = param[i].substring(key + 1);

            }
        }

        return postMultipart2(stringURL, keys, data);
    }


    public static String postHttpConnection(String stringURL, Hashtable<String, String> param) {
//		17/12/2018
        return ImagePost.postHttpConnectionWithPicture(stringURL, param, null, null, null);
    }

    public static String postHttpConnection(String stringURL, Hashtable<String, String> param, String fname, String fnamefullpath, String reqId) {
//		17/12/2018
        return ImagePost.postHttpConnectionWithPicture(stringURL, param, fname, fnamefullpath, reqId);
    }

    public static Recordset jsonToRecordset(String data) {
        return Stream.downStream(data);
    }

    public static String getHttpConnection(String stringURL) {
        stringURL = nikitaUrl(stringURL);
        StringBuffer data = new StringBuffer();
        try {

            InputStream is = doGet(stringURL).getEntity().getContent();

            int current = 0;
            while ((current = is.read()) != -1) {
                data.append((char) current);
            }

        } catch (Exception e) {
            data.append(e.getMessage());
        }
        return data.toString();// decompress(data.toString());
    }

    public static String getHttpConnection2(String stringURL) {
        StringBuffer data = new StringBuffer();
        try {

            InputStream is = doGet(stringURL).getEntity().getContent();

            int current = 0;
            while ((current = is.read()) != -1) {
                data.append((char) current);
            }

        } catch (Exception e) {
            data.append(e.getMessage());
        }
        return data.toString();// decompress(data.toString());
    }

    public static void getHttpConnection(String stringURL, String fname) throws Exception {

        stringURL = nikitaUrl(stringURL);
        StringBuffer data = new StringBuffer();
        FileOutputStream foOutputStream = new FileOutputStream(fname);

        InputStream is = doGet(stringURL).getEntity().getContent();

        int len = 0;
        byte[] buffer = new byte[1024];
        while ((len = is.read(buffer)) != -1) {
            foOutputStream.write(buffer, 0, len);
        }
        foOutputStream.flush();
        foOutputStream.close();
    }

    public static void writeToFile(String path, String content) {
        try {
            StringBuffer sbf = new StringBuffer();
            FileOutputStream foOutputStream = new FileOutputStream(path);

            InputStream is = IOUtils.toInputStream(content, "UTF-8");//doGet(stringURL).getEntity().getContent();

            int len = 0;
            byte[] buffer2 = new byte[1024];
            while ((len = is.read(buffer2)) != -1) {
                foOutputStream.write(buffer2, 0, len);
            }
            foOutputStream.flush();
            foOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	
	/*public static String decompress(byte[] str) {
        String outStr = "";
        try {
            if (str == null) {
                return str.toString();
            }
            GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str));
            BufferedReader bf = new BufferedReader(new InputStreamReader(gis));

            String line;
            while ((line = bf.readLine()) != null) {
                outStr += line;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return outStr;
    }*/

    public static HttpResponse doGet(String url) throws Exception {
        try {
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", getSslSocketFactory(), 443));//EasySSLSocketFactory

            HttpParams params = new BasicHttpParams();
            params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
            DefaultHttpClient httpClient = new DefaultHttpClient(cm, params);


            HttpGet httpget = new HttpGet(url);
//	    httpget.addHeader("Accept-Encoding", "gzip");	
            httpget.setParams(params);
            HttpResponse response;
            response = httpClient.execute(httpget);
            return response;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

    }
	
	/*public static byte[] decompress(byte[] contentBytes){
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try{
			IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(contentBytes)), out);
		} catch(IOException e){
			throw new RuntimeException(e);
		}
		return out.toByteArray();
	}*/

    public static String decompressGzip(String str) {
        String outStr = "";
        try {
            if (str == null || str.length() == 0) {
                return str;
            }

            GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str.getBytes("ISO-8859-1")));
            BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "ISO-8859-1"));

            String line;
            while ((line = bf.readLine()) != null) {
                outStr += line;
            }

        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return outStr;
    }

    public static byte[] compressGzip(String str) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] wb = new byte[1024];

        try {

            if (str == null || str.length() == 0) {
                return wb;
            }

            GZIPOutputStream gzip = new GZIPOutputStream(out);
            gzip.write(str.getBytes());
            gzip.close();

//	        outStr = out.toString("ISO-8859-1");

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return out.toByteArray();
    }

    public static InputStream getCompressed(InputStream is) throws IOException {
        byte data[] = new byte[2048];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(bos);
        BufferedInputStream entryStream = new BufferedInputStream(is, 2048);
        ZipEntry entry = new ZipEntry("");
        zos.putNextEntry(entry);
        int count;
        while ((count = entryStream.read(data, 0, 2048)) != -1) {
            zos.write(data, 0, count);
        }
        entryStream.close();
        zos.closeEntry();
        zos.close();

        return new ByteArrayInputStream(bos.toByteArray());
    }

    private static String uncompressInputStream(InputStream inputStream) throws IOException {
        StringBuilder value = new StringBuilder();

        GZIPInputStream gzipIn = null;
        InputStreamReader inputReader = null;
        BufferedReader reader = null;

        try {
            gzipIn = new GZIPInputStream(inputStream);
//	        inputReader = new InputStreamReader(gzipIn, "UTF-8");
            inputReader = new InputStreamReader(gzipIn, "ISO-8859-1");
            reader = new BufferedReader(inputReader);

            String line = "";
            while ((line = reader.readLine()) != null) {
                value.append(line + "\n");
            }
        } finally {
            try {
                if (gzipIn != null) {
                    gzipIn.close();
                }

                if (inputReader != null) {
                    inputReader.close();
                }

                if (reader != null) {
                    reader.close();
                }

            } catch (IOException io) {

                io.printStackTrace();
            }

        }

        return value.toString();
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static SSLSocketFactory getSslSocketFactory() {
        SSLSocketFactory sf = null;
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");//"BKS
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs)
            InputStream in = Utility.getAppContext().getResources().openRawResource(R.raw.bcafx);
            //InputStream in = Utility.getAppContext().getAssets().open("bcafx.bks");
            try {
                // Initialize the keystore with the provided trusted certificates
                // Also provide the password of the keystore
                trusted.load(in, "bcaf123".toCharArray());
            } finally {
                in.close();
            }
            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.
            sf = new SSLSocketFactory(trusted);
            // Hostname verification from certificate
            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return sf;
    }

    private static long iRandom = 1;//System.currentTimeMillis()

    public static String nikitaUrl(String url) {
        iRandom = iRandom + 1;
        String token = "";
        try {
//		 		token=Utility.getSetting(Utility.getAppContext(), "Token", ""+iRandom);
            token = Utility.getSetting(Utility.getAppContext(), "Token", "");
        } catch (Exception e) {
        }
        if (token.equals("")) {
            token = "" + iRandom;
        }

        StringBuffer result = new StringBuffer();
        StringBuffer addParam = new StringBuffer();
        try {
            PackageInfo pInfo = Utility.getAppContext().getPackageManager().getPackageInfo(Utility.getAppContext().getPackageName(), 0);
            String version = URLEncoder.encode(pInfo.versionName).trim();


            /*TelephonyManager tm = (TelephonyManager) Utility.getAppContext().getSystemService(android.content.Context.TELEPHONY_SERVICE);
            String imei = tm.getDeviceId().toUpperCase().trim();*/
            String imei = Utility.getImeiOrUUID(Utility.getAppContext()).getImei();
            addParam.append("i=").append(imei).append("&u=").append(Utility.getSetting(Utility.getAppContext(), "USERNAME", "").trim()).append("&v=").append(version).append("&token=").append(token.trim());
        } catch (Exception e) {
            addParam = new StringBuffer();
            addParam.append("i=").append("imei").append("&u=").append("user").append("&v=").append("versi").append("&token=").append(token.trim());
        }


        if (url.contains("?")) {
            result.append(url.substring(0, url.indexOf("?") + 1)).append(addParam.toString()).append("&").append(url.substring(url.indexOf("?") + 1));
        } else if (url.contains("&")) {
            result.append(url.substring(0, url.indexOf("&"))).append("?").append(addParam.toString()).append(url.substring(url.indexOf("&")));
        } else {
            result.append(url).append("?").append(addParam.toString());
        }

        return result.toString().trim();
    }

    public static boolean getNewToken() {
        try {
            String url = SettingActivity.URL_SERVER;
            String[] sf = Utility.split(url, ":");
            url = (String) sf[0] + ":" + (String) sf[1] + ":" + (String) sf[2].substring(0, sf[2].indexOf("/")) + "/";
            url = url + "DafOauth2/oauth/token?username=daf&password=dc5000b1f422e6848590654186af9054&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=password";
            String jsonToken = Utility.getHttpConnection2(url);
            Nset set = Nset.readJSON(jsonToken);
            String token = set.getData("access_token").toString();
            if (!token.equalsIgnoreCase("")) {
                Utility.setSetting(Utility.getAppContext(), "Token", token);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean getRefresh_NewToken(Context context) {
        String url = SettingActivity.URL_SERVER;
        String[] sf = Utility.split(url, ":");
        url = (String) sf[0] + ":" + (String) sf[1] + ":" + (String) sf[2].substring(0, sf[2].indexOf("/")) + "/";
        url = url + "DafOauth2/oauth/token?refresh_token=" + Utility.getSetting(context, "Refresh_Token", "") + "&client_id=FifDaf&client_secret=2da1770602a697f83cdaaa8dc26ac2e4&grant_type=refresh_token";
        String jsonToken = Utility.getHttpConnection2(url);
        Nset set = Nset.readJSON(jsonToken);
        String refresh_token = set.getData("refresh_token").toString();
        String token = set.getData("access_token").toString();
        if (!refresh_token.equalsIgnoreCase("")) {
            Utility.setSetting(context, "Refresh_Token", refresh_token);
            Utility.setSetting(Utility.getAppContext(), "Token", token);
            return true;
        } else {
            return false;
        }
    }

    public static boolean getNewToken2() {
        try {
            String url = "http://testauthtoken.fifgroup.co.id:8380/auth/realms/fifgroup/protocol/openid-connect/token";
            String[] keys = new String[]{"username", "password", "client_id", "client_secret", "grant_type"};
            String[] data = new String[]{"repo", "repo123", "fifgroup-token", "261f1b80-7a18-438e-b9fa-2f9575c97e0b", "password"};
            /*Utility.getSetting(getAppContext(), "USERNAME", ""),
                    Utility.getSetting(getAppContext(), "PASSWORD", ""),*/
            String jsonToken = Utility.postMultiPartForGetToken(url, keys, data);
            Nset set = Nset.readJSON(jsonToken);
            String bearer = set.getData("token_type").toString();
            String token = set.getData("access_token").toString();
            if (!token.equalsIgnoreCase("")) {
//				Utility.setSetting(Utility.getAppContext(), "Token", bearer +" " + token);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }


    public static String postMultipart(String url, String[] keys, String[] data) {
        url = nikitaUrl(url);

//		Log.i("URL", url);
        String strResponse = null;
        try {

            HttpClient client = new DefaultHttpClient();

            //13kcrazy
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

            HttpParams params = new BasicHttpParams();
            params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


            params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 300000);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
            client = new DefaultHttpClient(cm, params);
            //13kcrazy

            HttpPost post = new HttpPost(url);

            MultipartEntity reqEntity = new MultipartEntity(

                    HttpMultipartMode.BROWSER_COMPATIBLE);

            for (int i = 0; i < keys.length; i++) {
                reqEntity.addPart(keys[i], new StringBody(data[i]));
            }
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			reqEntity.writeTo(out);
//			String cd = out.toString();
            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            strResponse = EntityUtils.toString(resEntity);
            String ege = strResponse;

            if (strResponse != null) {
//				Log.i("RESPONSE", strResponse);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

//			Log.e("Debug", "error: " + ex.getMessage(), ex);

        }
        return strResponse != null ? strResponse : "Connection Timeout";
    }

    public static String repeat(String sString, int iTimes) {
        String output = "";
        for (int i = 0; i < iTimes; i++)
            output = output + sString;
        return output;
    }

    @SuppressWarnings("deprecation")
    public static String getURLenc(String... get) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < get.length; i++) {
            buffer.append((i % 2) == 0 ? get[i] : URLEncoder.encode(get[i]));
        }
        return buffer.toString();
    }

    public static int getSettingInt(Context context, String key, int def) {
        return Utility.getInt(getSetting(context, key, def + ""));
    }

    public static String getSetting(Context context, String key, String def) {// baca
        // data
        // yang
        // disimpan(string)
        if (context != null) {
            SharedPreferences settings = context.getApplicationContext().getSharedPreferences(context.getResources().getString(R.string.shared_preference_name), 0);
            String silent = settings.getString(key, def);
            return silent;
        } else {
            return def;
        }
    }

    public static void setSetting(Context context, String key, String val) {// Simpat
        // data
        // string
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences(context.getResources().getString(R.string.shared_preference_name), 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public static void clearSetting(Context context) {
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences(context.getResources().getString(R.string.shared_preference_name), 0);
        String tmpDeviceId = settings.getString("DeviceId", "");
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        editor.putString("DeviceId", tmpDeviceId);
        editor.commit();

    }

    public static byte[] DownloadFromUrl(String surl) {

        try {
            URL url = new URL(surl);
            URLConnection ucon = url.openConnection();
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            ByteArrayBuffer baf = new ByteArrayBuffer(50);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            return baf.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static final String MD5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static void openPDF(Context context, String filename) {
        File pdfFile = new File(filename);
        if (pdfFile.exists()) {
            Uri path = Uri.fromFile(pdfFile);
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                context.startActivity(pdfIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static void openSignature(ImageView context, String path) {
        openSignature(context, path, 0);
    }

    public static void OpenImage(ImageView context, String path) {
        OpenImage(context, path, 0);
    }

    public static void openFingerQ(ImageView context, String path) {
        openFingerQ(context, path, 0);
    }

    public static void openFingerQ(ImageView context, String path, int defImage) {
        String pathFQ = path + ".imgicon.png";
        try {
            if (context == null) {
                if (new File(pathFQ).exists()) {
                    new File(pathFQ).delete();
                }
                return;
            }

            Utility.KeepRatioW = 128;

            if (!new File(pathFQ).exists()) {
                copyFingerImageFile(path, pathFQ);
            }
            path = pathFQ;

            if (path.trim().equals("")) {
                context.setImageResource(defImage);
                return;
            } else if (!new File(path).exists()) {
                context.setImageResource(defImage);
                return;
            }
            int scale = 1;// int height=1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            if (options.outWidth > context.getResources().getDisplayMetrics().widthPixels || options.outHeight > context.getResources().getDisplayMetrics().heightPixels) {
                scale = options.outWidth / Utility.KeepRatioW;// 960px
                scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
                scale = (scale % 2 != 0 ? (scale + 1) : scale);
                // height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
            } else {
                // height=context.getResources().getDisplayMetrics().heightPixels;
            }
            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            context.setImageBitmap(BitmapFactory.decodeFile(path, options));
        } catch (Exception e) {
            context.setImageResource(defImage);
        }
    }

    public static void openSignature(ImageView context, String path, int defImage) {
        try {
            Utility.KeepRatioW = 128;

            if (path.trim().equals("")) {
                context.setImageResource(defImage);
                return;
            } else if (!new File(path).exists()) {
                context.setImageResource(defImage);
                return;
            }
            int scale = 1;// int height=1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            if (options.outWidth > context.getResources().getDisplayMetrics().widthPixels || options.outHeight > context.getResources().getDisplayMetrics().heightPixels) {
                scale = options.outWidth / Utility.KeepRatioW;// 960px
                scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
                scale = (scale % 2 != 0 ? (scale + 1) : scale);
                // height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
            } else {
                // height=context.getResources().getDisplayMetrics().heightPixels;
            }
			
			
			/*if (options.outWidth > Utility.KeepRatioW || options.outHeight > Utility.KeepRatioW) {
				scale = options.outWidth / Utility.KeepRatioW;// 960px
				//scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
				scale = (scale % 2 != 0 ? (scale + 1) : scale);
				// height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
			} else {
				// height=context.getResources().getDisplayMetrics().heightPixels;
			}*/

            // ((LayoutParams)context.getLayoutParams()).height=height;
            // ((LayoutParams)context.getLayoutParams()).width=getResources().getDisplayMetrics().widthPixels;
            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            context.setImageBitmap(BitmapFactory.decodeFile(path, options));
        } catch (Exception e) {
            context.setImageResource(defImage);
        }
    }

    public static void OpenImage(ImageView context, String path, int defImage) {
        try {
            Utility.KeepRatioW = 128;

            if (path.trim().equals("")) {
                context.setImageResource(defImage);
                return;
            } else if (!new File(path).exists()) {
                context.setImageResource(defImage);
                return;
            }
            int scale = 1;// int height=1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
			
			/*
			if (options.outWidth > context.getResources().getDisplayMetrics().widthPixels || options.outHeight > context.getResources().getDisplayMetrics().heightPixels) {
				scale = options.outWidth / Utility.KeepRatioW;// 960px
				//scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
				scale = (scale % 2 != 0 ? (scale + 1) : scale);
				// height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
			} else {
				// height=context.getResources().getDisplayMetrics().heightPixels;
			}
			*/

            if (options.outWidth > Utility.KeepRatioW || options.outHeight > Utility.KeepRatioW) {
                scale = options.outWidth / Utility.KeepRatioW;// 960px
                //scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
                scale = (scale % 2 != 0 ? (scale + 1) : scale);
                // height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
            } else {
                // height=context.getResources().getDisplayMetrics().heightPixels;
            }

            // ((LayoutParams)context.getLayoutParams()).height=height;
            // ((LayoutParams)context.getLayoutParams()).width=getResources().getDisplayMetrics().widthPixels;
            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            context.setImageBitmap(BitmapFactory.decodeFile(path, options));
        } catch (Exception e) {
            context.setImageResource(defImage);
        }
    }

    public static void OpenImage(ImageView context, String path, int defImage, int ratio) {
        try {

            if (path.trim().equals("")) {
                context.setImageResource(defImage);
                return;
            } else if (!new File(path).exists()) {
                context.setImageResource(defImage);
                return;
            }
            int scale = 5;// int height=1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
			
			/*if (options.outWidth > context.getResources().getDisplayMetrics().widthPixels || options.outHeight > context.getResources().getDisplayMetrics().heightPixels) {
				scale = options.outWidth / Utility.KeepRatioW;// 960px
				//scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
				scale = (scale % 2 != 0 ? (scale + 1) : scale);
				// height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
			} else {
				// height=context.getResources().getDisplayMetrics().heightPixels;
			}*/


//			if (options.outWidth > ratio || options.outHeight > ratio) {
//				scale = options.outWidth / Utility.KeepRatioW;// 960px
//				//scale = scale + (((options.outWidth - Utility.KeepRatioW * scale) >= 1) ? 1 : 0);
//				scale = (scale % 2 != 0 ? (scale + 1) : scale);
//				// height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
//			} else {
//				// height=context.getResources().getDisplayMetrics().heightPixels;
//			}


            // Calculate inSampleSize
            int sda = calculateInSampleSize(options, ratio, ratio);
            options.inSampleSize = calculateInSampleSize(options, ratio, ratio);
            options.inJustDecodeBounds = false;

            options.inPreferredConfig = Config.ARGB_8888;
            options.inDither = true;

            // ((LayoutParams)context.getLayoutParams()).height=height;
            // ((LayoutParams)context.getLayoutParams()).width=getResources().getDisplayMetrics().widthPixels;
//			options = new BitmapFactory.Options();
//			options.inSampleSize = scale;
            Bitmap bimp = BitmapFactory.decodeFile(path, options);
            context.setImageBitmap(bimp);
        } catch (Exception e) {
            context.setImageResource(defImage);
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 5;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static void getSplash(Context context) {
        try {
            byte[] data = Utility.DownloadFromUrl(Utility.getURLenc("http://www.takalen.com/splash.php", ""));
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            if (bmp.getWidth() >= 10) {
                StringBuffer buf = new StringBuffer();
                for (int i = 0; i < data.length; i++) {
                    buf.append((char) data[i]);
                }
                Utility.setSetting(context, "SPLASH", buf.toString());
            }
        } catch (Exception e) {
        }
    }

    public static int getInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }

    public static long getLong(String s) {
        try {
            return Long.parseLong(s);
        } catch (Exception e) {
            return 0;
        }
    }

    public static void getSetup(Context context) {
        try {
            String s = Utility.getHttpConnection(Utility.getURLenc("http://www.neyama.com/cctv/putsetup.php?username=", Utility.getSetting(context, "DUSER", "")));
            if (s.length() >= 5) {
                String[] sbuff = s.split(";");
//				Log.i("getSetup", s);
                for (int i = 0; i < sbuff.length; i++) {
                    String buff = sbuff[i].trim();
                    if (buff.startsWith("maxsize=")) {
                        Utility.setSetting(context, "MAXSIZE", buff.substring(8));
                    } else if (buff.startsWith("interval=")) {
                        Utility.setSetting(context, "INTERVAL", buff.substring(9));
                    } else if (buff.startsWith("hidepause=")) {
                        Utility.setSetting(context, "HIDEPAUSE", buff.substring(10));
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public static View getInflater(Context context, int layout) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layout, null, false);
    }

    public static void showDialogSingleChoiceItems(Context context, String title, String[] data, int i, DialogInterface.OnClickListener listener) {
        Builder dlg = new AlertDialog.Builder(context);
        dlg.setSingleChoiceItems(data, i, listener);
        if (title != null) {
            if (title.trim().length() >= 1) {
                dlg.setTitle(title);
            }
        }
        dlg.create().show();
    }

    public static void createFolder(String folder) {
        buildResource(new String[]{"mkdir", folder});
    }

    private static void buildResource(String[] str) {
        try {
            Process ps = Runtime.getRuntime().exec(str);
            try {
                ps.waitFor();
//				Log.v("Directory", "Directory");
            } catch (InterruptedException e) {
//				Log.v("Directory", "Directory ." + e.getMessage());
            }
        } catch (IOException e) {
//			Log.v("Directory", "Directory .." + e.getMessage());
        }
    }

    public static void removeFolder(String folder) {
        try {
            Process ps = Runtime.getRuntime().exec("rm -rf " + folder);
            try {
                ps.waitFor();
//				Log.v("rDirectory", "Directory " + folder);
            } catch (InterruptedException e) {
//				Log.v("rDirectory", "Directory ." + e.getMessage());
            }
        } catch (Exception e) {
        }

    }

    public static void deleteFileAll(String folder) {
        deleteFolderAll(new File(folder));
    }

    public static void deleteFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }

    public static void deleteFolderAll(File dir) {
        try {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    file.delete();
                } else {
                    deleteFolderAll(file);
                    file.delete();
                }
            }
        } catch (Exception e) {
        }
        dir.delete();
    }

    /*
     * untuk menghapus semua file yg ada pada folder
     * tanpa folder root ikut terhapus
     */
    public static void deleteAllFileInFolder(String path) {
        File dir = new File(path);
        try {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    file.delete();
                } else {
                    deleteFolderAll(file);
                    file.delete();
                }
            }
        } catch (Exception e) {
        }
    }

    public static void copyFingerImageFile(String origin, String destination) {
        try {
            FileInputStream is = new FileInputStream(origin);
            OutputStream os = new FileOutputStream(destination);
            DataInputStream dis = new DataInputStream(is);
            int i = dis.readInt();
            byte[] none = new byte[i];
            dis.read(none, 0, i);

            os.write(none, 0, i);

            os.flush();
            os.close();
            is.close();
        } catch (Exception e) {
        }
    }

    public static void copyFile(String origin, String destination) {
        try {
            copyFile(new FileInputStream(origin), destination);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static InputStream convertFileToStream(File file) {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String readDirectJsonDAF(String dir) {
//		Connection.DBdelete("Log_File");
//		int ll = 0;
        BufferedReader br;
        String result = "", line;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(dir), "UTF-8"));

//    		Connection.DBinsert("Log_File", "status=alltable", "starttime="+Utility.getTime(), "endtime=");
//        	Connection.DBinsert("Log_File", "status=readline"+ll, "starttime="+Utility.getTime(), "endtime=");

            while ((line = br.readLine()) != null) {

                //check log waktu execute file sampai insert
//	        	if (ll > 0) {
//	        		Connection.DBinsert("Log_File", "status=readline"+ll, "starttime="+Utility.getTime(), "endtime=");
//				}else {
//					Connection.DBupdate("Log_File", "endtime="+Utility.getTime(), "where", "status=readline"+(ll-1));
//				}
//	        	ll++;

                Nset order = Nset.newObject();
                order = Nset.readJSON(line);
                if (order.getArraySize() > 0) {

                    for (int j = 0; j < order.getArraySize(); j++) {

                        //cek urutan terkahir yang menandakan insert data berhasil dan update versioning
                        String status = order.getData(j).getData("status").toString();

                        if (status.equalsIgnoreCase("")) {
                            String table = order.getData(j).getData("table").toString();
                            String sdsd = "";
                            if (table.equalsIgnoreCase("MST_EDUCATION")) {
                                sdsd = table;
                            }
//							Connection.DBinsert("Log_File", "status="+table, "starttime="+Utility.getTime(), "endtime=");

                            for (int k = 0; k < order.getData(j).getData("data").getArraySize(); k++) {
                                String code = order.getData(j).getData("data").getData(k).getData("code").toString();
                                String drst = order.getData(j).getData("data").getData(k).getData("data").toString();

                                Nset nn = Nset.readJSON(drst);
                                String status2 = nn.getData("status").toString();
                                if (status2.equalsIgnoreCase("OK")) {
                                    Recordset xxx = Stream.downStream(drst);
                                    if (xxx.getRows() > 0) {
                                        if (code.equals("truncate")) {
                                            Syncronizer.Truncate(table, xxx);
                                        } else if (code.equals("insert")) {
//											Syncronizer.deleteByP_ID(table, xxx);
                                            Syncronizer.insertToDBSpecialDAF(table, xxx);
                                        } else if (code.equals("delete")) {
                                            Vector<Vector<String>> vQry = xxx.getAllDataVector();
                                            Vector<String> vQry2 = vQry.get(0);
                                            for (int l = 0; l < vQry2.size(); l++) {
                                                Connection.DBquery(vQry2.elementAt(l).toString());
                                            }

                                        } else if (code.equals("update")) {
                                            Vector<Vector<String>> vQry = xxx.getAllDataVector();
                                            Vector<String> vQry2 = vQry.get(0);
                                            for (int l = 0; l < vQry2.size(); l++) {
                                                Connection.DBquery(vQry2.elementAt(l).toString());
                                            }

                                        }
                                    }
                                } else {
                                    //log tabel yang gagal;
                                }

                            }
//							Connection.DBupdate("Log_File", "endtime="+Utility.getTime(), "where", "status="+table);
                        } else {


                            String cabang = order.getData(j).getData("BRANCH").toString();
                            String generator = order.getData(j).getData("GENERATOR").toString();
                            String global = order.getData(j).getData("GLOBAL").toString();

                            if (status.equalsIgnoreCase("DONE")) {

                                Connection.DBquery("update TRN_MOBILE_DATA_SYNC_INFO set last_version_no='" + global + "' where office_code='00000' and data_sync_id='GLOBAL'");
                                Connection.DBquery("update TRN_MOBILE_DATA_SYNC_INFO set last_version_no='" + cabang + "' where office_code='" + Utility.OFFICE_CODE + "' and data_sync_id='BRANCH'");
                                Connection.DBquery("update TRN_MOBILE_DATA_SYNC_INFO set last_version_no='" + generator + "' where office_code='00000' and data_sync_id='GENERATOR'");
                                result = "ok";

                                Utility.deleteFile(dir);
                                Utility.deleteFile(dir.substring(0, dir.indexOf(".")) + ".rar");

//								Utility.deleteFile("json.rar");
                            } else {
                                result = "failed";
                            }

                        }


                    }
                }
//        		Connection.DBupdate("Log_File", "endtime="+Utility.getTime(), "where", "status=alltable");
//        		Connection.DBinsert("Log_File", "status=readline"+ll, "starttime="+Utility.getTime(), "endtime=");
            }
        } catch (IOException ex) {
            //do something
            ex.printStackTrace();
        }

        //jika download sukses maka hapus file
//	    deleteFile(dir);
//	    deleteFile(Utility.getDefaultPath("fjson.gz"));

        return result;
    }

    public static String read(String dir) {
        BufferedReader br;
        String result = "", line;
        StringBuilder sb = new StringBuilder();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(dir), "UTF-8"));
            while ((line = br.readLine()) != null) {
                Nset order = Nset.newObject();
                order = Nset.readJSON(line);
                if (order.getData("data").toString() != null) {
                    Nset n = Nset.newObject();
                    n = Nset.readJSON(line);
                    String str = n.getData("data").toString();
                    Recordset xxx = Stream.downStream(str);
                    if (xxx.getRows() > 0) {

                    }

                }
                sb.append(line).append("\n");
            }
        } catch (IOException ex) {
            //do something
            ex.printStackTrace();
        }
        return result;
    }

    public static void write(String dir, String text) {
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dir), "UTF-8"));
            bw.write("");
            for (int i = 0; i < text.length(); i++) {
                if (text.charAt(i) != '\n') {
                    bw.append(text.charAt(i));
                } else {
                    bw.newLine();
                }
            }
            bw.flush();
        } catch (IOException ex) {
            //do something
        }
    }

    public static String readFile(InputStream is) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {

            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                byteArrayOutputStream.write(buffer, 0, length);
            }
            is.close();
        } catch (Exception e) {
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    public static String readFile(String path) {
        BufferedReader br;
        String sCurrentLine = "";
        StringBuffer sb = new StringBuffer();
        try {

            br = new BufferedReader(new FileReader(new File(path)));

            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine);
            }

        } catch (Exception e) {
        }
        return sb.toString();
    }

    public static String readLargeFile(String path) {
        StringBuffer sb = new StringBuffer();
        try {
            LineIterator li = FileUtils.lineIterator(new File(path), "UTF-8");//FileUtils.lineIterator(new FileReader(new File(path), "UTF-8"));//new LineIterator(new FileReader(new File(path)));
            try {
                while (li.hasNext()) {
                    String line = li.nextLine();
                    sb.append(line);
                    // do something with line
                }
            } finally {
                LineIterator.closeQuietly(li);
            }

        } catch (Exception e) {
        }
        return sb.toString();
    }

    public static void copyFile(InputStream is, String destination) {
        try {
            OutputStream os = new FileOutputStream(destination);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
            os.close();
            is.close();
        } catch (Exception e) {
        }
    }

    public static int getNumber(String s) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            if ("01234567890".indexOf(s.charAt(i)) != -1) {
                buf.append(s.charAt(i));
            }
        }
        try {
            return Integer.parseInt(buf.toString());
        } catch (Exception e) {
        }
        return 0;
    }

    public static String insertString(String original, String sInsert, int igroup) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < original.length(); i++) {
            if ((i % igroup) == 0 && igroup != 0 && i != 0) {
                sb.append(sInsert + original.substring(i, i + 1));
            } else {
                sb.append(original.substring(i, i + 1));
            }
        }
        return sb.toString();
    }

    public static String Now() {
        Calendar calendar = Calendar.getInstance();

        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
    }

    public static String formatCurrency(String original) {
        return insertStringMax(insertStringRev(original, ".", 3), " ", 11);
    }

    public static String insertStringMax(String original, String sInsert, int max) {
        StringBuffer sb = new StringBuffer();
        for (int i = original.length(); i < max; i++) {
            sb.append(sInsert);
        }
        sb.append(original);
        return sb.toString();
    }

    public static String insertStringRev(String original, String sInsert, int igroup) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < original.length(); i++) {

            if (((original.length() - i) % igroup) == 0 && igroup != 0 && i != 0) {
                sb.append(sInsert + original.substring(i, i + 1));
            } else {
                sb.append(original.substring(i, i + 1));
            }
        }
        return sb.toString();
    }

    public static String notNull(String s) {
        if (s != null) {
            return s;
        }
        return "";
    }

    public static boolean isExit;

    public static void setExit(boolean isExit) {
        Utility.isExit = isExit;
    }

    public static boolean isExit() {
        return isExit;
    }

    public static long converDateToLong(String date) {

        SimpleDateFormat df = null;

        if (date.length() == 10) {
            if (date.contains("-")) {
                df = new SimpleDateFormat("dd-MM-yyyy");
            } else {
                df = new SimpleDateFormat("dd/MM/yyyy");
            }
        } else if (date.length() == 16) {
            if (date.contains("-")) {
                df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            } else {
                df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            }
        } else if (date.length() == 19) {
            if (date.contains("-")) {
                df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            } else {
                df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            }
        } else {
            if (date.contains("-")) {
                df = new SimpleDateFormat("dd-MM-yyyy");
            } else {
                df = new SimpleDateFormat("dd/MM/yyyy");
            }
        }

        try {
            Date dt = df.parse(date);
            long hasil = dt.getTime();
            return hasil;
        } catch (Exception e) {
        }

        return 0;
    }

    @SuppressLint("DefaultLocale")
    public static boolean containsChar(String kalimat, char character) {
        boolean isContain = false;
        String charLower = kalimat.toLowerCase();
        char[] charArray = charLower.toCharArray();
        for (int i = 0; i < charLower.length(); i++) {
            if (charArray[i] == character) {
                isContain = true;
                return isContain;
            }
        }
        return isContain;
    }

    // untuk mendapatkan font dari asset
    public static Typeface getFonttype(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "BentonSansComp-Bold.ttf");
    }

    // untuk mendapatkan path dari images
    public static String getPathTheme(String nameFile) {
        return getDefaultPath() + "themes/" + nameFile;
    }

    public static Bitmap LoadImage(String url) {
        Bitmap bitmap = null;
        try {

            // mendapatkan nama file gambar
            String nameFile = url;
            if (nameFile.contains("/")) {
                for (int i = 0; i < nameFile.length(); i++) {
                    if (nameFile.contains("/")) {
                        nameFile = nameFile.substring(nameFile.indexOf("/") + 1);
                    }
                }

                if (nameFile.contains(".")) {
                    nameFile = nameFile.substring(0, nameFile.indexOf(".") + 1);
                }
            }

            String path = Utility.getDefaultPath() + "themes/" + nameFile;

            if (isContainImages(path)) {
                bitmap = BitmapFactory.decodeFile(path);
            } else {
                URL urlx = new URL(url);
                HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();
                InputStream is = httpConn.getInputStream();
                bitmap = BitmapFactory.decodeStream(is);

                // save bitmap to file
                SaveBitmap(bitmap, nameFile);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    // untuk menyimpan bitmap kedalam file di sdcard
    public static void SaveBitmap(Bitmap bitmap, String nameFile) {
        String path = Utility.getDefaultSign();
        OutputStream os = null;
        File f = new File(path);

        if (!f.exists()) {
            f.mkdirs();
        }

        File file = new File(path, nameFile);
        if (!file.exists()) {
            try {
                os = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static void SaveBitmap2(Bitmap bitmap, String nameFile) {
        String path = Utility.getDefaultPath();
        OutputStream os = null;
        File f = new File(path);

        if (!f.exists()) {
            f.mkdirs();
        }

        File file = new File(path, nameFile);
        if (!file.exists()) {
            try {
                os = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isContainImages(String path) {
        boolean isThere = false;
        File file = new File(path);
        if (file.exists()) {
            isThere = true;
        } else {
            isThere = false;
        }
        return isThere;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static void i(String tag, String msg) {
//		Log.i(tag, msg);
    }

    /**
     * checking contex application if null application close by system
     *
     * @param activity
     */
    public static void checkingAppContex(Activity activity) {

        if (Utility.getAppContext() == null) {
            Utility.setExit(true);
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
        }
    }

    public static boolean isHaveExternalStorage() {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }

        return false;
    }

    public static void changeColorRowTable(int position, LinearLayout layout) {
        try {
            if (position != 0) {
                int sum = position % 2;
                if (sum != 0) {
                    layout.setBackgroundColor(Color.parseColor("#c9c9ca"));
                } else {
                    layout.setBackgroundColor(Color.parseColor("#ebeaea"));
                }
            } else {
                layout.setBackgroundColor(Color.parseColor("#ebeaea"));
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setImageTouch(final ImageView img) {
        img.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    img.setBackgroundResource(R.drawable.select);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    img.setBackgroundResource(0);
                }
                return false;
            }
        });
    }

    public static void imagePost(String URL, String fileName) {

    }

    public static boolean isDuplicateImage(String Url) {
        boolean isDuplicate = false;
        Recordset server = Syncronizer.getHttpStream(Url);
        Vector<String> record = server.getRecordFromHeader("");
        for (int i = 0; i < record.size(); i++) {

        }
        return isDuplicate;
    }

    // untuk mengecek apakah record disuatu tabel sudah ada
    public static boolean isAvailableRecordTable(String table, String field, String record) {
        boolean isThere = false;
        try {
            Cursor c = Connection.DBexecute("select " + field + " from " + table + " where " + field + " = '" + record + "'");
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    isThere = true;
                } while (c.moveToNext());
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isThere;
    }

    public static Vector<LinkedHashMap<String, String>> getRecordWithSeparate(Vector<String> data, String separate) {
        Vector<LinkedHashMap<String, String>> v = new Vector<LinkedHashMap<String, String>>();
        for (int i = 0; i < data.size(); i++) {
            if (data.elementAt(i).contains(separate)) {
                LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
                map.put(data.elementAt(i).substring(0, data.elementAt(i).indexOf(separate)), data.elementAt(i).substring(data.elementAt(i).indexOf(separate) + 1));
                v.add(map);
            }
        }
        return v;
    }

    public static void LoadImageDirect(ImageView img, String url) {
        Bitmap bitmap = null;
        try {
            // mendapatkan nama file gambar
            if (url.contains("http")) {
                String nameFile = url;
                if (nameFile.contains("/")) {
                    for (int i = 0; i < nameFile.length(); i++) {
                        if (nameFile.contains("/")) {
                            nameFile = nameFile.substring(nameFile.indexOf("/") + 1);
                        }
                    }

                    if (nameFile.contains(".")) {
                        nameFile = nameFile.substring(0, nameFile.indexOf(".") + 1);
                    }
                }

                String path = Utility.getDefaultPath() + "themes/" + nameFile;

                if (isContainImages(path)) {
                    bitmap = BitmapFactory.decodeFile(path);
                } else {
                    URL urlx = new URL(url);
                    HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();
                    InputStream is = httpConn.getInputStream();
                    bitmap = BitmapFactory.decodeStream(is);

                    // save bitmap to file
                    SaveBitmap(bitmap, nameFile);
                }

                img.setImageBitmap(bitmap);
            } else {
                if (isContainImages(url))
                    img.setImageBitmap(BitmapFactory.decodeFile(url));
                else
                    img.setImageResource(0);

            }

        } catch (MalformedURLException e) {
            img.setImageResource(0);
        } catch (IOException e) {
            img.setImageResource(0);
        }
    }

    public static int pxToDp(Context context, int px) {
        return px / context.getResources().getDisplayMetrics().densityDpi;
    }

    public static int dpToPx(Context context, int dp) {
        return dp * context.getResources().getDisplayMetrics().densityDpi;
    }

    public static void saveFile(String path, String url) {
        try {
            URL urlx = new URL(url);
            HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();
            InputStream is = httpConn.getInputStream();

            FileOutputStream fileOutputStream = new FileOutputStream(path);
            byte[] buffer = new byte[16 * 1024];
            int bufferLength = -1;
            while ((bufferLength = is.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveImage(String imgPath, String url) {
        Bitmap bitmap = null;
        try {

            String path = Utility.getDefaultSign(imgPath);

            if (isContainImages(path)) {
                bitmap = BitmapFactory.decodeFile(path);
            } else {
                URL urlx = new URL(url);
                HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();
                InputStream is = httpConn.getInputStream();
                bitmap = BitmapFactory.decodeStream(is);

                // save bitmap to file
                SaveBitmap(bitmap, imgPath);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void downloadDB(String url, File directory) {
        try {

            URL urlx = new URL(url);
            HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();

            httpConn.getHeaderFields();
            String contentencode = httpConn.getContentEncoding() != null ? httpConn.getContentEncoding().toLowerCase() : "";
            if (httpConn.getResponseCode() != 304) {

                InputStream is = httpConn.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);


                byte[] buffer = new byte[1024];
                int bufferLength = 0;

                if (contentencode.contains("gzip")) {
                    GZIPInputStream gzipInputStream = new GZIPInputStream(is);

                    while ((bufferLength = gzipInputStream.read(buffer)) > 0) {
                        fileOutputStream.write(buffer, 0, bufferLength);
                    }
                } else {
                    while ((bufferLength = is.read(buffer)) > 0) {
                        fileOutputStream.write(buffer, 0, bufferLength);
                    }
                }


                fileOutputStream.close();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            String sd = e.getMessage();
            e.printStackTrace();
        }
    }

    public static void downloadDBGzip(String url, File directory) {
        try {

            URL urlx = new URL(url);
            HttpURLConnection httpConn = (HttpURLConnection) urlx.openConnection();
            InputStream is = httpConn.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = httpConn.getContentLength();

            byte[] buffer = new byte[1024 * 1024];
            int bufferLength = 0;
            while ((bufferLength = is.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void decompressGzipFile(String gzipFile, String newFile) {
        try {
        	/*ZipInputStream zis = new ZipInputStream(new FileInputStream(gzipFile));
        	byte[] buffer = new byte[1024];
        	ZipEntry ze = zis.getNextEntry();
    		
        	while(ze!=null){
        			
        	   String fileName = ze.getName();
               File newFile = new File(Utility.getDefaultPath() + File.separator + fileName);
//        	   File newFile = new File(newFiles);
                    
               System.out.println("file unzip : "+ newFile.getAbsoluteFile());
                    
                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();
                  
                FileOutputStream fos = new FileOutputStream(newFile);             

                int len;
                while ((len = zis.read(buffer)) > 0) {
                	fos.write(buffer, 0, len);
                }
            		
                fos.close();   
                ze = zis.getNextEntry();
        	}
        	
            zis.closeEntry();
        	zis.close();*/

//            FileInputStream fis = new FileInputStream(gzipFile);
            GZIPInputStream gis = new GZIPInputStream(new FileInputStream(gzipFile));
            FileOutputStream fos = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            //close resources
            fos.close();
            gis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static List<String[]> CSVRead(String filename, Context context) {
        // Get Buffered Reader
        List<String[]> lineList = new ArrayList<String[]>();

        // Get Lines
        String strLine;

        try {
            AssetManager assetMgr = context.getAssets();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assetMgr.open(filename)));

            // Init lineList

            while ((strLine = bufferedReader.readLine()) != null) {
                String[] strings = strLine.split("\\|");
                lineList.add(strings);
            }
            // Close the input stream
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return lineList;
    }

    // create by jadmiko
    // date: 12-01-2018
    public static String getPrintCounter(int num) {
        String[] nomina = {"", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam",
                "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"};

        if (num < 12) {
            return num + "( " + nomina[(int) num] + " )";
        }
        if (num >= 12 && num <= 19) {
            return num + "( " + nomina[(int) num % 10] + " belas " + ")";
        }
        if (num >= 20 && num <= 99) {
            return num + "( " + nomina[(int) num / 10] + " puluh " + nomina[(int) num % 10] + " )";
        }
        if (num >= 100 && num <= 199) {
            return num + "( " + "seratus " + getPrintCounter(num % 100) + " )";
        }
        if (num >= 200 && num <= 999) {
            return num + "( " + nomina[(int) num / 100] + " ratus " + getPrintCounter(num % 100) + " )";
        }
        if (num >= 1000 && num <= 1999) {
            return num + "( " + "seribu " + getPrintCounter(num % 1000) + " )";
        }
        if (num >= 2000 && num <= 999999) {
            return num + "( " + getPrintCounter((int) num / 1000) + " ribu " + getPrintCounter(num % 1000) + " )";
        }
        if (num >= 1000000 && num <= 999999999) {
            return num + "( " + getPrintCounter((int) num / 1000000) + " juta " + getPrintCounter(num % 1000000) + " )";
        }
        return "";
    }

    public static String postMultipart2(String url, String[] keys, String[] data) {
        url = nikitaUrl(url);

//		Log.i("URL", url);
        String strResponse = null;
        try {

            HttpClient client = new DefaultHttpClient();

            //13kcrazy
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

            HttpParams params = new BasicHttpParams();
            params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setConnectionTimeout(params, 30000);//asli=30000
            HttpConnectionParams.setSoTimeout(params, 300000);//asli=300000
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
            client = new DefaultHttpClient(cm, params);
            //13kcrazy

            HttpPost post = new HttpPost(url);

            MultipartEntity reqEntity = new MultipartEntity(

                    HttpMultipartMode.BROWSER_COMPATIBLE);

            for (int i = 0; i < keys.length; i++) {
                reqEntity.addPart(keys[i], new StringBody(data[i]));
            }

            //baris mulai 17:04 13/11/2018
            if (Global.getText("action.corrupt").equalsIgnoreCase("y")) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                reqEntity.writeTo(out);
                String stringEntity = out.toString();

                boolean dataRusak = false;
                for (int i = 0; i < keys.length; i++) {
//					String abc = data[i];
                    if (isAscii(data[i]) == false) {
                        dataRusak = true;
                        break;
                    }
                }

                if (stringEntity != "") {
                    if (isAscii(stringEntity) == true && dataRusak == false) {
                        post.setEntity(reqEntity);
                        HttpResponse response = client.execute(post);
                        HttpEntity resEntity = response.getEntity();
                        strResponse = EntityUtils.toString(resEntity);
                        out.close();
                    } else {
                        strResponse = "data_stream_rusak";
                        out.close();
                    }
                } else {
                    post.setEntity(reqEntity);
                    HttpResponse response = client.execute(post);
                    HttpEntity resEntity = response.getEntity();
                    strResponse = EntityUtils.toString(resEntity);
                    out.close();
                }
            } else {
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                HttpEntity resEntity = response.getEntity();
                strResponse = EntityUtils.toString(resEntity);
            }
            //baris selesai 17:04 13/11/2018
            String ege = strResponse;

            if (strResponse != null) {
//				Log.i("RESPONSE", strResponse);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

//			Log.e("Debug", "error: " + ex.getMessage(), ex);

        }
        if (strResponse == null) {
            return "Connection Timeout";
        } else {
            return strResponse;
        }

        // return strResponse!=null ? strResponse: "Connection Timeout";
    }

    private static boolean isAscii(String input) {
        return Charset.forName("US-ASCII").newEncoder().canEncode(input);
    }

    public static void encrypt(InputStream fis, OutputStream fos, String password) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        // Here you read the cleartext.
//        FileInputStream fis = new FileInputStream("data/cleartext");

        // This stream write the encrypted text. This stream will be wrapped by another stream.
//        FileOutputStream fos = new FileOutputStream("data/encrypted");

        // Length is 16 byte
        // Careful when taking user input!!! https://stackoverflow.com/a/3452620/1188357
        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
//        SecretKeySpec sks = new SecretKeySpec(password.getBytes(), "AES");
        // Create cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[8];
        while ((b = fis.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        fis.close();
    }

    public static InputStream decrypt(FileInputStream encryptedData, InputStream decryptedData, String password) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
//        FileInputStream fis = new FileInputStream("data/encrypted");

//        FileOutputStream fos = new FileOutputStream("data/decrypted");

//        InputStream decrypted = decryptedData;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(encryptedData, cipher);
        int b;
        byte[] d = new byte[8];
        while ((b = cis.read(d)) != -1) {
//            fos.write(d, 0, b);
            baos.write(d, 0, b);
        }

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
//        decrypted = bais;
//        fos.flush();
//        fos.close();
        baos.flush();
        baos.close();
        cis.close();
//        bais.close();
        return bais;

    }

    public static String postMultipart3(String url, String[] keys, String[] data) {
        url = nikitaUrl(url);

//		Log.i("URL", url);
        String strResponse = null;
        try {

            HttpClient client = new DefaultHttpClient();

            //13kcrazy
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

            HttpParams params = new BasicHttpParams();
            params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


            params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setConnectionTimeout(params, 0);
            HttpConnectionParams.setSoTimeout(params, 0);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
            client = new DefaultHttpClient(cm, params);
            //13kcrazy

            HttpPost post = new HttpPost(url);

            MultipartEntity reqEntity = new MultipartEntity(

                    HttpMultipartMode.BROWSER_COMPATIBLE);

            for (int i = 0; i < keys.length; i++) {
                reqEntity.addPart(keys[i], new StringBody(data[i]));
            }
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			reqEntity.writeTo(out);
//			String cd = out.toString();
            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            strResponse = EntityUtils.toString(resEntity);
            String ege = strResponse;

            if (strResponse != null) {
//				Log.i("RESPONSE", strResponse);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

//			Log.e("Debug", "error: " + ex.getMessage(), ex);

        }
        return strResponse != null ? strResponse : "Connection Timeout";
    }

    public static String postMultiPartForGetToken(String url, String[] keys, String[] data) {
//		url = nikitaUrl(url);

//		Log.i("URL", url);
        String strResponse = null;
        try {

            HttpClient client = new DefaultHttpClient();

            //13kcrazy
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

            HttpParams params = new BasicHttpParams();
            params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


            params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
            client = new DefaultHttpClient(cm, params);
            //13kcrazy

            HttpPost post = new HttpPost(url);

//			MultipartEntity reqEntity = new MultipartEntity(
//
//					HttpMultipartMode.BROWSER_COMPATIBLE);
//
//			for (int i = 0; i < keys.length; i++) {
//				reqEntity.addPart(keys[i], new StringBody(data[i]));
//			}


//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			reqEntity.writeTo(out);
//			String cd = out.toString();

//          post.setEntity(reqEntity);

            /*StringBuilder json = new StringBuilder();
            json.append("{");
            json.append("\"client_secret\":\"261f1b80-7a18-438e-b9fa-2f9575c97e0b\",");
            json.append("\"client_id\":\"fifgroup-token\"");
            json.append("\"grant_type\":\"password\"");
            json.append("\"password\":\"repo123\"");
            json.append("\"username\":\"repo\"");
            json.append("}");

            post.setEntity(new StringEntity(json.toString()));*/

            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            for (int i = 0; i < keys.length; i++) {
                nvps.add(new BasicNameValuePair(keys[i], data[i]));
            }
/*			nvps.add(new BasicNameValuePair("client_secret", "261f1b80-7a18-438e-b9fa-2f9575c97e0b"));
			nvps.add(new BasicNameValuePair("client_id", "fifgroup-token"));
			nvps.add(new BasicNameValuePair("grant_type", "password"));
			nvps.add(new BasicNameValuePair("password", "repo123"));
			nvps.add(new BasicNameValuePair("username", "repo"));*/

            post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            strResponse = EntityUtils.toString(resEntity);
            String ege = strResponse;

            if (strResponse != null) {
//				Log.i("RESPONSE", strResponse);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

//			Log.e("Debug", "error: " + ex.getMessage(), ex);

        }
        return strResponse != null ? strResponse : "Connection Timeout";
    }

    public static void copyAsset(Context context, String file) {
        String extDir = context.getFilesDir() + "/";
        try {
            InputStream is = context.getAssets().open(file);
            FileOutputStream os = new FileOutputStream(extDir + file);
            CopyStream(is, os);
            is.close();
            os.flush();
            os.close();
        } catch (IOException e) {
            try {
                String[] list = context.getAssets().list(file);
                File dir = new File(extDir + file);
                dir.mkdir();
                for (String listFile : list) {
                    copyAsset(context, file + "/" + listFile);
                }
            } catch (IOException e1) {
                e1.printStackTrace();
//                Log.e(MainActivity.TAG, "Failed to copy resoruces to SD card : " + e.getMessage());
            }
        }
    }


}
