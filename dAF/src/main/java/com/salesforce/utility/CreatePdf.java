package com.salesforce.utility;

import android.content.Context;
import android.os.Environment;

import org.oss.pdfreporter.PdfReporter;
import org.oss.pdfreporter.repo.RepositoryManager;

import java.util.HashMap;
import java.util.List;

public class CreatePdf {
    Context ctx;

    public CreatePdf(Context ctx) {
        this.ctx = ctx;
    }

    public String create(HashMap data, String file,String outputname){
        PdfReporter pdfReporter = getExporter2(file, getFilenameFromJrxml(file),"tmp",outputname);

        pdfReporter.setFillParameters(data);
        try {
            String name = pdfReporter.exportPdf();
            Utility.deleteFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/view.pdf");
            Utility.copyFile(name, Environment.getExternalStorageDirectory().getAbsolutePath() + "/view.pdf");
            Utility.deleteFile(name);
            return outputname;
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
//        return "";
    }

//    private PdfReporter getExporter(String jrxmlPath, String reportFolder, String extraFolder) {
//        final String rootFolder = getRootFolder();
//        final String resourceFolder = getResourcesFolder();
//
//        RepositoryManager repo = PdfReporter.getRepositoryManager();
//        repo.reset();
//        repo.setDefaultResourceFolder(resourceFolder);
//        repo.setDefaulReportFolder(rootFolder + RepositoryManager.PATH_DELIMITER + "jrxml" + RepositoryManager.PATH_DELIMITER + reportFolder);
//
//        if (null != extraFolder) {
//            repo.addExtraReportFolder(getRootFiels() + RepositoryManager.PATH_DELIMITER + extraFolder);
//        }
//        repo.addExtraReportFolder(resourceFolder);
//        PdfReporter reporter = new PdfReporter(jrxmlPath, getOuputPdfFolder(), getFilenameFromJrxml(jrxmlPath));
//        return  reporter;
//    }

    private PdfReporter getExporter2(String jrxmlPath, String reportFolder, String extraFolder, String outputPdfName) {
        final String rootFolder = getRootFolder();
        final String resourceFolder = getResourcesFolder();

        RepositoryManager repo = PdfReporter.getRepositoryManager();
        repo.reset();
        repo.setDefaultResourceFolder(resourceFolder);
        repo.setDefaulReportFolder(rootFolder + RepositoryManager.PATH_DELIMITER + "jrxml");
//        repo.setDefaulReportFolder(getRootFiels() + RepositoryManager.PATH_DELIMITER + "jrxml");

        if (null != extraFolder) {
            String a = getRootFiels() + RepositoryManager.PATH_DELIMITER + extraFolder;
            repo.addExtraReportFolder(getRootFiels() + RepositoryManager.PATH_DELIMITER + extraFolder);
        }

        repo.addExtraReportFolder(resourceFolder);
        PdfReporter reporter = new PdfReporter(jrxmlPath, getOuputPdfFolder(), outputPdfName);

        return  reporter;
    }

    private String getRootFolder() {
        return ctx.getFilesDir() + "/reports";
    }

    private String getRootFiels() {
        return ctx.getExternalFilesDir(null).toString();
    }

    private String getResourcesFolder() {
        return getRootFolder() + "/resources";
    }

    private String getOuputPdfFolder() {
        return ctx.getExternalFilesDir(null).toString();
    }

    private String getFilenameFromJrxml(String jrxml) {
        return jrxml.replace(".jrxml", "");
    }

}
