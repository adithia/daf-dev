package com.salesforce.utility;

import java.io.File;
import java.util.ArrayList;

import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import com.salesforce.component.Component;
import com.salesforce.database.Connection;
import com.salesforce.database.Recordset;
import com.salesforce.generator.Generator;
import com.salesforce.generator.ui.SignatureUi;

public class VerificationPhoto {
	ArrayList<ModelVerificationPhoto> dataResult;
	String sComponents;
	Vector<Component> cekKomponen = new Vector<Component>();
	Vector<Component> cekKeterangan = new Vector<Component>();
	Component tmp = null;
	String resultDataStream;
	//di perlukan untuk menggecekan file size
	File imgTemp2 = null;
	public VerificationPhoto(String sComponent) {
//		this.sComponents = sComponent;
		if(sComponent.contains("#")){
			Vector<String> a = Utility.splitVector(sComponent, "#");		
			this.cekKomponen = getComponent(Utility.splitVector(a.get(0), "|"));
			this.cekKeterangan = getComponent(Utility.splitVector(a.get(1), "|"));
			this.cekKomponen = deleteComponent();
			this.sComponents = getComponentlabel();
		}else{
			this.sComponents = sComponent;
			this.cekKomponen = getComponent(Utility.splitVector(sComponent, "|"));
		}
				
	}
	
	private Vector<Component> deleteComponent(){
		Vector<Component> tampung = new Vector<Component>();
		for(int i=0; i<cekKeterangan.size();i++){
			Component tmp = cekKeterangan.get(i);
			if(tmp.getText().length()<=1){
				tampung.add(cekKomponen.get(i)); 
			}
		}
		return tampung;
	}
	private String getComponentlabel(){
		String a = "";
		for(Component s:cekKomponen){
//			a = a +"$"+s.getName()+"|";  
			if(a==""){
				a ="$"+s.getName();
			}else{
				a=a+"|$"+s.getName();
			}
		}
		return a;
	}
	
	public ArrayList<ModelVerificationPhoto> getResult() {
		if (dataResult == null) {
			if(!cekKomponen.isEmpty()){
				prosesCheck(this.sComponents);
			}			
		}
		return this.dataResult;
	}

	private Vector<Component> getComponent(Vector<String> namaComponent) {
		Vector<Component> result = new Vector<Component>();
		
		for (int i = 0; i < namaComponent.size(); i++) {
			for (int j = 0; j < Generator.forms.size(); j++) {
				if (Generator.forms
						.get(j)
						.getName()
						.equalsIgnoreCase(
								getFormOrComponentName(namaComponent.get(i), 1))) {
					for (int k = 0; k < Generator.forms.get(j)
							.getAllComponent().size(); k++) {
						if (Generator.forms
								.get(j)
								.getComponent(k)
								.getName()
								.equalsIgnoreCase(
										getFormOrComponentName(
												namaComponent.get(i), 2))) {
							result.add(Generator.forms.get(j).getComponent(k));
						}
					}
				}
			}
		}

		return result;
	}
	

	/*
	 * pilih 1 untuk mendapatkan kode form
	 * pilih 2 untuk mendapatkan kode komponent
	 */
	private String getFormOrComponentName(String namaComponent, int pilih) {
		Vector result = Utility.splitVector(namaComponent, "F");
		if (pilih == 1) {
			return "F" + result.get(1);
		} else if (pilih == 2) {
			return namaComponent.substring(1);
		} else {
			return "";
		}

	}

	private void prosesCheck(String sComponent) {
		Vector picture = Utility.splitVector(sComponent, "|");
		dataResult = new ArrayList<ModelVerificationPhoto>();
		for (int i = 0; i < picture.size(); i++) {
			dataResult.add(checkData(picture.get(i).toString()));
		}
	}
	/*
	 * lokasi 1 = tmp folder
	 * lokasi 2 = images folder
	 */
	private boolean checkImage(String tile, String head, int lokasi) {
		File imgTemp = null;
		Boolean result = false;
		String folder = "";		
		if (lokasi == 1) {
			folder = Utility.getDefaultTempPath();
		} else if (lokasi == 2) {
			folder = Utility.getDefaultImagePath();
		} else {
			folder = Utility.getDefaultTempPath();
		}
		boolean tes_hasil;
		if (head.contains("jpg")) {
			imgTemp = new File(folder + "/" + tile + "_0_" + head);
//			tes_hasil = getListFile2(imgTemp.toString(),".jpg");
			if (imgTemp.exists()) {
				result = true;
				imgTemp2 = imgTemp;
			} else {
				imgTemp = new File(folder + "/" + tile + "_1_" + head);
//				tes_hasil = getListFile2(imgTemp.toString(),".jpg");
				if (imgTemp.exists()) {
					result = true;
					imgTemp2 = imgTemp;
				} else {
					result = false;
				}
			}
		} else if (head.contains("png")) {
			imgTemp = new File(folder + "/" + tile + "_3_" + head);
//			tes_hasil = getListFile2(imgTemp.toString(),".png");
			if (imgTemp.exists()) {
				result = true;
				imgTemp2 = imgTemp;
			} else {
					result = false;
//				}
			}
		}else if(head.contains("FP")){
			imgTemp = new File(folder + "/" + tile + "_4_FP");
			// result = false;
			//check file biometrick
//			tes_hasil = getListFile2(imgTemp.toString(),".png");
			if (imgTemp.exists()) {
				//check file img
				if(getListFile(imgTemp.toString())==false){
					imgTemp = new File(folder+"/finger-nikita.imgicon.png");
					if(imgTemp.exists()){
						result = true;
						imgTemp2 = imgTemp;
					}else{
						result = false;
					}
				}else{
					result = true;
					imgTemp2 = imgTemp;
				}
			} else {
				result = false;
			}
		}
		return result;
	}

	private boolean checkDetailFingerPrint(File imgTemp) {
		Boolean result = false;		
		Recordset vP = Connection
				.DBquery("select path from TBL_PATH_Finger where orderCode ='"
						+ Generator.currOrderCode + "'");
		Vector finger = vP.getAllDataVector();
		String a = null;
		int hasil = 0;
		for (int i = 0; i < finger.size(); i++) {
			a = finger.get(i).toString();
			a = a.substring(a.lastIndexOf("_"), a.length() - 1);
			File check = new File(imgTemp.toString() + a);
			if (check.exists()) {
				hasil = hasil + 1;
			} else {
				hasil = hasil + 0;
			}
		}
		if (hasil == finger.size()) {
			result = true;
		}
		return result;
	}
	
	private Boolean getListFile(String path){		
		Boolean result = false;
		File parentDir = new File(path.substring(0,path.lastIndexOf("/")));
		ArrayList<File> listFile = new ArrayList<File>();		
		File[] list = parentDir.listFiles();
		for(File f :list){
			if(f.getName().toLowerCase().endsWith(".png")){
				listFile.add(f);				
				if(f.toString().contains(path)){
					result = true;
					break;
				}else{
					result = false;
				}			
			}
		}
		return result;
	}

	private Boolean getListFile2(String path,String ekstensi){		
		Boolean result = false;
		File parentDir = new File(path.substring(0,path.lastIndexOf("/")));
		File[] list = parentDir.listFiles();
		ArrayList<File> listFile = new ArrayList<File>();				
		for(File f :list){
			if(f.getName().toLowerCase().endsWith(ekstensi)){
				listFile.add(f);				
				String a = path.substring(path.lastIndexOf("/")+1);
				String b=""; 
						try{
							a.substring(a.lastIndexOf("|"));
						}catch(Exception e){}
				if(b.length()>0){
					if(f.toString().contains(b)){
						result = true;
						break;
					}else{
						result = false;
					}
				}else{
					if(f.toString().contains(a)){
						result = true;
						break;
					}else{
						result = false;
					}
				}
				
			}			
		}
		return result;
	}
	
	
	
	private boolean chekImageStream(String source) {
		String namaKomponen = source.replace("$", "");
		
			for (int i = 0; i < this.cekKomponen.size(); i++) {
				tmp = cekKomponen.get(i);				

						if (tmp.getName().equalsIgnoreCase(namaKomponen)) {
							break;

					}				
				
			}
			resultDataStream = tmp.getDataComp(tmp, source);
				
		if (resultDataStream!=null&&resultDataStream.length() > 9 ) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkImageFile(String source) {
		String tile;
		String head;
		String strCode = null;
		Boolean result = false;
		String activityId = Generator.currModelActivityID;

		String[] strCodetmp = Utility.split(tmp.getList(), "|");
		if (strCodetmp.length > 1) {
			for (int i = 0; i < strCodetmp.length; i++) {
				if (strCodetmp[i].contains("@")) {
					strCode = strCodetmp[i].replace("@", "$");
				}
			}
			tile = activityId + "." + tmp.getName();
			head = getValueComp(strCode) + ".jpg";
		} else {
			tile = activityId + "." + tmp.getName();
			
			if (tmp.getText().length() <= 2) {	
				if(tmp instanceof SignatureUi){
					head = ((SignatureUi)tmp).getStrID()+".png";
				}else{
					head = tmp.getText();	
				}
			} else {
				head = tmp.getText().substring(tmp.getText().lastIndexOf("_")+1);
			}

		}

		Boolean tmpFile = checkImage(tile, head, 1);
		Boolean imgFile = checkImage(tile, head, 2);

		if (tmpFile == true && imgFile == false) {
			result = true;
		} else if (tmpFile == false && imgFile == true) {
			result = true;
		} else if (tmpFile == true && imgFile == true) {
			result = true;
		} else if (tmpFile == false && imgFile == false) {
			result = false;
		} else {
			result = false;
		}

		return result;
	}
	/*
	 * role dalam memberikan log
	 */
	private ModelVerificationPhoto checkData(String source) {

		Boolean check_stream = chekImageStream(source);
		Boolean check_imageFile = checkImageFile(source);

		ModelVerificationPhoto result = new ModelVerificationPhoto();

		if (check_stream == false && check_imageFile == false) {
			result.setNotification("Anda belum melakukan foto");
			result.setFlag(source + ":0");
			result.setLabel(tmp.getLabel());
		} else if (check_stream == false && check_imageFile == true) {
			result.setNotification("file gambar belum terbentuk mohon capture kembali");
			result.setFlag(source + ":1");			
			deleteFile();
			result.setLabel(tmp.getLabel());
		} else if (check_stream == true && check_imageFile == false) {
			result.setNotification("file gambar belum terbentuk mohon capture kembali");
			result.setFlag(source + ":2");
			tmp.setText("");
			result.setLabel(tmp.getLabel());
		} else if (check_stream == true && check_imageFile == true) {		
			//after UAT meeting
			boolean file_size = checkFileSize();			
			if(file_size==true){
				result.setNotification("OK");
				result.setFlag(source + ":3");
				result.setLabel(tmp.getLabel());
			}else{
				result.setNotification("data corrupt");
				result.setFlag(source + ":4");
				tmp.setText("");
				deleteFile();
				result.setLabel(tmp.getLabel());
			}
		} else {
			return result;
		}
		return result;
	}
	/*untuk melakukan pengecekan apakah 
	 * file ada 
	*/
	private boolean checkFileSize(){
		boolean result = false;
		double file_size = 0;
		if(imgTemp2.exists()){
			file_size = imgTemp2.length();
		}
		if(file_size>0){
			result = true;
		}
			
		return result;
	}
	
	private boolean checkFileSize2(){
		boolean result = false;
		double file_size = 0;
		String folder = imgTemp2.toString().substring(0,imgTemp2.toString().lastIndexOf("/"));
		String fileName= imgTemp2.toString().substring(imgTemp2.toString().lastIndexOf("/")+1);
		File parentDir = new File(folder);
		File[] list = parentDir.listFiles();
		for (File f : list){
			if(f.toString().contains(fileName)){
				file_size = f.length();
				if(file_size>0){
					result = true;
				}	
			}
		}			
		return result;
	}

	
	
	/*untuk menghapus fila yang di anggap  
	 * corrupt
	 */	
	private void deleteFile(){
		if(imgTemp2.exists()){
			imgTemp2.delete();
			if(imgTemp2.exists()){
				imgTemp2.delete();	
			}
		}			
	}
	
	private void deleteFile2(){
		String folder = imgTemp2.toString().substring(0,imgTemp2.toString().lastIndexOf("/"));
		String fileName= imgTemp2.toString().substring(imgTemp2.toString().lastIndexOf("/")+1);
		File parentDir = new File(folder);
		File[] list = parentDir.listFiles();
		for (File f : list){
			if(f.toString().contains(fileName)){
					f.delete();
				}	
			}
	}
	private String getValueComp(String comp) {
		return StringEscapeUtils.escapeSql(tmp.getDataComp(tmp, comp));
	}

	public Vector<Component> getCekKomponen() {
		return cekKomponen;
	}

	public void setCekKomponen(Vector<Component> cekKomponen) {
		this.cekKomponen = cekKomponen;
	}

	public String getsComponents() {
		return sComponents;
	}

	public void setsComponents(String sComponents) {
		this.sComponents = sComponents;
	}

}
