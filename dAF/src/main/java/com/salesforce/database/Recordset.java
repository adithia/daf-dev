package com.salesforce.database;

import java.util.ArrayList;
import java.util.Vector;

public interface Recordset {
	public Vector<String> getAllHeaderVector();
	public Vector<Vector<String>> getAllDataVector();
	public ArrayList<String> getAllHeaderList();
	public ArrayList<ArrayList<String>> getAllDataList();
	public Vector<String> getRecordFromHeader(String colname);
	
//	public Vector<String> getFirstData();
	
	public int getRows();
	public int getCols();
	
	public String getText(int row, int col);
	public String getText(int row, String colname);
	public String getHeader(int col);
}
