package com.salesforce.database;

public class QueryClass {
	
	public static String versionNo(String office_code, String dataSync){
		Recordset data = Connection.DBquery("select last_version_no from TRN_MOBILE_DATA_SYNC_INFO where office_code='"+office_code+"' and data_sync_id='"+dataSync+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "last_version_no");
		}
		return "";
	}

	public static String getBussUnit(String bussDesc){
		Recordset data = Connection.DBquery("SELECT buss_unit FROM MST_BU WHERE buss_unit_desc = '"+bussDesc+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "buss_unit");
		}
		return "";
	}
	
	public static String getBussName(String bussId,String coy_id){
		Recordset data = Connection.DBquery("SELECT buss_unit_desc FROM MST_BU WHERE buss_unit = '"+bussId+"' AND COY_ID='"+coy_id+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "buss_unit_desc");
		}
		return "";
	}
	
	public static String getVersionNo(){
		Recordset data = Connection.DBquery("select verno from IVERSION where is_download ='0' order by versionid asc");
		if (data.getRows() > 0) {
			return data.getText(0, "VERSIONNO");
		}
		return "";
	}
	
	public static String getCoyId(String coyName){
		Recordset data = Connection.DBquery("SELECT coy_id FROM MST_COY_ID WHERE coy_name = '"+coyName+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "coy_id");
		}
		return "";
	}
	
	public static String getCoyName(String coyId){
		Recordset data = Connection.DBquery("SELECT coy_name FROM MST_COY_ID WHERE coy_id = '"+coyId+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "coy_name");
		}
		return "";
	}
	
	public static String getPlatformID(String platFormName){
		Recordset data = Connection.DBquery("SELECT platform FROM MST_PLATFORM WHERE platform_desc = '"+platFormName+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "platform");
		}
		return "";
	}
	
	public static String getPlatformName(String platformId,String coy_id){
		Recordset data = Connection.DBquery("SELECT PLATFORM_DESC FROM MST_PLATFORM WHERE PLATFORM='"+platformId+"' AND COY_ID='"+coy_id+"'");
		if(data.getRows()>0){
			return data.getText(0, "platform_desc");
		}
		return "";
	}
	
	public static String getDataDraft(String whereClause){
		Recordset data = Connection.DBquery("SELECT data FROM DRAFT WHERE orderCode ='"+whereClause+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "data");
		}
		return "";
	}
	
	public static String getDataLogOrder(String whereClause){
		Recordset data = Connection.DBquery("SELECT data FROM log_order WHERE order_id ='"+whereClause+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "data");
		}
		return "";
	}
	
	public static boolean isOndataPending(String orderid){
		Recordset data = Connection.DBquery("SELECT data FROM data_activity WHERE orderCode ='"+orderid+"'");
		if (data.getRows() > 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isSent(String orderid){
		Recordset data = Connection.DBquery("SELECT data FROM log_order WHERE sent ='Y' AND order_id ='"+orderid+"'");
		if (data.getRows() > 0) {
			return true;
		}
		return false;
	}
	
	public static String getDataPending(String whereClause){
		Recordset data = Connection.DBquery("SELECT data FROM data_activity WHERE activityId ='"+whereClause+"'");
		if (data.getRows() > 0) {			
			return data.getText(0, "data");
		}
		return "";
	}
	
	public static String isTasklits(String whereClause){
		Recordset data = Connection.DBquery("SELECT isTasklist FROM data_activity WHERE activityId ='"+whereClause+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "istasklist");
		}
		return "";
	}
	
	public static String getDataPendingNView(String whereClause){
		Recordset data = Connection.DBquery("SELECT nview FROM data_activity WHERE activityId ='"+whereClause+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "nview");
		}
		return "";
	}
	
	public static boolean isHaveTable(String table){
		Recordset data = Connection.DBquery("SELECT * FROM "+table);
		if (data.getRows() > 0) {
			return true;
		}
		return false;
	}
	
	/*public static String getJobcodeByUser(String user){
		Recordset data = Connection.DBquery("SELECT jobcode FROM Log_Login WHERE user ='"+user+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "jobcode");
		}
		return "";
	}
	
	public static String getDateLogByUser(String user){
		Recordset data = Connection.DBquery("SELECT datelog FROM Log_Login WHERE user ='"+user+"'");
		if (data.getRows() > 0) {
			return data.getText(0, "datelog");
		}
		return "";
	}
	
	public static boolean getPasswordByUser(String user, String psw){
		Recordset data = Connection.DBquery("SELECT password FROM Log_Login WHERE user ='"+user+"'");
		if (data.getRows() > 0) {
			if (psw.equalsIgnoreCase(data.getText(0, "password"))) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean userExist(String user){
		Recordset data = Connection.DBquery("SELECT user FROM Log_Login WHERE user ='"+user+"'");
		if (data.getRows() > 0) {
			return true;
		}
		return false;
	}*/
}
