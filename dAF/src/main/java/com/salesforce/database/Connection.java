package com.salesforce.database;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.salesforce.utility.Utility;

@SuppressLint("DefaultLocale")
public class Connection {
	public interface HttpListener {
		public void onStream(InputStream is, OutputStream os); 
	}
	private static SQLiteDatabase mConn ;
	//private static Context curractivity;
	private static String dbVersion="2";
	
	public static Context getCurrContext() {
		return Utility.getAppContext();
	}
	
	public static void DBopen(Context activity){
		Utility.setAppContext(activity);
		DBopen();
	}
	public static void DBopen(){
		if (!Utility.getSetting(getCurrContext().getApplicationContext(), "DBVERSION", "").equals(dbVersion)) {
			copyDB();
			Utility.setSetting(getCurrContext().getApplicationContext(), "DBVERSION", dbVersion);
		}		

		if (mConn!=null) {
			if (mConn.isOpen()) {
				return;
			}
		}else{
			checkDB();
		}
		try {
			mConn=SQLiteDatabase.openDatabase(getDefaultDB(), null, SQLiteDatabase.OPEN_READWRITE);	
		} catch (Exception e) { 
			e.printStackTrace();
		}			
	}
	private static String getDefaultDB(){
		return Utility.getDefaultPath("data.db");	
	}
	private static void checkDB() {
		try {
			SQLiteDatabase db=SQLiteDatabase.openDatabase(getDefaultDB(), null, SQLiteDatabase.OPEN_READWRITE);
			db.close();
			return ;
		} catch (Exception e) {}
		copyDB();
	}
	private static void copyDB() {
		Utility.createFolderAll(getDefaultDB());
		try {
			InputStream is =  getCurrContext().getAssets().open("data.db");
			OutputStream os = new FileOutputStream(getDefaultDB());

			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception e) {}
	}
	public static Cursor DBexecute(String SQL){
		try {
			return mConn.rawQuery(SQL, null);
		} catch (Exception e) {
			return Connection.ZeroCursor();
		}
	}
	
	public synchronized static long DBQueryDAF(String SQL){
		try {
			Cursor curr = Connection.DBexecute(SQL);
			curr.close();
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
		
	}

	public synchronized static Recordset DBquery(String SQL){
		Vector<String> header = new Vector<String>();
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		ArrayList<String> headerList = new ArrayList<String>();
		ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
		
		try {
			Cursor curr = Connection.DBexecute(SQL);
			
			for (int i = 0; i < curr.getColumnCount(); i++) {
				header.addElement(curr.getColumnName(i).toLowerCase());
				headerList.add(curr.getColumnName(i).toLowerCase());
			}
			
			while (curr.moveToNext()) {
				Vector<String> field = new Vector<String>();
				ArrayList<String> fieldList = new ArrayList<String>();
				for (int i = 0; i < curr.getColumnCount(); i++) {
					field.addElement(curr.getString(i));
					fieldList.add(curr.getString(i));
				}
				data.addElement(field);
				dataList.add(fieldList);
			}
			curr.close();
		} catch (Exception e) { 
			e.printStackTrace();
		}
		
		return new Recordset() {	
			Vector<String> headerNRecord = new Vector<String>();
			Vector<String> header; 
//			Vector<String> firstData; 
			Vector<Vector<String>> data ;
			ArrayList<String> headerList;
			ArrayList<ArrayList<String>> dataList;
			
			public Recordset get(Vector<String> header, ArrayList<String> headerList, Vector<Vector<String>> data, ArrayList<ArrayList<String>> dataList ){
				this.header=header;
				this.data=data;
				this.dataList = dataList;
				this.headerList = headerList;
				return this;
			}
			public String getText(int row, String colname) {
				return getText(row, header.indexOf(colname));
			}
			public String getText(int row, int col) {
				try {
					String record = data.elementAt(row).elementAt(col);
					if (record != null) {
						return record;
					}else
						return "";
					
				} catch (Exception e) { 
					return "";
				}
			}
			public int getRows() {
				return data.size();
			}			
			public String getHeader(int col) {
				return header.elementAt(col);
			}
			public int getCols() {
				return header.size();
			}
			@Override
			public Vector<String> getAllHeaderVector() {
				return header;
			}
			@Override
			public Vector<String> getRecordFromHeader(String colname) {
				for (int i = 0; i < header.size(); i++) {
					if (colname.equalsIgnoreCase(header.elementAt(i).toString())) {
						for (int j = 0; j < getRows(); j++) {
							String record = getText(j, i);
							headerNRecord.add(record);							
						}
					}
				}
				return headerNRecord;
			}
			@Override
			public Vector<Vector<String>> getAllDataVector() {
				// TODO Auto-generated method stub
				return data;
			}
			@Override
			public ArrayList<ArrayList<String>> getAllDataList() {
				// TODO Auto-generated method stub
				return dataList;
			}
			@Override
			public ArrayList<String> getAllHeaderList() {
				// TODO Auto-generated method stub
				return headerList;
			}
//			@Override
//			public Vector<String> getFirstData() {
//				// TODO Auto-generated method stub
//				return firstData;
//			}
		}.get(header, headerList, data, dataList);
	}
	
	public static void DBdelete(String table, String...namevalue){
		StringBuffer whereClause = new StringBuffer() ;
		String[] whereArgs = new String[0]; 
		
		whereArgs = new String[namevalue.length]; 
		for (int j = 0; j < namevalue.length;j++) {
			int key = namevalue[j].indexOf("=");
			if (key!=-1) {
				whereClause.append(namevalue[j].substring(0, key)).append("=?").append((j!=namevalue.length-1)?"  ":""); 
				whereArgs[j]=namevalue[j].substring(key+1);
			}
		}
		DBdelete(table, whereClause.toString(), whereArgs);
	}
	public static void DBdelete(String table){
		DBdelete(table, null, null);
	}		
	public static long DBdeleteDAF(String table){
		return DBdeleteDAF(table, null, null);
	}
	public static void DBdelete(String table, String whereClause, String[] whereArgs){
		try {
			mConn.delete(table, whereClause, whereArgs);
		} catch (Exception e) {
			Log.e("DBError", e.getMessage());
			e.printStackTrace();
		}	
	}
	public static long DBdeleteDAF(String table, String whereClause, String[] whereArgs){
		try {
			return mConn.delete(table, whereClause, whereArgs);
		} catch (Exception e) {
			Log.e("DBError", e.getMessage());
			e.printStackTrace();
			return 0;
		}	
	}
	public static long DBinsert(String table, ContentValues contentvalues){
		try {
			return mConn.insert(table, "", contentvalues);
			
		} catch (Exception e) {
			Log.e("DBError", e.getMessage());
			return 0;
		}	
	}
	public static long DBinsert(String table, String...namevalue){
		ContentValues contentValues = new ContentValues();
		for (int i = 0; i < namevalue.length; i++) {
			int key = namevalue[i].indexOf("=");
		
			if (key!=-1) {
				contentValues.put(namevalue[i].substring(0, key), namevalue[i].substring(key+1));
			}
		}
		return DBinsert(table, contentValues);
	}
	public static void DBinsert2(String table, Vector<String> namevalue){
		ContentValues contentValues = new ContentValues();
		for (int i = 0; i < namevalue.size(); i++) {
			int key = namevalue.elementAt(i).toString().indexOf("=");
		
			if (key!=-1) {
				String strVal = "";
				strVal = namevalue.elementAt(i).toString().substring(key+1);
				if (strVal.contentEquals("null")) {
					strVal = "";
				}
				contentValues.put(namevalue.elementAt(i).toString().substring(0, key), strVal);
			}
		}
		mConn.beginTransaction();
		DBinsert(table, contentValues);
		mConn.setTransactionSuccessful();
		mConn.endTransaction();
//		DBinsert(table, contentValues);
	}
	public static long DBinsert(String table, Vector<String> namevalue){
		ContentValues contentValues = new ContentValues();
		for (int i = 0; i < namevalue.size(); i++) {
			int key = namevalue.elementAt(i).toString().indexOf("=");
		
			if (key!=-1) {
				String strVal = "";
				strVal = namevalue.elementAt(i).toString().substring(key+1);
				if (strVal.contentEquals("null")) {
					strVal = "";
				}
				contentValues.put(namevalue.elementAt(i).toString().substring(0, key), strVal);
//				contentValues.put(namevalue.elementAt(i).toString().substring(0, key), namevalue.elementAt(i).toString().substring(key+1));
			}
		}
		return DBinsert(table, contentValues);
	}
	public static void DBupdate(String table, ContentValues initialValues, String whereClause, String whereArgs[]){
		try {
			mConn.update(table, initialValues, whereClause, whereArgs);
		} catch (Exception e) {
			Log.e("DBError", e.getMessage());
			e.printStackTrace();
		}	
	}
	public static long DBupdateDAF(String table, ContentValues initialValues, String whereClause, String whereArgs[]){
		try {
			return mConn.update(table, initialValues, whereClause, whereArgs);
		} catch (Exception e) {
			Log.e("DBError", e.getMessage());
			e.printStackTrace();
			return 0;
		}	
	}
	
	public static void DBupdate(String table, String...namevalue){
		ContentValues contentValues = new ContentValues();
		StringBuffer whereClause = new StringBuffer() ;
		String[] whereArgs = new String[0]; 
		for (int i = 0; i < namevalue.length; i++) {
			if (namevalue[i].toLowerCase().trim().equals("where")) {
				whereArgs = new String[namevalue.length-i-1]; 
				for (int j = i+1; j < namevalue.length;j++) {
					int key = namevalue[j].indexOf("=");
					if (key!=-1) {
						whereClause.append(namevalue[j].substring(0, key)).append("=?").append((j!=namevalue.length-1)?"  ":""); 
						whereArgs[j-i-1]=namevalue[j].substring(key+1);
					}
				}
				break;
			}else{
				int key = namevalue[i].indexOf("=");
				if (key!=-1) {
					contentValues.put(namevalue[i].substring(0, key), namevalue[i].substring(key+1));
				}
			}
			
		}
		DBupdate(table, contentValues, whereClause.toString(), whereArgs);
	}
	
	public static long DBupdateDAF(String table, String...namevalue){
		ContentValues contentValues = new ContentValues();
		StringBuffer whereClause = new StringBuffer() ;
		String[] whereArgs = new String[0]; 
		for (int i = 0; i < namevalue.length; i++) {
			if (namevalue[i].toLowerCase().trim().equals("where")) {
				whereArgs = new String[namevalue.length-i-1]; 
				for (int j = i+1; j < namevalue.length;j++) {
					int key = namevalue[j].indexOf("=");
					if (key!=-1) {
						whereClause.append(namevalue[j].substring(0, key)).append("=?").append((j!=namevalue.length-1)?"  ":""); 
						whereArgs[j-i-1]=namevalue[j].substring(key+1);
					}
				}
				break;
			}else{
				int key = namevalue[i].indexOf("=");
				if (key!=-1) {
					contentValues.put(namevalue[i].substring(0, key), namevalue[i].substring(key+1));
				}
			}
			
		}
		return DBupdateDAF(table, contentValues, whereClause.toString(), whereArgs);
	}
	public static void DBcreate(String SQL){
		try {
			mConn.execSQL(SQL);
		} catch (Exception e) {
			Log.i("akbar", e.getMessage());
			e.printStackTrace();
		}
	}
	public static void DBdrop(String table){
		try {
			mConn.execSQL("DROP table "+table);
		} catch (Exception e) {}
	}
	public static void DBcreate(String tablename, String...fields){
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("CREATE table ").append(tablename).append(" (id  INTEGER PRIMARY KEY AUTOINCREMENT, ");
		for (int i = 0; i < fields.length; i++) {
			if (!fields[i].trim().equalsIgnoreCase("id")) {
				if (fields[i].trim().startsWith("-")) {
					sbuff.append(fields[i].substring(1)).append(" INTEGER").append((i!=fields.length-1)?",":");");
				}else{
					sbuff.append(fields[i]).append(" TEXT").append((i!=fields.length-1)?",":");");
				}	
			}		
		}
		DBcreate(sbuff.toString());
	}
	public static void DBclose(){
		try {
			mConn.close();
		} catch (Exception e) {}
	}
	public static void removeFile(String file){
		Utility.deleteFileAll(file);
	}
	
	public static String getSetting(String key, String def){ 
	       SharedPreferences settings = getCurrContext().getApplicationContext().getSharedPreferences("nikita.framework", 0);
	       String silent = settings.getString(key, def);
	       return silent;
	}
	public static void setSetting(String key, String val){ 
	      SharedPreferences settings = getCurrContext().getApplicationContext().getSharedPreferences("nikita.framework", 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString(key, val);
	      editor.commit();
	}	
	public static String openHttp(String url){
		return Utility.getHttpConnection(url);
	}
	public static String openHttp(String link, HttpListener list){
		StringBuffer data = new StringBuffer();
		try{
	  		 URL url = new URL(link);
	  		 URLConnection ucon = url.openConnection();
	  		 ucon.setConnectTimeout(25000);
	
	  		 InputStream is = ucon.getInputStream();
	  		 if (list!=null) {
	  			list.onStream(is, ucon.getOutputStream());
			 }else{
				 int current = 0;
	             while ((current = is.read()) != -1) {
	             	 data.append((char) current);
	             }  
			 }        
         } catch (Exception e) { data.append("Connection Timeout"); }
         return data.toString();
	}
	public static InputStream openFile(String file){
		try {
			return new FileInputStream(file);
		} catch (Exception e) { }		
		return null;
	}
	public static String openFileString(String file){
		try {
			InputStream is =openFile(file);
			byte[] buffer = new byte[1024];
			int length;StringBuffer sbBuffer = new StringBuffer("");
			while ((length = is.read(buffer)) > 0) {
				sbBuffer.append(new String(buffer, 0, length));
			}
			return sbBuffer.toString();
		} catch (Exception e) { }
		return "";
	}
	public InputStream readAsset(String name) {
		try {
			return getCurrContext().getAssets().open(name);
		} catch (Exception e) { return  null; }		
	}
	public static OutputStream writeFile(String file){
		try {
			return new FileOutputStream(file);
		} catch (Exception e) { } 
		return null;
	}
	public static void saveFile(String file, byte[] out){
		try {
			OutputStream os = new FileOutputStream(file);
			os.write(out);
			os.flush();
			os.close(); 
		} catch (Exception e) {}
	}
	@SuppressWarnings("unused")
	private static String path(String file){
		return file;
	}
	private static Cursor ZeroCursor() {
		return new Cursor() {
			public int getCount() {return 0;}
			public int getPosition() {return 0;}
			public boolean move(int offset) {return false;}
			public boolean moveToPosition(int position) {return false;}
			public boolean moveToFirst() {return false;}
			public boolean moveToLast() {return false;}
			public boolean moveToNext() {return false;}
			public boolean moveToPrevious() {return false;}
			public boolean isFirst() {return false;}
			public boolean isLast() {return false;}
			public boolean isBeforeFirst() {return false;}
			public boolean isAfterLast() {return false;}
			public int getColumnIndex(String columnName) {return 0;}
			public int getColumnIndexOrThrow(String columnName)throws IllegalArgumentException {return 0;}
			public String getColumnName(int columnIndex) {return ""; }
			public String[] getColumnNames() { return new String[0];}
			public int getColumnCount() {return 0;}
			public byte[] getBlob(int columnIndex) { return new byte[0]; }
			public String getString(int columnIndex) { return ""; }
			public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) { }
			public short getShort(int columnIndex) { return 0; }
			public int getInt(int columnIndex) { return 0; }
			public long getLong(int columnIndex) {return 0;}
			public float getFloat(int columnIndex) {return 0;}
			public double getDouble(int columnIndex) {return 0;}
			public int getType(int columnIndex) {return 0;}
			public boolean isNull(int columnIndex) {return false;}
			public void deactivate() {}
			public boolean requery() {return false; }
			public void close() {}
			public boolean isClosed() { return false; }
			public void registerContentObserver(ContentObserver observer) {}
			public void unregisterContentObserver(ContentObserver observer) {}
			public void registerDataSetObserver(DataSetObserver observer) {}
			public void unregisterDataSetObserver(DataSetObserver observer) {}
			public void setNotificationUri(ContentResolver cr, Uri uri) {}

			@Override
			public Uri getNotificationUri() {
				return null;
			}

			public boolean getWantsAllOnMoveCalls() {return false;}

			@Override
			public void setExtras(Bundle extras) {

			}

			public Bundle getExtras() {return null;}
			public Bundle respond(Bundle extras) {return null;}
		};
	}
	 
	 
}
