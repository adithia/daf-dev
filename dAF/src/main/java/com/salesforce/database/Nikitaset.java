package com.salesforce.database;

import java.util.ArrayList;
import java.util.Vector;

import android.database.Cursor;

public class Nikitaset implements Recordset {
	final Vector<String> header = new Vector<String>();
	final Vector<Vector<String>> data = new Vector<Vector<String>>();
	final ArrayList<String> headerList = new ArrayList<String>();
	final ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();

	public void addHeader(String text) {
		header.addElement(text);
		headerList.add(text);
	}

	public void addHeader(String... headers) {
		for (int i = 0; i < headers.length; i++) {
			header.addElement(headers[i]);
			headerList.add(headers[i]);
		}
	}

	public void addRow(String... fields) {
		Vector<String> field = new Vector<String>();
		ArrayList<String> fieldList = new ArrayList<String>();
		for (int i = 0; i < header.size(); i++) {
			if (i > fields.length) {
				field.addElement("");
				fieldList.add("");
			} else {
				field.addElement(fields[i]);
				fieldList.add(fields[i]);
			}
		}
		data.addElement(field);
		dataList.add(fieldList);
	}

	public Nikitaset() {

	}

	public String getText(int row, String colname) {
		return getText(row, header.indexOf(colname));
	}

	public String getText(int row, int col) {
		try {
			return data.elementAt(row).elementAt(col);
		} catch (Exception e) {
		}
		return "";
	}

	public int getRows() {
		return data.size();
	}

	public String getHeader(int col) {
		return header.elementAt(col);
	}

	public int getCols() {
		return header.size();
	}

	@Override
	public Vector<String> getAllHeaderVector() {
		return header;
	}

	public static Recordset getRecordset(Nset curr) {
		final Vector<String> header = new Vector<String>();
		final Vector<Vector<String>> data = new Vector<Vector<String>>();
		final ArrayList<String> headerList = new ArrayList<String>();
		final ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
	 
		for (int i = 0; i < curr.getData("header").getArraySize(); i++) {
			header.addElement(curr.getData("header").getData(i).toString());
			headerList.add(curr.getData("header").getData(i).toString());
		}

		for (int i = 0; i < curr.getData("data").getArraySize(); i++) {
			Vector<String> s = new Vector<String>();
			ArrayList<String> fieldList = new ArrayList<String>();
			for (int c = 0; c < curr.getData("data").getData(i).getArraySize(); c++) {
				s.addElement(curr.getData("data").getData(i).getData(c).toString());
				fieldList.add(curr.getData("data").getData(i).getData(c).toString());
			}
			data.addElement(s);
			dataList.add(fieldList);			
		}
		
		return new Recordset() {
			public String getText(int row, String colname) {
				return getText(row, header.indexOf(colname));
			}

			public String getText(int row, int col) {
				try {
					return data.elementAt(row).elementAt(col);
				} catch (Exception e) {
				}
				return "";
			}

			public int getRows() {
				return data.size();
			}

			public String getHeader(int col) {
				return header.elementAt(col);
			}

			public int getCols() {
				return header.size();
			}

			@Override
			public Vector<String> getAllHeaderVector() {
				return header;
			}

			@Override
			public Vector<String> getRecordFromHeader(String colname) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Vector<Vector<String>> getAllDataVector() {
				// TODO Auto-generated method stub
				return data;
			}

			@Override
			public ArrayList<ArrayList<String>> getAllDataList() {
				// TODO Auto-generated method stub
				return dataList;
			}

			@Override
			public ArrayList<String> getAllHeaderList() {
				// TODO Auto-generated method stub
				return headerList;
			}
		};		
	}
	public static Recordset getRecordset(Cursor curr) {
		final Vector<String> header = new Vector<String>();
		final Vector<Vector<String>> data = new Vector<Vector<String>>();
		try {
			for (int i = 0; i < curr.getColumnCount(); i++) {
				header.addElement(curr.getColumnName(i));
			}

			while (curr.moveToNext()) {
				Vector<String> field = new Vector<String>();
				for (int i = 0; i < curr.getColumnCount(); i++) {
					field.addElement(curr.getString(i));
				}
				data.addElement(field);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Recordset() {
			public String getText(int row, String colname) {
				return getText(row, header.indexOf(colname));
			}

			public String getText(int row, int col) {
				try {
					return data.elementAt(row).elementAt(col);
				} catch (Exception e) {
				}
				return "";
			}

			public int getRows() {
				return data.size();
			}

			public String getHeader(int col) {
				return header.elementAt(col);
			}

			public int getCols() {
				return header.size();
			}

			@Override
			public Vector<String> getAllHeaderVector() {
				return header;
			}

			@Override
			public Vector<String> getRecordFromHeader(String colname) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Vector<Vector<String>> getAllDataVector() {
				// TODO Auto-generated method stub
				return data;
			}

			@Override
			public ArrayList<ArrayList<String>> getAllDataList() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ArrayList<String> getAllHeaderList() {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	@Override
	public Vector<String> getRecordFromHeader(String colname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector<Vector<String>> getAllDataVector() {
		// TODO Auto-generated method stub
		return data;
	}

	@Override
	public ArrayList<ArrayList<String>> getAllDataList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getAllHeaderList() {
		// TODO Auto-generated method stub
		return null;
	}

}
